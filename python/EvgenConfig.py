import os

__all__ = []

from PyJobTransformsCore.TransformConfig import *

knownGenerators = ( 'Herwig', 'Herwigpp', 'Pythia', 'PythiaRhad', 'PythiaB', 'PythiaGS', 'PythiaResMod', 'PythiaTopMdiff', 'Isajet', 'Hijing', 'PhoJet', 'Jimmy', 'McAtNlo', 'MadCUP',
                    'MadGraph', 'CompHep', 'AcerMC', 'Alpgen', 'TopRex', 'Cascade', 'GRAPPA',
                    'ParticleGenerator', 'CosmicGenerator', 'Exhume', 'Charybdis',
                    'Tauola', 'Photos', 'EvtGen', 'Sherpa', 'SherpaF', 'SherpaFF', 'Matchig', 'Winhac', 'Lhef', 'LhaExt' , 'Pomwig', 'Herwigpp', 'Hydjet',
                    'Pythia8', 'Helac', 'BeamHaloGenerator', 'Starlight')
        

class EvgenConfig(TransformConfig):
    __slots__ = ()
    
    generators = ListOfStrings("List of used generators", allowedValues=knownGenerators)
    inputfilebase = String("base of the input filename needed for some generators")
    weighting = Integer("allow or not events weighting",1, AllowedExpression("value >= 0"))
    specialConfig = String("special configuration for subsequent prod steps")
    inputconfbase = String("base of the input filename needed for some generator configs")

    def __init__(self,name='evgenConfig',metaData=['generators']):
        TransformConfig.__init__(self,name,metaData)
    def __init__(self,name='evgenConfig',metaData=['weighting']):
        TransformConfig.__init__(self,name,metaData)
        self.auxfiles = [ 'PDGTABLE.MeV', 'pdt.table', 'DECAY.DEC', 'Bdecays0.dat', 'Bs2Jpsiphi.DEC' ]
        self.minevents = 5000
        self.maxeventsstrategy = 'ABORT'
        self.efficiency = 0.95
        self.specialConfig = 'NONE'

# make configuration object
evgenConfig = EvgenConfig()
