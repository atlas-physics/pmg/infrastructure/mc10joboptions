export PYTHONPATH=./MC10JobOptions:$PYTHONPATH
export JOBOPTSEARCHPATH=./MC10JobOptions:$JOBOPTSEARCHPATH
cd ./share/
tar xzf txtfiles_mc10.tar.gz
cd ../ 
mv ./share ./MC10JobOptions
mv ./python/* ./MC10JobOptions
mv ./common/*.py ./MC10JobOptions
rmdir ./common
rmdir ./python
mkdir ./share
mv ./MC10JobOptions/*DEC ./share
mv ./MC10JobOptions/*model ./share
mv ./MC10JobOptions/*txt ./share
export DATAPATH=./share:$DATAPATH
