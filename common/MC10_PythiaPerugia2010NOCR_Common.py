#______________________________________________________________________________________
#
# Tune Perugia 10 NOCR [334] Perugia 2010 with no CR, less MPI
# liza.mijovic_at_nospam_cern.ch
#
#______________________________________________________________________________________

from Pythia_i.Pythia_iConf import Pythia
topAlg += Pythia()
Pythia = topAlg.Pythia

# MC10 runs with Pythia 6.4.23, NOCR only on PYTUNE since 6.4.24;
#Pythia.Tune_Name="PYTUNE_334"

# => need to pass parameter explicitly
Pythia.Tune_Name="ATLAS_-1"              # turn off ATLAS defaults (tune etc.)
Pythia.PygiveCommand += [ "MSTJ(22)=2" ] # ATLAS stable particles convention

# Perugia 10 NOCR [334] parameter list:
Pythia.PygiveCommand += [ "MSTU(112)=4",
                          "PARU(112)=0.19200",
                          "MSTJ(11)=5",
                          "MSTJ(41)=12",
                          "PARJ(1)=0.08000",
                          "PARJ(2)=0.21000",
                          "PARJ(3)=0.94000",
                          "PARJ(4)=0.04000",
                          "PARJ(11)=0.35000",
                          "PARJ(12)=0.35000",
                          "PARJ(13)=0.54000",
                          "PARJ(25)=0.63000",
                          "PARJ(26)=0.12000",
                          "PARJ(41)=0.35000",
                          "PARJ(42)=0.90000",
                          "PARJ(81)=0.26000",
                          "MSTP(3)=1",
                          "MSTP(64)=3",
                          "MSTP(70)=2",
                          "MSTP(72)=2",
                          "MSTP(82)=5",
                          "MSTP(88)=0",
                          "MSTP(95)=0",
                          "PARP(1)=0.19200",
                          "PARP(61)=0.19200",
                          "PARP(67)=1.00000",
                          "PARP(71)=2.00000",
                          "PARP(72)=0.26000",
                          "PARP(78)=0.00000",
                          "PARP(80)=0.01000",
                          "PARP(82)=2.15000",
                          "PARP(83)=1.80000",
                          "PARP(90)=0.26000",
                          "PARP(93)=10.00000" ]



# ATLAS default masses and EW params:
Pythia.PythiaCommand += [
    # initializations
    "pyinit pylisti 12",
    "pyinit pylistf 1",
    "pystat 1 3 4 5",
    "pyinit dumpr 1 5",
    # mass
    "pydat2 pmas 6 1 172.5",    # PDG2007 TOP mass 
    "pydat2 pmas 24 1 80.403",  # PDG2007 W mass
    "pydat2 pmas 23 1 91.1876", # PDG2007 Z0 mass
    # settings below have no effect (widths calculated perturbatively in Pythia)
    #"pydat2 pmas 24 2 2.141",   # PDG2007 W width
    #"pydat2 pmas 23 2 2.4952",  # PDG2007 Z0 width
    ]

#______________________________________________________________________________________
