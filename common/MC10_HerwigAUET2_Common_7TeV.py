###############################################################
# job option fragments for year 2011 7TeV production
# U.E. tuning parameters for HERWIG 6.510 + Jimmy 4.31
# contact: Claire Gwenlan
###############################################################

from Herwig_i.Herwig_iConf import Herwig
topAlg += Herwig()

Herwig = topAlg.Herwig
Herwig.HerwigCommand = [
    # mass  
    "rmass 6 172.5",    # TOP mass
    "rmass 198 80.403", # PDG2010 W mass
    "rmass 199 80.403", # PDG2010 W mass 
    "rmass 200 91.1876",# PDG2010 Z0 mass
    "gamw 2.141",       # PDG2010 W width
    "gamz 2.4952",      # PDG2010 Z0 width
    # main switch 
    "jmueo 1",          # Underlying event option (2->2 QCD)
    "msflag 1",         # turn on multiple interactions
    "jmbug 0",
    # AUET2 (LO**) tune settings
    "ispac 2",          # ISR-shower scheme
    "qspac 2.5",        # ISR shower cut-off (default value)
    "ptrms 1.2",        # primordial kT
    "ptjim 4.976",      # for 7TeV (min. pT of secondary scatters: ptjim=3.696*(sqrt(s)/1800.)**0.219)
    "jmrad 73 2.339",   # Inverse proton radius squared  
    "prsof 0",          # soft underlying event off (Herwig parameter)
     # PDF 
    "modpdf 20651",     # LO** pdf
    "autpdf HWLHAPDF",  # external PDF library
    # Fragmentation
    "clpow 1.20",       # to fix the ratio of mesons to baryons in B decays
    "pltcut 0.0000000000333",  # to make Ks and Lambda stable
    "ptmin 10.",        # (D=10.) min. pT in hadronic jet production
    # Others
    "mixing 1",         # (D=1) include neutral B meson mixing 
    "maxpr 5"           # print out event record
    ]

