m###############################################################
# job option fragments for year 2010 production 
# U.E. tuning parameters for PYTHIA 6.421 [CTEQ66 tune]
# use Pythia_i/src/atlasTune.cxx
# contact: Claire Gwenlan
###############################################################

from Pythia_i.Pythia_iConf import Pythia
topAlg += Pythia()
Pythia = topAlg.Pythia

# CTEQ66 tune by Holger Schulz (using Professor); based on MC09c

# switch on MC09c tune
Pythia.Tune_Name="ATLAS_20090003"
# Can alternatively set MC09c using PYTUNE from Pythia 6.423
# Pythia.Tune_Name="PYTUNE_331"

Pythia.PythiaCommand = [
    # initializations
    "pyinit pylisti 12",
    "pyinit pylistf 1",
    "pystat 1 3 4 5",
    "pyinit dumpr 1 5",
    # mass
    "pydat2 pmas 6 1 172.5",    # PDG2007 TOP mass 
    "pydat2 pmas 24 1 80.403",  # PDG2007 W mass
    "pydat2 pmas 23 1 91.1876", # PDG2007 Z0 mass
    # the settings below have no effect (widths calculated perturbatively in Pythia)
    #"pydat2 pmas 24 2 2.141",   # PDG2007 W width
    #"pydat2 pmas 23 2 2.4952",  # PDG2007 Z0 width
    ]

# reset parameters appropriate for CTEQ66
Pythia.PythiaCommand += [
    "pypars mstp 51 10550",    # CTEQ66 (for proton PDF)
    "pypars mstp 53 10550",    #
    "pypars mstp 55 10550",    #
    "pypars parp 78 0.2596",   # colour reconnection in FS
    "pypars parp 82 1.5995",   #
    "pypars parp 90 0.2167"    #
    ]
