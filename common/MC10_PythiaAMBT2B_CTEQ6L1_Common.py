#######################################################################################
#
# Job options file for PYTHIA AMBT2B (CTEQ6L1)
# author: C. Gwenlan
#=====================================================================================

from Pythia_i.Pythia_iConf import Pythia
topAlg += Pythia()
Pythia = topAlg.Pythia

Pythia.PythiaCommand += [
    # initializations
    "pyinit pylisti 12",
    "pyinit pylistf 1",
    "pystat 1 3 4 5",
    "pyinit dumpr 1 5",
    # mass
    "pydat2 pmas 6 1 172.5",    # PDG2007 TOP mass
    "pydat2 pmas 24 1 80.403",  # PDG2007 W mass
    "pydat2 pmas 23 1 91.1876", # PDG2007 Z0 mass
    # settings below have no effect (widths calculated perturbatively in Pythia)
    #"pydat2 pmas 24 2 2.141",   # PDG2007 W width
    #"pydat2 pmas 23 2 2.4952",  # PDG2007 Z0 width
    ]


# set up AMBT1 tune
Pythia.Tune_Name="ATLAS_20100001"


## Explicit AMBT2B-CTEQ6L1 params
Pythia.PythiaCommand += [

    ## Set PDF to CTEQ6L1
    "pypars mstp 51 10042",
    "pypars mstp 52 2",
    
    # pT shower and Lund-Bowler frag
    "pydat1 mstj 41 12",
    "pydat1 mstj 11 5",

    # Flavour parameters
    "pydat1 parj 1  7.272809e-02",
    "pydat1 parj 2  2.018845e-01",
    "pydat1 parj 3  9.498471e-01",
    "pydat1 parj 4  3.316182e-02",
    "pydat1 parj 11 3.089764e-01",
    "pydat1 parj 12 4.015396e-01",
    "pydat1 parj 13 5.442874e-01",
    "pydat1 parj 25 6.276964e-01",
    "pydat1 parj 26 1.292377e-01",

    # Fragmentation parameters
    "pydat1 parj 21 3.001463e-01",
    "pydat1 parj 41 3.683123e-01",
    "pydat1 parj 42 1.003531e+00",
    "pydat1 parj 47 8.727703e-01",
    "pydat1 parj 81 2.564716e-01",
    "pydat1 parj 82 8.296215e-01",
    
    ## Different Lambda_QCD for different bits of the shower:
    ## Lambda is given by PARP(1) for hard interactions, by PARP(61) for space-like
    ## showers, by PARP(72) for time-like showers not from a resonance decay,
    ## and by PARJ(81) for time-like ones from a resonance decay
    ## We explicitly set the initial state Lambdas to the AMBT1/accidental LHAPDF values
    "pypars mstp 3 1",
    #
    "pydat1 paru 112 0.192",
    "pypars parp 1 0.192",
    "pypars parp 61 0.192",

    ## ISR cutoff scheme
    "pypars mstp 70 0",

    ## ISR stage fixed params for 3-param tune
    "pypars parp 67 1.00",
    "pypars parp 91 2.00", # intrinsic kT

    ## ISR tuning stage results
    "pypars parp 62 1.13",
    "pypars parp 64 0.68",
    "pypars parp 72 0.527",
    
    ## MPI params
    "pypars parp 77 3.566023e-01",
    "pypars parp 78 2.350777e-01",
    "pypars parp 82 2.342685e+00",
    "pypars parp 84 6.053474e-01",
    "pypars parp 90 2.456730e-01",

    ]
