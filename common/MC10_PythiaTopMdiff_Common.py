# MC10_PythiaTopMdiff_Common.py, to hold common tune settings

from PythiaExo_i.PythiaTopMdiff_iConf import PythiaTopMdiff
topAlg += PythiaTopMdiff()
Pythia = topAlg.PythiaTopMdiff

#theApp.EvtMax = 10

#----------------------------------------------------------------------------------------------------------------------
# set up tune
Pythia.Tune_Name="ATLAS_20100001"

# Can alternatively set AMBT1 using PYTUNE from Pythia 6.423
# Pythia.Tune_Name="PYTUNE_340"

# relevant proc. setup
Pythia.PythiaCommand += [
    "pyinit pylisti 12",
    "pyinit pylistf 1",
    "pystat 1 3 4 5",
    "pyinit dumpr 1 5",
    # mass
    "pydat2 pmas 24 1 80.403",  # PDG2007 W mass
    "pydat2 pmas 23 1 91.1876", # PDG2007 Z0 mass
    ]



