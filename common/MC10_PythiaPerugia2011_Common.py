#######################################################################################
#
# Job options file for Perugia2011
# author: A. Buckley
# reference Perugia2011 http://arxiv.org/abs/arXiv:1005.3457 v4
#=====================================================================================

from Pythia_i.Pythia_iConf import Pythia
topAlg += Pythia()
Pythia = topAlg.Pythia

## PYTUNE 350: Perugia2011 -- not available before PYTHIA 6.4.25
#topAlg.Pythia.Tune_Name="PYTUNE_350"

# => need to pass parameter explicitly
Pythia.Tune_Name="ATLAS_-1"              # turn off ATLAS defaults (tune etc.)
Pythia.PygiveCommand += [ "MSTJ(22)=2" ] # ATLAS stable particles convention

## Explicit P2011 params, taken from Tables 5, 6, 7 in paper
Pythia.PygiveCommand += [ "MSTJ(11)=5",
                          "PARJ(1)=0.087",
                          "PARJ(2)=0.19",
                          "PARJ(3)=0.95",
                          "PARJ(4)=0.043",
                          "PARJ(6)=1.0",
                          "PARJ(7)=1.0",
                          "PARJ(11)=0.35",
                          "PARJ(12)=0.40",
                          "PARJ(13)=0.54",
                          "PARJ(21)=0.33",
                          "PARJ(25)=0.63",
                          "PARJ(26)=0.12",
                          "PARJ(41)=0.35",
                          "PARJ(42)=0.80",
                          "PARJ(45)=0.55",
                          "PARJ(46)=1.0",
                          "PARJ(47)=1.0",
                          #
                          "MSTP(51)=7", # CTEQ5L pdf
                          "MSTP(52)=1", # CTEQ5L pdf
                          "MSTP(3)=1",
                          "MSTP(64)=2",
                          "MSTU(112)=5",
                          "PARP(61)=0.26",
                          "PARP(72)=0.26",
                          "PARJ(81)=0.26",
                          "PARP(1)=0.16",
                          "PARU(112)=0.16",
                          "PARP(64)=1.0",
                          "MSTP(67)=2",
                          "PARP(67)=1.0",
                          "PARP(71)=1.0",
                          "MSTP(70)=0",
                          "MSTP(72)=2",
                          "PARP(62)=1.5",
                          "PARJ(82)=1.0",
                          "MSTP(91)=1",
                          "PARP(91)=1.0",
                          "PARP(93)=10.0",
                          #
                          "MSTP(81)=21",
                          "PARP(82)=2.93",
                          "PARP(89)=7000",
                          "PARP(90)=0.265",
                          "MSTP(82)=3",
                          "MSTP(33)=0",
                          "MSTP(88)=0",
                          "PARP(79)=2.0",
                          "MSTP(89)=0",
                          "PARP(80)=0.015",
                          "MSTP(95)=8",
                          "PARP(78)=0.036",
                          "PARP(77)=1.0",
    ]



Pythia.PythiaCommand += [
    # initializations
    "pyinit pylisti 12",
    "pyinit pylistf 1",
    "pystat 1 3 4 5",
    "pyinit dumpr 1 5",
    # mass
    "pydat2 pmas 6 1 172.5",    # PDG2007 TOP mass
    "pydat2 pmas 24 1 80.403",  # PDG2007 W mass
    "pydat2 pmas 23 1 91.1876", # PDG2007 Z0 mass
    # settings below have no effect (widths calculated perturbatively in Pythia)
    #"pydat2 pmas 24 2 2.141",   # PDG2007 W width
    #"pydat2 pmas 23 2 2.4952",  # PDG2007 Z0 width
    ]



