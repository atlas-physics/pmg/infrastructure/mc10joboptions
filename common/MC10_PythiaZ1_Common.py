###############################################################
# job option fragments for year 2010 production
# Z1 tune
# use Pythia_i/src/atlasTune.cxx
# contact: Claire Gwenlan
###############################################################

from Pythia_i.Pythia_iConf import Pythia
topAlg += Pythia()
Pythia = topAlg.Pythia


Pythia.PythiaCommand = [
    # initializations
    "pyinit pylisti 12",
    "pyinit pylistf 1",
    "pystat 1 3 4 5",
    "pyinit dumpr 1 5",
    # mass
    "pydat2 pmas 6 1 172.5",    # PDG2007 TOP mass 
    "pydat2 pmas 24 1 80.403",  # PDG2007 W mass
    "pydat2 pmas 23 1 91.1876", # PDG2007 Z0 mass
    # the settings below have no effect (widths calculated perturbatively in Pythia)
    #"pydat2 pmas 24 2 2.141",   # PDG2007 W width
    #"pydat2 pmas 23 2 2.4952",  # PDG2007 Z0 width
    ]

# set up AMBT1 tune
Pythia.Tune_Name="ATLAS_20100001"

## Explicit Z1 params
Pythia.PythiaCommand += [

    ## Set PDF to (internal) CTEQ5L
    "pypars mstp 51 7",
    "pypars mstp 52 1",
    
    # MPI parameters - adjusted from AMBT1
    "pypars parp 82  1.9320",
    "pypars parp 90  0.2750"

    # flavour/fragmentation parameters (all defaults/AMBT1 values, except PARJ(46)
    "pydat1 mstj 11 4",
    "pydat1 parj 1  0.1000",
    "pydat1 parj 2  0.3000",
    "pydat1 parj 3  0.4000",
    "pydat1 parj 4  0.0500",
    "pydat1 parj 5  0.5000",
    "pydat1 parj 6  0.5000",
    "pydat1 parj 7  0.5000",
    "pydat1 parj 11 0.5000",
    "pydat1 parj 12 0.6000",
    "pydat1 parj 13 0.7500",
    "pydat1 parj 21 0.3600",
    "pydat1 parj 25 1.0000",
    "pydat1 parj 26 0.4000",
    "pydat1 parj 41 0.3000",
    "pydat1 parj 42 0.5800",
    "pydat1 parj 45 0.5000",
    "pydat1 parj 46 1.0000", # AMBT1 value =0.7500
    ]
