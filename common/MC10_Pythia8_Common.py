###############################################################
# job option fragments for year 2010/2011 production with tune 4C
# contact: James Monk
###############################################################

from Pythia8_i.Pythia8_iConf import Pythia8_i
Pythia8 = Pythia8_i()
topAlg += Pythia8
Pythia8.Commands += ["Tune:pp=5"]
Pythia8.Commands += [ "Main:timesAllowErrors = 500"]
Pythia8.Commands += ["6:m0 = 172.5"]
Pythia8.Commands += ["23:m0 = 91.1876"]
Pythia8.Commands += ["23:mWidth = 2.4952"]
Pythia8.Commands += ["24:m0 = 80.403"]
Pythia8.Commands += ["24:mWidth = 2.141"]
Pythia8.Commands += ["ParticleDecays:limitTau0 = on"]  # set K_S, Lambda stable
