###############################################################
# Common job option fragments for Herwig++ 
# contact: Andy Buckley, Claire Gwenlan
###############################################################

from Herwigpp_i.Herwigpp_iConf import Herwigpp
topAlg += Herwigpp()

## Get basic Herwig++ Atlas tune params
from Herwigpp_i import config as hw
cmds = hw.energy_cmds(runArgs.ecmEnergy) + hw.base_cmds() + hw.lo_pdf_cmds() + hw.ue_tune_cmds("LO**-LHC-MU900-2")


