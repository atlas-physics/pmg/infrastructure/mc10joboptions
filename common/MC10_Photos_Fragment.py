# ... Photos

from Photos_i.Photos_iConf import Photos
topAlg += Photos()
Photos = topAlg.Photos
Photos.PhotosCommand = [        "photos pmode 1",
                                "photos xphcut 0.01",
                                "photos alpha -1.",
                                "photos interf 1",
                                "photos isec 1",
                                "photos itre 0",
                                "photos iexp 1",
                                "photos iftop 0"]

from MC10JobOptions.PhotosEvgenConfig import evgenConfig
