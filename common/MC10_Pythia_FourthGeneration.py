###############################################################
# Pythia settings for including fourth-generation particles
# in simulated events.
#
# $Id: $
#
# Authors:
#  Tobias Golling, Yale University
#  Rocco Mandrysch, Humboldt University of Berlin
#  Michael G Wilson, SLAC National Accelerator Laboratory
#==============================================================

include( "MC10JobOptions/MC10_Pythia_Common.py" )

Pythia.PythiaCommand += [

    "pypars mstp 1 4", # MSTP(1): Maximum number of generations

    ## The four-generation CKM mixing matrix needs to be set even
    ## if specific channels are not simulated because the width
    ## of a particle depends on its branching fractions.
    ##
    ## VCKM(I,J): Squared matrix elements of the CKM flavour mixing
    ##            matrix.
    ##   I: up-type generation index, i.e., 1 = u, 2 = c, 3 = t, 4 = t'
    ##   J: down-type generation index, i.e., 1 = d, 2 = s, 3 = b, 4 = b'
    ##
    ## The values provided here are from work done within the CKMfitter
    ## group: they are the central values when fitting to all
    ## tree-level decay constraints, epsilon_K, B(d,s) oscillations,
    ## and the angle gamma in the unitarity triangle.  See the talk
    ## by Heiko Lacker, "Constraints on CKM matrix elements with a
    ## fourth generation", given at the Second Workshop on Beyond
    ## 3 Generation Standard Model, in Taipeii, Taiwan, 14-16 January 2010:
    ##
    ##   http://indico.cern.ch/sessionDisplay.py?sessionId=3&confId=68036
    ##
    ## (This talk gives limits; the central values below were obtained
    ## directly from Heiko Lacker and Andreas Menzel.  In particular,
    ## the central values satisfy unitarity.)
    ##
    ## Remeber that the values below are the _elements squared_.

    "pydat2 vckm 1 1 0.94901",   # |V_ud| = 0.97417
    "pydat2 vckm 1 2 0.050760",  # |V_us| = 0.22530
    "pydat2 vckm 1 3 0.0000116", # |V_ub| = 0.00341
    "pydat2 vckm 1 4 0.0002131", # |V_u4| = 0.01460
    "pydat2 vckm 2 1 0.050369",  # |V_cd| = 0.22443
    "pydat2 vckm 2 2 0.94616",   # |V_cs| = 0.97271
    "pydat2 vckm 2 3 0.001695",  # |V_cb| = 0.04117
    "pydat2 vckm 2 4 0.001764",  # |V_c4| = 0.04200
    "pydat2 vckm 3 1 0.0000412", # |V_td| = 0.00642
    "pydat2 vckm 3 2 0.001421",  # |V_ts| = 0.03769
    "pydat2 vckm 3 3 0.98829",   # |V_tb| = 0.99413
    "pydat2 vckm 3 4 0.010252",  # |V_t4| = 0.10125
    "pydat2 vckm 4 1 0.0005726", # |V_4d| = 0.02393
    "pydat2 vckm 4 2 0.001650",  # |V_4s| = 0.04062
    "pydat2 vckm 4 3 0.010006",  # |V_4b| = 0.10003
    "pydat2 vckm 4 4 0.98778",   # |V_44| = 0.99387

    ## Switch all fourth-generation channels from "not existing" (-1)
    ## to "on" (1).
    ##
    ## MDME(IDC,1): on/off switch for individual decay channel IDC...
    ## Warning: the two values -1 and 0 ... are quite different.
    ## In neither case is the channel generated, but in the later case,
    ## the channel still contributes to the total width of a resonance,
    ## and thus affects both the simulated lineshape and the generated
    ## cross section.

    "pydat3 mdme   7 1  1",  # d --> W- t'
    "pydat3 mdme  15 1  1",  # u --> W+ b'
    "pydat3 mdme  23 1  1",  # s --> W- t'
    "pydat3 mdme  31 1  1",  # c --> W+ b'
    "pydat3 mdme  39 1  1",  # b --> W- t'
    "pydat3 mdme  47 1  1",  # t --> W+ b'
    "pydat3 mdme 180 1  1",  # Z0 --> b' b'bar
    "pydat3 mdme 181 1  1",  # Z0 --> t' t'bar
    "pydat3 mdme 188 1  1",  # Z0 --> tau'- tau'+
    "pydat3 mdme 189 1  1",  # Z0 --> nu'_tau nu'_taubar
    "pydat3 mdme 193 1  1",  # W+ --> dbar t'
    "pydat3 mdme 197 1  1",  # W+ --> sbar t'
    "pydat3 mdme 201 1  1",  # W+ --> bbar t'
    "pydat3 mdme 202 1  1",  # W+ --> b'bar u
    "pydat3 mdme 203 1  1",  # W+ --> b'bar c
    "pydat3 mdme 204 1  1",  # W+ --> b'bar t
    "pydat3 mdme 205 1  1",  # W+ --> b'bar t'
    "pydat3 mdme 209 1  1",  # W+ --> tau'+ nu'_tau
    "pydat3 mdme 216 1  1",  # h0 --> b' b'bar
    "pydat3 mdme 217 1  1",  # h0 --> t' t'bar    
    "pydat3 mdme 221 1  1"  # h0 --> tau'- tau'+
    ]
