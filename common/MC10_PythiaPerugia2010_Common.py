#######################################################################################
#
# Job options file for Perugia2010  
# author: C. Gwenlan 
# reference Perugia2010 arXiv:1005.3457v1 [hep-ph]
#=====================================================================================

from Pythia_i.Pythia_iConf import Pythia
topAlg += Pythia()
Pythia = topAlg.Pythia

# PYTUNE 327: Perugia2010
Pythia.Tune_Name="PYTUNE_327"


Pythia.PythiaCommand += [
    # initializations
    "pyinit pylisti 12",
    "pyinit pylistf 1",
    "pystat 1 3 4 5",
    "pyinit dumpr 1 5",
    # mass
    "pydat2 pmas 6 1 172.5",    # PDG2007 TOP mass 
    "pydat2 pmas 24 1 80.403",  # PDG2007 W mass
    "pydat2 pmas 23 1 91.1876", # PDG2007 Z0 mass
    # settings below have no effect (widths calculated perturbatively in Pythia)
    #"pydat2 pmas 24 2 2.141",   # PDG2007 W width
    #"pydat2 pmas 23 2 2.4952",  # PDG2007 Z0 width
    ]


