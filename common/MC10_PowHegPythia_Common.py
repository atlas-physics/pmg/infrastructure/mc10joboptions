###############################################################
# job option fragments for year 2010 production
# U.E. tuning parameters are for POWHEG with PYTHIA 6.421
# NB. - Pythia not tuned with CTEQ66 -> 
#       according to Paolo Nason, using NLO PDFs for ME and LO for PS/UE OK
#       -> use LO* for PS/UE, with AMBT1 tune (for MC10)
#     - **** Reading external Les Houches event file sets mstp(51) to 10042
#       to change proton pdf mstp(51), see 
#       https://twiki.cern.ch/twiki/bin/view/AtlasProtected/PowhegForATLAS
# contact: Ulrich Husemann, Christoph Wasicki (Feb. 2010)
###############################################################

from Pythia_i.Pythia_iConf import Pythia
topAlg += Pythia()
Pythia = topAlg.Pythia

# use AMBT1 tune
Pythia.Tune_Name="ATLAS_20100001"

## use MC09' tune (for CTEQ6L1) --- used for MC09 production
# Pythia.Tune_Name="ATLAS_20090002"

Pythia.PythiaCommand = [
    # initializations
    "pyinit user lhef", # read external Les Houches event file
    "pyinit pylisti 12",
    "pyinit pylistf 1",
    "pystat 1 3 4 5",
    "pyinit dumpr 1 5",
    # mass
    "pydat2 pmas 6 1 172.5",    # PDG2007 TOP mass 
    "pydat2 pmas 24 1 80.403",  # PDG2007 W mass
    "pydat2 pmas 23 1 91.1876", # PDG2007 Z0 mass
    # settings below have no effect (widths calculated perturbatively in Pythia)
    #"pydat2 pmas 24 2 2.141",   # PDG2007 W width
    #"pydat2 pmas 23 2 2.4952",  # PDG2007 Z0 width

    ]

