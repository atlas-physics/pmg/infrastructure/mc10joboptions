###############################################################
# job option fragments for year 2010 production
# U.E. tuning parameters for PYTHIA 6.423 
# use PythiaExo_i/src/atlasTuneResMod.cxx
# contact: Claire Gwenlan, Liza Mijovic
###############################################################

from PythiaExo_i.PythiaResMod_iConf import PythiaResMod
topAlg += PythiaResMod()
PythiaResMod = topAlg.PythiaResMod

# use AMBT1 tune
PythiaResMod.Tune_Name="ATLAS_20100001"

# Can alternatively set AMBT1 using PYTUNE from Pythia 6.423
# PythiaResMod.Tune_Name="PYTUNE_340"

PythiaResMod.PythiaCommand = [
    # initializations
    "pyinit pylisti 12",
    "pyinit pylistf 1",
    "pystat 1 3 4 5",
    "pyinit dumpr 1 5",
    # mass
    "pydat2 pmas 6 1 172.5",    # PDG2007 TOP mass 
    "pydat2 pmas 24 1 80.403",  # PDG2007 W mass
    "pydat2 pmas 23 1 91.1876", # PDG2007 Z0 mass
    # the settings below have no effect (widths calculated perturbatively in Pythia)
    #"pydat2 pmas 24 2 2.141",   # PDG2007 W width
    #"pydat2 pmas 23 2 2.4952",  # PDG2007 Z0 width
    ]

