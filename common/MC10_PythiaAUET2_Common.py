###############################################################
# job option fragments for year 2010 production
# U.E. tuning parameters for PYTHIA 6.423 
# use Pythia_i/src/atlasTune.cxx
# contact: Claire Gwenlan
###############################################################

from Pythia_i.Pythia_iConf import Pythia
topAlg += Pythia()
Pythia = topAlg.Pythia

# set up AMBT1 tune
Pythia.Tune_Name="ATLAS_20100001"


Pythia.PythiaCommand += [
    # initializations
    "pyinit pylisti 12",
    "pyinit pylistf 1",
    "pystat 1 3 4 5",
    "pyinit dumpr 1 5",
    # mass
    "pydat2 pmas 6 1 172.5",    # PDG2007 TOP mass 
    "pydat2 pmas 24 1 80.403",  # PDG2007 W mass
    "pydat2 pmas 23 1 91.1876", # PDG2007 Z0 mass
    # the settings below have no effect (widths calculated perturbatively in Pythia)
    #"pydat2 pmas 24 2 2.141",   # PDG2007 W width
    #"pydat2 pmas 23 2 2.4952",  # PDG2007 Z0 width
    ]

# reset parameters for Pythia AUET2 
Pythia.PythiaCommand += [

    # Switch to LO** PDF
    "pypars mstp 51 20651",
    "pypars mstp 53 20651",  # not needed; just adding so LO** printed, rather than LO* in log file
    "pypars mstp 55 20651",  # not needed; just adding so LO** printed, rather than LO* in log file
    "pypars mstp 52 2",
    "pypars mstp 54 2",
    "pypars mstp 56 2",
    
    # Switch on Bowler:
    "pydat1 mstj 11 5",

    # Flavour parameters
    "pydat1 parj 1  7.272809e-02",
    "pydat1 parj 2  2.018845e-01",
    "pydat1 parj 3  9.498471e-01",
    "pydat1 parj 4  3.316182e-02",
    "pydat1 parj 11 3.089764e-01",
    "pydat1 parj 12 4.015396e-01",
    "pydat1 parj 13 5.442874e-01",
    "pydat1 parj 25 6.276964e-01",
    "pydat1 parj 26 1.292377e-01",

    # Fragmentation parameters
    "pydat1 parj 21 3.001463e-01",
    "pydat1 parj 41 3.683123e-01",
    "pydat1 parj 42 1.003531e+00",
    "pydat1 parj 47 8.727703e-01",
    "pydat1 parj 81 2.564716e-01",
    "pydat1 parj 82 8.296215e-01",

    # Different Lambda_QCD for different bits of the shower:
    # Lambda is given by PARP(1) for hard interactions, by PARP(61) for space-like
    # showers, by PARP(72) for time-like showers not from a resonance decay,
    # and by PARJ(81) for time-like ones from a resonance decay
    "pypars mstp 3 1",

    # Set some initial state Lambdas (to the PDF values of MRST LO** in this case)
    "pydat1 paru 112 0.265",
    "pypars parp 1   0.265",
    "pypars parp 61  0.265",

    # Set the nominal number of flavours assumed in the alpha_s expression, with
    # respect to which Lambda_QCD is defined (default is 5)
    "pydat1 mstu 112 4",

    # Make ISR Lambda_QCD tunable
    "pypars mstp 70 0",

    # alpha_s scheme for FSR (see Pythia update notes)
    "pypars mstp 64 3",

    # ISR ...
    "pypars mstp 72 2",

    # ISR parameters 
    "pypars parp 62  2.803673e+00",
    "pypars parp 64  2.213751e+00",
    "pypars parp 67  6.635356e-01",
    "pypars parp 72  2.496789e-01",
    "pypars parp 91  1.918441e+00",

    # MPI parameters
    "pypars parp 77  1.127437e+00",
    "pypars parp 78  3.320624e-01",
    "pypars parp 82  2.451288e+00",
    "pypars parp 84  5.296569e-01",
    "pypars parp 90  2.290732e-01"
    
    ]



