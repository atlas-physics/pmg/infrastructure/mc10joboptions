###############################################################
#
# Job options file for dijet production with pythia
#
# author: C. Gwenlan (June'11)
#
# reference Perugia2011 parameters as in  arXiv:1005.3457 v4 [hep-ph]
# Perugia2011 (PYTUNE 350; from 6.425 onwards)
#
# reference JO: MC10.105014.J5.pythia_jetjet.py
#===============================================================

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_PythiaPerugia2011_Common.py" )

Pythia.PythiaCommand += [
			      "pysubs msel 0",
			      "pysubs ckin 3 280.",
#			      "pysubs ckin 4 560.",
			      "pysubs msub 11 1",
			      "pysubs msub 12 1",
			      "pysubs msub 13 1",
			      "pysubs msub 68 1",
			      "pysubs msub 28 1",
			      "pysubs msub 53 1"]

     
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
