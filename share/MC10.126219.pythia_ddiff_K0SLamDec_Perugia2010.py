# Double diffractive sample
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_PythiaPerugia2010_Common.py" )

Pythia.PythiaCommand += [ "pysubs msel 0",
                         "pysubs msub 94 1" ]

# K0S, Lambda to decay
Pythia.PythiaCommand += [ "pydat1 mstj 22 1" ]

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9

#==============================================================
#
# End of job options file
#
###############################################################
