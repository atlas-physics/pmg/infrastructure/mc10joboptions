###############################################################
#
# Job options file
#
# Pythia J5 event generation with pT-deltaY weighting.
# cross section as default pythia sample = 2.35 nb
#
# Prepared by Graham Jones, Graham.Jones [at] cern.ch 
#==============================================================


# dijet production with pythia
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

Pythia.PythiaCommand += [
			      "pysubs msel 0",
			      "pysubs ckin 3 280.",
			      "pysubs ckin 4 560.",
			      "pysubs msub 11 1",
			      "pysubs msub 12 1",
			      "pysubs msub 13 1",
			      "pysubs msub 68 1",
			      "pysubs msub 28 1",
			      "pysubs msub 53 1"]

#--------------------------------------------------------------
# Truth Jets
#--------------------------------------------------------------

# Make truth jets for JetForwardFilter:
from AthenaCommon.AppMgr import ServiceMgr as svcMgr
include( "AthenaCommon/Atlas_Gen.UnixStandardJob.py" )
from JetRec.JetGetters import *
Kt6Alg = make_StandardJetGetter('AntiKt',0.6,'Truth').jetAlgorithmHandle()
Kt6Alg.AlgTools['JetFinalPtCut'].MinimumSignal = 7.0*GeV

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

## Truth filter
# Add the filter to the correct sequence!
from GeneratorFilters.GeneratorFiltersConf import JetForwardFilter
forwardFilter=JetForwardFilter()
topAlg += forwardFilter

# General properties
forwardFilter.JetContainer = "AntiKt6TruthJets"

# Kinematic cuts
forwardFilter.MinDeltaY = 0.0
forwardFilter.MaxDeltaY = 100.0
forwardFilter.MinPt1 = 12.0 #GeV
forwardFilter.MinPt2 = 12.0
forwardFilter.MaxPt1 = 70000.0
forwardFilter.MaxPt2 = 70000.0

#Gaussian mixture model describing pdf of unweighted sample
forwardFilter.muXs = [ 308.461671267, 458.793615796, 311.232220747, 374.402666072, 338.986256082  ]
forwardFilter.muYs = [ 1.85127578192, 0.926590108993, 1.00280472952, 1.20822909987, 0.865010150568  ]
forwardFilter.sigmaXs = [ 32.573160645, 78.8867349373, 25.0254926669, 76.6497378518, 50.4892907648  ]
forwardFilter.sigmaYs = [ 0.978953598344, 0.644222489657, 0.6599688185, 0.806506086674, 0.581966849896  ]
forwardFilter.rhos = [ 0.0439088233919, 0.00398010746322, -0.0169812240678, 0.0587563328364, 0.0294175746488  ]
forwardFilter.weights = [ 0.180307539497, 0.0656004061124, 0.280381101604, 0.201411426856, 0.272299525932  ]

#Target probability (how much weighting required roughly)
#Lower = more suppression
import math
forwardFilter.SuppressionFactor = 5.0*math.pow(10.0, -8.0)

#--------------------------------------------------------------
# Apply filtering
#--------------------------------------------------------------

try:
  StreamEVGEN.RequireAlgs += [ "JetForwardFilter" ]
except Exception, e:
  pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
# N_gen = 232955523; N_pass = 100000 -> eff = 4.2927*10^-4
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.00042927
evgenConfig.minevents=1000 #~12 hours
evgenConfig.weighting=0
