###############################################################
#
# TopReX jobOptions file for MC10 prepared by
# Filipe Veloso (filipe.veloso@cern.ch)
#
# t \bar t -> b W q Z (W -> l nu, Z -> tau tau, l = e, mu, tau)
#
#==============================================================

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

# Systematics
Pythia.PythiaCommand += [ "pypars mstp 3 1",      #set mstp 3 1 (setting Lambda in alpha_s)
                          "pypars parp 1 0.192",  #reset the defaults after mstp 3 1
                          "pypars parp 72 0.384", #FSR Labmda value in running alpha_s (ATLAS def 0.192)
                          "pydat1 paru 112 0.192",#reset defaults after mstp 3 1
                          "pydat1 mstu 112 4",    #reset defaults after mstp 3 1
                          "pypars parp 61 0.192"] #reset defaults after mstp 3 1
Pythia.PythiaCommand += [ "pydat1 parj 82 0.5" ]   

# Pythia options
Pythia.PythiaCommand += ["pyinit user toprex"]      # Use TopReX
Pythia.PythiaCommand += ["pysubs ckin 3 10."]       # allowed Pt values for 2->2: Pt>CKIN(3) GeV/c

# Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )
Pythia.PythiaCommand += ["pydat3 mdcy 15 1 0"]      # Turn off tau decays.

# Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )
Pythia.PythiaCommand += ["pydat1 parj 90 20000"]    # Turn off FSR.

# Reads ECM
ecm=7000
try:
     ecm=runArgs.ecmEnergy
except NameError:
     print "Check ECM in TopReX log"

# Creates the TopReX input file
toprexFile = """*           Input parameters for TopRex
* totally blank card or card with a first non-blank symbol '*' is ignored
* first non-blank item is a user-defined variable. This card has the form:
*   "variable's name" "=" "variable's value"   "comment (not needed)"
* the card without '=' character will be readed but not recognized

* process ttbar production
 iproc = 20 !      ! no. of TopRex process
 ecm = %d ! CMS energy in GeV

*...top decay channels
 MID(1,1) = 2
 MID(2,1) = 2
 MID(3,1) = 2
 MID(5,1) = -1
 MID(6,1) = -1
 MID(11,1) = -1
 MID(12,1) = -1
 MID(13,1) = -1
 MID(14,1) = -1
 MID(15,1) = 3
 MID(16,1) = 3

*...FCNC couplings
Gfcnc(1,1) = 0.1            ! t g U
Gfcnc(1,2) = 0.1            ! t g C
Gfcnc(1,3) = 0.1            ! t A U
Gfcnc(1,4) = 0.1            ! t A C
Gfcnc(1,5) = 0.1            ! t Z U
Gfcnc(1,6) = 0.1            ! t Z C
Gfcnc(2,1) = 0.7071067812   ! t g U
Gfcnc(2,2) = 0.7071067812   ! t g C
Gfcnc(2,3) = 0.7071067812   ! t A U
Gfcnc(2,4) = 0.7071067812   ! t A C
Gfcnc(2,5) = 0.7071067812   ! t Z U
Gfcnc(2,6) = 0.7071067812   ! t Z C
""" % ecm
f = open('toprex.dat','w')
f.write(toprexFile)
f.close()

# W decay channels: W->lnu (e,mu,tau)
Pythia.PythiaCommand += ["pydat3 mdme 190 1 0", "pydat3 mdme 191 1 0", "pydat3 mdme 192 1 0"]
Pythia.PythiaCommand += ["pydat3 mdme 193 1 0", "pydat3 mdme 194 1 0", "pydat3 mdme 195 1 0"]
Pythia.PythiaCommand += ["pydat3 mdme 196 1 0", "pydat3 mdme 197 1 0", "pydat3 mdme 198 1 0"]
Pythia.PythiaCommand += ["pydat3 mdme 199 1 0", "pydat3 mdme 200 1 0", "pydat3 mdme 201 1 0"]
Pythia.PythiaCommand += ["pydat3 mdme 202 1 0", "pydat3 mdme 203 1 0", "pydat3 mdme 204 1 0"]
Pythia.PythiaCommand += ["pydat3 mdme 205 1 0", "pydat3 mdme 206 1 1", "pydat3 mdme 207 1 1"]
Pythia.PythiaCommand += ["pydat3 mdme 208 1 1", "pydat3 mdme 209 1 0"]
# Z decay channels: Z->tau tau
Pythia.PythiaCommand += ["pydat3 mdme 174 1 0", "pydat3 mdme 175 1 0", "pydat3 mdme 176 1 0"]
Pythia.PythiaCommand += ["pydat3 mdme 177 1 0", "pydat3 mdme 178 1 0", "pydat3 mdme 179 1 0"]
Pythia.PythiaCommand += ["pydat3 mdme 182 1 0", "pydat3 mdme 183 1 0", "pydat3 mdme 184 1 0"]
Pythia.PythiaCommand += ["pydat3 mdme 185 1 0", "pydat3 mdme 186 1 1", "pydat3 mdme 187 1 0"]

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9

#==============================================================
#
# End of job options file
#
###############################################################
