from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ("MC10JobOptions/MC10_Pythia8_Common.py")

Pythia8.Commands += ['SoftQCD:all = off',
                     'HardQCD:all = off',
                     '25:m0 = 115',
                     'HiggsSM:ffbar2HZ = on',
                     '25:onMode = off',
                     '25:onIfMatch = 5 5',
                     '23:onMode = off',
                     '23:onIfMatch = 11 11',
                     '23:onIfMatch = 13 13',
                     '23:onIfMatch = 15 15']

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.Pythia8EvgenConfig import evgenConfig
#evgenConfig.efficiency = 0.9
#evgenConfig.minevents=100  

#==============================================================
#
# End of job options file
#
############################################################### 
