# Double diffractive sample
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

# DW tune to Tevatron data (old shower/MI) [R. Field, hep-ph/0610012]
include ( "MC10JobOptions/MC10_PythiaDW_Common.py" )

Pythia.PythiaCommand += [ "pysubs msel 0",
                         "pysubs msub 94 1" ]


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9

#==============================================================
#
# End of job options file
#
###############################################################
