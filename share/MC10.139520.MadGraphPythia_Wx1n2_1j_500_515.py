##############################################################
#
# Job options file
#
#==============================================================
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

# --> use the following pythia tune if CTEQ6l1 pdf has been used
include ( "MC10JobOptions/MC10_PythiaMC09p_Common.py" )

Pythia.PythiaCommand +=  [
    "pyinit user madgraph",
    "pystat 1 3 4 5",
    "pyinit dumpr 1 5", # printout of first 5 events
    "pyinit pylistf 1",
    "pydat1 mstj 1 1", # string fragmentation on
    "pypars mstp 61 1", # initial-state radiation on
    "pypars mstp 71 1", # final-state radiation on
    "pypars mstp 81 1", # Multiple interactions on
    "pypars mstp 128 1", # fix junk output for documentary particles
    "pydat1 mstu 21 1" # prevent Pythia from exiting when it exceeds its errors limit
    ]

# ... TAUOLA and Photos
Pythia.PythiaCommand += ["pydat1 parj 90 20000", # restrict lep QED rad.
                         "pydat3 mdcy 15 1 0" ] # Turn off tau decays.
## ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )
## ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

from MC10JobOptions.MadGraphEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'group.phys-gener.madgraph.139520.Wx1n2_1j_500_515.TXT.mc10_v1'
#evgenConfig.efficiency = 1.000
#==============================================================
#
# End of job options file
#
###############################################################
phojf=open('./pythia_card.dat', 'w')
phojinp = """
! exclusive or inclusive matching
      MSTP(61)=1
      MSTP(71)=1
      MSTJ(1)=1
      IMSS(22)= 24
      IEXCFILE=0
      showerkt=T
      qcut=20
      """
phojf.write(phojinp)
phojf.close()
