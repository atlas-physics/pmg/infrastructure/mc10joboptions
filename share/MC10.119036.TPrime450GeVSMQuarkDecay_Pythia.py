#==============================================================
#
# Job options fragment to produce t't'bar events with Pythia.
# Decay Mode: all
# clement.helsens@cern.ch: 10/15/2010
#
#==============================================================
 
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

Pythia.PythiaCommand += ["pydat1 parj 90 20000",   # Turn off lepton radiation.
                         "pydat3 mdcy 15 1 0"]     # Turn off tau decays.

Pythia.PythiaCommand += [
#            "pysubs msel 6",          # Heavy flavor production (ttbar)
             "pysubs msel 8",          # Heavy Quark production (tprimetprimebar)
#            "pysubs ckin 3 18.",      # ptmin for hard-scattering process
#            "pypars mstp 43 2",       # only Z0, not gamma*

             # W decays (all decays)
             "pydat3 mdme 190 1 1", #W->qq
             "pydat3 mdme 191 1 1", #W->qq
             "pydat3 mdme 192 1 1", #W->qq
             "pydat3 mdme 194 1 1", #W->qq
             "pydat3 mdme 195 1 1", #W->qq
             "pydat3 mdme 196 1 1", #W->qq
             "pydat3 mdme 198 1 1", #W->qq
             "pydat3 mdme 199 1 1", #W->qq
             "pydat3 mdme 200 1 1", #W->qq
             "pydat3 mdme 206 1 1", #W->e,nu_e
             "pydat3 mdme 207 1 1", #W->mu,nu_mu
             "pydat3 mdme 208 1 1", #W->tau,nu_tau

            # tprime production taken from D0

            # VCKM is CKM matrix elements square
            "pydat2 vckm 4 1 0.577350269",
            "pydat2 vckm 4 2 0.577350269",
            "pydat2 vckm 4 3 0.577350269",
            "pydat2 vckm 4 4 0.0",

            # Branching Ratios
            "pydat3 brat 69 0.333333333333", #set the branching ration to 0 for t'->d+W
            "pydat3 brat 70 0.333333333333", #set the branching ration to 0 for t'->s+W
            "pydat3 brat 71 0.333333333333", #set the branching ration to 1 for t'->b+W
            "pydat3 brat 72 0.0", #set the branching ration to 1 for t'->b'+W

            # Set the maximum number of generation to 4
             "pypars mstp 1 4",   

            # Set Heavy Quarks masses and width
            "pydat2 pmas 7 1 400.0", #set b' mass such that m_b' > m_t'-m_W
            "pydat2 pmas 7 2 10.0", #set b' width
            "pydat2 pmas 8 1 450.0", #set t' mass
            "pydat2 pmas 8 2 10.0", #set t' width

            # CG -- PYINT4 common block not currently explicitly supported by Pythia_i
            # Set parameters via PYGIVE directly for the moment (below)
            #
            ## control mass/width calculation
            ##"pyint4 mwid 8 0", # 0 => don't treat particle as resonance; (fails due to violation of charge conservation)
            #"pyint4 mwid 8 2", # 0 => use pmas(8,2) as total width
            ##"pyint4 mwid 7 0", # 0 => don't treat particle as resonance; (fails due to violation of charge conservation)
            #"pyint4 mwid 7 2", # 0 => use pmas(7,2) as total width (inoperant since b's are not produced)

            # Set Top Quark decays
            "pydat3 mdme 41 1 -1", # g t    -1 means not allowed
            "pydat3 mdme 42 1 -1", # gamma t
            "pydat3 mdme 43 1 -1", # Z0 t
            "pydat3 mdme 44 1 1", # W+ d
            "pydat3 mdme 45 1 1", # W+ s
            "pydat3 mdme 46 1 1", # W+ b

            # Set Heavy Quark decays
            "pydat3 mdme 66 1 -1", # g t'    -1 means not allowed
            "pydat3 mdme 67 1 -1", # gamma t'
            "pydat3 mdme 68 1 -1", # Z0 t'
            "pydat3 mdme 69 1 1", # W+ d
            "pydat3 mdme 70 1 1", # W+ s
            "pydat3 mdme 71 1 1", # W+ b
            "pydat3 mdme 72 1 -1", # W+ b'
            "pydat3 mdme 73 1 -1", # h0  t'
            "pydat3 mdme 74 1 -1", # H+ b
            "pydat3 mdme 75 1 -1", # H+ b'

            # Set Heavy Quark decays
            "pydat3 mdme 47 1 1", # W+ b'    set to 1 but not allowed because m_b' > m_t'-m_W
            "pydat3 mdme 48 1 -1", # h0 t
            "pydat3 mdme 49 1 -1", # H+ b
            "pydat3 mdme 50 1 -1", # SUSY
            "pydat3 mdme 51 1 -1", # SUSY
            "pydat3 mdme 52 1 -1", # SUSY
            "pydat3 mdme 53 1 -1", # SUSY
            "pydat3 mdme 54 1 -1", # SUSY
            "pydat3 mdme 55 1 -1", # SUSY

           ]

Pythia.PygiveCommand += [
                         # control mass/width calculation
                         # "mwid(8)=0",   # 0 => don't treat particle as resonance; (fails due to violation of charge conservation)
                          "mwid(8)=2",   # 0 => use pmas(8,2) as total width
                         # "mwid(7)=0",   # 0 => don't treat particle as resonance; (fails due to violation of charge conservation)
                          "mwid(7)=2"   # 0 => use pmas(7,2) as total width (inoperant since b's are not produced)
                         ]



# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.95
# evgenConfig.minevents = 10
# evgenConfig.maxeventsfactor = 1000.
