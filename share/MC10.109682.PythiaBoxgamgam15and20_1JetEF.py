###############################################################
#
# Job options file
#
# non-resonant production of gamma gamma with Pythia
# !!! only BOX !!!
# (based on MC10.105964.Pythiagamgam15.py)
#
# 21 Oct 2010: for 7TeV, we use AntiKt04. (so far we used Cone04.)
#
# Responsible person(s)
#   21 May, 2009-xx xxx, 20xx: Junichi Tanaka (Junichi.Tanaka@cern.ch)
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )
Pythia.PythiaCommand += [ "pysubs msel 0",
                          "pysubs msub 114 1",
                          "pysubs ckin 3 10.",
                          "pydat1 parj 90 20000." ]

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

# TruthJet filter
try:
     from JetRec.JetGetters import *
     c4=make_StandardJetGetter('AntiKt',0.4,'Truth')
     c4alg = c4.jetAlgorithmHandle()
     c4alg.JetFinalEtCut.MinimumSignal = 10.*GeV
except Exception, e:
     pass
                         
from GeneratorFilters.GeneratorFiltersConf import PhotonFilter
topAlg += PhotonFilter("Photon15EF")
topAlg += PhotonFilter("Photon20EF")

Photon15EF = topAlg.Photon15EF
Photon15EF.Ptcut = 15000.
Photon15EF.Etacut = 2.7
Photon15EF.NPhotons = 2

Photon20EF = topAlg.Photon20EF
Photon20EF.Ptcut = 20000.
Photon20EF.Etacut = 2.7
Photon20EF.NPhotons = 1

from GeneratorFilters.GeneratorFiltersConf import VBFForwardJetsFilter
topAlg += VBFForwardJetsFilter()

# 1 jet EF with overlap removal
VBFForwardJetsFilter = topAlg.VBFForwardJetsFilter
VBFForwardJetsFilter.JetMinPt=15.*GeV
VBFForwardJetsFilter.JetMaxEta=5.0
VBFForwardJetsFilter.NJets=1
VBFForwardJetsFilter.Jet1MinPt=-1.*GeV
VBFForwardJetsFilter.Jet1MaxEta=5.0
VBFForwardJetsFilter.Jet2MinPt=-1.*GeV
VBFForwardJetsFilter.Jet2MaxEta=5.0
VBFForwardJetsFilter.UseOppositeSignEtaJet1Jet2=False
VBFForwardJetsFilter.MassJJ=-1.*GeV
VBFForwardJetsFilter.DeltaEtaJJ=-1.0
VBFForwardJetsFilter.TruthJetContainer="AntiKt4TruthJets"
VBFForwardJetsFilter.LGMinPt=15.*GeV
VBFForwardJetsFilter.LGMaxEta=2.5
VBFForwardJetsFilter.DeltaRJLG=0.05
VBFForwardJetsFilter.RatioPtJLG=0.3

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------

try:
     StreamEVGEN.RequireAlgs +=  [ "Photon15EF", "Photon20EF", "VBFForwardJetsFilter" ]
except Exception, e:
     pass
               
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.02655*0.9
# 5000/188342=0.02655
