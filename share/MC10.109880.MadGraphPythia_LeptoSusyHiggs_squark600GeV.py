#--------------------------------------------------------------
# GMSB Higgs with MadGraph/Pythia
#--------------------------------------------------------------
# Simona Rolli <simonarolli@fnal.gov> Feb, 2010
#--------------------------------------------------------------
###############################################################
#
# Job options file
#
#==============================================================
# Central Production set up
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")
#topSequence = AlgSequence()
#theApp.EvtMax=20000

include ( "MC10JobOptions/MC10_PythiaMC09p_Common.py" )
Pythia.PythiaCommand += ["pyinit user madgraph"]

Pythia.PythiaCommand +=  [
    "pydat3 mdcy 307 1 0",
    "pydat3 mdcy 323 1 0",
    "pydat3 mdcy 325 1 0"]
#    "pydat3 mdcy 327 1 0"]

Pythia.PythiaCommand += ["pydat3 mdcy 15 1 0"] # Turn off tau decays.
Pythia.PythiaCommand += ["pydat1 parj 90 20000."] # for Photos

# ... TAUOLA
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )
# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.MadGraphEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'group09.phys-gener.madgraph.109880.LeptoSusyHiggs_squark600GeV.TXT.v1'
evgenConfig.efficiency = 0.9
#==============================================================
#
# End of job options file
#
###############################################################
