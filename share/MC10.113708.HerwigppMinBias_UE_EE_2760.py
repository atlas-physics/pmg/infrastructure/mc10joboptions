## Job options file for Herwig++, min bias production
## Energy-dependent UE tune (2.76 TeV)
## http://projects.hepforge.org/herwig/trac/wiki/MB_UE_tunes
## Claire Gwenlan, April '11

## Get a handle on the top level algorithms' sequence
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

try:
     include("MC10JobOptions/MC10_Herwigpp_Common.py")
except NameError:
     ## TODO: This fallback is a bad idea...
     # from Herwigpp_i.Herwigpp_iConf import Herwigpp
     # topAlg += Herwigpp()
     cmds =  ""

## Add to commands
cmds += """
## Set up min bias process
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEMinBias
set /Herwig/Cuts/JetKtCut:MinKT 0.0*GeV
set /Herwig/Cuts/QCDCuts:MHatMin 0.0*GeV
set /Herwig/Cuts/QCDCuts:X1Min 0.01
set /Herwig/Cuts/QCDCuts:X2Min 0.01

# LHC-UE-EE-2760-1.in

# Colour reconnection settings
set /Herwig/Hadronization/ColourReconnector:ColourReconnection Yes
set /Herwig/Hadronization/ColourReconnector:ReconnectionProbability 0.55 

# Colour Disrupt settings
set /Herwig/Partons/RemnantDecayer:colourDisrupt 0.15

# inverse hadron radius
set /Herwig/UnderlyingEvent/MPIHandler:InvRadius 1.1*GeV2
# Min KT parameter
set /Herwig/UnderlyingEvent/KtCut:MinKT 3.31*GeV
# This should always be 2*MinKT!!
set  /Herwig/UnderlyingEvent/UECuts:MHatMin 6.62*GeV

set /Herwig/UnderlyingEvent/MPIHandler:IdenticalToUE 0
set /Herwig/UnderlyingEvent/MPIHandler:softInt Yes
set /Herwig/UnderlyingEvent/MPIHandler:twoComp Yes
set /Herwig/UnderlyingEvent/MPIHandler:DLmode 3

## Disable diffractive PDFs
set /Herwig/Particles/pomeron:PDF /Herwig/Partons/NoPDF
"""

## Set commands
topAlg.Herwigpp.Commands = cmds.splitlines()

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.HerwigppEvgenConfig import evgenConfig
#evgenConfig.efficiency = 0.9
#evgenConfig.minevents=100

#==============================================================
#
# End of job options file
#
###############################################################
