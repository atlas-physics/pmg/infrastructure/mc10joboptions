#
# Job options file
# Wolfgang Mader, February 8 2007
# (Wolfgang.Mader@SLAC.Stanford.EDU)
#
#==============================================================

import AthenaCommon.AtlasUnixGeneratorJob
from AthenaCommon.AppMgr import ServiceMgr as svcMgr

#load relevant libraries
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

from Sherpa_i.Sherpa_iConf import ReadSherpa_i
sherpa = ReadSherpa_i()
sherpa.Files = [ "sherpa.evts" ]

topAlg += sherpa

#--------------------------------------------------------------
# Truth Jets
#--------------------------------------------------------------

#from RecExConfig.RecFlags  import rec
#rec.doTruth = True

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from MC10JobOptions.SherpaEvgenConfig import evgenConfig
 
# inputfilebase
evgenConfig.inputfilebase = 'Sherpa010103.108859.W3jetstotaunu'
evgenConfig.efficiency = 0.95
