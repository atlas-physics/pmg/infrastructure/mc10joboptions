###############################################################
#
# Job options file
#
# non-resonant production of gamma gamma with Pythia
# !!! only BOX !!!
# (based on MC10.105964.Pythiagamgam15.py)
#
# Responsible person(s)
#   21 May, 2009-xx xxx, 20xx: Junichi Tanaka (Junichi.Tanaka@cern.ch)
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )
Pythia.PythiaCommand += [ "pysubs msel 0",
                          "pysubs msub 114 1",
                          "pysubs ckin 3 10.",
                          "pydat1 parj 90 20000." ]

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

from GeneratorFilters.GeneratorFiltersConf import PhotonFilter
topAlg += PhotonFilter()

PhotonFilter = topAlg.PhotonFilter
PhotonFilter.Ptcut = 15000.
PhotonFilter.Etacut = 2.7
PhotonFilter.NPhotons = 2

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------

try:
     StreamEVGEN.RequireAlgs +=  [ "PhotonFilter" ]
except Exception, e:
     pass
               
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.1284*0.9
# 5000/38938=0.1284
