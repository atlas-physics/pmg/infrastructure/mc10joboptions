###############################################################
#
# Job options file
# Junichi TANAKA 
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence('TopAlg')
from Sherpa_i.Sherpa_iConf import Sherpa_i
sherpa = Sherpa_i()

# set beam energy (interface currently sets it for ECM=7TeV; needs explicit changing in JO for other ECMs]
try:
    eBeam=runArgs.ecmEnergy/2.0
    sherpa.Parameters += [ "BEAM_ENERGY_1=%s" % eBeam,"BEAM_ENERGY_2=%s" % eBeam ]
except NameError:
    pass

topAlg += sherpa
from MC10JobOptions.SherpaFEvgenConfig import evgenConfig

# dummy needed
evgenConfig.inputfilebase = 'sherpa'
try:
    if runArgs.ecmEnergy == 7000.0:
        evgenConfig.inputfilebase = 'group10.phys-gener.sherpa010203.125617.SherpabbAtautaulhMA500TB60_7TeV.TXT.v1'
except NameError:
    pass
evgenConfig.efficiency = 0.9
evgenConfig.weighting = 0
