###############################################################
# Job options file for generating ttbar events with Protos
# Nuno Castro (nuno.castro@cern.ch)
#==============================================================
#
from AthenaCommon.AlgSequence import AlgSequence
topAlg=AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_PythiaMC09p_Common.py" )

Pythia.PythiaCommand += ["pyinit user protos",
                        "pydat1 parj 90 20000.",
                        "pydat3 mdcy 15 1 0",
                        "pyinit dumpr 1 12",
                        "pyinit pylistf 1"
                       ]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------

from MC10JobOptions.PythiaEvgenConfig import evgenConfig

# inputfilebase
evgenConfig.inputfilebase = 'protos'
try:
  if runArgs.ecmEnergy == 7000.0:
     evgenConfig.inputfilebase ='group.phys-gener.mcprotos.119891.ttZp100.TXT.mc10_v1'
  if runArgs.ecmEnergy == 10000.0:
     evgenConfig.inputfilebase = 'protos.events'
except NameError:
  pass

evgenConfig.efficiency = 0.9

#==============================================================
# End of job options file
#
###############################################################

