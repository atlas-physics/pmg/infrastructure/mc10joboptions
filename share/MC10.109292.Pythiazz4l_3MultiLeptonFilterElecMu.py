################################################################
#
# Pythia ZZ->4l(e,mu,tau) with 3lep(=e or mu) filter
#
# Responsible person(s)
#   Dec 16, 2008 : Daniela Rebuzzi (daniela.rebuzzi@cern.ch)
#
################################################################
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

# ... Pythia
include ( "MC10JobOptions/MC10_Pythia_Common.py" )
Pythia.PythiaCommand += [
     "pysubs msel 0",
     "pysubs msub 22 1",  
     "pydat3 mdme 174 1 0",
     "pydat3 mdme 175 1 0",
     "pydat3 mdme 176 1 0",
     "pydat3 mdme 177 1 0",
     "pydat3 mdme 178 1 0",
     "pydat3 mdme 179 1 0",
     "pydat3 mdme 180 1 0",
     "pydat3 mdme 181 1 0",
     "pydat3 mdme 182 1 1",
     "pydat3 mdme 183 1 0",
     "pydat3 mdme 184 1 1",
     "pydat3 mdme 185 1 0",
     "pydat3 mdme 186 1 1",
     "pydat3 mdme 187 1 0",
     "pydat3 mdme 188 1 0",
     "pydat3 mdme 189 1 0",
     "pydat1 parj 90 20000.",
     "pydat3 mdcy 15 1 0"
     ]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import  MultiLeptonFilter
topAlg += MultiLeptonFilter()

MultiLeptonFilter = topAlg.MultiLeptonFilter
MultiLeptonFilter.NLeptons  = 3
MultiLeptonFilter.Etacut = 10.0
MultiLeptonFilter.Ptcut = 5000.0

try:
  StreamEVGEN.RequireAlgs +=  [ "MultiLeptonFilter" ]
except Exception, e:
  pass


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 1

#==============================================================
#
# End of job options file
#
###############################################################
