###############################################################
#
# Start of job options file
#
###############################################################


import AthenaCommon.AtlasUnixGeneratorJob

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

Pythia.PythiaCommand += [ "pysubs msel 0" ]
Pythia.PythiaCommand += [ "pysubs msub 141 1" ]            # f + f-bar -> Z'
Pythia.PythiaCommand += [ "pydat2 pmas 32 1 700.0" ]   # Z' mass

#
# Interference values
#
# 1 = gam*, 2 = Z, 3 = Z', 4 = Z/gam*, 5 = Z'/gam*, 6 = Z'/Z, 7 = Z/Z'/gam*
#
Pythia.PythiaCommand += [ "pypars mstp 44 3"]             # 3 = only Z'

#
# Z'WW coupling strength
#
# set to (mZ'/mW)^2 to create very wide resonance
#
Pythia.PythiaCommand += [ "pydat1 paru 129 10"]


#
#  In order to prevent double counting in Pythia when PHOTOS is used
#
Pythia.PythiaCommand += [ "pydat1 parj 90 20000" ]
#
#  Tell Pythia NOT to decay taus
#
Pythia.PythiaCommand += [ "pydat3 mdcy 15 1 0" ]

#
# Z' decays
#
Pythia.PythiaCommand += [ "pydat3 mdme 289 1 0" ]      # Z' -> d d-bar
Pythia.PythiaCommand += [ "pydat3 mdme 290 1 0" ]      # Z' -> u u-bar
Pythia.PythiaCommand += [ "pydat3 mdme 291 1 0" ]      # Z' -> s s-bar
Pythia.PythiaCommand += [ "pydat3 mdme 292 1 0" ]      # Z' -> c c-bar
Pythia.PythiaCommand += [ "pydat3 mdme 293 1 0" ]      # Z' -> b b-bar
Pythia.PythiaCommand += [ "pydat3 mdme 294 1 1" ]      # Z' -> t t-bar
Pythia.PythiaCommand += [ "pydat3 mdme 295 1 -1" ]     # Z' -> b' b'-bar
Pythia.PythiaCommand += [ "pydat3 mdme 296 1 -1" ]     # Z' -> t' t'-bar
Pythia.PythiaCommand += [ "pydat3 mdme 297 1 0" ]      # Z' -> e+ e-
Pythia.PythiaCommand += [ "pydat3 mdme 298 1 0" ]      # Z' -> nu_e nu_e
Pythia.PythiaCommand += [ "pydat3 mdme 299 1 0" ]      # Z' -> mu+ mu-
Pythia.PythiaCommand += [ "pydat3 mdme 300 1 0" ]      # Z' -> nu_mu nu_mu
Pythia.PythiaCommand += [ "pydat3 mdme 301 1 0" ]      # Z' -> tau+ tau-
Pythia.PythiaCommand += [ "pydat3 mdme 302 1 0" ]      # Z' -> nu_tau nu_tau
Pythia.PythiaCommand += [ "pydat3 mdme 303 1 -1" ]      # Z' -> tau'+ tau'-
Pythia.PythiaCommand += [ "pydat3 mdme 304 1 -1" ]      # Z' -> nu'_tau nu'_tau
Pythia.PythiaCommand += [ "pydat3 mdme 305 1 1" ]      # Z' -> W+ W-
Pythia.PythiaCommand += [ "pydat3 mdme 306 1 -1" ]      # Z' -> H+ H-
Pythia.PythiaCommand += [ "pydat3 mdme 307 1 0" ]      # Z' -> Z gamma
Pythia.PythiaCommand += [ "pydat3 mdme 308 1 -1" ]      # Z' -> Z h
Pythia.PythiaCommand += [ "pydat3 mdme 309 1 -1" ]      # Z' -> h A
Pythia.PythiaCommand += [ "pydat3 mdme 310 1 -1" ]      # Z' -> H A



#!
#! W decays to quarks
#!
Pythia.PythiaCommand += [ "pydat3 mdme 190 1 0",   # d u~
                          "pydat3 mdme 191 1 0",   # d cbar
                          "pydat3 mdme 192 1 0",   # d tbar
                          "pydat3 mdme 193 1 -1",  # d t'bar
                          "pydat3 mdme 194 1 0",   # s ubar
                          "pydat3 mdme 195 1 0",   # s cbar
                          "pydat3 mdme 196 1 0",   # s tbar
                          "pydat3 mdme 197 1 -1",  # s t'bar
                          "pydat3 mdme 198 1 0",   # b ubar
                          "pydat3 mdme 199 1 0",   # b cbar
                          "pydat3 mdme 200 1 0",   # b tbar
                          "pydat3 mdme 201 1 -1",  # b t'bar
                          "pydat3 mdme 202 1 -1",  # b' ubar
                          "pydat3 mdme 203 1 -1",  # b' cbar
                          "pydat3 mdme 204 1 -1",  # b' tbar
                          "pydat3 mdme 205 1 -1" ]  # b' t'bar


#!
#! W decays to leptons
#!
Pythia.PythiaCommand += [ "pydat3 mdme 206 1 1",   # e nu_e
                          "pydat3 mdme 207 1 1",   # mu nu_mu
                          "pydat3 mdme 208 1 1",   # tau nu_tau
                          "pydat3 mdme 209 -1 0" ]  # tau' nu_tau'

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )
  
# ... Photos 
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.95
