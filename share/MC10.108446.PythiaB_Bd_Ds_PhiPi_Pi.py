###############################################################
#
#  Job options file for generation of B-events 
#  in user selected exclusive channel
#
#  Decay channel: B0d->Ds(Phi(K+K-)Pi)Pi
#
#  Author:  W. Walkowiak, 208-11-09
# 
# PRODUCTION SYSTEM FRAGMENT
#==============================================================
from MC10JobOptions.PythiaBEvgenConfig import evgenConfig
# Reduced events/file due to long production times 
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_PythiaB_Common.py" )

#--------------------------------------------------------------
# Algorithms 
#--------------------------------------------------------------
PythiaB.ForceCDecay = "no"
#--------------------------------------------------------------				 
# -------------  FORCE   YOUR  B CHANNEL  HERE -------------
#--------------------------------------------------------------
# To force your B-decay channels decomment following 2 lines:
include( "MC10JobOptions/MC10_PythiaB_CloseAntibQuark.py" )
PythiaB.ForceBDecay = "yes"
#
# open your exclusive channel here  Bd -> Ds+pi- with Ds->phi pi
# (charge conjugated)
#
# DXChannel to be set here (close all D- -> phi pi- channels)
DXChannel = "Dsbarphipi"
include( "MC10JobOptions/MC10_PythiaB_DXphipi.py" )
#
# background channel B0->Ds+(phiPi+)Pi-  is c.c.
# 898: B0  -> Ds+Pi-  has to be created from B0 -> csbar
#
PythiaB.PythiaCommand += [ "pydat3 mdme 898 1 2",
                           "pydat3 mdme 898 2 0",
                           "pydat3 kfdp 898 1 431",
                           "pydat3 kfdp 898 2 -211",
                           "pydat3 kfdp 898 3 0",
                           "pydat3 kfdp 898 4 0",
                           "pydat3 mdme 831 1 1"];
PythiaB.ForceCDecay = "yes";
#
# w.w., 2006-04-02 revised:
# 
# user_finsel.F decay particle channel
#
PythiaB.ForceDecayChannel = "DsPhiX";
#
# Decay particle selection for DsPhiX: 
#   0. (off)/ 1. (on) -- ordered as follows:
#     1 : PhiKK   decay -- Phi -> K+K- 
#                          ((from B0s, B0bar or B-) or (from B0sbar, B0 or B+))
#     2 : DsPhi   decay -- Ds*- -> Ds-X, Ds- -> Phi pi-, Phi -> K+K- 
#                          (from B0s, B0bar or B-) or (from B0sbar, B0 or B+))
#     3 : A1RhoPi decay -- a1 -> RhoPi   (from B0s)
#     4 : RhoPiPi decay -- Rho -> Pi+Pi- (from B0s)
#
PythiaB.DecayChannelParameters = [1., 0., 0., 0.];
 
#
#--------------------------------------------------------------
# --------  PYTHIA PARAMETERS OPTIMAL FOR BEAUTY PRODUCTION  --
#--------------------------------------------------------------
# 'msel 5' is only for fast tests! 
#  for correct b-production you should use 'msel 1'
# 'mstj 26 0' = no mixing was defined in Btune as default
# 'mstj 22 2' = no K0S, Lambda0 decays in Pythia - defined in Btune as default

include( "MC10JobOptions/MC10_PythiaB_Btune.py" )

# PythiaB.PythiaCommand += ["pysubs ckin 3 6.",
PythiaB.PythiaCommand += ["pysubs ckin 3 10.",
                          "pysubs ckin 9 -3.5",
                          "pysubs ckin 10 3.5",
                          "pysubs ckin 11 -3.5",
                          "pysubs ckin 12 3.5",
                          "pysubs msel 1"]

#--------------------------------------------------------------
# -------------  DEFINE SELECTION CUTS  -------------
#--------------------------------------------------------------				
#  ------------- Selections on b  quarks   -------------
# simulate  only b-flavour events
PythiaB.flavour =  5.
# PythiaB force exclusive decay channels only on b=-5 side 
# ------------------- b=5  --- and/or ---  b=-5 --------
PythiaB.cutbq = ["6.0 2.5 and 6.0 2.5"]

# PythiaB.cutbq = ["6.0 4.5 and 6.0 4.5"]
#  ------------- LVL1 muon cuts 0=OFF 1=ON -------------
PythiaB.lvl1cut = [ 1.,  6., 2.5]
#  ------------- LVL2 muon/electron cuts  0=OFF 1=ON-------------
PythiaB.lvl2cut = [ 0.,  13.,     6.,   2.5]
#  ------------- Offline cuts 0=OFF 1=ON -------------
PythiaB.offcut = [ 1., 0.5, 2.5, 3., 2.5, 0.5, 2.5]
# 
#  ------------- Maximum number of tries for hard process -------------
PythiaB.maxTriesHard = 5000000.
# 
#  ------------- Number of repeated hadronization mhadr -------------
PythiaB.mhadr =  100.
#  ------------- Number of events requested  -------------
#==============================================================
#
# End of job options file
#
###############################################################
