#----------------------------------------------------------------------------------------------
#
# ttbar production with exotic top charge of Q_exo = -4/3
# the top decay chain is:
# t_exo -> b +W- (instead of t -> b W+ in SM) 
# and correspondingly for the anti-particles.
#
#----------------------------------------------------------------------------------------------


from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 


include ( "MC10JobOptions/MC10_Pythia_Common.py" )


Pythia.PythiaCommand += [ "pyinit user acermc",
			 "pydat1 parj 90 20000.",
			 "pydat3 mdcy 15 1 0" ]


include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


from MC10JobOptions.AcerMCEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'acermc'
try:
  if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputfilebase = 'group.phys-gener.acermc38.117230.ttexoq_7TeV.TXT.mc10_v1'
  if runArgs.ecmEnergy == 10000.0:
    evgenConfig.inputfilebase = 'group.phys-gener.acermc38.117230.ttexoq_10TeV.TXT.mc10_v1'
except NameError:
  pass
evgenConfig.efficiency = 0.95

