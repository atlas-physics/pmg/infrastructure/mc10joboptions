from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence('TopAlg')
from Sherpa_i.Sherpa_iConf import Sherpa_i
sherpa = Sherpa_i()

"""
(run){
  EXCLUSIVE_CLUSTER_MODE=1
  GENERATE_RESULT_DIRECTORY=1
  ACTIVE[25]=0
  ME_SIGNAL_GENERATOR=Comix
  CSS_AS_FS_FAC=0.4
  CSS_AS_IS_FAC=0.4
}(run)

(model){
  MODEL         = SM
}(model)

(processes){
  Process 93 93 ->  13 -13 22 93{3}
  CKKW sqr(20/E_CMS)
  Order_EW 3
  End process;
}(processes)
(selector){
  Mass 11 -11 40 7000
  Mass 13 -13 40 7000
  PT 22  10 7000
  PT 11  0 7000
  PT -11 0 7000
  PT 13  0 7000
  PT -13 0 7000
  DeltaR -11 22 0.5  1000
  DeltaR 11 22 0.5  1000
  DeltaR -13 22 0.5  1000
  DeltaR 13 22 0.5  1000
  DeltaR 93 22 0.1  1000
}(selector)
"""

try:
    sherpa.Parameters += [ 'RUNDATA=%s' % runArgs.jobConfig[0] ]
    sherpa.Parameters += [ 'RUNDATFILE:=%s' % runArgs.jobConfig[0] ]
except NameError:
    pass

topAlg += sherpa
from MC10JobOptions.SherpaFEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'group.phys-gener.sherpa010300.145162.Zmumugamma_3jets_7TeV.TXT.mc10_v1'
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 5000
evgenConfig.weighting = 0


