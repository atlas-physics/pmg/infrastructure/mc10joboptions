# Zprime production with pythia
# prepared by Stephane Willoq NOvember 005
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

#--------------------------------------------------------------
# Algorithms Private Options
#--------------------------------------------------------------

# Zprime resonance mass (in GeV)
ZprimeMass = 1750

# Minimum mass for Drell-Yan production (in GeV)
ckin1 = 875

# Z prime specific parameters for pythia :
include ( "MC10JobOptions/MC10_Pythia_Common.py" )
Pythia.PythiaCommand += [
       "pysubs msel 0",
       "pysubs msub 141 1",   # Z',Z,g with interference
       # Z' decays - quarks
       "pydat3 mdme 289 1 0",
       "pydat3 mdme 290 1 0",
       "pydat3 mdme 291 1 0",
       "pydat3 mdme 292 1 0",
       "pydat3 mdme 293 1 0",
       "pydat3 mdme 294 1 0",
       # Z' decays - leptons
       "pydat3 mdme 297 1 1", #Z'-> e+ e-
       "pydat3 mdme 298 1 0",
       "pydat3 mdme 299 1 0", #Z'-> mu+ mu-
       "pydat3 mdme 300 1 0",
       "pydat3 mdme 301 1 0", #Z'-> tau+ tau-
       "pydat3 mdme 302 1 0",
       # tau decays are left open
       "pysubs ckin 1 "+str(ckin1),  # sqrhat > 875
       #    "pysubs ckin 13 -3",  #
       #    "pysubs ckin 14 3",   # eta cuts
       #    "pysubs ckin 15 -3",  # |eta| < 3
       #    "pysubs ckin 16 3",   #
       "pydat1 mstu 1 0",
       "pydat1 mstu 2 0",
       "pydat2 pmas 32 1 "+str(ZprimeMass),
        # cutoff for QED FSR in Pythia to 20000 GeV (='infinity', photos takes care of it).
       "pydat1 parj 90 20000.",
       "pydat3 mdcy 15 1 0"  ## Turn off tau decays
       ]


include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9

#==============================================================
#
# End of job options file
#
###############################################################

