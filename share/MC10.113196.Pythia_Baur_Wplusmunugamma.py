#--------------------------------------------------------------
#Acceptance sample
# W+(munu) + Gamma with full interference between ISR and FSR digram with Baur/Pythia
#--------------------------------------------------------------
# Non-filtered cross section in Rel. 15.6.1 : 28.4 pb
# No filter , efficiency = 100% safe factor= 0.9

# File prepared by Zhijun Liang  March, 2010
#--------------------------------------------------------------

###############################################################
#
# Job options file
#
#==============================================================
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

# for CTEQ6L1
include ( "MC10JobOptions/MC10_PythiaMC09p_Common.py" )

Pythia.PythiaCommand += [ "pyinit user lhef",
                          "pyinit pylisti -1","pyinit pylistf 1","pyinit dumpr 1 2",
                          "pydat1 parj 90 20000", # Turn off FSR.
                          "pydat3 mdcy 15 1 0"   # Turn off tau decays.
]

## ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )


## ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.generators += ["Lhef", "Pythia"]
evgenConfig.inputfilebase = 'group09.phys-gener.Baur001Pythia.113196.Wplusmunugamma.TXT.v1'
evgenConfig.efficiency = 0.9
#==============================================================
#
# End of job options file
#
###############################################################

