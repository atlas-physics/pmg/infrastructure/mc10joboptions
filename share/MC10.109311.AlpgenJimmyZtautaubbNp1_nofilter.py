# Alpgen Z(->tautau)+bb+1p
#--------------------------------------------------------------
# File prepared by Soshi Tsuno (Soshi.Tsuno@cern.ch) Dec 2008
#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------
# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 

try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common.py" )
  if runArgs.ecmEnergy == 14000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_14TeV.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig


Herwig.HerwigCommand += [ "iproc alpgen",
                          "taudec TAUOLA"
                        ]
# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
# no filter so far.

from MC10JobOptions.AlpgenEvgenConfig import evgenConfig
 
# inputfilebase
#evgenConfig.inputfilebase = 'alpgen.109311.ZtautaubbNp1_pt20_nofilter'
evgenConfig.inputfilebase = 'group09.phys-gener.alpgen.109311.ZtautaubbNp1_pt20_nofilter_7Tev.TXT.v1'

# 7TeV
# Filter efficiency  = 1.0
# MLM matching efficiency = 0.3471
# Alpgen cross section = 6.98464156+-0.00442795 pb
# Herwig cross section = Alpgen cross section * eff(MLM) = 2.4245 pb
# Integrated Luminosity = 4124.62111 pb-1
# Number of events in 4-vector file of Alpgen = 5000/(eff*0.8) = 19000 events
#
# 10TeV
# Filter efficiency  = 1.0
# MLM matching efficiency = 0.3214
# Alpgen cross section = 15.19422388+-0.01113858 pb
# Herwig cross section = Alpgen cross section * eff(MLM) = 4.883 pb
# Number of events in 4-vector file of Alpgen = 5000/(eff*0.8) = 20000 events
#
# Filter efficiency x safety factor = eff x 90% = 0.90
evgenConfig.efficiency = 0.90
evgenConfig.minevents=5000
#==============================================================
#
# End of job options file
#
###############################################################
