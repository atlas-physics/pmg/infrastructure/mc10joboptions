###############################################################
#
#Job options file
#Bernardo Resende
#modified: Liza Mijovic (liza.mijovic@ijs.si)
#ISR/FSR systematics sample: Pythia parameters of 105205 are varied so that
#the reco top mass is increased wrt. the default value 
#(using the standard t-mass reco procedure)
#==============================================================

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

# same as 105205
Pythia.PythiaCommand += [ "pyinit user acermc",
			 "pydat1 parj 90 20000.",
			 "pydat3 mdcy 15 1 0" ]

#6250 specific:
Pythia.PythiaCommand += [ "pypars mstp 3 1",      #set mstp 3 1 (setting Lambda in alpha_s)
                          "pypars parp 1 0.192",  #reset defaults after mstp 3 1
                          "pypars parp 72 0.096", #FSR Labmda value in running alpha_s (ATLAS def 0.192)
                          "pydat1 paru 112 0.192",#reset defaults after mstp 3 1
                          "pydat1 mstu 112 4",    #reset defaults after mstp 3 1
                          "pypars parp 61 0.384"] #ISR Labmda value in running alpha_s (ATLAS def 0.192)


# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.AcerMCEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'group09.phys-gener.acermc37.105205.tt_7TeV.TXT.v1'
evgenConfig.efficiency = 0.95

#==============================================================
#
# End of job options file
#
###############################################################


