###############################################################
#
# Job options file
#
#--------------------------------------------------------------

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

Pythia.PythiaCommand+= ["pyinit user comphep",
                        "pydat1 parj 90 20000",  ## Turn off FSR
                        "pydat3 mdcy 15 1 0"]    ## Turn off tau decays

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )
# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import PhotonFilter
topAlg += PhotonFilter(name = "PhotonFilter")

from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
topAlg += MultiLeptonFilter(name = "MultiLeptonFilter")

PhotonFilter = topAlg.PhotonFilter
PhotonFilter.Ptcut = 15000.
PhotonFilter.Etacut = 2.7
PhotonFilter.NPhotons = 1

MultiLeptonFilter = topAlg.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 15000.
MultiLeptonFilter.Etacut = 2.7
MultiLeptonFilter.NLeptons = 2

try:
    StreamEVGEN.RequireAlgs +=  [ "PhotonFilter" ]
except Exception, e:
    pass
try:
    StreamEVGEN.RequireAlgs += [ "MultiLeptonFilter" ]
except Exception, e:
    pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------


from MC10JobOptions.CompHepEvgenConfig import evgenConfig

evgenConfig.inputfilebase = 'group.phys-gener.comphep451.119888.Mustarmugam2100.TXT.mc10_v1'
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 5000

#--------------------------------------------------------------
#
# End of job options file
#
###############################################################
