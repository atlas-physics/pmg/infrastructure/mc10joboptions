# J1F sample (filter on leading jet)
#

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

Pythia.PythiaCommand += [  "pysubs msel 0",
                           # ND
                           "pysubs msub 11 1",
                           "pysubs msub 12 1",
                           "pysubs msub 13 1",
                           "pysubs msub 28 1",
                           "pysubs msub 53 1",
                           "pysubs msub 68 1",
                           "pysubs msub 95 1",
                           # SD
                           "pysubs msub 92 1",
                           "pysubs msub 93 1",
                           # DD
                           "pysubs msub 94 1"
                           ]

Pythia.PythiaCommand += [  "pysubs ckin 3 15." ]

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

try:
     from JetRec.JetGetters import *
     a6alg=make_StandardJetGetter('AntiKt',0.6,'Truth').jetAlgorithmHandle()
     a6alg.JetFinalEtCut.MinimumSignal = 10.0*GeV
except Exception, e:
     pass

from GeneratorFilters.GeneratorFiltersConf import QCDTruthJetFilter
topAlg += QCDTruthJetFilter()

QCDTruthJetFilter = topAlg.QCDTruthJetFilter
QCDTruthJetFilter.MinPt = 35.*GeV
QCDTruthJetFilter.MaxEta = 999.
QCDTruthJetFilter.TruthJetContainer = "AntiKt6TruthJets"
QCDTruthJetFilter.DoShape = False

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------

try:
     StreamEVGEN.RequireAlgs += [ "QCDTruthJetFilter" ]
except Exception, e:
     pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig

evgenConfig.minevents = 5000

#==============================================================
#
# End of job options file
#
###############################################################

