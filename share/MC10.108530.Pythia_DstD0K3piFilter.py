###############################################################
# PRODUCTION FRAGMENT
#
# Job options file for generation of D*+ -> D0 pi+; D0 -> (6 intermediate states) -> K- pi+ pi- pi+
# 
#  1. K_1+ pi-   10323 211
#        K_1+ -> K*0 (K-pi+) pi+
#        K_1+ -> K+ rho0                  rho0-> pi pi 100% in decay.dec
#        K_1+ -> K- pi+ pi+
#  2. K*0 rho0
#         K*0->K- pi+
#  3. K+ a_1+    321 20213
#        a_1+-> rho0 pi+
#  4. K+ pi- rho0
#  5. K*0 pi - pi-
#         K*0->K- pi+
#  6. K+ pi- pi+ pi-
# 
#  AuthorList:
#  Woochun Park: October 5, 2010

#==============================================================

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

#--------------------------------------------------------------
# Algorithms 
#--------------------------------------------------------------

Pythia.PythiaCommand += ["pysubs ckin 3 2.",
                         "pysubs msel 1"]

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import DstD0K3piFilter
topAlg += DstD0K3piFilter()

DstD0K3piFilter = topAlg.DstD0K3piFilter
DstD0K3piFilter.DstPtcut = 5000.
DstD0K3piFilter.DstEtacut = 2.5

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------
try:
    StreamEVGEN.RequireAlgs +=  [ "DstD0K3piFilter" ]
except Exception, e:
    pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.0003
evgenConfig.minevents = 500

#==============================================================
#
# End of job options file
#
###############################################################
