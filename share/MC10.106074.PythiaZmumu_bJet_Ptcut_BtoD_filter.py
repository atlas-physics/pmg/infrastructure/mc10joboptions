###############################################################
#
# Job options file
# Developed by Pavel Staroba from CSC.006904.PythiaZeeJet.py
# Modified by Konstantin Toms ktoms@mail.cern.ch
# Filtering only Z->mumu + b-jets, and events with B -> D -> K Pi decays 
#
#==============================================================
# ... Main generator : Pythia
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_Pythia_Common.py" )
#include ( "MC10JobOptions/MC10_Pythia_Common.py" )
Pythia.PythiaCommand += [ "pysubs msel 13",
                         "pydat1 parj 90 20000", # Turn off FSR.
                         "pydat3 mdcy 15 1 0",   # Turn off tau decays.
                         "pysubs ckin 3 10.0",   # Lower P_T for hard 2 ->2 process.
                         "pysubs ckin 41 60.0",  # Lower Z invariant mass.
                         "pysubs ckin 43 60.0",  # Lower Z invariant mass.
                         "pydat3 mdme 174 1 0",
                         "pydat3 mdme 175 1 0",
                         "pydat3 mdme 176 1 0",
                         "pydat3 mdme 177 1 0",
                         "pydat3 mdme 178 1 0",
                         "pydat3 mdme 179 1 0",
                         "pydat3 mdme 182 1 0",    # Switch for Z->ee.
                         "pydat3 mdme 183 1 0",
                         "pydat3 mdme 184 1 1",    # Switch for Z->mumu.
                         "pydat3 mdme 185 1 0",
                         "pydat3 mdme 186 1 0",    # Switch for Z->tautau.
                         "pydat3 mdme 187 1 0",
			 "pysubs kfin 1  1 0",   # Switch off all flavours except b
                         "pysubs kfin 1 -1 0",
                         "pysubs kfin 2  1 0",
                         "pysubs kfin 2 -1 0",
                         "pysubs kfin 1  2 0",
                         "pysubs kfin 1 -2 0",
                         "pysubs kfin 2  2 0",
                         "pysubs kfin 2 -2 0",
                         "pysubs kfin 1  3 0",
                         "pysubs kfin 1 -3 0",
                         "pysubs kfin 2  3 0",
                         "pysubs kfin 2 -3 0",
                         "pysubs kfin 1  4 0",
                         "pysubs kfin 1 -4 0",
                         "pysubs kfin 2  4 0",
                         "pysubs kfin 2 -4 0",
                         "pysubs kfin 1  5 1",
                         "pysubs kfin 1 -5 1",
                         "pysubs kfin 2  5 1",
                         "pysubs kfin 2 -5 1",
			 "pydat3 mdme 747 1 0", # D0 -> K Pi only
                         "pydat3 mdme 748 1 0",
                         "pydat3 mdme 749 1 0",
                         "pydat3 mdme 750 1 0",
                         "pydat3 mdme 751 1 0",
                         "pydat3 mdme 752 1 0",
                         "pydat3 mdme 753 1 0",
                         "pydat3 mdme 754 1 0",
                         "pydat3 mdme 755 1 0",
                         "pydat3 mdme 756 1 0",
                         "pydat3 mdme 757 1 0",
                         "pydat3 mdme 758 1 0",
                         "pydat3 mdme 759 1 0",
                         "pydat3 mdme 760 1 0",
                         "pydat3 mdme 761 1 0",
                         "pydat3 mdme 762 1 0",
                         "pydat3 mdme 763 1 1", # D0 -> K Pi
                         "pydat3 mdme 764 1 1", # D0 -> K* Pi
                         "pydat3 mdme 765 1 0",
                         "pydat3 mdme 766 1 0",
                         "pydat3 mdme 767 1 0",
                         "pydat3 mdme 768 1 0",
                         "pydat3 mdme 769 1 0",
                         "pydat3 mdme 770 1 0",
                         "pydat3 mdme 771 1 0",
                         "pydat3 mdme 771 1 0",
                         "pydat3 mdme 773 1 0",
                         "pydat3 mdme 774 1 0",
                         "pydat3 mdme 775 1 0",
                         "pydat3 mdme 776 1 0",
                         "pydat3 mdme 777 1 0",
                         "pydat3 mdme 778 1 0",
                         "pydat3 mdme 779 1 0",
                         "pydat3 mdme 780 1 0",
                         "pydat3 mdme 781 1 0",
                         "pydat3 mdme 782 1 0",
                         "pydat3 mdme 783 1 0",
                         "pydat3 mdme 784 1 0",
                         "pydat3 mdme 785 1 0",
                         "pydat3 mdme 786 1 0",
                         "pydat3 mdme 787 1 0",
                         "pydat3 mdme 788 1 0",
                         "pydat3 mdme 789 1 0",
                         "pydat3 mdme 790 1 0",
                         "pydat3 mdme 791 1 0",
                         "pydat3 mdme 792 1 0",
                         "pydat3 mdme 793 1 0",
                         "pydat3 mdme 794 1 0",
                         "pydat3 mdme 795 1 0",
                         "pydat3 mdme 796 1 0",
                         "pydat3 mdme 797 1 0",
                         "pydat3 mdme 798 1 0",
                         "pydat3 mdme 799 1 0",
                         "pydat3 mdme 800 1 0",
                         "pydat3 mdme 801 1 0",
                         "pydat3 mdme 802 1 0",
                         "pydat3 mdme 803 1 0",
                         "pydat3 mdme 804 1 0",
                         "pydat3 mdme 805 1 0",
                         "pydat3 mdme 806 1 0",
                         "pydat3 mdme 807 1 0"
                         ]

# ... UE

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filters
#--------------------------------------------------------------

#from GeneratorFilters.GeneratorFiltersConf import BSignalFilter
#topAlg += BSignalFilter()
#BSignalFilter = topAlg.BSignalFilter
#BSignalFilter.BParticle_cuts = 511 # B^0
##BSignalFilter.Cuts_Final_hadrons_switch = True

from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
topAlg += ParentChildFilter()
ParentChildFilter = topAlg.ParentChildFilter
#ParentChildFilter.PDGParent = [411,413,421,423]  #  D^+-, D^{*+-},D^0,D^{*0}
#ParentChildFilter.PDGParent = [421]              #  D^0
#ParentChildFilter.PDGChild  = [321,211]          #  K^+-, pi^+-
ParentChildFilter.PDGParent = [511]               #  B^0
ParentChildFilter.PDGChild  = [421,423]           #  D^0,D^{*0}

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------

try:
#     StreamEVGEN.RequireAlgs += [ "ParentChildFilter","BSignalFilter" ]
	StreamEVGEN.RequireAlgs += [ "ParentChildFilter" ]
except Exception, e:
     pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
# MetaData: cross-section (nb)= 0.0376352
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.06
evgenConfig.minevents  = 500
#==============================================================
#
# End of job options file
#
###############################################################


