from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence('TopAlg')
from Sherpa_i.Sherpa_iConf import Sherpa_i
sherpa = Sherpa_i()

"""
(run){
  MASSIVE[15]=1
}(run)

(processes){
  Process 93 93 -> 15 -15 93{1};
  Order_EW 2;
  CKKW sqr(15/E_CMS)
  End process;
}(processes)

(selector){
  Mass 15 -15 40 14000
}(selector)

(me){
  ME_SIGNAL_GENERATOR = Comix
}(me)
"""

try:
    sherpa.Parameters += [ 'RUNDATA=%s' % runArgs.jobConfig[0] ]
except NameError:
    pass

topAlg += sherpa
from MC10JobOptions.SherpaFFEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 5000
evgenConfig.weighting = 0
