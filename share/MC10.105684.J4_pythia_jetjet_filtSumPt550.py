###############################################################
#
# Job options file
# Michiru Kaneda
#
#==============================================================

#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------

# dijet production with pythia
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

Pythia.PythiaCommand += [
			      "pysubs msel 0",
			      "pysubs ckin 3 140.",
			      "pysubs ckin 4 280.",
			      "pysubs msub 11 1",
			      "pysubs msub 12 1",
			      "pysubs msub 13 1",
			      "pysubs msub 68 1",
			      "pysubs msub 28 1",
			      "pysubs msub 53 1"]

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

##MultiObjects Filter (sumPt > 550GeV)
try:
     from JetRec.JetGetters import *
     c4=make_StandardJetGetter('Cone',0.4,'Truth')
     c4alg = c4.jetAlgorithmHandle()
except Exception, e:
     pass

from GeneratorFilters.GeneratorFiltersConf import MultiObjectsFilter
topAlg += MultiObjectsFilter()

MultiObjectsFilter = topAlg.MultiObjectsFilter
MultiObjectsFilter.PtCut = 10.*GeV
MultiObjectsFilter.EtaCut = 2.7
MultiObjectsFilter.JetPtCut = 10.*GeV
MultiObjectsFilter.JetEtaCut = 5.2
MultiObjectsFilter.UseSumPt = True
MultiObjectsFilter.SumPtCut = 550.*GeV
MultiObjectsFilter.UseEle = False
MultiObjectsFilter.UseMuo = True
MultiObjectsFilter.UsePho = False
MultiObjectsFilter.UseJet = True
MultiObjectsFilter.TruthJetContainer = "Cone4TruthJets"
MultiObjectsFilter.OutputLevel = 3

try:
     StreamEVGEN.RequireAlgs = [ "MultiObjectsFilter" ]
except Exception, e:
     pass


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.067
#==============================================================
#
# End of job options file
#
###############################################################

