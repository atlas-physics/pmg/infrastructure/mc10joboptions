###############################################################
#
# Job options file
#
# Higgs to tautau gamma with Pythia (gg->H and VBF H) with single lepton filter
#
# File prepared by Bruce Mellado December 2005
#
#=============================================================

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

# ... Pythia
include ( "MC10JobOptions/MC10_Pythia_Common.py" )

Pythia.PythiaCommand += [
                              "pysubs msel 0",
                              "pysubs msub 102 1",
                              "pysubs msub 123 1",
                              "pysubs msub 124 1",
                              "pydat2 pmas 25 1 120.",
                              "pydat3 mdme 210 1 0",
                              "pydat3 mdme 211 1 0",
                              "pydat3 mdme 212 1 0",
                              "pydat3 mdme 213 1 0",
                              "pydat3 mdme 214 1 0",
                              "pydat3 mdme 215 1 0",
                              "pydat3 mdme 216 1 0",
                              "pydat3 mdme 217 1 0",
                              "pydat3 mdme 218 1 0",
                              "pydat3 mdme 219 1 0",
                              "pydat3 mdme 220 1 1",
                              "pydat3 mdme 221 1 0",
                              "pydat3 mdme 222 1 0",
                              "pydat3 mdme 223 1 0",
                              "pydat3 mdme 224 1 0",
                              "pydat3 mdme 225 1 0",
                              "pydat3 mdme 226 1 0",
                              "pydat3 mdme 227 1 0",
                              "pydat3 mdme 228 1 0",
                              "pydat3 mdme 229 1 0",
                              "pydat3 mdme 230 1 0",
                              "pydat3 mdme 231 1 0",
                              "pydat3 mdme 232 1 0",
                              "pydat3 mdme 233 1 0",
                              "pydat3 mdme 234 1 0",
                              "pydat3 mdme 235 1 0",
                              "pydat3 mdme 236 1 0",
                              "pydat3 mdme 237 1 0",
                              "pydat3 mdme 238 1 0",
                              "pydat3 mdme 239 1 0",
                              "pydat3 mdme 240 1 0",
                              "pydat3 mdme 241 1 0",
                              "pydat3 mdme 242 1 0",
                              "pydat3 mdme 243 1 0",
                              "pydat3 mdme 244 1 0",
                              "pydat3 mdme 245 1 0",
                              "pydat3 mdme 246 1 0",
                              "pydat3 mdme 247 1 0",
                              "pydat3 mdme 248 1 0",
                              "pydat3 mdme 249 1 0",
                              "pydat3 mdme 250 1 0",
                              "pydat3 mdme 251 1 0",
                              "pydat3 mdme 252 1 0",
                              "pydat3 mdme 253 1 0",
                              "pydat3 mdme 254 1 0",
                              "pydat3 mdme 255 1 0",
                              "pydat3 mdme 256 1 0",
                              "pydat3 mdme 257 1 0",
                              "pydat3 mdme 258 1 0",
                              "pydat3 mdme 259 1 0",
                              "pydat3 mdme 260 1 0",
                              "pydat3 mdme 261 1 0",
                              "pydat3 mdme 262 1 0",
                              "pydat3 mdme 263 1 0",
                              "pydat3 mdme 264 1 0",
                              "pydat3 mdme 265 1 0",
                              "pydat3 mdme 266 1 0",
                              "pydat3 mdme 267 1 0",
                              "pydat3 mdme 268 1 0",
                              "pydat3 mdme 269 1 0",
                              "pydat3 mdme 270 1 0",
                              "pydat3 mdme 271 1 0",
                              "pydat3 mdme 272 1 0",
                              "pydat3 mdme 273 1 0",
                              "pydat3 mdme 274 1 0",
                              "pydat3 mdme 275 1 0",
                              "pydat3 mdme 276 1 0",
                              "pydat3 mdme 277 1 0",
                              "pydat3 mdme 278 1 0",
                              "pydat3 mdme 279 1 0",
                              "pydat3 mdme 280 1 0",
                              "pydat3 mdme 281 1 0",
                              "pydat3 mdme 282 1 0",
                              "pydat3 mdme 283 1 0",
                              "pydat3 mdme 284 1 0",
                              "pydat3 mdme 285 1 0",
                              "pydat3 mdme 286 1 0",
                              "pydat3 mdme 287 1 0",
                              "pydat3 mdme 288 1 0",
                              "pydat1 parj 90 20000", ## Turn off FSR
                              "pydat3 mdcy 15 1 0"]   ## Turn off tau decays


# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )


# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )



#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
topAlg += MultiLeptonFilter()

MultiLeptonFilter = topAlg.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 5000.
MultiLeptonFilter.Etacut = 2.7
MultiLeptonFilter.NLeptons = 1


try:
     StreamEVGEN.RequireAlgs +=  [ "MultiLeptonFilter" ]
except Exception, e:
     pass
               

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.46*0.9
