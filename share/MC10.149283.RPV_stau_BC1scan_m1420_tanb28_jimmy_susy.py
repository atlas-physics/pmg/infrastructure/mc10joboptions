from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

 
try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_HerwigRpv_Common_7TeV.py" )
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_HerwigRpv_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_HerwigRpv_Common.py" )
  if runArgs.ecmEnergy == 14000.0:
    include ( "MC10JobOptions/MC10_HerwigRpv_Common_14TeV.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.HerwigRpv_iConf import HerwigRpv
  Herwig = HerwigRpv("Herwig")
  topAlg += Herwig


Herwig.HerwigCommand += [ "iproc 13000",
                          "susyfile susy_149283_RPV_stau_BC1scan_m1420_tanb28.txt",
                          "taudec TAUOLA" ]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.HerwigEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9

from MC10JobOptions.SUSYEvgenConfig import evgenConfig
#==============================================================
#
# End of job options file
#
###############################################################
