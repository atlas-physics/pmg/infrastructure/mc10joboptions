###############################################################
#
# Job options file
#
#==============================================================
# Central Production set up
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_PythiaMC09p_Common.py" )
Pythia.PythiaCommand += ["pyinit user madgraph"]

Pythia.PythiaCommand +=  [
    "pysubs msel 0",
    "pystat 1 3 4 5",
    "pyinit dumpr 1 5",
    "pyinit pylistf 1",
    "pydat3 mdcy 15 1 0",
    "pydat1 parj 90 20000", # Turn off FSR.  
    #    "pypars mstp 81 1",
    #    "pymssm imss 1 11",
    "pyinit pylistf 1",  # 1 = normal listing
    "pyinit pylisti 12",
    ]
# ... TAUOLA
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.MadGraphEvgenConfig import evgenConfig
# dummy needed
evgenConfig.inputfilebase = 'MadGraph'
try:
    if runArgs.ecmEnergy == 7000.0:
        evgenConfig.inputfilebase ='group10.phys-gener.Pythia_MadGraph.115530.vlq_qu_nc_900.TXT.v1'
    if runArgs.ecmEnergy == 8000.0:
        evgenConfig.inputfilebase = 'user.mdavies.000001.PythiaMadGraph_vlq_qu_nc_900_8TeV'
except NameError:
    pass

evgenConfig.efficiency = 0.9
#==============================================================
#
# End of job options file
#
###############################################################
