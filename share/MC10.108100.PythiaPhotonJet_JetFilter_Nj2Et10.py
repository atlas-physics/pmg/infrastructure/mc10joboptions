#-------------------------------------------------------------
# Pythia photon+jet
# w/ JetFilter : ET(jet) > 10 GeV and N(jet) = 2
# Prepared by J. Tojo, November 2008
#-------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence( "TopAlg" ) 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )
Pythia.PythiaCommand += [ "pysubs msel 0",
                          "pysubs ckin 3 8.",
                          "pysubs msub 14 1",
                          "pysubs msub 29 1" ]

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import JetFilter
topAlg += JetFilter()

JetFilter = topAlg.JetFilter
JetFilter.JetNumber = 2
JetFilter.EtaRange = 2.7
JetFilter.JetThreshold = 10000.;  # 10 GeV
JetFilter.JetType = False; # true is a cone, false is a grid
JetFilter.GridSizeEta = 2; # sets the number of (approx 0.06 size) eta
JetFilter.GridSizePhi = 2; # sets the number of (approx 0.06 size) phi cells

#--------------------------------------------------------------
# Pool Output Options
#--------------------------------------------------------------
try:
     StreamEVGEN.RequireAlgs += ["JetFilter"]
except Exception, e:
     pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
# efficiency = 5000/93655 = 0.053
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.053
