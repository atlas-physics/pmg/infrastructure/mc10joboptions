###############################################################
#
# Job options file
#
# Process: Double diffractive inelastic - sqrt(s)=10TeV
# Contact: Arthur Moraes
# 
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

from Phojet_i.Phojet_iConf import Phojet

#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
topAlg +=Phojet()

#--------------------------------------------------------------
# PHOJET data-card file
#--------------------------------------------------------------
phojene=-1.
try:
  phojene=runArgs.ecmEnergy
except NameError:
  # needed (dummy) default
  phojene=10000.

phojf=open('./inparmPhojet.dat', 'w')
phojinp = """

***************************************************************
*      sample input file      PHOJET v1.0
*
*  - all the settings have to occur before the event generation
*  card EVENT-CMS
*  - order of the settings is unimportant,
*  - uppercase characters for the key words
***************************************************************
*
*
PARTICLE1 2212 0.
PARTICLE2 2212 0.
*
*  switch all processes on (default)
*
*             1  2  3  4  5  6  7  8
PROCESS       0  0  0  0  0  0  1  0
*
* LUND-DECAY 111 1 (pi0 will decay - default)
*
* DEBUG     1 7 15
*
*
EVENT-CMS      %s   0
*
*  that's all
*
ENDINPUT
"""%(phojene)

phojf.write(phojinp)
phojf.close()

from MC10JobOptions.PhoJetEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9

#==============================================================
#
# End of job options file
#
###############################################################


