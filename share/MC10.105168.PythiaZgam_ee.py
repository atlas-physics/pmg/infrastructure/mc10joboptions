# Z + 1 photon -> electrons
#--------------------------------------------------------------
# Modified by Ahmed Abdelalim (ahmed.ali.abdelalim@cern.ch)
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

Pythia.PythiaCommand += [
     "pysubs msel 0",
     "pysubs msub 19 1",      #
     "pysubs ckin 3 10.",
     "pydat3 mdme 174 1 0",
     "pydat3 mdme 175 1 0",
     "pydat3 mdme 176 1 0",
     "pydat3 mdme 177 1 0",
     "pydat3 mdme 178 1 0",
     "pydat3 mdme 179 1 0",
     "pydat3 mdme 182 1 1",    # Switch for Z->ee.
     "pydat3 mdme 183 1 0",
     "pydat3 mdme 184 1 0",    # Switch for Z->mumu.
     "pydat3 mdme 185 1 0",
     "pydat3 mdme 186 1 0",    # Switch for Z->tautau.
     "pydat3 mdme 187 1 0",
     "pydat1 parj 90 20000."]


# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


from GeneratorFilters.GeneratorFiltersConf import PhotonFilter
topAlg += PhotonFilter()

from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
topAlg += MultiLeptonFilter()

PhotonFilter = topAlg.PhotonFilter
PhotonFilter.Ptcut = 15000.
PhotonFilter.Etacut = 2.7
PhotonFilter.NPhotons = 1

MultiLeptonFilter = topAlg.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 15000.
MultiLeptonFilter.Etacut = 2.7
MultiLeptonFilter.NLeptons = 2


#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------

try:
     StreamEVGEN.RequireAlgs +=  [ "PhotonFilter" ]
except Exception, e:
     pass
try:
     StreamEVGEN.RequireAlgs += [ "MultiLeptonFilter" ]
except Exception, e:
     pass


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
# REL14 
# 6066/22728*0.9 = 0.2402 
evgenConfig.efficiency = 0.240
evgenConfig.minevents = 5000
#==============================================================
#
# End of job options file
#
###############################################################

