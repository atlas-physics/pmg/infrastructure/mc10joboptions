###############################################################
#
# Job options file
#
# Pythia ZH, Z->ee/mumu/tautau, H(130)->bb
#
# Responsible person(s)
#   12 Dec, 2008: Andrew Mehta (Andrew.Mehta@cern.ch)
#   18 Oct, 2010: Malachi Schram (malachi.schram@cern.ch)
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
include ( "MC10JobOptions/MC10_Pythia_Common.py" )
#--------------------------------------------------------------
# ZH, Z --> ll, H --> bb
Pythia.PythiaCommand +=[ "pysubs msel 0",
                         "pydat2 pmas 25 1 130.",
                         "pydat1 parj 90 20000", # Turn off FSR.
                         "pydat3 mdcy 15 1 0",  # Turn off tau decays.
                         "pysubs msub 24 1",    # ZH production.
                         "pydat3 mdme 174 1 0", # Switch off all Z decays
                         "pydat3 mdme 175 1 0", # except to leptons.
                         "pydat3 mdme 176 1 0",
                         "pydat3 mdme 177 1 0",
                         "pydat3 mdme 178 1 0",
                         "pydat3 mdme 179 1 0",
                         "pydat3 mdme 180 1 0",
                         "pydat3 mdme 181 1 0",
                         "pydat3 mdme 182 1 1",
                         "pydat3 mdme 183 1 0",
                         "pydat3 mdme 184 1 1",
                         "pydat3 mdme 185 1 0",
                         "pydat3 mdme 186 1 1", # Include Z->Tau??
                         "pydat3 mdme 187 1 0",
                         "pydat3 mdme 188 1 0",
                         "pydat3 mdme 189 1 0",
                         "pydat3 mdme 210 1 0", # Switch off all Higgs
                         "pydat3 mdme 211 1 0", # decays except to bb.
                         "pydat3 mdme 212 1 0",
                         "pydat3 mdme 213 1 0",
                         "pydat3 mdme 214 1 1",
                         "pydat3 mdme 215 1 0",
                         "pydat3 mdme 216 1 0",
                         "pydat3 mdme 217 1 0",
                         "pydat3 mdme 218 1 0",
                         "pydat3 mdme 219 1 0",
                         "pydat3 mdme 220 1 0",
                         "pydat3 mdme 221 1 0",
                         "pydat3 mdme 222 1 0",
                         "pydat3 mdme 223 1 0",
                         "pydat3 mdme 224 1 0",
                         "pydat3 mdme 225 1 0",
                         "pydat3 mdme 226 1 0"]
# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
