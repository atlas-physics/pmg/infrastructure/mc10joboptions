###############################################################
#
# Job options file for generation of cc events
# no decay channel is specified.
# Only events containing at least two muons
# with pT>2.5GeV |eta|<2.5 are written to output
# Selection criteria can be changed by datacards
#==============================================================
from MC10JobOptions.PythiaBEvgenConfig import evgenConfig
evgenConfig.minevents = 500
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_PythiaB_Common.py" )

#--------------------------------------------------------------
#              PARAMETERS  SPECIFIC  TO   PYTHIABMODULE
#--------------------------------------------------------------
PythiaB.ForceBDecay = "no"
PythiaB.ForceCDecay = "no"

include( "MC10JobOptions/MC10_PythiaB_Bchannels.py" )
include( "MC10JobOptions/MC10_PythiaB_Btune.py" )

#PythiaB.PythiaCommand += ["pysubs ckin 3 6.5",
PythiaB.PythiaCommand += ["pysubs ckin 3 8.5",
                           "pysubs msel 1"]
#--------------------------------------------------------------
# -------------  DEFINE SELECTION CUTS  -------------
#--------------------------------------------------------------
PythiaB.flavour =  4.
#  ------------- Selections on c quarks   -------------
#PythiaB.cutbq = ["4. 4.5 or 4. 4.5"]
PythiaB.cutbq = ["4. 4. or 4. 4."]
#  ------------- LVL1 muon cuts 0=OFF 1=ON -------------
PythiaB.lvl1cut = [ 1.,  2.5, 2.5]
#  ------------- LVL2 muon/electron cuts  0=OFF 1=ON-------------
PythiaB.lvl2cut = [ 1.,  13.,     2.5,   2.5]
#  ------------- Offline cuts 0=OFF 1=ON -------------
PythiaB.offcut = [ 0., 0., 102.5, 0., 102.5, 0., 102.5]
#  ------------- Number of repeated hadronization mhadr -------------
PythiaB.mhadr =  10.

#==============================================================
#
# End of job options file
#
###############################################################
