###############################################################
#
# Job options file
# prepared by Haiping Peng in Rel. 15.6.1.7 (Feb 2010)
# Acceptance sample
# Z (tau tau)+ Gamma with full interference between ISR and FSR digram with MadGraph/Pythia
# with Z 200GeV >mass >120GeV
#
#==============================================================

# ... Main generator : Pythia
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_PythiaMC09p_Common.py" )
Pythia.PythiaCommand += [ "pyinit user lhef",
                          "pyinit pylisti -1","pyinit pylistf 1","pyinit dumpr 1 2",
                          "pydat1 parj 90 20000", # Turn off FSR.
                          "pydat3 mdcy 15 1 0"   # Turn off tau decays.
]

## ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

## ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


#---------------------------------------------------------------------------
# Filter
#---------------------------------------------------------------------------
#
from GeneratorFilters.GeneratorFiltersConf import PhotonFilter
topAlg += PhotonFilter()
PhotonFilter = topAlg.PhotonFilter
PhotonFilter.Ptcut = 10000.
PhotonFilter.Etacut = 2.7
PhotonFilter.NPhotons = 1

from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
topAlg += LeptonFilter()
LeptonFilter = topAlg.LeptonFilter
LeptonFilter.Etacut = 2.7

try:
    StreamEVGEN.RequireAlgs +=  [ "PhotonFilter" ]
except Exception, e:
    pass
try:
    StreamEVGEN.RequireAlgs += [ "LeptonFilter" ]
except Exception, e:
    pass
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
# Non-filtered cross section in Rel. 15.6.1.7 : 8.7505e-02 pb
#--------------------------------------------------------------
# efficiency1 = 5511/18519*0.95=2.9759e-01*0.95=2.8271e-01
# efficiency2 = 5492/18519*0.95=2.9656e-01*0.95=2.8173e-01
# efficiency3 = 5590/18519*0.95=3.0185e-01*0.95=2.8676e-01
# efficiency4 = 5462/18519*0.95=2.9494e-01*0.95=2.8019e-01
# efficiency5 = 5513/18519*0.95=2.9769e-01*0.95=2.8281e-01
# efficiency6 = 5482/18519*0.95=2.9602e-01*0.95=2.8122e-01
# efficiency7 = 5380/18519*0.95=2.9051e-01*0.95=2.7599e-01
# efficiency8 = 5465/18519*0.95=2.9510e-01*0.95=2.8035e-01
# efficiency9 = 5507/18519*0.95=2.9737e-01*0.95=2.8250e-01
# efficienc10 = 5568/18519*0.95=3.0066e-01*0.95=2.8563e-01
# total effi =54970/185190*0.95=2.9683e-01*0.95=2.8199e-01
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
#from EvgenJobOptions.MadGraphEvgenConfig import evgenConfig
evgenConfig.generators += ["Lhef", "Pythia"]
evgenConfig.inputfilebase = "group09.phys-gener.MadGraph.105966.Ztautaugamma_M120_200.TXT.v1"
evgenConfig.efficiency = 0.27
#  ------------- Number of events requested  -------------
evgenConfig.minevents = 5000
#==============================================================
#
# End of job options file
#
###############################################################

