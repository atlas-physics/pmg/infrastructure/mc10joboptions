from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

#include ( "MC09JobOptions/MC9_PythiaMC09p_Common.py" )# use CTEQ6l PDFs
include ( "MC10JobOptions/MC10_PythiaMC09p_Common.py" )# use CTEQ6l PDFs

Pythia.PythiaCommand+= ["pyinit user lhef",
                         "pydat3 mdcy 15 1 0",
                         "pydat1 parj 90 20000"
                        ]
# Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

# Event Filter
from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
topAlg += MultiElecMuTauFilter()

MultiElecMuTauFilter = topAlg.MultiElecMuTauFilter
MultiElecMuTauFilter.MaxEta = 2.7
MultiElecMuTauFilter.MinPt = 10000.
MultiElecMuTauFilter.NLeptons = 2
MultiElecMuTauFilter.IncludeHadTaus = False

try:
    StreamEVGEN.RequireAlgs += [ "MultiElecMuTauFilter" ]
except Exception, e:
    pass

from MC10JobOptions.LhefEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'group10.phys-gener.Calchep26J.119705.MajNunnMN500MAUX3000aV4FLAM1000.TXT.v1'
evgenConfig.minevents = 5000  #This should give < 1% stat error after full acceptance
#evgenConfig.efficiency = 0.50 # Roughly 50% efficient for lowest mass. Being conservative using for all

