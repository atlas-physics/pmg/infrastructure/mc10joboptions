###############################################################
# PRODUCTION SYSTEM FRAGMENT
#       jobOptions for production of Yc(4260)-> J/psi pi+pi-,
#                                    J/psi -> mu+mu-
#
#==============================================================

#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
#evgenConfig.efficiency = 0.07

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

from GeneratorFilters.GeneratorFiltersConf import MultiMuonFilter
topAlg += MultiMuonFilter()

from GeneratorFilters.GeneratorFiltersConf import MuonFilter
topAlg += MuonFilter()

# AtRndmGenSvc.ReadFromFile = true;
Pythia = topAlg.Pythia

# Yc(4260)-> J/psi pi+pi-, J/psi -> mu+mu-
Pythia.PythiaCommand += [
     "pysubs msel 0",             #  turn OFF global process selection
     "pysubs msub 86 1",          #  g+g -> J/psi+g  turned ON
     "pyint2 kfpr 86 1 100443",   #  request Psi' instead of J/psi
     "pydat2 pmas 100443 1 4.26", #  set Psi(2s) mass to 4.26 GeV
     "pydat3 mdme 1567 1 0",      #  Psi' -> ee turned OFF
     "pydat3 mdme 1568 1 0",      #  Psi' -> mumu turned OFF
     "pydat3 mdme 1569 1 0",      #  Psi' -> random turned OFF
     "pydat3 mdme 1570 1 1",      #  Psi' -> J/psi pi+pi- turned ON
     "pydat3 mdme 1571 1 0",      #  Psi' -> J/psi pi0pi0 turned OFF
     "pydat3 mdme 1572 1 0",      #  Psi' -> J/psi eta turned OFF
     "pydat3 mdme 1573 1 0",      #  Psi' -> J/psi pi0 turned OFF
     "pydat3 mdme 1574 1 0",      #  Psi' -> chi_0c gamma turned OFF
     "pydat3 mdme 1575 1 0",      #  Psi' -> chi_1c gamma turned OFF
     "pydat3 mdme 1576 1 0",      #  Psi' -> chi_2c gamma turned OFF
     "pydat3 mdme 1577 1 0",      #  Psi' -> eta_c gamma turned OFF
     "pydat3 mdme 858 1 0",       #  J/psi -> ee turned OFF
     "pydat3 mdme 859 1 1",       #  J/psi -> mumu turned ON
     "pydat3 mdme 860 1 0"        #  J/psi -> random turned OFF
     ]


Pythia.PythiaCommand += ["pyinit pylistf 1",

                         "pysubs ckin 3 10.", # lower pT cut on hard process in 10 GeV
                         
                         "pystat mstat 1",
                         "pyinit dumpr 0 10",# dump this event range to screen
                             
                         ]


MultiMuonFilter = topAlg.MultiMuonFilter
MultiMuonFilter.Ptcut = 4000.
MultiMuonFilter.Etacut = 2.5
MultiMuonFilter.NMuons = 2

MuonFilter = topAlg.MuonFilter
MuonFilter.Ptcut = 6000.
MuonFilter.Etacut = 2.5

try:
     StreamEVGEN.RequireAlgs += ["MultiMuonFilter"]
     StreamEVGEN.RequireAlgs += ["MuonFilter"]
except Exception, e:
     pass

#==============================================================
#
# End of job options file
#
###############################################################
