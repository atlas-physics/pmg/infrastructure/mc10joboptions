################################################################
#
# MC@NLO/JIMMY/HERWIG H460 -> W+W- -> l+nu tau-nu or tau+nu l-nu,
# where l = e or mu
#
# Responsible person(s)
#   Jan. 20, 2011 : Tiesheng Dai (Tiesheng.Dai@cern.ch)
#
################################################################
#
# Job options file
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 
# ... Herwig
try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig

Herwig.HerwigCommand += [ "modbos 1 5", "modbos 2 4",
                          "maxpr 10",
                          "taudec TAUOLA"]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

from MC10JobOptions.McAtNloEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'mcatnlo0341.116538'
try:
  if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.mcatnlo0341.116538.07TeV_H460_WpWm.TXT.v1'
  if runArgs.ecmEnergy == 8000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.mcatnlo0341.116538.08TeV_H460_WpWm.TXT.v1'
  if runArgs.ecmEnergy == 10000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.mcatnlo0341.116538.10TeV_H460_WpWm.TXT.v1'
except NameError:
  pass

evgenConfig.efficiency = 0.9
#==============================================================
#
# End of job options file
#
###############################################################
