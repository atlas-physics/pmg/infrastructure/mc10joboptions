###########################################################
#                                                         #
#  R-hadron evgen job options fragment                    #
#  for running with csc_evgen08_trf.py                    #
#                                                         #
#                                                         #
#  Revised by P. Jackson for MC9 production               #
#     May 19th, 2010                                      #
#                                                         #
###########################################################

MASS=200
MASSX=100
CASE='gluino'
MODEL='intermediate'
DECAY='true'

include("MC10JobOptions/MC10_Pythia_StoppedGluino_Decay_Common.py")
evgenConfig.inputfilebase='group10.phys-gener.sgluG4.123039.Pythia_StoppedGluino_200.TXT.v1'
evgenConfig.minevents=100
