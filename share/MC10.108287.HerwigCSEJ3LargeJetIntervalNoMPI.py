###############################################################
#
# Job options file for Evgen
#
# Prepared by Graham Jones <graham.jones@cern.ch>
#
# Rel 15.1.0.3 (June 2009)
#
# Colour singlet exchange with fixed AlphaS tuned to Tevatron
# data. J3 parton Pt range. With multiple partonic interactions
# turned off.
#
#==============================================================

#Herwig as main generator
from AthenaCommon.AlgSequence import AlgSequence
topAlg=AlgSequence("TopAlg")
 

try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_Herwig_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_Herwig_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_Herwig_Common.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig


Herwig.HerwigCommand += ["iproc 2400",
                         "jmueo 0",
                         "msflag 0",
                         "ptmin 70.0",
                         "asfixd 0.17", ##Default is 0.25
                         "omega0 -0.3", #-ve to use new colour singlet process
                         "taudec TAUOLA" #for tau decay
                         ]


#Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

#Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Truth Jets
#--------------------------------------------------------------

# Make truth jets for JetGapFilter:
from AthenaCommon.AppMgr import ServiceMgr as svcMgr
include( "AthenaCommon/Atlas_Gen.UnixStandardJob.py" )
from JetRec.JetGetters import *
Kt7Alg = make_StandardJetGetter('Kt',0.7,'Truth', False).jetAlgorithmHandle()

#Change min pt of found jets to 0.0
Kt7Alg.AlgTools['JetFinalEtCut'].MinimumSignal = 0.0*GeV

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

## Truth filter
# Add the filter
from GeneratorFilters.GeneratorFiltersConf import JetIntervalFilter
etaFilter=JetIntervalFilter()
topAlg += etaFilter

# General properties
etaFilter.JetContainer = "Kt7TruthJets"
etaFilter.JetNumber = 2
etaFilter.OppositeSideJets = False

# Jet Energy Cuts (Cut on Et)
etaFilter.JetCutByPt = False

# Only used if cutting by et
etaFilter.Jet1MinEt = 12.0*GeV
etaFilter.Jet1MaxEt = 7000.0*GeV
etaFilter.Jet2MinEt = 12.0*GeV
etaFilter.Jet2MaxEt = 7000.0*GeV

# Jet Position Cuts (absolute)
etaFilter.Jet1MaxEta = 100.0
etaFilter.Jet1MinEta = 0.0
etaFilter.Jet2MaxEta = 100.0
etaFilter.Jet2MinEta = 0.0

# Jet delta eta cut
etaFilter.MinDeltaEta = 3.0
etaFilter.MaxDeltaEta = 10.0

# Control event weighting
etaFilter.WeightEvents = True
etaFilter.UniformMaxWeightBelowGausMean = False

# J3 tune
etaFilter.GausMean = 0.1608
etaFilter.GausSigma = 1.9138
etaFilter.AbsDEtaCutOff = 8.5

#--------------------------------------------------------------
# POOL / Root output
#--------------------------------------------------------------

try:
  StreamEVGEN.RequireAlgs += [ "JetIntervalFilter" ]
except Exception, e:
     pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
#Efficiency = (9900.0/58604033.0)*0.9 = .0001520
#Non-filtered cross section in Rel. 15.1.0.3 : 377237.2 pb
#--------------------------------------------------------------

from MC10JobOptions.HerwigEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.000152
evgenConfig.minevents=50

#==============================================================
#
# End of job options file
#
###############################################################
