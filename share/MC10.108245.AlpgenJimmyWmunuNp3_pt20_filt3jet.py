###############################################################
#
# Job options file
# Wouter Verkerke
#
#==============================================================
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 3

#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------

# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 

try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common.py" )
  if runArgs.ecmEnergy == 14000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_14TeV.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig


Herwig.HerwigCommand += [ "iproc alpgen",
                          "taudec TAUOLA",
                        ]
# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

# TruthJet filter
try:
     from JetRec.JetGetters import * 
     c4=make_StandardJetGetter('Cone',0.4,'Truth') 
     c4alg = c4.jetAlgorithmHandle() 
except Exception, e:
     pass
 
from GeneratorFilters.GeneratorFiltersConf import TruthJetFilter
topAlg += TruthJetFilter()

TruthJetFilter = topAlg.TruthJetFilter
TruthJetFilter.Njet=3;
TruthJetFilter.NjetMinPt=30.*GeV;
TruthJetFilter.NjetMaxEta=5;
TruthJetFilter.jet_pt1=30.*GeV;
TruthJetFilter.TruthJetContainer="Cone4TruthJets";

try:
     StreamEVGEN.RequireAlgs = [ "TruthJetFilter" ]
except Exception, e:
     pass


from MC10JobOptions.AlpgenEvgenConfig import evgenConfig
 
# inputfilebase
evgenConfig.inputfilebase = 'alpgen.108245.WmunuNp3.pt20.filt3jet.v2'
# Information on sample 108245
# Filter efficiency  = 0.2359
# MLM matching efficiency = 0.18
# Number of Matrix Elements in input file  = 18000
# Alpgen cross section = 1099.7 pb
# Herwig cross section = Alpgen cross section * eff(MLM) = 201.9 pb
# Cross section after filtering = 47.6 pb
# Lumi/500 events post-filter = 500/eff(Filter)/XS(jimmy) = 10.50 pb-1
#
# Filter efficiency estimate below reduced by 20% to produce 650 events on average,
# of which only 500 will be used in further processing
evgenConfig.efficiency = 0.18875
#==============================================================
#
# End of job options file
#
###############################################################
