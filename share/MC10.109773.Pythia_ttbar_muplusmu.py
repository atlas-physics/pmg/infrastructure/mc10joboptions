#==============================================================
#
# Job options fragment to produce ttbar events with Pythia.
# Decay Mode: 2 muons
# Martin Flechl, 20/6/09
#
#==============================================================

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

Pythia.PythiaCommand += ["pydat1 parj 90 20000",   # Turn off lepton radiation.
                         "pydat3 mdcy 15 1 0"]     # Turn off tau decays.

Pythia.PythiaCommand += [
            "pysubs msel 6",          # Heavy flavor production (ttbar)
#            "pysubs ckin 3 18.",      # ptmin for hard-scattering process
#            "pypars mstp 43 2",       # only Z0, not gamma*

            # W decays
            "pydat3 mdme 190 1 0", #W->qq
            "pydat3 mdme 191 1 0", #W->qq
            "pydat3 mdme 192 1 0", #W->qq
            "pydat3 mdme 194 1 0", #W->qq
            "pydat3 mdme 195 1 0", #W->qq
            "pydat3 mdme 196 1 0", #W->qq
            "pydat3 mdme 198 1 0", #W->qq
            "pydat3 mdme 199 1 0", #W->qq
            "pydat3 mdme 200 1 0", #W->qq
            "pydat3 mdme 206 1 0", #W->e,nu_e
            "pydat3 mdme 207 1 1", #W->mu,nu_mu
            "pydat3 mdme 208 1 0", #W->tau,nu_tau
            ]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.98
