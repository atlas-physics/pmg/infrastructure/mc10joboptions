###############################################################

#

# Job options file

# usage : 

# Sarah Allwood-Spiers

#==============================================================



# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )

MessageSvc = Service( "MessageSvc" )

MessageSvc.OutputLevel = 3



#--------------------------------------------------------------

# Generator

#--------------------------------------------------------------



### Herwig



from AthenaCommon.AlgSequence import AlgSequence 

topAlg = AlgSequence("TopAlg") 

 

try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common.py" )
  if runArgs.ecmEnergy == 14000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_14TeV.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig




# ... Tauola



Herwig.HerwigCommand+= [ "iproc alpgen" ]

Herwig.HerwigCommand+= [ "taudec TAUOLA"]



include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )



# ... Photos

include ( "MC10JobOptions/MC10_Photos_Fragment.py" )





#--------------------------------------------------------------

# Filter

#--------------------------------------------------------------

# no filter

#---



from MC10JobOptions.AlpgenEvgenConfig import evgenConfig

 
# inputfilebase
evgenConfig.inputfilebase = 'alpgen.106262.ttbarlnlnNp2_pt20'

evgenConfig.efficiency = 0.9



#Filter efficiency = 1.000

#MLM matching efficiency = 0.24

#2500 events per ME tarball producing 600 output events.

#Alpgen cross section = 22.82 pb

#Herwig cross section = Alpgen cross section *eff(MLM) = 5.471 pb

#Lumi/500 events = 500/XS(Herwig) = 91.39 pb^-1

#Filter efficiency estimate reduced by 10% to produce 550 events on average, of which only 500 will be used in further processing



#==============================================================

#

# End of job options file

#

###############################################################

