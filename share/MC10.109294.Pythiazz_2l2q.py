################################################################
#
# Pythia ZZ(*)->2l2q (l=e,mu,tau & q=d,u,s,c,b) no filter
#
# Responsible person(s)
#   May 18, 2010 : Joost Vossebeld (joost.vossebeld@cern.ch)
#
################################################################
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

# ... Pythia
include ( "MC10JobOptions/MC10_Pythia_Common.py" )
Pythia.PythiaCommand += [
     "pysubs msel 0",
     "pysubs msub 22 1",
     "pydat3 mdme 174 1 5",
     "pydat3 mdme 175 1 5",
     "pydat3 mdme 176 1 5",
     "pydat3 mdme 177 1 5",
     "pydat3 mdme 178 1 5",
     "pydat3 mdme 179 1 0",
     "pydat3 mdme 180 1 0",
     "pydat3 mdme 181 1 0",
     "pydat3 mdme 182 1 4",
     "pydat3 mdme 183 1 0",
     "pydat3 mdme 184 1 4",
     "pydat3 mdme 185 1 0",
     "pydat3 mdme 186 1 4",
     "pydat3 mdme 187 1 0",
     "pydat3 mdme 188 1 0",
     "pydat3 mdme 189 1 0",
     "pydat1 parj 90 20000.",
     "pydat3 mdcy 15 1 0"
     ]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 1

#==============================================================
#
# End of job options file
#
###############################################################
