from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence('TopAlg')
from Sherpa_i.Sherpa_iConf import Sherpa_i
sherpa = Sherpa_i()

"""
(run){
  EXCLUSIVE_CLUSTER_MODE=1
  GENERATE_RESULT_DIRECTORY=1
  ACTIVE[25]=0
  ME_SIGNAL_GENERATOR=Amegic

  CSS_AS_FS_FAC=0.4
  CSS_AS_IS_FAC=0.4 
}(run)

(model){
  MODEL         = SM
}(model)

(processes){
##changed for Z decay!
  Process 93 93 -> 11 -11 93{1};
  Order_EW 2;
  
  CKKW sqr(20/E_CMS)
  Scales VAR{PPerp2(p[2]+p[3])+Abs2(p[2]+p[3])}
  End process;
}(processes)
(selector){
Mass 11 -11 40. 99999.
}(selector)
"""

try:
    sherpa.Parameters += [ 'RUNDATA=%s' % runArgs.jobConfig[0] ]
except NameError:
    pass

topAlg += sherpa
from MC10JobOptions.SherpaFFEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 5000
evgenConfig.weighting = 0
