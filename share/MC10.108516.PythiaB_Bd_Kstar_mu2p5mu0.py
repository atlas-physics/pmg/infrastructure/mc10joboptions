###############################################################################
#
# MC10.108520.PythiaB_Bd_Kstar_mu2p5mu0.py
# Author: Pavel Reznicek (Pavel.Reznicek@cern.ch)
# Generation of Bd -> K*0 mu+ mu- decay using hand-written model in PythiaB
# PRODUCTION SYSTEM FRAGMENT
#
###############################################################################

#------------------------------------------------------------------------------
# Production driving parameters
#------------------------------------------------------------------------------

from MC10JobOptions.PythiaBEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.5

#------------------------------------------------------------------------------
# Import all needed algorithms (in the proper order)
#------------------------------------------------------------------------------

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include( "MC10JobOptions/MC10_PythiaB_Common.py" )

from GeneratorFilters.GeneratorFiltersConf import BSignalFilter
topAlg += BSignalFilter()
BSignalFilter = topAlg.BSignalFilter

#------------------------------------------------------------------------------
# PythiaB parameters settings
#------------------------------------------------------------------------------

PythiaB.ForceCDecay = "no"
PythiaB.ForceBDecay = "yes"

# Clasical PythiaB way of producing exclusive channel (close whole the decay and then open only one channel)
include( "MC10JobOptions/MC10_PythiaB_Bchannels.py" )
include( "MC10JobOptions/MC10_PythiaB_CloseAntibQuarkNew.py" )

# Open channel for Bd -> Kstar(892) mu+ mu-
PythiaB.PythiaCommand += [ "pydat3 mdme 4547 1 1",
                           "pydat3 kfdp 4547 1 13",
                           "pydat3 kfdp 4547 2 -13",
                           "pydat3 kfdp 4547 3 313",
                           "pydat3 kfdp 4547 4 0",
                           "pydat3 kfdp 4547 5 0",
                           "pydat3 brat 4547 0.0000001" ]

# User-finsel to redo the kinematics of the decay using proper model
PythiaB.ForceDecayChannel = "BdKstarMuMu"
# LVL1 and LVL2 cuts: pT_L1 eta_L1 pT_L2 eta_L2
PythiaB.DecayChannelParameters = [ 1., 2.5, 2.5, 1., 0.0, 2.5 ]

# Production settings
include( "MC10JobOptions/MC10_PythiaB_Btune.py" )

PythiaB.PythiaCommand += [ "pysubs ckin 3 5.",
                           "pysubs ckin 9 -3.5",
                           "pysubs ckin 10 3.5",
                           "pysubs ckin 11 -3.5",
                           "pysubs ckin 12 3.5",
                           "pysubs msel 1" ]

# Simulate only b-flavour events
PythiaB.flavour = 5.

# Pythia b-quark cuts
PythiaB.cutbq = [ "0 102.5 and 2.5 2.5" ]

# Repeated hadronization
PythiaB.mhadr = 1

#------------------------------------------------------------------------------
# Signal event filtering
#------------------------------------------------------------------------------

# Filtering on LVL1/LVL2 is done already in the ForceDecayChannel parameters, thus must be off here
# LVL1: pT_L1, eta_L1
PythiaB.lvl1cut = [ 0., 2.5, 2.5 ]
# LVL2: pdg (muon/electron), pT_L2, eta_L2
PythiaB.lvl2cut = [ 0., 13., 0., 2.5 ]
# Offline: pT, eta cuts for kaon/pion, muon, electron
PythiaB.offcut = [ 0., 0.5, 2.5, 0., 2.5, 0.5, 2.5 ]

# Hadronic tracks cuts
BSignalFilter.Cuts_Final_hadrons_switch = True
BSignalFilter.Cuts_Final_hadrons_pT     = 500.0
BSignalFilter.Cuts_Final_hadrons_eta    = 2.5
BSignalFilter.BParticle_cuts            = 511

#------------------------------------------------------------------------------
# POOL / Root output
#------------------------------------------------------------------------------

try:
  StreamEVGEN.RequireAlgs += [ "BSignalFilter" ]
except Exception, e:
  pass

###############################################################################
#
# End of job options fragment for Bd -> K*0 mu+ mu- decay
#
###############################################################################
