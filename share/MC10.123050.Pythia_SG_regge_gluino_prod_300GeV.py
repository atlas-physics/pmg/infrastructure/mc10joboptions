###########################################################
#                                                         #
#  R-hadron evgen job options fragment                    #
#  for running with csc_evgen08_trf.py                    #
#                                                         #                   
#                                                         #
#  Revised by P. Jackson for MC9 production               #
#     May 19th, 2010                                      #
#                                                         #
###########################################################

MASS=300
CASE='gluino'
DECAY='false'
MODEL='regge'

include("MC10JobOptions/MC10_Pythia_StoppedGluino_Production_Common.py")
