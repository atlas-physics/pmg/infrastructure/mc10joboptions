# dijet production with pythia
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

#--------------------------------------------------------------
# File prepared by Ian Hinchliffe Nov 2005
#--------------------------------------------------------------
Pythia.PythiaCommand += [
			      "pysubs msel 0",
			      "pysubs ckin 3 17.",
			      "pysubs ckin 4 35.",
			      "pysubs msub 11 1",
			      "pysubs msub 12 1",
			      "pysubs msub 13 1",
			      "pysubs msub 68 1",
			      "pysubs msub 28 1",
			      "pysubs msub 53 1",

# use fragmentation from Peterson for c/b, simmetric Lund for light quark                   

                              "pydat1 mstj 11 3",       # frag. function Peterson
                              "pydat1 parj 54 -0.07",   # c hadronization
                              "pydat1 parj 54 -0.006"   # b hadronization
                              ]
                              

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
