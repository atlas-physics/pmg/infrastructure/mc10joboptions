###############################################################
#
# Job options file
# Pythia8 Ztautau
# contact: Robindra Prabhu (prabhu@cern.ch) (April 2011)
#
#===============================================================
# ... Main generator : Pythia8

MessageSvc = Service( "MessageSvc" )
#MessageSvc.OutputLevel = 4

from AthenaCommon.AppMgr import ServiceMgr as svcMgr

import AthenaServices
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ("MC10JobOptions/MC10_Pythia8_Common.py")

Pythia8.Commands += ["WeakSingleBoson:ffbar2gmZ = on"] #create Z bosons
Pythia8.Commands += ["PhaseSpace:mHatMin = 60."] # lower invariant mass
Pythia8.Commands += ["23:onMode = off"]  # switch off all Z decays
Pythia8.Commands += ["23:onIfAny = 15"]  # switch on Z->tautau decays
#Pythia8.Commands += ["15:onMode = off"]  # turn off tau decays 


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
# efficiency = 0.95 - no filtering
from MC10JobOptions.Pythia8EvgenConfig import evgenConfig
evgenConfig.efficiency = 0.95
#==============================================================
#
# End of job options file
#
###############################################################
