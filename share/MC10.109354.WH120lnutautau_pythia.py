###############################################################
#
# Job options file
#
# Pythia WH, W->enu/munu/taunu, H(120)->tautau
#
# Responsible person(s)
#   12 Dec, 2008: Andrew Mehta (Andrew.Mehta@cern.ch)
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

#--------------------------------------------------------------
# WH, W --> lnu, H --> bb
Pythia.PythiaCommand +=[ "pysubs msel 0",
                         "pydat2 pmas 25 1 120.",
                         "pydat1 parj 90 20000", # Turn off FSR.
                         "pydat3 mdcy 15 1 0",     # Turn off tau decays.
                         "pysubs msub 26 1",    # WH production.
                         "pydat3 mdme 174 1 0", # Switch off all W decays
                         "pydat3 mdme 190 1 0", #apart from enu, munu, taunu
                         "pydat3 mdme 191 1 0",
                         "pydat3 mdme 192 1 0",
                         "pydat3 mdme 194 1 0",
                         "pydat3 mdme 195 1 0",
                         "pydat3 mdme 196 1 0",
                         "pydat3 mdme 198 1 0",
                         "pydat3 mdme 199 1 0",
                         "pydat3 mdme 200 1 0",
                         "pydat3 mdme 206 1 1",
                         "pydat3 mdme 207 1 1",
                         "pydat3 mdme 208 1 1",
                         "pydat3 mdme 210 1 0", # Switch off all Higgs
                         "pydat3 mdme 211 1 0", # decays except to tautau.
                         "pydat3 mdme 212 1 0",
                         "pydat3 mdme 213 1 0",
                         "pydat3 mdme 214 1 0", #bb
                         "pydat3 mdme 215 1 0",
                         "pydat3 mdme 216 1 0",
                         "pydat3 mdme 217 1 0",
                         "pydat3 mdme 218 1 0",
                         "pydat3 mdme 219 1 0",
                         "pydat3 mdme 220 1 1", #tau tau
                         "pydat3 mdme 221 1 0",
                         "pydat3 mdme 222 1 0",
                         "pydat3 mdme 223 1 0",
                         "pydat3 mdme 224 1 0",
                         "pydat3 mdme 225 1 0",
                         "pydat3 mdme 226 1 0"]
# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
