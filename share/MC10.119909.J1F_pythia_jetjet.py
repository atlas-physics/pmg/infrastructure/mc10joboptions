# J1F sample (filter on leading jet)
#

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

Pythia.PythiaCommand += [  "pysubs msel 1" ]

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

try:
     from JetRec.JetGetters import *
     a6alg=make_StandardJetGetter('AntiKt',0.6,'Truth').jetAlgorithmHandle()
     a6alg.JetFinalEtCut.MinimumSignal = 10.0*GeV
except Exception, e:
     pass

from GeneratorFilters.GeneratorFiltersConf import QCDTruthJetFilter
topAlg += QCDTruthJetFilter()

QCDTruthJetFilter = topAlg.QCDTruthJetFilter
QCDTruthJetFilter.MinPt = 20.*GeV
QCDTruthJetFilter.MaxPt = 60.*GeV
QCDTruthJetFilter.MaxEta = 999.
QCDTruthJetFilter.TruthJetContainer = "AntiKt6TruthJets"
#QCDTruthJetFilter.DoShape = True

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------

try:
     StreamEVGEN.RequireAlgs += [ "QCDTruthJetFilter" ]
except Exception, e:
     pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig

evgenConfig.minevents = 3000
#evgenConfig.efficiency = 0.00260844428571

#==============================================================
#
# End of job options file
#
###############################################################

