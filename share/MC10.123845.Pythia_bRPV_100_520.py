#-------------------------------------------------------------------------
#
# Bilinear RPV mSUGRA grid point
# m0_m12 masses in jobOptions, tan beta = 10, A0=0, sign(mu)=+1
#
# contact :  A. Redelbach, R. Mackeprang
#
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )
Pythia.SusyInputFile = "susy_bRPV_100_520_slha.txt";

Pythia.PythiaCommand += [ "pysubs msel 39", "pymssm imss 1 11" ]

# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = INFO

#---------------------------------------------------------------
# Includes
#---------------------------------------------------------------

Pythia.PythiaCommand += ["pydat1 parj 90 20000"] # Turn off FSR.
Pythia.PythiaCommand += ["pydat3 mdcy 15 1 0"]   # Turn off tau decays.

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos 
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------
# Add POOL persistency

from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9

from MC10JobOptions.SUSYEvgenConfig import evgenConfig
#---------------------------------------------------------------
#End of job options file
