###############################################################
#
# Job options file
# Developed by Pavel Staroba from CSC.006904.PythiaZeeJet.py
# in Rel. 14.2.0.1 (July 2008)
#
#==============================================================
# ... Main generator : Pythia
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

from Pythia_i.Pythia_iConf import Pythia
topAlg += Pythia()
Pythia = topAlg.Pythia

Pythia.PygiveCommand += [ "msel=13",
                          "parj(90)=20000", # Turn off FSR.
                          "mdcy(15,1)=0",   # Turn off tau decays.
                          "ckin(3)=10.0",   # Lower P_T for hard 2 ->2 process.
                          "ckin(41)=40.0",  # Lower Z invariant mass.
                          "ckin(43)=40.0",  # Lower Z invariant mass.
                          "mdme(174,1)=0",
                          "mdme(175,1)=0",
                          "mdme(176,1)=0",
                          "mdme(177,1)=0",
                          "mdme(178,1)=0",
                          "mdme(179,1)=0",
                          "mdme(182,1)=1",    # Switch for Z->ee.
                          "mdme(183,1)=0",
                          "mdme(184,1)=0",    # Switch for Z->mumu.
                          "mdme(185,1)=0",
                          "mdme(186,1)=0",    # Switch for Z->tautau.
                          "mdme(187,1)=0"
                         ]


# use only recommended ATLAS parameter settings
# all other ATLAS defaults are switched off
Pythia.Tune_Name="ATLAS_-1"
# call DW
Pythia.Direct_call_to_pytune=103
# keep ATLAS stable particles definition
Pythia.PygiveCommand += [ "mstj(22)=2" ]
# Turn off U.E. while keeping same PS algorithm
Pythia.PygiveCommand += [ "mstp(81)=20" ]


# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )




#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
# efficiency = 0.9 - no filtering
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.90
#==============================================================
#
# End of job options file
#
###############################################################
