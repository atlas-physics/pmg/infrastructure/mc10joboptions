###############################################################
#
# Job options file
# Junichi TANAKA 
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence('TopAlg')
from Sherpa_i.Sherpa_iConf import Sherpa_i
sherpa = Sherpa_i()
topAlg += sherpa
from MC10JobOptions.SherpaFEvgenConfig import evgenConfig

# dummy needed
evgenConfig.inputfilebase = 'sherpa'
try:
    if runArgs.ecmEnergy == 7000.0:
        evgenConfig.inputfilebase = 'group10.phys-gener.sherpa010203.116140.SherpabbAtautaulhMA250TB20_7TeV.TXT.v1'
except NameError:
    pass
evgenConfig.efficiency = 0.9
evgenConfig.weighting = 0
