###############################################################
#
# Job options file for min bias (ND)
#
# author: C. Gwenlan (June'10)
# AMBT1 eigentune 1- for systematic studies
# reference JO: MC10.105001.pythia_minbias.py
#===============================================================

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

#--------------------------------------------------------------
# AMBT1 systematic variation: eigentune 1-
Pythia.PythiaCommand += [
			      "pypars parp 77 1.35480",
                              "pypars parp 78 0.59025",
                              "pypars parp 82 2.07370",
                              "pypars parp 84 0.82325",
                              "pypars parp 90 0.24553"]

#--------------------------------------------------------------

Pythia.PythiaCommand += [  "pysubs msel 1" ]

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig

evgenConfig.efficiency = 0.9


#==============================================================
#
# End of job options file
#
###############################################################

