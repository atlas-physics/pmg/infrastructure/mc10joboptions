###############################################################
#
# Job options file
## Donatella Cavalli
#==============================================================

# ... Main generator : Pythia
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )
Pythia.PythiaCommand += [
                               "pysubs msel 0",
                               "pysubs msub 2 1",
#                               "pysubs ckin 3 20.",
                               "pydat3 mdme 190 1 0",
                               "pydat3 mdme 191 1 0",
                               "pydat3 mdme 192 1 0",
                               "pydat3 mdme 194 1 0",
                               "pydat3 mdme 195 1 0",
                               "pydat3 mdme 196 1 0",
                               "pydat3 mdme 198 1 0",
                               "pydat3 mdme 199 1 0",
                               "pydat3 mdme 200 1 0",
                               "pydat3 mdme 206 1 0",
                               "pydat3 mdme 207 1 0",
                               "pydat3 mdme 208 1 1",
                               "pydat3 mdcy 15 1 0",   # Turn off tau decays.
                               "pydat1 parj 90 20000", # Turn off FSR.
                               ]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import TauFilter
topAlg += TauFilter()

TauFilter = topAlg.TauFilter
TauFilter.Ntaus = 1
TauFilter.Ptcute = 1200000
TauFilter.EtaMaxe = 2.5
TauFilter.Ptcutmu = 1200000
TauFilter.EtaMaxmu = 2.5
TauFilter.Ptcuthad = 12000
TauFilter.EtaMaxhad = 2.7
TauFilter.OutputLevel = INFO

try:
     StreamEVGEN.RequireAlgs +=  [ "TauFilter" ]
except Exception, e:
     pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.27

#==============================================================
#
# End of job options file
#
###############################################################
