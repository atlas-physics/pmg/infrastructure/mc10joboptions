# Bruce Mellado December 2006
#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------

# ... Main generator : Pythia
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
include ( "MC10JobOptions/MC10_Pythia_Common.py" )
Pythia.PythiaCommand += [ "pysubs msel 0",          # Users decay choice.
                         "pydat1 parj 90 20000", # Turn off FSR.
                         "pydat3 mdcy 15 1 0",
                         "pysubs msub 2 1",        # Create W bosons.
                         "pysubs ckin 1 500.0",     # Lower mass cut
                         "pydat3 mdme 190 1 0",
                         "pydat3 mdme 191 1 0",
                         "pydat3 mdme 192 1 0",
                         "pydat3 mdme 193 1 0",
                         "pydat3 mdme 194 1 0",
                         "pydat3 mdme 195 1 0",
                         "pydat3 mdme 196 1 0",
                         "pydat3 mdme 197 1 0",
                         "pydat3 mdme 198 1 0",
                         "pydat3 mdme 199 1 0",
                         "pydat3 mdme 200 1 0",
                         "pydat3 mdme 201 1 0",
                         "pydat3 mdme 201 1 0",
                         "pydat3 mdme 202 1 0",
                         "pydat3 mdme 203 1 0",
                         "pydat3 mdme 204 1 0",
                         "pydat3 mdme 205 1 0",
                         "pydat3 mdme 206 1 1",
                         "pydat3 mdme 207 1 1",
                         "pydat3 mdme 208 1 1",
                         "pydat3 mdme 209 1 0"]

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )



###############################################################
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency=0.95
