###############################################################
#
# Job options file
#
# Alpgen gam+4parton (inclusive)
#
# Responsible person(s)
#   16 Dec, 2008-xx xxx, 20xx: Junichi TANAKA (Junichi.Tanaka@cern.ch)
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 

try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common.py" )
  if runArgs.ecmEnergy == 14000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_14TeV.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig


Herwig.HerwigCommand += [ "iproc alpgen",
                          "taudec TAUOLA"
                        ]
# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

from MC10JobOptions.AlpgenEvgenConfig import evgenConfig
 
# 7 TeV - Filter efficiency  = 1
# 7 TeV - MLM matching efficiency = 0.09914
# 7 TeV - Alpgen cross section = 3523 pb
# 7 TeV - Herwig cross section = Alpgen cross section * eff(MLM) = 349 pb
# 7 TeV - Cross section after filtering = 349 pb
# input file names
evgenConfig.inputfilebase = 'group09.phys-gener.alpgen.106127.gamNp5_pt20_7TeV.TXT.v1'
evgenConfig.efficiency = 0.9000
evgenConfig.minevents=5000
#==============================================================
#
# End of job options file
#
###############################################################
