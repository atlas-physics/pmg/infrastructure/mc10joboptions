################################################################
#
# Pythia Z->tautau with leptonic decay with 2lepEF (lep=e,mu)
#
# Responsible person(s)
#   Nov 12, 2008 : Junichi TANAKA (Junichi.Tanaka@cern.ch)
#
################################################################
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

# ... Pythia
include ( "MC10JobOptions/MC10_Pythia_Common.py" )
Pythia.PythiaCommand += [ "pysubs msel 0",
                          "pydat1 parj 90 20000", # Turn off FSR.
                          "pydat3 mdcy 15 1 0",   # Turn off tau decays.
                          "pysubs msub 1 1",      # Create Z bosons.
                          "pysubs ckin 1 60.0",   # Lower invariant mass.
                          "pydat3 mdme 174 1 0",
                          "pydat3 mdme 175 1 0",
                          "pydat3 mdme 176 1 0",
                          "pydat3 mdme 177 1 0",
                          "pydat3 mdme 178 1 0",
                          "pydat3 mdme 179 1 0",
                          "pydat3 mdme 182 1 0",    # Switch for Z->ee.
                          "pydat3 mdme 183 1 0",
                          "pydat3 mdme 184 1 0",    # Switch for Z->mumu.
                          "pydat3 mdme 185 1 0",
                          "pydat3 mdme 186 1 1",    # Switch for Z->tautau.
                          "pydat3 mdme 187 1 0",
                          "pydat3 mdme 188 1 0",
                          "pydat3 mdme 189 1 0"
                          ]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment_LeptonicDecay.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
topAlg += MultiLeptonFilter()

MultiLeptonFilter = topAlg.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 5000.
MultiLeptonFilter.Etacut = 3.0
MultiLeptonFilter.NLeptons = 2

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------
try:
    StreamEVGEN.RequireAlgs += [ "MultiLeptonFilter" ]
except Exception, e:
    pass            

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.34
#==============================================================
#
# End of job options file
#
###############################################################
