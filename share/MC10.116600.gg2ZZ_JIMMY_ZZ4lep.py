################################################################
#
# gg2ZZ1.0/JIMMY/HERWIG gg -> ZZ, with Z -> ee + mumu + tautau
#
# Responsible person(s)
#   April, 12 2010 : Daniela Rebuzzi (Daniela.Rebuzzi@cern.ch)
#
################################################################
#
# Job options file
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")
 
# ... Herwig
try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common_7TeV.py" )
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common.py" )
  if runArgs.ecmEnergy == 14000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common_14TeV.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig

Herwig.HerwigCommand += [ "modbos 1 5", "modbos 2 5",
                          "maxpr 10",
                          "modpdf 10000",    #  CTEQ6M
                          "taudec TAUOLA"]

# set UE tune appropriate for CTEQ6M (MC08 tune)
try:
  if runArgs.ecmEnergy == 7000.0:
    Herwig.HerwigCommand += ["ptjim 3.33","jmrad 73 1.8"]
  if runArgs.ecmEnergy == 10000.0:
    Herwig.HerwigCommand += ["ptjim 3.58","jmrad 73 1.8"]
except NameError:
  pass


# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import  MultiLeptonFilter
topAlg += MultiLeptonFilter()

MultiLeptonFilter = topAlg.MultiLeptonFilter
MultiLeptonFilter.NLeptons  = 3
MultiLeptonFilter.Etacut = 10.0
MultiLeptonFilter.Ptcut = 5000.0

try:
  StreamEVGEN.RequireAlgs +=  [ "MultiLeptonFilter" ]
except Exception, e:
  pass

from MC10JobOptions.McAtNloEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'gg2ZZ.116600'
try:
  if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.gg2ZZ.116600.07TeV_ZZ.TXT.v1'
  if runArgs.ecmEnergy == 8000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.gg2ZZ.116600.08TeV_ZZ.TXT.v1'
  if runArgs.ecmEnergy == 10000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.gg2ZZ.116600.10TeV_ZZ.TXT.v1'
except NameError:
  pass

               
evgenConfig.efficiency = 0.90
#==============================================================
#
# End of job options file
#
###############################################################
