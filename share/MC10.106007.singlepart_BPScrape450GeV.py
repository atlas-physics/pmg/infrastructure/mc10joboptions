# Single protons with injection energy scraping on beampipe (+x side) around z=-3m
# based on studies by D.Bocian (LHC PN335, scenario 5)

# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

from ParticleGenerator.ParticleGeneratorConf import ParticleGenerator
topAlg += ParticleGenerator()

ParticleGenerator = topAlg.ParticleGenerator

# For VERBOSE output from ParticleGenerator.
ParticleGenerator.OutputLevel = 1

ParticleGenerator.orders = [
 "PDGcode: constant 2212",
 "energy: constant 450000",
 "vertX: constant 9.0",
 "vertY: constant 0.0",
 "vertZ: constant -20850.0",
 "targetx: constant 29.0",
 "targety: constant 0.0",
 "targetz: constant -3000.0",
 "time: constant -20850.0"
 ]

#==============================================================
#
# End of job options file
#
###############################################################
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.SingleEvgenConfig import evgenConfig
