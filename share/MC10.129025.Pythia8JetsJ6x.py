###############################################################
#
# Job options file
# Pythia8 QCD samples
# contact: James Monk, Claire Gwenlan
#==============================================================

# ... Main generator : Pythia8

MessageSvc = Service( "MessageSvc" )
#MessageSvc.OutputLevel = 4

from AthenaCommon.AppMgr import ServiceMgr as svcMgr
  
import AthenaServices
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")
include ("MC10JobOptions/MC10_Pythia8_Common.py")

Pythia8.Commands += ["HardQCD:all = on"]
Pythia8.Commands += ["PhaseSpace:pTHatMin = 560."]
#Pythia8.Commands += ["PhaseSpace:pTHatMax = 1120."]

#
from MC10JobOptions.Pythia8EvgenConfig import evgenConfig
evgenConfig.efficiency = 0.95
