###############################################################
# PRODUCTION FRAGMENT
#
# Job options file for generation of cc-bar events 
#
# Only events containing at least one Charm Hadron
# in the decay of interest are written to output.
#
# Author : Leonid Gladilin (gladilin@mail.cern.ch), 2010-0004-20
#
# Selection criteria can be changed by datacards
#==============================================================

#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_PythiaB_Common.py" )

#--------------------------------------------------------------
# Algorithms 
#--------------------------------------------------------------
#--------------------------------------------------------------
#              PARAMETERS  SPECIFIC  TO   PYTHIABMODULE
#--------------------------------------------------------------
PythiaB.ForceBDecay = "no"
PythiaB.ForceCDecay = "no"
#
PythiaB.maxTriesHard = 500000
#
PythiaB.ForceDecayChannel = "CharmHadrons"
#
#                                  D+-  Ds+-  Lc+- D*(2)pi D*mu D*el D0(2) D0mu D0el |eta|
PythiaB.DecayChannelParameters = [ 2.8, 2.8, -2.8, 2.8,   -2.8, 0.0, 0.0,  0.0, 0.0,  2.7]
#--------------------------------------------------------------
# --------  PYTHIA PARAMETERS OPTIMAL FOR BEAUTY PRODUCTION  --
#--------------------------------------------------------------
#  'msel 5' is only for fast tests! 
#  for correct b-producion you should use 'msel 1'
# 'mstj 26 0' = no mixing was defined in Btune as default
# 'mstj 22 2' = no K0S, Lambda0 decays in Pythia - defined in Btune as default

include( "MC10JobOptions/MC10_PythiaB_Btune.py" )

PythiaB.PythiaCommand += ["pysubs ckin 3 2.",
				 "pysubs msel 4"]

#--------------------------------------------------------------
# -------------  DEFINE SELECTION CUTS  -------------
#--------------------------------------------------------------
# Use "PythiaB.flavour =  4."  (events with c-quarks)       before rel. 15.1.0
# Use "PythiaB.flavour =  5."  (events with b-quarks)       before rel. 15.1.0
# PythiaB.flavour =  4.
# Use "PythiaB.flavour =  45." (events with b- and c-quarks) since rel. 15.1.0
PythiaB.flavour =  45.	
#  ------------- Selections on b  quarks   -------------
PythiaB.cutbq = ["0. 4.5 or 0. 4.5"]
#  ------------- LVL1 muon cuts 0=OFF 1=ON -------------
PythiaB.lvl1cut = [ 0.,  4., 2.5]
#  ------------- LVL2 muon/electron cuts  0=OFF 1=ON-------------
PythiaB.lvl2cut = [ 0.,  13.,     6.,   2.8]
#  ------------- Offline cuts 0=OFF 1=ON -------------
PythiaB.offcut = [ 0., 0.5, 2.5, 3., 2.5, 0.5, 2.5]
#  ------------- Number of repeated hadronization mhadr -------------
PythiaB.mhadr =  1

from MC10JobOptions.PythiaBEvgenConfig import evgenConfig
#evgenConfig.minevents = 5000

#==============================================================
#
# End of job options file
#
###############################################################
