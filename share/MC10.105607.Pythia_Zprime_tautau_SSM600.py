# Zprime production with pythia
# prepared by Kevin Black October 006
# - minor update by J.Tanaka (April, 2008)
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

#--------------------------------------------------------------
# Algorithms Private Options
#--------------------------------------------------------------
 
# Zprime resonance mass (in GeV)
ZprimeMass = 600
 
# Minimum mass for Drell-Yan production (in GeV)
ckin1 = 300
 
# Z prime specific parameters for pythia :
include ( "MC10JobOptions/MC10_Pythia_Common.py" )
Pythia.PythiaCommand += [
        "pysubs msel 0",
        "pysubs msub 141 1", # Z',Z,g with interference
        "pydat3 mdcy 15 1 0", # turn off tau decays
        # Z' decays - quarks
        "pydat3 mdme 289 1 0",
        "pydat3 mdme 290 1 0",
        "pydat3 mdme 291 1 0",
        "pydat3 mdme 292 1 0",
        "pydat3 mdme 293 1 0",
        "pydat3 mdme 294 1 0",
        # Z' decays - leptons
        "pydat3 mdme 297 1 0", #Z'-> e+ e-
        "pydat3 mdme 298 1 0",
        "pydat3 mdme 299 1 0", #Z'-> mu+ mu-
        "pydat3 mdme 300 1 0",
        "pydat3 mdme 301 1 1", #Z'-> tau+ tau-
        "pydat3 mdme 302 1 0",
        # tau decays are left open
        "pysubs ckin 1 "+str(ckin1),  # sqrhat > 500
        "pydat1 mstu 1 0",
        "pydat1 mstu 2 0",
        "pydat2 pmas 32 1 "+str(ZprimeMass),
        # cutoff for QED FSR in Pythia to 20000 GeV (='infinity', photos takes care of it).
        "pydat1 parj 90 20000."     
        ]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )
 
# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )
 


from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9

#==============================================================
#
# End of job options file
#
###############################################################
