###############################################################
#
# Start of job options file
#
###############################################################


import AthenaCommon.AtlasUnixGeneratorJob

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_PythiaD6_Common.py" )
#
#--------------------------------------------------------------
#
#  Full user control
#
Pythia.PythiaCommand += [ "pysubs msel 0" ]     #desired subprocesses have to be switched on in MSUB, i.e. full user control
#
#  LQ production
#
#++
#    gg -> LQLQbar
Pythia.PythiaCommand += [ "pysubs msub 163 1" ]  #turn on
#
#    qqbar -> LQLQbar
Pythia.PythiaCommand += [ "pysubs msub 164 1" ]    #turn on
#
#  LQ mass
Pythia.PythiaCommand += [ "pydat2 pmas 42 1 1000.0" ]     #set mass of LQ
Pythia.PythiaCommand += [ "pydat3 mdcy 42 2 539" ]
Pythia.PythiaCommand += [ "pydat3 mdcy 42 3 2" ]
#
#  To avoid problems in MC generation (mass ranges)
# range of allowed mass values of the two (or one) resonances produced in a "true" 2->2 process

Pythia.PythiaCommand += [ "pysubs ckin 41 900" ]  #m1>150GeV
Pythia.PythiaCommand += [ "pysubs ckin 42 1100" ]  #m1<350GeV
Pythia.PythiaCommand += [ "pysubs ckin 43 900" ]  #m2>150GeV
Pythia.PythiaCommand += [ "pysubs ckin 44 1100" ]  #m2<350GeV

#
#  Branching fraction for the decay described below
Pythia.PythiaCommand += [ "pydat3 brat 539 1.0" ]  #set branching ration of LQ

#
#  This is relevant to both the decay AND production mechanism for single LQ
#
Pythia.PythiaCommand += [ "pydat3 kfdp 539 1 3" ]  #decay product Q
Pythia.PythiaCommand += [ "pydat3 kfdp 539 2 14" ] #decay product nu_{Q}

#
#  Coupling: lambda=sqrt(4pi*alpha_em)
#
#  This choice of lambda was made to make single LQ production to have approx. same cross section as pair production
#
Pythia.PythiaCommand += [ "pydat1 paru 151 0.01" ]
#
#-------------------------------------------------------------
#
#  In order to prevent double counting in Pythia when PHOTOS is used
#
Pythia.PythiaCommand += [ "pydat1 parj 90 20000" ]
#
#  Tell Pythia NOT to decay taus
#
Pythia.PythiaCommand += [ "pydat3 mdcy 15 1 0" ]
#
#-------------------------------------------------------------
#
#  Print the event listing for events x though y:
#
Pythia.PythiaCommand += [ "pyinit dumpr 1 20" ]
#
#-------------------------------------------------------------
#
# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

#
# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )



#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
#evgenConfig.efficiency = 0.95
evgenConfig.efficiency = 0.95

#==============================================================
#
# End of job options file
#
###############################################################
