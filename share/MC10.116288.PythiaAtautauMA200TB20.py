###############################################################
#
# Job options file
#
# Pythia MSSM gg->A, A->tautau, tanBeta=20, MA=200 GeV
#
# Responsible person(s)
#   19 Nov 2008 - xx xxx, 20xx: Trevor VICKEY (Trevor.Vickey@cern.ch)
#
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

# ... Pythia
include ( "MC10JobOptions/MC10_Pythia_Common.py" )
Pythia.PythiaCommand += [     "pysubs msel 0",
                              "pysubs msub 157 1",
                              "pymssm imss 1 1", # General MSSM
                              "pymssm rmss 5 20.", # tanBeta=20
                              "pymssm rmss 19 200.", # MA=200 GeV
                              "pydat3 mdme 420 1 0", # Decay of A0 (420-502)
                              "pydat3 mdme 421 1 0",
                              "pydat3 mdme 422 1 0",
                              "pydat3 mdme 423 1 0",
                              "pydat3 mdme 424 1 0",
                              "pydat3 mdme 425 1 0",
                              "pydat3 mdme 426 1 0",
                              "pydat3 mdme 427 1 0",
                              "pydat3 mdme 428 1 0",
                              "pydat3 mdme 429 1 0",
                              "pydat3 mdme 430 1 1",
                              "pydat3 mdme 432 1 0",
                              "pydat3 mdme 433 1 0",
                              "pydat3 mdme 434 1 0",
                              "pydat3 mdme 435 1 0",
                              "pydat3 mdme 436 1 0",
                              "pydat3 mdme 437 1 0",
                              "pydat3 mdme 438 1 0",
                              "pydat3 mdme 439 1 0",
                              "pydat3 mdme 440 1 0",
                              "pydat3 mdme 441 1 0",
                              "pydat3 mdme 442 1 0",
                              "pydat3 mdme 443 1 0",
                              "pydat3 mdme 444 1 0",
                              "pydat3 mdme 445 1 0",
                              "pydat3 mdme 446 1 0",
                              "pydat3 mdme 447 1 0",
                              "pydat3 mdme 448 1 0",
                              "pydat3 mdme 449 1 0",
                              "pydat3 mdme 450 1 0",
                              "pydat3 mdme 451 1 0",
                              "pydat3 mdme 452 1 0",
                              "pydat3 mdme 453 1 0",
                              "pydat3 mdme 454 1 0",
                              "pydat3 mdme 455 1 0",
                              "pydat3 mdme 456 1 0",
                              "pydat3 mdme 457 1 0",
                              "pydat3 mdme 458 1 0",
                              "pydat3 mdme 459 1 0",
                              "pydat3 mdme 460 1 0",
                              "pydat3 mdme 461 1 0",
                              "pydat3 mdme 462 1 0",
                              "pydat3 mdme 463 1 0",
                              "pydat3 mdme 464 1 0",
                              "pydat3 mdme 465 1 0",
                              "pydat3 mdme 466 1 0",
                              "pydat3 mdme 467 1 0",
                              "pydat3 mdme 468 1 0",
                              "pydat3 mdme 469 1 0",
                              "pydat3 mdme 470 1 0",
                              "pydat3 mdme 471 1 0",
                              "pydat3 mdme 472 1 0",
                              "pydat3 mdme 473 1 0",
                              "pydat3 mdme 474 1 0",
                              "pydat3 mdme 475 1 0",
                              "pydat3 mdme 476 1 0",
                              "pydat3 mdme 477 1 0",
                              "pydat3 mdme 478 1 0",
                              "pydat3 mdme 479 1 0",
                              "pydat3 mdme 480 1 0",
                              "pydat3 mdme 481 1 0",
                              "pydat3 mdme 482 1 0",
                              "pydat3 mdme 483 1 0",
                              "pydat3 mdme 484 1 0",
                              "pydat3 mdme 485 1 0",
                              "pydat3 mdme 486 1 0",
                              "pydat3 mdme 487 1 0",
                              "pydat3 mdme 488 1 0",
                              "pydat3 mdme 489 1 0",
                              "pydat3 mdme 490 1 0",
                              "pydat3 mdme 491 1 0",
                              "pydat3 mdme 492 1 0",
                              "pydat3 mdme 493 1 0",
                              "pydat3 mdme 494 1 0",
                              "pydat3 mdme 495 1 0",
                              "pydat3 mdme 496 1 0",
                              "pydat3 mdme 497 1 0",
                              "pydat3 mdme 498 1 0",
                              "pydat3 mdme 499 1 0",
			      "pydat3 mdme 500 1 0",
			      "pydat3 mdme 501 1 0",
			      "pydat3 mdme 502 1 0",
                              "pydat1 parj 90 20000",
                              "pydat3 mdcy 15 1 0"] 


# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

# Filter
from GeneratorFilters.GeneratorFiltersConf import ATauFilter
topAlg += ATauFilter()

ATauFilter = topAlg.ATauFilter
ATauFilter.Etacut      = 10.0
ATauFilter.llPtcute    = 5000.0
ATauFilter.llPtcutmu   = 5000.0
ATauFilter.lhPtcute    = 5000.0
ATauFilter.lhPtcutmu   = 5000.0
ATauFilter.lhPtcuth    = 0.0
ATauFilter.hhPtcut     = 10000.0
ATauFilter.OutputLevel = INFO

try:
     StreamEVGEN.RequireAlgs +=  [ "ATauFilter" ]
except Exception, e:
     pass

from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.874*0.9
#v15.6.1.7
#5000/5718=0.874 (7TeV)
#==============================================================
#
# End of job options file
#
###############################################################
