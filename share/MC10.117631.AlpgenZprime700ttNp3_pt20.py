###############################################################
#
# Job options file
# James Ferrando
#
#==============================================================
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 2
MessageSvc.infoLimit = 1000

#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------

# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 
try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_7TeV.py" )
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_8TeV.py" ) 
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common.py" )
  if runArgs.ecmEnergy == 14000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_14TeV.py" ) 

except NameError:
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig 

Herwig.HerwigCommand += [ "iproc alpgen",
                          "taudec TAUOLA",
                        ]
# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from MC10JobOptions.AlpgenEvgenConfig import evgenConfig
 
# inputfilebase
evgenConfig.inputfilebase = 'group10.phys-gener.alpgenzp.117631.AlpgenZprime700ttNp3'
try:
  if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.alpgenzp.117631.AlpgenZprime700ttNp3_7TeV.TXT.v1'
  if runArgs.ecmEnergy == 8000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.alpgenzp.117631.AlpgenZprime700ttNp3_8TeV.TXT.v1'    
  if runArgs.ecmEnergy == 10000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.alpgenzp.117631.AlpgenZprime700ttNp3_10TeV.TXT.v1'    
except NameError:
  pass

# 7 TeV - Information about sample 117631
# 7 TeV - Filter efficiency  = 1.0000
# 7 TeV - MLM matching efficiency =  0.224
# 7 TeV - Number of Matrix Elements in input file  = 2500 
# 7 TeV - Alpgen cross section =  0.692   pb
# 7 TeV - Herwig cross section = Alpgen cross section * eff(MLM) = 0.155 pb
evgenConfig.efficiency = 1.00000
#==============================================================
#
# End of job options file
#
###############################################################
