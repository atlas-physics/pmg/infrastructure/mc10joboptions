###############################################################
# Filtered minbias for e/gamma studies
# Generator Options
# Monika Weilers/ Ian Hinchliffe
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

#--------------------------------------------------------------
# Event related parameters
#--------------------------------------------------------------
include ( "MC10JobOptions/MC10_Pythia_Common.py" )

Pythia.PythiaCommand+= [  "pysubs msel 1" ]

#--------------------------------------------------------------
# Filter Options ('standard' jet filter)
#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import JetFilter
topAlg += JetFilter()

JetFilter = topAlg.JetFilter
JetFilter.JetNumber = 1
JetFilter.EtaRange = 2.7
JetFilter.JetThreshold = 6000.;  # Note this is 6 GeV
JetFilter.JetType=False; #true is a cone, false is a grid
JetFilter.GridSizeEta=2; # sets the number of (approx 0.06 size) eta
JetFilter.GridSizePhi=2; # sets the number of (approx 0.06 size) phi cells

#--------------------------------------------------------------
# Pool Output Options
#--------------------------------------------------------------
try:
     StreamEVGEN.RequireAlgs += ["JetFilter"]
except Exception, e:
     pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.045 # conservative

#==============================================================
#
# End of job options file
#
###############################################################
