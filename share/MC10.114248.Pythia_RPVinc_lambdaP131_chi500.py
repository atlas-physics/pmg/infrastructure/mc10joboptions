#-------------------------------------------------------------------------
# RPV inclusive decy of chi0 to:
# 2169    1   53    0.250000    nu_ebar         mu+             e-                                              
# 2170    1   53    0.250000    nu_e            mu-             e+                                              
# 2181    1   53    0.250000    nu_mubar        e+              e-                                              
# 2182    1   53    0.250000    nu_mu           e-              e+                                              
#
# contact :  C. Horn, R. Bruneliere
# 
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

Pythia.PythiaCommand += ["pyinit pylistf 1"] 
Pythia.PythiaCommand += ["pysubs msel 0"]  # !
Pythia.PythiaCommand += ["pysubs msub 271 1"]  # squark pair production 
Pythia.PythiaCommand += ["pysubs msub 272 1"]  # squark pair production 
Pythia.PythiaCommand += ["pysubs msub 273 1"]  # squark pair production 
Pythia.PythiaCommand += ["pysubs msub 274 1"]  # squark pair production 
Pythia.PythiaCommand += ["pysubs msub 275 1"]  # squark pair production 
Pythia.PythiaCommand += ["pysubs msub 276 1"]  # squark pair production 
Pythia.PythiaCommand += ["pysubs msub 277 1"]  # squark pair production
Pythia.PythiaCommand += ["pysubs msub 278 1"]  # squark pair production
Pythia.PythiaCommand += ["pysubs msub 279 1"]  # squark pair production
Pythia.PythiaCommand += ["pysubs msub 280 1"]  # squark pair production 
Pythia.PythiaCommand += ["pymssm imss 1 1"] 
Pythia.PythiaCommand += ["pymssm rmss 1 500"] #M1  
Pythia.PythiaCommand += ["pymssm rmss 2 5000."] #M2  
Pythia.PythiaCommand += ["pymssm rmss 3 1000"] #M3 
Pythia.PythiaCommand += ["pymssm rmss 4 800."] #mu 
Pythia.PythiaCommand += ["pymssm rmss 5 2."]  #tan beta 
Pythia.PythiaCommand += ["pymssm rmss 6 800."] #left slepton 
Pythia.PythiaCommand += ["pymssm rmss 7 5000."] #right slepton
Pythia.PythiaCommand += ["pymssm rmss 8 500"] #left squark
Pythia.PythiaCommand += ["pymssm rmss 9 5000."] #right squark
Pythia.PythiaCommand += ["pymssm rmss 10 5000."] #third generation, L 
Pythia.PythiaCommand += ["pymssm rmss 11 5000."] #third generation, sbottom
Pythia.PythiaCommand += ["pymssm rmss 12 5000."] #third generation, stop_R
Pythia.PythiaCommand += ["pymssm rmss 13 5000."] #third generation, stau
Pythia.PythiaCommand += ["pymssm rmss 14 5000."] #third generation, stau
#Pythia.PythiaCommand += ["pydat3 mdme 1988 1 0"] # switch explictly off  
#Pythia.PythiaCommand += ["pydat3 mdme 1989 1 0"] # switch explictly off
#Pythia.PythiaCommand += ["pydat3 mdme 1709 1 0"] # switch explictly off
Pythia.PythiaCommand += ["pymssm imss 52 3"] #switch on RPV lambda^prime coupling
Pythia.PythiaCommand += ["pymsrv rvlamp 131 0.1"] # setting lambda^prime coupling strength 

Pythia.PythiaCommand += ["pydat3 mdcy 15 1 0"]   # turn off pythia tau decays
Pythia.PythiaCommand += ["pydat1 parj 90 20000"] # turn off pythia FSR

# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = INFO

#---------------------------------------------------------------
# Includes
#---------------------------------------------------------------
# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9

from MC10JobOptions.SUSYEvgenConfig import evgenConfig
# ---------------------------------------------------------------
# End of job options file
#
# ##############################################################


