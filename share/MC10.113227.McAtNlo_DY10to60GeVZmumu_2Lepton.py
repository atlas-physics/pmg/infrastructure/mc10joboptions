###############################################################
#
# Job options file
# Marc Goulette, Claire Gwenlan
#
#===============================================================

# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common_7TeV.py" )
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common.py" )
  if runArgs.ecmEnergy == 14000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common_14TeV.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig

Herwig.HerwigCommand += [ "taudec TAUOLA"]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter

MultiLeptFiltL=MultiLeptonFilter(name = "MultiLeptFiltL",
                          Ptcut = 5.*GeV,
                          Etacut = 5.0,
                          NLeptons = 2)

MultiLeptFiltH=MultiLeptonFilter(name = "MultiLeptFiltH",
                          Ptcut = 15.*GeV,
                          Etacut = 5.0,
                          NLeptons = 1)

topAlg += MultiLeptFiltL
topAlg += MultiLeptFiltH

try:
  StreamEVGEN.RequireAlgs = [ "MultiLeptFiltL","MultiLeptFiltH"]
except Exception, e:
  pass


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.McAtNloEvgenConfig import evgenConfig

#dummy needed
evgenConfig.inputfilebase = 'mcatnlo'
try:
  if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.mcatnlo342.113227.DY10to60GeV_Zmumu_no_filter_Cteq66_PdfSet10550_7TeV.TXT.v1'    
#    print "EEEE 7 TeV"
  if runArgs.ecmEnergy == 8000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.mcatnlo342.113227.DY10to60GeV_Zmumu_no_filter_Cteq66_PdfSet10550_8TeV.TXT.v1'    
#    print "EEEE 8 TeV"
except NameError:
  pass

evgenConfig.efficiency = 0.95
#==============================================================
#
# End of job options file
#
###############################################################
