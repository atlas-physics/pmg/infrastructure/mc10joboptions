###############################################################
#
# Job options file for dijet production with pythia
#
# author: C. Gwenlan (June'10)
# AMBT1 eigentune 1- for systematic studies
# reference JO: MC10.105017.J8_pythia_jetjet.py
#===============================================================

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

#--------------------------------------------------------------
# AMBT1 systematic variation: eigentune 1-
Pythia.PythiaCommand += [
			      "pypars parp 77 1.35480",
                              "pypars parp 78 0.59025",
                              "pypars parp 82 2.07370",
                              "pypars parp 84 0.82325",
                              "pypars parp 90 0.24553"]

#--------------------------------------------------------------
Pythia.PythiaCommand += [
			      "pysubs msel 0",
			      "pysubs ckin 3 2240.",
			      "pysubs msub 11 1",
			      "pysubs msub 12 1",
			      "pysubs msub 13 1",
			      "pysubs msub 68 1",
			      "pysubs msub 28 1",
			      "pysubs msub 53 1"]

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
