###############################################################
#
# Job options file
# Marcos Jimenez Belenguer marcos.jimenez.belenguer@cern.ch
# H->a0a0->4photons
#
#===============================================================

# ... Main generator : Pythia
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_Pythia_Common.py" )
Pythia.PythiaCommand += [
    "pysubs msel 0",          # Users decay choice.
    "pydat1 parj 90 20000",   # Turn off FSR.
    "pydat3 mdcy 15 1 0",     # Turn off tau decays.
    "pysubs msub 152 1",      # Create Higgs
    "pydat2 pmas 25 1 2000.", # Higgs mass h0
    "pydat2 pmas 35 1 120.",  # Higgs mass H0
    "pydat2 pmas 36 1 0.3",   # Higgs mass A0
    "pydat3 mdme 334 1  0", # Higgs H0 decay (334-357)
    "pydat3 mdme 335 1  0",
    "pydat3 mdme 336 1  0",
    "pydat3 mdme 337 1  0",
    "pydat3 mdme 338 1  0",
    "pydat3 mdme 339 1  0",
    "pydat3 mdme 340 1  0",
    "pydat3 mdme 341 1  0",
    "pydat3 mdme 342 1  0",
    "pydat3 mdme 343 1  0",
    "pydat3 mdme 344 1  0",
    "pydat3 mdme 345 1  0",
    "pydat3 mdme 346 1  0",
    "pydat3 mdme 347 1  0",
    "pydat3 mdme 348 1  0",
    "pydat3 mdme 349 1  0",
    "pydat3 mdme 350 1  0",
    "pydat3 mdme 351 1  0",
    "pydat3 mdme 352 1  0",
    "pydat3 mdme 353 1  0",
    "pydat3 mdme 354 1  0",
    "pydat3 mdme 355 1  0",
    "pydat3 mdme 356 1  0",
    "pydat3 mdme 357 1  1", # H0->A0A0
    "pydat3 mdme 420 1  0", # Higgs A0 decay (420-502)
    "pydat3 mdme 421 1  0",
    "pydat3 mdme 422 1  0",
    "pydat3 mdme 423 1  0",
    "pydat3 mdme 424 1  0",
    "pydat3 mdme 425 1  0",
    "pydat3 mdme 426 1  0",
    "pydat3 mdme 427 1  0",
    "pydat3 mdme 428 1  0",
    "pydat3 mdme 429 1  0",
    "pydat3 mdme 430 1  0",
    "pydat3 mdme 431 1  0",
    "pydat3 mdme 432 1  0",
    "pydat3 mdme 433 1  1", # A0->gamma gamma
    "pydat3 mdme 434 1  0",
    "pydat3 mdme 435 1  0",
    "pydat3 mdme 436 1  0",
    "pydat3 mdme 437 1  0",
    "pydat3 mdme 438 1  0",
    "pydat3 mdme 439 1  0",
    "pydat3 mdme 440 1  0",
    "pydat3 mdme 441 1  0",
    "pydat3 mdme 442 1  0",
    "pydat3 mdme 443 1  0",
    "pydat3 mdme 444 1  0",
    "pydat3 mdme 445 1  0",
    "pydat3 mdme 446 1  0",
    "pydat3 mdme 447 1  0",
    "pydat3 mdme 448 1  0",
    "pydat3 mdme 449 1  0",
    "pydat3 mdme 450 1  0",
    "pydat3 mdme 451 1  0",
    "pydat3 mdme 452 1  0",
    "pydat3 mdme 453 1  0",
    "pydat3 mdme 454 1  0",
    "pydat3 mdme 455 1  0",
    "pydat3 mdme 456 1  0",
    "pydat3 mdme 457 1  0",
    "pydat3 mdme 458 1  0",
    "pydat3 mdme 459 1  0",
    "pydat3 mdme 460 1  0",
    "pydat3 mdme 461 1  0",
    "pydat3 mdme 462 1  0",
    "pydat3 mdme 463 1  0",
    "pydat3 mdme 464 1  0",
    "pydat3 mdme 465 1  0",
    "pydat3 mdme 466 1  0",
    "pydat3 mdme 467 1  0",
    "pydat3 mdme 468 1  0",
    "pydat3 mdme 469 1  0",
    "pydat3 mdme 470 1  0",
    "pydat3 mdme 471 1  0",
    "pydat3 mdme 472 1  0",
    "pydat3 mdme 473 1  0",
    "pydat3 mdme 474 1  0",
    "pydat3 mdme 475 1  0",
    "pydat3 mdme 476 1  0",
    "pydat3 mdme 477 1  0",
    "pydat3 mdme 478 1  0",
    "pydat3 mdme 479 1  0",
    "pydat3 mdme 480 1  0",
    "pydat3 mdme 481 1  0",
    "pydat3 mdme 482 1  0",
    "pydat3 mdme 483 1  0",
    "pydat3 mdme 484 1  0",
    "pydat3 mdme 485 1  0",
    "pydat3 mdme 486 1  0",
    "pydat3 mdme 487 1  0",
    "pydat3 mdme 488 1  0",
    "pydat3 mdme 489 1  0",
    "pydat3 mdme 490 1  0",
    "pydat3 mdme 491 1  0",
    "pydat3 mdme 492 1  0",
    "pydat3 mdme 493 1  0",
    "pydat3 mdme 494 1  0",
    "pydat3 mdme 495 1  0",
    "pydat3 mdme 496 1  0",
    "pydat3 mdme 497 1  0",
    "pydat3 mdme 498 1  0",
    "pydat3 mdme 499 1  0",
    "pydat3 mdme 500 1  0",
    "pydat3 mdme 501 1  0",
    "pydat3 mdme 502 1  0"
    ]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
# efficiency = 0.9 - no filtering
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.90
#==============================================================
#
# End of job options file
#
###############################################################
