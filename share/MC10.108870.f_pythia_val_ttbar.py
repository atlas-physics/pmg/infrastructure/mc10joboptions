#_________________________________________________________________
#
# nov. 2008
# Author: L. Mijovic: liza.mijovic@ijs.si
#_________________________________________________________________
# 
# fortran Pythia validation jobOptions
# process: ttbar, semileptonic (e, mu)
#_________________________________________________________________

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 


# configure Pythia
#_________________________________________________________________
# include Atlas Pythia defaults (standard for production)
include ( "MC10JobOptions/MC10_Pythia_Common.py" )

# set Pythia process
Pythia.PythiaCommand += [  "pydat1 parj 90 20000",  # set photon FSR cutoff to a large value == turn off photon FSR (standard for production)
                           "pydat3 mdcy 15 1 0",    # turn off tau decays (standard for production)
                           "pysubs msel 6"  ]       # select process: ttbar  


# top decay to W+b
Pythia.PythiaCommand += [  "pydat3 mdme 41 1 0", # gt
                           "pydat3 mdme 42 1 0", # gamma t
                           "pydat3 mdme 43 1 0", # Z0 t
                           "pydat3 mdme 44 1 0", # W+ d
                           "pydat3 mdme 45 1 0", # W+ s
                           "pydat3 mdme 46 1 1", # W+ b
                           "pydat3 mdme 47 1 0", # W+ b'
                           "pydat3 mdme 48 1 0", # h0 t
                           "pydat3 mdme 49 1 0", # H+ b
                           "pydat3 mdme 50 1 0", # ~chi_10 ~t_1
                           "pydat3 mdme 51 1 0", # ~chi_20 ~t_1
                           "pydat3 mdme 52 1 0", # ~chi_30 ~t_1
                           "pydat3 mdme 53 1 0", # ~chi_40 ~t_1
                           "pydat3 mdme 54 1 0", # ~g ~t_1
                           "pydat3 mdme 55 1 0" # ~gravitino ~t_1
                           ]   

# W+(had.) and W-(lept.) decay: (note: generate sample with W-(had.) and W+(lept.) in 1:1 proportions)
Pythia.PythiaCommand += [ "pydat3 mdme 190 1 2", # dbar u
                          "pydat3 mdme 191 1 2", # dbar c
                          "pydat3 mdme 192 1 2", # dbar t
                          "pydat3 mdme 193 1 0", # dbar t'
                          "pydat3 mdme 194 1 2", # sbar u
                          "pydat3 mdme 195 1 2", # sbar c
                          "pydat3 mdme 196 1 2", # sbar t
                          "pydat3 mdme 197 1 0", # sbar t'
                          "pydat3 mdme 198 1 2", # bbar u
                          "pydat3 mdme 199 1 2", # bbar c
                          "pydat3 mdme 200 1 2", # bbar t
                          "pydat3 mdme 201 1 0", # bbar t'
                          "pydat3 mdme 202 1 0", # b'bar u
                          "pydat3 mdme 203 1 0", # b'bar c
                          "pydat3 mdme 204 1 0", # b'bar t
                          "pydat3 mdme 205 1 0", # b'bar t'
                          "pydat3 mdme 206 1 3", # e+ nu_e
                          "pydat3 mdme 207 1 3", # mu+ nu_mu
                          "pydat3 mdme 208 1 0", # tau+ nu_tau
                          "pydat3 mdme 209 1 0"  # tau'+ nu'_tau
                          ]
#_________________________________________________________________


#add Tauola and Photos (standard for production)
#_________________________________________________________________
# Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )
 
# Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )
#_________________________________________________________________


# Configuration for EvgenJobTransforms
#_________________________________________________________________

from MC10JobOptions.PythiaEvgenConfig import evgenConfig

evgenConfig.efficiency = 0.9

#evgenConfig.minevents = 10

#_________________________________________________________________


