#
#  Control file to generate W_R (2100 GeV) -> l Nu_l (100 GeV) -> l l jet jet with 100% Ne Nmu mixing
#
#  Prepared by V.Savinov ( vladimirsavinov@gmail.com, http://totoro.phyast.pitt.edu/~savinov ), Feb. 16, 2011
#  and Kirill Skovpen (kirill.skovpen@gmail.com)
# 
#--------------------------------------------------------------
#
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )
#
#--------------------------------------------------------------
#
#  Full user control
#
Pythia.PythiaCommand += ["pysubs msel 0"]
#
#  ffbar -> W_R+
#
Pythia.PythiaCommand += ["pysubs msub 354 1"]
#
#  Masses of W_R, Z_R and Majorana neutrinos
#    
Pythia.PythiaCommand += ["pydat2 pmas 9900024 1 2100.0"]
Pythia.PythiaCommand += ["pydat2 pmas 9900023 1 2100.0"]
#
Pythia.PythiaCommand += ["pydat2 pmas 9900012 1 100.0"]
Pythia.PythiaCommand += ["pydat2 pmas 9900014 1 100.0"]
Pythia.PythiaCommand += ["pydat2 pmas 9900016 1 100.0"]
# 
#-------------------------------------------------------------
#
#  Force certain decays of W_R+ and W_R-: we are going to use only channels with leptons
#
Pythia.PythiaCommand += ["pydat3 mdme 4185 1 0"]  
Pythia.PythiaCommand += ["pydat3 mdme 4186 1 0"]   
Pythia.PythiaCommand += ["pydat3 mdme 4187 1 0"]   
Pythia.PythiaCommand += ["pydat3 mdme 4188 1 0"]   
Pythia.PythiaCommand += ["pydat3 mdme 4189 1 0"]   
Pythia.PythiaCommand += ["pydat3 mdme 4190 1 0"]   
Pythia.PythiaCommand += ["pydat3 mdme 4191 1 0"]   
Pythia.PythiaCommand += ["pydat3 mdme 4192 1 0"]   
Pythia.PythiaCommand += ["pydat3 mdme 4193 1 0"]   

#  e nu_Rmu

Pythia.PythiaCommand += ["pydat3 kfdp 4194 1 -11"]
Pythia.PythiaCommand += ["pydat3 kfdp 4194 2 9900014"]

#  mu nu_Re

Pythia.PythiaCommand += ["pydat3 kfdp 4195 1 -13"]
Pythia.PythiaCommand += ["pydat3 kfdp 4195 2 9900012"]

#
#  e+   nu_Rmu
#
Pythia.PythiaCommand += ["pydat3 mdme 4194 1 1"]
#
#  mu+  nu_Re
#
Pythia.PythiaCommand += ["pydat3 mdme 4195 1 1"]
#
#  tau+ nu_rTau
#
Pythia.PythiaCommand += ["pydat3 mdme 4196 1 0"]
#
#-------------------------------------------------------------
#
#  In order to prevent double counting in Pythia when PHOTOS is used
#
Pythia.PythiaCommand += ["pydat1 parj 90 20000"]
#
#  Tell Pythia NOT to decay taus
#
Pythia.PythiaCommand += ["pydat3 mdcy 15 1 0"]
#
#  Print the event listing for events x though y: 
#
Pythia.PythiaCommand += ["pyinit dumpr 1 20"]
#
#-------------------------------------------------------------
#
# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )
#
#-------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
