#--------------------------------------------------------------
# non-resonant production of diphotons with Pythia (including gg->gamgam)
# prepared by David Joffe and Azeddine Kasmi
# adapted for CSC production by Rashid Mehdiyev [rmehdi@lps.umontreal.ca]
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_Pythia_Common.py" )
Pythia.PythiaCommand += [       "pysubs msel 0",
                               "pysubs msub 18 1",
                               "pysubs msub 114 1",
                               "pysubs ckin 3 100.", 
                               "pyinit pylisti 12",
                               "pyinit pylistf 1",
                               "pystat 1 3 4 5",
                               "pyinit dumpr 1 5",
                               "pydat1 parj 90 20000."]

include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )
#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import PhotonFilter
topAlg += PhotonFilter()

PhotonFilter = topAlg.PhotonFilter
PhotonFilter.Ptcut = 100000.
PhotonFilter.Etacut = 3.5
PhotonFilter.NPhotons = 2
  
#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------
 
try:
     StreamEVGEN.RequireAlgs +=  [ "PhotonFilter" ]
except Exception, e:
     pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#-------------------------------------------------------------- 
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.73
