#H++ production with Pythia
#
# prepared by Kamal Benslama December 08 2005
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

#--------------------------------------------------------------
# Algorithms Private Options
#--------------------------------------------------------------

# H++  mass (in GeV)
hplusplusMass = 130

# H++ specific parameters for pythia :
Pythia.PythiaCommand += [
       "pysubs msel 0",
       "pydat1 parj 90 20000",
       "pydat3 mdcy 15 1 0",
       "pysubs msub 351 1",   # H++ production via ww fusion
       #LRSM parameters
       "pypars parp 181 1.",
       "pypars parp 182 0.",
       "pypars parp 183 0.",
       "pypars parp 184 0.",
       "pypars parp 185 0.",
       "pypars parp 186 0.",
       "pypars parp 187 0.",
       "pypars parp 188 0.",
       "pypars parp 189 0.",
       # vev value
       "pypars parp 192 9.",
       # H++ decays
       "pydat3 mdme 4271 1 1", # ee channel
       "pydat3 mdme 4272 1 0",
       "pydat3 mdme 4273 1 0",
       "pydat3 mdme 4274 1 0", # mumu channel
       "pydat3 mdme 4275 1 0",
       "pydat3 mdme 4276 1 0",
       "pydat3 mdme 4277 1 0",
       "pydat1 mstu 1 0",
       "pydat1 mstu 2 0",
       "pydat2 pmas 9900041 1 "+str(hplusplusMass),
       "pyinit pylisti 12"
       ]
# Tauola

include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# Photos

include ( "MC10JobOptions/MC10_Photos_Fragment.py" )
#==============================================================
#
# End of job options file
#
###############################################################
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
