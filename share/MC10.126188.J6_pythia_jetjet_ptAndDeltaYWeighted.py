###############################################################
#
# Job options file
#
# Pythia J6 event generation with pT-deltaY weighting.
# cross section as default pythia sample = 3.36*10^-2 nb
#
# Prepared by Graham Jones, Graham.Jones [at] cern.ch 
#==============================================================


# dijet production with pythia
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

Pythia.PythiaCommand += ["pysubs msel 0",
                         "pysubs ckin 3 560.",
                         "pysubs ckin 4 1120.",
                         "pysubs msub 11 1",
                         "pysubs msub 12 1",
                         "pysubs msub 13 1",
                         "pysubs msub 68 1",
                         "pysubs msub 28 1",
                         "pysubs msub 53 1"]

#--------------------------------------------------------------
# Truth Jets
#--------------------------------------------------------------

# Make truth jets for JetForwardFilter:
from AthenaCommon.AppMgr import ServiceMgr as svcMgr
include( "AthenaCommon/Atlas_Gen.UnixStandardJob.py" )
from JetRec.JetGetters import *
Kt6Alg = make_StandardJetGetter('AntiKt',0.6,'Truth').jetAlgorithmHandle()
Kt6Alg.AlgTools['JetFinalPtCut'].MinimumSignal = 7.0*GeV

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

# Add the filter to the correct sequence!
from GeneratorFilters.GeneratorFiltersConf import JetForwardFilter
forwardFilter=JetForwardFilter()
topAlg += forwardFilter

# General properties
forwardFilter.JetContainer = "AntiKt6TruthJets"

# Kinematic cuts
forwardFilter.MinDeltaY = 0.0
forwardFilter.MaxDeltaY = 100.0
forwardFilter.MinPt1 = 12.0 #GeV
forwardFilter.MinPt2 = 12.0
forwardFilter.MaxPt1 = 70000.0
forwardFilter.MaxPt1 = 70000.0

#Gaussian mixture model describing pdf of unweighted sample
forwardFilter.muXs = [ 731.820706007, 650.622888383, 876.309383036, 602.530218871, 598.243999224  ]
forwardFilter.muYs = [ 0.892697648674, 0.838326551009, 0.636286301218, 1.13424195645, 0.906778129894  ]
forwardFilter.sigmaXs = [ 96.2004823802, 62.7173987389, 130.165394961, 33.8618587606, 106.448356464  ]
forwardFilter.sigmaYs = [ 0.624528580115, 0.579696667046, 0.450634754226, 0.748058955852, 0.632388461803  ]
forwardFilter.rhos = [ 0.1703415594, 0.152446976162, 0.138403307704, -0.00697320644882, -0.0805129444035  ]
forwardFilter.weights = [ 0.189362835943, 0.276024505247, 0.0627850096023, 0.360614756845, 0.111212892363  ]

#Target probability (how much weighting required roughly)
#Lower = more suppression
import math
forwardFilter.SuppressionFactor = 5.0*math.pow(10.0, -8.0)

#--------------------------------------------------------------
# Apply filtering
#--------------------------------------------------------------

try:
  StreamEVGEN.RequireAlgs += [ "JetForwardFilter" ]
except Exception, e:
     pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
# N_gen = 176948119; N_pass = 100000 -> eff = 5.651*10^-4
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.0005651
evgenConfig.minevents=1000
evgenConfig.weighting=0
