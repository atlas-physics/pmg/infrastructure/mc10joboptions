###############################################################
#
#       jobOptions for production of B+ -> Psi(2S) K+,
#                      Psi(2S) -> J/psi pi+pi-, J/psi -> mu+mu-,
#       overwriting psi' and using B+ -> psi' K+
#
#==============================================================
#--------------------------------------------------------------
# General Application Configuration options
#--------------------------------------------------------------
from MC10JobOptions.PythiaBEvgenConfig import evgenConfig
evgenConfig.minevents = 500

#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_PythiaB_Common.py" )

#--------------------------------------------------------------	
#              PARAMETERS  SPECIFIC  TO   PYTHIAB
#--------------------------------------------------------------
PythiaB.ForceCDecay = "no"

# overwrite channels and close antib
include( "MC10JobOptions/MC10_PythiaB_Bchannels.py" )
include( "MC10JobOptions/MC10_PythiaB_CloseAntibQuarkNew.py" )

#--------------------------------------------------------------	
# -------------  FORCE   YOUR  B CHANNEL  HERE -------------
#--------------------------------------------------------------
#include( "MC10JobOptions/MC10_PythiaB_CloseAntibQuark.py" )

PythiaB.ForceBDecay = "yes"

# B+ -> Psi(2S) K+, Psi(2S) -> J/psi pi+pi-, J/psi -> mu+mu-
#PythiaB.PythiaCommand += ["pydat3 mdme 932 1 1",    
#                                "pydat3 kfdp 932 1 100443",   #  request Psi' instead of J/psi
PythiaB.PythiaCommand += ["pydat3 mdme 4651 1 1",    
                                "pydat3 mdme 1567 1 0",      #  Psi' -> ee turned OFF
                                "pydat3 mdme 1568 1 0",      #  Psi' -> mumu turned OFF
                                "pydat3 mdme 1569 1 0",      #  Psi' -> random turned OFF
                                "pydat3 mdme 1570 1 1",      #  Psi' -> J/psi pi+pi- turned ON
                                "pydat3 mdme 1571 1 0",      #  Psi' -> J/psi pi0pi0 turned OFF
                                "pydat3 mdme 1572 1 0",      #  Psi' -> J/psi eta turned OFF
                                "pydat3 mdme 1573 1 0",      #  Psi' -> J/psi pi0 turned OFF
                                "pydat3 mdme 1574 1 0",      #  Psi' -> chi_0c gamma turned OFF
                                "pydat3 mdme 1575 1 0",      #  Psi' -> chi_1c gamma turned OFF
                                "pydat3 mdme 1576 1 0",      #  Psi' -> chi_2c gamma turned OFF
                                "pydat3 mdme 1577 1 0",      #  Psi' -> eta_c gamma turned OFF
 				"pydat3 mdme 858 1 0",
 				"pydat3 mdme 860 1 0"        ]

#--------------------------------------------------------------
# --------  PYTHIA PARAMETERS OPTIMAL FOR BEAUTY PRODUCTION  --
#-------------------------------------------------------------
#  'msel 5' is only for fast tests! 
#  for correct b-producion you should use 'msel 1'
# 'mstj 26 0' = no mixing was defined in Btune as default
# 'mstj 22 2' = no K0S, Lambda0 decays in Pythia - defined in Btune as default

include( "MC10JobOptions/MC10_PythiaB_Btune.py" )

PythiaB.PythiaCommand += ["pysubs ckin 3 10.",
                          "pysubs ckin 9 -3.5",
                          "pysubs ckin 10 3.5",
                          "pysubs ckin 11 -3.5",
                          "pysubs ckin 12 3.5",
                          "pysubs msel 1"]
#--------------------------------------------------------------
# -------------  DEFINE SELECTION CUTS  -------------
#--------------------------------------------------------------	
#  ------------- Selections on b  quarks   -------------
# simulate  only b-flavour events
PythiaB.flavour =  5.
# PythiaB force exclusive decay channels only on b=-5 side 
# ------------------- b=5  --- and/or ---  b=-5 --------
PythiaB.cutbq = ["0. 102.5 and 8. 2.5"]
#  ------------- LVL1 muon cuts 0=OFF 1=ON -------------
PythiaB.lvl1cut = [ 1.,  4., 2.5]
#  ------------- LVL2 muon/electron cuts  0=OFF 1=ON-------------
PythiaB.lvl2cut = [ 0.,  13.,     4.,   2.5]
#  ------------- Offline cuts 0=OFF 1=ON -------------
PythiaB.offcut = [ 1., 0.5, 2.5, 4., 2.5, 0.5, 2.5]
#  ------------- Number of repeated hadronization mhadr -------------
PythiaB.mhadr =  1. 
#==============================================================
#
# End of job options file
#
###############################################################

