#--------------------------------------------------------------
# 
# Job options file
#
#
# Herwig & Jimmy gg->bt_H+/ (mH+=400GeV,tan(beta)=7) 
# 3 photon EF
#
# Responsible person(s)
#   23 Jan, 2008 :  Caleb LAMPEN (lampen@physics.arizona.edu)
# 
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence
topAlg=AlgSequence()


try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_Herwig_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_Herwig_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_Herwig_Common.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig


topAlg.Herwig.HerwigCommand += [ "iproc 3839",              #iproc gg->bt_H+ + ch. conjg
                                  "susyfile susy_mHp400tanb15.txt",#isawig input file
                                  "taudec TAUOLA",           #taudec tau dcay package
                                  "effmin 0.0000000001"]     #To fix early termination
#Setup tauola and photos. 
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )
                                 
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
topAlg +=MultiLeptonFilter()
topAlg.MultiLeptonFilter.Ptcut = 7000.
topAlg.MultiLeptonFilter.NLeptons = 3
#Also, Eta is cut at 10.0 by default

try:
  StreamEVGEN.RequireAlgs = ["MultiLeptonFilter"]
except Exception, e:
  pass

#######################################################
# FILTER EFFICIENCY
#######################################################
from MC10JobOptions.HerwigEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.131
from MC10JobOptions.SUSYEvgenConfig import evgenConfig
# 5000/34327=0.1457 (x0.9=0.131)
#==============================================================
#
# End of job options file
#
#####################################
