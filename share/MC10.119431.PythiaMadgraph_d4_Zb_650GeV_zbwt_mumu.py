###############################################################
#
# Job options file
#
#==============================================================

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

# --> use the following pythia tune if CTEQ6l1 pdf has been used
include ( "MC10JobOptions/MC10_PythiaMC09p_Common.py" )

Pythia.PythiaCommand += [ "pyinit user madgraph",
                          "pydat1 parj 90 20000", # Turn off FSR.
                          "pydat3 mdcy 15 1 0"    # Turn off tau decays.
                          ]
# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )
# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

from MC10JobOptions.MadGraphEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'group.phys-gener.madgraph.119431.d4_Zb.TXT.mc10_v1'
evgenConfig.efficiency = 0.9

#==============================================================
#
# End of job options file
#
###############################################################
