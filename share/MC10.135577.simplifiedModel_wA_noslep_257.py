## Herwig++ job option file for Susy 2-parton -> 2-sparticle processes

## Get a handle on the top level algorithms' sequence
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

try:
     include ( "MC10JobOptions/MC10_Herwigpp_Common.py" )
except NameError:
     cmds =  ""

## Add Herwig++ parameters for this process
cmds += """
## Generate the process in MSSM equivalent to 2-parton -> 2-sparticle processes in Fortran Herwig
## Read the MSSM model details
read MSSM.model
cd /Herwig/NewPhysics 

#incoming parton
insert HPConstructor:Incoming 0 /Herwig/Particles/g
insert HPConstructor:Incoming 1 /Herwig/Particles/u
insert HPConstructor:Incoming 2 /Herwig/Particles/ubar
insert HPConstructor:Incoming 3 /Herwig/Particles/d
insert HPConstructor:Incoming 4 /Herwig/Particles/dbar
insert HPConstructor:Incoming 5 /Herwig/Particles/s
insert HPConstructor:Incoming 6 /Herwig/Particles/sbar
insert HPConstructor:Incoming 7 /Herwig/Particles/c
insert HPConstructor:Incoming 8 /Herwig/Particles/cbar

#outgoing sparton (squark)
insert HPConstructor:Outgoing 0 /Herwig/Particles/~chi_1+
insert HPConstructor:Outgoing 0 /Herwig/Particles/~chi_20
set HPConstructor:Processes Exclusive

#switch off decay modes
set /Herwig/Particles/W+/W+->dbar,u;:OnOff Off
set /Herwig/Particles/W+/W+->sbar,c;:OnOff Off
set /Herwig/Particles/W+/W+->sbar,u;:OnOff Off
set /Herwig/Particles/W+/W+->dbar,c;:OnOff Off
set /Herwig/Particles/W+/W+->bbar,c;:OnOff Off

set /Herwig/Particles/W-/W-->d,ubar;:OnOff Off
set /Herwig/Particles/W-/W-->s,cbar;:OnOff Off
set /Herwig/Particles/W-/W-->s,ubar;:OnOff Off
set /Herwig/Particles/W-/W-->d,cbar;:OnOff Off
set /Herwig/Particles/W-/W-->b,cbar;:OnOff Off

set /Herwig/Particles/Z0/Z0->d,dbar;:OnOff Off
set /Herwig/Particles/Z0/Z0->s,sbar;:OnOff Off
set /Herwig/Particles/Z0/Z0->b,bbar;:OnOff Off
set /Herwig/Particles/Z0/Z0->u,ubar;:OnOff Off
set /Herwig/Particles/Z0/Z0->c,cbar;:OnOff Off
set /Herwig/Particles/Z0/Z0->nu_e,nu_ebar;:OnOff Off
set /Herwig/Particles/Z0/Z0->nu_mu,nu_mubar;:OnOff Off
set /Herwig/Particles/Z0/Z0->nu_tau,nu_taubar;:OnOff Off

## Read the SUSY spectrum file (SLHA format)
setup MSSM/Model simplifiedModel_wA_nosl.000257.txt
"""

## Set the command vector
topAlg.Herwigpp.Commands = cmds.splitlines()

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.HerwigppEvgenConfig import evgenConfig
from MC10JobOptions.SUSYEvgenConfig import evgenConfig

#==============================================================
#
# End of job options file
#
###############################################################
evgenConfig.auxfiles +=[ "simplifiedModel_wA_nosl*.txt"]
evgenConfig.auxfiles +=[ "MSSM.model"]
