###############################################################
#
# Job options file
#
# WH, W->all, H->WW where not (W->lepnu/W->qq or both W->lepnu) (lep=e, mu and tau)
#
# Responsible person(s) Junichi TANAKA
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

# ... Pythia
include ( "MC10JobOptions/MC10_Pythia_Common.py" )
Pythia.PythiaCommand += [
     "pysubs msel 0",
     "pydat1 parj 90 20000",  # Turn off FSR.
     "pydat3 mdcy 15 1 0",    # Turn off tau decays.
     "pysubs msub 26 1",      # WH production
     "pydat2 pmas 25 1 135", # Higgs mass
     "pydat2 pmas 25 2 0.00614", # Higgs width
     "pysubs ckin 45 2.",
     "pysubs ckin 47 2.",
     "pydat3 mdme 190 1  1",  # W-decay 190-209
     "pydat3 mdme 191 1  1",
     "pydat3 mdme 192 1  1",
     "pydat3 mdme 193 1 -1",
     "pydat3 mdme 194 1  1",
     "pydat3 mdme 195 1  1",
     "pydat3 mdme 196 1  1",
     "pydat3 mdme 197 1 -1",
     "pydat3 mdme 198 1  1",
     "pydat3 mdme 199 1  1",
     "pydat3 mdme 200 1  1",
     "pydat3 mdme 201 1 -1",
     "pydat3 mdme 202 1 -1",
     "pydat3 mdme 203 1 -1",
     "pydat3 mdme 204 1 -1",
     "pydat3 mdme 205 1 -1",
     "pydat3 mdme 206 1  1",
     "pydat3 mdme 207 1  1",
     "pydat3 mdme 208 1  1",
     "pydat3 mdme 209 1 -1",
     "pydat3 mdme 210 1  0",  # H-decay 210-226 (SM)
     "pydat3 mdme 211 1  0",
     "pydat3 mdme 212 1  0",
     "pydat3 mdme 213 1  0",
     "pydat3 mdme 214 1  0",
     "pydat3 mdme 215 1  0",
     "pydat3 mdme 216 1 -1",
     "pydat3 mdme 217 1 -1",
     "pydat3 mdme 218 1  0",
     "pydat3 mdme 219 1  0",
     "pydat3 mdme 220 1  0",  # H->tautau
     "pydat3 mdme 221 1 -1",
     "pydat3 mdme 222 1  0",
     "pydat3 mdme 223 1  0",  # H->gamgam
     "pydat3 mdme 224 1  0",  # H->gamZ
     "pydat3 mdme 225 1  0",  # H->ZZ
     "pydat3 mdme 226 1  1"   # H->WW
     ]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

# ... Filter H->VV->Children
from GeneratorFilters.GeneratorFiltersConf import HtoVVFilter
topAlg += HtoVVFilter("HtoVVFilter1")
HtoVVFilter1 = topAlg.HtoVVFilter1
HtoVVFilter1.PDGGrandParent = 25
HtoVVFilter1.PDGParent = 24
HtoVVFilter1.PDGChild1 = [11,12,13,14,15,16]
HtoVVFilter1.PDGChild2 = [1,2,3,4,5,6]

topAlg += HtoVVFilter("HtoVVFilter2")
HtoVVFilter2 = topAlg.HtoVVFilter2
HtoVVFilter2.PDGGrandParent = 25
HtoVVFilter2.PDGParent = 24
HtoVVFilter2.PDGChild1 = [11,12,13,14,15,16]
HtoVVFilter2.PDGChild2 = [11,12,13,14,15,16]

try:
    StreamEVGEN.VetoAlgs  = [ "HtoVVFilter1" ]
    StreamEVGEN.VetoAlgs += [ "HtoVVFilter2" ]
except Exception, e:
    pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.95
#==============================================================
#
# End of job options file
#
###############################################################
