###############################################################
#
# Job options file
#
# Alpgen Z->ee+0parton (exclusive) no filtering cut applied
#
# Responsible person(s)
#	Louis Helary
#==============================================================
# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 

try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common.py" )
  if runArgs.ecmEnergy == 14000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_14TeV.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig


Herwig.HerwigCommand += [ "iproc alpgen",
                          "taudec TAUOLA",
                        ]
# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------



#Information on sample:
#Filter efficiency  = 1.0000
#MLM matching efficiency = 98.9
#Number of Matrix Elements in input file  = 640
#Alpgen cross section = 687.1 pb
#Herwig cross section = Alpgen cross section * eff(MLM) =680.6
#Cross section after filtering = 680.6
#Lumi/500 events post-filter = 500/eff(Filter)/XS(jimmy) =

# Filter efficiency estimate below reduced by 10% to produce 555 events on average,
# of which only 500 will be used in further processing

from MC10JobOptions.AlpgenEvgenConfig import evgenConfig 
 
# inputfilebase
evgenConfig.inputfilebase = 'MC10.107606.AlpgenJimmyZeeNp0_ptJets50'
evgenConfig.efficiency = 0.7812



#==============================================================
#
# End of job options file
#
###############################################################



