###############################################################
#
# Job options file for dijet production with pythia
#
# author: C. Gwenlan (June'10); based on L. Mijovic's studies
# ISR  systematics sample: more hard ISR activity, external LO process + PYTHIA                                
# PARP(67) and PARP(64) are varied                                                                             
# AMBT1 default values: PARP(67)=4., PARP(64)=1.                                                                
# PARP(67): controls suppression of ISR branchings above the coherence scale                                
#           PARP(67) was introduced in 6.4.19 for the new shower, see Pythia update notes for more info     
# PARP(64): multiplies ISR alpha_strong evolution scale, the effect is \propto 1/(lambda_ISR^2)             
#           see Pythia Manual for more info                                                                
#
# reference JO: MC10.105009.J0_pythia_jetjet.py
#===============================================================

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 


include ( "MC10JobOptions/MC10_Pythia_Common.py" )

Pythia.PythiaCommand +=[ "pypars parp 67 6.0" ]   
Pythia.PythiaCommand +=[ "pypars parp 64 0.25" ]  

#--------------------------------------------------------------
Pythia.PythiaCommand += [
			      "pysubs msel 0",
			      "pysubs ckin 3 8.",
			      "pysubs ckin 4 17.",
			      "pysubs msub 11 1",
			      "pysubs msub 12 1",
			      "pysubs msub 13 1",
			      "pysubs msub 68 1",
			      "pysubs msub 28 1",
			      "pysubs msub 53 1"]
                              
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
