#min bias sample with single muon filter
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 


include ( "MC10JobOptions/MC10_Pythia_Common.py" )

Pythia.PythiaCommand += [  "pysubs msel 1" ]

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig

evgenConfig.efficiency = 0.0002
evgenConfig.minevents = 50

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
# bugfix setting by Borut
try:
   svcMgr.EventSelector.EventsPerRun=2000000000
except:
   pass


from GeneratorFilters.GeneratorFiltersConf import MuonFilter

topAlg += MuonFilter()

MuonFilter = topAlg.MuonFilter
MuonFilter.Ptcut = 4000.0
MuonFilter.Etacut = 3.0

try:
     StreamEVGEN.RequireAlgs += [ "MuonFilter" ]
except Exception, e:
     pass


#==============================================================
#
# End of job options file
#
###############################################################
