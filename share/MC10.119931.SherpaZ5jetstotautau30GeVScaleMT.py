from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence('TopAlg')
from Sherpa_i.Sherpa_iConf import Sherpa_i
sherpa = Sherpa_i()

# set alpha_s scale factor in parton shower (value recommended by Sherpa authors for W/Z)
params="""
CSS_AS_FS_FAC=0.4
CSS_AS_IS_FAC=0.4
"""
sherpa.Parameters = params.strip().splitlines()

# set beam energy (interface currently sets it for ECM=7TeV; needs explicit changing in JO for other ECMs]
try:
  eBeam=runArgs.ecmEnergy/2.0
  sherpa.Parameters += [ "BEAM_ENERGY_1=%s" % eBeam,"BEAM_ENERGY_2=%s" % eBeam ]
except NameError:
  pass

#

#change scale to be MPerp 
sherpa.Parameters += [  "SCALES=VAR{MPerp2(p[2]+p[3])}"  ]

topAlg += sherpa
from MC10JobOptions.SherpaFEvgenConfig import evgenConfig

 dummy needed
evgenConfig.inputfilebase = 'sherpa'
try:
  if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.sherpa010203.118961.SherpaZ5jetstotautau30GeV_7TeV.TXT.v1'
  if runArgs.ecmEnergy == 8000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.sherpa010203.118961.SherpaZ5jetstotautau30GeV_8TeV.TXT.v1'
except NameError:
  pass

evgenConfig.efficiency = 0.9
evgenConfig.weighting = 0
