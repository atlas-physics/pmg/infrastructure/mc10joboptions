###############################################################
#
# Job options file
# Pythia8 W->enu (no filter)
# contact: Robindra Prabhu (prabhu@cern.ch) (April 2011)
#
#===============================================================

# ... Main generator : Pythia8

MessageSvc = Service( "MessageSvc" )
#MessageSvc.OutputLevel = 4

from AthenaCommon.AppMgr import ServiceMgr as svcMgr

import AthenaServices
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ("MC10JobOptions/MC10_Pythia8_Common.py")

#Pythia8.Commands += ["PartonLevel:FSR = off"] # turn off FSR
Pythia8.Commands += ["PartonLevel:FSR = on"] # turn on FSR (no Photons interface to Pythia8)
Pythia8.Commands += ["WeakSingleBoson:ffbar2W = on"] #create W bosons
Pythia8.Commands += ["24:onMode = off"] # switch off all W decays
Pythia8.Commands += ["24:onIfAny = 11"] # switch on W->enu
#Pythia8.Commands += ["15:onMode = off"] # turn off tau decays

# ... Tauola
#include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
#include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
# efficiency = 0.9 - no filtering
from MC10JobOptions.Pythia8EvgenConfig import evgenConfig
evgenConfig.efficiency = 0.95

#==============================================================
#
# End of job options file
#
###############################################################
