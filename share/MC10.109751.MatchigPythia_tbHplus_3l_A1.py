###############################################################
#
# Job options file
#
# Matchig / Pythia gg/gb->t[b]H+ with >=3 leptons
#                 (mostly H+->chargino/neutralino)
# based on input from Caleb Lampen
# A1: tan beta=7; mu=135 GeV; M2=210 GeV
#
# Responsible person(s)
#   June 3, 2009 : Martin Flechl (Martin.Flechl@cern.ch)
#                  
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

# ... Pythia
include ( "MC10JobOptions/MC10_Pythia_Common.py" )
Pythia.PythiaCommand += [
                         "pyinit user matchig",  #matchig user process
                         "pysubs msel 0",
                         "pysubs msub 161 1",    #f + g -> f' + H+/-
                         "pysubs msub 401 1",    #g + g -> t + b + H+/-
                         "pysubs msub 402 1",    #q + qbar -> t + b + H+/-
                         "pypars mstp 129 1000", #number of attempts to find highest xsec with 2->3 variables
                         "pypars mstp 2 2",      #second order running alpha strong
                         "pypars mstp 4 1",      #Higgs sector. =1: neutral Higgs couplings set in PARU blocks
                        ]

#MSSM properties
Pythia.PythiaCommand += [
                         "pymssm rmss 2 210.0",  #M2
                         "pymssm rmss 4 135",    #mu
                         "pymssm rmss 5 7",      #tan beta
                         "pymssm rmss 19 390",   #mA
#
                         "pymssm imss 1 1",      #general MSSM (parameters set by RMSS)
                         "pymssm rmss 1 105.0",  #SU(1) gaugino mass
                         "pymssm rmss 3 800.0",  #SU(3) gluino mass parameter
                         "pymssm rmss 6 150.0",  #left slepton physical mass
                         "pymssm rmss 7 150.0",  #right slepton physical mass
                         "pymssm rmss 8 1000.0", #left squark mass
                         "pymssm rmss 9 1000.0", #right squark mass
                         "pymssm rmss 10 1000.0",#left squark mass (3rd generation)
                         "pymssm rmss 11 1000.0",#right sbottom mass
                         "pymssm rmss 12 1000.0",#right stop mass
                         "pymssm rmss 13 250.0", #left stau mass
                         "pymssm rmss 14 250.0", #right stau mass
                         "pymssm rmss 15 2000",  #top trilinear coupling Ab
                         "pymssm rmss 16 2000",  #tau trilinear coupling At
                         "pymssm rmss 17 0",     #tau trilinear coupling Atau
                        ]

#For TAUOLA/PHOTOS
Pythia.PythiaCommand += [
                         "pydat1 parj 90 20000",   # Turn off FSR.
                         "pydat3 mdcy 15 1 0"      # Turn off tau decays.
                        ]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#######################################

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
topAlg += MultiLeptonFilter()

MultiLeptonFilter = topAlg.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 7000
MultiLeptonFilter.NLeptons = 3

try:
  StreamEVGEN.RequireAlgs +=  [ "MultiLeptonFilter" ]
except Exception, e:
  pass
        

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.11     #some safety, actual value is around 0.123

#==============================================================
#
# End of job options file
#
###############################################################
