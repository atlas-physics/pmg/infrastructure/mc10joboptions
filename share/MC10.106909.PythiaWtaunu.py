###############################################################
#
# Job options file
#
# Pythia W->taunu <s>(mW>40GeV)</s> w/o EF
# - 21 March, 2008 : remove "pysubs ckin 1 40.0"
#   for the consistency with DS5107.
#   However, this does not affect results. (just for the consistency)
#
#==============================================================
# ... Main generator : Pythia
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )
Pythia.PythiaCommand +=[ "pysubs msel 12",
                         "pydat1 parj 90 20000", # Turn off FSR.
                         "pydat3 mdcy 15 1 0",   # Turn off tau decays.
#                         "pysubs ckin 1 40.0",     # Lower invariant mass.
                         "pydat3 mdme 190 1 0",
                         "pydat3 mdme 191 1 0",
                         "pydat3 mdme 192 1 0",
                         "pydat3 mdme 193 1 0",
                         "pydat3 mdme 194 1 0",
                         "pydat3 mdme 195 1 0",
                         "pydat3 mdme 196 1 0",
                         "pydat3 mdme 197 1 0",
                         "pydat3 mdme 198 1 0",
                         "pydat3 mdme 199 1 0",
                         "pydat3 mdme 200 1 0",
                         "pydat3 mdme 201 1 0",
                         "pydat3 mdme 202 1 0",
                         "pydat3 mdme 203 1 0",
                         "pydat3 mdme 204 1 0",
                         "pydat3 mdme 205 1 0",
                         "pydat3 mdme 206 1 0",    # Switch for W->enu.
                         "pydat3 mdme 207 1 0",    # Switch for W->munu.
                         "pydat3 mdme 208 1 1",    # Switch for W->taunu.
                         "pydat3 mdme 209 1 0"
                         ]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.90
#==============================================================
#
# End of job options file
#
###############################################################
