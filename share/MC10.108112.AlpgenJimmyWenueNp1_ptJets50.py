###############################################################
#
# Job options file
#
# Alpgen W->enue+1parton (exclusive) no filtering cut applied,
# 
#
# Responsible person(s)
#	Louis Helary
#==============================================================
# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 

try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common.py" )
  if runArgs.ecmEnergy == 14000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_14TeV.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig


Herwig.HerwigCommand += [ "iproc alpgen",
                          "taudec TAUOLA",
                        ]
# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------



#Information on sample:
#Filter efficiency  = 1.0000
#MLM matching efficiency = 0.564
#Number of Matrix Elements in input file  = 1000
#Alpgen cross section = 559.99 pb
#Herwig cross section = Alpgen cross section * eff(MLM) = 315.8pb
#Cross section after filtering = 315.8pb (no filtering)
#Lumi/500 events post-filter = 500/eff(Filter)/XS(jimmy) =

# Filter efficiency estimate below reduced by 10% to produce 555 events on average,
# of which only 500 will be used in further processing

from MC10JobOptions.AlpgenEvgenConfig import evgenConfig 
 
# inputfilebase
evgenConfig.inputfilebase = 'MC10.108112.AlpgenJimmyWenueNp1_ptJets50'
evgenConfig.efficiency = 0.95



#==============================================================
#
# End of job options file
#
###############################################################



