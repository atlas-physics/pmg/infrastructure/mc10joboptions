
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

#--------------------------------------------------------------
# File prepared by Rashid Mehdiyev [rmehdi@lps.umontreal.ca] 
# Excited quark production with pythia 
#--------------------------------------------------------------

######################################################
#                                                    #
#           Algorithm Private Options                #
#                                                    #
######################################################

#Excited Quark Mass (in GeV)
M_ExQ = 1500.0

#Mass Scale parameter (Lambda, in GeV)
M_Lam = 5000.0

include ( "MC10JobOptions/MC10_Pythia_Common.py" )
Pythia.PythiaCommand +=["pysubs msel 0",
                        "pysubs msub 147 1",
                        "pysubs msub 148 1",
                        "pydat2 pmas 4000001 1 "+str(M_ExQ),
                        "pydat2 pmas 4000002 1 "+str(M_ExQ),
                        "pytcsm rtcm 41 "+str(M_Lam),
                       # select ExQ decays to gamma + quark
                        "pydat3 mdme 4145 1 0",
                        "pydat3 mdme 4146 1 1",
                        "pydat3 mdme 4147 1 0",
                        "pydat3 mdme 4148 1 0",
                        "pydat3 mdme 4149 1 0",
                        "pydat3 mdme 4150 1 1",
                        "pydat3 mdme 4151 1 0",
                        "pydat3 mdme 4152 1 0",
                        "pydat3 mdcy 15 1 0",     # Turn off tau decays. 
                        "pydat1 parj 90 20000",   # Turn off FSR.
                        ]


# Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )
# Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import PhotonFilter
topAlg += PhotonFilter()

PhotonFilter = topAlg.PhotonFilter
PhotonFilter.Ptcut = 10000.
PhotonFilter.Etacut = 2.7
PhotonFilter.NPhotons = 1

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9

#==============================================================
#
# End of job options file
#
###############################################################
