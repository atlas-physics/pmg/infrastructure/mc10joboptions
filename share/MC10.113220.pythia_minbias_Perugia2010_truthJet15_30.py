#######################################
# Perugia2010 Pythia Minbias (same as 105001, but with Perugia2010 tune)
# Adapted by S. Zenz to cut on a leading truth jet with 15 GeV < pT < 30 GeV
#######################################

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_PythiaPerugia2010_Common.py" )

Pythia.PythiaCommand += [  "pysubs msel 1" ]

from GeneratorFilters.GeneratorFiltersConf import TruthJetFilter

from JetRec.JetGetters import *
akt6=make_StandardJetGetter('AntiKt',0.6,'Truth')
akt6alg = akt6.jetAlgorithmHandle()
akt6alg.AlgTools["JetFinalPtCut"].MinimumSignal = 4.*GeV
akt6alg.AlgTools["JetFinalPtCut"].UseTransverseMomentum = True

from GeneratorFilters.GeneratorFiltersConf import TruthJetFilter

topAlg += TruthJetFilter("TruthJetFilterLow")
topAlg.TruthJetFilterLow.OutputLevel=INFO
topAlg.TruthJetFilterLow.TruthJetContainer="AntiKt6TruthJets"
topAlg.TruthJetFilterLow.Njet = 1
topAlg.TruthJetFilterLow.NjetMinPt = 15.*GeV
topAlg.TruthJetFilterLow.NjetMaxEta = 2.5
topAlg.TruthJetFilterLow.jet_pt1 = 15.*GeV

topAlg += TruthJetFilter("TruthJetFilterHigh")
topAlg.TruthJetFilterHigh.OutputLevel=INFO
topAlg.TruthJetFilterHigh.TruthJetContainer="AntiKt6TruthJets"
topAlg.TruthJetFilterHigh.Njet = 1
topAlg.TruthJetFilterHigh.NjetMinPt = 30.*GeV
topAlg.TruthJetFilterHigh.NjetMaxEta = 2.5
topAlg.TruthJetFilterHigh.jet_pt1 = 30.*GeV

try:
  StreamEVGEN.RequireAlgs += ["TruthJetFilterLow"]
  StreamEVGEN.VetoAlgs += ["TruthJetFilterHigh"]
except Exception, e:
  pass  

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig

evgenConfig.efficiency = 0.024


#==============================================================
#
# End of job options file
#
###############################################################

