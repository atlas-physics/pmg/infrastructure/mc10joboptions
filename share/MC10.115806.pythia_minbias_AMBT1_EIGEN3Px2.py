###############################################################
#
# Job options file for min bias
#
# author: C. Gwenlan (June'10)
# AMBT1 eigentune 3+ (2-sigma) for systematic studies
# reference JO: MC10.105001.pythia_minbias.py
#===============================================================

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

#--------------------------------------------------------------
# AMBT1 systematic variation: eigentune 3+ (2-sigma)
Pythia.PythiaCommand += [
			      "pypars parp 77 0.94191",
                              "pypars parp 78 0.45867",
                              "pypars parp 82 2.26510",
                              "pypars parp 84 0.63362",
                              "pypars parp 90 0.21056"]

#--------------------------------------------------------------

Pythia.PythiaCommand += [  "pysubs msel 1" ]

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig

evgenConfig.efficiency = 0.9


#==============================================================
#
# End of job options file
#
###############################################################

