###############################################################
#
# Job options file
#
# Pythia W->taunu(only hadronic decay) w/o GEF
#
# Responsible person(s)
#   11 June, 2008-xx xxx, 20xx: Junichi TANAKA (Junichi.Tanaka@cern.ch)
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

# ... Pythia
include ( "MC10JobOptions/MC10_Pythia_Common.py" )
Pythia.PythiaCommand += [ "pysubs msel 0",
                          "pysubs msub 2 1",
                          "pydat1 parj 90 20000", # Turn off photon rad.
                          "pydat3 mdcy 15 1 0",   # Turn off tau decays.
                          "pydat3 mdme 190 1 0",
                          "pydat3 mdme 191 1 0",
                          "pydat3 mdme 192 1 0",
                          "pydat3 mdme 193 1 0",
                          "pydat3 mdme 194 1 0",
                          "pydat3 mdme 195 1 0",
                          "pydat3 mdme 196 1 0",
                          "pydat3 mdme 197 1 0",
                          "pydat3 mdme 198 1 0",
                          "pydat3 mdme 199 1 0",
                          "pydat3 mdme 200 1 0",
                          "pydat3 mdme 201 1 0",
                          "pydat3 mdme 202 1 0",
                          "pydat3 mdme 203 1 0",
                          "pydat3 mdme 204 1 0",
                          "pydat3 mdme 205 1 0",
                          "pydat3 mdme 206 1 0",    # Switch for W->enu.
                          "pydat3 mdme 207 1 0",    # Switch for W->munu.
                          "pydat3 mdme 208 1 1",    # Switch for W->taunu.
                          "pydat3 mdme 209 1 0"
                          ]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment_HadronicDecay.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.90
#==============================================================
#
# End of job options file
#
###############################################################
