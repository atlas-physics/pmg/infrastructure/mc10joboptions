###############################################################
#
# Job options file
#
# bbA, A->tautau->lh with MA=300GeV, tanBeta=20 by Sherpa
#
# Responsible person(s)
#   13 Aug, 2008-xx xxx, 20xx: Wolfgang Mader (Wolfgang.Mader@cern.ch)
#
#==============================================================
import AthenaCommon.AtlasUnixGeneratorJob
from AthenaCommon.AppMgr import ServiceMgr as svcMgr

#load relevant libraries
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

# ... Sherpa
from Sherpa_i.Sherpa_iConf import ReadSherpa_i
sherpa = ReadSherpa_i()
sherpa.Files = [ "sherpa.evts" ]
topAlg += sherpa

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.SherpaEvgenConfig import evgenConfig
 
# inputfilebase
evgenConfig.inputfilebase = 'Sherpa10103.109123.SherpabbAtautaulhMA300TB20_v2'
evgenConfig.efficiency = 0.95
#==============================================================
#
# End of job options file
#
###############################################################
