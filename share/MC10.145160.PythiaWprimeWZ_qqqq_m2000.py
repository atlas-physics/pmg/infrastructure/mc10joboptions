###############################################################
#
# Start of W' --> WZ --> jjjj job options file
# David Adams
# September 2011
#
# SSM W' production, i.e. the default extended gauge couplings of Altarelli
# (Z. Phys C 45, 109 (1989)). These give B_L = 8.1% (for each lepton) and
# B_WZ = 1.3%. Increase PARU(135) (default 1) to increase the WZ partial width.
#
###############################################################


import AthenaCommon.AtlasUnixGeneratorJob

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

Pythia.PythiaCommand += [ "pysubs msel 0" ]

# Wprime production
Pythia.PythiaCommand += [ "pysubs msub 142 1" ]

# Set mass
Pythia.PythiaCommand += [ "pydat2 pmas 34 1 2000.0" ]

Pythia.PythiaCommand += [ "pydat3 mdme 311 1 0",   # d ubar
                          "pydat3 mdme 312 1 0",   # d cbar
                          "pydat3 mdme 313 1 0",   # d tbar
                          "pydat3 mdme 314 1 -1",  # d t'bar
                          "pydat3 mdme 315 1 0",   # s ubar
                          "pydat3 mdme 316 1 0",   # s cbar
                          "pydat3 mdme 317 1 0",   # s tbar
                          "pydat3 mdme 318 1 -1",  # s t'bar
                          "pydat3 mdme 319 1 0",   # b ubar
                          "pydat3 mdme 320 1 0",   # b cbar
                          "pydat3 mdme 321 1 0",   # b tbar
                          "pydat3 mdme 322 1 -1",  # b t'bar
                          "pydat3 mdme 323 1 -1",  # b' ubar
                          "pydat3 mdme 324 1 -1",  # b' cbar
                          "pydat3 mdme 325 1 -1",  # b' tbar
                          "pydat3 mdme 326 1 -1",  # b' t'bar
                          "pydat3 mdme 327 1 0",   # e nue
                          "pydat3 mdme 328 1 0",   # mu numu
                          "pydat3 mdme 329 1 0",   # tau nutau
                          "pydat3 mdme 330 1 -1",  # tau' nutau'
                          "pydat3 mdme 331 1 1",   # W Z
                          "pydat3 mdme 332 1 0",   # ? ?
                          "pydat3 mdme 333 1 -1"]  # ? ?

# cutoff for QED FSR in Pythia to 20000 GeV (='infinity', photos takes care of it).
#
#  In order to prevent double counting in Pythia when PHOTOS is used
#
Pythia.PythiaCommand += [ "pydat1 parj 90 20000" ]
#
#  Tell Pythia NOT to decay taus
#
Pythia.PythiaCommand += [ "pydat3 mdcy 15 1 0" ]


#!
#! W decays to quarks
#!
Pythia.PythiaCommand += [ "pydat3 mdme 190 1 1",   # d u~
                          "pydat3 mdme 191 1 1",   # d cbar
                          "pydat3 mdme 192 1 1",   # d tbar
                          "pydat3 mdme 193 1 -1",  # d t'bar
                          "pydat3 mdme 194 1 1",   # s ubar
                          "pydat3 mdme 195 1 1",   # s cbar
                          "pydat3 mdme 196 1 1",   # s tbar
                          "pydat3 mdme 197 1 -1",  # s t'bar
                          "pydat3 mdme 198 1 1",   # b ubar
                          "pydat3 mdme 199 1 1",   # b cbar
                          "pydat3 mdme 200 1 1",   # b tbar
                          "pydat3 mdme 201 1 -1",  # b t'bar
                          "pydat3 mdme 202 1 -1",  # b' ubar
                          "pydat3 mdme 203 1 -1",  # b' cbar
                          "pydat3 mdme 204 1 -1",  # b' tbar
                          "pydat3 mdme 205 1 -1" ]  # b' t'bar


#!
#! W decays to leptons
#!
Pythia.PythiaCommand += [ "pydat3 mdme 206 1 0",   # e nu_e
                          "pydat3 mdme 207 1 0",   # mu nu_mu
                          "pydat3 mdme 208 1 0",   # tau nu_tau
                          "pydat3 mdme 209 -1 0" ]  # tau' nu_tau'

 

#!
#! Z decays to quarks
#!
Pythia.PythiaCommand += [ "pydat3 mdme 174 1 1",   # d d~
                          "pydat3 mdme 175 1 1",   # u u~
                          "pydat3 mdme 176 1 1",   # s s~
                          "pydat3 mdme 177 1 1",   # c c~
                          "pydat3 mdme 178 1 1",   # b b~
                          "pydat3 mdme 179 1 1" ]   # t t~


#!
#! Z decays to leptons
#!
Pythia.PythiaCommand += [ "pydat3 mdme 182 1 0",   # e -e+
                          "pydat3 mdme 183 1 0",   # nu_e nu_ebar
                          "pydat3 mdme 184 1 0",   # mu- mu+
                          "pydat3 mdme 185 1 0",   # nu_mu nu_mubar
                          "pydat3 mdme 186 1 0",   # tau- tau+
                          "pydat3 mdme 187 1 0" ]   # nu_tau nu_taubar

 
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

  
## ... Photos
 
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )



#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.95
