###############################################################
#
# Job options file for dijet production with pythia
#
# author: C. Gwenlan (June'10)
# AMBT1 "MARKUS1" tune - for systematic studies
# reference JO: MC10.105015.J6_pythia_jetjet.py
#===============================================================

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

#--------------------------------------------------------------
# AMBT1 systematic variation: MARKUS1
Pythia.PythiaCommand += [
                              "pypars parp 84 0.45",
                              "pypars parp 90 0.193"]

#--------------------------------------------------------------
Pythia.PythiaCommand += [
			      "pysubs msel 0",
			      "pysubs ckin 3 560.",
			      "pysubs ckin 4 1120.",
			      "pysubs msub 11 1",
			      "pysubs msub 12 1",
			      "pysubs msub 13 1",
			      "pysubs msub 68 1",
			      "pysubs msub 28 1",
			      "pysubs msub 53 1"]

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
