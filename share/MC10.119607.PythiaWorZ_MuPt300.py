###############################################################
#
# Job options file
# Created by David Adams
# April 2011
#
# Generate W --> mu nu, W --> tau nu, Z --> mu mu, and Z --> tau tau
# with pT_mu > 300 GeV.
# Intended as background for Wprime --> mu nu.
# Note invariant mass threshold of 30 GeV to get well below W/Z resonances
# but avoid gamma*.
#
#===============================================================

# ... Main generator : Pythia
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_Pythia_Common.py" )
Pythia.PythiaCommand += [ "pysubs msel 0",          # Users decay choice.
                         "pydat1 parj 90 20000", # Turn off FSR.
                         "pydat3 mdcy 15 1 0",     # Turn off tau decays.
                         "pysubs msub 1 1",        # Create Z bosons.
                         "pysubs msub 2 1",        # Create W bosons.
                         "pysubs ckin 1 30.0",     # Lower invariant mass.
                         "pydat3 mdme 174 1 0",
                         "pydat3 mdme 175 1 0",
                         "pydat3 mdme 176 1 0",
                         "pydat3 mdme 177 1 0",
                         "pydat3 mdme 178 1 0",
                         "pydat3 mdme 179 1 0",
                         "pydat3 mdme 182 1 0",    # Switch for Z->ee.
                         "pydat3 mdme 183 1 0",
                         "pydat3 mdme 184 1 1",    # Switch for Z->mumu.
                         "pydat3 mdme 185 1 0",
                         "pydat3 mdme 186 1 1",    # Switch for Z->tautau.
                         "pydat3 mdme 187 1 0",
                         "pydat3 mdme 190 1 0",
                         "pydat3 mdme 191 1 0",
                         "pydat3 mdme 192 1 0",
                         "pydat3 mdme 194 1 0",
                         "pydat3 mdme 195 1 0",
                         "pydat3 mdme 196 1 0",
                         "pydat3 mdme 198 1 0",
                         "pydat3 mdme 199 1 0",
                         "pydat3 mdme 200 1 0",
                         "pydat3 mdme 206 1 0",    # Switch for W->enu.
                         "pydat3 mdme 207 1 1",    # Switch for W->munu.
                         "pydat3 mdme 208 1 1"]    # Switch for W->taunu.

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

from GeneratorFilters.GeneratorFiltersConf import MuonFilter
topAlg += MuonFilter()

MuonFilter = topAlg.MuonFilter
MuonFilter.Ptcut = 300000.
MuonFilter.Etacut = 10.0

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------

try:
     StreamEVGEN.RequireAlgs +=  [ "MuonFilter" ]
except Exception, e:
     pass
#-------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
# MC10 measurement: eff = 0.000007, sigma*eff = 0.13 pb
# Processing time: 1.4 events/hr
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.000007
#  ------------- Number of events requested  -------------
evgenConfig.minevents = 50
#==============================================================
#
# End of job options file
#
###############################################################
