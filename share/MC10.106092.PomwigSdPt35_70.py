###############################################################
#
# Job options file: Pomwig SD
#
# Process description: Dijet production in single diffraction,
#                      jet pt in bin 35-70 GeV
#
# Author: Vojtech Juranek (vojtech.juranek@cern.ch)
#
# Tested on Rel.: 15.0.0
#
#==============================================================

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg")

from Pomwig_i.Pomwig_iConf import Pomwig
topAlg +=Pomwig()
topAlg.Pomwig.HerwigCommand = ["iproc 11500",         # dijet production (QCD 2->2)
                            "modpdf1 10042",          # PDFLIB parton set
                            "modpdf2 -1",             # PDFLIB parton set
                            "autpdf HWLHAPDF",   
                            "msflag 0",               # turn off UE  
                            "beam1type P         ", 
                            "beam2type E-        ",   # diffractive proton
                            "beam1energy 5000.0",
                            "beam2energy 5000.0",
                            "ptmin 35.0",             # minimum p_T
                            "ptmax 70.0"              # maximum p_T
                            ]

topAlg.Pomwig.nstru=14   # Pomeron structure functions H1 2006 
topAlg.Pomwig.ifit=2     # Fit B Pomeron

# Cross section (NB): 1857.07
# Rel.: 15.0.0.3, 5556 events
from MC10JobOptions.PomwigEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
