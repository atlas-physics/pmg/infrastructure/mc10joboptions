
#--------------------------------------------------------------
# Author  : Leonardo Carminati
#
# Purpose : Inclusive di-photon sample (brem+hard process). 
#
#--------------------------------------------------------------

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

Pythia.PythiaCommand += [
			      "pysubs msel 0",
			      "pysubs ckin 3  12.",
			      "pysubs msub 14 1",
			      "pysubs msub 29 1",
                              "pysubs msub 18 1",
                              "pysubs msub 114 1"
]

#-------------------------------------------------------------
# Filter
#-------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import DirectPhotonFilter
topAlg += DirectPhotonFilter()

DirectPhotonFilter = topAlg.DirectPhotonFilter
DirectPhotonFilter.Ptcut = 14000.
DirectPhotonFilter.Etacut =  2.7
DirectPhotonFilter.NPhotons = 2

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------

try:
     StreamEVGEN.RequireAlgs +=  [ "DirectPhotonFilter" ]
except Exception, e:
     pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------

from MC10JobOptions.PythiaEvgenConfig import evgenConfig
# 500/915736*0.9=0.0005
evgenConfig.efficiency = 0.0005
evgenConfig.minevents = 500
#==============================================================
#
# End of job options file
#
###############################################################

