# Alpgen Z(->ee)+cc+1p
#--------------------------------------------------------------
#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------
# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 

try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common.py" )
  if runArgs.ecmEnergy == 14000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_14TeV.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig


Herwig.HerwigCommand += [ "iproc alpgen",
                          "taudec TAUOLA"
                        ]
# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
# no filter so far.

from MC10JobOptions.AlpgenEvgenConfig import evgenConfig
 
# inputfilebase
#evgenConfig.inputfilebase = 'alpgen.117601.ZeeccNp1_pt20_nofilter'
evgenConfig.inputfilebase = 'group10.phys-gener.alpgen.117601.ZeeccNp1_pt20_nofilter_7Tev.TXT.v1'

# 7TeV
# Filter efficiency  = 1.0
# MLM matching efficiency = 0.3487
# Alpgen cross section = 6.98464156+-0.00442795 pb
# Herwig cross section = Alpgen cross section * eff(MLM) = 2.4357 pb
# Integrated Luminosity = 2052.78966 pb-1
# Number of events in 4-vector file of Alpgen = 5000/(eff*0.8) = 20000 events
#
# 10TeV
# Filter efficiency  = 1.0
# MLM matching efficiency = 0.3258
# Alpgen cross section = 15.18440945+-0.00907735 pb
# Herwig cross section = Alpgen cross section * eff(MLM) = 4.947 pb
# Number of events in 4-vector file of Alpgen = 5000/(eff*0.8) = 20000 events
#
# Filter efficiency x safety factor = eff x 90% = 0.90

# 7 TeV - Information on sample 	117601	
# 7 TeV - Filter efficiency  = 	1.0000	
# 7 TeV - MLM matching efficiency = 	0.44	
# 7 TeV - Number of Matrix Elements in input file  = 	14750	
# 7 TeV - Alpgen cross section = 	12.5	 pb
# 7 TeV - Herwig cross section = Alpgen cross section * eff(MLM) = 	5.5	 pb
# 7 TeV - Cross section after filtering = 	5.5	 pb
# 7 TeV - Lumi/5000 events post-filter = 5000/eff(Filter)/XS(jimmy) = 	904.94	 pb-1
#		
# 7 TeV - Filter efficiency estimate below reduced by 10% to produce 5556 events on average,		
# 7 TeV - of which only 5000 will be used in further processing		

evgenConfig.efficiency = 0.90
evgenConfig.minevents=5000
#==============================================================
#
# End of job options file
#
###############################################################
