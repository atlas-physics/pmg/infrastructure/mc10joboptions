##############################################################
#
# Job options file
#
# Alpgen W->taunu+2parton (exclusive) with HH Cut,
#   1hadtau (12GeV, |eta|<2.7) and 2jets (15GeV, |eta|<5)
#
# Responsible person(s)
#   16 Dec, 2008-xx xxx, 20xx: Junichi TANAKA (Junichi.Tanaka@cern.ch)
#
#==============================================================
# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 

try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common.py" )
  if runArgs.ecmEnergy == 14000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_14TeV.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig


Herwig.HerwigCommand += [ "iproc alpgen",
                          "taudec TAUOLA",
                        ]
# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
# TruthJet filter
try:
    from JetRec.JetGetters import *
    c4=make_StandardJetGetter('Cone',0.4,'Truth')
    c4alg = c4.jetAlgorithmHandle()
    c4alg.JetFinalEtCut.MinimumSignal = 10.*GeV
except Exception, e:
    pass

from GeneratorFilters.GeneratorFiltersConf import TauFilter
topAlg += TauFilter()

from GeneratorFilters.GeneratorFiltersConf import VBFForwardJetsFilter
topAlg += VBFForwardJetsFilter()

TauFilter = topAlg.TauFilter
TauFilter.Ntaus     = 1
TauFilter.Ptcute    = 9000000.
TauFilter.EtaMaxe   = 2.5
TauFilter.Ptcutmu   = 9000000.
TauFilter.EtaMaxmu  = 2.5
TauFilter.Ptcuthad  = 12000.0
TauFilter.EtaMaxhad = 2.7

VBFForwardJetsFilter = topAlg.VBFForwardJetsFilter
VBFForwardJetsFilter.JetMinPt=15.*GeV
VBFForwardJetsFilter.JetMaxEta=5.0
VBFForwardJetsFilter.NJets=2
VBFForwardJetsFilter.Jet1MinPt=-1.*GeV
VBFForwardJetsFilter.Jet1MaxEta=5.0
VBFForwardJetsFilter.Jet2MinPt=-1.*GeV
VBFForwardJetsFilter.Jet2MaxEta=5.0
VBFForwardJetsFilter.UseOppositeSignEtaJet1Jet2=False
VBFForwardJetsFilter.MassJJ=-1.*GeV
VBFForwardJetsFilter.DeltaEtaJJ=-1.0
VBFForwardJetsFilter.TruthJetContainer="Cone4TruthJets"
VBFForwardJetsFilter.LGMinPt=15.*GeV
VBFForwardJetsFilter.LGMaxEta=2.5
VBFForwardJetsFilter.DeltaRJLG=0.05
VBFForwardJetsFilter.RatioPtJLG=0.3

try:
    StreamEVGEN.RequireAlgs = [ "TauFilter", "VBFForwardJetsFilter" ]
except Exception, e:
    pass

from MC10JobOptions.AlpgenEvgenConfig import evgenConfig
 
# inputfilebase
evgenConfig.inputfilebase = 'alpgenV213.106157.WtaunuNp2_10TeV'
evgenConfig.efficiency = 0.4271
#==============================================================
#
# End of job options file
#
###############################################################
#106157 MC10.106157.AlpgenJimmyWtaunuNp2HHCut.py nveto=5425 nevent=2107 nsave=1000 MLM=0.27974 +- 0.0051721 EF=0.474608 +- 0.0108787 EFjobO=.4271472 n for 500 = 4707.2
