###############################################################
#
# Job options file
#
# Sherpa bbH->mumu (mH=130GeV, tan(beta)=20) 
#
# Responsible person(s)
#   25 Jul, 2008-xx xxx, 20xx: Peter Steinbach (P.Steinbach@physik.tu-dresden.de)
#
#==============================================================
#--------------------------------------------------------------
# ATLAS default Application Configuration options
#--------------------------------------------------------------
#
from AthenaCommon.AppMgr import ServiceMgr as svcMgr
#
#load relevant libraries
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
#
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
from Sherpa_i.Sherpa_iConf import ReadSherpa_i
#
sherpa = ReadSherpa_i()
sherpa.Files = [ "sherpa.evts" ]
#
topAlg += sherpa
#
from MC10JobOptions.SherpaEvgenConfig import evgenConfig
 
# inputfilebase
evgenConfig.inputfilebase = 'sherpa10103.106362.SherpabbAmumuMA130TB20_v2'
evgenConfig.efficiency = 0.95
#
#==============================================================
#
# End of job options file
################################################################ 
