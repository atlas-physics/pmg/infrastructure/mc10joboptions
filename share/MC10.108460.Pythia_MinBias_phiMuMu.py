#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.032

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

#--------------------------------------------------------------
# File prepared by Ian Hinchliffe Nov 2005
#--------------------------------------------------------------
Pythia.PythiaCommand += [
			      "pysubs msel 0",
			      "pysubs ckin 3 35.",
			      "pysubs ckin 4 70.",
			      "pysubs msub 11 1",
			      "pysubs msub 12 1",
			      "pysubs msub 13 1",
			      "pysubs msub 68 1",
			      "pysubs msub 28 1",
			      "pysubs msub 53 1"]

Pythia.PythiaCommand += ["pydat3 mdme 656 1 0",    
                          "pydat3 mdme 657 1 0",
                          "pydat3 mdme 658 1 0",
                          "pydat3 mdme 659 1 0",
                          "pydat3 mdme 660 1 0",
                          "pydat3 mdme 661 1 0",
                          "pydat3 mdme 662 1 0",
                          "pydat3 mdme 663 1 0",
                          "pydat3 mdme 664 1 0",
                          "pydat3 mdme 665 1 1",
                          "pydat3 mdme 666 1 0" ]

from GeneratorFilters.GeneratorFiltersConf import BSignalFilter
topAlg += BSignalFilter()
#------- Muon Trigger Cuts --------
BSignalFilter = topAlg.BSignalFilter
#-------------- Level 1 Muon Cuts --------------------- 
BSignalFilter.LVL1MuonCutOn = True
BSignalFilter.LVL1MuonCutPT = 4000.0 
BSignalFilter.LVL1MuonCutEta = 2.5 
#-------------- Level 2 lepton cuts -------------------
# These will only function if LVL1 trigger used. 
BSignalFilter.LVL2MuonCutOn = True 
BSignalFilter.LVL2MuonCutPT = 2500.0 
BSignalFilter.LVL2MuonCutEta = 2.5
try:
     StreamEVGEN.RequireAlgs += ["BSignalFilter"]
except Exception, e:
     pass

#==============================================================
#
# End of job options file
#
###############################################################
