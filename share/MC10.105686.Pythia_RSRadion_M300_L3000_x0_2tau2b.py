#
# Prepared by: Chaouki Boulahouache to generate Radion decays.
#           Mass of the Radion is 300GeV and Width is 0.83GeV for
#           the case of Lambda_phi = 3TeV and ksi = 0.
#      This decay mode is  Radion --> h0 h0 --> 2b 2tau.
#
#--------------------------------------------------------------
# General Application Configuration options
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
#
# Full user control
# To be able to define my own process
include ( "MC10JobOptions/MC10_Pythia_Common.py" )
Pythia.PythiaCommand += [
			"pysubs msel 0",
                       # Processes and parameters
                        "pysubs msub 152 1",
                        "pydat2 pmas 35 1 300.", # H0 --> 35 
                        "pydat2 pmas 35 2 0.09", # H0 --> 35 
                        "pydat2 pmas 25 1 125.",  #  h0 --> 25 
                        # Z decays
                        "pydat3 mdme 174 1 0",#Z->qqbar
                        "pydat3 mdme 175 1 0",#Z->qqbar
                        "pydat3 mdme 176 1 0",#Z->qqbar
                        "pydat3 mdme 177 1 0",#Z->qqbar
                        "pydat3 mdme 178 1 0",#Z->qqbar
                        "pydat3 mdme 179 1 0",#Z->qqbar
                        "pydat3 mdme 182 1 0",#Z->ee
                        "pydat3 mdme 183 1 0",#Z->nue-nue
                        "pydat3 mdme 184 1 0",#Z->mumu
                        "pydat3 mdme 185 1 0",#Z->numu-numu
                        "pydat3 mdme 186 1 0",#Z->tautau
                        "pydat3 mdme 187 1 0",#Z->nutau-nutau
                        # W decays
                        "pydat3 mdme 190 1 0",#W->qq
                        "pydat3 mdme 191 1 0",#W->qq
                        "pydat3 mdme 192 1 0",#W->qq
                        "pydat3 mdme 194 1 0",#W->qq
                        "pydat3 mdme 195 1 0",#W->qq
                        "pydat3 mdme 196 1 0",#W->qq
                        "pydat3 mdme 198 1 0",#W->qq
                        "pydat3 mdme 199 1 0",#W->qq
                        "pydat3 mdme 200 1 0",#W->qq
                        "pydat3 mdme 206 1 0",#W->lv
                        "pydat3 mdme 207 1 0",#W->lv
                        "pydat3 mdme 208 1 0",#W->lv(tau)
                        # H decays
                        "pydat3 mdme 210 1 0",# h0->qqbar d
                        "pydat3 mdme 211 1 0",# h0->qqbar u
                        "pydat3 mdme 212 1 0",# h0->qqbar s
                        "pydat3 mdme 213 1 0",# h0->qqbar c 
                        "pydat3 mdme 214 1 1",# h0->qqbar b
                        "pydat3 mdme 215 1 0",# h0->qqbar t
                        "pydat3 mdme 218 1 0",# h0->ee
                        "pydat3 mdme 219 1 0",# h0->mumu
                        "pydat3 mdme 220 1 1",# h0->tautau
                        "pydat3 mdme 222 1 0",# h0->gg
                        "pydat3 mdme 223 1 0",# h0->YY
                        "pydat3 mdme 224 1 0",# h0->gZ
                        "pydat3 mdme 225 1 0",# h0->ZZ
                        "pydat3 mdme 226 1 0",# h0->WW
                        "pydat3 mdme 226 1 0",
                        "pydat3 mdme 227 1 0",
                        "pydat3 mdme 228 1 0",
                        "pydat3 mdme 229 1 0",
                        "pydat3 mdme 230 1 0",
                        "pydat3 mdme 231 1 0",
                        "pydat3 mdme 232 1 0",
                        "pydat3 mdme 233 1 0",
                        "pydat3 mdme 234 1 0",
                        "pydat3 mdme 235 1 0",
                        "pydat3 mdme 236 1 0",
                        "pydat3 mdme 237 1 0",
                        "pydat3 mdme 238 1 0",
                        "pydat3 mdme 239 1 0",
                        "pydat3 mdme 240 1 0",
                        "pydat3 mdme 241 1 0",
                        "pydat3 mdme 242 1 0",
                        "pydat3 mdme 243 1 0",
                        "pydat3 mdme 244 1 0",
                        "pydat3 mdme 245 1 0",
                        "pydat3 mdme 246 1 0",
                        "pydat3 mdme 247 1 0",
                        "pydat3 mdme 248 1 0",
                        "pydat3 mdme 249 1 0",
                        "pydat3 mdme 250 1 0",
                        "pydat3 mdme 251 1 0",
                        "pydat3 mdme 252 1 0",
                        "pydat3 mdme 253 1 0",
                        "pydat3 mdme 254 1 0",
                        "pydat3 mdme 255 1 0",
                        "pydat3 mdme 256 1 0",
                        "pydat3 mdme 257 1 0",
                        "pydat3 mdme 258 1 0",
                        "pydat3 mdme 259 1 0",
                        "pydat3 mdme 260 1 0",
                        "pydat3 mdme 261 1 0",
                        "pydat3 mdme 262 1 0",
                        "pydat3 mdme 263 1 0",
                        "pydat3 mdme 264 1 0",
                        "pydat3 mdme 265 1 0",
                        "pydat3 mdme 266 1 0",
                        "pydat3 mdme 267 1 0",
                        "pydat3 mdme 268 1 0",
                        "pydat3 mdme 269 1 0",
                        "pydat3 mdme 270 1 0",
                        "pydat3 mdme 271 1 0",
                        "pydat3 mdme 272 1 0",
                        "pydat3 mdme 273 1 0",
                        "pydat3 mdme 274 1 0",
                        "pydat3 mdme 275 1 0",
                        "pydat3 mdme 276 1 0",
                        "pydat3 mdme 277 1 0",
                        "pydat3 mdme 278 1 0",
                        "pydat3 mdme 279 1 0",
                        "pydat3 mdme 280 1 0",
                        "pydat3 mdme 281 1 0",
                        "pydat3 mdme 282 1 0",
                        "pydat3 mdme 283 1 0",
                        "pydat3 mdme 284 1 0",
                        "pydat3 mdme 285 1 0",
                        "pydat3 mdme 286 1 0",
                        "pydat3 mdme 287 1 0",
                        "pydat3 mdme 288 1 0",
                        "pydat3 mdme 334 1 0", # H0->ddbar
                        "pydat3 mdme 335 1 0", # H0->uubar
                        "pydat3 mdme 336 1 0", # H0->ssbar
                        "pydat3 mdme 337 1 0", # H0->ccbar
                        "pydat3 mdme 338 1 0", # H0->bbbar
                        "pydat3 mdme 339 1 0", # H0->ttbar
                        "pydat3 mdme 340 1 0", # H0->b'b'bar
                        "pydat3 mdme 341 1 0", # H0->t't'bar
                        "pydat3 mdme 342 1 0", # H0->ee
                        "pydat3 mdme 343 1 0", # H0->mumu
                        "pydat3 mdme 344 1 0", # H0->tautau
                        "pydat3 mdme 345 1 0", # H0->tau'tau'
                        "pydat3 mdme 346 1 0", # H0->gg
                        "pydat3 mdme 347 1 0", # H0->gammgamm
                        "pydat3 mdme 348 1 0", # H0->gammaz0
                        "pydat3 mdme 349 1 0", # H0->z0z0
                        "pydat3 mdme 350 1 0", # H0->ww
                        "pydat3 mdme 351 1 0", # H0->Z0h0
                        "pydat3 mdme 352 1 1", # H0->h0h0
                        "pydat3 mdme 353 1 0", # H0->W+H-
                        "pydat3 mdme 354 1 0", # H0->H+W-
                        "pydat3 mdme 355 1 0", # H0->Z0A0
                        "pydat3 mdme 356 1 0", # H0->h0A0
                        "pydat3 mdme 357 1 0", # H0->A0A0
		        "pyinit pylisti 12",  # Dumps all the particle and decay data after initialization.
                        "pyinit pylistf 1",
		        "pystat 1 3 4 5",
  		        "pyinit dumpr 1 5"]

## ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

## ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import ParentTwoChildrenFilter

# ParentTwoChildrenFilter 
topAlg += ParentTwoChildrenFilter()
ParentTwoChildrenFilter = ParentTwoChildrenFilter()
ParentTwoChildrenFilter.PDGParent = [25]
ParentTwoChildrenFilter.PDGChild = [5,15]
#ParentTwoChildrenFilter.OutputLevel = 3

try:
     StreamEVGEN.RequireAlgs = [ "ParentTwoChildrenFilter" ]
except Exception, e:
     pass

#
#-------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.15
