#-------------------------------------------------------------------------
#
# RPV displaced vertex with decay to muons
# 
# contact :  C. Horn, R. Bruneliere
#
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

Pythia.PythiaCommand += ["pysubs msel 0"]  # !
## Switch off Pythia color strgins, they don't work correctly for late decaying particles!
Pythia.PythiaCommand += ["pypars mstp 95 0"]
##
Pythia.PythiaCommand += ["pysubs msub 271 1"]  # squark pair production 
Pythia.PythiaCommand += ["pysubs msub 272 1"]  # squark pair production 
Pythia.PythiaCommand += ["pysubs msub 273 1"]  # squark pair production 
Pythia.PythiaCommand += ["pysubs msub 274 1"]  # squark pair production 
Pythia.PythiaCommand += ["pysubs msub 275 1"]  # squark pair production 
Pythia.PythiaCommand += ["pysubs msub 276 1"]  # squark pair production 
Pythia.PythiaCommand += ["pysubs msub 277 1"]  # squark pair production 
Pythia.PythiaCommand += ["pysubs msub 278 1"]  # squark pair production 
Pythia.PythiaCommand += ["pysubs msub 279 1"]  # squark pair production 
Pythia.PythiaCommand += ["pysubs msub 280 1"]  # squark pair production 
#######
Pythia.PythiaCommand+= ["pymssm imss 1 1"] # general MSSM !!
Pythia.PythiaCommand+= ["pymssm imss 52 3"] #switch on RPV lambda^prime coupling
Pythia.PythiaCommand += ["pymsrv rvlamp 211 0.000003"] # setting lambda^prime coupling strength for muons; need to change with LSPmass 
## with current masses: 0.000001 -> ct=9m
Pythia.PythiaCommand += ["pydat1 mstj 22 3"] # allow long decay length !
Pythia.PythiaCommand += ["pydat1 parj 72 100000."] # max length set to 100m
Pythia.PythiaCommand += ["pydat3 mdme 2241 1 0"] # switch explicetly off RPV decays to neutralinos (for muons)
Pythia.PythiaCommand += ["pydat3 mdme 2242 1 0"] # switch explicetly off RPV decays to neutralinos (for muons)
##
Pythia.PythiaCommand += ["pymssm rmss 1 500."] #M1 gaugino
Pythia.PythiaCommand += ["pymssm rmss 2 5000."] #M2 gaugino
Pythia.PythiaCommand += ["pymssm rmss 3 5000."] #M3 gluino
Pythia.PythiaCommand += ["pymssm rmss 4 800."] #mu higgsino, default=800  , make chi10 lighter than other gauginos
Pythia.PythiaCommand += ["pymssm rmss 5 2."]  #tan beta , default=2  , make chi10 lighter than other gauginos
Pythia.PythiaCommand += ["pymssm rmss 6 5000."] #left slepton, makes sneutrino mass high (also eL), to surpress LQD decay to neutrinos
Pythia.PythiaCommand += ["pymssm rmss 7 5000."] #right slepton, irrelevant in LQD
Pythia.PythiaCommand += ["pymssm rmss 8 700."] #left squark
Pythia.PythiaCommand += ["pymssm rmss 9 700."] #right squark
Pythia.PythiaCommand += ["pymssm rmss 10 5000."] #third generation #set high values to surpress production
Pythia.PythiaCommand += ["pymssm rmss 11 5000."] #third generation
Pythia.PythiaCommand += ["pymssm rmss 12 5000."] #third generation
Pythia.PythiaCommand += ["pymssm rmss 13 5000."] #third generation
Pythia.PythiaCommand += ["pymssm rmss 14 5000."] #third generation

#---------------------------------------------------------------
# Includes
#---------------------------------------------------------------

Pythia.PythiaCommand += ["pydat1 parj 90 20000"] # Turn off FSR.
Pythia.PythiaCommand += ["pydat3 mdcy 15 1 0"]   # Turn off tau decays.

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9

from MC10JobOptions.SUSYEvgenConfig import evgenConfig

#---------------------------------------------------------------
#  Decay Length Filter
#---------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import DecayLengthFilter
topAlg += DecayLengthFilter()

DecayLengthFilter = topAlg.DecayLengthFilter
DecayLengthFilter.McEventCollection = "GEN_EVENT"
# specify desired decay region:
DecayLengthFilter.Rmin = 0
DecayLengthFilter.Rmax = 560
DecayLengthFilter.Zmin = 0
DecayLengthFilter.Zmax = 2720
DecayLengthFilter.particle_id = 1000022;

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------

try:
    StreamEVGEN.RequireAlgs += [ "DecayLengthFilter" ]
except Exception, e:
    pass

#---------------------------------------------------------------
#End of job options file
#
###############################################################

