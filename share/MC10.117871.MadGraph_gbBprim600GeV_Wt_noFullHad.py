###############################################################
# Job options file
# g b -> Bprime -> Wt 
# semi-leptonic and di-leptonic, no taus
# Huaqiao ZHANG,   Huaqiao.Zhang@cern.ch 
###############################################################

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_PythiaMC09p_Common.py" )

Pythia.PythiaCommand += [ "pyinit user madgraph",
                          "pyinit pylisti -1","pyinit pylistf 1","pyinit dumpr 1 2",
                          "pydat1 parj 90 20000", # Turn off FSR.
                          "pydat3 mdcy 15 1 0"   # Turn off tau decays.
                          ]

## ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

## ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.generators = ["Pythia","MadGraph"]
evgenConfig.efficiency = 0.90 
evgenConfig.inputfilebase = 'group.phys-gener.MadGraph5.117871.BprimeWt600GeVcteq6l1.TXT.mc10_v1'


