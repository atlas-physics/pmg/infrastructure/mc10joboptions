###############################################################
#
# Job options file for Hydjet generation of
# Pb + Pb collisions at 3950 GeV/(colliding nucleon pair)
#
# Andrzej Olszewski
#
# Sept 2009
#
#==============================================================

#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# configuring the Athena application for a 'generator' job
import AthenaCommon.AtlasUnixGeneratorJob

# make sure we are loading the ParticleProperty service
from AthenaCommon.AppMgr import ServiceMgr as svcMgr

from AthenaCommon.Configurable import Configurable
svcMgr.MessageSvc.OutputLevel = 3

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

from Hydjet_i.Hydjet_iConf import Hydjet
topAlg += Hydjet()

#--------------------------------------------------------------
# Algorithms Private Options
#--------------------------------------------------------------
if not hasattr(svcMgr, 'AtRndmGenSvc'):
    from AthenaServices.AthenaServicesConf import AtRndmGenSvc
    svcMgr += AtRndmGenSvc()

# not actually used in Hydjet interface
svcMgr.AtRndmGenSvc.Seeds = \
       ["HYDJET 999999991 999999992", "HYDJET_INIT 999999993 999999994"]

# Use fixed seeds for reproducibility
import random
seed1 = random.randint(2<<15-1,1<<31-1)
seed1 &= 0xfffffffe

#----------------------
# Hydjet Parameters
#----------------------
Hydjet = topAlg.Hydjet
Hydjet.Initialize = ["e 3950", "a 208", "nh 23500",
                     "ifb 1", "bmin 0", "bmax 3", # central impact range [fm]
                     "nhsel 2", "ishad 1", "ptmin 10.0",
                     "ylfl 4.0", "ytfl 1.5", "tf 0.1", "fpart 1",
                     "ienglu 0", "ianglu 0", "t0 1", "tau0 0.1", "nf 0",
                     "mstp 51 7", "mstp 81 0", "mstu 21 1", "paru 14 1.0",
                     "msel 1", "nseed "+str(seed1) ]

#---------------------------------------------------------------
# Pool Persistency
#---------------------------------------------------------------
from AthenaPoolCnvSvc.WriteAthenaPool import AthenaPoolOutputStream

#--- StreamEVGEN ---
StreamEVGEN = AthenaPoolOutputStream( "StreamEVGEN" )

# 2101 == EventInfo
# 133273 == MCTruth (HepMC)
# 54790518 == HijigEventParams
try:
    StreamEVGEN.ItemList  = [ "2101#*","133273#GEN_EVENT" ]
    StreamEVGEN.ItemList += [ "54790518#*" ]
except Exception, e:
    pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
# not implemented yet for Hydjet
from MC10JobOptions.HydjetEvgenConfig import evgenConfig
#==============================================================
#
# End of CSC hydjet
#
###############################################################
