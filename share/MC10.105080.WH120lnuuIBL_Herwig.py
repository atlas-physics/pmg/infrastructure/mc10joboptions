###############################################################
#
# Job options file
#
# Herwig WH production with a higgs and W pT filter
# Author: Giacinto Piacquadio (2/5/2008)
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")
 

try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_Herwig_Common_7TeV.py" )
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_Herwig_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_Herwig_Common.py" )
  if runArgs.ecmEnergy == 14000.0:
    include ( "MC10JobOptions/MC10_Herwig_Common_14TeV.py" )


except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig

Herwig.HerwigCommand += ["iproc 12602",
                         "rmass 201 120.0",
                         "taudec TAUOLA"]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

# Add the filters:
# Higgs and W filter
from GeneratorFilters.GeneratorFiltersConf import ParticleFilter
from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
ParticleFilterHiggs= ParticleFilter(name = "ParticleFilterHiggs",
                                    StatusReq=195,
                                    PDG=25,
                                    Ptcut = 150.*GeV,
                                    Etacut = 3.0,
                                    Energycut = 1000000000.0)

ParticleFilterW= ParticleFilter(name = "ParticleFilterW",
                                StatusReq=195,
                                PDG=24,
                                Ptcut = 100.*GeV,
                                Etacut = 3.0,
                                Energycut = 1000000000.0)

LeptonFilter= LeptonFilter(name = "LeptonFilter")
LeptonFilter.Ptcut = 15.*GeV
LeptonFilter.Etacut = 3.0

topAlg += LeptonFilter
topAlg += ParticleFilterHiggs
topAlg += ParticleFilterW

try:
     StreamEVGEN.RequireAlgs += [ "LeptonFilter" ]
     StreamEVGEN.RequireAlgs += [ "ParticleFilterHiggs" ]
     StreamEVGEN.RequireAlgs += [ "ParticleFilterW" ]
except Exception, e:
     pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.HerwigEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.0382
# 5000/117925=0.0424
#==============================================================
#
# End of job options file
#
###############################################################
