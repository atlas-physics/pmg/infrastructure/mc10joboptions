###############################################################
# Whizard VBS
# 
# Jan Schumacher <j.schumacher@physik.tu-dresden.de>
# CME: 14TeV
# Process: qq -> qq l nu l nu
#          q: u d s U D S
#          l, nu: all flavours
#      QCD off, no KM resonance
# Whizard cross section: 124.199fb +-(stat) 0.071fb
# Filter efficiency: 0.3887 +-(stat) 0.0015
# Pythia cross section (Whizard * eff): 48.28fb

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_PythiaMC09p_Common.py" )
Pythia.PythiaCommand += [
    # Use Madgraph interface
    "pyinit user lhef",

    # Set process type and W' mass
    "pysubs msel 0",
    
    "pydat2 pmas 25 1 1.E9",         # Higgs mass

    # No tau decays (Tauola)
    "pydat3 mdcy 15 1 0",
    # No FSR (Photos)
    "pydat1 parj 90 20000",

    "pyinit pylistf 1",
    
    # list 1st 10 events
    "pyinit dumpr 1 10",

    # print the decay info
    "pyinit pylisti 12"
]


# Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )



#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
topAlg += MultiLeptonFilter()

MultiLeptonFilter.Ptcut = 15000.
MultiLeptonFilter.Etacut = 2.7
MultiLeptonFilter.NLeptons = 2


#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------

try:
  StreamEVGEN.RequireAlgs += [ 'MultiLeptonFilter' ]
except Exception, e:
  pass


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.LhefEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.4
evgenConfig.inputfilebase = "Whizard.105007.vbs_km_ew_lhe"
# Ask for 1980
evgenConfig.minevents  = 1500
evgenConfig.maxeventsfactor = 1.5
