#########################################
# r>15.2.0 example jO
# Pythia 6.4.20 UED swicthes description
#########################################
#
# Integer switches
#
#      IUED(1) =  The main UED ON(=1)/OFF(=0) switch  
#                 Default value = 0
#
#      IUED(2) =  On/Off switch for the extension to (N+4)-dimensional gravity
#                 (switching it on enables gravity-mediated LKP decay): ON(=1)/OFF(=0)  
#                 Default value = 0
#
#      IUED(3) =  The number of KK excitation quark flavors  
#                 Default value = 5
#
#      IUED(4) =  N, the number of large extra dimensions where only the graviton propagates.
#                 Only used when IUED(2)=1.  
#                 Default value = 6 (can be set to 2, 4 or 6)
# 
#      IUED(5) =  Selects whether the code takes Lambda (=0) or Lambda*R (=1) as input. 
#                 See also   {RUED(3:4)}.  
#                 Default value = 0
#
#      IUED(6) =  Selects whether the KK particle masses include radiative corrections (=1) 
#                 or are nearly degenerate m_KK ~ 1/R (=0).  
#                 Default value = 1
#
# Real switches
#
#      RUED(1) =  1/R, the curvature of the extra dimension  
#                 Default value = 1000 GeV
#
#      RUED(2) =  M_D, the (4+N)-dimensional Planck scale. Only used when IUED(2)=1.  
#                 Default value = 5000 GeV
#
#      RUED(3) =  Lambda, the cutoff scale. Used when IUED(5)=0.
#                 Default value = 20000 GeV
#
#      RUED(4) =  Lambda*R, the cutoff scale times the radius of the extra dimension.
#                 Used when IUED(5)=1.  
#                 Default value = 20

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

Pythia.PythiaCommand += [
    "pysubs msel 0",
    "pysubs msub 142 1", 
    "pydat3 mdme 311 1 0",
    "pydat3 mdme 312 1 0",
    "pydat3 mdme 313 1 0",
    "pydat3 mdme 314 1 -1",
    "pydat3 mdme 315 1 0",
    "pydat3 mdme 316 1 0",
    "pydat3 mdme 317 1 0",
    "pydat3 mdme 318 1 -1",
    "pydat3 mdme 319 1 0",
    "pydat3 mdme 320 1 0",
    "pydat3 mdme 321 1 0",
    "pydat3 mdme 322 1 -1",
    "pydat3 mdme 323 1 -1",
    "pydat3 mdme 324 1 -1",
    "pydat3 mdme 325 1 -1",
    "pydat3 mdme 326 1 -1",
    "pydat3 mdme 327 1 0",
    "pydat3 mdme 328 1 0",
    "pydat3 mdme 329 1 0",
    "pydat3 mdme 330 1 -1",
    # W' decay to WZ
    "pydat3 mdme 331 1 1",
    "pydat3 mdme 332 1 0",
    "pydat3 mdme 333 1 0",
    # W' mass
    "pydat2 pmas 34 1 500",
    # W' mass cutoff (60% of mass)
    "pysubs ckin 1 300",
    # W decays (hadronic)
    "pydat3 mdme 190 1 1",
    "pydat3 mdme 191 1 1",
    "pydat3 mdme 192 1 1",
    "pydat3 mdme 193 1 -1",
    "pydat3 mdme 194 1 1",
    "pydat3 mdme 195 1 1",
    "pydat3 mdme 196 1 1",
    "pydat3 mdme 197 1 -1",
    "pydat3 mdme 198 1 1",
    "pydat3 mdme 199 1 1",
    "pydat3 mdme 200 1 1",
    "pydat3 mdme 201 1 -1",
    "pydat3 mdme 202 1 -1",
    "pydat3 mdme 203 1 -1",
    "pydat3 mdme 204 1 -1",
    "pydat3 mdme 205 1 -1",
    "pydat3 mdme 206 1 0",
    "pydat3 mdme 207 1 0",
    "pydat3 mdme 208 1 0",
    "pydat3 mdme 209 1 -1",
    # Z decays (ee)
    "pydat3 mdme 174 1 0",
    "pydat3 mdme 175 1 0",
    "pydat3 mdme 176 1 0",
    "pydat3 mdme 177 1 0",
    "pydat3 mdme 178 1 0",
    "pydat3 mdme 179 1 0",
    "pydat3 mdme 180 1 -1",
    "pydat3 mdme 181 1 -1",
    "pydat3 mdme 182 1 1",
    "pydat3 mdme 183 1 0",
    "pydat3 mdme 184 1 0",
    "pydat3 mdme 185 1 0",
    "pydat3 mdme 186 1 0",
    "pydat3 mdme 187 1 0",
    "pydat3 mdme 188 1 -1",
    "pydat3 mdme 189 1 -1",
    "pydat1 parj 90 20000",
    "pydat3 mdcy 15 1 0",
    "pystat 1 3 4 5",
    "pyinit dumpr 1 5",
    "pyinit pylistf 1",
    "pyinit pylisti 12"
    ]

include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9

#==============================================================
#
# End of job options file
#
###############################################################
