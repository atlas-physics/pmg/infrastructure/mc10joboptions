###############################################################
#
# Job options file
#
# Herwig VBF H->gamgam (mH=110GeV) with 2 photon EF
#
# Responsible person(s)
#   24 June, 2008-xx xxx, 20xx: Junichi TANAKA (Junichi.Tanaka@cern.ch)
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 

try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_Herwig_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_Herwig_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_Herwig_Common.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig

# ... Herwig
Herwig.HerwigCommand += [ "iproc 11912",
                          "rmass 201 110.0" ]

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import PhotonFilter
topAlg += PhotonFilter()

PhotonFilter = topAlg.PhotonFilter
PhotonFilter.Ptcut = 20000.
PhotonFilter.Etacut = 2.7
PhotonFilter.NPhotons = 2

try:
     StreamEVGEN.RequireAlgs +=  [ "PhotonFilter" ]
except Exception, e:
     pass

from MC10JobOptions.HerwigEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.70
# 14.2.21.2
#5000/6345=.78802206461780929866
#5000/6345*0.9=.70921985815602836879
#==============================================================
#
# End of job options file
#
###############################################################
