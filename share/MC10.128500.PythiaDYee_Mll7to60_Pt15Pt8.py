###############################################################
#
# Job options file
#
#===============================================================

# ... Main generator : Pythia
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_Pythia_Common.py" )
Pythia.PythiaCommand += [ "pysubs msel 0",          # Users decay choice.
                          "pydat1 parj 90 20000",   # Turn off FSR.
                          "pydat3 mdcy 15 1 0",     # Turn off tau decays.
                          "pysubs msub 1 1",        # Create Z bosons.
                          "pysubs ckin 1 7.0",      # Lower invariant mass.
                          "pysubs ckin 2 60.0",     # Higher invariant mass.
                          "pydat3 mdme 174 1 0",
                          "pydat3 mdme 175 1 0",
                          "pydat3 mdme 176 1 0",
                          "pydat3 mdme 177 1 0",
                          "pydat3 mdme 178 1 0",
                          "pydat3 mdme 179 1 0",
                          "pydat3 mdme 182 1 1",    # Switch for Z->ee.
                          "pydat3 mdme 183 1 0",
                          "pydat3 mdme 184 1 0",    # Switch for Z->mumu.
                          "pydat3 mdme 185 1 0",
                          "pydat3 mdme 186 1 0",    # Switch for Z->tautau.
                          "pydat3 mdme 187 1 0"]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )
#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
topAlg += MultiLeptonFilter("Multi1TLeptonFilter")
topAlg += MultiLeptonFilter("Multi2LLeptonFilter")

Multi1TLeptonFilter = topAlg.Multi1TLeptonFilter
Multi1TLeptonFilter.Ptcut = 15000.
Multi1TLeptonFilter.Etacut = 5.0
Multi1TLeptonFilter.NLeptons = 1

Multi2LLeptonFilter = topAlg.Multi2LLeptonFilter
Multi2LLeptonFilter.Ptcut = 8000.
Multi2LLeptonFilter.Etacut = 5.0
Multi2LLeptonFilter.NLeptons = 2

try:
   StreamEVGEN.RequireAlgs  = [ "Multi1TLeptonFilter" ]
   StreamEVGEN.RequireAlgs += [ "Multi2LLeptonFilter" ]
except Exception, e:
   pass
                
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
# this value is not relevant in the central production
evgenConfig.efficiency = 0.90
#==============================================================
#
# End of job options file
#
###############################################################
