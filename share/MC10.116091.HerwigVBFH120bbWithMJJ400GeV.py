###############################################################
#
# Job options file
#
# Herwig VBF H->bb (mH=120GeV)
#
# Responsible person(s)
#   25 May, 2011: Eric OUELLETTE (eao@uvic.ca)
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

# ... Herwig
try:
    if runArgs.ecmEnergy == 7000.0:
        include ( "MC10JobOptions/MC10_Herwig_Common_7TeV.py" )
    if runArgs.ecmEnergy == 10000.0:
        include ( "MC10JobOptions/MC10_Herwig_Common.py" )
except NameError:
    # needed (dummy) default
    from Herwig_i.Herwig_iConf import Herwig
    topAlg += Herwig()
    Herwig = topAlg.Herwig

Herwig.HerwigCommand += [ "iproc 11905",
                          "rmass 201 120.0",
                          "taudec TAUOLA" ]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
try:
  from RecExConfig.RecFlags  import rec
  rec.doTruth = True
  from JetRec.JetGetters import * 
  c4=make_StandardJetGetter('AntiKt',0.4,'Truth') 
  c4alg = c4.jetAlgorithmHandle() 
except Exception, e:
  pass

from GeneratorFilters.GeneratorFiltersConf import VBFForwardJetsFilter
topAlg += VBFForwardJetsFilter()
VBFForwardJetsFilter = topAlg.VBFForwardJetsFilter
VBFForwardJetsFilter.MassJJ=400.0*GeV
VBFForwardJetsFilter.NJets = 4
VBFForwardJetsFilter.OutputLevel = 3
VBFForwardJetsFilter.TruthJetContainer="AntiKt4TruthJets"

try:
    StreamEVGEN.RequireAlgs = [ "VBFForwardJetsFilter" ]
except Exception, e:
    pass
              
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.HerwigEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.42
#==============================================================
#
# End of job options file
#
###############################################################
