## Herwig++ job option file for Susy 2-parton -> 2-sparticle processes

## Get a handle on the top level algorithms' sequence
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

try:
     include ( "MC10JobOptions/MC10_Herwigpp_LOstarJETS_Common.py" )
except NameError:
     cmds =  ""

## Add Herwig++ parameters for this process
cmds += """
## Generate the process in MSSM equivalent to 2-parton -> 2-sparticle processes in Fortran Herwig
## Read the MSSM model details

read MSSM.Hwpp250.model

cd /Herwig/NewPhysics 

#incoming parton
insert HPConstructor:Incoming 0 /Herwig/Particles/g
insert HPConstructor:Incoming 1 /Herwig/Particles/u
insert HPConstructor:Incoming 2 /Herwig/Particles/ubar
insert HPConstructor:Incoming 3 /Herwig/Particles/d
insert HPConstructor:Incoming 4 /Herwig/Particles/dbar
insert HPConstructor:Incoming 5 /Herwig/Particles/s
insert HPConstructor:Incoming 6 /Herwig/Particles/sbar
insert HPConstructor:Incoming 7 /Herwig/Particles/c
insert HPConstructor:Incoming 8 /Herwig/Particles/cbar


# create new model definition to play with
cp NewModel ComplicatedModel

# erase defaults
erase ComplicatedModel:HardProcessConstructors 0
erase ComplicatedModel:HardProcessConstructors 0
erase ComplicatedModel:HardProcessConstructors 0
erase ComplicatedModel:HardProcessConstructors 0
erase ComplicatedModel:HardProcessConstructors 0

#incoming parton
insert HPConstructor:Incoming 0 /Herwig/Particles/g
insert HPConstructor:Incoming 1 /Herwig/Particles/u
insert HPConstructor:Incoming 2 /Herwig/Particles/ubar
insert HPConstructor:Incoming 3 /Herwig/Particles/d
insert HPConstructor:Incoming 4 /Herwig/Particles/dbar
insert HPConstructor:Incoming 5 /Herwig/Particles/s
insert HPConstructor:Incoming 6 /Herwig/Particles/sbar
insert HPConstructor:Incoming 7 /Herwig/Particles/c
insert HPConstructor:Incoming 8 /Herwig/Particles/cbar


# insert process 1
cp HPConstructor HPConstructor_1

insert HPConstructor_1:Outgoing 0 /Herwig/Particles/~u_L
insert HPConstructor_1:Outgoing 0 /Herwig/Particles/~g
set HPConstructor_1:Processes Exclusive

insert ComplicatedModel:HardProcessConstructors 0 HPConstructor_1


# insert process 2
cp HPConstructor HPConstructor_2

insert HPConstructor_2:Outgoing 0 /Herwig/Particles/~u_Lbar
insert HPConstructor_2:Outgoing 0 /Herwig/Particles/~g
set HPConstructor_2:Processes Exclusive

insert ComplicatedModel:HardProcessConstructors 0 HPConstructor_2


# insert process 3
cp HPConstructor HPConstructor_3

insert HPConstructor_3:Outgoing 0 /Herwig/Particles/~u_R
insert HPConstructor_3:Outgoing 0 /Herwig/Particles/~g
set HPConstructor_3:Processes Exclusive

insert ComplicatedModel:HardProcessConstructors 0 HPConstructor_3


# insert process 4
cp HPConstructor HPConstructor_4

insert HPConstructor_4:Outgoing 0 /Herwig/Particles/~u_Rbar
insert HPConstructor_4:Outgoing 0 /Herwig/Particles/~g
set HPConstructor_4:Processes Exclusive

insert ComplicatedModel:HardProcessConstructors 0 HPConstructor_4


# insert process 5
cp HPConstructor HPConstructor_5

insert HPConstructor_5:Outgoing 0 /Herwig/Particles/~d_L
insert HPConstructor_5:Outgoing 0 /Herwig/Particles/~g
set HPConstructor_5:Processes Exclusive

insert ComplicatedModel:HardProcessConstructors 0 HPConstructor_5


# insert process 6
cp HPConstructor HPConstructor_6

insert HPConstructor_6:Outgoing 0 /Herwig/Particles/~d_Lbar
insert HPConstructor_6:Outgoing 0 /Herwig/Particles/~g
set HPConstructor_6:Processes Exclusive

insert ComplicatedModel:HardProcessConstructors 0 HPConstructor_6


# insert process 7
cp HPConstructor HPConstructor_7

insert HPConstructor_7:Outgoing 0 /Herwig/Particles/~d_R
insert HPConstructor_7:Outgoing 0 /Herwig/Particles/~g
set HPConstructor_7:Processes Exclusive

insert ComplicatedModel:HardProcessConstructors 0 HPConstructor_7


# insert process 8
cp HPConstructor HPConstructor_8

insert HPConstructor_8:Outgoing 0 /Herwig/Particles/~d_Rbar
insert HPConstructor_8:Outgoing 0 /Herwig/Particles/~g
set HPConstructor_8:Processes Exclusive

insert ComplicatedModel:HardProcessConstructors 0 HPConstructor_8


# insert process 9
cp HPConstructor HPConstructor_9

insert HPConstructor_9:Outgoing 0 /Herwig/Particles/~s_L
insert HPConstructor_9:Outgoing 0 /Herwig/Particles/~g
set HPConstructor_9:Processes Exclusive

insert ComplicatedModel:HardProcessConstructors 0 HPConstructor_9


# insert process 10
cp HPConstructor HPConstructor_10

insert HPConstructor_10:Outgoing 0 /Herwig/Particles/~s_Lbar
insert HPConstructor_10:Outgoing 0 /Herwig/Particles/~g
set HPConstructor_10:Processes Exclusive

insert ComplicatedModel:HardProcessConstructors 0 HPConstructor_10


# insert process 11
cp HPConstructor HPConstructor_11

insert HPConstructor_11:Outgoing 0 /Herwig/Particles/~s_R
insert HPConstructor_11:Outgoing 0 /Herwig/Particles/~g
set HPConstructor_11:Processes Exclusive

insert ComplicatedModel:HardProcessConstructors 0 HPConstructor_11


# insert process 12
cp HPConstructor HPConstructor_12

insert HPConstructor_12:Outgoing 0 /Herwig/Particles/~s_Rbar
insert HPConstructor_12:Outgoing 0 /Herwig/Particles/~g
set HPConstructor_12:Processes Exclusive

insert ComplicatedModel:HardProcessConstructors 0 HPConstructor_12


# insert process 13
cp HPConstructor HPConstructor_13

insert HPConstructor_13:Outgoing 0 /Herwig/Particles/~c_L
insert HPConstructor_13:Outgoing 0 /Herwig/Particles/~g
set HPConstructor_13:Processes Exclusive

insert ComplicatedModel:HardProcessConstructors 0 HPConstructor_13


# insert process 14
cp HPConstructor HPConstructor_14

insert HPConstructor_14:Outgoing 0 /Herwig/Particles/~c_Lbar
insert HPConstructor_14:Outgoing 0 /Herwig/Particles/~g
set HPConstructor_14:Processes Exclusive

insert ComplicatedModel:HardProcessConstructors 0 HPConstructor_14


# insert process 15
cp HPConstructor HPConstructor_15

insert HPConstructor_15:Outgoing 0 /Herwig/Particles/~c_R
insert HPConstructor_15:Outgoing 0 /Herwig/Particles/~g
set HPConstructor_15:Processes Exclusive

insert ComplicatedModel:HardProcessConstructors 0 HPConstructor_15


# insert process 16
cp HPConstructor HPConstructor_16

insert HPConstructor_16:Outgoing 0 /Herwig/Particles/~c_Rbar
insert HPConstructor_16:Outgoing 0 /Herwig/Particles/~g
set HPConstructor_16:Processes Exclusive

insert ComplicatedModel:HardProcessConstructors 0 HPConstructor_16



# switch to complicated model
set MSSM/Model:ModelGenerator ComplicatedModel

## Read the SUSY spectrum file (SLHA format)
setup MSSM/Model simplifiedModel.134023.txt
"""

## Set the command vector
topAlg.Herwigpp.Commands = cmds.splitlines()

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.HerwigppEvgenConfig import evgenConfig
from MC10JobOptions.SUSYEvgenConfig import evgenConfig

#==============================================================
#
# End of job options file
#
###############################################################
evgenConfig.auxfiles +=[ "simplifiedModel*.txt"]
evgenConfig.auxfiles +=[ "MSSM.Hwpp250.model"]
