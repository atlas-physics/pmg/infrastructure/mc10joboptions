# Single protons with injection energy scraping TAS absorber (+y side)
# based on studies by D.Bocian (LHC PN335, scenario 9)

# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

from ParticleGenerator.ParticleGeneratorConf import ParticleGenerator
topAlg += ParticleGenerator()

ParticleGenerator = topAlg.ParticleGenerator

# For VERBOSE output from ParticleGenerator.
ParticleGenerator.OutputLevel = 1

ParticleGenerator.orders = [
 "PDGcode: constant 2212",
 "energy: constant 450000",
 "vertX: constant 0.0",
 "vertY: constant 17.0",
 "vertZ: constant -20851.0",
 "targetx: constant 0.0",
 "targety: constant 24.0",
 "targetz: constant 20850.0",
 "time: constant -20851.0"
 ]

#==============================================================
#
# End of job options file
#
###############################################################
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.SingleEvgenConfig import evgenConfig
