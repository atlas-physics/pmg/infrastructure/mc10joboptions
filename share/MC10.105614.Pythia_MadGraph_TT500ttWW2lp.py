## **********************************************************************
## $Id: MC10.105614.Pythia_MadGraph_TT500ttWW2lp.py,v 1.1 2008-08-22 07:18:33 osamu Exp $
##
## Authors:
##  David Berge and Michael Wilson, CERN
## **********************************************************************

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_PythiaMC09p_Common.py" )
Pythia.PythiaCommand += ["pyinit user madgraph"]

## ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

## ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.95
evgenConfig.generators = ["Pythia","MadGraph"]
evgenConfig.inputfilebase = "MadGraph.105614.Pythia_MadGraph_TT500ttWW2lp"
