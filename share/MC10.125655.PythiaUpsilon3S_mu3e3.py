###############################################################
#
#       jobOptions for production of Upsilon(3S) resonance
#               in NRQCD colour-octet framework
#
# Upsilon(3S)->mu4mu4 channel only: no feeddown to U(2S) or U(1S)
#
# Author: Darren D Price ( Darren.Price@cern.ch )
# Date:   May 2008
#==============================================================
#--------------------------------------------------------------
# General Application Configuration options
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )


Pythia.PythiaCommand += [ # quarkonium processes
    
    "pysubs msel 62", # colour octet bottomonium production (461-479)
                            
    # ----- Upsilon production -----
    # --- 3S1(1)
    #"pysubs msub 461 1",  # gg->bb~[3S1(1)]+g
    # --- 3S1(8)
    #"pysubs msub 462 1",  # gg->bb~[3S1(8)]+g
    #"pysubs msub 465 1",  # gq->q+bb~[3S1(8)]
    #"pysubs msub 468 1",  # qq~->g+bb~[3S1(8)]
    # --- 1S0(8)
    #"pysubs msub 463 1",  # gg->bb~[1S0(8)]+g
    #"pysubs msub 466 1",  # gq->q+bb~[1S0(8)]
    #"pysubs msub 469 1",  # qq~->g+bb~[1S0(8)]
    # --- 3PJ(8)
    #"pysubs msub 464 1",  # gg->bb~[3PJ(8)]+g
    #"pysubs msub 467 1",  # gq->q+bb~[3PJ(8)]
    #"pysubs msub 470 1",  # qq~->g+bb~[3PJ(8)]

    # ----- Chi's -----
    #"pysubs msub 471 1",  # gg->bb~[3P0(1)]+g
    #"pysubs msub 472 1",  # gg->bb~[3P1(1)]+g
    #"pysubs msub 473 1",  # gg->bb~[3P2(1)]+g
    #"pysubs msub 474 1",  # qg->q+bb~[3P0(1)]
    #"pysubs msub 475 1",  # qg->q+bb~[3P1(1)]
    #"pysubs msub 476 1",  # qg->q+bb~[3P2(1)]
    #"pysubs msub 477 1",  # qq~->bb~[3P0(1)]+g
    #"pysubs msub 478 1",  # qq~->bb~[3P1(1)]+g
    #"pysubs msub 479 1",  # qq~->bb~[3P2(1)]+g
    
    ]                        

Pythia.PythiaCommand += [ # make Ups(1S) into Ups(3S)
     "pydat2 pmas 553 1 10.355",
     "pydat2 pmas 10551 1 10.433",
     "pydat2 pmas 20553 1 10.456",
     "pydat2 pmas 555 1 10.469",
     "pydat2 pmas 9900551 1 10.6",
     "pydat2 pmas 9900553 1 10.6",
     "pydat2 pmas 9910551 1 10.6",
     ]

Pythia.PythiaCommand += [ # force decays

#    "pydat3 mdme 858 1 0", # J/psi->e+e-
#    "pydat3 mdme 859 1 1", # J/psi->mumu 
#    "pydat3 mdme 860 1 0",  # J/psi->rndmflavpairs
#
#    "pydat3 mdme 1501 1 1", # chi0c->J/psi gamma 
#    "pydat3 mdme 1502 1 0", # chi0c->rfp 
#    
#    "pydat3 mdme 1555 1 1", # chi1c->J/psi gamma 
#    "pydat3 mdme 1556 1 0", # chi1c->rfp
#    
#    "pydat3 mdme 861 1 1", # chi2c->J/psi gamma 
#    "pydat3 mdme 862 1 0", # chi2c->rfp

    "pydat3 mdme 1034 1 0", # Upsilon->e+e- 
    "pydat3 mdme 1035 1 0", # Upsilon->mu+mu-
    "pydat3 mdme 1036 1 1", # Upsilon->tau+tau-
    "pydat3 mdme 1037 1 0", # Upsilon->ddbar
    "pydat3 mdme 1038 1 0", # Upsilon->uubar
    "pydat3 mdme 1039 1 0", # Upsilon->ssbar
    "pydat3 mdme 1040 1 0", # Upsilon->ccbar
    "pydat3 mdme 1041 1 0", # Upsilon->ggg
    "pydat3 mdme 1042 1 0", # Upsilon->gamma gg
    
#    "pydat3 mdme 1520 1 1", # chi0b->Upsilon gamma 
#    "pydat3 mdme 1521 1 0", # chi0b->gg
#    
#    "pydat3 mdme 1565 1 1", # chi1b->Upsilon gamma 
#    "pydat3 mdme 1566 1 0", # chi1b->gg
#    
#    "pydat3 mdme 1043 1 1", # chi2b->Upsilon gamma 
#    "pydat3 mdme 1044 1 0", # chi2b->gg
    
    ]

Pythia.PythiaCommand += [ # NRQCD matrix elements
    
    "pypars parp 146 3.54",   # Ups''-3S1(1) NRQCD ME
    "pypars parp 147 0.039",   # Ups''-3S1(8) NRQCD ME
    "pypars parp 148 0.005",   # Ups''-1S0(8) NRQCD ME
    "pypars parp 149 0.005",   # Ups''-3P0(8) NRQCD ME / m_b^2 
    "pypars parp 150 0.110",  # chi''_b0-3P0(1) NRQCD ME / m_b^2
        
    ]

Pythia.PythiaCommand += [

    "pysubs ckin 3 1.",   # lower pT cut on hard process in GeV
    ]

include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

from GeneratorFilters.GeneratorFiltersConf import TauFilter
topAlg += TauFilter()

TauFilter = topAlg.TauFilter
TauFilter.Ntaus = 2
TauFilter.EtaMaxe = 2.5
TauFilter.EtaMaxmu = 2.5
TauFilter.EtaMaxhad = 0.0

TauFilter.Ptcute = 500.0
TauFilter.Ptcutmu = 500.0
TauFilter.Ptcuthad = 999999.0


from GeneratorFilters.GeneratorFiltersConf import ATauFilter
topAlg += ATauFilter()
ATauFilter = topAlg.ATauFilter
ATauFilter.Etacut         = 2.7                   ## Max |Eta|
ATauFilter.llPtcute       = 3000.                 ## Min Pt(e)  for lep-lep
ATauFilter.llPtcutmu      = 3000.                 ## Min Pt(mu) for lep-lep
ATauFilter.lhPtcute       = 50000000.                 ## Min Pt(e)  for lep-had
ATauFilter.lhPtcutmu      = 50000000.                 ## Min Pt(mu) for lep-had
ATauFilter.lhPtcuth       = 10000000.                ## Min Pt(h)  for lep-had
ATauFilter.hhPtcut        = 10000000.                ## Min Pt(h)  for had-had
ATauFilter.maxdphi        = 3.2                   ## Max dPhi between tau and anti-tau

from GeneratorFilters.GeneratorFiltersConf import ElectronFilter
topAlg += ElectronFilter()
ElectronFilter=topAlg.ElectronFilter
ElectronFilter.Ptcut = 3000.
ElectronFilter.Etacut= 2.7

from GeneratorFilters.GeneratorFiltersConf import MuonFilter
topAlg += MuonFilter()
MuonFilter=topAlg.MuonFilter
MuonFilter.Ptcut = 3000.
MuonFilter.Etacut= 2.7

try:
     StreamEVGEN.RequireAlgs += ["TauFilter", "ATauFilter", "ElectronFilter", "MuonFilter"]
except Exception, e:
     pass

from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.00008
#evgenConfig.minevents = 5000



#==============================================================
#
# End of job options file
#
###############################################################

