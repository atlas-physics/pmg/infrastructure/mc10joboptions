###############################################################
#
# Job options file
#
# Pythia ttbar with BtoElectronFilter 
#
# Author: Jochen Hartert, 24 Feb. 2009
#
#==============================================================

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence( "TopAlg" )

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

Pythia.PythiaCommand += [ "pysubs msel 6",          # ttbar production
                          "pydat1 parj 90 20000" ]  # Turn off lepton radiation

# Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import WeightedBDtoElectronFilter
topAlg += WeightedBDtoElectronFilter()

WeightedBDtoElectronFilter = topAlg.WeightedBDtoElectronFilter
WeightedBDtoElectronFilter.EtaBins = [ 0., 0.73, 1.45, 2.0, 3.0 ]
WeightedBDtoElectronFilter.PtBins = [ 15000., 17000., 19000., 23000., 27000.,
                                      35000., 43000., 59000., 75000. ]
WeightedBDtoElectronFilter.BinPrescaleFactors = [ 817, 689, 771, 535, 389, 208, 79, 31,
                                                  623, 486, 538, 371, 287, 157, 49, 17,
                                                  147, 111, 100, 69,  41,  24,  7,  2,
                                                  88,  67,  60,  44,  26,  11,  3,  1 ]

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------

try:
     StreamEVGEN.RequireAlgs = [ "WeightedBDtoElectronFilter" ]
except Exception, e:
     pass


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
# efficiency = 500/463398*0.9 = 0.000971
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.minevents = 500
evgenConfig.efficiency = 0.000971
