## Job options file for Herwig++, QCD jet slice production
## Andy Buckley, April '10

## Get a handle on the top level algorithms' sequence
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

try:
     include("MC10JobOptions/MC10_Herwigpp_Common.py")
except NameError:
     ## TODO: This fallback is a bad idea...
     # from Herwigpp_i.Herwigpp_iConf import Herwigpp
     # topAlg += Herwigpp()
     cmds =  ""

## Add to commands
cmds += """
## Set up QCD jets process
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEQCD2to2
set /Herwig/Cuts/JetKtCut:MinKT 35*GeV
## set /Herwig/Cuts/JetKtCut:MaxKT 70*GeV
## IdenticalToUE should be set to 0 for QCD
set /Herwig/UnderlyingEvent/MPIHandler:IdenticalToUE 0
"""

## Set commands
topAlg.Herwigpp.Commands = cmds.splitlines()

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.HerwigppEvgenConfig import evgenConfig
#evgenConfig.efficiency = 0.9
#evgenConfig.minevents=100

#==============================================================
#
# End of job options file
#
###############################################################
