###############################################################
#
# Job options file
# Daniel Geerts
#
#==============================================================
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 2
MessageSvc.infoLimit = 1000

#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------

# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 
try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common.py" )
  if runArgs.ecmEnergy == 14000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_14TeV.py" )
except NameError:
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig 

#Herwig.HerwigCommand += [ "iproc alpgen",
#                          "taudec TAUOLA",
#                        ]
Herwig.HerwigCommand += [ "msflag  0",
  "iproc alpgen",
  "taudec TAUOLA",
]
# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from MC10JobOptions.AlpgenEvgenConfig import evgenConfig
 
# inputfilebase
evgenConfig.inputfilebase = 'alpgen'
try:
  if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.alpgen.107680.WenuNp0_pt20_7tev.TXT.v2'
  if runArgs.ecmEnergy == 8000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.alpgen.107680.WenuNp0_pt20_8tev.TXT.v1'
  if runArgs.ecmEnergy == 10000.0:
    evgenConfig.inputfilebase = 'alpgen.107680.WenuNp0.pt20.v2'
except NameError:
  pass

# 7 TeV - Information on sample 107680
# 7 TeV - Filter efficiency  = 1.0000
# 7 TeV - MLM matching efficiency = 0.84
# 7 TeV - Number of Matrix Elements in input file  = 800
# 7 TeV - Alpgen cross section = 8277.0 pb
# 7 TeV - Herwig cross section = Alpgen cross section * eff(MLM) = 6913.3 pb
# 7 TeV - Cross section after filtering = 6913.3 pb
# 7 TeV - Lumi/500 events post-filter = 500/eff(Filter)/XS(jimmy) = 0.07 pb-1
#
# 7 TeV - Filter efficiency estimate below reduced by 10% to produce 555 events on average,
# 7 TeV - of which only 500 will be used in further processing
# 8 TeV - Information on sample 	107680
# 8 TeV - Filter efficiency  = 	1.0000
# 8 TeV - MLM matching efficiency = 	0.83
# 8 TeV - Number of Matrix Elements in input file  = 	800
# 8 TeV - Alpgen cross section = 	9664.4	 pb
# 8 TeV - Herwig cross section = Alpgen cross section * eff(MLM) = 	7991.5	 pb
# 8 TeV - Cross section after filtering = 	7991.5	 pb
# 8 TeV - Lumi/500 events post-filter = 500/eff(Filter)/XS(jimmy) = 	0.06	 pb-1
#
# 8 TeV - Filter efficiency estimate below reduced by 10% to produce 5556 events on average,
# 8 TeV - of which only 5000 will be used in further processing
evgenConfig.efficiency = 0.90000
evgenConfig.minevents = 5000
# 10 TeV - Information on sample 107680
# 10 TeV - Filter efficiency  = 1.0000
# 10 TeV - MLM matching efficiency = 0.82
# 10 TeV - Number of Matrix Elements in input file  = 800
# 10 TeV - Alpgen cross section = 12479.8 pb
# 10 TeV - Herwig cross section = Alpgen cross section * eff(MLM) = 10184.7 pb
# 10 TeV - Cross section after filtering = 10184.7 pb
# 10 TeV - Lumi/500 events post-filter = 500/eff(Filter)/XS(jimmy) = 0.05 pb-1
#==============================================================
#
# End of job options file
#
###############################################################
