###############################################################
#
# Job options file
#
# Alpgen bbgam2j with VBF cut
#
# Responsible person(s)
#   17 June, 2009-xx xxx, 20xx: Koloina Randrianarivony (krandria@physics.carleton.ca)
#
#==============================================================

#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------
 
### Herwig
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")
 

try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common.py" )
  if runArgs.ecmEnergy == 14000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_14TeV.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig


Herwig.HerwigCommand += [ "iproc alpgen",
                          "taudec TAUOLA"
                          ]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

# VBFForwardJetsFilter with TruthJets
try:
     from RecExConfig.RecFlags import rec
     rec.doTruth = True
     from JetRec.JetGetters import * 
     c4=make_StandardJetGetter('Cone',0.4,'Truth') 
     c4alg = c4.jetAlgorithmHandle() 
     c4alg.JetFinalEtCut.MinimumSignal = 10.*GeV
except Exception, e:
     pass
 
from GeneratorFilters.GeneratorFiltersConf import VBFForwardJetsFilter
topAlg += VBFForwardJetsFilter()
VBFForwardJetsFilter = topAlg.VBFForwardJetsFilter
VBFForwardJetsFilter.JetMinPt=15.*GeV
VBFForwardJetsFilter.JetMaxEta=5.0
VBFForwardJetsFilter.NJets=3 # 2 VBF jets+ 1 from "bjet"
VBFForwardJetsFilter.Jet1MinPt=15.*GeV
VBFForwardJetsFilter.Jet1MaxEta=5.0
VBFForwardJetsFilter.Jet2MinPt=15.*GeV
VBFForwardJetsFilter.Jet2MaxEta=5.0
VBFForwardJetsFilter.UseOppositeSignEtaJet1Jet2=False
VBFForwardJetsFilter.MassJJ=300.*GeV
VBFForwardJetsFilter.DeltaEtaJJ=2.0
VBFForwardJetsFilter.TruthJetContainer="Cone4TruthJets"
VBFForwardJetsFilter.LGMinPt=15.*GeV
VBFForwardJetsFilter.LGMaxEta=3
VBFForwardJetsFilter.DeltaRJLG=0.05
VBFForwardJetsFilter.RatioPtJLG=0.1

try:
     StreamEVGEN.RequireAlgs += ["VBFForwardJetsFilter" ]
except Exception, e:
     pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.AlpgenEvgenConfig import evgenConfig
 
# inputfilebase
evgenConfig.inputfilebase = 'alpgenv213.109452.bb2j'
evgenConfig.efficiency = 0.1757*0.9
# EF eff = 0.1757, MLM eff = 0.2040
#evgenConfig.minevents=0
