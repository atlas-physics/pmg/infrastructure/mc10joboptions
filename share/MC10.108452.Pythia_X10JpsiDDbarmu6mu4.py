###############################################################
# PRODUCTION SYSTEM FRAGMENT
#       jobOptions for production of X(10 GeV) -> J/psi Psi(3770),
#                      J/psi -> mu+mu-, Psi(3770) -> D0 Dbar0,
#                      D0 -> K-pi+, Dbar0 -> K+pi-
#
#==============================================================

#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
#evgenConfig.efficiency = 0.05

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

from GeneratorFilters.GeneratorFiltersConf import MultiMuonFilter
topAlg += MultiMuonFilter()

from GeneratorFilters.GeneratorFiltersConf import MuonFilter
topAlg += MuonFilter()

# AtRndmGenSvc.ReadFromFile = true;
Pythia = topAlg.Pythia

# X(10 GeV) -> J/psi Psi(3770), J/psi -> mu+mu-, Psi(3770) -> D0 Dbar0,
# D0 -> K-pi+, Dbar0 -> K+pi-
Pythia.PythiaCommand += [
    "pysubs msel 0",           # turn OFF global process selection
    "pysubs msub 86 1",	       # g+g -> J/psi+g turned ON
    "pyint2 kfpr 86 1 553",    # request Upsilon(1S) instead of J/psi for process 86
    "pydat2 pmas 553 1 10.0",   # set Upsilon(1S) mass to 10.0 GeV
    "pydat3 mdme 1034 1 0",    # Upsilon -> e+e- turned OFF
    "pydat3 mdme 1035 1 0",    # Upsilon -> mu+mu- turned OFF
    "pydat3 mdme 1036 1 0",    # Upsilon -> tau+tau- turned OFF
    "pydat3 mdme 1037 1 0",    # Upsilon -> dd~ turned OFF
    "pydat3 mdme 1038 1 0",    # Upsilon -> uu~ turned OFF
    "pydat3 mdme 1039 1 0",    # Upsilon -> ss~ turned OFF
    "pydat3 mdme 1040 1 1",    # Upsilon -> cc~ turned ON
    "pydat3 mdme 1041 1 0",    # Upsilon -> g g g turned OFF
    "pydat3 mdme 1042 1 0",    # Upsilon -> gamma g g turned OFF
    "pydat3 kfdp 1040 1 443",      #  request J/psi instead of c
    "pydat3 kfdp 1040 2 100443",   #  request Psi(2S) instead of cbar
    "pydat2 pmas 100443 1 3.771",  #  set Psi(2S) mass to 3.771 GeV Psi(3770)
    "pydat3 mdme 858 1 0",         #  J/psi -> ee turned OFF
    "pydat3 mdme 859 1 1",         #  J/psi -> mumu turned ON
    "pydat3 mdme 860 1 0",         #  J/psi -> random turned OFF
    "pydat3 mdme 1567 1 1",        #  Psi' -> ee turned ON
    "pydat3 mdme 1568 1 0",        #  Psi' -> mumu turned OFF
    "pydat3 mdme 1569 1 0",        #  Psi' -> random turned OFF
    "pydat3 mdme 1570 1 0",        #  Psi' -> J/psi pi+pi- turned OFF
    "pydat3 mdme 1571 1 0",        #  Psi' -> J/psi pi0pi0 turned OFF
    "pydat3 mdme 1572 1 0",        #  Psi' -> J/psi eta turned OFF
    "pydat3 mdme 1573 1 0",        #  Psi' -> J/psi pi0 turned OFF
    "pydat3 mdme 1574 1 0",        #  Psi' -> chi_0c gamma turned OFF
    "pydat3 mdme 1575 1 0",        #  Psi' -> chi_1c gamma turned OFF
    "pydat3 mdme 1576 1 0",        #  Psi' -> chi_2c gamma turned OFF
    "pydat3 mdme 1577 1 0",        #  Psi' -> eta_c gamma turned OFF
    "pydat3 kfdp 1567 1 421",      #  request D0 instead of e-
    "pydat3 kfdp 1567 2 -421",     #  request Dbar0 instead of e+
    "pydat3 mdme 747 1 0",       # D0 -> e+ nu_e K- turned OFF
    "pydat3 mdme 748 1 0",       # D0 -> e+ nu_e K*- turned OFF
    "pydat3 mdme 749 1 0",       # D0 -> e+ nu_e Kbar0 pi- turned OFF
    "pydat3 mdme 750 1 0",       # D0 -> e+ nu_e K- pi0 turned OFF
    "pydat3 mdme 751 1 0",       # D0 -> e+ nu_e K*bar0 pi- turned OFF
    "pydat3 mdme 752 1 0",       # D0 -> e+ nu_e K*- pi0 turned OFF
    "pydat3 mdme 753 1 0",       # D0 -> e+ nu_e pi- turned OFF
    "pydat3 mdme 754 1 0",       # D0 -> e+ nu_e rho- turned OFF
    "pydat3 mdme 755 1 0",       # D0 -> mu+ nu_mu K- turned OFF
    "pydat3 mdme 756 1 0",       # D0 -> mu+ nu_mu K*- turned OFF
    "pydat3 mdme 757 1 0",       # D0 -> mu+ nu_mu Kbar0 pi- turned OFF
    "pydat3 mdme 758 1 0",       # D0 -> mu+ nu_mu K- pi0 turned OFF
    "pydat3 mdme 759 1 0",       # D0 -> mu+ nu_mu K*bar0 pi- turned OFF
    "pydat3 mdme 760 1 0",       # D0 -> mu+ nu_mu K*- pi0 turned OFF
    "pydat3 mdme 761 1 0",       # D0 -> mu+ nu_mu pi- turned OFF
    "pydat3 mdme 762 1 0",       # D0 -> mu+ nu_mu rho- turned OFF
    "pydat3 mdme 763 1 1",       # D0 -> K- pi+ turned ON
    "pydat3 mdme 764 1 0",       # D0 -> K*- pi+ turned OFF
    "pydat3 mdme 765 1 0",       # D0 -> K- rho+ turned OFF
    "pydat3 mdme 766 1 0",       # D0 -> K*- rho+ turned OFF
    "pydat3 mdme 767 1 0",       # D0 -> Kbar0 pi0 turned OFF
    "pydat3 mdme 768 1 0",       # D0 -> K*bar0 pi0 turned OFF
    "pydat3 mdme 769 1 0",       # D0 -> K*bar0 eta turned OFF
    "pydat3 mdme 770 1 0",       # D0 -> Kbar0 rho0 turned OFF
    "pydat3 mdme 771 1 0",       # D0 -> K*bar0 rho0 turned OFF
    "pydat3 mdme 772 1 0",       # D0 -> Kbar0 omega turned OFF
    "pydat3 mdme 773 1 0",       # D0 -> Kbar0 phi turned OFF
    "pydat3 mdme 774 1 0",       # D0 -> K- a_1+ turned OFF
    "pydat3 mdme 775 1 0",       # D0 -> K_1- pi+ turned OFF
    "pydat3 mdme 776 1 0",       # D0 -> K- K+ turned OFF
    "pydat3 mdme 777 1 0",       # D0 -> K*- K+ turned OFF
    "pydat3 mdme 778 1 0",       # D0 -> K- K*+ turned OFF
    "pydat3 mdme 779 1 0",       # D0 -> Kbar0 K0 turned OFF
    "pydat3 mdme 780 1 0",       # D0 -> K*bar0 K0 turned OFF
    "pydat3 mdme 781 1 0",       # D0 -> K*bar0 K*0 turned OFF
    "pydat3 mdme 782 1 0",       # D0 -> pi+ pi- turned OFF
    "pydat3 mdme 783 1 0",       # D0 -> pi0 pi0 turned OFF
    "pydat3 mdme 784 1 0",       # D0 -> phi rho0 turned OFF
    "pydat3 mdme 785 1 0",       # D0 -> K- pi+ pi0 turned OFF
    "pydat3 mdme 786 1 0",       # D0 -> K- pi+ rho0 turned OFF
    "pydat3 mdme 787 1 0",       # D0 -> K- K+ Kbar0 turned OFF
    "pydat3 mdme 788 1 0",       # D0 -> Kbar0 pi+ pi- turned OFF
    "pydat3 mdme 789 1 0",       # D0 -> K*bar0 pi+ pi- turned OFF
    "pydat3 mdme 790 1 0",       # D0 -> K- K0 pi+ turned OFF
    "pydat3 mdme 791 1 0",       # D0 -> K*bar0 K+ pi- turned OFF
    "pydat3 mdme 792 1 0",       # D0 -> K_S0 K_S0 K_S0 turned OFF
    "pydat3 mdme 793 1 0",       # D0 -> phi pi+ pi- turned OFF
    "pydat3 mdme 794 1 0",       # D0 -> pi+ pi- pi0 turned OFF
    "pydat3 mdme 795 1 0",       # D0 -> K- pi+ pi0 pi0 turned OFF
    "pydat3 mdme 796 1 0",       # D0 -> K- pi+ pi+ pi- turned OFF
    "pydat3 mdme 797 1 0",       # D0 -> Kbar0 pi+ pi- pi0 turned OFF
    "pydat3 mdme 798 1 0",       # D0 -> K*bar0 pi+ pi- pi0 turned OFF
    "pydat3 mdme 799 1 0",       # D0 -> Kbar0 K+ K- pi0 turned OFF
    "pydat3 mdme 800 1 0",       # D0 -> pi+ pi+ pi- pi- turned OFF
    "pydat3 mdme 801 1 0",       # D0 -> K- pi+ pi+ pi- pi0 turned OFF
    "pydat3 mdme 802 1 0",       # D0 -> Kbar0 pi+ pi+ pi- pi- turned OFF
    "pydat3 mdme 803 1 0",       # D0 -> Kbar0 pi+ pi- pi0 pi0 turned OFF
    "pydat3 mdme 804 1 0",       # D0 -> Kbar0 rho0 pi0 pi0 pi0 turned OFF
    "pydat3 mdme 805 1 0",       # D0 -> pi+ pi+ pi- pi- pi0 turned OFF
    "pydat3 mdme 806 1 0",       # D0 -> rho0 pi+ pi+ pi- pi- turned OFF
    "pydat3 mdme 807 1 0"        # D0 -> K+ K- pi+ pi- pi0 turned OFF
    ]


Pythia.PythiaCommand += ["pyinit pylistf 1",

                         "pysubs ckin 3 10.", # lower pT cut on hard process in 9 GeV
                         
                         "pystat mstat 1",
                         "pyinit dumpr 0 10",#dump this event range to screen
                             
                         ]


MultiMuonFilter = topAlg.MultiMuonFilter
MultiMuonFilter.Ptcut = 4000.
MultiMuonFilter.Etacut = 2.5
MultiMuonFilter.NMuons = 2

MuonFilter = topAlg.MuonFilter
MuonFilter.Ptcut = 6000.
MuonFilter.Etacut = 2.5

try:
     StreamEVGEN.RequireAlgs += ["MultiMuonFilter"]
     StreamEVGEN.RequireAlgs += ["MuonFilter"]
except Exception, e:
     pass

#==============================================================
#
# End of job options file
#
###############################################################
