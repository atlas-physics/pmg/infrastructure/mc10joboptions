###############################################################
# Job options file for Exhume sample
# 
# Created by Vlastimil Kus in rel. 15.9.0.5 (November 2010)
#==============================================================

from AthenaCommon.AlgSequence import AlgSequence
topAlg=AlgSequence("TopAlg")

from Exhume_i.Exhume_iConf import ExHuME
topAlg += ExHuME()

#--------------------------------------------------------------
# Exhume options
#--------------------------------------------------------------
ExHuME.HiggsDecay  = 15  #PDG code for higgs decay
ExHuME.MassMin     = 115 #PDG code for higgs decay
ExHuME.MassMax     = 125 #PDG code for higgs decay
#ExHuME.Process    = "GG"
#ExHuME.ETMin      = 50 
#ExHuME.CosThetaMax= 0.9 #max cos theta in COM.Large vale will lower speed

ExHuME.UsePhotos=True
ExHuME.UseTauola=True

from MC10JobOptions.ExhumeEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9

include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

###############################################################
# End of job options file
#
###############################################################
