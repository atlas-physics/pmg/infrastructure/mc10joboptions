###############################################################
#
# Job options file
#
# Herwig VBF H->gamgam (mH=150GeV) with 2 photon EF
#
# Responsible person(s)
#   24 June, 2008-xx xxx, 20xx: Junichi TANAKA (Junichi.Tanaka@cern.ch)
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 

try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_Herwig_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_Herwig_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_Herwig_Common.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig

# ... Herwig
Herwig.HerwigCommand += [ "iproc 11912",
                          "rmass 201 150.0" ]

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import PhotonFilter
topAlg += PhotonFilter()

PhotonFilter = topAlg.PhotonFilter
PhotonFilter.Ptcut = 20000.
PhotonFilter.Etacut = 2.7
PhotonFilter.NPhotons = 2

try:
     StreamEVGEN.RequireAlgs +=  [ "PhotonFilter" ]
except Exception, e:
     pass

from MC10JobOptions.HerwigEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.76
# 14.2.21.2
#5000/5913=.84559445290038897344
#5000/5913*0.9=.76103500761035007609
#==============================================================
#
# End of job options file
#
###############################################################
