###############################################################
#
# Job options file
# Wouter Verkerke
#
#==============================================================
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 3

#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------

# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 

try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common.py" )
  if runArgs.ecmEnergy == 14000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_14TeV.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig


Herwig.HerwigCommand += [ "iproc alpgen",
                          "taudec TAUOLA",
                        ]
# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from MC10JobOptions.AlpgenEvgenConfig import evgenConfig
 
# inputfilebase
evgenConfig.inputfilebase = 'alpgen.108103.ZeeNp1HighMass.pt20'
# Information on sample 108103
# High mass sample: m(ll) mass range is 110 - 1000 GeV
# Filter efficiency  = 1.0000
# MLM matching efficiency = 0.41
# Number of Matrix Elements in input file  = 1700
# Alpgen cross section = 11.3 pb
# Herwig cross section = Alpgen cross section * eff(MLM) = 4.6 pb
# Cross section after filtering = 4.6 pb
# Lumi/500 events post-filter = 500/eff(Filter)/XS(jimmy) = 108.39 pb-1
#
# Filter efficiency estimate below reduced by 10% to produce 555 events on average,
# of which only 500 will be used in further processing
evgenConfig.efficiency = 0.90000
#==============================================================
#
# End of job options file
#
###############################################################
