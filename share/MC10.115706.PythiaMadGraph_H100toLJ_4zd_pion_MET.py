from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_PythiaMC09p_Common.py" )

Pythia.PythiaCommand += [ "pyinit user lhef",
                          "pydat1 parj 90 20000.",
                          "pydat3 mdcy 15 1 0" ]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.90
evgenConfig.generators = ["Pythia","Lhef"]
evgenConfig.inputfilebase = "group10.phys-gener.MadGraph.115706.H100toLJ_4zd_pion_MET.TXT.v1"
