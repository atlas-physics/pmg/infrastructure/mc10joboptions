# EXCLUSIVE
#---------------------------------------------------------------
# Provided by Florian and Zuzana (23/03/11)
# Modified by NK (01/05/11)
#---------------------------------------------------------------
import os
import sys
#import AthenaPoolCnvSvc.WriteAthenaPool
#from OutputStreamAthenaPool.OutputStreamAthenaPool import AthenaPoolOutputStream

from MC10JobOptions.PythiaRhadEvgenConfig import evgenConfig

from AthenaCommon.AlgSequence import AlgSequence
job=AlgSequence()

from Pythia_i.PythiaRhad_iConf import PythiaRhad
job +=PythiaRhad()
evgenConfig.generators += ["MadGraph","PythiaRhad"]
evgenConfig.inputfilebase = 'group10.phys-gener.MadGraph.123927.Gluino1p_300_cteq5l.TXT.v1'
evgenConfig.minevents = 500

job.PythiaRhad.Tune_Name="PYTUNE_103"
#job.PythiaRhad.Tune_Name="ATLAS_20090002"   #use MC09' tune with CTEQ6L1 pdf

####added for matching#####
# choose IEXCFILE=0 if you provide inclusive (0j+1j+2j+...) LHE file,
# and  IEXCFILE=1 if you provide exclusive (e.g. 1j) LHE file
phojf=open('./pythia_card.dat', 'w')
phojinp = """
      IEXCFILE=1
      showerkt=F
      qcut=20
"""
phojf.write(phojinp)
phojf.close()

##########################
# gluino, generic, P=10%
##########################
MODEL='generic'
CASE='gluino'
GBALLPROB=0.1
#MASS=300
pdg={}
q3={}
apflag={}
names={}
antinames={}
masses={}

pdg[("generic","gluino")] = [1000993,1009213,1009313,1009323,1009113,1009223,1009333,1091114,1092114,1092214,1092224,1093114, 1093214,1093224,1093314,1093324,1093334,0,0,0]
q3[("generic","gluino")] = [0,3,0,3,0,0,0,-3,0,3,6,-3,0,3,-3,0,-3,0,0,0]
apflag[("generic","gluino")] = [0,1,1,1,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0]
names[("generic","gluino")] = ["~g_ball","~g_rho+","~g_K*0","~g_K*+", "~g_rho0","~g_omega","~g_phi","~g_Dlt-","~g_Dlt0", "~g_Dlt+","~g_Dlt++","~g_Sgm*-","~g_Sgm*0", "~g_Sgm*+","~g_Xi*-","~g_Xi*0 ","~g_Omg-"," "," "," "]
antinames[("generic","gluino")] = [" ","~g_rho-","~g_K*br0","~g_K*-"," "," "," ", "~g_Dltb+","~g_Dltb0","~g_Dltb-","~g_Dlb--","~g_Sgmb+","~g_Sgmb0","~g_Sgmb-","~g_Xibr+", "~g_Xib0","~g_Omgb+"," "," "," "]

PythiaRhad.PygiveCommand = []

for i in range(1,20):
    KC = str(400+i)
    PythiaRhad.PygiveCommand += [
        "KCHG("+KC+",1)="+str(q3[(MODEL,CASE)][i-1]),
        "KCHG("+KC+",2)=0",
        "KCHG("+KC+",3)="+str(apflag[(MODEL,CASE)][i-1]),
        "KCHG("+KC+",4)="+str(pdg[(MODEL,CASE)][i-1]),
        "CHAF("+KC+",1)="+names[(MODEL,CASE)][i-1],
        "CHAF("+KC+",2)="+antinames[(MODEL,CASE)][i-1]
        ]

#job.PythiaRhad.PythiaCommand += ["pyinit user lhef",
job.PythiaRhad.PythiaCommand += ["pyinit user madgraph",
                                 "pyinit pylisti -1",
                                 "pyinit pylistf 1",
                                 "pyinit dumpr 1 2",
                                 "pysubs ckin 3 18.",
                                 "pypars mstp 111 0",
                                 ]

job.PythiaRhad.RunGluinoHadrons=True
job.PythiaRhad.RunStopHadrons=False
job.PythiaRhad.RunSbottomHadrons=False
job.PythiaRhad.GluinoBallProbability=GBALLPROB

#PoolSvc = Service("PoolSvc")
#from PoolSvc.PoolSvcConf import PoolSvc
#from AthenaPoolCnvSvc.WriteAthenaPool import AthenaPoolOutputStream
#StreamEVGEN = AthenaPoolOutputStream("StreamEVGEN","madevents.root")
#Stream1.OutputFile = 'events.root'

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
# TruthJet filter
try:
     from JetRec.JetGetters import *
     c4=make_StandardJetGetter('AntiKt',0.4,'Truth')
     c4alg = c4.jetAlgorithmHandle()
except Exception, e:
     pass

from GeneratorFilters.GeneratorFiltersConf import TruthJetFilter
job += TruthJetFilter()

TruthJetFilter = job.TruthJetFilter
try:
    TruthJetFilter.Njet=1;
    TruthJetFilter.NjetMinPt=80.*GeV;
    TruthJetFilter.NjetMaxEta=3;
    TruthJetFilter.jet_pt1=80.*GeV;
    TruthJetFilter.TruthJetContainer="AntiKt4TruthJets";
except NameError:
  pass

try:
     StreamEVGEN.RequireAlgs = [ "TruthJetFilter" ]
except Exception, e:
     pass


