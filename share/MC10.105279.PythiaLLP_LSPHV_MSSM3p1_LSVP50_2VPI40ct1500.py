###########################################################
##  jobOptions for evgen of the process:                 ##
##  MSSM3.1 and LSP --> LSvP vPi vPi                     ##
##                                                       ##
##  author: Orin Harris, U. Washington                   ##
##          omh@cern.ch                                  ##
###########################################################

##create the pyupda file to be read
f = open('susy.hv.pyupda.in','w')
#             PDGID      Name                                      MASS (GeV)   
f.write('   1000022  ~chi_10                             0  0  0   100.00000     0.00001    10.00000  1.00000E-03  2  1\n')
#                   on/off       BR. Frac.  |<------------------Daughters---------------->|
f.write('              1   0     1.000000   5000022   6000111   6000111         0         0\n')
f.write('              1   0     0.000000   5000022   6000111         0         0         0\n')
#             PDGID    Name                                         MASS (GeV)   
f.write('   5000022  ~chi_90                             0  0  0    50.00000     0.01000     0.10000  0.00000E+00  0  0\n')
#             PDGID    Name                                         MASS (GeV)   
f.write('   6000111  ~v_pion                             0  0  0    40.00000     0.01000     0.10000  1.00000E-03  2  1\n')
#                   on/off       BR. Frac.  |<------------------Daughters---------------->|
f.write('              1   0     0.910000         5        -5         0         0         0\n')
f.write('              1   0     0.090000        15       -15         0         0         0\n')
f.close()

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

from PythiaExo_i.PythiaLLP_iConf import PythiaLLP
topAlg += PythiaLLP()
Pythia = topAlg.PythiaLLP

#################### copied from MC10_Pythia_Common.py ##################
Pythia.PythiaCommand = [
    "pypars mstp 5 20090002",  # useAtlasPythiaTune09
    
    # initializations
    "pyinit pylisti 12",
    "pyinit pylistf 1",
    "pystat 1 3 4 5",
    "pyinit dumpr 1 5",
    # mass
    "pydat2 pmas 6 1 172.5",    # PDG2007 TOP mass 
    "pydat2 pmas 24 1 80.403",  # PDG2007 W mass
    "pydat2 pmas 23 1 91.1876", # PDG2007 Z0 mass
    ]
#################### end of MC10_Pythia_Common.py ##################

# SUSY-HV production
Pythia.LLPpyupda = "susy.hv.pyupda.in"
Pythia.PythiaCommand += [ "pysubs msel 0" ] # Full user control/responsibility of turning on/off sub-processes
Pythia.PythiaCommand += [ "pydat1 parj 22 2" ] # relative increase in transverse momentum in a gluon jet generated with MSTJ(2)=2|4
Pythia.PythiaCommand += [ "pydat1 parj 71 500000" ] #max ct of particles
# if photons/tauola are used
Pythia.PythiaCommand += [ "pydat1 parj 90 20000",  # Turn off FSR.
                          "pydat3 mdcy 15 1 0" ]   # Turn off tau decays.

#SUSY settings
Pythia.PythiaCommand += ["pymssm imss 1 1"]   #General MSSM
Pythia.PythiaCommand += [
    "pysubs msub 210 1",  # qq-> ~l ~nu_l 
    "pysubs msub 225 1",  # qq-> chi3 chi40
    "pysubs msub 226 1",  # qq-> chi1+- chi1-+
    "pysubs msub 227 1",  # qq-> chi2+- chi2-+
    "pysubs msub 228 1",  # qq-> chi1+- chi2-+
    "pysubs msub 229 1",  # qq-> chi1 chi1+-
    "pysubs msub 230 1",  # qq-> chi2 chi1+-
    "pysubs msub 231 1",  # qq-> chi3 chi1+-
    "pysubs msub 232 1",  # qq-> chi4 chi1+-
    "pysubs msub 233 1",  # qq-> chi1 chi2+-
    "pysubs msub 234 1",  # qq-> chi2 chi2+-
    "pysubs msub 235 1",  # qq-> chi3 chi2+-
    "pysubs msub 236 1",  # qq-> chi4 chi2+-
    "pymssm rmss 1 180",  # U(1) gaugino mass
    "pymssm rmss 2 200",  # SU(2) gaugino mass
    "pymssm rmss 3 2000", # SU(3) (gluino) mass parameter
    "pymssm rmss 4 330",  # mu, the higgsino mass parameter
    "pymssm rmss 5 10",   # tan beta, the ratio of Higgs expectation values
    "pymssm rmss 13 330", #Left stau mass
    "pymssm rmss 14 330", #right stau mass
    "pymssm rmss 6 330",  # Left slepton mass (sneutrino mass is fixed by a sum rule)
    "pymssm rmss 7 330",  # Right slepton mass
    "pymssm rmss 8 2000", # Left squark mass
    "pymssm rmss 9 2000", # Right squark mass
    "pymssm rmss 10 2000",#Left squark mass for the 3rd generation
    "pymssm rmss 11 2000",#Right sbottom mass
    "pymssm rmss 12 2000",#Right stop mass
    ]

#set the mass of the chi_10 (LSP_{SM})
Pythia.PythiaCommand += ["pydat2 pmas 1000022 1 167.D0"]
#set the mass of the chi_90 (LSP_{Dark})
Pythia.PythiaCommand += ["pydat2 pmas 5000022 1 50.D0"]
#set the mass of the v-pion
Pythia.PythiaCommand += ["pydat2 pmas 6000111 1 40.D0"]
#set the lifetime of the v-pion (c tau in mm)
Pythia.PythiaCommand += ["pydat2 pmas 6000111 4 1500.D0"]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )
# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.95

