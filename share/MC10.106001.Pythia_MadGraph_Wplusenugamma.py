#--------------------------------------------------------------
#Acceptance sample

# W+(enu) + Gamma with full interference between ISR and FSR digram with MadGraph/Pythia
#--------------------------------------------------------------
# File prepared by Zhijun Liang  May 2009
# Non-filtered cross section in Rel. 15.6.1 : 28.0 pb
# No filter , efficiency = 100% safe factor= 0.9

#--------------------------------------------------------------
###############################################################
#
# Job options file
#
#==============================================================
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_PythiaMC09p_Common.py" )
Pythia.PythiaCommand += [ "pyinit user lhef",
                          "pyinit pylisti -1","pyinit pylistf 1","pyinit dumpr 1 2",
                          "pydat1 parj 90 20000", # Turn off FSR.
                          "pydat3 mdcy 15 1 0"   # Turn off tau decays.
]





#--------------------------------------------------------------
# Event related parameters
#--------------------------------------------------------------
#--------------------------------------------------------------
# Algorithms Private Options
#--------------------------------------------------------------


## ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )


## ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


#--------------------------------------------------------------
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
#from GeneratorFilters.GeneratorFiltersConf import PhotonFilter
#topAlg += PhotonFilter()
#PhotonFilter = topAlg.PhotonFilter
#PhotonFilter.Ptcut = 7000.
#PhotonFilter.Etacut = 2.5
#PhotonFilter.NPhotons = 1
#
#from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
#topAlg += LeptonFilter()
#LeptonFilter = topAlg.LeptonFilter
#LeptonFilter.Etacut = 2.5
#
#try:
#    StreamEVGEN.RequireAlgs +=  [ "PhotonFilter" ]
#except Exception, e:
#    pass
#try:
#    StreamEVGEN.RequireAlgs += [ "LeptonFilter" ]
#except Exception, e:
#    pass
##
#

from MC10JobOptions.PythiaEvgenConfig import evgenConfig
#from EvgenJobOptions.MadGraphEvgenConfig import evgenConfig
evgenConfig.generators += ["Lhef", "Pythia"]
evgenConfig.inputfilebase = 'group09.phys-gener.MadGraph442Pythia.106001.Wplusenugamma.TXT.v1'
evgenConfig.efficiency = 0.9
#==============================================================
#
# End of job options file
#
###############################################################

