#--------------------------------------------------------------
# File prepared by Marjorie Shapiro Sept 2010
#--------------------------------------------------------------
# dijet production requiring a D*+ in the final state
# Note: we only force the D*+ and not the D*- so we dont have strange
# correlations in our generated events
# We will also use EvtGen in order to force the D*+-->D0Pi+ and D0-->K-Pi+


include ( "MC10JobOptions/MC10.105011.J2_pythia_jetjet.py" )

# This code sets up EvtGen for inclusive decays
from HepMCTools.HepMCToolsConf import TranslateParticleCodes
topAlg += TranslateParticleCodes()
from EvtGen_i.EvtGen_iConf import EvtInclusiveDecay
topAlg += EvtInclusiveDecay()

TranslateParticleCodes = topAlg.TranslateParticleCodes
TranslateParticleCodes.OutputLevel = 3
TranslateParticleCodes.translateToPDTFrom = "PYTHIA"
TranslateParticleCodes.translateFromPDTTo = "EVTGEN"
TranslateParticleCodes.test = [0,10000000]

EvtInclusiveDecay = topAlg.EvtInclusiveDecay
EvtInclusiveDecay.OutputLevel = 3
EvtInclusiveDecay.userDecayFile = "DstarP2D0PiP_D02Kpi.DEC"
EvtInclusiveDecay.pdtFile = "inclusive.pdt"
EvtInclusiveDecay.decayFile = "inclusive.dec"

# The following is NEEDED with the default parameters of EvtInclusiveDecay
Pythia = topAlg.Pythia
Pythia.PythiaCommand += [
        "pydat1 parj 90 20000",   # prevent photon emission in parton showers
            "pydat1 mstj 26 0"        # turn off B mixing (done by EvtGen)
            ]

#
# We will match our D* to truth jets with cone of 0.6
try:
    from JetRec.JetGetters import *
    c6=make_StandardJetGetter('Cone',0.6,'Truth')
    c6alg = c6.jetAlgorithmHandle()
except Exception, e:
    pass
        

#--------------------------------------------------------------
# Filter on the D*
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import HeavyFlavorHadronFilter
topAlg += HeavyFlavorHadronFilter()
topAlg.HeavyFlavorHadronFilter.OutputLevel=INFO
topAlg.HeavyFlavorHadronFilter.RequestCharm=FALSE
topAlg.HeavyFlavorHadronFilter.RequestBottom=FALSE
topAlg.HeavyFlavorHadronFilter.Request_cQuark=FALSE
topAlg.HeavyFlavorHadronFilter.Request_bQuark=FALSE
topAlg.HeavyFlavorHadronFilter.RequestSpecificPDGID=TRUE
topAlg.HeavyFlavorHadronFilter.PDGID=413
topAlg.HeavyFlavorHadronFilter.RequireTruthJet=TRUE
topAlg.HeavyFlavorHadronFilter.TruthContainerName="Cone6TruthJets"
topAlg.HeavyFlavorHadronFilter.JetPtMin=17.0*GeV
topAlg.HeavyFlavorHadronFilter.JetEtaMax=3.0
topAlg.HeavyFlavorHadronFilter.PDGEtaMax=3.0
topAlg.HeavyFlavorHadronFilter.PDGPtMin=4.0*GeV

try:
    StreamEVGEN.RequireAlgs +=  [ "HeavyFlavorHadronFilter" ]
except Exception, e:
    pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.05
evgenConfig.auxfiles += [ 'inclusive.dec', 'inclusive.pdt', 'DstarP2D0PiP_D02Kpi.DEC' ]
