## Job options file for Herwig++, NLO WW (leptonic decays) production

## Get a handle on the top level algorithms' sequence
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

try:
     include ( "MC10JobOptions/MC10_Herwigpp_NLOME_Common.py" )
except NameError:
     # needed (dummy) default
     # from Herwigpp_i.Herwigpp_iConf import Herwigpp
     # topAlg += Herwigpp()
     cmds =  ""

## Add to commands
cmds += """
## Set up Powheg truncated shower
set /Herwig/Shower/Evolver:HardEmissionMode POWHEG

## Use 2-loop alpha_s
create Herwig::O2AlphaS /Herwig/AlphaQCD_O2
set /Herwig/Generators/LHCGenerator:StandardModelParameters:QCD/RunningAlphaS /Herwig/AlphaQCD_O2

# matrix element
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/PowhegMEPP2VV
set /Herwig/MatrixElements/SimpleQCD:MatrixElements[0]:Process WW

# force decays ivolving taus
set /Herwig/Particles/W+:Synchronized Not_synchronized
set /Herwig/Particles/W-:Synchronized Not_synchronized
#
do /Herwig/Particles/W+:SelectDecayModes W+->nu_tau,tau+;
do /Herwig/Particles/W-:SelectDecayModes W-->nu_ebar,e-; W-->nu_mubar,mu-; 
#
#switch off tau polarisation
set /Herwig/Decays/Tau1Meson:PolarizationOption Force
set /Herwig/Decays/Tau1Meson:TauMinusPolarization 0.0
set /Herwig/Decays/Tau1Meson:TauPlusPolarization 0.0
#
set /Herwig/Decays/Tau2Leptons:PolarizationOption Force
set /Herwig/Decays/Tau2Leptons:TauMinusPolarization 0.0
set /Herwig/Decays/Tau2Leptons:TauPlusPolarization 0.0
#
set /Herwig/Decays/Tau2Meson:PolarizationOption Force
set /Herwig/Decays/Tau2Meson:TauMinusPolarization 0.0
set /Herwig/Decays/Tau2Meson:TauPlusPolarization 0.0
#
set /Herwig/Decays/Tau2MesonPhoton:PolarizationOption Force
set /Herwig/Decays/Tau2MesonPhoton:TauMinusPolarization 0.0
set /Herwig/Decays/Tau2MesonPhoton:TauPlusPolarization 0.0
#
set /Herwig/Decays/Tau3Kaon:PolarizationOption Force
set /Herwig/Decays/Tau3Kaon:TauMinusPolarization 0.0
set /Herwig/Decays/Tau3Kaon:TauPlusPolarization 0.0
#
set /Herwig/Decays/Tau3Meson:PolarizationOption Force
set /Herwig/Decays/Tau3Meson:TauMinusPolarization 0.0
set /Herwig/Decays/Tau3Meson:TauPlusPolarization 0.0
#
set /Herwig/Decays/Tau3Pion:PolarizationOption Force
set /Herwig/Decays/Tau3Pion:TauMinusPolarization 0.0
set /Herwig/Decays/Tau3Pion:TauPlusPolarization 0.0
#
set /Herwig/Decays/Tau4Pion:PolarizationOption Force
set /Herwig/Decays/Tau4Pion:TauMinusPolarization 0.0
set /Herwig/Decays/Tau4Pion:TauPlusPolarization 0.0
#
set /Herwig/Decays/Tau5Pion:PolarizationOption Force
set /Herwig/Decays/Tau5Pion:TauMinusPolarization 0.0
set /Herwig/Decays/Tau5Pion:TauPlusPolarization 0.0
#
set /Herwig/Decays/TauKPi:PolarizationOption Force
set /Herwig/Decays/TauKPi:TauMinusPolarization 0.0
set /Herwig/Decays/TauKPi:TauPlusPolarization 0.0
#
set /Herwig/Decays/Tau1Vector:PolarizationOption Force
set /Herwig/Decays/Tau1Vector:TauMinusPolarization 0.0
set /Herwig/Decays/Tau1Vector:TauPlusPolarization 0.0
"""

## Set commands
topAlg.Herwigpp.Commands = cmds.splitlines()


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.HerwigppEvgenConfig import evgenConfig
#evgenConfig.efficiency = 0.9
#evgenConfig.minevents=100

#==============================================================
#
# End of job options file
#
###############################################################
