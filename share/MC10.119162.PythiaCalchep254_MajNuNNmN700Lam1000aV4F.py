from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

#include ( "MC10JobOptions/MC10_Pythia_Common.py" )
include ( "MC10JobOptions/MC10_PythiaMC09p_Common.py" )# use CTEQ6l PDFs

Pythia.PythiaCommand+= ["pyinit user lhef",
                         "pydat3 mdcy 15 1 0",
                         "pydat1 parj 90 20000"
                        ]
# Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

from MC10JobOptions.LhefEvgenConfig import evgenConfig

# dummy needed
evgenConfig.inputfilebase = 'Calchep254'
try:
  if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputfilebase ='group09.phys-gener.Calchep254.119162.MajNuNNmN700Lam1000aV4F.TXT.v1'
  if runArgs.ecmEnergy == 8000.0:
    evgenConfig.inputfilebase ='group09.phys-gener.Calchep254.119162.MajNuNNmN700Lam1000aV4F_8TeV.TXT.v1'
except NameError:
  pass

