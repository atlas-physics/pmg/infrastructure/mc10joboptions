## Herwig++ job option file for Susy SU3 2-parton -> 2-sparticle processes

## Get a handle on the top level algorithms' sequence
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

try:
     include ( "MC10JobOptions/MC10_Herwigpp_Common.py" )
except NameError:
     # needed (dummy) default
     # from Herwigpp_i.Herwigpp_iConf import Herwigpp
     # topAlg += Herwigpp()
     cmds =  ""
	
	
## Add Herwig++ parameters for this process
cmds += """

## Generate the process in MSSM equivalent to 2-parton -> 2-sparticle processes in Fortran Herwig
## Read the MUED model details
read MUED.model
cd /Herwig/NewPhysics

#incoming parton
insert HPConstructor:Incoming 0 /Herwig/Particles/g
insert HPConstructor:Incoming 1 /Herwig/Particles/u
insert HPConstructor:Incoming 2 /Herwig/Particles/ubar
insert HPConstructor:Incoming 3 /Herwig/Particles/d
insert HPConstructor:Incoming 4 /Herwig/Particles/dbar
insert HPConstructor:Incoming 5 /Herwig/Particles/s
insert HPConstructor:Incoming 6 /Herwig/Particles/sbar
insert HPConstructor:Incoming 7 /Herwig/Particles/c
insert HPConstructor:Incoming 8 /Herwig/Particles/cbar
insert HPConstructor:Incoming 9 /Herwig/Particles/b
insert HPConstructor:Incoming 10 /Herwig/Particles/bbar

#outgoing KK gluon
insert HPConstructor:Outgoing 0 /Herwig/Particles/KK1_g
#lefthanded KK quarks
insert HPConstructor:Outgoing 1 /Herwig/Particles/KK1_u_L
insert HPConstructor:Outgoing 2 /Herwig/Particles/KK1_u_Lbar
insert HPConstructor:Outgoing 3 /Herwig/Particles/KK1_d_L
insert HPConstructor:Outgoing 4 /Herwig/Particles/KK1_d_Lbar
insert HPConstructor:Outgoing 5 /Herwig/Particles/KK1_c_L
insert HPConstructor:Outgoing 6 /Herwig/Particles/KK1_c_Lbar
insert HPConstructor:Outgoing 7 /Herwig/Particles/KK1_s_L
insert HPConstructor:Outgoing 8 /Herwig/Particles/KK1_s_Lbar
insert HPConstructor:Outgoing 9 /Herwig/Particles/KK1_b_1
insert HPConstructor:Outgoing 10 /Herwig/Particles/KK1_b_1bar
insert HPConstructor:Outgoing 11 /Herwig/Particles/KK1_t_1
insert HPConstructor:Outgoing 12 /Herwig/Particles/KK1_t_1bar
#righthanded KK quarks
insert HPConstructor:Outgoing 13 /Herwig/Particles/KK1_u_R
insert HPConstructor:Outgoing 14 /Herwig/Particles/KK1_u_Rbar
insert HPConstructor:Outgoing 15 /Herwig/Particles/KK1_d_R
insert HPConstructor:Outgoing 16 /Herwig/Particles/KK1_d_Rbar
insert HPConstructor:Outgoing 17 /Herwig/Particles/KK1_c_R
insert HPConstructor:Outgoing 18 /Herwig/Particles/KK1_c_Rbar
insert HPConstructor:Outgoing 19 /Herwig/Particles/KK1_s_R
insert HPConstructor:Outgoing 20 /Herwig/Particles/KK1_s_Rbar
insert HPConstructor:Outgoing 21 /Herwig/Particles/KK1_b_2
insert HPConstructor:Outgoing 22 /Herwig/Particles/KK1_b_2bar
insert HPConstructor:Outgoing 23 /Herwig/Particles/KK1_t_2
insert HPConstructor:Outgoing 24 /Herwig/Particles/KK1_t_2bar
# KK leptons
insert HPConstructor:Outgoing 25 /Herwig/Particles/KK1_e_L-
insert HPConstructor:Outgoing 26 /Herwig/Particles/KK1_nu_eL
insert HPConstructor:Outgoing 27 /Herwig/Particles/KK1_mu_L-
insert HPConstructor:Outgoing 28 /Herwig/Particles/KK1_nu_muL
insert HPConstructor:Outgoing 29 /Herwig/Particles/KK1_tau_1-
insert HPConstructor:Outgoing 30 /Herwig/Particles/KK1_nu_tauL
insert HPConstructor:Outgoing 31 /Herwig/Particles/KK1_e_R-
insert HPConstructor:Outgoing 32 /Herwig/Particles/KK1_mu_R-
insert HPConstructor:Outgoing 33 /Herwig/Particles/KK1_tau_2-
# KK antileptons
insert HPConstructor:Outgoing 34 /Herwig/Particles/KK1_e_L+
insert HPConstructor:Outgoing 35 /Herwig/Particles/KK1_nu_eLbar
insert HPConstructor:Outgoing 36 /Herwig/Particles/KK1_mu_L+
insert HPConstructor:Outgoing 37 /Herwig/Particles/KK1_nu_muLbar
insert HPConstructor:Outgoing 38 /Herwig/Particles/KK1_tau_1+
insert HPConstructor:Outgoing 39 /Herwig/Particles/KK1_nu_tauLbar
insert HPConstructor:Outgoing 40 /Herwig/Particles/KK1_e_R+
insert HPConstructor:Outgoing 41 /Herwig/Particles/KK1_mu_R+
insert HPConstructor:Outgoing 42 /Herwig/Particles/KK1_tau_2+
# KK bosons
insert HPConstructor:Outgoing 43 /Herwig/Particles/KK1_Z0
insert HPConstructor:Outgoing 44 /Herwig/Particles/KK1_W+
insert HPConstructor:Outgoing 45 /Herwig/Particles/KK1_W-
insert HPConstructor:Outgoing 46 /Herwig/Particles/KK1_gamma
# KK Higgs
insert HPConstructor:Outgoing 47 /Herwig/Particles/KK1_h0
insert HPConstructor:Outgoing 48 /Herwig/Particles/KK1_A0
insert HPConstructor:Outgoing 49 /Herwig/Particles/KK1_H+
insert HPConstructor:Outgoing 50 /Herwig/Particles/KK1_H-

# set the UED parameters
set MUED/Model:InverseRadius 450.*GeV
set MUED/Model:LambdaR 2

"""
## Set the command vector
topAlg.Herwigpp.Commands = cmds.splitlines()

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.HerwigppEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
#evgenConfig.minevents=100
 
from MC10JobOptions.UEDEvgenConfig import evgenConfig

#==============================================================
#
# End of job options file
#
###############################################################
