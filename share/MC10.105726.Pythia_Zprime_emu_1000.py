#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
#--------------------------------------------------------------
# Algorithms Private Options
#--------------------------------------------------------------
# Main generator: Pythia
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )
Pythia.PythiaCommand += ["pysubs msel 0","pysubs msub 141 1", "pysubs ckin 1 750", "pysubs ckin 2 1250"]
Pythia.PythiaCommand += ["pydat2 pmas 32 1 1000.", "pydat3 mdme 289 1 0",
                         "pydat3 mdme 290 1 0", "pydat3 mdme 291 1 0",
                         "pydat3 mdme 292 1 0", "pydat3 mdme 293 1 0",
                         "pydat3 mdme 294 1 0", "pydat3 mdme 297 1 1",#change 297
                         "pydat3 mdme 298 1 0", "pydat3 mdme 300 1 0",
                         "pydat3 mdme 301 1 0", "pydat3 mdme 302 1 0",
			 "pyinit win 7000"]
			 #"pydat3 mdme 174 1 0", "pydat3 mdme 175 1 0",
                         #"pydat3 mdme 176 1 0", "pydat3 mdme 177 1 0",
                         #"pydat3 mdme 178 1 0", "pydat3 mdme 179 1 0",
                         #"pydat3 mdme 182 1 1", "pydat3 mdme 183 1 0",#change 182
                         #"pydat3 mdme 185 1 0", "pydat3 mdme 186 1 0",
			 #"pydat3 mdme 187 1 0", "pydat3 mdme 162 1 0",
                         #"pydat3 mdme 163 1 0", "pydat3 mdme 164 1 0",
                         #"pydat3 mdme 165 1 0", "pydat3 mdme 166 1 0",
			 #"pydat3 mdme 170 1 1", "pydat3 mdme 172 1 0",#change 170
			 #"pydat3 mdme 82 1 0", "pydat3 mdme 83 1 0",
                         #"pydat3 mdme 85 1 0", "pydat3 mdme 86 1 0",
			 #"pydat3 mdme 84 1 0", "pydat3 mdme 76 1 0", 
                         #"pydat3 mdme 77 1 0", "pydat3 mdme 78 1 0",
			 #"pydat3 mdme 79 1 0"]
#Pythia.PythiaCommand += ["pydat3 kfdp 182 1 11", "pydat3 kfdp 182 2 13",
	#		"pydat3 kfdp 184 1 11", "pydat3 kfdp 184 2 13",
		#	"pydat3 kfdp 170 1 11", "pydat3 kfdp 170 2 13",
			#"pydat3 kfdp 171 1 11", "pydat3 kfdp 297 2 13",
			#"pydat3 kfdp 299 1 11", "pydat3 kfdp 299 2 13"]
Pythia.PythiaCommand += ["pydat3 kfdp 297 1 -11", "pydat3 kfdp 297 2 13",
			"pydat3 kfdp 299 1 11", "pydat3 kfdp 299 2 -13"]
Pythia.PythiaCommand += ["pyinit pylisti 12"]
#Pythia.PythiaCommand += ["pypars mstp 51 29000", "pypars mstp 52 2",
 #                        "pypars mstp 53 29000", "pypars mstp 54 2",
 	#	         "pypars mstp 55 29000", "pypars mstp 56 2"]


Pythia.PythiaCommand += [
       #                  "pydat1 parj 90 20000.",  ## Turn off FSR
                         "pydat3 mdcy 15 1 0"]     ## Turn off tau decays

include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )
# include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
topAlg += MultiLeptonFilter()

MultiLeptonFilter = topAlg.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 5000.
MultiLeptonFilter.Etacut = 2.8
MultiLeptonFilter.NLeptons = 2

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------

try:
     StreamEVGEN.RequireAlgs += [ "MultiLeptonFilter" ]
except Exception, e:
     pass


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
theApp.Dlls +=["TruthExamples"]
#theApp.TopAlg +=["PrintMC"]

PrintMC = Algorithm("PrintMC")
PrintMC.McEventKey = "GEN_EVENT"
PrintMC.VerboseOutput = TRUE
PrintMC.PrintStyle = "Barcode"
PrintMC.FirstEvent = 1
PrintMC.LastEvent = 2
