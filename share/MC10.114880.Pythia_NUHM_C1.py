####################################################################################
####################################################################################
#                                                                                  #
# NUHM point: C1 (mH1^2=1.5 TeV^2, m1/2=350 GeV)                                   #
# Model description: arXiv:hep-ph/0703130                                          #
# "Collider signatures of gravitino dark matter with                               #
# a sneutrino NLSP" by Laura Covi and Sabine Kraml                                 #
#                                                                                  #
# contact :  Judita Mamuzic                                                        #
#                                                                                  #
#                                                                                  #
####################################################################################
####################################################################################

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

####################################################################################

#####################################################################################
# PRODUCTION CHANNEL SELECTION, CME AND LESHOUCHES INPUT :                          #
#####################################################################################
Pythia.PythiaCommand += [
	"pysubs msel 39",
	"pymssm imss 1 11",
        "pydat1 mstu 41 2."
                       ]

#####################################################################################
# PYTHIA CONTROL OUTPUT SETTING :                                                   #
#####################################################################################
Pythia.PythiaCommand += [
# here added to print out the set parameters
	"pyinit pylisti 13"]
#####################################################################################
# INCLUDES :                                                                        #
#####################################################################################
Pythia.SusyInputFile = "susy_NUHM_C1_slha.txt" #SLHA input file

Pythia.PythiaCommand += [
                         "pydat1 parj 90 20000",   # Turn off FSR
                         "pydat3 mdcy 15 1 0"      # Turn off tau decays
                         ]
# ... Tauola 
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )  
# ... Photos 
include ( "MC10JobOptions/MC10_Photos_Fragment.py" ) 

#####################################################################################
# FILTER EFFICIENCY :                                                               #
#####################################################################################
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9

# SUSYEvgenConfig
from MC10JobOptions.SUSYEvgenConfig import evgenConfig

# End of job options file
