###############################################################
#
# Job options file for min bias (ND)
#
# author: C. Gwenlan (June'10); based on L. Mijovic's studies
# FSR systematics sample: more FSR activity, external LO process + PYTHIA                                      
# PARP(72) and PARJ(82) are varied                                                                             
# AMBT1 default values: PARP(72)=0.192 GeV, PARJ(82)=1.GeV                                                      
# PARP(72) :  lambda_FSR                                                                                      
# PARJ(82) :  FSR regularization IR cutoff                                                                    
#             see Pythia Manual for more info                                                                 
#
# reference JO: MC10.105001.pythia_minbias.py
#===============================================================

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 


include ( "MC10JobOptions/MC10_Pythia_Common.py" )

Pythia.PythiaCommand += [  "pysubs msel 1" ]

Pythia.PythiaCommand += [ "pypars mstp 3 1",      #set mstp 3 1 (setting Lambda in alpha_s)
                          "pypars parp 1 0.192",  #reset the defaults after mstp 3 1
                          "pypars parp 72 0.384", #FSR Labmda value in running alpha_s (ATLAS def 0.192)
                          "pydat1 paru 112 0.192",#reset defaults after mstp 3 1
                          "pydat1 mstu 112 4",    #reset defaults after mstp 3 1
                          "pypars parp 61 0.192"] #reset defaults after mstp 3 1

Pythia.PythiaCommand +=[ "pydat1 parj 82 0.5" ]   


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig

evgenConfig.efficiency = 0.9


#==============================================================
#
# End of job options file
#
###############################################################

