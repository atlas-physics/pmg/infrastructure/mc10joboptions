###############################################################
#
# Job options file
#
# Pythia J4 event generation with pT-deltaY weighting.
# cross section as default pythia sample = 8.77*10^1 nb
#
# Prepared by Graham Jones, Graham.Jones [at] cern.ch 
#==============================================================


# dijet production with pythia
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

Pythia.PythiaCommand += [
			      "pysubs msel 0",
			      "pysubs ckin 3 140.",
			      "pysubs ckin 4 280.",
			      "pysubs msub 11 1",
			      "pysubs msub 12 1",
			      "pysubs msub 13 1",
			      "pysubs msub 68 1",
			      "pysubs msub 28 1",
			      "pysubs msub 53 1"]

#--------------------------------------------------------------
# Truth Jets
#--------------------------------------------------------------

# Make truth jets for JetForwardFilter:
from AthenaCommon.AppMgr import ServiceMgr as svcMgr
include( "AthenaCommon/Atlas_Gen.UnixStandardJob.py" )
from JetRec.JetGetters import *
Kt6Alg = make_StandardJetGetter('AntiKt',0.6,'Truth').jetAlgorithmHandle()
Kt6Alg.AlgTools['JetFinalPtCut'].MinimumSignal = 7.0*GeV

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

## Truth filter
# Add the filter to the correct sequence!
from GeneratorFilters.GeneratorFiltersConf import JetForwardFilter
forwardFilter=JetForwardFilter()
topAlg += forwardFilter

# General properties
forwardFilter.JetContainer = "AntiKt6TruthJets"

# Kinematic cuts
forwardFilter.MinDeltaY = 0.0
forwardFilter.MaxDeltaY = 100.0
forwardFilter.MinPt1 = 12.0 #GeV
forwardFilter.MinPt2 = 12.0
forwardFilter.MaxPt1 = 70000.0
forwardFilter.MaxPt2 = 70000.0

#Gaussian mixture model describing pdf of unweighted sample
forwardFilter.muXs = [ 159.275058626, 155.645881324, 171.867197633, 201.747938704, 240.837882812  ]
forwardFilter.muYs = [ 1.03189266858, 2.17909035299, 1.00765451785, 1.37604137583, 1.37245227245  ]
forwardFilter.sigmaXs = [ 16.856749924, 19.7956420505, 32.1524689514, 36.8247501374, 47.2882037785  ]
forwardFilter.sigmaYs = [ 0.676767606115, 1.10398470905, 0.669939566033, 0.910721599951, 0.932519989244  ]
forwardFilter.rhos = [ -0.0295220920674, 0.0413168419108, -0.0124643385239, -0.0142615434262, -0.0727598278409  ]
forwardFilter.weights = [ 0.277416979387, 0.203964382325, 0.259609251301, 0.18847133892, 0.0705380480676  ]

#Target probability (how much weighting required roughly)
#Lower = more suppression
import math
forwardFilter.SuppressionFactor = 5.0*math.pow(10.0, -8.0)

#--------------------------------------------------------------
# Apply filtering
#--------------------------------------------------------------

try:
  StreamEVGEN.RequireAlgs += [ "JetForwardFilter" ]
except Exception, e:
  pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
# N_gen = 288813735; N_pass = 100000 -> eff = 3.46243*10^-4
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.00034624
evgenConfig.minevents=1000 #~16hours
evgenConfig.weighting=0
