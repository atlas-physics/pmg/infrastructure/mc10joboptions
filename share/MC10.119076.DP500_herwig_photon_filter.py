
#--------------------------------------------------------------
# Author  : Lydia Roos (from MC10.115872.DP17_herwig_photon_filter.py)
#
# Purpose : Inclusive photon sample (brem+hard process). 
#
#--------------------------------------------------------------

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence( "TopAlg" ) 

try:
    if runArgs.ecmEnergy == 7000.0:
        include ( "MC10JobOptions/MC10_Herwig_Common_7TeV.py" )
    if runArgs.ecmEnergy == 10000.0:
        include ( "MC10JobOptions/MC10_Herwig_Common.py" )
except NameError:
    # needed (dummy) default
    from Herwig_i.Herwig_iConf import Herwig
    topAlg += Herwig()
    Herwig = topAlg.Herwig

Herwig.HerwigCommand += [ "iproc 11500",
                          "ptmin 495." ]

#-------------------------------------------------------------
# Apply Direct Photon Filter
#-------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import DirectPhotonFilter
topAlg += DirectPhotonFilter()

DirectPhotonFilter = topAlg.DirectPhotonFilter
DirectPhotonFilter.Ptcut = 500000.
DirectPhotonFilter.Etacut =  2.7
DirectPhotonFilter.NPhotons = 1

#--------------------------------------------------------------
# Pool Output Options
#--------------------------------------------------------------
try:
    StreamEVGEN.RequireAlgs += ["DirectPhotonFilter"]
except Exception, e:
    pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------

from MC10JobOptions.HerwigEvgenConfig import evgenConfig
# efficiency = 100/279315 *.9 = 0.0003
evgenConfig.efficiency = 0.0003
evgenConfig.minevents = 100
