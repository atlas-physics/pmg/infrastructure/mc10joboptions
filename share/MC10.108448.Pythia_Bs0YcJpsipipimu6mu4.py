###############################################################
# PRODUCTION SYSTEM FRAGMENT
#       jobOptions for production of Bs0 -> Yc(4260) phi,
#                      Yc -> J/psi pi+pi-, J/psi -> mu+mu-,
#                      phi -> K+K-
#
#==============================================================

#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
#evgenConfig.efficiency = 0.03

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

from GeneratorFilters.GeneratorFiltersConf import MultiMuonFilter
topAlg += MultiMuonFilter()

from GeneratorFilters.GeneratorFiltersConf import MuonFilter
topAlg += MuonFilter()

# AtRndmGenSvc.ReadFromFile = true;
Pythia = topAlg.Pythia

# Bs0 -> Yc(4260) phi, Yc -> J/psi pi+pi-, J/psi -> mu+mu-, phi -> K+K-
Pythia.PythiaCommand += [
    "pysubs msel 0",             #  turn OFF global process selection
    "pysubs msub 86 1",	         #  g+g -> J/psi+g  turned ON
    "pyint2 kfpr 86 1 531",   #  request B_s0 instead of J/psi
    "pydat3 mdme 953 1 0",    #  B_s0 -> nu_e e+ D_s- turned OFF
    "pydat3 mdme 954 1 0",    #  B_s0 -> nu_e e+ D*_s- turned OFF
    "pydat3 mdme 955 1 0",    #  B_s0 -> nu_e e+ D_1s- turned OFF
    "pydat3 mdme 956 1 0",    #  B_s0 -> nu_e e+ D*_0s- turned OFF
    "pydat3 mdme 957 1 0",    #  B_s0 -> nu_e e+ D*_1s- turned OFF
    "pydat3 mdme 958 1 0",    #  B_s0 -> nu_e e+ D*_2s- turned OFF
    "pydat3 mdme 959 1 0",    #  B_s0 -> nu_mu mu+ D_s- turned OFF
    "pydat3 mdme 960 1 0",    #  B_s0 -> nu_mu mu+ D*_s- turned OFF
    "pydat3 mdme 961 1 0",    #  B_s0 -> nu_mu mu+ D_1s- turned OFF
    "pydat3 mdme 962 1 0",    #  B_s0 -> nu_mu mu+ D*_0s- turned OFF
    "pydat3 mdme 963 1 0",    #  B_s0 -> nu_mu mu+ D*_1s- turned OFF
    "pydat3 mdme 964 1 0",    #  B_s0 -> nu_mu mu+ D*_2s- turned OFF
    "pydat3 mdme 965 1 0",    #  B_s0 -> nu_tau tau+ D_s- turned OFF
    "pydat3 mdme 966 1 0",    #  B_s0 -> nu_tau tau+ D*_s- turned OFF
    "pydat3 mdme 967 1 0",    #  B_s0 -> D_s- pi+ turned OFF
    "pydat3 mdme 968 1 0",    #  B_s0 -> D_s- rho+ turned OFF
    "pydat3 mdme 969 1 0",    #  B_s0 -> D_s- a_1+ turned OFF
    "pydat3 mdme 970 1 0",    #  B_s0 -> D*_s- pi+ turned OFF
    "pydat3 mdme 971 1 0",    #  B_s0 -> D*_s- rho+ turned OFF
    "pydat3 mdme 972 1 0",    #  B_s0 -> D*_s- a_1+ turned OFF
    "pydat3 mdme 973 1 0",    #  B_s0 -> D_s- D_s+ turned OFF
    "pydat3 mdme 974 1 0",    #  B_s0 -> D_s- D*_s+ turned OFF
    "pydat3 mdme 975 1 0",    #  B_s0 -> D*_s- D_s+ turned OFF
    "pydat3 mdme 976 1 0",    #  B_s0 -> D*_s- D*_s+ turned OFF
    "pydat3 mdme 977 1 0",    #  B_s0 -> eta_c eta turned OFF
    "pydat3 mdme 978 1 0",    #  B_s0 -> eta_c eta' turned OFF
    "pydat3 mdme 979 1 0",    #  B_s0 -> eta_c phi turned OFF
    "pydat3 mdme 980 1 0",    #  B_s0 -> J/psi eta turned OFF
    "pydat3 mdme 981 1 0",    #  B_s0 -> J/psi eta' turned OFF
    "pydat3 mdme 982 1 1",    #  B_s0 -> J/psi phi turned ON
    "pydat3 mdme 983 1 0",    #  B_s0 -> chi_1c eta turned OFF
    "pydat3 mdme 984 1 0",    #  B_s0 -> chi_1c eta' turned OFF
    "pydat3 mdme 985 1 0",    #  B_s0 -> chi_1c phi turned OFF
    "pydat3 mdme 986 1 0",    #  B_s0 -> u dbar cbar s turned OFF
    "pydat3 mdme 987 1 0",    #  B_s0 -> u cbar dbar s turned OFF
    "pydat3 mdme 988 1 0",    #  B_s0 -> c sbar cbar s turned OFF
    "pydat3 mdme 989 1 0",    #  B_s0 -> c cbar sbar s turned OFF
    "pydat3 mdme 990 1 0",    #  B_s0 -> u dbar ubar s turned OFF
    "pydat3 mdme 991 1 0",    #  B_s0 -> c sbar ubar s turned OFF
    "pydat3 kfdp 982 1 100443",   #  request Psi' instead of J/psi
    "pydat2 pmas 100443 1 4.26", # set Psi(2S) mass to 4.26 GeV
    "pydat3 mdme 1567 1 0",      #  Psi' -> ee turned OFF
    "pydat3 mdme 1568 1 0",      #  Psi' -> mumu turned OFF
    "pydat3 mdme 1569 1 0",      #  Psi' -> random turned OFF
    "pydat3 mdme 1570 1 1",      #  Psi' -> J/psi pi+pi- turned ON
    "pydat3 mdme 1571 1 0",      #  Psi' -> J/psi pi0pi0 turned OFF
    "pydat3 mdme 1572 1 0",      #  Psi' -> J/psi eta turned OFF
    "pydat3 mdme 1573 1 0",      #  Psi' -> J/psi pi0 turned OFF
    "pydat3 mdme 1574 1 0",      #  Psi' -> chi_0c gamma turned OFF
    "pydat3 mdme 1575 1 0",      #  Psi' -> chi_1c gamma turned OFF
    "pydat3 mdme 1576 1 0",      #  Psi' -> chi_2c gamma turned OFF
    "pydat3 mdme 1577 1 0",      #  Psi' -> eta_c gamma turned OFF
    "pydat3 mdme 656 1 1",    #  phi -> K+ K- turned ON
    "pydat3 mdme 657 1 0",    #  phi -> K_L0 K_S0 turned OFF
    "pydat3 mdme 658 1 0",    #  phi -> rho- pi+ turned OFF
    "pydat3 mdme 659 1 0",    #  phi -> rho0 pi0 turned OFF
    "pydat3 mdme 660 1 0",    #  phi -> rho+ pi- turned OFF
    "pydat3 mdme 661 1 0",    #  phi -> pi+ pi- pi0 turned OFF
    "pydat3 mdme 662 1 0",    #  phi -> gamma eta turned OFF
    "pydat3 mdme 663 1 0",    #  phi -> pi0 gamma turned OFF
    "pydat3 mdme 664 1 0",    #  phi -> e- e+ turned OFF
    "pydat3 mdme 665 1 0",    #  phi -> mu- mu+ turned OFF
    "pydat3 mdme 666 1 0",    #  phi -> pi+ pi- turned OFF
    "pydat3 mdme 858 1 0",    #  J/psi -> ee turned OFF
    "pydat3 mdme 859 1 1",    #  J/psi -> mumu turned ON
    "pydat3 mdme 860 1 0"     #  J/psi -> random turned OFF
    ]


Pythia.PythiaCommand += ["pyinit pylistf 1",

                         "pysubs ckin 3 10.", # lower pT cut on hard process in 10 GeV
                         
                         "pystat mstat 1",
                         "pyinit dumpr 0 10",#dump this event range to screen
                             
                         ]


MultiMuonFilter = topAlg.MultiMuonFilter
MultiMuonFilter.Ptcut = 4000.
MultiMuonFilter.Etacut = 2.5
MultiMuonFilter.NMuons = 2

MuonFilter = topAlg.MuonFilter
MuonFilter.Ptcut = 6000.
MuonFilter.Etacut = 2.5

try:
     StreamEVGEN.RequireAlgs += ["MultiMuonFilter"]
     StreamEVGEN.RequireAlgs += ["MuonFilter"]
except Exception, e:
     pass

#==============================================================
#
# End of job options file
#
###############################################################
