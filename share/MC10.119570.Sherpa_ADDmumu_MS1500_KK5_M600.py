from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence('TopAlg')
from Sherpa_i.Sherpa_iConf import Sherpa_i
sherpa = Sherpa_i()
topAlg += sherpa
from MC10JobOptions.SherpaFEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'group10.phys-gener.sherpa010203.119570.Sherpa_ADDmumu_MS1500_KK5_M600_7TeV.TXT.v1'
evgenConfig.efficiency = 0.9
evgenConfig.weighting = 0
