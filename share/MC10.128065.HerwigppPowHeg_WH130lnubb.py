## Get a handle on the top level algorithms' sequence
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

try:
    include ( "MC10JobOptions/MC10_Herwigpp_NLOME_Common.py" )
except NameError:
    # needed (dummy) default
    cmds =  ""
                    
## Add to commands
cmds += """
## Set up Powheg truncated shower
set /Herwig/Shower/Evolver:HardEmissionMode POWHEG
## Use 2-loop alpha_s
create Herwig::O2AlphaS /Herwig/AlphaQCD_O2
set /Herwig/Generators/LHCGenerator:StandardModelParameters:QCD/RunningAlphaS /Herwig/AlphaQCD_O2

## Set up Higgs + W process at NLO (set jet pT cut to zero so no cut on Z decay products)
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/PowhegMEPP2WH
set /Herwig/MatrixElements/SimpleQCD:MatrixElements[0]:FactorizationScaleOption Dynamic
set /Herwig/Cuts/JetKtCut:MinKT 0.0*GeV

## Higgs particle
set /Herwig/Particles/h0:NominalMass 130.*GeV
set /Herwig/Particles/h0:Width 0.00347*GeV
set /Herwig/Particles/h0:WidthCut 0.00347*GeV
set /Herwig/Particles/h0:WidthLoCut 0.00347*GeV
set /Herwig/Particles/h0:WidthUpCut 0.00347*GeV
set /Herwig/Particles/h0/h0->W+,W-;:OnOff 0
set /Herwig/Particles/h0/h0->Z0,Z0;:OnOff 0
set /Herwig/Particles/h0/h0->b,bbar;:OnOff 1
set /Herwig/Particles/h0/h0->c,cbar;:OnOff 0
set /Herwig/Particles/h0/h0->g,g;:OnOff 0
set /Herwig/Particles/h0/h0->gamma,gamma;:OnOff 0
set /Herwig/Particles/h0/h0->mu-,mu+;:OnOff 0
set /Herwig/Particles/h0/h0->t,tbar;:OnOff 0
set /Herwig/Particles/h0/h0->tau-,tau+;:OnOff 0

## W boson
set /Herwig/Particles/W+:NominalMass 80.403*GeV
set /Herwig/Particles/W+:Width 2.141*GeV
set /Herwig/Particles/W+:WidthCut 21.41*GeV
set /Herwig/Particles/W+:WidthLoCut 21.41*GeV
set /Herwig/Particles/W+:WidthUpCut 21.41*GeV
set /Herwig/Particles/W+/W+->bbar,c;:OnOff 0
set /Herwig/Particles/W+/W+->c,dbar;:OnOff 0
set /Herwig/Particles/W+/W+->c,sbar;:OnOff 0
set /Herwig/Particles/W+/W+->nu_e,e+;:OnOff 1
set /Herwig/Particles/W+/W+->nu_mu,mu+;:OnOff 1
set /Herwig/Particles/W+/W+->nu_tau,tau+;:OnOff 1
set /Herwig/Particles/W+/W+->sbar,u;:OnOff 0
set /Herwig/Particles/W+/W+->u,dbar;:OnOff 0
set /Herwig/Particles/W-:NominalMass 80.403*GeV
set /Herwig/Particles/W-:Width 2.141*GeV
set /Herwig/Particles/W-:WidthCut 21.41*GeV
set /Herwig/Particles/W-:WidthLoCut 21.41*GeV
set /Herwig/Particles/W-:WidthUpCut 21.41*GeV
set /Herwig/Particles/W-/W-->b,cbar;:OnOff 0
set /Herwig/Particles/W-/W-->cbar,d;:OnOff 0
set /Herwig/Particles/W-/W-->cbar,s;:OnOff 0
set /Herwig/Particles/W-/W-->nu_ebar,e-;:OnOff 1
set /Herwig/Particles/W-/W-->nu_mubar,mu-;:OnOff 1
set /Herwig/Particles/W-/W-->nu_taubar,tau-;:OnOff 1
set /Herwig/Particles/W-/W-->s,ubar;:OnOff 0
set /Herwig/Particles/W-/W-->ubar,d;:OnOff 0

## Z boson
set /Herwig/Particles/Z0:NominalMass 91.1876*GeV
set /Herwig/Particles/Z0:Width 2.4952*GeV
set /Herwig/Particles/Z0:WidthCut 24.952*GeV
set /Herwig/Particles/Z0:WidthLoCut 24.952*GeV
set /Herwig/Particles/Z0:WidthUpCut 24.952*GeV

## top
set /Herwig/Particles/t:NominalMass 172.5*GeV
set /Herwig/Particles/tbar:NominalMass 172.5*GeV

"""

## Set commands
topAlg.Herwigpp.Commands = cmds.splitlines()


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.HerwigppEvgenConfig import evgenConfig

#==============================================================
#
# End of job options file
#
############################################################### 
