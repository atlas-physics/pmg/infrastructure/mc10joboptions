###############################################################
# PRODUCTION SYSTEM FRAGMENT
#       jobOptions for production of Yb(12 GeV)-> Upsilon(1S) pi+pi-,
#                                    Upsilon(1S) -> mu+mu-
#
#==============================================================

#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
#evgenConfig.efficiency = 0.006

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

from GeneratorFilters.GeneratorFiltersConf import MultiMuonFilter
topAlg += MultiMuonFilter()

from GeneratorFilters.GeneratorFiltersConf import MuonFilter
topAlg += MuonFilter()

# AtRndmGenSvc.ReadFromFile = true;
Pythia = topAlg.Pythia

# Yb(12 GeV)-> Upsilon(1S) pi+pi-, Upsilon(1S) -> mu+mu-
Pythia.PythiaCommand += [
    "pysubs msel 0",             #  turn OFF global process selection
    "pysubs msub 86 1",	         #  g+g -> J/psi+g turned ON
    "pyint2 kfpr 86 1 100553",   #  request Upsilon' instead of J/psi
    "pydat2 pmas 100553 1 12.0", #  set Upsilon(2S) mass to 12.0 GeV
    "pydat3 mdme 1578 1 0",      #  Upsilon' -> e- e+ turned OFF
    "pydat3 mdme 1579 1 0",      #  Upsilon' -> mu- mu+ turned OFF
    "pydat3 mdme 1580 1 0",      #  Upsilon' -> tau- tau+ turned OFF
    "pydat3 mdme 1581 1 0",      #  Upsilon' -> d dbar turned OFF
    "pydat3 mdme 1582 1 0",      #  Upsilon' -> u ubar turned OFF
    "pydat3 mdme 1583 1 0",      #  Upsilon' -> s sbar turned OFF
    "pydat3 mdme 1584 1 0",      #  Upsilon' -> c cbar turned OFF
    "pydat3 mdme 1585 1 0",      #  Upsilon' -> g g g turned OFF
    "pydat3 mdme 1586 1 0",      #  Upsilon' -> gamma g g turned OFF
    "pydat3 mdme 1587 1 1",      #  Upsilon' -> Upsilon pi+ pi- turned ON
    "pydat3 mdme 1588 1 0",      #  Upsilon' -> Upsilon pi0 pi0 turned OFF
    "pydat3 mdme 1589 1 0",      #  Upsilon' -> chi_0b gamma turned OFF
    "pydat3 mdme 1590 1 0",      #  Upsilon' -> chi_1b gamma turned OFF
    "pydat3 mdme 1591 1 0",      #  Upsilon' -> chi_2b gamma turned OFF
    "pydat3 mdme 1034 1 0",      #  Upsilon -> e- e+ turned OFF
    "pydat3 mdme 1035 1 1",      #  Upsilon -> mu- mu+ turned ON
    "pydat3 mdme 1036 1 0",      #  Upsilon -> tau- tau+ turned OFF
    "pydat3 mdme 1037 1 0",      #  Upsilon -> d dbar turned OFF
    "pydat3 mdme 1038 1 0",      #  Upsilon -> u ubar turned OFF
    "pydat3 mdme 1039 1 0",      #  Upsilon -> s sbar turned OFF
    "pydat3 mdme 1040 1 0",      #  Upsilon -> c cbar turned OFF
    "pydat3 mdme 1041 1 0",      #  Upsilon -> g g g turned OFF
    "pydat3 mdme 1042 1 0"       #  Upsilon -> gamma g g turned OFF
    ]


Pythia.PythiaCommand += ["pyinit pylistf 1",

                         "pysubs ckin 3 3.", # lower pT cut on hard process in 3 GeV
                         
                         "pystat mstat 1",
                         "pyinit dumpr 0 10",#dump this event range to screen
                             
                         ]


MultiMuonFilter = topAlg.MultiMuonFilter
MultiMuonFilter.Ptcut = 4000.
MultiMuonFilter.Etacut = 2.5
MultiMuonFilter.NMuons = 2

MuonFilter = topAlg.MuonFilter
MuonFilter.Ptcut = 6000.
MuonFilter.Etacut = 2.5

try:
     StreamEVGEN.RequireAlgs += ["MultiMuonFilter"]
     StreamEVGEN.RequireAlgs += ["MuonFilter"]
except Exception, e:
     pass

#==============================================================
#
# End of job options file
#
###############################################################
