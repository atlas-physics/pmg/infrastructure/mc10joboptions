#--------------------------------------------------------------
# gamma gamma + 2 partons  with MadGraph/Pythia
#--------------------------------------------------------------
# File prepared by B.Brelier  February 2009
#--------------------------------------------------------------
###############################################################
#
# Job options file
#
#==============================================================
# Central Production set up
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_PythiaMC09p_Common.py" )
Pythia.PythiaCommand += ["pyinit user madgraph"]

Pythia.PythiaCommand +=  [
        "pystat 1 3 4 5",
        "pyinit dumpr 1 5",
        "pyinit pylistf 1",
        "pydat1 parj 90 20000", # Turn off FSR.
        "pydat3 mdcy 15 1 0",
        ]
# ... TAUOLA
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# pythia_card.dat data-card file necessary for the matching between ME and PS
#--------------------------------------------------------------
phojf=open('./pythia_card.dat', 'w')
phojinp = """
!...Fix (0) or event-based (1) PS scale
      PSSCALE=1

!... for exclusive samples =1
      IEXCFILE=1
      
!... shower kT option:
!.. use only with MSTP(81)>20 and put Qcut = xqcut
      showerkt=.true.
      
!...QCUT for matching using the kt measure cutoff
!   put Qcut=xqcut (in ME generation) if showerkt=.true.
!   otherwise, use Qcut > xqcut (default value: qcut=max(xqcut*1.2,xqcut+5) )
      QCUT = 15.

"""
phojf.write(phojinp)
phojf.close()

#--------------------------------------------------------------
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.MadGraphEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'MadGraph.105669.PythiaMadgraphAANp2_30M80'
evgenConfig.efficiency = 0.882
#==============================================================
#
# End of job options file
#
###############################################################
