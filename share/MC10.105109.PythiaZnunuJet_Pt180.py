from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")
include ( "MC10JobOptions/MC10_Pythia_Common.py" )
#Znunu+Jet production:
Pythia.PythiaCommand += [ "pysubs msel 13",
	                  "pydat1 parj 90 20000", # Turn off FSR.
	                  "pydat3 mdcy 15 1 0",   # Turn off tau decays.
	                  "pysubs ckin 3 180.0",   # Lower P_T for hard 2 ->2 process.
	                  "pydat3 mdme 174 1 0",
	                  "pydat3 mdme 175 1 0",
	                  "pydat3 mdme 176 1 0",
	                  "pydat3 mdme 177 1 0",
	                  "pydat3 mdme 178 1 0",
	                  "pydat3 mdme 179 1 0",
	                  "pydat3 mdme 182 1 0",    # Switch for Z->ee.
	                  "pydat3 mdme 183 1 1",
	                  "pydat3 mdme 184 1 0",    # Switch for Z->mumu.
	                  "pydat3 mdme 185 1 1",
	                  "pydat3 mdme 186 1 0",    # Switch for Z->tautau.
	                  "pydat3 mdme 187 1 1"]
	                         
    


# Photos for QED FSR
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )
# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9

