###############################################################
#
# Job options file for min bias (ND)
#
# author: C. Gwenlan (June'10); based on L. Mijovic's studies
# ISR  systematics sample: more hard ISR activity, external LO process + PYTHIA                                
# PARP(67) and PARP(64) are varied                                                                             
# AMBT1 default values: PARP(67)=4., PARP(64)=1.                                                                
# PARP(67): controls suppression of ISR branchings above the coherence scale                                
#           PARP(67) was introduced in 6.4.19 for the new shower, see Pythia update notes for more info     
# PARP(64): multiplies ISR alpha_strong evolution scale, the effect is \propto 1/(lambda_ISR^2)             
#           see Pythia Manual for more info                                                                
#
# reference JO: MC10.105001.pythia_minbias.py
#===============================================================

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 


include ( "MC10JobOptions/MC10_Pythia_Common.py" )

Pythia.PythiaCommand += [  "pysubs msel 1" ]

Pythia.PythiaCommand +=[ "pypars parp 67 6.0" ]   
Pythia.PythiaCommand +=[ "pypars parp 64 0.25" ]  


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig

evgenConfig.efficiency = 0.9


#==============================================================
#
# End of job options file
#
###############################################################

