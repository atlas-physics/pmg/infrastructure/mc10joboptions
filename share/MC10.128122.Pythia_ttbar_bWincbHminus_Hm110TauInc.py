###############################################################
#
# Job options file
#
# Pythia ttbar->bW bH+, W->inc, H-->tau(inc)nu [mH+=110GeV]
#
# Responsible person(s)
#   November 2010: Martin Flechl
#
#==============================================================

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

Pythia.PythiaCommand += ["pysubs msel 0",
                         "pysubs msel 6",          # ttbar production
                         "pydat1 parj 90 20000",   # Turn off lepton radiation
                         "pydat3 mdcy 15 1 0"]     # Turn off tau decays

#Setting the charged higgs mass and tan beta
Pythia.PythiaCommand += ["pydat2 pmas 37 1 110"]
Pythia.PythiaCommand += ["pydat1 paru 141 20"]

#Setting the correct top decays
Pythia.PythiaCommand += ["pydat3 mdme 41 1 0",
                         "pydat3 mdme 42 1 0",
                         "pydat3 mdme 43 1 0",
                         "pydat3 mdme 44 1 0",
                         "pydat3 mdme 45 1 0",
                         "pydat3 mdme 46 1 2", # t->W+b
                         "pydat3 mdme 48 1 0",
                         "pydat3 mdme 49 1 3", # t->H-b
                         "pydat3 mdme 50 1 0",
                         "pydat3 mdme 51 1 0",
                         "pydat3 mdme 52 1 0",
                         "pydat3 mdme 53 1 0",
                         "pydat3 mdme 54 1 0",
                         "pydat3 mdme 55 1 0"]

# Setting the H+ decay
Pythia.PythiaCommand += ["pydat3 mdme 503 1 0",
                         "pydat3 mdme 504 1 0",
                         "pydat3 mdme 505 1 0",
                         "pydat3 mdme 507 1 0",
                         "pydat3 mdme 508 1 0",
                         "pydat3 mdme 509 1 1", #H+ -> tau nu
                         "pydat3 mdme 511 1 0",
                         "pydat3 mdme 512 1 0",
                         "pydat3 mdme 513 1 0",
                         "pydat3 mdme 514 1 0",
                         "pydat3 mdme 515 1 0",
                         "pydat3 mdme 516 1 0",
                         "pydat3 mdme 517 1 0",
                         "pydat3 mdme 518 1 0",
                         "pydat3 mdme 519 1 0",
                         "pydat3 mdme 520 1 0",
                         "pydat3 mdme 521 1 0",
                         "pydat3 mdme 522 1 0",
                         "pydat3 mdme 523 1 0",
                         "pydat3 mdme 524 1 0",
                         "pydat3 mdme 525 1 0",
                         "pydat3 mdme 526 1 0",
                         "pydat3 mdme 527 1 0",
                         "pydat3 mdme 528 1 0",
                         "pydat3 mdme 529 1 0"]

# Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# End jobO fragment
#--------------------------------------------------------------
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.95

