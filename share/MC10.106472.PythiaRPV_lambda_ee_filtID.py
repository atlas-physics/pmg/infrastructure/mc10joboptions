#-------------------------------------------------------------------------
#
# RPV displaced vertex with decay to electrons, lambda coupling
# 
# contact :  C. Horn, R. Bruneliere  N. Barlow
#
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" ) 

Pythia.PythiaCommand += ["pysubs msel 0"]  # !
##
Pythia.PythiaCommand += ["pysubs msub 271 1"]  # squark pair production 
Pythia.PythiaCommand += ["pysubs msub 272 1"]  # squark pair production 
Pythia.PythiaCommand += ["pysubs msub 273 1"]  # squark pair production 
Pythia.PythiaCommand += ["pysubs msub 274 1"]  # squark pair production 
Pythia.PythiaCommand += ["pysubs msub 275 1"]  # squark pair production 
Pythia.PythiaCommand += ["pysubs msub 276 1"]  # squark pair production 
Pythia.PythiaCommand += ["pysubs msub 277 1"]  # squark pair production 
Pythia.PythiaCommand += ["pysubs msub 278 1"]  # squark pair production 
Pythia.PythiaCommand += ["pysubs msub 279 1"]  # squark pair production 
Pythia.PythiaCommand += ["pysubs msub 280 1"]  # squark pair production 
#######
Pythia.PythiaCommand+= ["pymssm imss 1 1"] # general MSSM !!
Pythia.PythiaCommand+= ["pymssm imss 51 3"] #switch on RPV lambda coupling
Pythia.PythiaCommand += ["pymsrv rvlam 121 0.00002"] # setting lambda coupling strength (to this value!)
## with current masses: 0.000001 -> ct=70cm
## with current masses: 0.000004 -> ct=4.4cm
## with current masses: 0.000010 -> ct=7mm
Pythia.PythiaCommand += ["pydat1 mstj 22 3"] # allow long decay length !
Pythia.PythiaCommand += ["pydat1 parj 72 100000."] # max length set to 100m 
Pythia.PythiaCommand += ["pydat3 mdme 2241 1 0"] # switch explicetly off RPV decays to neutrinos (for muons)
Pythia.PythiaCommand += ["pydat3 mdme 2242 1 0"] # switch explicetly off RPV decays to neutrinos (for muons)
##

Pythia.PythiaCommand += ["pydat3 mdme 2169 1 0"] # switch explictly off RPV decays to some stuff
Pythia.PythiaCommand += ["pydat3 mdme 2170 1 0"] # switch explictly off RPV decays to some stuff
Pythia.PythiaCommand += ["pydat3 mdme 2171 1 0"] # switch explictly off RPV decays to some stuff
Pythia.PythiaCommand += ["pydat3 mdme 2172 1 0"] # switch explictly off RPV decays to some stuff
Pythia.PythiaCommand += ["pydat3 mdme 2173 1 0"] # switch explictly off RPV decays to some stuff
Pythia.PythiaCommand += ["pydat3 mdme 2174 1 0"] # switch explictly off RPV decays to some stuff
Pythia.PythiaCommand += ["pydat3 mdme 2175 1 0"] # switch explictly off RPV decays to some stuff
Pythia.PythiaCommand += ["pydat3 mdme 2176 1 0"] # switch explictly off RPV decays to some stuff
Pythia.PythiaCommand += ["pydat3 mdme 2177 1 0"] # switch explictly off RPV decays to some stuff
Pythia.PythiaCommand += ["pydat3 mdme 2178 1 0"] # switch explictly off RPV decays to some stuff
Pythia.PythiaCommand += ["pydat3 mdme 2179 1 0"] # switch explictly off RPV decays to some stuff
Pythia.PythiaCommand += ["pydat3 mdme 2180 1 0"] # switch explictly off RPV decays to some stuff
Pythia.PythiaCommand += ["pydat3 mdme 2183 1 0"] # switch explictly off RPV decays to some stuff
Pythia.PythiaCommand += ["pydat3 mdme 2184 1 0"] # switch explictly off RPV decays to some stuff
Pythia.PythiaCommand += ["pydat3 mdme 2185 1 0"] # switch explictly off RPV decays to some stuff
Pythia.PythiaCommand += ["pydat3 mdme 2186 1 0"] # switch explictly off RPV decays to some stuff
Pythia.PythiaCommand += ["pydat3 mdme 2187 1 0"] # switch explictly off RPV decays to some stuff
Pythia.PythiaCommand += ["pydat3 mdme 2188 1 0"] # switch explictly off RPV decays to some stuff
Pythia.PythiaCommand += ["pydat3 mdme 2189 1 0"] # switch explictly off RPV decays to some stuff
Pythia.PythiaCommand += ["pydat3 mdme 2190 1 0"] # switch explictly off RPV decays to some stuff
Pythia.PythiaCommand += ["pydat3 mdme 2191 1 0"] # switch explictly off RPV decays to some stuff
Pythia.PythiaCommand += ["pydat3 mdme 2192 1 0"] # switch explictly off RPV decays to some stuff
Pythia.PythiaCommand += ["pydat3 mdme 2195 1 0"] # switch explictly off RPV decays to some stuff
Pythia.PythiaCommand += ["pydat3 mdme 2196 1 0"] # switch explictly off RPV decays to some stuff
Pythia.PythiaCommand += ["pydat3 mdme 2197 1 0"] # switch explictly off RPV decays to some stuff
Pythia.PythiaCommand += ["pydat3 mdme 2198 1 0"] # switch explictly off RPV decays to some stuff
Pythia.PythiaCommand += ["pydat3 mdme 2199 1 0"] # switch explictly off RPV decays to some stuff
Pythia.PythiaCommand += ["pydat3 mdme 2200 1 0"] # switch explictly off RPV decays to some stuff
Pythia.PythiaCommand += ["pydat3 mdme 2201 1 0"] # switch explictly off RPV decays to some stuff
Pythia.PythiaCommand += ["pydat3 mdme 2202 1 0"] # switch explictly off RPV decays to some stuff
Pythia.PythiaCommand += ["pydat3 mdme 2203 1 0"] # switch explictly off RPV decays to some stuff
Pythia.PythiaCommand += ["pydat3 mdme 2204 1 0"] # switch explictly off RPV decays to some stuff

Pythia.PythiaCommand += ["pymssm rmss 1 500."] #M1 gaugino 
Pythia.PythiaCommand += ["pymssm rmss 2 5000."] #M2 gaugino 
Pythia.PythiaCommand += ["pymssm rmss 3 5000."] #M3 gluino
Pythia.PythiaCommand += ["pymssm rmss 4 800."] #mu higgsino, default=800  , make chi10 lighter than other gauginos
Pythia.PythiaCommand += ["pymssm rmss 5 2."]  #tan beta , default=2  , make chi10 lighter than other gauginos
Pythia.PythiaCommand += ["pymssm rmss 6 5000."] #left slepton, makes sneutrino mass high (also eL), to surpress LQD decay to neutrinos
Pythia.PythiaCommand += ["pymssm rmss 7 5000."] #right slepton, irrelevant in LQD 
Pythia.PythiaCommand += ["pymssm rmss 8 700."] #left squark
Pythia.PythiaCommand += ["pymssm rmss 9 700."] #right squark
Pythia.PythiaCommand += ["pymssm rmss 10 5000."] #third generation #set high values to surpress production
Pythia.PythiaCommand += ["pymssm rmss 11 5000."] #third generation
Pythia.PythiaCommand += ["pymssm rmss 12 5000."] #third generation
Pythia.PythiaCommand += ["pymssm rmss 13 5000."] #third generation
Pythia.PythiaCommand += ["pymssm rmss 14 5000."] #third generation

# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = INFO

#---------------------------------------------------------------
# Includes
#---------------------------------------------------------------

Pythia.PythiaCommand += ["pydat1 parj 90 20000"] # Turn off FSR.
Pythia.PythiaCommand += ["pydat3 mdcy 15 1 0"]   # Turn off tau decays.

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


#---------------------------------------------------------------
#  Decay Length Filter
#---------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import DecayLengthFilter
topAlg += DecayLengthFilter()

DecayLengthFilter = topAlg.DecayLengthFilter
DecayLengthFilter.OutputLevel=DEBUG
DecayLengthFilter.McEventCollection = "GEN_EVENT"
# specify desired decay region:
DecayLengthFilter.Rmin = 250
DecayLengthFilter.Rmax = 1000
DecayLengthFilter.Zmin = 800
DecayLengthFilter.Zmax = 3000
DecayLengthFilter.particle_id = 1000022; 



#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
# 2101 = EventInfo
# 133273 = MCTruth (HepMC)
# optionally
include("AthenaSealSvc/AthenaSealSvc_joboptions.py" )
AthenaSealSvc.CheckDictionary = True

from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.58
#evgenConfig.minevents = 100

from MC10JobOptions.SUSYEvgenConfig import evgenConfig


###############################################################

# 
# End of job options file
#
###############################################################
