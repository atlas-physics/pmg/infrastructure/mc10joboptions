################################################################
#
# MC@NLO/JIMMY/HERWIG H195 -> W+W- -> tau+nu tau-nu
#
# Responsible person(s)
#   Mar. 11, 2011 : Tiesheng Dai (Tiesheng.Dai@cern.ch)
#
################################################################
#
# Job options file
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 
# ... Herwig
try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig

Herwig.HerwigCommand += [ "modbos 1 4", "modbos 2 4",
                          "maxpr 10",
                          "taudec TAUOLA"]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

from MC10JobOptions.McAtNloEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'mcatnlo0341.116653'
try:
  if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.mcatnlo0341.116653.07TeV_H195_WpWm.TXT.v1'
  if runArgs.ecmEnergy == 10000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.mcatnlo0341.116653.10TeV_H195_WpWm.TXT.v1'
except NameError:
  pass

evgenConfig.efficiency = 0.9
#==============================================================
#
# End of job options file
#
###############################################################
