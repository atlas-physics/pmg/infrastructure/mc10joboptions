###############################################################
#
# Job options file
#
# Pythia J2 event generation with eta interval weighting.
# cross section as default pythia sample = 4.1*10^4 nb
#
# Prepared by Graham Jones, Graham.Jones [at] cern.ch 
#==============================================================


# dijet production with pythia
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

Pythia.PythiaCommand += [
			      "pysubs msel 0",
			      "pysubs ckin 3 35.",
			      "pysubs ckin 4 70.",
			      "pysubs msub 11 1",
			      "pysubs msub 12 1",
			      "pysubs msub 13 1",
			      "pysubs msub 68 1",
			      "pysubs msub 28 1",
			      "pysubs msub 53 1"]

#--------------------------------------------------------------
# Truth Jets
#--------------------------------------------------------------

# Make truth jets for JetGapFilter:
from AthenaCommon.AppMgr import ServiceMgr as svcMgr
include( "AthenaCommon/Atlas_Gen.UnixStandardJob.py" )
from JetRec.JetGetters import *
Kt6Alg = make_StandardJetGetter('AntiKt',0.6,'Truth').jetAlgorithmHandle()
Kt6Alg.AlgTools['JetFinalPtCut'].MinimumSignal = 7.0*GeV

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

## Truth filter
# Add the filter to the correct sequence!
from GeneratorFilters.GeneratorFiltersConf import JetIntervalFilter
etaFilter=JetIntervalFilter()
topAlg += etaFilter

# General properties
etaFilter.JetContainer = "AntiKt6TruthJets"
etaFilter.JetNumber = 2
etaFilter.OppositeSideJets = False

# Jet Energy Cuts
etaFilter.JetCutByPt = True

# Only used if cutting by pt
etaFilter.Jet1MinPt = 12.0*GeV
etaFilter.Jet1MaxPt = 7000.0*GeV
etaFilter.Jet2MinPt = 12.0*GeV
etaFilter.Jet2MaxPt = 7000.0*GeV

# Jet Position Cuts (absolute)
etaFilter.Jet1MaxEta = 100.0
etaFilter.Jet1MinEta = 0.0
etaFilter.Jet2MaxEta = 100.0
etaFilter.Jet2MinEta = 0.0

# Jet delta eta cut
etaFilter.MinDeltaEta = 0.0
etaFilter.MaxDeltaEta = 10.0

# Control event weighting
etaFilter.WeightEvents = True
etaFilter.UniformMaxWeightBelowGausMean = False

# J2 tune
etaFilter.GausMean = 0.58
etaFilter.GausSigma = 1.90
etaFilter.AbsDEtaCutOff = 7.0

#--------------------------------------------------------------
# Apply filtering
#--------------------------------------------------------------

try:
  StreamEVGEN.RequireAlgs += [ "JetIntervalFilter" ]
except Exception, e:
     pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
# N_gen = 2.269*10^8; N_pass = 2*10^6 -> eff = 0.0088
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.0088
# evgenConfig.minevents=5000
