#-------------------------------------------------------------------------
#
# SPS1a + RPV susy process
# 
# contact :  S. French, C.G. Lester
#
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

Pythia.PythiaCommand += ["pysubs msel 0"]  # !
Pythia.SusyInputFile = "susy_SPS1a_slha.txt"
##
# Pythia.PythiaCommand += ["pypars mstp 81 1"] # Old jet showering scheme <===
Pythia.PythiaCommand += ["pydat1 parj 90 20000"] # Turn off FSR.
Pythia.PythiaCommand += ["pydat3 mdcy 15 1 0"]   # Turn off tau decays.
Pythia.PythiaCommand += ["pysubs msel 39"]
Pythia.PythiaCommand += ["pymssm imss 1 11"]
Pythia.PythiaCommand += ["pymssm imss 21 42"]
Pythia.PythiaCommand += ["pymssm imss 53 3"]
Pythia.PythiaCommand += ["pymsrv rvlamb 112 0.001"] ##ok with 0.0001

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

# radiate only the tau-s; neccessary!!
#from Photos_i.Photos_iConf import Photos
#topAlg += Photos()
#Photos = topAlg.Photos
#Photos.PhotosCommand = ["photos pmode 2","photos xphcut 0.01","photos alpha -1.","photos interf 1","photos isec 1","photos iftop 0"]

# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = INFO

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
include("AthenaSealSvc/AthenaSealSvc_joboptions.py" )
AthenaSealSvc.CheckDictionary = True

from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
#evgenConfig.minevents = 100

from MC10JobOptions.SUSYEvgenConfig import evgenConfig

#---------------------------------------------------------------
#End of job options file
#
###############################################################

