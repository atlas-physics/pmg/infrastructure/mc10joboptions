###############################################################
#
# Job options file
#
#==============================================================
# Central Production set up
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_PythiaMC09p_Common.py" )
Pythia.PythiaCommand += ["pyinit user madgraph"]

Pythia.PythiaCommand +=  [
    "pysubs msel 0",
    "pystat 1 3 4 5",
    "pyinit dumpr 1 5",
    "pyinit pylistf 1",
    "pydat1 parj 90 20000", # turn off FSR
    "pydat3 mdcy 15 1 0",   # turn off tau decays
    # "pypars mstp 81 1",
    #    "pymssm imss 1 11",
    "pyinit pylistf 1",  # 1 = normal listing
    "pyinit pylisti 12",
    ]
# ... TAUOLA
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.MadGraphEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'MadGraph.105137.KKGluon_SemiLeptonic_1000'
evgenConfig.efficiency = 0.9
#==============================================================
#
# End of job options file
#
###############################################################
