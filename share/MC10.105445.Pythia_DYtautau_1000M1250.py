###############################################################
#
# Job options file
# by: Jeremy Love
#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------

# ... Main generator : Pythia
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_Pythia_Common.py" )
Pythia.PythiaCommand += [
                        "pydat1 parj 90 20000",
                        "pydat3 mdcy 15 1 0",
                        "pysubs msel 0",
                        "pysubs msub 1 1",
                        "pysubs ckin 1 1000.0",      # Lower invariant mass.
                        "pysubs ckin 2 1250.0",     # Higher invariant mass.
                        "pydat3 mdme 174 1 0",
                        "pydat3 mdme 175 1 0",
                        "pydat3 mdme 176 1 0",
                        "pydat3 mdme 177 1 0",
                        "pydat3 mdme 178 1 0",
                        "pydat3 mdme 179 1 0",
                        "pydat3 mdme 182 1 0", #ee
                        "pydat3 mdme 183 1 0",
                        "pydat3 mdme 184 1 0", #mumu
                        "pydat3 mdme 185 1 0",
                        "pydat3 mdme 186 1 1", #tautau
                        "pydat3 mdme 187 1 0"]

include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import ATauFilter
topAlg += ATauFilter()

ATauFilter = topAlg.ATauFilter
ATauFilter.Etacut      = 2.7
ATauFilter.llPtcute    = 13000.0
ATauFilter.llPtcutmu   = 8000.0
ATauFilter.lhPtcute    = 13000.0
ATauFilter.lhPtcutmu   = 8000.0
ATauFilter.lhPtcuth    = 12000.0
ATauFilter.hhPtcut     = 12000.0
ATauFilter.OutputLevel = INFO


#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------


try:
    StreamEVGEN.RequireAlgs += [ "ATauFilter" ]
except Exception, e:
    pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.85
