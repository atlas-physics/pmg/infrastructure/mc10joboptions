#-------------------------------------------------------------------------
# Modified from 'CSC.006403.lightstop_jimmy_susy.py' :
#-----------------------------------------------------
# Modifications:
#  1. different 'susyfile' name (susy_LST1.txt)
#  2. adding     'syspin 0'
#  3. adding ParticleFilter to keep only events with N_gluino >=2
#  4. correcting extra space in JimmyCommand
#-------------------------------------------------------------------------
#
# light stop point
# cross section: 161 pb
# 
# contact :  JP Archambault, Zhaoyu Yang
#

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 

try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_Herwig_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_Herwig_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_Herwig_Common.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig

Herwig.HerwigCommand += [ "iproc 13000",
                          "susyfile susy_LST1.txt",
                          "taudec TAUOLA",
                          "syspin 0" ]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.HerwigEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.015

from MC10JobOptions.SUSYEvgenConfig import evgenConfig


##############################################################################
#
#  GLUINO FILTER
#
#======================================================================
# Generator Filter which selects events with >= 2 gluinos
#======================================================================
from GeneratorFilters.GeneratorFiltersConf import ParticleFilter
topAlg += ParticleFilter()

theGluinoPtcut  = 0.0
theGluinoEcut   = 10000000.0
theGluinoEtacut = 10.0
theGluinoPDG    = 1000021
theGluinoStatus = -1
theNumGluino    = 2

ParticleFilter   = topAlg.ParticleFilter
ParticleFilter.Ptcut     = theGluinoPtcut
ParticleFilter.Energycut = theGluinoEcut
ParticleFilter.Etacut    = theGluinoEtacut
ParticleFilter.PDG       = theGluinoPDG
ParticleFilter.StatusReq = theGluinoStatus
ParticleFilter.MinParts  = theNumGluino

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------

try:
     StreamEVGEN.RequireAlgs += [ "ParticleFilter" ]
except Exception, e:
     pass

#==============================================================
#
# End of job options file
#
###############################################################
