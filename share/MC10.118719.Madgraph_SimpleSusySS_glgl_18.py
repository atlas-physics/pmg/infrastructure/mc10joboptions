##############################################################
#
# Job options file
#
#==============================================================
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")
# --> use next line if you use 20650  LO* (modified LO PDF: MRST2007lomod)  as in pythia
# include ( "MC10JobOptions/MC10_Pythia_Common.py" )  
# --> use next line if CTEQ6l1 pdf has been used
include ( "MC10JobOptions/MC10_PythiaMC09p_Common.py" ) 
#--------------------------------------------------------------
# Algorithms Private Options
#--------------------------------------------------------------
Pythia = topAlg.Pythia
Pythia.PythiaCommand = ["pyinit user madgraph"]
#--------------------------------------------------------------
Pythia.PythiaCommand +=  [
            "pystat 1 3 4 5",
            "pyinit dumpr 1 5",
            "pyinit pylistf 1",
            "pydat3 mdcy 307 1 0",
            # "pypars mstp 81 21",
            "pypars mstp 128 1"
            ]
# ... TAUOLA and Photos
Pythia.PythiaCommand += ["pydat1 parj 90 20000",
                          "pydat3 mdcy 15 1 0" ]
# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )
# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.MadGraphEvgenConfig import evgenConfig
# dummy needed
evgenConfig.inputfilebase = 'Madgraph'
try:
  if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputfilebase = 'group09.phys-gener.Madgraph.118719.SimpleSusySS_glgl_18_7TeV.TXT.v1'
  if runArgs.ecmEnergy == 8000.0:
    evgenConfig.inputfilebase = 'group09.phys-gener.Madgraph.118719.SimpleSusySS_glgl_18_8TeV.TXT.v1'
except NameError:
  pass
evgenConfig.efficiency = 0.9
#==============================================================
#
# End of job options file
#
###############################################################
