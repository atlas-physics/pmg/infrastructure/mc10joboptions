####################################################################
#
# Job options file
#
# usage : call from include sentence in CSC_generation_skeleton.py
#
#===================================================================
#
# last modified - 05/06/06 - nick brett [n.brett1@physics.ox.ac.uk]
#
#===================================================================

# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )

#--------------------------------------------------------------
# Generator Configuration - Herwig
#--------------------------------------------------------------

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")
 

try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_Herwig_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_Herwig_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_Herwig_Common.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig


Herwig.HerwigCommand += [
            "iproc charybdis",  # select charybdis
            "charyb 1 1000",    # Planck mass
            "charyb 2 6",       # total number of dimensions
            "charyb 3 5000",    # minimum mass
            "charyb 4 10000",   # maximum mass
            "charyb 5 2",       # number of partiles in remant decay
            "charyb 6 1",       # time variation of temperature
            "charyb 7 1",       # grey body factors
            "charyb 8 1",       # kinematic limit
            "charyb 9 0",       # Yoshino-Rychkov c-s
            "charyb 10 1000",   # Maximum BH temperature
            "charyb 11 0",      # Boiling Remnant Model
            "charyb 12 1000",   # Minimum Remnant Mass
            "charyb 13 2",      # Planck mass definition
            "charyb 14 1",      # PDF scale
            "charyb 15 3",      # Particle production (3=all SM particles)
            "charyb 16 1",      # Charybdis printout
            "charyb 17 5000",    # beam energy
            "taudec TAUOLA"
            ]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#---------------------------------------------------------------
# Pool Persistency
#---------------------------------------------------------------

#==============================================================
#
# End of job options file
#
###############################################################
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.HerwigEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
 
