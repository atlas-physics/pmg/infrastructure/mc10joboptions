#--------------------------------------------------------------
# 
# Job options file
#
#
# Herwig & Jimmy gg->bt_H+/ (mH+=400GeV,tan(beta)=7) 
# 3 photon EF
# SUSY point close to kinematic limit: Mu = 200 GeV, M2 = 310GeV
#
# Responsible person(s)
#   2 Mar, 2009 :  Caleb LAMPEN (lampen@physics.arizona.edu)
# 
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence
topAlg=AlgSequence()


try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_Herwig_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_Herwig_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_Herwig_Common.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig

topAlg.Herwig.HerwigCommand += [ "iproc 3839",              #iproc gg->bt_H+ + ch. conjg
                                  "susyfile susy_mHp400tanb7_2.txt",#isawig input file
                                  "taudec TAUOLA",           #taudec tau dcay package
                                  "effmin 0.0000000001"]     #To fix early termination
#Setup tauola and photos. 
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )
                                 
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
topAlg +=MultiLeptonFilter()
topAlg.MultiLeptonFilter.Ptcut = 7000.
topAlg.MultiLeptonFilter.NLeptons = 3
#Also, Eta is cut at 10.0 by default

try:
  StreamEVGEN.RequireAlgs = ["MultiLeptonFilter"]
except Exception, e:
  pass

#######################################################
# FILTER EFFICIENCY
#######################################################
from MC10JobOptions.HerwigEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.1750
from MC10JobOptions.SUSYEvgenConfig import evgenConfig
# 5000/25719=0.1944 (x0.9=0.1750)
#==============================================================
#
# End of job options file
#

