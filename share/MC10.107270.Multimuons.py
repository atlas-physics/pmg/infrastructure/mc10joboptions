 ###############################################################
#
# Job options file for testing ParticleGenerator.
# Sergio Gonzalez Sevilla  (edited to be compliant with prod sys by Ian H)
# 2009/5 responsible: Sasa Fratina
#==============================================================

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

from ParticleGenerator.ParticleGeneratorConf import ParticleGenerator
topAlg += ParticleGenerator()

ParticleGenerator = topAlg.ParticleGenerator
# The following is needed to load the Athena Random
ParticleGenerator.OutputLevel = 1

ParticleGenerator.orders = [
 "id[0]: sequence -13 13",
 "pt[0]: flat 2000. 50000.",
 "eta[0]: flat -2.7 2.7",
 "phi[0]: flat 0. 6.2832",
 "VertX[0]: gauss 0. 0.015",
 "VertY[0]: gauss 0. 0.015",
 "vertZ[0]: gauss 0. 56.",	
 "id[1]: sequence -13 13",
 "pt[1]: flat 2000. 50000.",
 "eta[1]: flat -2.7 2.7",
 "phi[1]: flat 0. 6.2832",
 "VertX[1]: VertX[0]",
 "VertY[1]: VertY[0]",
 "vertZ[1]: VertZ[0]",	
 "id[2]: sequence -13 13",
 "pt[2]: flat 2000. 50000.",
 "eta[2]: flat -2.7 2.7",
 "phi[2]: flat 0. 6.2832",
 "VertX[2]: VertX[0]",
 "VertY[2]: VertY[0]",
 "vertZ[2]: VertZ[0]",	
 "id[3]: sequence -13 13",
 "pt[3]: flat 2000. 50000.",
 "eta[3]: flat -2.7 2.7",
 "phi[3]: flat 0. 6.2832",
 "VertX[3]: VertX[0]",
 "VertY[3]: VertY[0]",
 "vertZ[3]: VertZ[0]",	
 "id[4]: sequence -13 13",
 "pt[4]: flat 2000. 50000.",
 "eta[4]: flat -2.7 2.7",
 "phi[4]: flat 0. 6.2832",
 "VertX[4]: VertX[0]",
 "VertY[4]: VertY[0]",
 "vertZ[4]: VertZ[0]",	
 "id[5]: sequence -13 13",
 "pt[5]: flat 2000. 50000.",
 "eta[5]: flat -2.7 2.7",
 "phi[5]: flat 0. 6.2832",
 "VertX[5]: VertX[0]",
 "VertY[5]: VertY[0]",
 "vertZ[5]: VertZ[0]",	
 "id[6]: sequence -13 13",
 "pt[6]: flat 2000. 50000.",
 "eta[6]: flat -2.7 2.7",
 "phi[6]: flat 0. 6.2832",
 "VertX[6]: VertX[0]",
 "VertY[6]: VertY[0]",
 "vertZ[6]: VertZ[0]",	
 "id[7]: sequence -13 13",
 "pt[7]: flat 2000. 50000.",
 "eta[7]: flat -2.7 2.7",
 "phi[7]: flat 0. 6.2832",
 "VertX[7]: VertX[0]",
 "VertY[7]: VertY[0]",
 "vertZ[7]: VertZ[0]",	
 "id[8]: sequence -13 13",
 "pt[8]: flat 2000. 50000.",
 "eta[8]: flat -2.7 2.7",
 "phi[8]: flat 0. 6.2832",
 "VertX[8]: VertX[0]",
 "VertY[8]: VertY[0]",
 "vertZ[8]: VertZ[0]",	
 "id[9]: sequence -13 13",
 "pt[9]: flat 2000. 50000.",
 "eta[9]: flat -2.7 2.7",
 "phi[9]: flat 0. 6.2832",
 "VertX[9]: VertX[0]",
 "VertY[9]: VertY[0]",
 "vertZ[9]: VertZ[0]"
]

#---------------------------------------------------------------
# Pool Persistency
#---------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.SingleEvgenConfig import evgenConfig
#==============================================================
#
# End of job options file
#
###############################################################
