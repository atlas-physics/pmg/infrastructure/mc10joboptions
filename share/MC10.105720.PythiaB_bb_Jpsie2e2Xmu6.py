###############################################################
# PRODUCTION SYSTEM FRAGMENT
#
# Job options file for generation of B-events 
#  in user selected exclusive channel
#
# bb->J/psi(e2e2)mu6X using Photos
#
# Author: Darren Price (Darren.Price@cern.ch)
#
#==============================================================

#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_PythiaB_Common.py" )

#--- cutoff for QED FSR in Pythia to 20000 GeV (='infinity', photos takes care of it).
PythiaB.PythiaCommand += [ "pydat1 parj 90 20000" ]

#--------------------------------------------------------------				 
#              PARAMETERS  SPECIFIC  TO   PYTHIAB
#--------------------------------------------------------------
PythiaB.ForceCDecay = "no"

#--------------------------------------------------------------				 
# -------------  FORCE   YOUR  B CHANNEL  HERE -------------
#--------------------------------------------------------------
#--- To force your B-decay channels decomment following 2 lines:
include( "MC10JobOptions/MC10_PythiaB_CloseAntibQuark.py" )
PythiaB.ForceBDecay = "yes"

#--- Inclusive B -> J/psi(ee) X production
include( "MC10JobOptions/MC10_PythiaB_Jpsichannels.py" )

#--------------------------------------------------------------
# --------  PYTHIA PARAMETERS OPTIMAL FOR BEAUTY PRODUCTION  --
#--------------------------------------------------------------
include( "MC10JobOptions/MC10_PythiaB_Btune.py" )

#--- Force J/psi to e+ e-
PythiaB.PythiaCommand += [ "pydat3 mdme 889 1 1",    
                           "pydat3 mdme 858 1 1",
                           "pydat3 brat 858 1.",
                           "pydat3 mdme 859 1 0",
                           "pydat3 mdme 860 1 0",
                           "pysubs ckin 3 11.",
                           "pysubs ckin 9 -3.5",
                           "pysubs ckin 10 3.5",
                           "pysubs ckin 11 -3.5",
                           "pysubs ckin 12 3.5",
                           "pysubs msel 1" ]


#--- Selecton cut in PyhtiaB
#  ------------- Selections on b  quarks   -------------
# simulate  only b-flavour events
PythiaB.flavour =  5.
# PythiaB force exclusive decay channels only on b=-5 side 
# ------------------- b=5  --- and/or ---  b=-5 --------
PythiaB.cutbq = [ "10. 2.5 and 10. 2.5" ]
#  ------------- LVL1 muon cuts 0=OFF 1=ON -------------
PythiaB.lvl1cut = [ 1.,  6., 2.5 ]
#  ------------- LVL2 muon/electron cuts  0=OFF 1=ON-------------
#PythiaB.lvl2cut = [ 0.,  13.,     3.,   2.5 ]
#PythiaB.lvl2cut = { 0.,  11.,     6.,   2.5};
#  ------------- Offline cuts 0=OFF 1=ON -------------
PythiaB.offcut = [ 1., 0.0, 102.5, 0., 102.5, 2.0, 2.5 ]
#  ------------- Number of repeated hadronization mhadr -------------
PythiaB.mhadr = 10. 

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import BSignalFilter
topAlg += BSignalFilter()

from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
topAlg += MultiLeptonFilter()

BSignalFilter = topAlg.BSignalFilter
BSignalFilter.LVL1MuonCutOn = TRUE
BSignalFilter.LVL1MuonCutPT = 6000.0
BSignalFilter.LVL1MuonCutEta = 2.5

MultiLeptonFilter = topAlg.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 2000.0
MultiLeptonFilter.Etacut = 2.5
MultiLeptonFilter.NLeptons = 3

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------

try:
    StreamEVGEN.RequireAlgs +=  [ "BSignalFilter" ]
except Exception, e:
    pass
try:
    StreamEVGEN.RequireAlgs += [ "MultiLeptonFilter" ]
except Exception, e:
    pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
# efficiency = 500/526*0.9 = 0.856
from MC10JobOptions.PythiaBEvgenConfig import evgenConfig
evgenConfig.minevents = 500
evgenConfig.efficiency = 0.856
