#==============================================================
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 4

#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------

#-- Dll's and Algorithms
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

try:
   if runArgs.ecmEnergy == 7000.0:
      include ( "MC10JobOptions/MC10_McAtNloJimmy_Common_7TeV.py" )
   if runArgs.ecmEnergy == 8000.0:
      include ( "MC10JobOptions/MC10_McAtNloJimmy_Common_8TeV.py" )
   if runArgs.ecmEnergy == 10000.0:
      include ( "MC10JobOptions/MC10_McAtNloJimmy_Common.py" )
   if runArgs.ecmEnergy == 14000.0:
      include ( "MC10JobOptions/MC10_McAtNloJimmy_Common_14TeV.py" )
except NameError:
   # needed (dummy) default
   from Herwig_i.Herwig_iConf import Herwig
   topAlg += Herwig()
   Herwig = topAlg.Herwig

# ... Main generator : Herwig
Herwig.HerwigCommand += [ "taudec TAUOLA"]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filters
#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import TTbarMassFilter
topAlg += TTbarMassFilter()

TTbarMassFilter = topAlg.TTbarMassFilter
TTbarMassFilter.TopPairMassLowThreshold  = 900000.
TTbarMassFilter.TopPairMassHighThreshold = 1100000.
TTbarMassFilter.OutputLevel=INFO

try:
     StreamEVGEN.RequireAlgs = [ "TTbarMassFilter" ]
except Exception, e:
     pass


from GeneratorFilters.GeneratorFiltersConf import TTbarWToLeptonFilter
topAlg += TTbarWToLeptonFilter()

TTbarWToLeptonFilter = topAlg.TTbarWToLeptonFilter
TTbarWToLeptonFilter.Ptcut = 1.
TTbarWToLeptonFilter.OutputLevel=INFO

try:
     StreamEVGEN.RequireAlgs += [ "TTbarWToLeptonFilter" ]
except Exception, e:
     pass
#--------------------------------------------------------------

from MC10JobOptions.McAtNloEvgenConfig import evgenConfig

# dummy needed
evgenConfig.inputfilebase = 'mcatnlo'
try:
   if runArgs.ecmEnergy == 7000.0:
      evgenConfig.inputfilebase = 'group10.phys-gener.mcatnlo341.117303.ttbar_7TeV_mass3.TXT.v1'
   if runArgs.ecmEnergy == 8000.0:
      evgenConfig.inputfilebase = 'group10.phys-gener.mcatnlo341.117303.ttbar_8TeV_mass3.TXT.v1'
   if runArgs.ecmEnergy == 10000.0:
      evgenConfig.inputfilebase = 'group10.phys-gener.mcatnlo341.117303.ttbar_10TeV_mass3.TXT.v1'
   if runArgs.ecmEnergy == 14000.0:
      evgenConfig.inputfilebase = 'group10.phys-gener.mcatnlo341.117303.ttbar_14TeV_mass3.TXT.v1'
except NameError:
  pass

evgenConfig.efficiency = 0.0122 * 0.50

#==============================================================


