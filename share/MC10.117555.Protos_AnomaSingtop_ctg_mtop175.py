###############################################################
# Job options file for generating Anomalous top(c+g->t) events with Protos 2.01
# The top mass is 175 GeV 
# Muhammad alhroob (with the help of Nuno Castro) (alhroob@cern.ch)
#==============================================================
#
from AthenaCommon.AlgSequence import AlgSequence
topAlg=AlgSequence("TopAlg")

include ("MC10JobOptions/MC10_PythiaMC09p_Common.py")

Pythia.PythiaCommand += ["pyinit user protos",
                        "pydat1 parj 90 20000.",
                        "pydat3 mdcy 15 1 0",
                        "pyinit dumpr 1 12",
                        "pyinit pylistf 1"
                       ]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
#Dummy filename for 10 TeV used the same as for 7 TeV
evgenConfig.inputfilebase = 'protos'
try:
  if runArgs.ecmEnergy == 7000.0:
     evgenConfig.inputfilebase = 'group09.phys-gener.protos201.117555.AnomaSingtop_ctg_mtop175_7TeV.TXT.v2'
  if runArgs.ecmEnergy == 10000.0:
     evgenConfig.inputfilebase = 'group09.phys-gener.protos201.117555.AnomaSingtop_ctg_mtop175_10TeV.TXT.v1'	
except NameError:
  pass

evgenConfig.efficiency = 0.9
#==============================================================
# End of job options file
#
###############################################################
