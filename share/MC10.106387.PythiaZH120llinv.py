################################################################
#
# qq or gg --> ZH, H --> invisible, mH=120.0 GeV, Z --> ee or mumu
#
# Responsible persons
#   Nov 12, 2008 : Pauline Gagnon (Pauline.Gagnon@cern.ch)
#                  Malachi Schram (malachi.schram@cern.ch)
#
################################################################
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
#-------------------------------------------------------------------
# File prepared by Halasya Siva Subramania December 2006
# Modified for 10 TeV by Malachi Schram - October 2008
#-------------------------------------------------------------------
include ( "MC10JobOptions/MC10_Pythia_Common.py" )
Pythia.PythiaCommand += [
                        "pysubs msel 0",
                        "pysubs msub 24 1",         # ZH production
                        "pydat3 mdcy 25 1 0",       # H does not decay (invisible)
                        "pydat2 pmas 25 1 120.0",   # Higgs mass = 120 GeV
                        "pydat3 mdme 174 1 0",      # Z to ee and mumu only
                        "pydat3 mdme 175 1 0",
                        "pydat3 mdme 176 1 0",
                        "pydat3 mdme 177 1 0",
                        "pydat3 mdme 178 1 0",
                        "pydat3 mdme 179 1 0",
                        "pydat3 mdme 180 1 0",
                        "pydat3 mdme 181 1 0",
                        "pydat3 mdme 182 1 1",
                        "pydat3 mdme 183 1 0",
                        "pydat3 mdme 184 1 1",
                        "pydat3 mdme 185 1 0",
                        "pydat3 mdme 186 1 0",
                        "pydat3 mdme 187 1 0",
                        "pydat1 parj 90 20000",
                        "pydat3 mdcy 15 1 0"]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )


# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
topAlg += MultiLeptonFilter()

MultiLeptonFilter = topAlg.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 5000.
MultiLeptonFilter.Etacut = 3.0
MultiLeptonFilter.NLeptons = 2


#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------

try:
     StreamEVGEN.RequireAlgs +=  [ "MultiLeptonFilter" ]
except Exception, e:
     pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.78
#==============================================================
#
# End of job options file
#
###############################################################
