# quark compositeness with pythia & destructive interference
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

#--------------------------------------------------------------
# File prepared by POD & FR
#--------------------------------------------------------------
Pythia.PythiaCommand += [
                         "pysubs msel 51",       # technicolor & compositeness
			 "pysubs ckin 3 560.", 
			 "pysubs ckin 4 1120.",
                         "pytcsm rtcm 41 5000.", # compositeness scale
                         "pytcsm rtcm 42 1",     # positive interference
                         "pytcsm itcm 5 2"       # compositeness
                        ]
                              

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
