###############################################################
#
# Job options file
#
# Pythia W->taunu (inclusive), AMBT1_EIGEN3Px2 tune
#   implementation based on JobO for 11159X
#
# Responsible person(s)
#   6 Oct, 2010-xx xxx, 20xx: Martin Flechl (Martin.Flechl@cern.ch)
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

# ... Pythia
include ( "MC10JobOptions/MC10_Pythia_Common.py" )

# AMBT1 systematic variation: eigentune 3+ (2-sigma)
Pythia.PythiaCommand += [
                         "pypars parp 77 0.94191",
                         "pypars parp 78 0.45867",
                         "pypars parp 82 2.26510",
                         "pypars parp 84 0.63362",
                         "pypars parp 90 0.21056"]

Pythia.PythiaCommand += [ "pysubs msel 0",
                          "pysubs msub 2 1",
                          "pydat1 parj 90 20000", # Turn off photon rad.
                          "pydat3 mdcy 15 1 0",   # Turn off tau decays.
                          "pydat3 mdme 190 1 0",
                          "pydat3 mdme 191 1 0",
                          "pydat3 mdme 192 1 0",
                          "pydat3 mdme 193 1 0",
                          "pydat3 mdme 194 1 0",
                          "pydat3 mdme 195 1 0",
                          "pydat3 mdme 196 1 0",
                          "pydat3 mdme 197 1 0",
                          "pydat3 mdme 198 1 0",
                          "pydat3 mdme 199 1 0",
                          "pydat3 mdme 200 1 0",
                          "pydat3 mdme 201 1 0",
                          "pydat3 mdme 202 1 0",
                          "pydat3 mdme 203 1 0",
                          "pydat3 mdme 204 1 0",
                          "pydat3 mdme 205 1 0",
                          "pydat3 mdme 206 1 0",    # Switch for W->enu.
                          "pydat3 mdme 207 1 0",    # Switch for W->munu.
                          "pydat3 mdme 208 1 1",    # Switch for W->taunu.
                          "pydat3 mdme 209 1 0"
                          ]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.97

#==============================================================
#
# End of job options file
#
###############################################################

