###############################################################

#

# Job options file

# usage : 

# Sarah Allwood-Spiers

#==============================================================



# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )

MessageSvc = Service( "MessageSvc" )

MessageSvc.OutputLevel = 3



#--------------------------------------------------------------

# Generator

#--------------------------------------------------------------



### Herwig



from AthenaCommon.AlgSequence import AlgSequence 

topAlg = AlgSequence("TopAlg") 

 

try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common.py" )
  if runArgs.ecmEnergy == 14000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_14TeV.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig




# ... Tauola



Herwig.HerwigCommand+= [ "iproc alpgen" ]

Herwig.HerwigCommand+= [ "taudec TAUOLA"]



include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )



# ... Photos

include ( "MC10JobOptions/MC10_Photos_Fragment.py" )





#--------------------------------------------------------------

# Filter

#--------------------------------------------------------------

# no filter

#---



from MC10JobOptions.AlpgenEvgenConfig import evgenConfig

 
# inputfilebase
evgenConfig.inputfilebase = 'alpgen.106266.ttbarlnqqNp2_pt20'



#Information on sample 106266

#Filter efficiency = 1.000

#MLM matching efficiency = 0.24

#Number of MEs/tarball = 2500

#which will produce 600 output events.

#Alpgen cross section = 91.53 pb

#Herwig cross section = Alpgen cross section *eff(MLM) = 22.40 pb 

#Lumi/500 events = 500/XS(Herwig) = 22.32 pb-1

#Filter efficiency estimate reduced by 10% to produce 555 events on average, of which only 500 will be used in further processing

evgenConfig.efficiency = 0.9



#==============================================================

#

# End of job options file

#

###############################################################

