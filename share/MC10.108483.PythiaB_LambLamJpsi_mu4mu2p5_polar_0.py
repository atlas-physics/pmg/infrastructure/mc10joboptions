###############################################################################
#
# PRODUCTION SYSTEM FRAGMENT
# Generation of Lambda_b0 -> Lambda0(p pi) J/psi(mu+ mu-) decay using EvtDecay
# Author: Daniel Scheirich
#
###############################################################################

# MAKE EVTGEN USER DECAY FILE ON THE FLY
f = open("LAMBDAB.DEC","w")
f.write("\n")
f.write("Alias myJ/psi J/psi\n")
f.write("Decay myJ/psi\n")
f.write("1.0000    mu+  mu-     VLL;\n")
f.write("Enddecay\n")
f.write("\n")
f.write("Alias myLambda0 Lambda0\n")
f.write("Decay myLambda0\n")
f.write("1.0000  p+  pi-           HELAMP 0.906 0.0 0.423 0.0;\n")
f.write("Enddecay\n")
f.write("\n")
f.write("Decay Lambda_b0\n")
f.write("1.0000  myLambda0 myJ/psi HELAMP 0.813 1.534 0.429 -1.612 0.260 1.231 0.295 -1.849;\n")
f.write("Enddecay\n")
f.write("\n")
f.write("End\n")
f.close()

#------------------------------------------------------------------------------
# Production driving parameters
#------------------------------------------------------------------------------

from MC10JobOptions.PythiaBEvgenConfig import evgenConfig
evgenConfig.auxfiles   = [ "Bdecays0.dat","DECAY.DEC","pdt.table" ]
evgenConfig.minevents  = 500
evgenConfig.efficiency = 0.09

#------------------------------------------------------------------------------
# Import all needed algorithms (in the proper order)
#------------------------------------------------------------------------------

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include( "MC10JobOptions/MC10_PythiaB_Common.py" )

from EvtGen_i.EvtGen_iConf import EvtDecay
topAlg += EvtDecay()
EvtDecay = topAlg.EvtDecay

from GeneratorFilters.GeneratorFiltersConf import BSignalFilter
topAlg += BSignalFilter()
BSignalFilter = topAlg.BSignalFilter

#------------------------------------------------------------------------------
# PythiaB parameters settings
#------------------------------------------------------------------------------

# Stop all B-decays in Pythia and left them to decay by EvtGen
include( "MC10JobOptions/MC10_PythiaB_StopPytWeakBdecays.py" )

# Force user-finsel requiring Lambda_b in the final state
PythiaB.ForceDecayChannel = "LambdabJpsimumuLambda"
PythiaB.DecayChannelParameters = [7.,2.7]  # Minimum pt of Lambdab = 7, and maximum abs(etha)=2.7

# Production settings
include( "MC10JobOptions/MC10_PythiaB_Btune.py" )
PythiaB.PythiaCommand += ["pysubs ckin 3 8.",
                          "pysubs ckin 9 -3.5",
                          "pysubs ckin 10 3.5",
                          "pysubs ckin 11 -3.5",
                          "pysubs ckin 12 3.5",
                          "pysubs msel 1"]


# Pythia b-quark cuts
PythiaB.cutbq = [ "7. 2.7 or 0. 0." ]
#PythiaB.cutbq = ["0. 102.5 and 10. 2.7"]

# Repeated hadronization
PythiaB.mhadr = 4

#------------------------------------------------------------------------------
# Signal event filtering
#------------------------------------------------------------------------------

# Muon pT cuts selection
BSignalFilter.LVL1MuonCutOn  = True
BSignalFilter.LVL1MuonCutPT  = 4000.
BSignalFilter.LVL1MuonCutEta = 2.5
BSignalFilter.LVL2MuonCutOn  = True
BSignalFilter.LVL2MuonCutPT  = 2500.
BSignalFilter.LVL2MuonCutEta = 2.5

# Hadronic tracks cuts
BSignalFilter.Cuts_Final_hadrons_switch = True
BSignalFilter.Cuts_Final_hadrons_pT     = 500.
BSignalFilter.Cuts_Final_hadrons_eta    = 2.5
BSignalFilter.BParticle_cuts            = 5122

#------------------------------------------------------------------------------
# EvtGen decay table for signal
#------------------------------------------------------------------------------

EvtDecay.userDecayTableName = "LAMBDAB.DEC"
EvtDecay.setLambdabP=0.
EvtDecay.setLambdabSDM=True

#------------------------------------------------------------------------------
# POOL / Root output
#------------------------------------------------------------------------------

try:
  StreamEVGEN.RequireAlgs += [ "BSignalFilter" ]
except Exception, e:
  pass

###############################################################################
#
# End of job options fragment 
#
###############################################################################
