# ZZ diboson production with a lepton filter
# Author: Alan Barr
#         Tiesheng Dai (Configure with 7TeV and 10TeV ECM, Feb. 9, 2010)
#
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 
# ... Herwig
try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_Herwig_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_Herwig_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_Herwig_Common.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig

#--------------------------------------------------------------
# Event related parameters
#--------------------------------------------------------------
Herwig.HerwigCommand += [ "iproc 12810",  
                          "taudec TAUOLA" ]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )



# Add the filters:
# Electron or Muon filter
from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
topAlg += LeptonFilter()

LeptonFilter = topAlg.LeptonFilter
LeptonFilter.Ptcut = 10.*GeV
LeptonFilter.Etacut = 2.8

try:
     StreamEVGEN.RequireAlgs += [ "LeptonFilter" ]
except Exception, e:
     pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.HerwigEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.185

#==============================================================
#
# End of job options file
#
###############################################################
