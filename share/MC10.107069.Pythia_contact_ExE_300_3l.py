#--------------------------------------------------------------
#Job options file
#pp --> e , e*( e* mass = 300 GeV )
#Responsible person: Piyali Banerjee 
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

#--------------------------------------------------------------
# Algorithms Private Options
#--------------------------------------------------------------

# Channel switches

etoGnu = "0"
etoZe = "1"
etoWe = "0"
pptodx = "0"
pptoux = "0" 
pptoex = "1"

include ( "MC10JobOptions/MC10_Pythia_Common.py" ) 
Pythia.PythiaCommand+= ["pysubs msel 0",
                        "pysubs msub 167 "+pptodx,
                        "pysubs msub 168 "+pptoux,
                        "pysubs msub 169 "+pptoex,
                        "pydat2 pmas 4000001 1 1000.0",
                        "pydat2 pmas 4000002 1 1000.0",
                        "pydat2 pmas 4000011 1 300.0",
                        "pytcsm rtcm 41 300.",   # lambda scale
                        "pypars mstp 32 4",
                        "pydat3 mdme 174 1 0",
                        "pydat3 mdme 175 1 0",
                        "pydat3 mdme 176 1 0",
                        "pydat3 mdme 177 1 0",
                        "pydat3 mdme 178 1 0",
                        "pydat3 mdme 179 1 0",
                        
                        "pydat3 mdme 182 1 1",
                        "pydat3 mdme 183 1 0",      # to nu_e
                        "pydat3 mdme 184 1 1",
                        "pydat3 mdme 185 1 0",      # to nu_mu
                        
                        "pydat3 mdme 186 1 0",
                        "pydat3 mdme 187 1 0",
                        "pydat3 mdme 188 1 0",
                        "pydat3 mdme 189 1 0",
                        
                        "pydat3 mdme 206 1 0",
                        "pydat3 mdme 207 1 1",

                        "pydat3 mdme 208 1 0", # no tau
                        "pydat3 mdme 209 1 0",
                        
                        "pydat3 mdme 190 1 0",
                        "pydat3 mdme 191 1 0",
                        "pydat3 mdme 192 1 0",
                        "pydat3 mdme 194 1 0",
                        "pydat3 mdme 195 1 0",
                        "pydat3 mdme 196 1 0",
                        "pydat3 mdme 198 1 0",
                        "pydat3 mdme 199 1 0",
                        "pydat3 mdme 200 1 0",

                        "pydat3 mdme 4079 1 "+etoGnu,
                        "pydat3 mdme 4080 1 "+etoZe,
                        "pydat3 mdme 4081 1 "+etoWe,
                        
                        "pydat3 mdcy 15 1 0",
                        "pydat1 parj 90 20000",
                        "pyinit pylistf 1",
                        "pyinit dumpr 1 10",
                        "pyinit pylisti 12"
                        ]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
#==============================================================
#
# End of job options file
#
###############################################################

