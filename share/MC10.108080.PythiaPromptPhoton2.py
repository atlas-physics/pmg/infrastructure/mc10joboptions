#-------------------------------------------------------------
# Pythia prompt photons w/ p_T(photon) > 2 GeV
# Prepared by J. Tojo, January 2009
#-------------------------------------------------------------

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

Pythia.PythiaCommand += [ "pysubs msel 0",
                          "pysubs ckin 3 2.",
                          "pysubs msub 14 1",
                          "pysubs msub 29 1",
                          "pysubs msub 18 1",
                          "pysubs msub 114 1",
                          "pysubs msub 115 1" ]

#-------------------------------------------------------------
# Filter
#-------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import PhotonFilter
topAlg += PhotonFilter()

PhotonFilter = topAlg.PhotonFilter
PhotonFilter.Ptcut = 2000.
PhotonFilter.Etacut = 2.7
PhotonFilter.NPhotons = 1

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------
try:
     StreamEVGEN.RequireAlgs +=  [ "PhotonFilter" ]
except Exception, e:
     pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
# efficiency = 5000/9508*0.9 = 0.473 at 900 GeV
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.473
