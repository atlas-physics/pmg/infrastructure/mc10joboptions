from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

# --> use the following pythia tune if CTEQ6l1 pdf has been used
include ( "MC10JobOptions/MC10_PythiaMC09p_Common.py" )

Pythia.PythiaCommand += [ "pyinit user madgraph",
                          "pyinit pylisti -1","pyinit pylistf 1","pyinit dumpr 1 2",
                          "pydat1 parj 90 20000", # Turn off FSR.
                          "pydat3 mdcy 15 1 0"   # Turn off tau decays.
                          ]

## ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )


## ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

from MC10JobOptions.MadGraphEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'group10.phys-gener.madgraph50613.119329.HppHmm_300GeV.TXT.v1'

