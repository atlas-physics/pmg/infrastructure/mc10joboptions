###############################################################
#
# Job options file
# (based on original from Wouter Verkerke)
#
#==============================================================
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 3

#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------

#-- Dll's and Algorithms
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 

try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common.py" )
  if runArgs.ecmEnergy == 14000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common_14TeV.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig

# ... Main generator : Herwig
Herwig.HerwigCommand += [ "taudec TAUOLA"]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import MassRangeFilter
topAlg += MassRangeFilter()

MassRangeFilter = topAlg.MassRangeFilter
MassRangeFilter.PtCut  = 15000.
MassRangeFilter.PtCut2 = 15000.
MassRangeFilter.EtaCut  = 2.6
MassRangeFilter.EtaCut2 = 2.6
MassRangeFilter.InvMassMin = 300000.
MassRangeFilter.InvMassMax = 450000.
MassRangeFilter.PartId  = 11
MassRangeFilter.PartId2 = 11

try:
    StreamEVGEN.RequireAlgs = [ "MassRangeFilter" ]
except Exception, e:
    pass

from MC10JobOptions.McAtNloEvgenConfig import evgenConfig

# dummy needed
evgenConfig.inputfilebase = 'mcatnlo'
try:
  if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputfilebase = 'group09.phys-gener.mcatnlo342.115402.ttbar_2e15_300M450_eskim_7TeV.TXT.v1'
  if runArgs.ecmEnergy == 8000.0:
    evgenConfig.inputfilebase = 'group09.phys-gener.mcatnlo342.115402.ttbar_2e15_300M450_eskim_8TeV.TXT.v1'
  if runArgs.ecmEnergy == 10000.0:
    evgenConfig.inputfilebase = 'group09.phys-gener.mcatnlo342.000000.ttbar_2e15_300_450_eskim_10TeV.TXT.v1' 
except NameError:
  pass

# 15.6.6.5
# 179/200621=0.0008922
#evgenConfig.efficiency = 0.000892
# 15.6.12.5
# (617+635+626)/(702784+28+701765+28+701856+20)=0.00089153427
#evgenConfig.efficiency = 0.000892
#evgenConfig.efficiency = 0.0014*5
# 15.6.13.6
# (566+562)/(714286+713724)=0.00078991
evgenConfig.efficiency = 0.000790
evgenConfig.minevents  = 500
evgenConfig.maxeventsfactor = 1.1

#==============================================================
#
# End of job options file
#
###############################################################
