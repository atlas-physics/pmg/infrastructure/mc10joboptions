###############################################################
#
# Job options file
# C Collins-Tooth
# usage : 
#Evgen_trf.py ecmEnergy=7000 runNumber=116103 firstEvent=1 maxEvents=-1 randomSeed=1 jobConfig=MC10.116103.AlpgenJimmyttbarlnqqNp1_ttbarplusjetsfilter.py outputEvgenFile=/home/chrisc/mydata7/ATLFAST-AOD-ttjj-V15-Filtertests/herwigoutputs/PRODUCTIONTESTS/testHerwigNp1.root histogramFile='NONE' ntupleFile='NONE' inputGeneratorFile=/data/atlas03/chrisc/ALPGEN-output/ALPGEN-2Qwork-outputsdirectory/Baseline_ttbarlnqqNp1/group09.phys-gener.alpgen.116103.ttbarlnqqNp1.TXT.v1._00001.tar.gz > logfile.log
#==============================================================

# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 3

#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------

### Herwig

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 
try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common.py" )
  if runArgs.ecmEnergy == 14000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_14TeV.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig


  pass

#from Herwig_i.Herwig_iConf import Herwig

# ... Tauola
#FIX FROM J. FERRANDO:
#Herwig.HerwigCommand += [ "msflag  0",
#                          "iproc alpgen",
#                          "taudec TAUOLA",
#                          ]
#J. FERRANDO FIX - COMMENTED 2 LINES OUT:
Herwig.HerwigCommand+= [ "iproc alpgen" ]
Herwig.HerwigCommand+= [ "taudec TAUOLA"]

include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


#--------------------------------------------------------------
# JetRec on Truth
#--------------------------------------------------------------
try:
  from JetRec.JetGetters import *
  c4=make_StandardJetGetter('Cone',0.4,'Truth')
  c4alg = c4.jetAlgorithmHandle()
  c4alg.JetConeFinder.SeedPt = 2.*GeV
  c4alg.JetInitialEtCut.UseTransverseEnergy = True
  c4alg.JetInitialEtCut.MinimumSignal = 0.*MeV
  c4alg.JetFinalEtCut.UseTransverseEnergy =  True
  c4alg.JetFinalEtCut.MinimumSignal = 10.*GeV
except Exception, e:
  pass




#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import TTbarPlusJetsFilter
topAlg += TTbarPlusJetsFilter()
TTbarPlusJetsFilter = topAlg.TTbarPlusJetsFilter
TTbarPlusJetsFilter.InputJetContainer = "Cone4TruthJets"
try:
  StreamEVGEN.RequireAlgs += ["TTbarPlusJetsFilter"]
  #Stream1.RequireAlgs = [ "TTbarPlusJetsFilter" ]
except Exception, e:
  pass
      

from MC10JobOptions.AlpgenEvgenConfig import evgenConfig
 
# inputfilebase
evgenConfig.inputfilebase = 'group09.phys-gener.alpgen.116103.ttbarlnqqNp1.TXT.v1'
evgenConfig.minevents=500
evgenConfig.efficiency = 0.9

#MLM matching efficiency (UNW->MLMmatched) = 24.76 % (49539/200000) (Will use additional Safety factor 0.9)
#Filter efficiency (MLMmatched->FilteredEvents) = 21.42% (10611/49539) (Will use additional Safety factor 0.9 too)
#Alpgen events/ input file (to produce 555 evts) = 555/[(0.2476*0.9) * (0.2142*0.9)] = 12915 events.
#Alpgen cross section = 52.74 pb
#Herwig cross section = Alpgen cross section *eff(MLM) = 13.17 pb
#Lumi/500 FILTERED events = 500/[XS(Herwig) *(10611/49539) ] =  177.24pb-1

#==============================================================
#
# End of job options file
#
###############################################################
