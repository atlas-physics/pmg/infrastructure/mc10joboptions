###############################################################
#
# Job options file
#
# Pythia Z->tautau (inclusive) w/o GEF
#    Perugia2010 tune
#
# Perugia2010 parameters as in arXiv:1005.3457v1 [hep-ph]
# Perugia2010 (PYTUNE 327; from 6.423 onwards)
# reference: MC10.106052.PythiaZtautau.py
#
# Responsible person(s)
#   21 July, 2010: Martin Flechl (Martin.Flechl@cern.ch)
#
#==============================================================

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

# ... Pythia
include ( "MC10JobOptions/MC10_PythiaPerugia2010_Common.py" )
Pythia.PythiaCommand += [ "pysubs msel 0",
                         "pydat1 parj 90 20000", # Turn off FSR.
                         "pydat3 mdcy 15 1 0",   # Turn off tau decays.
                         "pysubs msub 1 1",        # Create Z bosons.
                         "pysubs ckin 1 60.0",     # Lower invariant mass.
                         "pydat3 mdme 174 1 0",
                         "pydat3 mdme 175 1 0",
                         "pydat3 mdme 176 1 0",
                         "pydat3 mdme 177 1 0",
                         "pydat3 mdme 178 1 0",
                         "pydat3 mdme 179 1 0",
                         "pydat3 mdme 182 1 0",    # Switch for Z->ee.
                         "pydat3 mdme 183 1 0",
                         "pydat3 mdme 184 1 0",    # Switch for Z->mumu.
                         "pydat3 mdme 185 1 0",
                         "pydat3 mdme 186 1 1",    # Switch for Z->tautau.
                         "pydat3 mdme 187 1 0",
                         "pydat3 mdme 188 1 0",
                         "pydat3 mdme 189 1 0"
                         ]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.95
#==============================================================
#
# End of job options file
#
###############################################################
