###############################################################
#
# Job options file for generation of B events 
# no decay channel is specified.
# Only events containing at least one muon  
# with pT>4GeV |eta|<2.5 are written to output
# Selection criteria can be changed by datacards
#==============================================================
#--------------------------------------------------------------
# General Application Configuration options
#--------------------------------------------------------------
from MC10JobOptions.PythiaBEvgenConfig import evgenConfig
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_PythiaB_Common.py" )

#--------------------------------------------------------------				 
#              PARAMETERS  SPECIFIC  TO   PYTHIABMODULE
#--------------------------------------------------------------
PythiaB.ForceBDecay = "no"
PythiaB.ForceCDecay = "no"

include( "MC10JobOptions/MC10_PythiaB_Btune.py" )

PythiaB.PythiaCommand += ["pysubs ckin 3 70.",
                          "pysubs ckin 4 140.",
				 "pysubs msel 1"]
#--------------------------------------------------------------
# -------------  DEFINE SELECTION CUTS  -------------
#--------------------------------------------------------------				
PythiaB.flavour =  5.				
#  ------------- Selections on b  quarks   -------------
PythiaB.cutbq = ["5. 2.5 or 5. 2.5"]
#  ------------- LVL1 muon cuts 0=OFF 1=ON -------------
PythiaB.lvl1cut = [ 0.,  4., 2.5]
#  ------------- LVL2 muon/electron cuts  0=OFF 1=ON-------------
PythiaB.lvl2cut = [ 0.,  13.,     2.5,   2.5]
#PythiaB.lvl2cut = { 0.,  11.,     6.,   2.5};
#  ------------- Offline cuts 0=OFF 1=ON -------------
PythiaB.offcut = [ 0., 0., 102.5, 0., 102.5, 0., 102.5]
#  ------------- Number of repeated hadronization mhadr -------------
PythiaB.mhadr =  1. 
#  ------------- For how many events store B-chain in NTUPLE -------------

#==============================================================
#
# End of job options file
#
###############################################################
