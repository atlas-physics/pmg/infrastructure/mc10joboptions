# dijet production with pythia
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

#--------------------------------------------------------------
# Algorithms Private Options
#--------------------------------------------------------------
include ( "MC10JobOptions/MC10_Pythia_Common.py" )
Pythia.PythiaCommand += [ "pysubs msel 0",
                          "pysubs ckin 3 32.6",
                          "pysubs ckin 13 2.5",
                          "pysubs ckin 14 5",
                          "pysubs msub 11 1",
                          "pysubs msub 12 1",
                          "pysubs msub 13 1",
                          "pysubs msub 68 1",
                          "pysubs msub 28 1",
                          "pysubs msub 53 1"]

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
#evgenConfig.efficiency = 0.9

# MC9 due to TestHepMC
# 4916/5556*0.9 = 0.7963
evgenConfig.efficiency = 0.796
