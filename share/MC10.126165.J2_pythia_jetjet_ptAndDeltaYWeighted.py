###############################################################
#
# Job options file
#
# Pythia J2 event generation with pT-deltaY weighting.
# cross section as default pythia sample = 4.1*10^4 nb
#
# Prepared by Graham Jones, Graham.Jones [at] cern.ch 
#==============================================================


# dijet production with pythia
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

Pythia.PythiaCommand += [
			      "pysubs msel 0",
			      "pysubs ckin 3 35.",
			      "pysubs ckin 4 70.",
			      "pysubs msub 11 1",
			      "pysubs msub 12 1",
			      "pysubs msub 13 1",
			      "pysubs msub 68 1",
			      "pysubs msub 28 1",
			      "pysubs msub 53 1"]

#--------------------------------------------------------------
# Truth Jets
#--------------------------------------------------------------

# Make truth jets for JetForwardFilter:
from AthenaCommon.AppMgr import ServiceMgr as svcMgr
include( "AthenaCommon/Atlas_Gen.UnixStandardJob.py" )
from JetRec.JetGetters import *
Kt6Alg = make_StandardJetGetter('AntiKt',0.6,'Truth').jetAlgorithmHandle()
Kt6Alg.AlgTools['JetFinalPtCut'].MinimumSignal = 7.0*GeV

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

## Truth filter
# Add the filter to the correct sequence!
from GeneratorFilters.GeneratorFiltersConf import JetForwardFilter
forwardFilter=JetForwardFilter()
topAlg += forwardFilter

# General properties
forwardFilter.JetContainer = "AntiKt6TruthJets"

# Kinematic cuts
forwardFilter.MinDeltaY = 0.0
forwardFilter.MaxDeltaY = 100.0
forwardFilter.MinPt1 = 12.0 #GeV
forwardFilter.MinPt2 = 12.0
forwardFilter.MaxPt1 = 70000.0
forwardFilter.MaxPt2 = 70000.0

#Gaussian mixture model describing pdf of unweighted sample
forwardFilter.muXs = [ 41.1906834879, 56.4181241182, 39.4076233489, 67.6023125194, 46.8288940238  ]
forwardFilter.muYs = [ 1.18533830295, 1.71836046335, 2.77224133783, 2.32242182429, 1.30465662185  ]
forwardFilter.sigmaXs = [ 6.84412318483, 10.5307300992, 7.03608163156, 15.3386288535, 9.83865204003  ]
forwardFilter.sigmaYs = [ 0.77216163022, 1.10053396833, 1.4108668406, 1.42540976559, 0.850795031316  ]
forwardFilter.rhos = [ -0.0238058789002, -0.110420425708, 0.0837159480683, -0.234414693131, -0.00520411125055  ]
forwardFilter.weights = [ 0.295657908394, 0.172900516491, 0.222801249733, 0.0623521648388, 0.246288160543  ]

#Target probability (how much weighting required roughly)
#Lower = more suppression
import math
forwardFilter.SuppressionFactor = 5.0*math.pow(10.0, -8.0)

#--------------------------------------------------------------
# Apply filtering
#--------------------------------------------------------------

try:
  StreamEVGEN.RequireAlgs += [ "JetForwardFilter" ]
except Exception, e:
  pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
# N_gen = 2601817354; N_pass = 300000 -> eff = 1.153*10^-4
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.0001153
evgenConfig.minevents=500 #~21 hours
evgenConfig.weighting=0
