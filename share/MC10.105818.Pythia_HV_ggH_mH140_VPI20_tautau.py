from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_Pythia_Common.py" )
# HV Higgs production with pythia

Pythia.PythiaCommand += ["pysubs msel 0"]
Pythia.PythiaCommand += ["pydat1 parj 22 2"]
Pythia.PythiaCommand += ["pydat1 parj 71 500000"] #max ct of particles

#Create gg --> H0 --> vpi vpi 
Pythia.PythiaCommand += ["pysubs msub 152 1"]
#set the mass of the H0
Pythia.PythiaCommand += ["pydat2 pmas 35 1 140.D0"]
#set the mass and lifetime of the A0
Pythia.PythiaCommand += ["pydat2 pmas 36 1 20.D0", 
                         "pydat2 pmas 36 4 630.D0"]
#Set the decay modes of the H0
Pythia.PythiaCommand += ["pydat3 mdme 334 1 0",
                         "pydat3 mdme 335 1 0",
                         "pydat3 mdme 336 1 0",
                         "pydat3 mdme 337 1 0",
                         "pydat3 mdme 338 1 0",
                         "pydat3 mdme 339 1 0",
                         "pydat3 mdme 340 1 0",
                         "pydat3 mdme 341 1 0",
                         "pydat3 mdme 342 1 0",
                         "pydat3 mdme 343 1 0",
                         "pydat3 mdme 344 1 0",
                         "pydat3 mdme 345 1 0",
                         "pydat3 mdme 346 1 0",
                         "pydat3 mdme 347 1 0",
                         "pydat3 mdme 348 1 0",
                         "pydat3 mdme 349 1 0",
                         "pydat3 mdme 350 1 0",
                         "pydat3 mdme 351 1 0",
                         "pydat3 mdme 352 1 0",
                         "pydat3 mdme 353 1 0",
                         "pydat3 mdme 354 1 0",
                         "pydat3 mdme 355 1 0",
                         "pydat3 mdme 356 1 0",
                         "pydat3 mdme 357 1 1"]
#set only the decay mode to vpion equal to one 
Pythia.PythiaCommand += ["pydat3 mdme 420 1 0",
                         "pydat3 mdme 421 1 0",
                         "pydat3 mdme 422 1 0",
                         "pydat3 mdme 423 1 0",
                         "pydat3 mdme 424 1 0",
                         "pydat3 mdme 425 1 0",
                         "pydat3 mdme 426 1 0",
                         "pydat3 mdme 427 1 0",
                         "pydat3 mdme 428 1 0",
                         "pydat3 mdme 429 1 0",
                         "pydat3 mdme 430 1 1",#tau tau
                         "pydat3 mdme 431 1 0",
                         "pydat3 mdme 432 1 0",
                         "pydat3 mdme 433 1 0",
                         "pydat3 mdme 434 1 0",
                         "pydat3 mdme 435 1 0",
                         "pydat3 mdme 436 1 0",
                         "pydat3 mdme 437 1 0",
                         "pydat3 mdme 438 1 0",
                         "pydat3 mdme 439 1 0",
                         "pydat3 mdme 440 1 0"                        
                         ]

#set branching fractions
Pythia.PythiaCommand += ["pydat3 brat 357 0.999"]
Pythia.PythiaCommand += ["pydat3 brat 430 0.999"]

#include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )
#include ( "MC10JobOptions/MC10_Photos_Fragment.py" )
#--------------------------------------------------------------

# Configuration for EvgenJobTransforms

#--------------------------------------------------------------

from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.95

