###############################################################
#
# Job options file for dijet production with pythia
#
# author: C. Gwenlan (June'10)
# Perugia HARD (PYTUNE 321) 
# [P. Skands, Perugia MPI workshop Oct08, T. Sjostrand & P. Skands hep-ph/0408302] 
# reference JO: MC10.105001.pythia_minbias.py
#===============================================================


# min bias sample (ND).
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

# use Perugia HARD tune 
include ( "MC10JobOptions/MC10_PythiaPerugiaHARD_Common.py" )

Pythia.PythiaCommand += [  "pysubs msel 1" ]

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig

evgenConfig.efficiency = 0.9


#==============================================================
#
# End of job options file
#
###############################################################

