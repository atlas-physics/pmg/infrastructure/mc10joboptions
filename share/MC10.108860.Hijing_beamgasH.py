#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# configuring the Athena application for a 'generator' job
import AthenaCommon.AtlasUnixGeneratorJob

# make sure we are loading the ParticleProperty service
from AthenaCommon.AppMgr import ServiceMgr as svcMgr

from AthenaCommon.Configurable import Configurable
svcMgr.MessageSvc.OutputLevel = 3

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

from Hijing_i.Hijing_iConf import Hijing
topAlg += Hijing()

#--------------------------------------------------------------
# Algorithms Private Options
#--------------------------------------------------------------

#----------------------
# Hijing Parameters
#----------------------
Hijing = topAlg.Hijing
Hijing.randomizeVertices = True
Hijing.wide = False
Hijing.Initialize = ["efrm 5000.", "frame LAB" ]
#, "targ A","iat 1", "izt 1" ]
Hijing.randomizeP = True # mirrors the event around


#from TruthExamples.TruthExamplesConf import PrintMC
#topAlg += PrintMC()


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.HijingEvgenConfig import evgenConfig
#==============================================================
#
# End of CSC hijing
#
###############################################################
