# Zprime production with pythia
# remove Z',Z,g interference and apply low mass cut OSC 12/2010
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

#--------------------------------------------------------------
# Algorithms Private Options
#--------------------------------------------------------------

# Zprime resonance mass (in GeV)
ZprimeMass = 1000

# Minimum mass for Drell-Yan production (in GeV)
ckin1 = 10

include ( "MC10JobOptions/MC10_PythiaResMod_Common.py" )

# enable using the modified pysgex.F
#(default: UseResMod=0; do not use modified pysgex.F)
#(UseResMod=1; use modified pysgex.F)
PythiaResMod.UseResMod=1

#other commands for PythiaResMod are as they are for Pythia alg.
# Z prime specific parameters for pythia :
PythiaResMod.PythiaCommand += [
       "pysubs msel 0",
       "pysubs msub 141 1",   # Z',Z,g with interference
       "pypars mstp 44 3",    # only Z'
       "pydat3 mdme 190 1 0",		 #!W decay into dbar u
       "pydat3 mdme 191 1 0",		 #!W decay into dbar c

       "pydat3 mdme 192 1 0",		 #!W decay into dbar t
       "pydat3 mdme 194 1 0",		 #!W decay into sbar u
       "pydat3 mdme 195 1 0",		 #!W decay into sbar c
       "pydat3 mdme 196 1 0",		 #!W decay into sbar t
       "pydat3 mdme 198 1 0",		 #!W decay into bbar u

       "pydat3 mdme 199 1 0",		 #!W decay into bbar c
       "pydat3 mdme 200 1 0",		 #!W decay into bbar t
       "pydat3 mdme 206 1 1",		 #!W decay into e+ nu_e
       "pydat3 mdme 207 1 1",		 #!W decay into mu+ nu_mu
       "pydat3 mdme 208 1 1",		 #!W decay into tau+ nu_tau	

       # All decays of Z'
       "pydat3 mdme 289 1 0",	   # Z' -> d d-bar
       "pydat3 mdme 290 1 0",	   # Z' -> u u-bar
       "pydat3 mdme 291 1 0",	   # Z' -> s s-bar
       "pydat3 mdme 292 1 0",	   # Z' -> c c-bar
       "pydat3 mdme 293 1 0",	   # Z' -> b b-bar
       "pydat3 mdme 294 1 1",	   # Z' -> t t-bar
       "pydat3 mdme 295 1 0",
       "pydat3 mdme 296 1 0",
       "pydat3 mdme 297 1 0",	   # Z' -> e+ e-
       "pydat3 mdme 298 1 0",	   # Z' -> nu_e nu_e
       "pydat3 mdme 299 1 0",	   # Z' -> mu+ mu-
       "pydat3 mdme 300 1 0",	   # Z' -> nu_mu nu_mu
       "pydat3 mdme 301 1 0",	   # Z' -> tau+ tau-
       "pydat3 mdme 302 1 0",	   # Z' -> nu_tau nu_tau
       "pydat3 mdme 303 1 0",
       "pydat3 mdme 304 1 0",
       "pydat3 mdme 305 1 0",
       "pydat3 mdme 306 1 0",
       "pydat3 mdme 307 1 0",
       "pydat3 mdme 308 1 0",
       "pydat3 mdme 309 1 0",
       "pydat3 mdme 310 1 0",	      
       "pysubs ckin 1 "+str(ckin1),
       "pydat2 pmas 32 1 "+str(ZprimeMass),
       
        # First generation
       "pydat1 paru 121 -0.693",
       "pydat1 paru 122 -1",
       "pydat1 paru 123 0.387",
       "pydat1 paru 124 1",
       "pydat1 paru 125 -0.08",
       "pydat1 paru 126 -1",
       "pydat1 paru 127 1",
       "pydat1 paru 128 1",

       # Second generation
       "pydat1 parj 180 -0.693",
       "pydat1 parj 181 -1",
       "pydat1 parj 182 0.387",
       "pydat1 parj 183 1",
       "pydat1 parj 184 -0.08",
       "pydat1 parj 185 -1",
       "pydat1 parj 186 1",
       "pydat1 parj 187 1",

       # Third generation
       "pydat1 parj 188 -0.693",
       "pydat1 parj 189 -1",
       "pydat1 parj 190 0.387",
       "pydat1 parj 191 1",
       "pydat1 parj 192 -0.08",
       "pydat1 parj 193 -1",
       "pydat1 parj 194 1",
       "pydat1 parj 195 1"     
       ]

# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = INFO

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------
# Add POOL persistency

# 2101 = EventInfo
# 133273 = MCTruth (HepMC)
# optionally
include("AthenaSealSvc/AthenaSealSvc_joboptions.py" )
AthenaSealSvc.CheckDictionary = True

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaResModEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
#==============================================================
#
# End of job options file
#
###############################################################
