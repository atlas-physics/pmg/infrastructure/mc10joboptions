# Diphoton production with single photon filter

# non-resonant production of gamma gamma with Pythia (including gg->gamgam)
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )
Pythia.PythiaCommand += [ "pysubs msel 0",
                          "pysubs msub 18 1",
                          "pysubs msub 114 1",
                          "pysubs ckin 3 20.",
                          "pydat1 parj 90 20000." ]

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

from GeneratorFilters.GeneratorFiltersConf import PhotonFilter
topAlg += PhotonFilter()

PhotonFilter = topAlg.PhotonFilter
PhotonFilter.Ptcut = 25000.
PhotonFilter.Etacut = 4.0
PhotonFilter.NPhotons = 1

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------

try:
     StreamEVGEN.RequireAlgs +=  [ "PhotonFilter" ]
except Exception, e:
     pass
               
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
# efficiency = 7172/10000*0.9 = 0.645
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.645
