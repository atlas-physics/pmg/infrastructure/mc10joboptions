#----------------------------------------------------------------------------------
# Randall-Sundrum graviton G* production, decaying to all SM particles
# Prepared by S.L. Cheung (Toronto) <slcheung@physics.utoronto.ca>,
#             E. Feng (Chicago) <Eric.Feng@cern.ch> 
#----------------------------------------------------------------------------------


#----- Model parameters
m_G  = 900.      # Mass of G* resonance [GeV]
k_mG = 0.54     # = sqrt(2)*x1*k/MPl, where 0.01 < k/MPl < 0.1


#----- Joboptions fragment
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")
include ( "MC10JobOptions/MC10_Pythia_Common.py" )     # MC10 common settings for Pythia
Pythia.PythiaCommand += [ "pysubs msel 0",

                          #--- Graviton production through qqbar and gg
                          "pysubs msub 391 1",        # qqbar
                          "pysubs msub 392 1",        # gg

                          #--- Decays
                          "pydat3 mdcy 347 1 1",
                          "pydat3 mdcy 15  1 0",       # Turn off tau decays (done by Tauola)

                          #--- Graviton mass
                          "pydat2 pmas 5000039 1 " + str(m_G),

                          #--- Open graviton decay channels to quarks
                          "pydat3 mdme 4084 1  1",     # d dbar              (~6% @ 1 TeV)
                          "pydat3 mdme 4085 1  1",     # u ubar              (~6% @ 1 TeV)
                          "pydat3 mdme 4086 1  1",     # s sbar              (~6% @ 1 TeV)
                          "pydat3 mdme 4087 1  1",     # c cbar              (~6% @ 1 TeV
                          "pydat3 mdme 4088 1  1",     # b bbar              (~6% @ 1 TeV)
                          "pydat3 mdme 4089 1  1",     # t tbar              (~6% @ 1 TeV)
                          "pydat3 mdme 4090 1 -1",     # b' b'bar
                          "pydat3 mdme 4091 1 -1",     # t' t'bar

                          #--- Open graviton decay channels to leptons
                          "pydat3 mdme 4092 1  1",     # e- e+               (~2% @ 1 TeV)
                          "pydat3 mdme 4093 1  1",     # nu_e nu_ebar        (~2% @ 1 TeV)
                          "pydat3 mdme 4094 1  1",     # mu- mu+             (~2% @ 1 TeV)
                          "pydat3 mdme 4095 1  1",     # nu_mu nu_mubar      (~2% @ 1 TeV)
                          "pydat3 mdme 4096 1  1",     # tau- tau+           (~2% @ 1 TeV)
                          "pydat3 mdme 4097 1  1",     # nu_tau nu_taubar    (~2% @ 1 TeV)
                          "pydat3 mdme 4098 1 -1",     # tau'- tau'+
                          "pydat3 mdme 4099 1 -1",     # nu'_tau nu'_taubar

                          #--- Open graviton decay channels to gauge bosons
                          "pydat3 mdme 4100 1  1",     # g g                 (~33% @ 1 TeV)
                          "pydat3 mdme 4101 1  1",     # gamma gamma         (~4%  @ 1 TeV)
                          "pydat3 mdme 4102 1  1",     # Z0 Z0               (~5%  @ 1 TeV)
                          "pydat3 mdme 4103 1  1",     # W+ W-               (~9%  @ 1 TeV)

                          #--- PARP(50) : (D=0.054) dimensionless coupling, which enters quadratically
                          #    in all partial widths of the excited graviton G* resonance,
                          #    is kappa_mG* = sqrt(2)*x1*k/MPl~, where x1~3.83 is the first zero
                          #    of the J1 Bessel function and MPl~ is the modified
                          #    Planck mass scale [Ran99, Bij01].
                          #    default corresponds to k/Mpl~=0.01 in hep-ph/0006114
                          "pypars parp 50 " + str(k_mG),
                          
                          #--- Cutoff for QED FSR in Pythia to 20000 GeV (='infinity', photos takes care of it)
                          "pydat1 parj 90 20000."
                          ]

#----- Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

#----- Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
