# Alpgen Z(->ee)+bb+0p with 4-lepton filter
#--------------------------------------------------------------
# File prepared by Soshi Tsuno (Soshi.Tsuno@cern.ch) Dec 2008
#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------
# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 

try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common.py" )
  if runArgs.ecmEnergy == 14000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_14TeV.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig


Herwig.HerwigCommand += [ "iproc alpgen",
                          "taudec TAUOLA"
                        ]
# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
topAlg += MultiElecMuTauFilter()

MultiElecMuTauFilter = topAlg.MultiElecMuTauFilter
MultiElecMuTauFilter.NLeptons  = 4
MultiElecMuTauFilter.MaxEta = 10.0
MultiElecMuTauFilter.MinPt = 5000.0
MultiElecMuTauFilter.MinVisPtHadTau = 10000.0 # pt-cut on the visible hadronic tau
MultiElecMuTauFilter.IncludeHadTaus = True # one can choose whether to include hadronic taus or not

try:
     StreamEVGEN.RequireAlgs = [ "MultiElecMuTauFilter" ]
except Exception, e:
     pass

from MC10JobOptions.AlpgenEvgenConfig import evgenConfig
 
# inputfilebase
#evgenConfig.inputfilebase = 'alpgen.109400.ZeebbNp0_pt20_4leptons'
evgenConfig.inputfilebase = 'group09.phys-gener.alpgen.109400.ZeebbNp0_pt20_4leptons_7Tev.TXT.v1'

# 7TeV
# Filter efficiency  = 0.00498
# MLM matching efficiency = 0.7320
# Alpgen cross section = 8.71142000+-0.00374676 pb
# Herwig cross section = Alpgen cross section * eff(MLM) = 6.3725 pb
# Integrated Luminosity = 15692.3042 pb-1
# Number of events in 4-vector file of Alpgen = 5000/(eff*0.8) = 1720000 events
#
# 10TeV
# Filter efficiency  = 0.0044
# MLM matching efficiency = 0.7073
# Alpgen cross section = 17.27900000+-0.00813682 pb
# Herwig cross section = Alpgen cross section * eff(MLM) = 12.22 pb
# Number of events in 4-vector file of Alpgen = 5000/(eff*0.8) = 2010000 events
#
# Filter efficiency x safety factor = eff x 80% = 0.00352
evgenConfig.efficiency = 0.00352
evgenConfig.minevents=5000
#==============================================================
#
# End of job options file
#
###############################################################
