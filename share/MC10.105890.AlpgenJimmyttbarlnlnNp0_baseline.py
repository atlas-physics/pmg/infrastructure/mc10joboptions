###############################################################
#
# Job options file
# usage : 
# Sarah Allwood-Spiers
#==============================================================

# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 3

#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------

### Herwig

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common.py" )
  if runArgs.ecmEnergy == 14000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_14TeV.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig


#from Herwig_i.Herwig_iConf import Herwig

# ... Tauola

Herwig.HerwigCommand+= [ "iproc alpgen" ]
Herwig.HerwigCommand+= [ "taudec TAUOLA"]

include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
# no filter
#---

from MC10JobOptions.AlpgenEvgenConfig import evgenConfig

# dummy needed
evgenConfig.inputfilebase = 'alpgen'
try:
  if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputfilebase = 'group09.phys-gener.alpgen.105890.ttbarlnlnNp0.TXT.v1'
  if runArgs.ecmEnergy == 10000.0:
    evgenConfig.inputfilebase = 'alpgen.105890.ttbarlnlnNp0'
except NameError:
  pass

evgenConfig.efficiency = 0.9

#7TeV EVENTS:
#No filter
#MLM matching efficiency = 36.3 %
#Alpgen events/ input file (to produce >=550 evts) = 3200
#Alpgen cross section = 9.12 pb
#Herwig cross section = Alpgen cross section *eff(MLM) = 3.314 pb
#Lumi/500 events = 500/XS(Herwig) =  151 pb
#Filter efficiency estimate reduced by 10% to produce 550 events on average, of which only 500 will be used in further processing


#10TeV EVENTS:
#No filter
#MLM matching efficiency = 31 %
#Alpgen events/ input file (to produce >=550 evts) = 3000
#Alpgen cross section = 23.8 pb
#Herwig cross section = Alpgen cross section *eff(MLM) = 7.44 pb
#Lumi/500 events = 500/XS(Herwig) =  67 pb
#Filter efficiency estimate reduced by 10% to produce 550 events on average, of which only 500 will be used in further processing

#==============================================================
#
# End of job options file
#
###############################################################
