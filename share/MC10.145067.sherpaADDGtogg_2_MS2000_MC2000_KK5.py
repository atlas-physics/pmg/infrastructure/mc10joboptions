from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence('TopAlg')
from Sherpa_i.Sherpa_iConf import Sherpa_i
sherpa = Sherpa_i()
topAlg += sherpa
from MC10JobOptions.SherpaFEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'group.phys-gener.sherpa.145067.ADDGtogg_2_MS2000_MC2000_KK5_7TeV.TXT.mc10_v1'
evgenConfig.efficiency = 0.9
evgenConfig.weighting = 0
