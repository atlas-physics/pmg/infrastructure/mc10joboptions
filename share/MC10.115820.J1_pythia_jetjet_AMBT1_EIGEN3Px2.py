###############################################################
#
# Job options file for dijet production with pythia
#
# author: C. Gwenlan (June'10)
# AMBT1 eigentune 3+ (2-sigma) for systematic studies
# reference JO: MC10.105010.J1_pythia_jetjet.py
#===============================================================

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

#--------------------------------------------------------------
# AMBT1 systematic variation: eigentune 3+ (2-sigma)
Pythia.PythiaCommand += [
			      "pypars parp 77 0.94191",
                              "pypars parp 78 0.45867",
                              "pypars parp 82 2.26510",
                              "pypars parp 84 0.63362",
                              "pypars parp 90 0.21056"]

#--------------------------------------------------------------
Pythia.PythiaCommand += [
			      "pysubs msel 0",
			      "pysubs ckin 3 17.",
			      "pysubs ckin 4 35.",
			      "pysubs msub 11 1",
			      "pysubs msub 12 1",
			      "pysubs msub 13 1",
			      "pysubs msub 68 1",
			      "pysubs msub 28 1",
			      "pysubs msub 53 1"]

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
