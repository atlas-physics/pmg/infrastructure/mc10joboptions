#----------------------------
# GGM bino - like neutralino
# 
# contact :  N. Panikashvili
#----------------------------

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

Pythia.SusyInputFile = "susy_bino_1000_700_slha.txt";

Pythia.PythiaCommand += ["pysubs msel 0"]  # !
Pythia.PythiaCommand += ["pymssm imss 1 11" ]
Pythia.PythiaCommand += ["pymssm imss 11 1" ]
Pythia.PythiaCommand += ["pymssm imss 21 50" ]
Pythia.PythiaCommand += ["pymssm imss 22 50" ]

Pythia.PythiaCommand += ["pysubs msub 243 1"]  # q qbar > go go
Pythia.PythiaCommand += ["pysubs msub 244 1"]  # g g    > go go
 
Pythia.PythiaCommand += ["pysubs msub 216 1"]
Pythia.PythiaCommand += ["pysubs msub 217 1"]
Pythia.PythiaCommand += ["pysubs msub 220 1"]
Pythia.PythiaCommand += ["pysubs msub 226 1"]
Pythia.PythiaCommand += ["pysubs msub 229 1"]
Pythia.PythiaCommand += ["pysubs msub 230 1"]

#######
 
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = INFO
 
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
# 2101 = EventInfo
# 133273 = MCTruth (HepMC)
# optionally
include("AthenaSealSvc/AthenaSealSvc_joboptions.py" )
AthenaSealSvc.CheckDictionary = True
 
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
 
from MC10JobOptions.SUSYEvgenConfig import evgenConfig

#---------------------------------------------------------------
#End of job options file
#
###############################################################

