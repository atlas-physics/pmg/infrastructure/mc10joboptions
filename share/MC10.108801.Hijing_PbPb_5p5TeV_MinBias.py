###############################################################
#
# Job options file for Hijing generation of
# Pb + Pb collisions at 3950 GeV/(colliding nucleon pair)
#
# Andrzej.Olszewski@ifj.edu.pl
# Brian Cole
#
# Dec 2008
#
#==============================================================

#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# configuring the Athena application for a 'generator' job
import AthenaCommon.AtlasUnixGeneratorJob

# make sure we are loading the ParticleProperty service
from AthenaCommon.AppMgr import ServiceMgr as svcMgr

from AthenaCommon.Configurable import Configurable
svcMgr.MessageSvc.OutputLevel = 3

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

from Hijing_i.Hijing_iConf import Hijing
topAlg += Hijing()

#--------------------------------------------------------------
# Algorithms Private Options
#--------------------------------------------------------------
if not hasattr(svcMgr, 'AtRndmGenSvc'):
    from AthenaServices.AthenaServicesConf import AtRndmGenSvc
    svcMgr += AtRndmGenSvc()

svcMgr.AtRndmGenSvc.Seeds = \
       ["HIJING 327213897 979111713", "HIJING_INIT 31452781 78713307"]

#----------------------
# Hijing Parameters
#----------------------
Hijing = topAlg.Hijing
Hijing.Initialize = ["efrm 3950.", "frame CMS", "proj A", "targ A",
                    "iap 208", "izp 82", "iat 208", "izt 82",
# simulation of minimum-bias events
                    "bmin 0", "bmax 20",
# turns OFF jet quenching:
                    "ihpr2 4 0",
# Jan24,06 turns ON decays charm and  bottom but not pi0, lambda, ... 
                    "ihpr2 12 2",
# turns ON retaining of particle history - truth information:
                    "ihpr2 21 1"]

#---------------------------------------------------------------
# Pool Persistency
#---------------------------------------------------------------
from AthenaPoolCnvSvc.WriteAthenaPool import AthenaPoolOutputStream

#--- StreamEVGEN ---
StreamEVGEN = AthenaPoolOutputStream( "StreamEVGEN" )

# 2101 == EventInfo
# 133273 == MCTruth (HepMC)
# 54790518 == HijigEventParams
try:
    StreamEVGEN.ItemList  = [ "2101#*","133273#GEN_EVENT" ]
    StreamEVGEN.ItemList += [ "54790518#*" ]
except Exception, e:
    pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.HijingEvgenConfig import evgenConfig
