###############################################################
#
# Job options file
# Developed by Pavel Staroba
# in Rel. 15.6.1.7 (February 2010)
# from MC10.106081.McAtNloWplusenu_no_filter.py
#
#===============================================================

# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common_7TeV.py" )
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common.py" )
  if runArgs.ecmEnergy == 14000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common_14TeV.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig

Herwig.HerwigCommand += [ "taudec TAUOLA"]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
# efficiency = 0.95 - no filtering:
# Non-filtered cross NLO section in Rel. 15.6.1.7 : 5.87928 nb
from MC10JobOptions.McAtNloEvgenConfig import evgenConfig

#dummy needed
evgenConfig.inputfilebase = 'mcatnlo341'
try:
  if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputfilebase = 'group09.phys-gener.mcatnlo341.108328.Wplustaunu_no_filter_7TeV.TXT.v1'
    print "EEEE 7 TeV"
  if runArgs.ecmEnergy == 8000.0:
    evgenConfig.inputfilebase = 'group09.phys-gener.mcatnlo341.108328.Wplustaunu_no_filter_8TeV.TXT.v1'
    print "EEEE 8 TeV"
except NameError:
  pass

evgenConfig.efficiency = 0.95
#==============================================================
#
# End of job options file
#
###############################################################
