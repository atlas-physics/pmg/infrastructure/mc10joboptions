#-------------------------------------------------------------
# Pythia photon+jet w/ p_T(photon) > 35 GeV
# Based on JO prepared by J. Tojo, December 2009
#-------------------------------------------------------------

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_PythiaAUET24Minus_Common.py" )
Pythia.PythiaCommand += [ "pysubs msel 0",
                          "pysubs ckin 3 33.",
                          "pysubs msub 14 1",
                          "pysubs msub 29 1" ]

#-------------------------------------------------------------
# Filter
#-------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import PhotonFilter
topAlg += PhotonFilter()

PhotonFilter = topAlg.PhotonFilter
PhotonFilter.Ptcut = 35000.
PhotonFilter.Etacut = 2.7
PhotonFilter.NPhotons = 1

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------

try:
     StreamEVGEN.RequireAlgs +=  [ "PhotonFilter" ]
except Exception, e:
     pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
# efficiency = 5000/8271*0.9 = 0.544 at 7 TeV
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.544
