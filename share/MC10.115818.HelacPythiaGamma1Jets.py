## gamma + multi-Jets generation, with HELAC-PHEGAS / Pythia 

## get a handle on topalg
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

# Number of events to be processed (default is 10)
theApp.EvtMax = -1

from Pythia_i.Pythia_iConf import Pythia
topAlg += Pythia()

## use Pythia MC09' tuning for CTEQ6L1 PDFset 
include ( "MC10JobOptions/MC10_PythiaMC09p_Common.py" )

## Call HELAC-PHEGAS from Pythia
Pythia.PythiaCommand += ["pyinit user HELAC"]

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
#from MC10JobOptions.HelacEvgenConfig import evgenConfig
#evgenConfig.efficiency = 0.9

from MC10JobOptions.HelacEvgenConfig import evgenConfig
#evgenConfig.efficiency = 0.9
evgenConfig.efficiency = 0.90
#evgenConfig.minevents=100
evgenConfig.inputfilebase = 'group09.phys-gener.helac.115001.Gamma1jets.TXT.v1'

