#Technicolor Strawman dimuon resonances
#prepared by Kevin Black, John Butler, Jeremy Love
#Technirho and TechniOmega to dimuons, choose parameters so that the 
#two techniPi decay mode is kinematically inaccessible, increasing the dilepton
#branching ratio
#Use typical parameters from previous studies
#M(pi_tc0) = M(pi_tc+) = M(pi_tc-)
#M(pi_tc) = M(rho_tc) -100 GeV
#Mass Scale Paramter (M_V = 200*(M(Rho_tc)/210)
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

######################################################
#                                                    #
#           Algorithm Private Options                #
#                                                    #
######################################################

#TechniRho Mass (in GeV)
M_rhoTC = 800

#TechniPi Mass (in GeV)
M_piTC  = M_rhoTC - 100

#Vector Mass
M_V = 200.0*(M_rhoTC/210.0)

Upper_mass_range = 1.008*M_rhoTC + 10
Lower_mass_range = 1.008*M_rhoTC - 10

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

# generic pythia setup
Pythia.PythiaCommand +=  ["pydat1 parj 90 200000",
                          "pydat3 mdcy 15 1 0"] 

Pythia.PythiaCommand +=  ["pysubs msel 0",
                          "pysubs msub 194 1"]

# change ee to mumu
Pythia.PythiaCommand += ["pyint2 kfpr 194 1 13"]
# M(pi_tc0)
Pythia.PythiaCommand += ["pydat2 pmas 3000111 1 "+str(M_piTC)]
# M(pi_tc+)
Pythia.PythiaCommand += ["pydat2 pmas 3000211 1 "+str(M_piTC)]
# M(pi'_tc+)
Pythia.PythiaCommand += ["pydat2 pmas 3000221 1 "+str(M_piTC)]
# M(rho_tc0)
Pythia.PythiaCommand += ["pydat2 pmas 3000113 1 "+str(M_rhoTC)]
# M(rho_tc+)
Pythia.PythiaCommand += ["pydat2 pmas 3000213 1 "+str(M_rhoTC)]
# M(omega_tc0)
Pythia.PythiaCommand += ["pydat2 pmas 3000223 1 "+str(M_rhoTC)]
# M_V
Pythia.PythiaCommand += ["pytcsm rtcm 12 "+str(M_V),
                         "pytcsm rtcm 13 "+str(M_V)]
# hat(m) range
Pythia.PythiaCommand += ["pysubs ckin 1 "+str(Lower_mass_range),
                         "pysubs ckin 2 "+str(Upper_mass_range)]

include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.EvgenConfig import evgenConfig
evgenConfig.generators += [ "Pythia" ]
evgenConfig.auxfiles += [ "PDGTABLE.MeV", "pdt.table", "DECAY.DEC" ]
evgenConfig.efficiency = 0.9
