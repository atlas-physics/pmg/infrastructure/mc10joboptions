###############################################################
# $Id: $
#
# Authors:
#  Tobias Golling, Yale University
#  Rocco Mandrysch, Humboldt University of Berlin
#  Michael G Wilson, SLAC National Accelerator Laboratory
#  Modified for u4u4 Note by Gokhan Unel / UCI
#==============================================================

# ... Main generator : Pythia
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include( "MC10JobOptions/MC10_Pythia_FourthGeneration.py" )

Pythia.PythiaCommand += [
    "pysubs msel 8",          # u4/t' production channel
    "pydat2 pmas 7 1 200", # d4/b' mass
    "pydat2 pmas 8 1 200", # u4/t' mass

    # t' decay channel 
    "pydat3 mdme 66 1  0", # t' --> g t'
    "pydat3 mdme 67 1  0", # t' --> gamma t'
    "pydat3 mdme 68 1  0", # t' --> Z0 t'
    "pydat3 mdme 69 1  1", # t' --> W+ d
    "pydat3 mdme 70 1  0", # t' --> W+ s
    "pydat3 mdme 71 1  0", # t' --> W+ b
    "pydat3 mdme 72 1  0", # t' --> W+ b'
    "pydat3 mdme 73 1  0" # t' --> h0 t'
    ]

Pythia.PythiaCommand += ["pydat1 parj 90 20000",   # Turn off FSR (for Photos)
                         "pydat3 mdcy 15 1 0"   ]  # Turn off tau decays (for Tauola)

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


#--------------------------------------------------------------
# Filter: Require at least one lepton
#--------------------------------------------------------------

# MultiLeptonFilter
from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
topAlg += MultiLeptonFilter()

MultiLeptonFilter = topAlg.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 10.*GeV
MultiLeptonFilter.Etacut = 2.7
MultiLeptonFilter.NLeptons = 1

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------
try:
    StreamEVGEN.RequireAlgs += [ "MultiLeptonFilter" ]
except Exception, e:
    pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.40
