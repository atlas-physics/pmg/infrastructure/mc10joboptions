###############################################################
# Job options file
# Elisa Piccaro
# Low mass Drell Yan -  mumu - 8<M<15 - 7TeV - 1Filter 
#==============================================================

# ... Main generator : Pythia
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_Pythia_Common.py" )


Pythia.PythiaCommand +=[ "pysubs msel 0",          # Users decay choice.
                         "pydat1 parj 90 20000",   # Turn off FSR.
                         "pydat3 mdcy 15 1 0",     # Turn off tau decays.
                         #
                         # Z production:
                         "pysubs msub 1 1",        # Create Z bosons.
                         "pysubs ckin 1 8.0",      # Lower invariant mass.
                         "pysubs ckin 2 15.0",     # Higher invariant mass.
                         "pydat3 mdme 174 1 0",
                         "pydat3 mdme 175 1 0",
                         "pydat3 mdme 176 1 0",
                         "pydat3 mdme 177 1 0",
                         "pydat3 mdme 178 1 0",
                         "pydat3 mdme 179 1 0",
                         "pydat3 mdme 182 1 0",    # Switch for Z->ee.
                         "pydat3 mdme 183 1 0",
                         "pydat3 mdme 184 1 1",    # Switch for Z->mumu.
                         "pydat3 mdme 185 1 0",
                         "pydat3 mdme 186 1 0",    # Switch for Z->tautau.
                         "pydat3 mdme 187 1 0"
                         #cteq66 PDF sets
#                         "pypars mstp 51 10550", # proton PDF 
#                         "pypars mstp 53 10550", # pion pdf
#                         "pypars mstp 55 10550"  # photon pdf
                         ]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
topAlg += MultiLeptonFilter()

MultiLeptonFilter = topAlg.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 3000.
MultiLeptonFilter.Etacut = 2.7
MultiLeptonFilter.NLeptons = 1

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------

try:
    StreamEVGEN.RequireAlgs += [ "MultiLeptonFilter" ]
except Exception, e:
    pass


#--------------------------------------------------------------
#Configuration for EvgenJobTransforms
#--------------------------------------------------------------
# Cross section and efficiency in Rel. 15.5.1:
# cross section = 4.433 nb
# efficiency = 5024/10000*0.9 = 0.45216
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.45216

 
#==============================================================
#
# End of job options file
#
###############################################################
