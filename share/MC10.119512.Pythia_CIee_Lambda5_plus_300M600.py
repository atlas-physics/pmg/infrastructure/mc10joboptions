###############################################################
# Generator fragment
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

# Mass Scale parameter (Lambda, in GeV)
M_Lamb = 5000.0   
# sign of interference
sign = 1


Pythia.PythiaCommand +=[ "pysubs msel 0",        # Users decay choice.
                         "pydat1 parj 90 20000", # Turn off FSR.
                                                 # (leptons do not radiate)
                         
                         "pydat3 mdcy 15 1 0",   # Turn off tau decays.
                         
                         # Contact interaction
                         "pysubs msub 20 0",     # via gamma W+
                         "pysubs msub 165 1",    # via gamma* Z0 
                         "pysubs msub 166 0",    # via W+-
                         "pysubs msub 381 0",
                         "pysubs msub 382 0",

                         # turn on anomalous coupling
                         "pytcsm itcm 5 2",
                         "pytcsm rtcm 41 "+str(M_Lamb),
                         "pytcsm rtcm 42 "+str(sign),
                         
                         "pysubs ckin 1 300.0",   # Lower invariant mass.
                         "pysubs ckin 2 600.0",   # Higher invariant mass.
                         "pyint2 kfpr 165 1 11",  #choose decay into electrons
                         # "pyint2 kfpr 166 1 13",
                         "pypars mstp 32 4"]
## ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

## ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9

#==============================================================
#
# End of job options file
#
###############################################################


# From the Pythia manual:

# Some processes are implemented to allow the
# introduction of anomalous coupling, in addition
# to the Standard Model ones. These can be switched
# on by ITCM(5) >= 1; the default ITCM(5) = 0
# corresponds to the Standard Model behaviour.

# In processes 381 and 382, the quark substructure
# is included in the left-left isoscalar model [Eic84, Chi90]
# for ITCM(5) = 1, with compositeness scale lambda given
# in RTCM(41) (default 1000 GeV) and sign eta of interference
# term in RTCM(42) (default +1; only other alternative -1).
# The above model assumes that only u and d quarks are composite (at
# least at the scale studied); with ITCM(5) = 2 compositeness
# terms are included in the interactions between all quarks.
# When ITCM(5) = 0, the two processes are equivalent with 11 and 12.
# A consistent set of high-pT jet production processes in
# compositeness scenarios is thus obtained by combining 381 and
# 382 with 13, 28, 53 and 68.

## The processes 165 and 166 are basically equivalent to 1 and 2,
## i.e. gamma*/Z0 and W+- exchange, respectively, but with less
## detail (no mass-dependent width, etc.). The reason for this
## duplication is that the resonance treatment formalism of
## processes 1 and 2 could not easily be extended to include
## other than s-channel graphs. In processes 165 and 166, only
## one final-state flavour is generated at the time; this flavour
## should be set in KFPR(165,1) and KFPR(166,1), respectively. For
## process 166 one gives the down-type flavour, and the program will
## associate the up-type flavour of the same generation. Defaults
## are 11 in both cases, i.e. e+ e- and e+ nu e (w- nu e ) final
## states. While ITCM(5) = 0 gives the Standard Model results,
## ITCM(5) = 1 contains the left-left isoscalar model (which does not
## affect process 166), and ITCM(5) = 3 the helicity-non-conserving
## model (which affects both) [Eic84, Lan91]. Both models above
## assume that only u and d quarks are composite; with
## ITCM(5) = 2 or 4, respectively, contact terms are included
## for all quarks in the initial state. The relevant parameters
## are RTCM(41) and RTCM(42), as above.

## Note that processes 165 and 166 are book-kept as 2 to 2
## processes, while 1 and 2 are 2 to 1 ones. This means that
## the default Q^2 scale in parton distributions is pT^2 for
## the former and s-hat for the latter. To make contact between
## the two, it is recommended to set MSTP(32) = 4, so as to
## use s-hat as scale also for processes 165 and 166.

## In process 20, for W-gamma pair production, it
## is possible to set an anomalous magnetic moment for the W in
# RTCM(46) (= eta = K - 1; where K = 1 is the Standard Model value).
# The production process is affected according to the formulae
# of [Sam91], while W decay currently remains unaffected. It is
# necessary to set ITCM(5) = 1 to enable this extension.
