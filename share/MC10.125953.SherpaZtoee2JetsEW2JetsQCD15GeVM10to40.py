from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence('TopAlg')
from Sherpa_i.Sherpa_iConf import Sherpa_i
sherpa = Sherpa_i()

"""
(run){
  WRITE_MAPPING_FILE=3
  ME_SIGNAL_GENERATOR=Comix
  ACTIVE[25]=0
  EXCLUSIVE_CLUSTER_MODE=1
}(run)

(processes){
  Process 93 93 -> 11 -11 93 93 93{2}
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Max_Epsilon 1e-3
  End process;
}(processes)

(selector){
  Mass 11 -11 10.0 40.0
  NJetFinder 2 15.0 0.0 0.4
}(selector)
"""

try:
    sherpa.Parameters += [ 'RUNDATA=%s' % runArgs.jobConfig[0] ]
except NameError:
    pass

topAlg += sherpa
from MC10JobOptions.SherpaFEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'group10.phys-gener.sherpa010203.125953.SherpaZtoee2JetsEW2JetsQCD15GeVM10to40_7TeV.TXT.v1'
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 100
evgenConfig.weighting = 0
