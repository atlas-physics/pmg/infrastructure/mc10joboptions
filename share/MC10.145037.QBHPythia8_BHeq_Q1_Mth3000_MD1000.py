#################################
# JobOption for quantum black hole production.
# July 2nd, 2011
# Andreas Reinsch <areinsch@uoregon.edu>
#

#---------------------------------------------------
# Pythia initialisation for Lhef_i
#---------------------------------------------------

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_Pythia8_Common.py" )


Pythia8 = Algorithm( "Pythia8_i" )
#Pythia8.PythiaCommand =    [
#
#   "pyinit user lhef",           # set the interface
#   "pysubs msel 0",      # set the user process
#   "pyinit pylisti -1",   # set the event listing detail at initialisation (-1->turn off)
#   "pyinit pylistf 2",      # set the event listing detail in the event dump (2->more detail)
#   "pyinit dumpr 1 2"      # set the range of events to dump
#
#   ]

Pythia8.LHEFile = 'unweighted_events.lhe'
#Pythia8.UserProcess = ""  # Does not exist in 15.6.12.6
#Pythia8.CollisionEnergy = 7000
#Pythia8.Commands += [""]
#Pythia8.Commands += [""]
#Pythia8.Commands += [""]
#Pythia8.Commands += [""]


from TruthExamples.TruthExamplesConf import TestHepMC
TestHepMC.EnergyDifference = 100000000.      # set the energy balance threshold to an arbitrary high number


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.Pythia8EvgenConfig import evgenConfig
evgenConfig.efficiency = 1.0
evgenConfig.generators = ["Lhef"]
evgenConfig.inputfilebase = "group.phys-gener.qbh.145037.BHeq_Q1_Mth3000_MD1000.TXT.mc10_v1"

