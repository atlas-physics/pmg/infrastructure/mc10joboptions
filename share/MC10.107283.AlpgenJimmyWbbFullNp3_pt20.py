###############################################################
#
# Job options file
# Daniel Geerts
#
#==============================================================
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 3

#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------

# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 
try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common.py" )
  if runArgs.ecmEnergy == 14000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_14TeV.py" )
except NameError:
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig 


Herwig.HerwigCommand += [ "iproc alpgen",
                          "taudec TAUOLA",
                        ]
# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from MC10JobOptions.AlpgenEvgenConfig import evgenConfig
 

# dummy needed
evgenConfig.inputfilebase = 'alpgen'
try:
  if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.alpgen.107283.WbbFullNp3_pt20_7tev.TXT.v2'
  if runArgs.ecmEnergy == 8000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.alpgen.107283.WbbFullNp3_pt20_8tev.TXT.v1'
except NameError:
  pass
 
# 7 TeV - Information on sample 107283
# 7 TeV - Filter efficiency  = 1.0000
# 7 TeV - MLM matching efficiency = 0.28
# 7 TeV - Number of Matrix Elements in input file  = 2400
# 7 TeV - Alpgen cross section = 22.6 pb
# 7 TeV - Herwig cross section = Alpgen cross section * eff(MLM) = 6.3 pb
# 7 TeV - Cross section after filtering = 6.3 pb
# 7 TeV - Lumi/500 events post-filter = 500/eff(Filter)/XS(jimmy) = 79.42 pb-1
#
# 7 TeV - Filter efficiency estimate below reduced by 10% to produce 555 events on average,
# 7 TeV - of which only 500 will be used in further processing
#EXT:
# 7 TeV - Information on sample 	107283	
# 7 TeV - Filter efficiency  = 	1.0000	
# 7 TeV - MLM matching efficiency = 	0.28	
# 7 TeV - Number of Matrix Elements in input file  = 	23000	
# 7 TeV - Alpgen cross section = 	25.8	 pb
# 7 TeV - Herwig cross section = Alpgen cross section * eff(MLM) = 	7.3	 pb
# 7 TeV - Cross section after filtering = 	7.3	 pb
# 7 TeV - Lumi/5000 events post-filter = 5000/eff(Filter)/XS(jimmy) = 	685.45	 pb-1
#		
# 7 TeV - Filter efficiency estimate below reduced by 10% to produce 5556 events on average,		
# 7 TeV - of which only 5000 will be used in further processing		
# 8 TeV - Information on sample 	107283
# 8 TeV - Filter efficiency  = 	1.0000
# 8 TeV - MLM matching efficiency = 	0.27
# 8 TeV - Number of Matrix Elements in input file  = 	2400
# 8 TeV - Alpgen cross section = 	31.2	 pb
# 8 TeV - Herwig cross section = Alpgen cross section * eff(MLM) = 	8.6	 pb
# 8 TeV - Cross section after filtering = 	8.6	 pb
# 8 TeV - Lumi/500 events post-filter = 500/eff(Filter)/XS(jimmy) = 	58.43	 pb-1
#
# 8 TeV - Filter efficiency estimate below reduced by 10% to produce 5556 events on average,
# 8 TeV - of which only 5000 will be used in further processing	
evgenConfig.efficiency = 0.90000
evgenConfig.minevents = 5000
#==============================================================
#
# End of job options file
#
###############################################################
