###############################################################
#
# Job options file
#
#==============================================================
##
## Job config for HERWIG+JIMMY in "min bias" mode. The JIMMY multiple scattering
## model does not extend down to arbitrary scales, so this JO fragment should be
## used with care, ensuring in analysis (via cuts or otherwise) that effects
## from below the MPI cutoff scale do not strongly influence physics studies.
##
## This process has been specifically requested for MPI model comparison studies
## in underlying event analyses at sqrt(s) > 900 GeV.
##
## author: Andy Buckley, April '10

# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg")  

try:
  if runArgs.ecmEnergy == 900.0:
    include ( "MC10JobOptions/MC10_Herwig_Common_900GeV.py" ) 
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_Herwig_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_Herwig_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_Herwig_Common.py" )
  if runArgs.ecmEnergy == 14000.0:
    include ( "MC10JobOptions/MC10_Herwig_Common_14TeV.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig

#
Herwig.HerwigCommand += [ "iproc 11500"]

# set JMUEO=0 and PTMIN=PTJIM for lowest pT Jimmy can do
# (PTJIM values are from AUET1 tune)
Herwig.HerwigCommand += [ "jmueo 0"]

try:
  if runArgs.ecmEnergy == 900.0:
    Herwig.HerwigCommand += [ "ptmin 2.36"]
  if runArgs.ecmEnergy == 7000.0:
    Herwig.HerwigCommand += [ "ptmin 4.14"]
  if runArgs.ecmEnergy == 10000.0:
    Herwig.HerwigCommand += [ "ptmin 4.56"]
  if runArgs.ecmEnergy == 14000.0:
    Herwig.HerwigCommand += [ "ptmin 5.00"]
except NameError:
  pass


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.HerwigEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9

#==============================================================
#
# End of job options file
#
###############################################################
