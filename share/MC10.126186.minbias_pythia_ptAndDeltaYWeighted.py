###############################################################
#
# Job options file
#
# Pythia Minbias event generation with pT-deltaY weighting.
# cross section as default pythia sample = 4.8445e+07 nb
#
# Prepared by Graham Jones, Graham.Jones [at] cern.ch 
#==============================================================

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 


include ( "MC10JobOptions/MC10_Pythia_Common.py" )


Pythia.PythiaCommand += [  "pysubs msel 1" ]

#--------------------------------------------------------------
# Truth Jets
#--------------------------------------------------------------

# Make truth jets for JetForwardFilter:
from AthenaCommon.AppMgr import ServiceMgr as svcMgr
include( "AthenaCommon/Atlas_Gen.UnixStandardJob.py" )
from JetRec.JetGetters import *
Kt6Alg = make_StandardJetGetter('AntiKt',0.6,'Truth').jetAlgorithmHandle()
Kt6Alg.AlgTools['JetFinalPtCut'].MinimumSignal = 7.0*GeV

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

## Truth filter
# Add the filter to the correct sequence!
from GeneratorFilters.GeneratorFiltersConf import JetForwardFilter
forwardFilter=JetForwardFilter()
topAlg += forwardFilter

# General properties
forwardFilter.JetContainer = "AntiKt6TruthJets"

# Kinematic cuts
forwardFilter.MinDeltaY = 0.0
forwardFilter.MaxDeltaY = 100.0
forwardFilter.MinPt1 = 7.0 #GeV
forwardFilter.MinPt2 = 7.0
forwardFilter.MaxPt1 = 70000.0
forwardFilter.MaxPt1 = 70000.0

#Gaussian mixture model describing pdf of unweighted sample
forwardFilter.muXs = [ 42.7804364667, 10.0344611279, 12.0034310085, 24.3514724287, 16.5209591004,  ]
forwardFilter.muYs = [ 1.55643018905, 2.93597136968, 1.49198019253, 1.65075071435, 2.31840566883,  ]
forwardFilter.sigmaXs = [ 17.993075552, 1.64640470995, 2.50151623428, 6.74799018614, 3.7129743994,  ]
forwardFilter.sigmaYs = [ 1.13346128772, 1.81994749811, 0.977007802684, 1.17274475424, 1.58984264608,  ]
forwardFilter.rhos = [ 0.0655896477441, 0.129461741532, 0.111939302463, 0.18579371176, 0.033098733478,  ]
forwardFilter.weights = [ 0.00711704732108, 0.372873347478, 0.337260887694, 0.0639170224219, 0.218831695085,  ]


#Target probability (how much weighting required roughly)
#Lower = more suppression
import math
forwardFilter.SuppressionFactor = 7.0*math.pow(10.0, -7.0)

#--------------------------------------------------------------
# Apply filtering
#--------------------------------------------------------------

try:
  StreamEVGEN.RequireAlgs += [ "JetForwardFilter" ]
except Exception, e:
     pass


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
# N_gen = 2498914018; N_pass = 400000-> eff = 1.6*10^-4
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.0001601
evgenConfig.minevents=1000
evgenConfig.weighting=0
