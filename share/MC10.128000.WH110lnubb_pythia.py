#Modified by Lashkar Kashif to include lepton filter, 03/02/2011
#============================================================== 
from AthenaCommon.AlgSequence import AlgSequence  
topAlg = AlgSequence("TopAlg")  
include ( "MC10JobOptions/MC10_Pythia_Common.py" )   
#-------------------------------------------------------------- 
# WH, W --> lnu, H --> bb 
Pythia.PythiaCommand +=[ "pysubs msel 0", 
                         "pydat2 pmas 25 1 110.", 
                         "pydat1 parj 90 20000", # Turn off FSR. 
                         "pydat3 mdcy 15 1 0",   # Turn off tau decays. 
                         "pysubs msub 26 1",     # WH production. 
                         "pydat3 mdme 174 1 0",  # Switch off all W decays 
                         "pydat3 mdme 190 1 0",  #apart from enu, munu 
                         "pydat3 mdme 191 1 0", 
                         "pydat3 mdme 192 1 0", 
                         "pydat3 mdme 194 1 0", 
                         "pydat3 mdme 195 1 0", 
                         "pydat3 mdme 196 1 0", 
                         "pydat3 mdme 198 1 0", 
                         "pydat3 mdme 199 1 0", 
                         "pydat3 mdme 200 1 0", 
                         "pydat3 mdme 206 1 1", 
                         "pydat3 mdme 207 1 1", 
                         "pydat3 mdme 208 1 1", 
                         "pydat3 mdme 210 1 0",  # Switch off all Higgs 
                         "pydat3 mdme 211 1 0",  # decays except to bb. 
                         "pydat3 mdme 212 1 0", 
                         "pydat3 mdme 213 1 0", 
                         "pydat3 mdme 214 1 1",  #bb 
                         "pydat3 mdme 215 1 0", 
                         "pydat3 mdme 216 1 0", 
                         "pydat3 mdme 217 1 0", 
                         "pydat3 mdme 218 1 0", 
                         "pydat3 mdme 219 1 0", 
                         "pydat3 mdme 220 1 0", #tau tau 
                         "pydat3 mdme 221 1 0", 
                         "pydat3 mdme 222 1 0", 
                         "pydat3 mdme 223 1 0", 
                         "pydat3 mdme 224 1 0", 
                         "pydat3 mdme 225 1 0", 
                         "pydat3 mdme 226 1 0"] 
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )  
include ( "MC10JobOptions/MC10_Photos_Fragment.py" ) 

from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
topAlg += LeptonFilter()
	
LeptonFilter = topAlg.LeptonFilter
LeptonFilter.Ptcut = 15.*GeV
LeptonFilter.Etacut = 3.0
	
try:
     StreamEVGEN.RequireAlgs += [ "LeptonFilter" ]
except Exception, e:
     pass
#-------------------------------------------------------------- 
# Configuration for EvgenJobTransforms 
#-------------------------------------------------------------- 
from MC10JobOptions.PythiaEvgenConfig import evgenConfig 
evgenConfig.efficiency = 0.68 
#evgenConfig.minevents = 1000
