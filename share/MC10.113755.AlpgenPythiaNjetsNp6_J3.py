###############################################################
#
# Job options file
# usage :
# David Lopez Mateos
#==============================================================

# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 3

#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------

### Pythia

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")
 
 
include ( "MC10JobOptions/MC10_PythiaMC09p_Common.py" )

# ... Tauola

Pythia.PythiaCommand+= [ "pyinit user alpgen",
                         "pydat1 parj 90 20000.", # Turn off FSR.
                         "pydat3 mdcy 15 1 0", # Turn off tau decays.
                         "pypars mstp 143 1", # matching
                         "pypars mstp 86 1" 
                         ]

include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
# no filter
#---

from MC10JobOptions.AlpgenPythiaEvgenConfig import evgenConfig
 
# input file names need updating for MC9
evgenConfig.inputfilebase = 'group09.phys-gener.alpgen213.113155.NjetsNp6_J3.TXT.v1'
evgenConfig.efficiency = 1.0
evgenConfig.minevents = 250
