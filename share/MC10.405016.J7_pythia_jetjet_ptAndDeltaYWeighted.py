###############################################################
#
# Job options file
#
# Pythia J7 event generation with eta interval weighting.
# cross section as default pythia sample = 1.3744*10.0^-4 nb
#
# Prepared by Graham Jones, Graham.Jones [at] cern.ch 
#==============================================================


# dijet production with pythia
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

Pythia.PythiaCommand += [
			      "pysubs msel 0",
			      "pysubs ckin 3 1120.",
			      "pysubs ckin 4 2240.",
			      "pysubs msub 11 1",
			      "pysubs msub 12 1",
			      "pysubs msub 13 1",
			      "pysubs msub 68 1",
			      "pysubs msub 28 1",
			      "pysubs msub 53 1"]

#--------------------------------------------------------------
# Truth Jets
#--------------------------------------------------------------

# Make truth jets for JetGapFilter:
from AthenaCommon.AppMgr import ServiceMgr as svcMgr
include( "AthenaCommon/Atlas_Gen.UnixStandardJob.py" )
from JetRec.JetGetters import *
Kt6Alg = make_StandardJetGetter('AntiKt',0.6,'Truth').jetAlgorithmHandle()
Kt6Alg.AlgTools['JetFinalPtCut'].MinimumSignal = 7.0*GeV

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

## Truth filter
# Add the filter to the correct sequence!
from GeneratorFilters.GeneratorFiltersConf import JetForwardFilter
forwardFilter=JetForwardFilter()
topAlg += forwardFilter

# General properties
forwardFilter.JetContainer = "AntiKt6TruthJets"

# Kinematic cuts
forwardFilter.MinDeltaY = 0.0
forwardFilter.MaxDeltaY = 100.0

forwardFilter.MinPt1 = 12.0 #GeV
forwardFilter.MinPt2 = 12.0
forwardFilter.MaxPt1 = 70000.0
forwardFilter.MaxPt1 = 70000.0

#Gaussian mixture model describing pdf of unweighted sample
forwardFilter.muXs = [ 1235.65611335, 1337.20359519, 1496.686216, 1173.96772319, 1102.67803426  ]
forwardFilter.muYs = [ 0.688851472157, 0.602340924317, 0.410901213092, 0.818145557919, 0.722348192358  ]
forwardFilter.sigmaXs = [ 77.8586420155, 115.548506361, 192.124549153, 40.6985973916, 219.676876912  ]
forwardFilter.sigmaYs = [ 0.468090403256, 0.416996813533, 0.291194301127, 0.538943544657, 0.514711333694  ]
forwardFilter.rhos = [ 0.171337455587, 0.19911223652, 0.143508681313, -0.0163563335871, -0.110200271979  ]
forwardFilter.weights = [ 0.298532639582, 0.217525883278, 0.0672763705558, 0.375236961566, 0.0414281450188  ]


#Target probability (how much weighting required roughly)
#Lower = more suppression
import math
forwardFilter.SuppressionFactor = 5.0*math.pow(10.0, -8.0)

#--------------------------------------------------------------
# Apply filtering
#--------------------------------------------------------------

try:
  StreamEVGEN.RequireAlgs += [ "JetForwardFilter" ]
except Exception, e:
     pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
# N_gen =192216792; N_pass =100000 -> eff = 5.2024*10^-4
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.00052024
evgenConfig.minevents = 500
evgenConfig.weighting = 0

