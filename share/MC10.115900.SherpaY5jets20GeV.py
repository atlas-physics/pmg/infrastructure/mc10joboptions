# Gamma+5Jets
#
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence('TopAlg')
from Sherpa_i.Sherpa_iConf import Sherpa_i
sherpa = Sherpa_i()

# set alpha_s scale factor in parton shower 
params="""
CSS_AS_FS_FAC=0.4
CSS_AS_IS_FAC=0.4
PDF_SET=cteq6.6m
K_PERP_MEAN_1=1.258
K_PERP_MEAN_2=1.258
K_PERP_SIGMA_1=1.078
K_PERP_SIGMA_2=1.078
SIGMA_ND_FACTOR=0.332
SCALE_MIN=2.63
RESCALE_EXPONENT=0.192
PROFILE_PARAMETERS=0.8 0.4
MASSIVE[15]=1
"""
sherpa.Parameters = params.strip().splitlines()

# set beam energy (interface currently sets it for ECM=7TeV; needs explicit changing in JO for other ECMs]
try:
  eBeam=runArgs.ecmEnergy/2.0
  sherpa.Parameters += [ "BEAM_ENERGY_1=%s" % eBeam,"BEAM_ENERGY_2=%s" % eBeam ]
except NameError:
  pass

#
topAlg += sherpa
from MC10JobOptions.SherpaFEvgenConfig import evgenConfig

# dummy needed
evgenConfig.inputfilebase = 'sherpa'
try:
  if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.sherpa010203.115900.SherpaY5jets20GeV_7TeV.TXT.v1'
  if runArgs.ecmEnergy == 8000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.sherpa010203.115900.SherpaY5jets20GeV_8TeV.TXT.v1'
except NameError:
  pass

evgenConfig.efficiency = 0.9
# low unweighting efficiency (set minevents=1000)
evgenConfig.minevents=1000
