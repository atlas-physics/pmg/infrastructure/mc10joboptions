###############################################################
#
# Job options file: Pomwig DPE
#
# Process description: Dijet production in double pomeron
#                      exchange, jet pt in bin 17-35 GeV
#
# Author: Vojtech Juranek (vojtech.juranek@cern.ch)
#
# Tested on Rel.: 15.0.0
#
#==============================================================


from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg")

from Pomwig_i.Pomwig_iConf import Pomwig
topAlg +=Pomwig()
topAlg.Pomwig.HerwigCommand = ["iproc 11500",       # dijet production (QCD 2->2)
                            "modpdf -1",            # PDFLIB parton set
                            "msflag 0",             # turn off UE    
                            "beam1type E-      ",   # diffractive proton
                            "beam2type E-      ",   # diffractive proton
                            "beam1energy 5000.0",   
                            "beam2energy 5000.0",
                            "ptmin 17.0",           # minimum p_T
                            "ptmax 35.0"            # maximum p_T
                            ]

topAlg.Pomwig.nstru=14   # Pomeron structure functions H1 2006 
topAlg.Pomwig.ifit=2     # Fit B Pomeron

# Cross section (NB): 1540.24 
# Rel.: 15.0.0.3, 5552 events
from MC10JobOptions.PomwigEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
