################################################################
#
# MC@NLO/JIMMY/HERWIG a(mass=10GeV, width=0.9MeV)->tautau (light Higgs)
# filter for ll final state only
#
# Responsible person(s)
#   Oct 21, 2010 : Junichi TANAKA (Junichi.Tanaka@cern.ch)
#
################################################################
#
# Job options file
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common.py" )
  if runArgs.ecmEnergy == 14000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common_14TeV.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig

# ... Herwig
Herwig.HerwigCommand += [ "taudec TAUOLA" ]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

# ... Event Filter

from GeneratorFilters.GeneratorFiltersConf import MultiObjectsFilter
topAlg += MultiObjectsFilter()

MultiObjectsFilter = topAlg.MultiObjectsFilter
MultiObjectsFilter.PtCut = 500.
MultiObjectsFilter.EtaCut = 3.0
MultiObjectsFilter.UseEle = True
MultiObjectsFilter.UseMuo = True
MultiObjectsFilter.UseJet = False
MultiObjectsFilter.UsePho = False
MultiObjectsFilter.UseSumPt = True
MultiObjectsFilter.PtCutEach = [3000., 500.]
MultiObjectsFilter.SumPtCut = 5000.

from GeneratorFilters.GeneratorFiltersConf import ATauFilter
topAlg += ATauFilter()
ATauFilter = topAlg.ATauFilter
ATauFilter.Etacut         = 10.                   ## Max |Eta|
ATauFilter.llPtcute       = 500.                 ## Min Pt(e)  for lep-lep
ATauFilter.llPtcutmu      = 500.                 ## Min Pt(mu) for lep-lep
ATauFilter.lhPtcute       = 50000000.                 ## Min Pt(e)  for lep-had
ATauFilter.lhPtcutmu      = 50000000.                 ## Min Pt(mu) for lep-had
ATauFilter.lhPtcuth       = 10000000.                ## Min Pt(h)  for lep-had
ATauFilter.hhPtcut        = 10000000.                ## Min Pt(h)  for had-had
#ATauFilter.maxdphi        = 3.0                   ## Max dPhi between tau and anti-tau



try:
  StreamEVGEN.RequireAlgs = [ "MultiObjectsFilter", "ATauFilter" ]
except Exception, e:
  pass

# input file names need updating for MC10
from MC10JobOptions.McAtNloEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'mcatnlo'
try:
  if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.mcatnlo342.116364.a10p5_tautau_7TeV.TXT.v1'
  if runArgs.ecmEnergy == 8000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.mcatnlo342.116364.a10p5_tautau_8TeV.TXT.v1'
except NameError:
  pass

evgenConfig.efficiency = 0.02






#==============================================================
#
# End of job options file
#
###############################################################
