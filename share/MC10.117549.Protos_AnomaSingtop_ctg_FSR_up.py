###############################################################
# Job options file for generating Anomalous top(c+g->t) events with Protos 2.01
# The top mass is 172.5 GeV 
# more FSR
# Muhammad alhroob (alhroob@cern.ch)
#==============================================================
#
from AthenaCommon.AlgSequence import AlgSequence
topAlg=AlgSequence("TopAlg")

include ("MC10JobOptions/MC10_PythiaMC09p_Common.py")

# Systematics(FSR up)
Pythia.PythiaCommand += [ "pypars mstp 3 1",      #set mstp 3 1 (setting Lambda in alpha_s)
                          "pypars parp 1 0.192",  #reset the defaults after mstp 3 1
                          "pypars parp 72 0.384", #FSR Labmda value in running alpha_s (ATLAS def 0.192)
                          "pydat1 paru 112 0.192",#reset defaults after mstp 3 1
                          "pydat1 mstu 112 4",    #reset defaults after mstp 3 1
                          "pypars parp 61 0.192"] #reset defaults after mstp 3 1
Pythia.PythiaCommand += [ "pydat1 parj 82 0.5" ]  

# Pythia options
Pythia.PythiaCommand += ["pyinit user protos",
                        "pydat1 parj 90 20000.",
                        "pydat3 mdcy 15 1 0",
                        "pyinit dumpr 1 12",
                        "pyinit pylistf 1"
                       ]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
# Dummy filename for 8, 10 and 14 TeV used the same as for 7 TeV
evgenConfig.inputfilebase = 'protos'
try:
  if runArgs.ecmEnergy == 7000.0:
     evgenConfig.inputfilebase = 'group09.phys-gener.protos201.117551.AnomaSingtop_ctg_7TeV.TXT.v1'
  if runArgs.ecmEnergy == 8000.0:
     evgenConfig.inputfilebase = 'group09.phys-gener.protos201.117551.AnomaSingtop_ctg_8TeV.TXT.v1'
  if runArgs.ecmEnergy == 10000.0:
     evgenConfig.inputfilebase = 'group09.phys-gener.protos201.117551.AnomaSingtop_ctg_10TeV.TXT.v1'
  if runArgs.ecmEnergy == 14000.0:
     evgenConfig.inputfilebase = 'group09.phys-gener.protos201.117551.AnomaSingtop_ctg_14TeV.TXT.v1' 	
except NameError:
  pass

evgenConfig.efficiency = 0.9
#==============================================================
# End of job options file
#
###############################################################
