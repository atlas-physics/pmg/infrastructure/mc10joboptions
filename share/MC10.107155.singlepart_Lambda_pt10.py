###############################################################
#
# Single Lambda pt10
# for tracking performance, Nov 2008
#
#==============================================================

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

from ParticleGenerator.ParticleGeneratorConf import ParticleGenerator
topAlg += ParticleGenerator()

ParticleGenerator = topAlg.ParticleGenerator

# For VERBOSE output from ParticleGenerator.
ParticleGenerator.OutputLevel = 1

ParticleGenerator.orders = [
 "PDGcode: sequence -3122 3122",
 "pt: constant 10000",
 "eta: flat -2.7 2.7",
 "phi: flat -3.14159 3.14159"
 ]

#==============================================================
#
# End of job options file
#
###############################################################
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.SingleEvgenConfig import evgenConfig
