#
#  Control file to generate LQ production in Pythia, M(LQ) = 250 GeV, decay: LQ -> u e or nu d
#
#  Dec. 17, 2009: Modified by Regina Caputo to include different masses, turn off single prod.
#
#  April 23, 2010: Modified by John Stupak to include decay to nu d/s
#
import AthenaCommon.AtlasUnixGeneratorJob

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

#include ( "MC10JobOptions/MC10_Pythia_Common.py" )
include ( "MC10JobOptions/MC10_PythiaD6_Common.py" )
#
#--------------------------------------------------------------
#
#  Full user control
#
Pythia.PythiaCommand += ["pysubs msel 0"]     #desired subprocesses have to be switched on in MSUB, i.e. full user control
#
#  LQ production
#
#++
#
#    qg -> lLQ
#Pythia.PythiaCommand += ["pysubs msub 162 1"]   #turn on
#
#    gg -> LQLQbar
Pythia.PythiaCommand += ["pysubs msub 163 1"]  #turn on
#
#    qqbar -> LQLQbar
Pythia.PythiaCommand += ["pysubs msub 164 1"]    #turn on
#
#  LQ mass
Pythia.PythiaCommand += ["pydat2 pmas 42 1 250."]     #set mass of LQ
Pythia.PythiaCommand += ["pydat3 mdcy 42 2 539"]
Pythia.PythiaCommand += ["pydat3 mdcy 42 3 2"]
#
#  To avoid problems in MC generation (mass ranges)
# range of allowed mass values of the two (or one) resonances produced in a "true" 2->2 process

Pythia.PythiaCommand += ["pysubs ckin 41 150.0"]  #m1>150GeV
Pythia.PythiaCommand += ["pysubs ckin 42 350.0"]  #m1<350GeV
Pythia.PythiaCommand += ["pysubs ckin 43 150.0"]  #m2>150GeV
Pythia.PythiaCommand += ["pysubs ckin 44 350.0"]  #m2<350GeV

Pythia.PythiaCommand += ["pydat1 paru 151 0.01"]
#
#  Branching fraction for the decay described below
Pythia.PythiaCommand += ["pydat3 brat 539 .5"]  #set branching ration of LQ
Pythia.PythiaCommand += ["pydat3 brat 540 .5"] 
#
#  This is relevant to both the decay AND production mechanism for single LQ
#
Pythia.PythiaCommand += ["pydat3 kfdp 539 1 2"]#decay product u
Pythia.PythiaCommand += ["pydat3 kfdp 539 2 11"]#decay product e
Pythia.PythiaCommand += ["pydat3 kfdp 540 1 1"]#d
Pythia.PythiaCommand += ["pydat3 kfdp 540 2 12"]#nu_e
Pythia.PythiaCommand += ["pydat3 mdme 539 1 4"]
Pythia.PythiaCommand += ["pydat3 mdme 540 1 5"]
Pythia.PythiaCommand += ["pydat3 mdme 539 2 0"]
Pythia.PythiaCommand += ["pydat3 mdme 540 2 0"]

#
#  Coupling: lambda=sqrt(4pi*alpha_em)
#
#  This choice of lambda was made to make single LQ production to have approx. same cross section as pair production
#
#Pythia.PythiaCommand += ["pydat1 paru 151 .8"]#I think this is just for single LQ prod.
# 
#-------------------------------------------------------------
#
#  In order to prevent double counting in Pythia when PHOTOS is used
#
Pythia.PythiaCommand += ["pydat1 parj 90 20000"]
#
#  Tell Pythia NOT to decay taus
#
Pythia.PythiaCommand += ["pydat3 mdcy 15 1 0"]
#
#-------------------------------------------------------------
#
#  Print the event listing for events x though y: 
#
Pythia.PythiaCommand += ["pyinit dumpr 1 20"]
#
#-------------------------------------------------------------
#
# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

#
# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 1.

#==============================================================
#
# End of job options file
#
###############################################################
        


