###############################################################
#
# Created by G. Pasztor (November 2009)
# Modified by C.Mora Herrera (February 2011)
# * add Photos and Tauola
# * turn off underlying event
#==============================================================

#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_PythiaB_Common.py" )

#--------------------------------------------------------------
#              PARAMETERS  SPECIFIC  TO   PYTHIABMODULE
#--------------------------------------------------------------
PythiaB.ForceBDecay = "no"
PythiaB.ForceCDecay = "no"

#--------------------------------------------------------------
# --------  PYTHIA PARAMETERS OPTIMAL FOR BEAUTY PRODUCTION  --
#--------------------------------------------------------------
include( "MC10JobOptions/MC10_PythiaB_Btune.py" )

PythiaB.PythiaCommand += [ "pysubs ckin 3 1.",
                           "pysubs msel 1" ]

#--------------------------------------------------------------
# -------------  DEFINE SELECTION CUTS  -------------
#--------------------------------------------------------------
PythiaB.flavour =  4.
#  ------------- Selections on b  quarks   -------------
PythiaB.cutbq = ["0. 102.5 or 0. 102.5"]
#  ------------- LVL1 muon cuts 0=OFF 1=ON -------------
PythiaB.lvl1cut = [ 0., 0., 102.5]
#  ------------- LVL2 muon/electron cuts  0=OFF 1=ON-------------
PythiaB.lvl2cut = [ 0., 11., 3., 2.7]
#  ------------- Offline cuts 0=OFF 1=ON -------------
PythiaB.offcut = [ 0., 0., 102.5, 0., 102.5, 0., 102.5]
#  ------------- Number of repeated hadronization mhadr -------------
PythiaB.mhadr =  1.

#--------------------------------------------------------------
# ---------------- Use Tauola and Photos ----------------
#--------------------------------------------------------------
# ... Tauola
PythiaB.PythiaCommand += [ "pydat3 mdcy 15 1 0"] # turn off tau decays
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
PythiaB.PythiaCommand += ["pydat1 parj 90 20000"] # turn off FSR for Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# ------- Turn off the Underlying Event -----------------
#--------------------------------------------------------------
PythiaB.PythiaCommand += [ "pypars mstp 81 20" ]

#----------------------------------------------------------------
# Lepton Filter 
#----------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
topAlg += LeptonFilter()

LeptonFilter = topAlg.LeptonFilter
LeptonFilter.Ptcut = 3000.
LeptonFilter.Etacut = 2.7

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------
try:
    StreamEVGEN.RequireAlgs += [ "LeptonFilter" ]
except Exception, e:
    pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
# efficiency = 500/68116*0.9 = 0.0066 at 7 TeV
from MC10JobOptions.PythiaBEvgenConfig import evgenConfig
evgenConfig.minevents  = 500
evgenConfig.efficiency = 0.0066
