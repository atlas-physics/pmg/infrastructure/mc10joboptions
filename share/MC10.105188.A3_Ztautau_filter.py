###############################################################
#
# Job options file
#
# Donatella Cavalli
# This replaces the originall 5148 which was wrong: Ian Hinchliffe
#
# April, 2008 (by Junichi Tanaka)
# - update GEF efficiency (for a minor bug-fix in PY6.4.14)
#
# July, 2007 (by Junichi Tanaka for tau-group)
# - turn on gamma-interference
# - suppress FSR photon
#==============================================================
#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------

# ... Main generator : Pythia
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )
Pythia.PythiaCommand += [
                         "pysubs msel 0",
                         "pysubs msub 1 1",
                         "pysubs ckin 1 60.",
                         "pydat3 mdme 174 1 0",
                         "pydat3 mdme 175 1 0",
                         "pydat3 mdme 176 1 0",
                         "pydat3 mdme 177 1 0",
                         "pydat3 mdme 178 1 0",
                         "pydat3 mdme 179 1 0",
                         "pydat3 mdme 182 1 0",
                         "pydat3 mdme 183 1 0",
                         "pydat3 mdme 184 1 0",
                         "pydat3 mdme 185 1 0",
                         "pydat3 mdme 186 1 1",
                         "pydat3 mdme 187 1 0",
                         "pydat3 mdcy 15 1 0",
                         "pydat1 parj 90 20000."
                         ]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import ATauFilter
topAlg += ATauFilter()

ATauFilter = topAlg.ATauFilter
ATauFilter.Etacut      = 2.7
ATauFilter.llPtcute    = 13000.0
ATauFilter.llPtcutmu   = 8000.0
ATauFilter.lhPtcute    = 13000.0
ATauFilter.lhPtcutmu   = 8000.0
ATauFilter.lhPtcuth    = 12000.0
ATauFilter.hhPtcut     = 12000.0
ATauFilter.maxdphi     = 2.9
ATauFilter.OutputLevel = INFO

try:
     StreamEVGEN.RequireAlgs +=  [ "ATauFilter" ]
except Exception, e:
     pass


#==============================================================
#
# End of job options file
#
###############################################################
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
# v12/13
#evgenConfig.efficiency = 0.125
# v14.0.0
evgenConfig.efficiency = 0.103
#48077 ==> 5511
#eff(EF) = .11462861659421344925
#eff(EF)x0.9 = .10316575493479210432
