###########################################################
##  jobOptions for evgen of the process:                 ##
##  h --> v-pion v-pion (v-pion --> u-boson)             ##
##                                                       ##
##  author: Daniel Ventura, U. Washington                ##
##          ventura@cern.ch                              ##
###########################################################

##create the pyupda file to be read
f = open('ggH.leptjets.pyupda.in','w')
#             PDGID      Name                                      MASS (GeV)   
f.write('        35       H0                             0  0  0   200.00000     0.01000     0.10000  0.00001E+00  2  1\n')
#                   on/off       BR. Frac.  |<------------------Daughters---------------->|
f.write('              1   0     0.399800   6000111   6000111         0         0         0\n')
f.write('              0   0     0.300000   6001022   6001022         0         0         0\n')
f.write('              0   0     0.300000   6001022   6001022   6001022         0         0\n')
f.write('              0   0     0.000100   6001022   6001022   6001022   6001022         0\n')
f.write('              1   0     0.000100        21        21\n')
#             PDGID    Name                                         MASS (GeV)   
f.write('   6000111  ~v_pion                             0  0  0     4.00000     0.01000     0.10000  1.00000E-03  2  1\n')
#                   on/off       BR. Frac.  |<------------------Daughters---------------->|
f.write('              1   0     1.000000   6001022   6001022         0         0         0\n')
#             PDGID    Name                                         MASS (GeV)   
f.write('   6001022  ~Uboson                             0  0  0     0.60000     0.01000     0.10000  5.00000E+00  2  1\n')
#                   on/off       BR. Frac.  |<------------------Daughters---------------->|
f.write('              1   0     0.2500        11       -11         0         0         0\n')
f.write('              1   0     0.2500        13       -13         0         0         0\n')
f.write('              1   0     0.5000       211      -211         0         0         0\n')
f.close()

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

#include ("MC10JobOptions/MC10_Pythia_Common.py")

from PythiaExo_i.PythiaLLP_iConf import PythiaLLP
topAlg += PythiaLLP()
Pythia = topAlg.PythiaLLP
#####################################################################################
#####################################################################################
Pythia.PythiaCommand += ["pypars mstp 5 20090002"] 
Pythia.PythiaCommand += [
    # initializations
    "pyinit pylisti 12",
    "pyinit pylistf 1",
    "pystat 1 3 4 5",
    "pyinit dumpr 1 5",
    # mass
    "pydat2 pmas 6 1 172.5",    # PDG2007 TOP mass 
    "pydat2 pmas 24 1 80.403",  # PDG2007 W mass
    "pydat2 pmas 24 2 2.141",   # PDG2007 W width
    "pydat2 pmas 23 1 91.1876", # PDG2007 Z0 mass
    "pydat2 pmas 23 2 2.4952",  # PDG2007 Z0 width
    ]

#####################################################################################
#####################################################################################

# HV Higgs lepton-jets production
Pythia.LLPpyupda = "ggH.leptjets.pyupda.in"
Pythia.PythiaCommand += ["pysubs msel 0"]
Pythia.PythiaCommand += ["pydat1 parj 22 2"]
Pythia.PythiaCommand += ["pydat1 parj 71 500000"] #max ct of particles
Pythia.PythiaCommand += ["pypars mstp 127 1"]

#Create gg --> H0 --> vpi vpi 
Pythia.PythiaCommand += ["pysubs msub 152 1"]

#set the mass of the H0
Pythia.PythiaCommand += ["pydat2 pmas 35 1 120.D0"]

#set the mass of the v-pion
Pythia.PythiaCommand += ["pydat2 pmas 6000111  1 10.D0"]

#set the lifetime of the v-pion (c tau in mm)
Pythia.PythiaCommand += ["pydat2 pmas 6000111 4 500.D0"]

#set the mass of the u-boson
Pythia.PythiaCommand += ["pydat2 pmas 6001022 1 0.6D0"]
Pythia.PythiaCommand += ["pydat2 pmas 6001022 4 0.D0"]

# FSR/tau decay treatment
Pythia.PythiaCommand += ["pydat1 parj 90 20000.",  ## Turn off FSR
                         "pydat3 mdcy 15 1 0"]     ## Turn off tau decays

# Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )
# Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------

# Configuration for EvgenJobTransforms

#--------------------------------------------------------------

#from EvgenJobOptions.PythiaEvgenConfig import evgenConfig
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.95


