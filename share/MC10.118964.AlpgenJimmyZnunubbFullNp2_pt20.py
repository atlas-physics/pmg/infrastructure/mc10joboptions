###############################################################
#
# Job options file
# Daniel Geerts
#
#==============================================================
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 3

#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------

# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 

try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common.py" )
  if runArgs.ecmEnergy == 14000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_14TeV.py" )
except NameError:
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig 


Herwig.HerwigCommand += [ "iproc alpgen",
                          "taudec TAUOLA",
                        ]
# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from MC10JobOptions.AlpgenEvgenConfig import evgenConfig
 

# dummy needed
evgenConfig.inputfilebase = 'alpgen'
try:
  if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.alpgen.118964.ZbbnunuFullNp2_pt20_7tev.TXT.v1'
  if runArgs.ecmEnergy == 8000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.alpgen.118964.ZbbnunuFullNp2_pt20_8tev.TXT.v1'
except NameError:
  pass

# 7 TeV - Information on sample 	118964	
# 7 TeV - Filter efficiency  = 	1.0000	
# 7 TeV - MLM matching efficiency = 	0.27	
# 7 TeV - Number of Matrix Elements in input file  = 	2450	
# 7 TeV - Alpgen cross section = 	18.8	 pb
# 7 TeV - Herwig cross section = Alpgen cross section * eff(MLM) = 	5.1	 pb
# 7 TeV - Cross section after filtering = 	5.1	 pb
# 7 TeV - Lumi/500 events post-filter = 500/eff(Filter)/XS(jimmy) = 	98.23	 pb-1
#		
# 7 TeV - Filter efficiency estimate below reduced by 10% to produce 555 events on average,		
# 7 TeV - of which only 500 will be used in further processing		
evgenConfig.efficiency = 0.90000
#==============================================================
#
# End of job options file
#
###############################################################
