###############################################################
#
# Job options file
#
# Herwig VBF H->gamgam (mH=105GeV) with 2 photon EF
#
# Responsible person(s)
#   24 June, 2008-xx xxx, 20xx: Junichi TANAKA (Junichi.Tanaka@cern.ch)
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 

try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_Herwig_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_Herwig_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_Herwig_Common.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig

# ... Herwig
Herwig.HerwigCommand += [ "iproc 11912",
                          "rmass 201 105.0" ]

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import PhotonFilter
topAlg += PhotonFilter()

PhotonFilter = topAlg.PhotonFilter
PhotonFilter.Ptcut = 20000.
PhotonFilter.Etacut = 2.7
PhotonFilter.NPhotons = 2

try:
     StreamEVGEN.RequireAlgs +=  [ "PhotonFilter" ]
except Exception, e:
     pass

from MC10JobOptions.HerwigEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.68
# 14.2.21.2
#5000/6563=.76184671644065214078
#5000/6563*0.9=.68566204479658692670
#==============================================================
#
# End of job options file
#
###############################################################
