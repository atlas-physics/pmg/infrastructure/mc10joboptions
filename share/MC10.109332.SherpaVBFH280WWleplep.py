# Higgs to WW with Sherpa (VBF H) for 10 TeV run
#-----------------------------------------------]---------------
# Bill Quayle 09/2408
#--------------------------------------------------------------

import AthenaCommon.AtlasUnixGeneratorJob
from AthenaCommon.AppMgr import ServiceMgr as svcMgr

#load relevant libraries
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

# ... Sherpa
from Sherpa_i.Sherpa_iConf import ReadSherpa_i
sherpa = ReadSherpa_i()
sherpa.Files = [ "sherpa.evts" ]
topAlg += sherpa


# ... Photos
#include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------

#---------------------------------------------------------------
# Configuration for EvgenJobTransforms
#---------------------------------------------------------------

from MC10JobOptions.SherpaEvgenConfig import evgenConfig
 
# inputfilebase
evgenConfig.inputfilebase = 'Sherpa10102.109332.SherpaVBFH280wwleplep'

evgenConfig.efficiency = 1  

#---------------------------------------------------------------
#---------------------------------------------------------------


# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
#MessageSvc = Service( "MessageSvc" )
#MessageSvc.OutputLevel = 5



