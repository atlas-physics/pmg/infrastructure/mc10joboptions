###############################################################
#
# Job options file
# Wouter Verkerke
#
#==============================================================
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 3

#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------

# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 

try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common.py" )
  if runArgs.ecmEnergy == 14000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_14TeV.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig


Herwig.HerwigCommand += [ "iproc alpgen",
                          "taudec TAUOLA",
                        ]
# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

# TruthJet filters
try:
     from JetRec.JetGetters import *
     c4=make_StandardJetGetter('Cone',0.4,'Truth')
     c4alg = c4.jetAlgorithmHandle()
except Exception, e:
     pass
 
from GeneratorFilters.GeneratorFiltersConf import TruthJetFilter

topAlg += TruthJetFilter("OneJetFilter")
topAlg += TruthJetFilter("ThreeJetsFilter")
topAlg += TruthJetFilter("DPhiFilter")
 
OneJetFilter = topAlg.OneJetFilter
OneJetFilter.Njet=1;
OneJetFilter.NjetMinPt=120.*GeV;
OneJetFilter.NjetMaxEta=2.8;
OneJetFilter.jet_pt1=120.*GeV;
OneJetFilter.applyDeltaPhiCut=False;
OneJetFilter.TruthJetContainer="Cone4TruthJets";

ThreeJetsFilter = topAlg.ThreeJetsFilter
ThreeJetsFilter.Njet=3;
ThreeJetsFilter.NjetMinPt=25.*GeV;
ThreeJetsFilter.NjetMaxEta=2.8;
ThreeJetsFilter.jet_pt1=60.*GeV;
ThreeJetsFilter.applyDeltaPhiCut=False;
ThreeJetsFilter.TruthJetContainer="Cone4TruthJets";

DPhiFilter = topAlg.DPhiFilter
DPhiFilter.Njet=2;
DPhiFilter.NjetMinPt=25.*GeV;
DPhiFilter.NjetMaxEta=2.8;
DPhiFilter.jet_pt1=70.*GeV;
DPhiFilter.applyDeltaPhiCut=True;
DPhiFilter.MinDeltaPhi=0.8;
DPhiFilter.TruthJetContainer="Cone4TruthJets";

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------
try:
     StreamEVGEN.AcceptAlgs+=[ "OneJetFilter", "ThreeJetsFilter", "DPhiFilter" ]
except Exception, e:
     pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.AlpgenEvgenConfig import evgenConfig
 
# inputfilebase
evgenConfig.inputfilebase = 'alpgen.107368.QcdbbJ3Np3.SUSYfilt.pt20'
# Information on sample 107368
# Filter efficiency  = 0.7272
# MLM matching efficiency = 0.226
# Number of Matrix Elements in input file  = 4600
# Alpgen cross section = 42145.0 pb
# Herwig cross section = Alpgen cross section * eff(MLM) = 9525.0 pb
# Cross section after filtering = 6926.3 pb
# Lumi/500 events post-filter = 500/eff(Filter)/XS(jimmy) = 0.07 pb-1
#
# Filter efficiency estimate below reduced by 20% to produce 625 events on average,
# of which only 500 will be used in further processing
evgenConfig.efficiency = 0.58173


#==============================================================
#
# End of job options file
#
###############################################################
