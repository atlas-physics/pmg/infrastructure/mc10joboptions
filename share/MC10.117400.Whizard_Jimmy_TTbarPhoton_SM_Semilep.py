# -*- coding: iso-8859-1 -*-
###############################################################
# Job options file for WHIZARD with Herwig+Jimmy
# Oliver Rosenthal, December 2010
#
#==============================================================
#--------------------------------------------------------------
# General Application Configuration options
#--------------------------------------------------------------
import AthenaCommon.AtlasUnixGeneratorJob

from AthenaCommon.AppMgr import theApp
from AthenaCommon.AppMgr import ServiceMgr

# make sure we are loading the ParticleProperty service
from PartPropSvc.PartPropSvcConf import PartPropSvc
ServiceMgr += PartPropSvc()

#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
ServiceMgr.MessageSvc.OutputLevel = INFO

#--------------------------------------------------------------
# Event related parameters
#--------------------------------------------------------------
# Number of events to be processed (default is 10)
#theApp.EvtMax = -1
#--------------------------------------------------------------
# Algorithms Private Options
#--------------------------------------------------------------
# from AthenaServices.AthenaServicesConf import AtRndmGenSvc
# ServiceMgr += AtRndmGenSvc()
# ServiceMgr.AtRndmGenSvc.Seeds = ["HERWIG 4789899 989240512", "HERWIG_INIT 820021 2347532"]


from AthenaCommon.AlgSequence import AlgSequence
topAlg=AlgSequence()

# ... Main generator : Herwig
try:
    if runArgs.ecmEnergy == 7000.0:
      include ( "MC10JobOptions/MC10_Herwig_Common_7TeV.py" )
    if runArgs.ecmEnergy == 8000.0:
      include ( "MC10JobOptions/MC10_Herwig_Common_8TeV.py" )
except NameError:
    # needed (dummy) default
    from Herwig_i.Herwig_iConf import Herwig
    topAlg += Herwig()
    Herwig = topAlg.Herwig


Herwig.HerwigCommand += [
			  # Initializations
			  "iproc lhef",
			  "taudec TAUOLA"]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import TTbarPhotonWhizardHwgFilter
topAlg += TTbarPhotonWhizardHwgFilter()
TTbarPhotonWhizardHwgFilter = topAlg.TTbarPhotonWhizardHwgFilter

try:
   StreamEVGEN.RequireAlgs = [ "TTbarPhotonWhizardHwgFilter" ]
except Exception, e:
   pass


from MC10JobOptions.EvgenConfig import evgenConfig, knownGenerators
evgenConfig.generators += [ "Lhef", "Herwig" ]

# dummy needed
evgenConfig.inputfilebase = 'whizard'
try:
  if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.whizard.117400.ttbarphoton_sm_semilep_7TeV.TXT.v1'
  if runArgs.ecmEnergy == 8000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.whizard.117400.ttbarphoton_sm_semilep_8TeV.TXT.v1'
except NameError:
  pass


# events passing filter: 72% => 0.72*0.95= 0.68
evgenConfig.efficiency = 0.68
#==============================================================
#
# End of job options file
#
##############################################################
