###############################################################
#
# Job options file
#
# t bar t Higgs (105 GeV) to gamma gamma with Pythia (ttH)
#
# Responsible person(s)
#   2 Oct, 2008-xx xxx, 20xx: Bertrand Brelier (brelier@lps.umontreal.ca)
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

# ... Pythia
include ( "MC10JobOptions/MC10_Pythia_Common.py" )
Pythia.PythiaCommand += [
     "pysubs msel 0",
     "pypars mstp 7 6",
     "pysubs msub 121 1",
     "pysubs msub 122 1",
     "pydat2 pmas 25 1 105.",
     "pydat2 pmas 25 2 0.00262",
     "pydat3 mdme 210 1 0",
     "pydat3 mdme 211 1 0",
     "pydat3 mdme 212 1 0",
     "pydat3 mdme 213 1 0",
     "pydat3 mdme 214 1 0",
     "pydat3 mdme 215 1 0",
     "pydat3 mdme 216 1 -1",
     "pydat3 mdme 217 1 -1",
     "pydat3 mdme 218 1 0",
     "pydat3 mdme 219 1 0",
     "pydat3 mdme 220 1 0",
     "pydat3 mdme 221 1 -1",
     "pydat3 mdme 222 1 0",
     "pydat3 mdme 223 1 1", #H-> GamGam
     "pydat3 mdme 224 1 0",
     "pydat3 mdme 225 1 0",
     "pydat3 mdme 226 1 0",
     "pydat1 parj 90 20000",
     "pydat3 mdcy 15 1 0" ]

# ... TAUOLA
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import PhotonFilter
topAlg += PhotonFilter()

PhotonFilter = topAlg.PhotonFilter
PhotonFilter.Ptcut = 20000.
PhotonFilter.Etacut = 2.7
PhotonFilter.NPhotons = 2

try:
     StreamEVGEN.RequireAlgs +=  [ "PhotonFilter" ]
except Exception, e:
     pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.79
#==============================================================
#
# End of job options file
#
###############################################################
