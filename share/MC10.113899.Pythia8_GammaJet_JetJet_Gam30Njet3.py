#--------------------------------------------------------
# Pythia8 Gamma+Jet and 2nd hard scattering jetjet (113899)
# Photon filter: pT(gamma) > 30GeV |eta|<2.8
# Truth jet filter:  njets > 3 |eta(jets)| < 3.5
# Prepared by B.Salvachua, Apr 2011
#--------------------------------------------------------

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ("MC10JobOptions/MC10_Pythia8_Common.py")

#--------------------------------------------------------
# PYTHIA8: Gamma + Jets AND 2nd hard scattering jetjet
#--------------------------------------------------------

Pythia8.Commands += ["PromptPhoton:qg2qgamma = on",
                     "PromptPhoton:qqbar2ggamma = on",
                     "PromptPhoton:gg2ggamma = on",
                     "PhaseSpace:pTHatMin = 8.",
                     "PartonLevel:MI = on",
                     "SecondHard:generate = on",
                     "SecondHard:TwoJets = on"
                     ]

#--------------------------------------------------------
# Photon Filter
#--------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import PhotonFilter
topAlg += PhotonFilter()

PhotonFilter = topAlg.PhotonFilter
PhotonFilter.Ptcut = 30000.
PhotonFilter.Etacut = 2.8
PhotonFilter.NPhotons = 1

#--------------------------------------------------------
# Truth Jet Filter
#--------------------------------------------------------

from JetRec.JetGetters import *
akt4=make_StandardJetGetter('AntiKt',0.4,'Truth')
akt4alg = akt4.jetAlgorithmHandle()
akt4alg.AlgTools["JetFinalPtCut"].MinimumSignal = 4.*GeV
akt4alg.AlgTools["JetFinalPtCut"].UseTransverseMomentum = True
con4=make_StandardJetGetter('Cone',0.4,'Truth')
con4alg = akt4.jetAlgorithmHandle()
con4alg.AlgTools["JetFinalPtCut"].MinimumSignal = 4.*GeV
con4alg.AlgTools["JetFinalPtCut"].UseTransverseMomentum = True

from GeneratorFilters.GeneratorFiltersConf import TruthJetFilter
topAlg += TruthJetFilter("TruthJetFilterLow")
topAlg.TruthJetFilterLow.OutputLevel=INFO
topAlg.TruthJetFilterLow.TruthJetContainer="AntiKt4TruthJets"
topAlg.TruthJetFilterLow.Njet = 3
topAlg.TruthJetFilterLow.NjetMinPt = 0.*GeV
topAlg.TruthJetFilterLow.NjetMaxEta = 3.5
topAlg.TruthJetFilterLow.jet_pt1 = 0.*GeV

#--------------------------------------------------------
# POOL / Root output
#--------------------------------------------------------

try:
    StreamEVGEN.RequireAlgs += [ "PhotonFilter" ]
    StreamEVGEN.RequireAlgs += [ "TruthJetFilterLow" ]
except Exception, e:
    pass


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
#efficiency = 5067/384652*0.9 = 0.0118
from MC10JobOptions.Pythia8EvgenConfig import evgenConfig
evgenConfig.efficiency = 0.01 # photon and jet filter
