###############################################################
#
# Psi2S->mu0mu0 channel
#
#==============================================================
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.7
#--------------------------------------------------------------
# General Application Configuration options
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

from GeneratorFilters.GeneratorFiltersConf import BSignalFilter
topAlg += BSignalFilter()

Pythia.PythiaCommand += ["pysubs msel 61",
                         "pysubs ckin 3 1.0"]

Pythia.PythiaCommand += ["pydat2 pmas 9900441 1 3.700",
                         "pydat2 pmas 9900443 1 3.700",
                         "pydat2 pmas 9910441 1 3.700"]

#   J/psi -> mu+,mu-
Pythia.PythiaCommand += ["pydat3 mdme 858 1 0",
                         "pydat3 mdme 859 1 1",
                         "pydat3 mdme 860 1 0",
                         "pydat3 mdme 1567 1 0",
                         "pydat3 mdme 1568 1 1",
                         "pydat3 mdme 1569 1 0",
                         "pydat3 mdme 1570 1 0",
                         "pydat3 mdme 1571 1 0",
                         "pydat3 mdme 1572 1 0",
                         "pydat3 mdme 1573 1 0",
                         "pydat3 mdme 1574 1 0",
                         "pydat3 mdme 1575 1 0",
                         "pydat3 mdme 1576 1 0",
                         "pydat3 mdme 1577 1 0"]

Pythia.PythiaCommand += ["pyint2 kfpr 421 1 100443",
                         "pydat3 kfdp 4211 1 100443",
                         "pydat3 kfdp 4212 1 100443",
                         "pydat3 kfdp 4213 1 100443"]
Pythia.PythiaCommand += [#"pypars parp  142 0",
    #"pypars parp  143 0",
    #"pypars parp  144 0",
    "pypars parp  145 0",
    "pypars parp  146 0"]

Pythia.PythiaCommand += ["pystat 2 5 6"]


#------- Muon Trigger Cuts --------
BSignalFilter = topAlg.BSignalFilter
#-------------- Level 1 Muon Cuts --------------------- 
BSignalFilter.LVL1MuonCutOn = True
BSignalFilter.LVL1MuonCutPT = 0.0 
BSignalFilter.LVL1MuonCutEta = 4.
#-------------- Level 2 lepton cuts -------------------
# These will only function if LVL1 trigger used. 
BSignalFilter.LVL2MuonCutOn = True 
BSignalFilter.LVL2MuonCutPT = 0.0 
BSignalFilter.LVL2MuonCutEta = 4.

try:
     StreamEVGEN.RequireAlgs += ["BSignalFilter"]
except Exception, e:
     pass

#==============================================================
#
# End of job options file
#
###############################################################

