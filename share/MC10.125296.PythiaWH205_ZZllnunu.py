###############################################################
#
# Job options file
#
# WH, W->all, H->ZZ where Z->ll, Z->nunu (l=e, mu and tau)
#
# Responsible person(s) Junichi TANAKA
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

# ... Pythia
include ( "MC10JobOptions/MC10_Pythia_Common.py" )
Pythia.PythiaCommand += [
     "pysubs msel 0",
     "pydat1 parj 90 20000",  # Turn off FSR.
     "pydat3 mdcy 15 1 0",    # Turn off tau decays.
     "pysubs msub 26 1",      # WH production
     "pydat2 pmas 25 1 205", # Higgs mass
     "pydat2 pmas 25 2 1.63", # Higgs width
     "pysubs ckin 45 2.",
     "pysubs ckin 47 2.",
     "pydat3 mdme 174 1  0",  # Z-decay 174-189
     "pydat3 mdme 175 1  0",
     "pydat3 mdme 176 1  0",
     "pydat3 mdme 177 1  0",
     "pydat3 mdme 178 1  0",
     "pydat3 mdme 179 1  0",
     "pydat3 mdme 180 1 -1",
     "pydat3 mdme 181 1 -1",
     "pydat3 mdme 182 1  4",
     "pydat3 mdme 183 1  5",
     "pydat3 mdme 184 1  4",
     "pydat3 mdme 185 1  5",
     "pydat3 mdme 186 1  4",
     "pydat3 mdme 187 1  5",
     "pydat3 mdme 188 1 -1",
     "pydat3 mdme 189 1 -1",
     "pydat3 mdme 190 1  1",  # W-decay 190-209
     "pydat3 mdme 191 1  1",
     "pydat3 mdme 192 1  1",
     "pydat3 mdme 193 1 -1",
     "pydat3 mdme 194 1  1",
     "pydat3 mdme 195 1  1",
     "pydat3 mdme 196 1  1",
     "pydat3 mdme 197 1 -1",
     "pydat3 mdme 198 1  1",
     "pydat3 mdme 199 1  1",
     "pydat3 mdme 200 1  1",
     "pydat3 mdme 201 1 -1",
     "pydat3 mdme 202 1 -1",
     "pydat3 mdme 203 1 -1",
     "pydat3 mdme 204 1 -1",
     "pydat3 mdme 205 1 -1",
     "pydat3 mdme 206 1  1",
     "pydat3 mdme 207 1  1",
     "pydat3 mdme 208 1  1",
     "pydat3 mdme 209 1 -1",
     "pydat3 mdme 210 1  0",  # H-decay 210-226 (SM)
     "pydat3 mdme 211 1  0",
     "pydat3 mdme 212 1  0",
     "pydat3 mdme 213 1  0",
     "pydat3 mdme 214 1  0",
     "pydat3 mdme 215 1  0",
     "pydat3 mdme 216 1 -1",
     "pydat3 mdme 217 1 -1",
     "pydat3 mdme 218 1  0",
     "pydat3 mdme 219 1  0",
     "pydat3 mdme 220 1  0",  # H->tautau
     "pydat3 mdme 221 1 -1",
     "pydat3 mdme 222 1  0",
     "pydat3 mdme 223 1  0",  # H->gamgam
     "pydat3 mdme 224 1  0",  # H->gamZ
     "pydat3 mdme 225 1  1",  # H->ZZ
     "pydat3 mdme 226 1  0"   # H->WW
     ]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.95
#==============================================================
#
# End of job options file
#
###############################################################
