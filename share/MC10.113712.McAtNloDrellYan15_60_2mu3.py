###############################################################
#
# Job options file
# Edited by Jack Goddard
#
# DYmumu  15<M<60 GeV,  Two Lepton Filters Applied: |eta| < 2.7, pt > 3
#  
# validated in Rel. 16.6.3.5 (June 2011)
# adapted from MC10.106088.McAtNloZmumu_no_filter.py
# Originally Developed by Pavel Staroba
#
#===============================================================

# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common_7TeV.py" )
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common.py" )
  if runArgs.ecmEnergy == 14000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common_14TeV.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig

Herwig.HerwigCommand += [ "taudec TAUOLA"]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
topAlg += MultiLeptonFilter()

MultiLeptonFilter = topAlg.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 3000.
MultiLeptonFilter.Etacut = 2.7
MultiLeptonFilter.NLeptons = 2

try:
  StreamEVGEN.RequireAlgs += [ "MultiLeptonFilter" ]
except Exception, e:
  pass


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
#
# Evgen Calculated efficiency = (0.402)*0.9 = 0.3618 
# Cross Section in Rel. 16.6.3.5 : 1.499 nb
#
#--------------------------------------------------------------
from MC10JobOptions.McAtNloEvgenConfig import evgenConfig


evgenConfig.inputfilebase = 'group.phys-gener.mcatnlo342.113712.DYmumu_15_60.TXT.mc10_v1'

evgenConfig.efficiency = 0.3618


#==============================================================
#
# End of job options file
#
###############################################################
