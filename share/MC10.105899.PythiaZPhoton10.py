from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

#--------------------------------------------------------------
# File prepared by Jiahang Zhong June 2007
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
include ( "MC10JobOptions/MC10_Pythia_Common.py" )
Pythia.PythiaCommand += [     "pysubs msel 0",
                              "pysubs ckin 3 10.",
                              "pysubs msub 19 1", # Z decays
                              "pydat3 mdme 174 1 0",
                              "pydat3 mdme 175 1 0",
                              "pydat3 mdme 176 1 0",
                              "pydat3 mdme 177 1 0",
                              "pydat3 mdme 178 1 0",
                              "pydat3 mdme 179 1 0",
                              "pydat3 mdme 182 1 1",    # Switch for Z->ee.
                              "pydat3 mdme 183 1 0",
                              "pydat3 mdme 184 1 1",    # Switch for Z->mumu.
                              "pydat3 mdme 185 1 0",
                              "pydat3 mdme 186 1 1",    # Switch for Z->tautau.
                              "pydat3 mdme 187 1 0"]


#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import PhotonFilter
topAlg += PhotonFilter()

PhotonFilter = topAlg.PhotonFilter
PhotonFilter.Ptcut = 10000.
PhotonFilter.Etacut = 2.7
PhotonFilter.NPhotons = 1


#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------

try:
     StreamEVGEN.RequireAlgs +=  [ "PhotonFilter" ]
except Exception, e:
     pass


#==============================================================
#
# End of job options file
#
###############################################################
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.6
