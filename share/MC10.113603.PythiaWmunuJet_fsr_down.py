###############################################################
#
# Job options file
# Developed by Pavel Staroba from CSC.006910.PythiaWenuJet.py
# in Rel. 14.2.0.1 (July 2008)
#
#==============================================================
# ... Main generator : Pythia
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

# FSR systematics sample: less FSR activity, external LO process + PYTHIA
# PARP(72) and PARJ(82) are varied                                                                             
# MC09 default values: PARP(72)=0.192 GeV, PARJ(82)=1.GeV                                                      
# PARP(72) :  lambda_FSR                                                                                      
# PARJ(82) :  FSR regularization IR cutoff                                                                    
#             see Pythia Manual for more info
Pythia.PythiaCommand += [ "pypars mstp 3 1",      #set mstp 3 1 (setting Lambda in alpha_s)
                          "pypars parp 1 0.192",  #reset the defaults after mstp 3 1
                          "pypars parp 72 0.096", #FSR Labmda value in running alpha_s (ATLAS def 0.192)
                          "pydat1 paru 112 0.192",#reset defaults after mstp 3 1
                          "pydat1 mstu 112 4",    #reset defaults after mstp 3 1
                          "pypars parp 61 0.192"] #reset defaults after mstp 3 1

Pythia.PythiaCommand +=[ "pydat1 parj 82 2.0" ]

Pythia.PythiaCommand += [ "pysubs msel 14",
                         "pydat1 parj 90 20000", # Turn off FSR.
                         "pydat3 mdcy 15 1 0",   # Turn off tau decays.
                         "pysubs ckin 3 10.0",   # Lower P_T for hard 2 ->2 process.
                         "pydat3 mdme 190 1 0",
                         "pydat3 mdme 191 1 0",
                         "pydat3 mdme 192 1 0",
                         "pydat3 mdme 194 1 0",
                         "pydat3 mdme 195 1 0",
                         "pydat3 mdme 196 1 0",
                         "pydat3 mdme 198 1 0",
                         "pydat3 mdme 199 1 0",
                         "pydat3 mdme 200 1 0",
                         "pydat3 mdme 206 1 0",    # Switch for W->enu.
                         "pydat3 mdme 207 1 1",    # Switch for W->munu.
                         "pydat3 mdme 208 1 0"     # Switch for W->taunu.
                         ]

# ... UE

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
# efficiency = 0.9 - no filtering
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.90
#==============================================================
#
# End of job options file
#
###############################################################
