###############################################################
#
# Job options file
# Wouter Verkerke
# MC9 modifications : Renaud Bruneliere
# MC10 modifications : Daniel Geerts
#
#==============================================================
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 3

#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------

# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 

try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common.py" )
  if runArgs.ecmEnergy == 14000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_14TeV.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig


Herwig.HerwigCommand += [ "iproc alpgen",
                          "taudec TAUOLA",
                        ]
# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

# TruthJet filter
try:
  from JetRec.JetGetters import *
  if runArgs.ecmEnergy == 7000.0:
    c4=make_StandardJetGetter('AntiKt',0.4,'Truth')
  if runArgs.ecmEnergy == 8000.0:
    c4=make_StandardJetGetter('AntiKt',0.4,'Truth')
  if runArgs.ecmEnergy == 10000.0:
    c4=make_StandardJetGetter('Cone',0.4,'Truth')
  c4alg = c4.jetAlgorithmHandle()
except Exception, e:
  pass

from GeneratorFilters.GeneratorFiltersConf import TruthJetFilter
topAlg += TruthJetFilter()

TruthJetFilter = topAlg.TruthJetFilter
try:
  if runArgs.ecmEnergy == 7000.0:
    TruthJetFilter.Njet=1;
    TruthJetFilter.NjetMinPt=25.*GeV;
    TruthJetFilter.NjetMaxEta=5;
    TruthJetFilter.jet_pt1=25.*GeV;
    TruthJetFilter.TruthJetContainer="AntiKt4TruthJets";
  if runArgs.ecmEnergy == 8000.0:
    TruthJetFilter.Njet=1;
    TruthJetFilter.NjetMinPt=25.*GeV;
    TruthJetFilter.NjetMaxEta=5;
    TruthJetFilter.jet_pt1=25.*GeV;
    TruthJetFilter.TruthJetContainer="AntiKt4TruthJets";
  if runArgs.ecmEnergy == 10000.0:
    TruthJetFilter.Njet=1;
    TruthJetFilter.NjetMinPt=30.*GeV;
    TruthJetFilter.NjetMaxEta=5;
    TruthJetFilter.jet_pt1=30.*GeV;
    TruthJetFilter.TruthJetContainer="Cone4TruthJets";
except NameError:
  pass

try:
     StreamEVGEN.RequireAlgs = [ "TruthJetFilter" ]
except Exception, e:
     pass


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.AlpgenEvgenConfig import evgenConfig
 
# inputfilebase
evgenConfig.inputfilebase = 'alpgen'
try:
  if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputfilebase = 'group09.phys-gener.alpgen.107715.ZnunuNp5_pt20_filt1jet_7tev.TXT.v2'
    evgenConfig.efficiency = 0.8982
  if runArgs.ecmEnergy == 8000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.alpgen.107715.ZnunuNp5_pt20_filt1jet_8tev.TXT.v1'
    evgenConfig.efficiency = 0.897
  if runArgs.ecmEnergy == 10000.0:
    evgenConfig.inputfilebase = 'alpgen.107715.ZnunuNp5.pt20.filt1jet.v3'
    evgenConfig.efficiency = 0.8950
except NameError:
  pass

evgenConfig.minevents=5000


# 7 TeV - Information on sample 107715
# 7 TeV - Filter efficiency  = 0.9987
# 7 TeV - MLM matching efficiency = 0.117
# 7 TeV - Number of Matrix Elements in input file  = 5000
# 7 TeV - Alpgen cross section = 37.716 pb
# 7 TeV - Herwig cross section = Alpgen cross section * eff(MLM) = 4.423 pb
# 7 TeV - Cross section after filtering = 4.417 pb
# 7 TeV - Lumi/500 events post-filter = 500/eff(Filter)/XS(jimmy) = 113.19 pb-1
# 7 TeV - Filter efficiency estimate below reduced by 10% to produce 555 events on average,
# 7 TeV - of which only 500 will be used in further processing
#EXTENSION		
# 7 TeV - Information on sample 	107715	
# 7 TeV - Filter efficiency  = 	0.9987	
# 7 TeV - MLM matching efficiency = 	0.12	
# 7 TeV - Number of Matrix Elements in input file  = 	5000	
# 7 TeV - Alpgen cross section = 	35.0	 pb
# 7 TeV - Herwig cross section = Alpgen cross section * eff(MLM) = 	4.1	 pb
# 7 TeV - Cross section after filtering = 	4.1	 pb
# 7 TeV - Lumi/500 events post-filter = 500/eff(Filter)/XS(jimmy) = 	122.36	 pb-1
#		
# 7 TeV - Filter efficiency estimate below reduced by 10% to produce 555 events on average,		
# 7 TeV - of which only 500 will be used in further processing		
# 8 TeV - Information on sample 	107715
# 8 TeV - Filter efficiency  = 	0.9987
# 8 TeV - MLM matching efficiency = 	0.10
# 8 TeV - Number of Matrix Elements in input file  = 	5700
# 8 TeV - Alpgen cross section = 	51.1	 pb
# 8 TeV - Herwig cross section = Alpgen cross section * eff(MLM) = 	4.9	 pb
# 8 TeV - Cross section after filtering = 	4.9	 pb
# 8 TeV - Lumi/500 events post-filter = 500/eff(Filter)/XS(jimmy) = 	102.83	 pb-1
#
# 8 TeV - Filter efficiency estimate below reduced by 10% to produce 5556 events on average,
# 8 TeV - of which only 5000 will be used in further processing		
evgenConfig.efficiency = 0.897
# 10 TeV - Information on sample 107715
# 10 TeV - Filter efficiency  = 0.994
# 10 TeV - MLM matching efficiency = 0.12
# 10 TeV - Number of Matrix Elements in input file  = 6500
# 10 TeV - Alpgen cross section = 80.3 pb
# 10 TeV - Herwig cross section = Alpgen cross section * eff(MLM) = 9.60 pb
# 10 TeV - Cross section after filtering = 9.55 pb
# 10 TeV - Lumi/500 events post-filter = 500/eff(Filter)/XS(jimmy) = 52.349 pb-1
# 10 TeV - Filter efficiency estimate below reduced by 10% to produce 555 events on average,
# 10 TeV - of which only 500 will be used in further processing
#==============================================================
#
# End of job options file
#
###############################################################
