###############################################################
#
# Job options file
#
# Pythia gg/qq->ttH->WbWbWW->lvb jjb lv jj, two like-sign lepton 
#                      (mH=165GeV)
#
# Responsible person
#   18 July, 2008: Huaqiao Zhang (Huaqiao.Zhang@cern.ch)
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 


# ... Pythia
include ( "MC10JobOptions/MC10_Pythia_Common.py" )
Pythia.PythiaCommand += [
                              "pysubs msel 0",
## process channel  
                              "pysubs msub 121 1",
                              "pysubs msub 122 1",
## higgs mass point setting
                              "pydat2 pmas 25 1 165.",
## higgs decay mode setting
                              "pydat3 mdme 210 1 0",
                              "pydat3 mdme 211 1 0",
                              "pydat3 mdme 212 1 0",
                              "pydat3 mdme 213 1 0",
                              "pydat3 mdme 214 1 0",
                              "pydat3 mdme 215 1 0",
                              "pydat3 mdme 218 1 0",
                              "pydat3 mdme 219 1 0",
                              "pydat3 mdme 220 1 0",
                              "pydat3 mdme 222 1 0",
                              "pydat3 mdme 223 1 0",
                              "pydat3 mdme 224 1 0",
                              "pydat3 mdme 225 1 0",
                              "pydat3 mdme 226 1 1",
## W decay mode setting
                              "pydat3 mdme 190 1 2",
                              "pydat3 mdme 191 1 2",
                              "pydat3 mdme 192 1 2",
                              "pydat3 mdme 194 1 2",
                              "pydat3 mdme 195 1 2",
                              "pydat3 mdme 196 1 2",
                              "pydat3 mdme 198 1 2",
                              "pydat3 mdme 199 1 2",
                              "pydat3 mdme 200 1 2",
                              "pydat3 mdme 206 1 3",
                              "pydat3 mdme 207 1 3",
                              "pydat3 mdme 208 1 0",
## other settings
                              "pydat3 mdcy 15 1 0",
                              "pydat1 parj 90 20000" ]


# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )


# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.90
#==============================================================
#
# End of job options file
#
###############################################################
