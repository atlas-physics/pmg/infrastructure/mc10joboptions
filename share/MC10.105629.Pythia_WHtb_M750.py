# $Id: MC10.105629.Pythia_WHtb_M750.py,v 1.1 2008-10-02 22:01:02 osamu Exp $
#
# Job options to produce W' events with Pythia.
#

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

Pythia.PythiaCommand += [
    "pysubs msel 0",
    "pysubs msub 142 1",       # f+fbar -> W'
    "pydat2 pmas 34 1 750.",   # W' mass 
    # W' decays  to quarks
    "pydat3 mdme 311 1 0",
    "pydat3 mdme 312 1 0",
    "pydat3 mdme 313 1 0",
    "pydat3 mdme 315 1 0",
    "pydat3 mdme 316 1 0",
    "pydat3 mdme 317 1 0",
    "pydat3 mdme 319 1 0",
    "pydat3 mdme 320 1 0",
    "pydat3 mdme 321 1 1",    # bbar+t
    "pydat3 mdme 322 1 0",
    "pydat3 mdme 323 1 0",
    "pydat3 mdme 324 1 0",
    "pydat3 mdme 325 1 0",
    "pydat3 mdme 326 1 0",
    "pydat3 mdme 330 1 0",
    "pydat3 mdme 331 1 0",
    "pydat3 mdme 332 1 0",
    "pydat3 mdme 333 1 0",
    # W' decay to e nu - mu nu - tau nu 
    "pydat3 mdme 327 1 0",
    "pydat3 mdme 328 1 0",
    "pydat3 mdme 329 1 0",
    # W decay
    "pydat3 mdme 206 1 1",   # W -> e nu  
    "pydat3 mdme 207 1 1",   # W -> mu nu 
    "pydat3 mdme 208 1 1" # W -> tau nu 
#,  
#    # set beam energy:
#    "pyinit win 10000.",
#    # print some output: 
#    "pyinit pylistf 2",
#    "pyinit dumpr 1 5",
#    "pystat 1 2 3 4"
    ]

#theApp.TopAlg += [ "TestHepMC" ]
#TestHepMC = Algorithm( "TestHepMC" )
#TestHepMC.BeamEnergy = 10000000.

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )
# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9

#evgenConfig.minevents = 100.
#evgenConfig.maxeventsfactor = 500.
