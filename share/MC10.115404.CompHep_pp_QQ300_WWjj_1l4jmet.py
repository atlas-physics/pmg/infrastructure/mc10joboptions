###############################################################
#
# Job options file
#
#-----------------------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_PythiaMC09p_Common.py" )

#--------------------------------------------------------------
# Algorithms Private Options
#--------------------------------------------------------------

Pythia.PythiaCommand+= ["pyinit user comphep",
                        "pyinit pylistf 1",
                        "pyinit pylisti 12",
                        "pyinit dumpr 1 12",
                        "pydat3 mdcy 15 1 0",
#                        "pysubs msub 1 1",        # Create Z bosons.
#                        "pydat3 mdme 174 1 1",
#                        "pydat3 mdme 175 1 1",
#                        "pydat3 mdme 176 1 1",
#                        "pydat3 mdme 177 1 1",
#                        "pydat3 mdme 178 1 1",
#                        "pydat3 mdme 179 1 0",
#                        "pydat3 mdme 182 1 0",    # Switch for Z->ee.
#                        "pydat3 mdme 183 1 0",
#                        "pydat3 mdme 184 1 0",    # Switch for Z->mumu.
#                        "pydat3 mdme 185 1 0",
#                        "pydat3 mdme 186 1 0",    # Switch for Z->tautau.
#                        "pydat3 mdme 187 1 0",
## W production:
                        "pysubs msub 2 1",        # Create W bosons.
                        "pydat3 mdme 190 1 1",
                        "pydat3 mdme 191 1 1",
                        "pydat3 mdme 192 1 1",
                        "pydat3 mdme 194 1 1",
                        "pydat3 mdme 195 1 1",
                        "pydat3 mdme 196 1 1",
                        "pydat3 mdme 198 1 1",
                        "pydat3 mdme 199 1 1",
                        "pydat3 mdme 200 1 1",
                        "pydat3 mdme 206 1 1",    # Switch for W->enu.
                        "pydat3 mdme 207 1 1",    # Switch for W->munu.
                        "pydat3 mdme 208 1 1",
                        "pydat1 parj 90 20000"]

#--------------------------------------------------------------
# Tuning for Pythia 6.4
#-------------------------------------------------------------

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )
# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.CompHepEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'group10.phys-gener.CompHep.115404.pp_QQ300_WWjj_1l4jmet.TXT.v1'
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 5000
#
# End of job options file
#
###############################################################
