###############################################################
#
#       jobOptions for production of Upsilon(2S) resonance
#               in NRQCD colour-octet framework
#
# Upsilon(2S)->mu0mu0 channel only: no feeddown to U(1S)
#
# Author: Darren D Price ( Darren.Price@cern.ch )
# Date:   Aug 2010
#==============================================================
#--------------------------------------------------------------
# General Application Configuration options
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

from GeneratorFilters.GeneratorFiltersConf import BSignalFilter
topAlg += BSignalFilter()

Pythia.PythiaCommand += [ # quarkonium processes
    
    "pysubs msel 62", # colour octet bottomonium production (461-479)
                            
    # ----- Upsilon production -----
    # --- 3S1(1)
    #"pysubs msub 461 1",  # gg->bb~[3S1(1)]+g
    # --- 3S1(8)
    #"pysubs msub 462 1",  # gg->bb~[3S1(8)]+g
    #"pysubs msub 465 1",  # gq->q+bb~[3S1(8)]
    #"pysubs msub 468 1",  # qq~->g+bb~[3S1(8)]
    # --- 1S0(8)
    #"pysubs msub 463 1",  # gg->bb~[1S0(8)]+g
    #"pysubs msub 466 1",  # gq->q+bb~[1S0(8)]
    #"pysubs msub 469 1",  # qq~->g+bb~[1S0(8)]
    # --- 3PJ(8)
    #"pysubs msub 464 1",  # gg->bb~[3PJ(8)]+g
    #"pysubs msub 467 1",  # gq->q+bb~[3PJ(8)]
    #"pysubs msub 470 1",  # qq~->g+bb~[3PJ(8)]

    # ----- Chi's -----
    #"pysubs msub 471 1",  # gg->bb~[3P0(1)]+g
    #"pysubs msub 472 1",  # gg->bb~[3P1(1)]+g
    #"pysubs msub 473 1",  # gg->bb~[3P2(1)]+g
    #"pysubs msub 474 1",  # qg->q+bb~[3P0(1)]
    #"pysubs msub 475 1",  # qg->q+bb~[3P1(1)]
    #"pysubs msub 476 1",  # qg->q+bb~[3P2(1)]
    #"pysubs msub 477 1",  # qq~->bb~[3P0(1)]+g
    #"pysubs msub 478 1",  # qq~->bb~[3P1(1)]+g
    #"pysubs msub 479 1",  # qq~->bb~[3P2(1)]+g
    
    ]                        

Pythia.PythiaCommand += [ # make Ups(1S) into Ups(2S)
     "pydat2 pmas 553 1 10.023",
     "pydat2 pmas 10551 1 10.233",
     "pydat2 pmas 20553 1 10.256",
     "pydat2 pmas 555 1 10.269",
     "pydat2 pmas 9900551 1 10.5",
     "pydat2 pmas 9900553 1 10.5",
     "pydat2 pmas 9910551 1 10.5",
     ]

Pythia.PythiaCommand += [ # force decays

    "pydat3 mdme 858 1 0", # J/psi->e+e-
    "pydat3 mdme 859 1 1", # J/psi->mumu 
    "pydat3 mdme 860 1 0",  # J/psi->rndmflavpairs

    "pydat3 mdme 1501 1 1", # chi0c->J/psi gamma 
    "pydat3 mdme 1502 1 0", # chi0c->rfp 
    
    "pydat3 mdme 1555 1 1", # chi1c->J/psi gamma 
    "pydat3 mdme 1556 1 0", # chi1c->rfp
    
    "pydat3 mdme 861 1 1", # chi2c->J/psi gamma 
    "pydat3 mdme 862 1 0", # chi2c->rfp

    "pydat3 mdme 1034 1 0", # Upsilon->e+e- 
    "pydat3 mdme 1035 1 1", # Upsilon->mu+mu- 
    "pydat3 mdme 1036 1 0", # Upsilon->tau+tau-
    "pydat3 mdme 1037 1 0", # Upsilon->ddbar
    "pydat3 mdme 1038 1 0", # Upsilon->uubar
    "pydat3 mdme 1039 1 0", # Upsilon->ssbar
    "pydat3 mdme 1040 1 0", # Upsilon->ccbar
    "pydat3 mdme 1041 1 0", # Upsilon->ggg
    "pydat3 mdme 1042 1 0", # Upsilon->gamma gg
    
    "pydat3 mdme 1520 1 1", # chi0b->Upsilon gamma
    "pydat3 mdme 1521 1 0", # chi0b->gg
    
    "pydat3 mdme 1565 1 1", # chi1b->Upsilon gamma
    "pydat3 mdme 1566 1 0", # chi1b->gg
    
    "pydat3 mdme 1043 1 1", # chi2b->Upsilon gamma
    "pydat3 mdme 1044 1 0", # chi2b->gg
    
    ]

Pythia.PythiaCommand += [ # NRQCD matrix elements
    
    "pypars parp 146 4.63",   # Ups'-3S1(1) NRQCD ME
    "pypars parp 147 0.055",   # Ups'-3S1(8) NRQCD ME
    "pypars parp 148 0.008",   # Ups'-1S0(8) NRQCD ME
    "pypars parp 149 0.008",   # Ups'-3P0(8) NRQCD ME / m_b^2 
    "pypars parp 150 0.103",  # chi'_b0-3P0(1) NRQCD ME / m_b^2
        
    ]

Pythia.PythiaCommand += [

    "pysubs ckin 3 1.",   # lower pT cut on hard process in GeV
    ]

#------- Muon Trigger Cuts --------
BSignalFilter = topAlg.BSignalFilter
#-------------- Level 1 Muon Cuts --------------------- 
BSignalFilter.LVL1MuonCutOn = True
BSignalFilter.LVL1MuonCutPT = 0.0 
BSignalFilter.LVL1MuonCutEta = 10.
#-------------- Level 2 lepton cuts -------------------
# These will only function if LVL1 trigger used. 
BSignalFilter.LVL2MuonCutOn = True 
BSignalFilter.LVL2MuonCutPT = 0.0 
BSignalFilter.LVL2MuonCutEta = 10.

try:
     StreamEVGEN.RequireAlgs += ["BSignalFilter"]
except Exception, e:
     pass

from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.minevents = 5000
#==============================================================
#
# End of job options file
#
###############################################################

