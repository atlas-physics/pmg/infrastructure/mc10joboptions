###############################################################
#
# Job options file
# Daniel Geerts
#
#==============================================================
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 3

#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------

# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 

try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common.py" )
  if runArgs.ecmEnergy == 14000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_14TeV.py" )
except NameError:
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig 


Herwig.HerwigCommand += [ "iproc alpgen",
                          "taudec TAUOLA",
                        ]
# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from MC10JobOptions.AlpgenEvgenConfig import evgenConfig
 

# dummy needed
evgenConfig.inputfilebase = 'alpgen'
try:
  if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.alpgen.107280.WbbFullNp0_pt20_7tev.TXT.v2'
  if runArgs.ecmEnergy == 8000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.alpgen.107280.WbbFullNp0_pt20_8tev.TXT.v1'
except NameError:
  pass
 
# 7 TeV - Information on sample 107280
# 7 TeV - Filter efficiency  = 1.0000
# 7 TeV - MLM matching efficiency = 0.87
# 7 TeV - Number of Matrix Elements in input file  = 800
# 7 TeV - Alpgen cross section = 52.4 pb
# 7 TeV - Herwig cross section = Alpgen cross section * eff(MLM) = 45.6 pb
# 7 TeV - Cross section after filtering = 45.6 pb
# 7 TeV - Lumi/500 events post-filter = 500/eff(Filter)/XS(jimmy) = 10.97 pb-1
#
# 7 TeV - Filter efficiency estimate below reduced by 10% to produce 555 events on average,
# 7 TeV - of which only 500 will be used in further processing
#EXT:
# 7 TeV - Information on sample 	107280	
# 7 TeV - Filter efficiency  = 	1.0000	
# 7 TeV - MLM matching efficiency = 	0.87	
# 7 TeV - Number of Matrix Elements in input file  = 	7500	
# 7 TeV - Alpgen cross section = 	52.4	 pb
# 7 TeV - Herwig cross section = Alpgen cross section * eff(MLM) = 	45.6	 pb
# 7 TeV - Cross section after filtering = 	45.6	 pb
# 7 TeV - Lumi/5000 events post-filter = 5000/eff(Filter)/XS(jimmy) = 	109.64	 pb-1
#		
# 7 TeV - Filter efficiency estimate below reduced by 10% to produce 5556 events on average,		
# 7 TeV - of which only 5000 will be used in further processing		
# 8 TeV - Information on sample 	107280
# 8 TeV - Filter efficiency  = 	1.0000
# 8 TeV - MLM matching efficiency = 	0.86
# 8 TeV - Number of Matrix Elements in input file  = 	800
# 8 TeV - Alpgen cross section = 	62.0	 pb
# 8 TeV - Herwig cross section = Alpgen cross section * eff(MLM) = 	53.6	 pb
# 8 TeV - Cross section after filtering = 	53.6	 pb
# 8 TeV - Lumi/500 events post-filter = 500/eff(Filter)/XS(jimmy) = 	9.33	 pb-1
#
# 8 TeV - Filter efficiency estimate below reduced by 10% to produce 5556 events on average,
# 8 TeV - of which only 5000 will be used in further processing	
evgenConfig.efficiency = 0.90000
evgenConfig.minevents = 5000
#==============================================================
#
# End of job options file
#
###############################################################
