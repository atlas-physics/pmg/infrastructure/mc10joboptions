#-------------------------------------------------------------
# Pythia photon+jet w/ p_T(photon) > 35 GeV
# Prepared by J. Tojo, December 2009
# Adapted for Pythia8 by R. Prabhu (prabhu@cern.ch), April 2011 
#-------------------------------------------------------------
# ... Main generator: Pythia8

MessageSvc = Service( "MessageSvc" )
#MessageSvc.OutputLevel = 4

from AthenaCommon.AppMgr import ServiceMgr as svcMgr

import AthenaServices

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ("MC10JobOptions/MC10_Pythia8_Common.py")

Pythia8.Commands += ["PromptPhoton:qg2qgamma = on"]
Pythia8.Commands += ["PromptPhoton:qqbar2ggamma = on"]
Pythia8.Commands += ["PromptPhoton:gg2ggamma = on"]
Pythia8.Commands += ["PhaseSpace:pTHatMin = 33."]

#-------------------------------------------------------------
# Filter
#-------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import PhotonFilter
topAlg += PhotonFilter()

PhotonFilter = topAlg.PhotonFilter
PhotonFilter.Ptcut = 35000.
PhotonFilter.Etacut = 2.7
PhotonFilter.NPhotons = 1

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------

try:
     StreamEVGEN.RequireAlgs +=  [ "PhotonFilter" ]
except Exception, e:
     pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
# efficiency = 5000/8271*0.9 = 0.544 at 7 TeV
from MC10JobOptions.Pythia8EvgenConfig import evgenConfig
evgenConfig.efficiency = 0.544
