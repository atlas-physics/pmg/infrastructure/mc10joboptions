###############################################################
#
# Job options file for generation of B events 
# no decay channel is specified.
# Only events containing at least two muons 
# with pT>2.5GeV |eta|<2.5 are written to output
# Selection criteria can be changed by datacards
#==============================================================
from MC10JobOptions.PythiaBEvgenConfig import evgenConfig
#evgenConfig.minevents = 5000
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_PythiaB_Common.py" )

#--------------------------------------------------------------				 
#              PARAMETERS  SPECIFIC  TO   PYTHIABMODULE
#--------------------------------------------------------------
PythiaB.ForceBDecay = "no"
PythiaB.ForceCDecay = "no"

include( "MC10JobOptions/MC10_PythiaB_Bchannels.py" )
include( "MC10JobOptions/MC10_PythiaB_Btune.py" )

PythiaB.PythiaCommand += ["pysubs ckin 3 30.",
				 "pysubs msel 1"]
#--------------------------------------------------------------
# -------------  DEFINE SELECTION CUTS  -------------
#--------------------------------------------------------------				
PythiaB.flavour =  5.				
#  ------------- Selections on b  quarks   -------------
PythiaB.cutbq = ["0. 4.5 or 0. 4.5"]
#  ------------- LVL1 muon cuts 0=OFF 1=ON -------------
#PythiaB.lvl1cut = [ 1.,  0.0, 2.7]
#  ------------- LVL2 muon/electron cuts  0=OFF 1=ON-------------
#PythiaB.lvl2cut = [ 1.,  13.,     0.0,   2.7]
#  ------------- Offline cuts 0=OFF 1=ON -------------
#PythiaB.offcut = [ 0., 0., 102.5, 0., 102.5, 0., 102.5]
#  ------------- Number of repeated hadronization mhadr -------------
PythiaB.mhadr =  1. 

# add filters

from GeneratorFilters.GeneratorFiltersConf import ParticleFilter

PartFiltMu=ParticleFilter(name = "PartFiltMu",
                                    StatusReq=1,
                                    PDG=13,
                                    Ptcut = 15.*GeV,
                                    Etacut = 3.0,
                                    Energycut = 1000000000.0)

PartFiltPi=ParticleFilter(name = "PartFiltPi",
                                    StatusReq=1,
                                    PDG=211,
                                    Ptcut = 15.*GeV,
                                    Etacut = 3.0,
                                    Energycut = 1000000000.0)

PartFiltK=ParticleFilter(name = "PartFiltK",
                                    StatusReq=1,
                                    PDG=321,
                                    Ptcut = 15.*GeV,
                                    Etacut = 3.0,
                                    Energycut = 1000000000.0)

topAlg += PartFiltMu
topAlg += PartFiltPi
topAlg += PartFiltK

try:
     StreamEVGEN.AcceptAlgs += [ "PartFiltMu" ]
     StreamEVGEN.AcceptAlgs += [ "PartFiltPi" ]
     StreamEVGEN.AcceptAlgs += [ "PartFiltK" ]
except Exception, e:
     pass


#==============================================================
#
# End of job options file
#
###############################################################
