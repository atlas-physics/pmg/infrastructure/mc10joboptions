###############################################################
#
# Job options file for POWHEG with Pythia
# U. Husemann, C. Wasicki, Nov. 2009 / Feb. 2010
#
# 4vec was generated with Powheg-BOX/gg_H for 7TeV (ver1.0) (J.Tanaka)
#
#==============================================================
#--------------------------------------------------------------
# General Application Configuration options
#--------------------------------------------------------------
import AthenaCommon.AtlasUnixGeneratorJob

from AthenaCommon.AppMgr import theApp
from AthenaCommon.AppMgr import ServiceMgr

# make sure we are loading the ParticleProperty service
from PartPropSvc.PartPropSvcConf import PartPropSvc
ServiceMgr += PartPropSvc()

#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
ServiceMgr.MessageSvc.OutputLevel = INFO

from AthenaCommon.AlgSequence import AlgSequence
topAlg=AlgSequence()

# ... Main generator : Pythia
include( "MC10JobOptions/MC10_PowHegPythiaPerugia2011_Common.py")

Pythia.PythiaCommand += [   "pysubs ckin 45 2.",
                            "pysubs ckin 47 2.",
                            "pydat3 mdme 190 1 0", # W decay
                            "pydat3 mdme 191 1 0",
                            "pydat3 mdme 192 1 0",
                            "pydat3 mdme 194 1 0",
                            "pydat3 mdme 195 1 0",
                            "pydat3 mdme 196 1 0",
                            "pydat3 mdme 198 1 0",
                            "pydat3 mdme 199 1 0",
                            "pydat3 mdme 200 1 0",
                            "pydat3 mdme 206 1 1",
                            "pydat3 mdme 207 1 1",
                            "pydat3 mdme 208 1 1",
                            "pydat3 mdme 210 1 0", # Higgs decay
                            "pydat3 mdme 211 1 0",
                            "pydat3 mdme 212 1 0",
                            "pydat3 mdme 213 1 0",
                            "pydat3 mdme 214 1 0",
                            "pydat3 mdme 215 1 0",
                            "pydat3 mdme 218 1 0",
                            "pydat3 mdme 219 1 0",
                            "pydat3 mdme 220 1 0",
                            "pydat3 mdme 222 1 0",
                            "pydat3 mdme 223 1 0",
                            "pydat3 mdme 224 1 0",
                            "pydat3 mdme 225 1 0",
                            "pydat3 mdme 226 1 1" ]

# ... Tauola
Pythia.PythiaCommand += [ "pydat3 mdcy 15 1 0"] # turn off tau decays
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
Pythia.PythiaCommand += ["pydat1 parj 90 20000"] # turn off FSR for Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

from MC10JobOptions.EvgenConfig import evgenConfig, knownGenerators

evgenConfig.generators += [ "Lhef", "Pythia" ]

#dummy needed
evgenConfig.inputfilebase = 'powheg'
try:
  if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.powhegv1.116710.ggH_SM_M170_7TeV.TXT.v1'
except NameError:
  pass

evgenConfig.efficiency = 0.9
#==============================================================
#
# End of job options file
#
###############################################################
