#Reyhaneh Rezvani , 22 Jan 2011

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")
include ( "MC10JobOptions/MC10_Pythia_Common.py" )
	
Pythia.PythiaCommand += ["pydat1 parj 90 20000.",      ## Turn off FSR
	                 "pydat3 mdcy 15 1 0"] ## Turn off tau decays
Pythia.PythiaCommand +=["pyinit user exograviton"]
Pythia.PythiaCommand += ["grav 1 2","grav 2 1111",
                         "grav 3 7000","grav 4 3000",
	                 "grav 5 80","grav 6 3000",
	                 "grav 7 5E+5"]
	

#####################################################################
# This is to decrease both ISR & FSR:
#####################################################################
Pythia.PythiaCommand += [ "pypars mstp 3 1",      #set mstp 3 1 (setting Lambda in alpha_s)
                          "pypars parp 1 0.192",  #reset the defaults after mstp 3 1
                          "pydat1 paru 112 0.192",#reset defaults after mstp 3 1
                          "pydat1 mstu 112 4",    #reset defaults after mstp 3 1
                          "pypars parp 61 0.192"] #reset defaults after mstp 3 1
	
#--------------------------------------------------------
# decrease ISR activity
#--------------------------------------------------------
# ISR starting scale:
Pythia.PythiaCommand +=[ "pypars parp 67 0.5" ]
# ~ renorm. scale used for ISR
Pythia.PythiaCommand +=[ "pypars parp 64 4.0" ]
#--------------------------------------------------------
# decrease FSR activity:
#--------------------------------------------------------
# Labmda value in running alpha_s (ATLAS def 0.192)
Pythia.PythiaCommand +=[ "pypars parp 72 0.096" ]
# FSR cutoff
Pythia.PythiaCommand +=[ "pydat1 parj 82 2.0" ]
###################################################################
###################################################################


include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )
	
	
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9

