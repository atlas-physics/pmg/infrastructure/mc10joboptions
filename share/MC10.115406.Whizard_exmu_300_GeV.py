#---------------------------------------------------------------
# Job options file
# p p -> mu, mu, gama (mass exmu = 300 GeV)
# Responsible person: Piyali Banerjee
#----------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_PythiaMC09p_Common.py" )

# Channel switches
mugama = "1"
muZ = "0"
munuW = "0" 

Pythia.PythiaCommand+= ["pyinit user lhef",
                        "pysubs msel 0",
                        "pydat2 pmas 4000013 1 300.0",
                        "pytcsm rtcm 41 300.",   # lambda scale
                        "pypars mstp 32 4",
                        
                        "pydat3 mdme 7998 1 "+mugama,
                        "pydat3 mdme 7999 0 "+muZ,
                        "pydat3 mdme 8000 0 "+munuW,
                        
                        "pydat3 mdcy 15 1 0",
                        "pydat1 parj 90 20000",
                        "pyinit pylistf 1",
                        "pyinit dumpr 1 10",
                        "pyinit pylisti 12"
                        ]

# Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.LhefEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
# dummy needed
evgenConfig.inputfilebase = 'Whizard'
try:
  if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputfilebase = "group10.phys-gener.Whizard.115406.exmu_300_GeV.TXT.v1"
  if runArgs.ecmEnergy == 8000.0:
    evgenConfig.inputfilebase = "group10.phys-gener.Whizard.115406.exmu_300_GeV_8TeV.TXT.v1"
except NameError:
  pass
