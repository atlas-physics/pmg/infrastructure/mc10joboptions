## Job options file for PowHeg W- ->taunu, inclusive tau decays
## Responsible: Martin Flechl
## Created: 31/3/2011
## Based on JobO for dataset 108291

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

# ... Main generator : Herwig
try:
    if runArgs.ecmEnergy == 7000.0:
        include ( "MC10JobOptions/MC10_PowHegJimmy_Common_7TeV.py" )
    if runArgs.ecmEnergy == 8000.0:
        include ( "MC10JobOptions/MC10_PowHegJimmy_Common_8TeV.py" )
    if runArgs.ecmEnergy == 10000.0:
        include ( "MC10JobOptions/MC10_PowHegJimmy_Common.py" )
    if runArgs.ecmEnergy == 14000.0:
        include ( "MC10JobOptions/MC10_PowHegJimmy_Common_14TeV.py" )
except NameError:
    # needed (dummy) default
    from Herwig_i.Herwig_iConf import Herwig
    topAlg += Herwig()
    Herwig = topAlg.Herwig
        
Herwig.HerwigCommand += [ "taudec TAUOLA"]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

from MC10JobOptions.EvgenConfig  import evgenConfig, knownGenerators
evgenConfig.generators += ["Lhef", "Herwig"]

#same as for Pythia (107391)!
evgenConfig.inputfilebase = 'group10.phys-gener.PowHegBoxV1.107391.Wmintaunu_7TeV.TXT.v1'
evgenConfig.efficiency = 0.9
evgenConfig.minevents  = 5000

#
# End of job options file
#
###############################################################
