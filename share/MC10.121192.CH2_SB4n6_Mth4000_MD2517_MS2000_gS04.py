###############################################################
# Job options for generating events with Pythia based on 
# an event file created with Charybdis2
# 10/2009 J. Frost <james.frost@cern.ch>
#
#--------------------------------------------------------------
# Private Application Configuration option
#--------------------------------------------------------------

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_PythiaCT66_Common.py" )

Pythia.PythiaCommand = [	
			"pyinit user lhef",		# set the interface
                        #"pydat3 mdcy 15 1 0",          # Do tau decays in Pythia 
                        "pydat1 parj 90 20000",         # Use Photos
			]

# Tauola
#include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

### Tauola doesn't get tau polarisation for decay from LHEF in Pythia

# Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# TestHepMC setup
#--------------------------------------------------------------
#avoid warnings due to energy imbalance (intended feature from Charybdis2)
from TruthExamples.TruthExamplesConf import TestHepMC
TestHepMC.EnergyDifference = 100000000.
TestHepMC.OutputLevel = DEBUG

from MC10JobOptions.LhefEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
evgenConfig.inputfilebase = "group09.phys-gener.Charybdis2.121192.SB4_CH_n6_Mth4000_MD2517_MS2000_gS0_4.TXT.v1"

