###############################################################
#
# Job options file
# prepared by Zhijun Liang Haiping Peng in Rel. 15.6.1.7 (March 2010)
# Acceptance sample
# Z (mu mu)+ Gamma with full interference between ISR and FSR digram with MadGraph/Pythia
# with Z mass >40GeV

# Non-filtered cross section in Rel. 15.6.1 : 10.4 pb
# filter efficiency  100%  safe factor 0.9
#
#==============================================================

# ... Main generator : Pythia
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

# for CTEQ6L1
include ( "MC10JobOptions/MC10_PythiaMC09p_Common.py" )

Pythia.PythiaCommand += [ "pyinit user lhef",
                          "pyinit pylisti -1","pyinit pylistf 1","pyinit dumpr 1 2",
                          "pydat1 parj 90 20000", # Turn off FSR.
                          "pydat3 mdcy 15 1 0"   # Turn off tau decays.
]


## ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )


## ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )



from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.generators += ["Lhef", "Pythia"]
evgenConfig.inputfilebase = 'group09.phys-gener.Baur001Pythia.113199.Zmumugamma.TXT.v1'
evgenConfig.efficiency = 0.9


#==============================================================
#
# End of job options file
#
###############################################################

