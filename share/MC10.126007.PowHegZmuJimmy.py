###############################################################
#
#
#  POWHEG  WZ NLO production
#  
#  Z^- ->mu mu interfaced to HERWIG
#
#  use same events as for 113905
#
#==============================================================

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")


# ... Main generator : Herwig
try:
    if runArgs.ecmEnergy == 7000.0:
        include ( "MC10JobOptions/MC10_PowHegJimmy_Common_7TeV.py" )
    if runArgs.ecmEnergy == 8000.0:
        include ( "MC10JobOptions/MC10_PowHegJimmy_Common_8TeV.py" )
    if runArgs.ecmEnergy == 10000.0:
        include ( "MC10JobOptions/MC10_PowHegJimmy_Common.py" )
    if runArgs.ecmEnergy == 14000.0:
        include ( "MC10JobOptions/MC10_PowHegJimmy_Common_14TeV.py" )
except NameError:
    # needed (dummy) default
    from Herwig_i.Herwig_iConf import Herwig
    topAlg += Herwig()
    Herwig = topAlg.Herwig
                
Herwig.HerwigCommand += [ "taudec TAUOLA"]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
#
#from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
#topAlg += LeptonFilter()
#
#LeptonFilter = topAlg.LeptonFilter
#LeptonFilter.Ptcut = 10000.
#LeptonFilter.Etacut = 2.7
#
#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------
# 
#try:
#  StreamEVGEN.RequireAlgs += [ 'LeptonFilter' ]
#except Exception, e:
#  pass
# 
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
#
from MC10JobOptions.EvgenConfig  import evgenConfig, knownGenerators
evgenConfig.generators += ["Lhef", "Herwig"]

#input created using POWHEG-BOX v1
evgenConfig.inputfilebase = 'PowHeg'
try:
  if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.PowHegBoxV1.113905.Zmu_7TeV.TXT.v3'
  if runArgs.ecmEnergy == 8000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.PowHegBoxV1.113905.Zmu_8TeV.TXT.v1'
  if runArgs.ecmEnergy == 10000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.PowHegBoxV1.113905.Zmu_10TeV.TXT.v1'
  if runArgs.ecmEnergy == 14000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.PowHegBoxV1.113905.Zmu_14TeV.TXT.v1'
except NameError:
  pass

evgenConfig.efficiency = 0.9
evgenConfig.minevents  = 5000

#
# End of job options file
#
###############################################################
