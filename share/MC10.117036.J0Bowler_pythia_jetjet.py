# dijet production with pythia
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 


include ( "MC10JobOptions/MC10_Pythia_Common.py" )

#--------------------------------------------------------------
# File prepared by Ian Hinchliffe Nov 2005
#--------------------------------------------------------------
Pythia.PythiaCommand += [
			      "pysubs msel 0",
			      "pysubs ckin 3 8.",
			      "pysubs ckin 4 17.",
			      "pysubs msub 11 1",
			      "pysubs msub 12 1",
			      "pysubs msub 13 1",
			      "pysubs msub 68 1",
			      "pysubs msub 28 1",
			      "pysubs msub 53 1",

# use Lund-Bowler modified for c and b fragmentation

                              "pydat1 mstj 11 4",       # frag. Lund-Bowler for c && b fragmentation
                              "pydat1 parj 41 0.85",    # Lund-Bowler a
                              "pydat1 parj 42 1.03",    # Lund-Bowler b
                              "pydat1 parj 46 0.85"  ]  # Lund-Bowler rQ

                              
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
