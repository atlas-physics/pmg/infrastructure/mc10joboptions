###############################################################
#
# Job options file
# Developed by Pavel Staroba from CSC.006904.PythiaZeeJet.py
# in Rel. 14.2.0.1 (July 2008)
#
#==============================================================
# ... Main generator : Pythia
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

# ISR  systematics sample: more hard ISR activity, external LO process + PYTHIA
# PARP(67) and PARP(64) are varied                                                                             
# MC09 default values: PARP(67)=4., PARP(64)=1.                                                                
# PARP(67): controls suppression of ISR branchings above the coherence scale                                
#           PARP(67) was introduced in 6.4.19 for the new shower, see Pythia update notes for more info     
# PARP(64): multiplies ISR alpha_strong evolution scale, the effect is \propto 1/(lambda_ISR^2)             
#           see Pythia Manual for more info
Pythia.PythiaCommand +=[ "pypars parp 67 6.0" ]
Pythia.PythiaCommand +=[ "pypars parp 64 0.25" ]

Pythia.PythiaCommand += [ "pysubs msel 13",
                         "pydat1 parj 90 20000", # Turn off FSR.
                         "pydat3 mdcy 15 1 0",   # Turn off tau decays.
                         "pysubs ckin 3 10.0",   # Lower P_T for hard 2 ->2 process.
                         "pysubs ckin 41 40.0",  # Lower Z invariant mass.
                         "pysubs ckin 43 40.0",  # Lower Z invariant mass.
                         "pydat3 mdme 174 1 0",
                         "pydat3 mdme 175 1 0",
                         "pydat3 mdme 176 1 0",
                         "pydat3 mdme 177 1 0",
                         "pydat3 mdme 178 1 0",
                         "pydat3 mdme 179 1 0",
                         "pydat3 mdme 182 1 0",    # Switch for Z->ee.
                         "pydat3 mdme 183 1 0",
                         "pydat3 mdme 184 1 1",    # Switch for Z->mumu.
                         "pydat3 mdme 185 1 0",
                         "pydat3 mdme 186 1 0",    # Switch for Z->tautau.
                         "pydat3 mdme 187 1 0"
                         ]

# ... UE

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
# efficiency = 0.9 - no filtering
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.90
#==============================================================
#
# End of job options file
#
###############################################################
