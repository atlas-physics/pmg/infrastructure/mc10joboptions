################################################################
#
# Pythia ZZ->all with 1lepEF. (mZ>10GeV)
#
# Responsible person(s)
#   Jun 30, 2011 : Junichi.Tanaka@cern.ch
#
################################################################
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

# ... Pythia
include ( "MC10JobOptions/MC10_Pythia_Common.py" )
Pythia.PythiaCommand += [
     "pysubs msel 0",
     "pysubs msub 22 1",
     "pysubs ckin 41 10.",
     "pysubs ckin 43 10.",
     "pydat3 mdme 174 1 1",
     "pydat3 mdme 175 1 1",
     "pydat3 mdme 176 1 1",
     "pydat3 mdme 177 1 1",
     "pydat3 mdme 178 1 1",
     "pydat3 mdme 179 1 1",
     "pydat3 mdme 180 1 0",
     "pydat3 mdme 181 1 0",
     "pydat3 mdme 182 1 1",
     "pydat3 mdme 183 1 1",
     "pydat3 mdme 184 1 1",
     "pydat3 mdme 185 1 1",
     "pydat3 mdme 186 1 1",
     "pydat3 mdme 187 1 1",
     "pydat3 mdme 188 1 0",
     "pydat3 mdme 189 1 0",
     "pydat1 parj 90 20000.",
     "pydat3 mdcy 15 1 0"
     ]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

# Add the filters:
# Electron or Muon filter
from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
topAlg += LeptonFilter()

LeptonFilter = topAlg.LeptonFilter
LeptonFilter.Ptcut = 15.*GeV
LeptonFilter.Etacut = 5.0

try:
    StreamEVGEN.RequireAlgs += [ "LeptonFilter" ]
except Exception, e:
    pass
              
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.1878
#==============================================================
#
# End of job options file
#
###############################################################
