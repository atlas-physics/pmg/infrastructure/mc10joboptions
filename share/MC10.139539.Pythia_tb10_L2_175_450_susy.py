#-------------------------------------------------------------------------
# benchmark point provided by Michelangelo Mangano
# contact :  M-H. Genest
#
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

Pythia.SusyInputFile = "susy_tb10_L2_175_450_slha.txt";
Pythia.PythiaCommand += [ "pydat1 parj 90 20000", "pydat3 mdcy 15 1 0", "pysubs msel 39", "pymssm imss 1 11" ]

# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = INFO

#---------------------------------------------------------------
# Includes
#---------------------------------------------------------------
# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------

from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9

from MC10JobOptions.SUSYEvgenConfig import evgenConfig
#==============================================================
#
# End of job options file
#
###############################################################
