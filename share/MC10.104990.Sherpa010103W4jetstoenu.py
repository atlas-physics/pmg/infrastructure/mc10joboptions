###############################################################
#
# Job options file
#
# Sherpa W+4jets->enu
#
# Wolfgang Mader, July 19 2008
# (Wolfgang.Mader@CERN.CH)
#
# modified and applied to W+j->enu
# Eva-Lotte Quatuor, March 20 2009
# (Eva-lotte.Quatuor@CEno!spamRN.CH)
#
# Responsible person(s)
#   18 Jul, 2008-xx xxx, 20xx: Peter Steinbach (P.Steinbach@physik.tu-dresden.de)
#   20 Mar, 2009-xx xxx, 20xx: Eva-Lotte Quatuor
#   27 Apr, 2010-31 Dec, 2010: Marcello Barisonzi
#
# Total XS (pb)     : 8791.7 +- ( 2.59277 = 0.0294911 % )
#
#==============================================================
#--------------------------------------------------------------
# ATLAS default Application Configuration options
#--------------------------------------------------------------
#
from AthenaCommon.AppMgr import ServiceMgr as svcMgr
#
#load relevant libraries
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
#
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
from Sherpa_i.Sherpa_iConf import ReadSherpa_i
#
sherpa = ReadSherpa_i()
sherpa.Files = [ "sherpa.evts" ]
#
topAlg += sherpa
#
# in case csc_evgen_EventWeight.py does not support Sherpa the following lines
# are required to copy the EventWeight also to EventInfo (not only EventCollection)
# please recheck necessity in releases later than AtlasProduction 14.5.1.4
#
#
from MC10JobOptions.SherpaEvgenConfig import evgenConfig
 
# inputfilebase
evgenConfig.inputfilebase = 'group09.phys-gener.sherpa010103.104990.W4jetstoenu.TXT.v3'
evgenConfig.efficiency = 0.90
#
#==============================================================
#
# End of job options file
################################################################

