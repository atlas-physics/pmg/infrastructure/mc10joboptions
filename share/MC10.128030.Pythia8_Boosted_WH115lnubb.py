from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ("MC10JobOptions/MC10_Pythia8_Common.py")

Pythia8.Commands += ['SoftQCD:all = off',
                     'HardQCD:all = off',
                     '25:m0 = 115',
                     '25:onMode = off',
                     '25:onIfMatch = 5 5',
                     'HiggsSM:ffbar2HW = on',
                     '24:onMode = off',
                     '24:onIfMatch = 11 12',
                     '24:onIfMatch = 13 14',
                     'PhaseSpace:pTHatMin = 100']

#--------#
# Filter #
#--------#

from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
topAlg += LeptonFilter()

LeptonFilter = topAlg.LeptonFilter
LeptonFilter.Ptcut = 10000.
LeptonFilter.Etacut = 2.7




#==============================================================
#
# End of job options file
#
###############################################################
