###############################################################
#
# Job options file for dijet production with pythia
#
# author: C. Gwenlan (July'10)
#
# DW tune [R. Field, hep-ph/0610012] (PYTUNE 103)
#
# reference JO: MC10.105011.J2.pythia_jetjet.py
#===============================================================

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_PythiaDW_Common.py" )

Pythia.PythiaCommand += [
                  "pysubs msel 0",
                  "pysubs ckin 3 35.",
#                  "pysubs ckin 4 70.",
                  "pysubs msub 11 1",
                  "pysubs msub 12 1",
                  "pysubs msub 13 1",
                  "pysubs msub 68 1",
                  "pysubs msub 28 1",
                  "pysubs msub 53 1"]


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
