from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_PythiaMC09p_Common.py" )

Pythia.PythiaCommand += [ "pyinit user madgraph",
                          "pydat1 parj 90 20000.",
                          "pydat3 mdcy 15 1 0" ]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.MadGraphEvgenConfig import evgenConfig
#from MC10JobOptions.PythiaEvgenConfig import evgenConfig
#evgenConfig.generators = ["Pythia","Lhef"]
evgenConfig.efficiency = 0.90

# dummy needed
evgenConfig.inputfilebase = 'MadGraph'
try:
  if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputfilebase = "group09.phys-gener.MadGraph.115652.HtoLJ_8zd_pion.TXT.v2"
  if runArgs.ecmEnergy == 8000.0:
    evgenConfig.inputfilebase = "group09.phys-gener.MadGraph.115652.HtoLJ_8zd_pion_8TeV.TXT.v1"
except NameError:
  pass
