#Wprime production with pythia
#
# prepared by Kamal Benslama December 08 2005
# updated for 10 TeV production, release 14 by Frederic Brochu, July 25 2008.

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )


#--------------------------------------------------------------
# Algorithms Private Options
#--------------------------------------------------------------

# Wprime resonance mass (in GeV)
WprimeMass = 2500


# Wprime specific parameters for pythia :
Pythia.PythiaCommand += [
       "pysubs msel 0",
       "pydat1 parj 90 20000",
       "pydat3 mdcy 15 1 0",
       "pysubs msub 142 1",   # SSM W'
       # W' decays  to quarks
       "pydat3 mdme 311 1 0",
       "pydat3 mdme 312 1 0",
       "pydat3 mdme 313 1 0",
       "pydat3 mdme 315 1 0",
       "pydat3 mdme 316 1 0",
       "pydat3 mdme 317 1 0",
       "pydat3 mdme 319 1 0",
       "pydat3 mdme 320 1 0",
       "pydat3 mdme 321 1 0",
       # W' decay to e nu - mu nu - tau nu 
       "pydat3 mdme 327 1 1",
       "pydat3 mdme 328 1 1",
       "pydat3 mdme 329 1 1",
       #
       "pydat1 mstu 1 0",
       "pydat1 mstu 2 0",
       "pydat2 pmas 34 1 "+str(WprimeMass),
       "pyinit pylisti 12"
       ]
# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )
# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#==============================================================
#
# End of job options file
#
###############################################################
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9


