# quark compositeness, positive interference
# Lambda = 5 TeV
# dijet production with pythia

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

Pythia.PythiaCommand += [
	                "pysubs msel 51",        # compositeness  
                        "pysubs ckin 3 1200.",
                        "pysubs ckin 4 2000.",
                        "pytcsm rtcm 41 5000.",  # compositness scale
                        "pytcsm rtcm 42 1",      # interference sign
                        "pytcsm itcm 5 2",       # 2-on,1-ud,0-off
                        "pypars mstp 82 4"]
#---------------------------------------------------------------
#---------------------------------------------------------------
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )
 
# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
