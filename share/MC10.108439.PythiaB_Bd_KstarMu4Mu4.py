###############################################################
#
# Job options file for generation of B-events 
#  in user selected exclusive channel
#
#==============================================================
from MC10JobOptions.PythiaBEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.55

#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_PythiaB_Common.py" )

#--------------------------------------------------------------
# Algorithms 
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import BSignalFilter
topAlg += BSignalFilter()

#--------------------------------------------------------------
#              PARAMETERS  SPECIFIC  TO   PYTHIAB
#--------------------------------------------------------------
PythiaB.ForceCDecay = "no"
PythiaB.ForceBDecay = "yes"
#--------------------------------------------------------------
# -------------  FORCE   YOUR  B CHANNEL  HERE -------------
#--------------------------------------------------------------
# To force your B-decay channels decomment following 2 lines:
include( "MC10JobOptions/MC10_PythiaB_CloseAntibQuark.py" )
# T O   O P E N    T H E    U S E R    C H A N N E L
# For Bd -> Kstar(892) mu+ mu- 
#
PythiaB.PythiaCommand += ["pydat3 mdme 875 1 1", 
                                "pydat3 kfdp 875 1 13",
                                "pydat3 kfdp 875 2 -13",
                                "pydat3 kfdp 875 3 313",
                                "pydat3 kfdp 875 4 0",
                                "pydat3 kfdp 875 5 0",
                                "pydat3 brat 875 0.0000001"     ]
PythiaB.ForceDecayChannel = "BdKstarMuMu"
# lvl1 and lvl2 cuts pt_L1 eta_L1 pt_L2 eta_L2
PythiaB.DecayChannelParameters = [1., 4.0, 2.5, 1., 4.0, 2.5] ;
#--------------------------------------------------------------
# --------  PYTHIA PARAMETERS OPTIMAL FOR BEAUTY PRODUCTION  --
#--------------------------------------------------------------
include( "MC10JobOptions/MC10_PythiaB_Btune.py" )

PythiaB.PythiaCommand += ["pysubs ckin 3 9.",
                                "pysubs ckin 9 -3.5",
				"pysubs ckin 10 3.5",
				"pysubs ckin 11 -3.5",
				"pysubs ckin 12 3.5",
          "pysubs msel 1"]
#--------------------------------------------------------------
# -------------  DEFINE SELECTION CUTS  -------------
#--------------------------------------------------------------
#  ------------- Selections on b  quarks   -------------
# simulate  only b-flavour events
PythiaB.flavour =  5.
# PythiaB force exclusive decay channels only on b=-5 side
# ------------------- b=5  --- and/or ---  b=-5 --------
PythiaB.cutbq = ["0. 102.5 and 9. 2.5"]
#  ------------- LVL1 muon cuts 0=OFF 1=ON -------------
# for BsPhiMuMu, BdKstarMuMu BsGammaMuMu lvl1,lvl2 must be OFF
PythiaB.lvl1cut = [ 0.,  6., 2.5]
#  ------------- LVL2 muon/electron cuts  0=OFF 1=ON-------------
PythiaB.lvl2cut = [ 0.,  13.,     6.,   2.5]
#PythiaB.lvl2cut = { 0.,  11.,     6.,   2.5};
# ???  ------------- Offline cuts 0=OFF 1=ON -------------
PythiaB.offcut = [ 0., 0.5, 2.5, 3., 2.5, 0.5, 2.5]
#  ------------- Number of repeated hadronization mhadr -------------
PythiaB.mhadr =  4. 
#  ------------- For how many events store B-chain in NTUPLE -------------
BSignalFilter = topAlg.BSignalFilter
BSignalFilter.Cuts_Final_hadrons_switch = True
BSignalFilter.Cuts_Final_hadrons_pT = 500.0
BSignalFilter.Cuts_Final_hadrons_eta = 2.5
BSignalFilter.BParticle_cuts = 511
###############################################################
# Add POOL persistency
try:
     StreamEVGEN.RequireAlgs += ["BSignalFilter"]
except Exception, e:
     pass
#==============================================================
#
# End of job options file
#
###############################################################
