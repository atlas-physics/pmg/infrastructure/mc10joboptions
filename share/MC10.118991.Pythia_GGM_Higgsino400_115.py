#----------------------------
# GGM higgsino - like neutralino
#
# contact :  N. Panikashvili
#----------------------------

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

Pythia.SusyInputFile = "susy_higgsino_400_115_slha.txt";

Pythia.PythiaCommand += ["pysubs msel 0"]  # !
Pythia.PythiaCommand += ["pymssm imss 1 11" ]
Pythia.PythiaCommand += ["pymssm imss 11 1" ]
Pythia.PythiaCommand += ["pymssm imss 21 50" ]
Pythia.PythiaCommand += ["pymssm imss 22 50" ]

Pythia.PythiaCommand += ["pysubs msub 243 1"]  # q qbar > go go
Pythia.PythiaCommand += ["pysubs msub 244 1"]  # g g    > go go

Pythia.PythiaCommand += ["pysubs msub 216 1"]
Pythia.PythiaCommand += ["pysubs msub 217 1"]
Pythia.PythiaCommand += ["pysubs msub 220 1"]
Pythia.PythiaCommand += ["pysubs msub 226 1"]
Pythia.PythiaCommand += ["pysubs msub 229 1"]
Pythia.PythiaCommand += ["pysubs msub 230 1"]

#######

# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = INFO

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------

include("AthenaSealSvc/AthenaSealSvc_joboptions.py" )
AthenaSealSvc.CheckDictionary = True

from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.2
from MC10JobOptions.SUSYEvgenConfig import evgenConfig

from GeneratorFilters.GeneratorFiltersConf import ZtoLeptonFilter
topAlg += ZtoLeptonFilter()
ZtoLeptonFilter = topAlg.ZtoLeptonFilter

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------

try:
     StreamEVGEN.RequireAlgs += [ "ZtoLeptonFilter" ]
except Exception, e:
     pass

#---------------------------------------------------------------
#End of job options file
#
###############################################################

