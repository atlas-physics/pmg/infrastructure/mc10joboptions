#--------------------------------------------------------------
# File prepared by Marjorie Shapiro Sept 2010
#--------------------------------------------------------------
# dijet production requiring heavy flavor inside a truth jet


include ( "MC10JobOptions/MC10.105012.J3_pythia_jetjet.py" )

# This code sets up EvtGen for inclusive decays
from HepMCTools.HepMCToolsConf import TranslateParticleCodes
topAlg += TranslateParticleCodes()
from EvtGen_i.EvtGen_iConf import EvtInclusiveDecay
topAlg += EvtInclusiveDecay()

TranslateParticleCodes = topAlg.TranslateParticleCodes
TranslateParticleCodes.OutputLevel = 3
TranslateParticleCodes.translateToPDTFrom = "PYTHIA"
TranslateParticleCodes.translateFromPDTTo = "EVTGEN"
TranslateParticleCodes.test = [0,10000000]

EvtInclusiveDecay = topAlg.EvtInclusiveDecay
EvtInclusiveDecay.OutputLevel = 3
EvtInclusiveDecay.pdtFile = "inclusive.pdt"
EvtInclusiveDecay.decayFile = "inclusive.dec"

# The following is NEEDED with the default parameters of EvtInclusiveDecay
Pythia = topAlg.Pythia
Pythia.PythiaCommand += [
        "pydat1 parj 90 20000",   # prevent photon emission in parton showers
            "pydat1 mstj 26 0"        # turn off B mixing (done by EvtGen)
            ]


#
# We will match  to antikt with cone of 0.4
try:
    from JetRec.JetGetters import *
    c4=make_StandardJetGetter('AntiKt',0.4,'Truth')
    c4alg = c4.jetAlgorithmHandle()
except Exception, e:
    pass

#--------------------------------------------------------------
# Filter on the B and charm hadrons
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import HeavyFlavorHadronFilter
topAlg += HeavyFlavorHadronFilter()
topAlg.HeavyFlavorHadronFilter.OutputLevel=INFO
topAlg.HeavyFlavorHadronFilter.RequestCharm=TRUE
topAlg.HeavyFlavorHadronFilter.RequestBottom=TRUE
topAlg.HeavyFlavorHadronFilter.Request_cQuark=FALSE
topAlg.HeavyFlavorHadronFilter.Request_bQuark=FALSE
topAlg.HeavyFlavorHadronFilter.RequestSpecificPDGID=FALSE
topAlg.HeavyFlavorHadronFilter.RequireTruthJet=TRUE
topAlg.HeavyFlavorHadronFilter.TruthContainerName="AntiKt4TruthJets"
topAlg.HeavyFlavorHadronFilter.JetPtMin=17.0*GeV
topAlg.HeavyFlavorHadronFilter.JetEtaMax=3.0
topAlg.HeavyFlavorHadronFilter.BottomPtMin=5.0*GeV
topAlg.HeavyFlavorHadronFilter.CharmPtMin=5.0*GeV
topAlg.HeavyFlavorHadronFilter.BottomEtaMax=2.7
topAlg.HeavyFlavorHadronFilter.CharmEtaMax=2.7
topAlg.HeavyFlavorHadronFilter.DeltaRFromTruth=0.3

try:
    StreamEVGEN.RequireAlgs +=  [ "HeavyFlavorHadronFilter" ]
except Exception, e:
    pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.19
##evgenConfig.minevents=100
evgenConfig.auxfiles += [ 'inclusive.dec', 'inclusive.pdt' ]

