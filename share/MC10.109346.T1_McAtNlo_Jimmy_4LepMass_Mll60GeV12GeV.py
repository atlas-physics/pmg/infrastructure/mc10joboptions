###############################################################
#
# Job options file
# Theodota Lagouri
# (based on original from Wouter Verkerke, Carl Gwilliam)
#
#==============================================================
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
#MessageSvc = Service( "MessageSvc" )
#MessageSvc.OutputLevel = 3

#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------

#-- Dll's and Algorithms
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 

try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common.py" )
  if runArgs.ecmEnergy == 14000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common_14TeV.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig

# ... Main generator : Herwig
Herwig.HerwigCommand += [ "taudec TAUOLA"]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import FourLeptonMassFilter
topAlg += FourLeptonMassFilter()
FourLeptonMassFilter = topAlg.FourLeptonMassFilter
FourLeptonMassFilter.MinPt = 5000.
FourLeptonMassFilter.MaxEta = 3.
FourLeptonMassFilter.MinMass1 = 60000.
FourLeptonMassFilter.MaxMass1 = 14000000.
FourLeptonMassFilter.MinMass2 = 12000.
FourLeptonMassFilter.MaxMass2 = 14000000.
FourLeptonMassFilter.AllowElecMu = True
FourLeptonMassFilter.AllowSameCharge = True

try:
     StreamEVGEN.RequireAlgs = [ "FourLeptonMassFilter" ]
except Exception, e:
     pass


from MC10JobOptions.McAtNloEvgenConfig import evgenConfig

# dummy needed
evgenConfig.inputfilebase = 'mcatnlo'
try:
  if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputfilebase = 'group09.phys-gener.mcatnlo341.109346.ttbar_7TeV.TXT.v1'
except NameError:
  pass

#
# FourLeptonMassFilter
# 1000/319254=0.00313+-0.00010
#

evgenConfig.minevents=1000
evgenConfig.efficiency=0.003

#==============================================================
#
# End of job options file
#
###############################################################
