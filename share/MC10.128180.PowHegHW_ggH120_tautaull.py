###############################################################
#
# Job options file for POWHEG with Herwig/Jimmy
# U. Husemann, C. Wasicki, Nov. 2009 / Feb. 2010
#
# 4vec was generated with Powheg-BOX/gg_H for 7TeV (ver1.0) (J.Tanaka)
#
#==============================================================
#--------------------------------------------------------------
# General Application Configuration options
#--------------------------------------------------------------
import AthenaCommon.AtlasUnixGeneratorJob

from AthenaCommon.AppMgr import theApp
from AthenaCommon.AppMgr import ServiceMgr

# make sure we are loading the ParticleProperty service
from PartPropSvc.PartPropSvcConf import PartPropSvc
ServiceMgr += PartPropSvc()

#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
ServiceMgr.MessageSvc.OutputLevel = INFO

from AthenaCommon.AlgSequence import AlgSequence
topAlg=AlgSequence()

# ... Main generator : Herwig
try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_PowHegJimmy_Common_7TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_PowHegJimmy_Common.py" )
  if runArgs.ecmEnergy == 14000.0:
    include ( "MC10JobOptions/MC10_PowHegJimmy_Common_14TeV.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig

# ID=1-6 for dd,...,tt, 7-9 for ee,mumu,tautau, 10,11,12 for WW,ZZ,gamgam
# iproc=-100-ID (lhef=-100)
Herwig.HerwigCommand += [ "iproc -109" ]
# W decay
Herwig.HerwigCommand += [ "taudec TAUOLA" ]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment_LeptonicDecay.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

from MC10JobOptions.EvgenConfig import evgenConfig, knownGenerators

evgenConfig.generators += [ "Lhef", "Herwig" ]

#dummy needed
evgenConfig.inputfilebase = 'powheg'
try:
  if runArgs.ecmEnergy == 7000.0:
      evgenConfig.inputfilebase = 'group10.phys-gener.powhegv1.116702.ggH_SM_M120_7TeV.TXT.v1'
except NameError:
  pass

evgenConfig.efficiency = 0.9
#==============================================================
#
# End of job options file
#
###############################################################
