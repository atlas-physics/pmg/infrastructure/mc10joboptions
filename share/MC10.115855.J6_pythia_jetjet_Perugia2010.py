###############################################################
#
# Job options file for dijet production with pythia
#
# author: C. Gwenlan (June'10)
#
# reference Perugia2010 parameters as in arXiv:1005.3457v1 [hep-ph]
# Perugia2010 (PYTUNE 327; from 6.423 onwards)
#
# reference JO: MC10.105015.J6.pythia_jetjet.py
#===============================================================

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_PythiaPerugia2010_Common.py" )

Pythia.PythiaCommand += [
			      "pysubs msel 0",
			      "pysubs ckin 3 560.",
			      "pysubs ckin 4 1120.",
			      "pysubs msub 11 1",
			      "pysubs msub 12 1",
			      "pysubs msub 13 1",
			      "pysubs msub 68 1",
			      "pysubs msub 28 1",
			      "pysubs msub 53 1"]

     
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
