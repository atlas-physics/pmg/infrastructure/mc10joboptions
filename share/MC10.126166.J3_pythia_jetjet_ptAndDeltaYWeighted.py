###############################################################
#
# Job options file
#
# Pythia J3 event generation with pT-deltaY weighting.
# cross section as default pythia sample = 2.19*10^3 nb
#
# Prepared by Graham Jones, Graham.Jones [at] cern.ch 
#==============================================================


# dijet production with pythia
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

Pythia.PythiaCommand += [
			      "pysubs msel 0",
			      "pysubs ckin 3 70.",
			      "pysubs ckin 4 140.",
			      "pysubs msub 11 1",
			      "pysubs msub 12 1",
			      "pysubs msub 13 1",
			      "pysubs msub 68 1",
			      "pysubs msub 28 1",
			      "pysubs msub 53 1"]

#--------------------------------------------------------------
# Truth Jets
#--------------------------------------------------------------

# Make truth jets for JetForwardFilter:
from AthenaCommon.AppMgr import ServiceMgr as svcMgr
include( "AthenaCommon/Atlas_Gen.UnixStandardJob.py" )
from JetRec.JetGetters import *
Kt6Alg = make_StandardJetGetter('AntiKt',0.6,'Truth').jetAlgorithmHandle()
Kt6Alg.AlgTools['JetFinalPtCut'].MinimumSignal = 7.0*GeV

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

## Truth filter
# Add the filter to the correct sequence!
from GeneratorFilters.GeneratorFiltersConf import JetForwardFilter
forwardFilter=JetForwardFilter()
topAlg += forwardFilter

# General properties
forwardFilter.JetContainer = "AntiKt6TruthJets"

# Kinematic cuts
forwardFilter.MinDeltaY = 0.0
forwardFilter.MaxDeltaY = 100.0
forwardFilter.MinPt1 = 12.0 #GeV
forwardFilter.MinPt2 = 12.0
forwardFilter.MaxPt1 = 70000.0
forwardFilter.MaxPt2 = 70000.0

#Gaussian mixture model describing pdf of unweighted sample
forwardFilter.muXs = [ 89.6164046121, 81.0997840196, 127.224835906, 78.4255812267, 107.870326395  ]
forwardFilter.muYs = [ 1.16315323985, 1.0867533849, 1.97220467099, 2.46041662529, 1.49842025863  ]
forwardFilter.sigmaXs = [ 18.2227648687, 11.0117967074, 27.7088049534, 11.8994737516, 19.2754233322  ]
forwardFilter.sigmaYs = [ 0.763767540934, 0.709434309515, 1.23725468411, 1.24482204501, 0.972815467913  ]
forwardFilter.rhos = [ -0.00178512009972, -0.0316705558389, -0.195907089706, 0.0548964023745, -0.0890443335585  ]
forwardFilter.weights = [ 0.247729709333, 0.290764621233, 0.0641700969037, 0.222290296975, 0.175045275554  ]

#Target probability (how much weighting required roughly)
#Lower = more suppression
import math
forwardFilter.SuppressionFactor = 5.0*math.pow(10.0, -8.0)

#--------------------------------------------------------------
# Apply filtering
#--------------------------------------------------------------

try:
  StreamEVGEN.RequireAlgs += [ "JetForwardFilter" ]
except Exception, e:
  pass


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
# N_gen = 542728045; N_pass = 98000 -> eff = 1.805*10^-4
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.0001805
evgenConfig.minevents=500 #~15hours
evgenConfig.weighting=0
