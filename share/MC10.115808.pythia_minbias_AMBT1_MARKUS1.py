###############################################################
#
# Job options file for min bias (ND)
#
# author: C. Gwenlan (June'10)
# AMBT1 "MARKUS1" tune - for systematic studies
# reference JO: MC10.105001.pythia_minbias.py
#===============================================================

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

#--------------------------------------------------------------
# AMBT1 systematic variation: MARKUS1
Pythia.PythiaCommand += [
                              "pypars parp 84 0.45",
                              "pypars parp 90 0.193"]

#--------------------------------------------------------------

Pythia.PythiaCommand += [  "pysubs msel 1" ]

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig

evgenConfig.efficiency = 0.9


#==============================================================
#
# End of job options file
#
###############################################################

