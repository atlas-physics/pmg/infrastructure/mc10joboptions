###############################################################
# $Id: $
#
# Authors:
#  Tobias Golling, Yale University
#  Rocco Mandrysch, Humboldt University of Berlin
#  Michael G Wilson, SLAC National Accelerator Laboratory
#==============================================================

# ... Main generator : Pythia
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include( "MC10JobOptions/MC10_Pythia_FourthGeneration.py" )

Pythia.PythiaCommand += [
    "pysubs msel 7",         # b' production channel
    "pydat2 pmas 7 1 500.0", # b' mass
    "pydat2 pmas 8 1 500.0", # t' mass

    # b' decay channel 
    "pydat3 mdme 56 1  0", # b' --> g b'
    "pydat3 mdme 57 1  0", # b' --> gamma b'
    "pydat3 mdme 58 1  0", # b' --> Z0 b'
    "pydat3 mdme 59 1  0", # b' --> W- u
    "pydat3 mdme 60 1  0", # b' --> W- c
    "pydat3 mdme 61 1  1", # b' --> W- t
    "pydat3 mdme 62 1  0", # b' --> W- t'
    "pydat3 mdme 63 1  0" # b' --> h0 b'
    ]

Pythia.PythiaCommand += ["pydat1 parj 90 20000",   # Turn off FSR (for Photos)
                         "pydat3 mdcy 15 1 0"   ]  # Turn off tau decays (for Tauola)

Pythia.PythiaCommand += [ "pypars mstp 3 1",      #set mstp 3 1 (setting Lambda in alpha_s)
                          "pypars parp 1 0.192",  #reset the defaults after mstp 3 1
                          "pypars parp 72 0.096", #FSR Labmda value in running alpha_s (ATLAS def 0.192)
                          "pydat1 paru 112 0.192",#reset defaults after mstp 3 1
                          "pydat1 mstu 112 4",    #reset defaults after mstp 3 1
                          "pypars parp 61 0.192"] #reset defaults after mstp 3 1

Pythia.PythiaCommand +=[ "pydat1 parj 82 2.0" ]  

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


#--------------------------------------------------------------
# Filter: Require at least one lepton
#--------------------------------------------------------------

# MultiLeptonFilter
from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
topAlg += MultiLeptonFilter()

MultiLeptonFilter = topAlg.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 10.*GeV
MultiLeptonFilter.Etacut = 2.7
MultiLeptonFilter.NLeptons = 1

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------
try:
    StreamEVGEN.RequireAlgs += [ "MultiLeptonFilter" ]
except Exception, e:
    pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.70
