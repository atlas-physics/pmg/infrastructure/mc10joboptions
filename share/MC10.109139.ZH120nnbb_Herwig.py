###############################################################
#
# Job options file
#
# Herwig ZH production with a higgs and Z pT filter
# Author: Giacinto Piacquadio (8/7/2008)
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 

try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_Herwig_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_Herwig_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_Herwig_Common.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig

Herwig.HerwigCommand += ["iproc 12705",
                         "rmass 201 120.0",
			 "taudec TAUOLA"]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

# Add the filters:
from GeneratorFilters.GeneratorFiltersConf import ParticleFilter
from GeneratorFilters.GeneratorFiltersConf import MissingEtFilter
ParticleFilterHiggs= ParticleFilter(name = "ParticleFilterHiggs",
                                    StatusReq=195,
                                    PDG=25,
                                    Ptcut = 150.*GeV,
                                    Etacut = 3.0,
                                    Energycut = 1000000000.0)

MissingEtFilter= MissingEtFilter(name = "MissingEtFilter",
				 MEtcut = 100.*GeV)

topAlg += ParticleFilterHiggs
topAlg += MissingEtFilter

try:
     StreamEVGEN.RequireAlgs += [ "MissingEtFilter" ]
     StreamEVGEN.RequireAlgs += [ "ParticleFilterHiggs" ]
except Exception, e:
     pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.HerwigEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.0194
# 5000/232397=0.0215
#==============================================================
#
# End of job options file
#
###############################################################
