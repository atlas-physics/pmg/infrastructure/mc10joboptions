

###############################################################
#
# Job options file
# Pythia8
# contact: James Monk, Claire Gwenlan
#==============================================================

# ... Main generator : Pythia8

MessageSvc = Service( "MessageSvc" )
#MessageSvc.OutputLevel = 4

from AthenaCommon.AppMgr import ServiceMgr as svcMgr
  
import AthenaServices
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")
include ("MC10JobOptions/MC10_Pythia8_Common.py")



# turn on monophoton process
Pythia8.Commands += ['ExtraDimensionsLED:ffbar2Ggamma= on']
Pythia8.Commands += ['ExtraDimensionsLED:n = 4     ']
Pythia8.Commands += ['ExtraDimensionsLED:MD = 1500.']
Pythia8.Commands += ['5000039:m0 = 1035.    ']
Pythia8.Commands += ['5000039:mWidth = 209.']
Pythia8.Commands += ['5000039:mMin = 0.     ']
Pythia8.Commands += ['5000039:mMax = 1500. '] 
Pythia8.Commands += ["PhaseSpace:pTHatMin = 40"]



#
from MC10JobOptions.Pythia8EvgenConfig import evgenConfig
evgenConfig.efficiency = 0.95
