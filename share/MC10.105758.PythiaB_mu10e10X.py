###############################################################
#
# Job options file for generation of B/C events 
# no decay channel is specified.
# Only events containing at least one electron and one muon
# with pT>10GeV |eta|<2.5 are written to output
# Selection criteria can be changed by datacards
# Responsible: Jiahang ZHONG (jiahang.zhong@cernNOSPAM.ch)
#==============================================================
#from EvgenJobOptions.PythiaBEvgenConfig import evgenConfig
from MC10JobOptions.PythiaBEvgenConfig import evgenConfig
evgenConfig.minevents = 200   # Reduced events/file due to long production times 
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

#include ( "EvgenJobOptions/MC10_PythiaB_Common.py" )
include ( "MC10JobOptions/MC10_PythiaB_Common.py" )


#--------------------------------------------------------------				 
#              PARAMETERS  SPECIFIC  TO   PYTHIABMODULE
#--------------------------------------------------------------
PythiaB.ForceBDecay = "no"
PythiaB.ForceCDecay = "no"
PythiaB.maxTriesHard= 500000

#include( "EvgenJobOptions/MC10_PythiaB_Btune.py" )
include( "MC10JobOptions/MC10_PythiaB_Btune.py" )

PythiaB.PythiaCommand += ["pysubs ckin 3 15.",
				 "pysubs msel 1"]
#--------------------------------------------------------------
# -------------  DEFINE SELECTION CUTS  -------------
#--------------------------------------------------------------				
PythiaB.flavour =  45.				
#  ------------- Selections on b  quarks   -------------
PythiaB.cutbq = ["15. 4.5 or 15. 4.5"]
#  ------------- LVL1 muon cuts 0=OFF 1=ON -------------
PythiaB.lvl1cut = [ 1.,  10, 2.5]
#  ------------- LVL2 muon/electron cuts  0=OFF 1=ON-------------
PythiaB.lvl2cut = [ 1.,  11.,     10,   2.5]
#  ------------- Offline cuts 0=OFF 1=ON -------------
PythiaB.offcut = [ 0., 0., 102.5, 0., 102.5, 0., 102.5]
#  ------------- Number of repeated hadronization mhadr -------------
PythiaB.mhadr =  50. 
#==============================================================

PythiaB.PythiaCommand += ["pydat1 parj 90 20000.",  ## Turn off FSR
                         "pydat3 mdcy 15 1 0"]     ## Turn off tau decays

include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )
#
# End of job options file
#
###############################################################
