#
# Job options fragment to produce ttbar events with Pythia.
#

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

Pythia.PythiaCommand += [
    "pysubs msel 6",          # Heavy flavor production (ttbar)
    "pysubs ckin 3 18.",      # ptmin for hard-scattering process
    "pypars mstp 43 2",       # only Z0, not gamma*
    "pydat1 parj 90 20000", # Turn off FSR.
    "pydat3 mdcy 15 1 0" ]     # Turn off tau decays.

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.95

#==============================================================
#
# End of job options file
#
###############################################################

