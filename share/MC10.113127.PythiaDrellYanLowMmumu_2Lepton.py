###############################################################
# Job options file
# Claire Gwenlan
# Low mass Drell Yan -  mumu - 10<M<60 - Di-lepton Filter
#==============================================================

# ... Main generator : Pythia
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_Pythia_Common.py" )


Pythia.PythiaCommand +=[ "pysubs msel 0",          # Users decay choice.
                         "pydat1 parj 90 20000",   # Turn off FSR.
                         "pydat3 mdcy 15 1 0",     # Turn off tau decays.
                         #
                         # Z production:
                         "pysubs msub 1 1",        # Create Z bosons.
                         "pysubs ckin 1 10.0",     # Lower invariant mass.
                         "pysubs ckin 2 60.0",     # Higher invariant mass.
                         "pydat3 mdme 174 1 0",
                         "pydat3 mdme 175 1 0",
                         "pydat3 mdme 176 1 0",
                         "pydat3 mdme 177 1 0",
                         "pydat3 mdme 178 1 0",
                         "pydat3 mdme 179 1 0",
                         "pydat3 mdme 182 1 0",    # Switch for Z->ee.
                         "pydat3 mdme 183 1 0",
                         "pydat3 mdme 184 1 1",    # Switch for Z->mumu.
                         "pydat3 mdme 185 1 0",
                         "pydat3 mdme 186 1 0",    # Switch for Z->tautau.
                         "pydat3 mdme 187 1 0"
                         ]


# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter

MultiLeptFiltL=MultiLeptonFilter(name = "MultiLeptFiltL",
                          Ptcut = 5.*GeV,
                          Etacut = 5.0,
                          NLeptons = 2)

MultiLeptFiltH=MultiLeptonFilter(name = "MultiLeptFiltH",
                          Ptcut = 15.*GeV,
                          Etacut = 5.0,
                          NLeptons = 1)

topAlg += MultiLeptFiltL
topAlg += MultiLeptFiltH

try:
  StreamEVGEN.RequireAlgs = [ "MultiLeptFiltL","MultiLeptFiltH"]
except Exception, e:
  pass
   
#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------

try:
     StreamEVGEN.RequireAlgs += [ "MultiLeptonFilter" ]
except Exception, e:
     pass


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.90

#==============================================================
#
# End of job options file
#
###############################################################
