###############################################################
#
# Job options file
# Wouter Verkerke
#
#==============================================================
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 2
MessageSvc.infoLimit = 1000

#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------

# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 
try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common.py" )
  if runArgs.ecmEnergy == 14000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_14TeV.py" )
except NameError:
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig 

Herwig.HerwigCommand += [ "iproc alpgen",
                          "taudec TAUOLA",
                        ]
# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

# Muon filter
from GeneratorFilters.GeneratorFiltersConf import MuonFilter
topAlg += MuonFilter()

MuonFilter.Ptcut  = 10.*GeV 
MuonFilter.Etacut = 2.8

try:
     StreamEVGEN.RequireAlgs += [ "MuonFilter" ]
except Exception, e:
     pass

from MC10JobOptions.AlpgenEvgenConfig import evgenConfig
 
# inputfilebase
evgenConfig.inputfilebase = 'alpgen'
try:
  if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputfilebase = 'group09.phys-gener.alpgen.117747.QcdbbFullJ2Np2_TOPfiltmu_pt20.TXT.v1'    
  if runArgs.ecmEnergy == 10000.0:
    evgenConfig.inputfilebase = 'alpgen.107747.QcdbbFullJ2Np2.TOPfiltmu.pt20'    
except NameError:
  pass

# 7 TeV - Information on sample 117747
# 7 TeV - Filter efficiency  = 0.0146
# 7 TeV - MLM matching efficiency = 0.36
# 7 TeV - Number of Matrix Elements in input file  = 145000
# 7 TeV - Alpgen cross section = 1339650.0 pb
# 7 TeV - Herwig cross section = Alpgen cross section * eff(MLM) = 475736.5 pb
# 7 TeV - Cross section after filtering = 6932.7 pb
# 7 TeV - Lumi/500 events post-filter = 500/eff(Filter)/XS(jimmy) = 6932.69 pb-1
#
# 7 TeV - Filter efficiency estimate below reduced by 20% to produce 625 events on average,
# 7 TeV - of which only 500 will be used in further processing
evgenConfig.efficiency = 0.01166
#==============================================================
#
# End of job options file
#
###############################################################
