###############################################################
#
# Job options file
#
# Pythia Z->tauntau (inclusive), AMBT1_EIGEN1M tune
#   implementation based on JobO for 11159X
#
# Responsible person(s)
#   6 Oct, 2010-xx xxx, 20xx: Martin Flechl (Martin.Flechl@cern.ch)
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

# ... Main generator : Pythia
include ( "MC10JobOptions/MC10_Pythia_Common.py" )

# AMBT1 systematic variation: eigentune 1-
Pythia.PythiaCommand += [
                         "pypars parp 77 1.35480",
                         "pypars parp 78 0.59025",
                         "pypars parp 82 2.07370",
                         "pypars parp 84 0.82325",
                         "pypars parp 90 0.24553"]
#

Pythia.PythiaCommand += [ "pysubs msel 0",
                         "pydat1 parj 90 20000", # Turn off FSR.
                         "pydat3 mdcy 15 1 0",   # Turn off tau decays.
                         "pysubs msub 1 1",        # Create Z bosons.
                         "pysubs ckin 1 60.0",     # Lower invariant mass.
                         "pydat3 mdme 174 1 0",
                         "pydat3 mdme 175 1 0",
                         "pydat3 mdme 176 1 0",
                         "pydat3 mdme 177 1 0",
                         "pydat3 mdme 178 1 0",
                         "pydat3 mdme 179 1 0",
                         "pydat3 mdme 182 1 0",    # Switch for Z->ee.
                         "pydat3 mdme 183 1 0",
                         "pydat3 mdme 184 1 0",    # Switch for Z->mumu.
                         "pydat3 mdme 185 1 0",
                         "pydat3 mdme 186 1 1",    # Switch for Z->tautau.
                         "pydat3 mdme 187 1 0",
                         "pydat3 mdme 188 1 0",
                         "pydat3 mdme 189 1 0"
                         ]

# ... UE

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
# efficiency = 0.9 - no filtering
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.90
#==============================================================
#
# End of job options file
#
###############################################################
