#--------------------------------------------------------------
# Herwig dijet w/ JetFilter : ET(jet) > 500 GeV and N(jet) = 1
# Prepared by L. Roos, June, 2011 
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence( "TopAlg" ) 

try:
    if runArgs.ecmEnergy == 7000.0:
        include ( "MC10JobOptions/MC10_Herwig_Common_7TeV.py" )
    if runArgs.ecmEnergy == 10000.0:
        include ( "MC10JobOptions/MC10_Herwig_Common.py" )
except NameError:
    # needed (dummy) default
    from Herwig_i.Herwig_iConf import Herwig
    topAlg += Herwig()
    Herwig = topAlg.Herwig

Herwig.HerwigCommand += [ "iproc 11500",
                          "ptmin 495." ]

#--------------------------------------------------------------
# Filter Options
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import JetFilter
topAlg += JetFilter()

JetFilter = topAlg.JetFilter
JetFilter.JetNumber = 1
JetFilter.EtaRange = 2.7
JetFilter.JetThreshold = 500000.;  # Note this is 500 GeV
JetFilter.JetType = False; #true is a cone, false is a grid
JetFilter.GridSizeEta = 2; # sets the number of (approx 0.06 size) eta
JetFilter.GridSizePhi = 2; # sets the number of (approx 0.06 size) phi cells

#--------------------------------------------------------------
# Pool Output Options
#--------------------------------------------------------------
try:
    StreamEVGEN.RequireAlgs += ["JetFilter"]
except Exception, e:
    pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
# efficiency = 5000/11314*0.9 = 0.398
from MC10JobOptions.HerwigEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.398
