###############################################################
#
# Job options file for high-pT top events
# Ulrich Husemann (ulrich.husemann(at)desy.de)
#
#==============================================================
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 3

#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------

#-- Dll's and Algorithms
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")
 

try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common.py" )
  if runArgs.ecmEnergy == 14000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common_14TeV.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig

# ... Main generator : Herwig
Herwig.HerwigCommand += [ "taudec TAUOLA"]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import TTbarFilter
topAlg += TTbarFilter()

TTbarFilter = topAlg.TTbarFilter
TTbarFilter.Ptcut = 500000.

try:
    StreamEVGEN.RequireAlgs = [ "TTbarFilter" ]
except Exception, e:
    pass


from MC10JobOptions.McAtNloEvgenConfig import evgenConfig
 
# inputfilebase
evgenConfig.inputfilebase = 'mcatnlo31.105208.ttbar'
evgenConfig.efficiency = 0.003
evgenConfig.minevents = 500

#--------------------------------------------------------------
# MC@NLO data card
#--------------------------------------------------------------
#  './mcatnlo31.105208.ttbar._00002'                ! prefix for BASES files
#  './mcatnlo31.105208.ttbar._00002'               ! prefix for event files
#   10000 1 1 1 1 ! energy, fren, ffact, frenmc, ffactmc
#   -1706                          ! -1705/1706=bb/tt
#   172.5                       ! M_Q
#   0.32 0.32 0.5 1.5 5.0 0.75 ! quark and gluon masses
#  'P'  'P'               ! hadron types
#  'CTEQ'   56            ! PDF group and id number
#   -1                     ! Lambda_5, <0 for default
#  'MS'                   ! scheme
#   3000000                       ! number of events
#   0                        ! 0 => wgt=+1/-1, 1 => wgt=+w/-w
#   112905620                          ! seed for rnd numbers
#   0.3                             ! zi
#   10 10                 ! itmx1,itmx2

#--------------------------------------------------------------
# HERWIG data card
#--------------------------------------------------------------
#  '@EVENTFILE@'        ! event file
#   1000000                       ! number of events
#   1                        ! 0->Herwig PDFs, 1 otherwise
#  'P'  'P'               ! hadron types
#   5000.00 5000.00               ! beam momenta
#   -1706                          ! -1705/1706=bb/tt
#  'CTEQ'                      ! PDF group (1)
#   56                         ! PDF id number (1)
#  'CTEQ'                      ! PDF group (2)
#   56                         ! PDF id number (2)
#   -1                     ! Lambda_5, < 0 for default
#   172.5 80.403                ! M_t, M_W
#   0.32 0.32 0.5 1.5 5.0 0.75 ! quark and gluon masses

#==============================================================
#
# End of job options file
#
###############################################################

