# LSTC (Z,Gamma) & Z->ee using Pythia
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

#--------------------------------------------------------------
# File prepared by Louis Helary Oct 10
#--------------------------------------------------------------
MrhoT=300.
MaT=330.
MPiT=200.


Pythia.PythiaCommand += ["pysubs msel 0", #Turn off all process
                       "pysubs msub 194 0",  # f+fbar -> f'+fbar' (ETC)
                       "pysubs msub 361 0",  # f + fbar -> W_L+ W_L-
                       "pysubs msub 362 0",  # f + fbar -> W_L+/- pi_T-/+
                       "pysubs msub 363 0",  # pi_tc+ pi_tc-
                       "pysubs msub 364 0",  # f + fbar -> gamma pi_T0
                       "pysubs msub 365 0",  # gamma pi_tc0'
                       "pysubs msub 366 0",  # f + fbar -> Z0 pi_T0
                       "pysubs msub 368 0",  # f + fbar -> W+/- pi_T-/+
                       "pysubs msub 370 0",  # f + fbar' -> W_L+/- Z_L0
                       "pysubs msub 371 0",  # f + fbar' -> W_L+/- pi_T0
                       "pysubs msub 372 0",  # f + fbar' -> pi_T+/- Z_L0
                       "pysubs msub 373 0",  # pi_tc+ pi_tc-
                       "pysubs msub 374 0",  # f + fbar' -> gamma pi_T+/-
                       "pysubs msub 375 0",  # f + fbar' -> Z0 pi_T+/-  (Z_T pi_tc+-)
                       "pysubs msub 376 0",  # f + fbar' -> W+/- pi_T0  (W_T pi_tc0)
                       "pysubs msub 367 0",  # Z pi_tc0'
                       "pysubs msub 377 0",  # W pi_tc0'
                       "pysubs msub 378 0",  # f + fbar' -> gamma W+/- (ETC
                       "pysubs msub 379 1",  # f + fbar -> gamma Z0 (ETC)
                       "pysubs msub 380 0",  # f + fbar -> Z0 Z0 (ETC)
                       "pysubs ckin 1 "+str(MrhoT-25), # Kinematical cut s>240 
                       "pysubs ckin 2 "+str(MaT+25)] # Kinematical cut s<260 
# Z -> e e 

Pythia.PythiaCommand += ["pydat3 mdme 174 1 0", # Z -> d dbar 
                        "pydat3 mdme 175 1 0", # Z -> u ubar 
                        "pydat3 mdme 176 1 0", # Z -> s sbar
                        "pydat3 mdme 177 1 0", # Z -> c cbar
                        "pydat3 mdme 178 1 0", # Z -> b bbar
                        "pydat3 mdme 179 1 0", # Z -> t tbar
                        "pydat3 mdme 182 1 1", # Z -> e+ e-
                        "pydat3 mdme 183 1 0", # Z -> nue nue
                        "pydat3 mdme 184 1 0", # Z -> mu mu
                        "pydat3 mdme 185 1 0", # Z -> numu numu
                        "pydat3 mdme 186 1 0", # Z -> tau tau
                        "pydat3 mdme 187 1 0"] # Z -> nutau nutau

#LSTC Parameters:
#mass:
Pythia.PythiaCommand += ["pydat2 pmas 3000111 1 "+str(MPiT), # pi_T0
                        "pydat2 pmas 3000211 1 "+str(MPiT), # pi_T+/-
                        "pydat2 pmas 3000221 1 "+str(MPiT*2), # pi_T0'
                        "pydat2 pmas 3000115 1 "+str(MaT), # a_T0
                        "pydat2 pmas 3000215 1 "+str(MaT), # a_T+/-
                        "pydat2 pmas 3000113 1 "+str(MrhoT), # rho_T0
                        "pydat2 pmas 3000213 1 "+str(MrhoT), # rho_T+/-
                        "pydat2 pmas 3000223 1 "+str(MrhoT)] # omega_T



Pythia.PythiaCommand += ["pypars mstp 42 0"] # switching off W width
Pythia.PythiaCommand += ["pypars mstp 32 4"] # Changing the scale of the interaction
Pythia.PythiaCommand += ["pytcsm rtcm 47 1.", # ratio coupling g_a-pi-pi to g_rho-pi-pi
                        "pytcsm rtcm 12 "+str(MrhoT), # M_V1
                        "pytcsm rtcm 13 "+str(MrhoT), # M_A1
                        "pytcsm rtcm 48 "+str(MrhoT), # M_V2
                        "pytcsm rtcm 49 "+str(MrhoT), # M_A2
                        "pytcsm rtcm 50 "+str(MrhoT), # M_V3
                        "pytcsm rtcm 51 "+str(MrhoT), # M_A3
                        "pytcsm rtcm 2 1.", # charge of up type TQ: QD=QU-1
                        "pytcsm rtcm 3 0.3333333"] # sin(chi)


Pythia.PythiaCommand += ["pydat1 parj 90 20000", # Turn off FSR.
                         "pydat3 mdcy 15 1 0"]     # Turn off tau decays.
			 
# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter Lepton & gamma
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import PhotonFilter
topAlg += PhotonFilter()

from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
topAlg += LeptonFilter()

PhotonFilter = topAlg.PhotonFilter
PhotonFilter.Ptcut = 30000.
PhotonFilter.Etacut = 3.
PhotonFilter.NPhotons = 1

LeptonFilter = topAlg.LeptonFilter
LeptonFilter.Ptcut = 15000.
LeptonFilter.Etacut = 3.
#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------

try:
     StreamEVGEN.RequireAlgs +=  [ "PhotonFilter" ]
except Exception, e:
     pass
try:
     StreamEVGEN.RequireAlgs += [ "LeptonFilter" ]
except Exception, e:
     pass


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------

from MC10JobOptions.EvgenConfig import evgenConfig
evgenConfig.generators += [ "Pythia" ]
evgenConfig.auxfiles += [ 'PDGTABLE.MeV', 'pdt.table', 'DECAY.DEC' ] 

#evgenConfig.efficiency = 0.97

