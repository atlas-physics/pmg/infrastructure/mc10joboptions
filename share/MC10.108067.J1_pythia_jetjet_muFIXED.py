# dijet production with pythia
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

#--------------------------------------------------------------
# File prepared by Gordon Watts  (gwatts@phys.washington.edu)
#--------------------------------------------------------------
include ( "MC10JobOptions/MC10_Pythia_Common.py" )

Pythia.PythiaCommand += [ "pysubs msel 0",
                          "pysubs ckin 3 17.",
                          "pysubs ckin 4 35.",
                          "pysubs msub 11 1",
                          "pysubs msub 12 1",
                          "pysubs msub 13 1",
                          "pysubs msub 68 1",
                          "pysubs msub 28 1",
                          "pysubs msub 53 1"
#                          "pypars mstp 82 4"
                          ]

#---------------------------------------------------------------
# Make sure we have a decent muon floating around!
from GeneratorFilters.GeneratorFiltersConf import MuonFilter
topAlg += MuonFilter()

ParticleFilter = topAlg.MuonFilter
ParticleFilter.Ptcut = 3000.0
ParticleFilter.Etacut = 2.8

#---------------------------------------------------------------
# Since we are using a filter, re-define the StreamEVGEN for output
# so we only write out the proper events.

try:
     StreamEVGEN.RequireAlgs += ['MuonFilter']
except Exception, e:
     pass

# 12.0.1 Framework Communication Requirements
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency=0.00701
