# Alpgen Z(->tautau)+bb+0p with 3-lepton filter
#--------------------------------------------------------------
# File prepared by Soshi Tsuno (Soshi.Tsuno@cern.ch) Dec 2008
#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------
# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 

try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common.py" )
  if runArgs.ecmEnergy == 14000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_14TeV.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig


Herwig.HerwigCommand += [ "iproc alpgen",
                          "taudec TAUOLA"
                        ]
# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
topAlg += MultiLeptonFilter()
MultiLeptonFilter          = topAlg.MultiLeptonFilter
MultiLeptonFilter.Ptcut    = 5000.                ## Min Pt
MultiLeptonFilter.Etacut   = 10.                  ## Max |Eta|
MultiLeptonFilter.NLeptons = 2                    ## Min Nlep

from GeneratorFilters.GeneratorFiltersConf import ATauFilter
topAlg += ATauFilter()
ATauFilter = topAlg.ATauFilter
ATauFilter.Etacut         = 10.                   ## Max |Eta|
ATauFilter.llPtcute       = 5000.                 ## Min Pt(e)  for lep-lep
ATauFilter.llPtcutmu      = 5000.                 ## Min Pt(mu) for lep-lep
ATauFilter.lhPtcute       = 5000.                 ## Min Pt(e)  for lep-had
ATauFilter.lhPtcutmu      = 5000.                 ## Min Pt(mu) for lep-had
ATauFilter.lhPtcuth       = 10000.                ## Min Pt(h)  for lep-had
ATauFilter.hhPtcut        = 10000.                ## Min Pt(h)  for had-had
#ATauFilter.maxdphi        = 3.0                   ## Max dPhi between tau and anti-tau

try:
    StreamEVGEN.RequireAlgs +=  [ "MultiLeptonFilter", "ATauFilter" ]
except Exception, e:
    pass

from MC10JobOptions.AlpgenEvgenConfig import evgenConfig
 
# inputfilebase
#evgenConfig.inputfilebase = 'alpgen.109390.ZtautaubbNp0_pt20_3leptons'
evgenConfig.inputfilebase = 'group09.phys-gener.alpgen.109390.ZtautaubbNp0_pt20_3leptons_7Tev.TXT.v1'

# 7TeV
# Filter efficiency  = 0.09808
# MLM matching efficiency = 0.7274
# Alpgen cross section = 8.70535000+-0.00376307 pb
# Herwig cross section = Alpgen cross section * eff(MLM) = 6.3321 pb
# Integrated Luminosity = 7896.29366 pb-1
# Number of events in 4-vector file of Alpgen = 5000/(eff*0.8) = 90000 events
#
# 10TeV
# Filter efficiency  = 0.0904
# MLM matching efficiency = 0.7159
# Alpgen cross section = 17.27900000+-0.00813682 pb
# Herwig cross section = Alpgen cross section * eff(MLM) = 12.37 pb
# Number of events in 4-vector file of Alpgen = 5000/(eff*0.8) = 97000 events
#
# Filter efficiency x safety factor = eff x 80% = 0.07232
evgenConfig.efficiency = 0.07232
evgenConfig.minevents=5000
#==============================================================
#
# End of job options file
#
###############################################################
