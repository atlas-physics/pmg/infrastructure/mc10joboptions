from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_PythiaMC09p_Common.py" )
Pythia.PythiaCommand+= ["pyinit pylisti 12",
			"pyinit pylistf 1",
			"pyinit dumpr 1 10",
			"pysubs msel 0",
			"pysubs msub 370 1",
			"pydat2 pmas 3000111 1 350.0",
			"pydat2 pmas 3000113 1 350.0",
			"pydat2 pmas 3000115 1 350.0",
			"pydat2 pmas 3000211 1 350.0",
			"pydat2 pmas 3000213 1 350.0",
			"pydat2 pmas 3000215 1 350.0",
			"pydat2 pmas 3000221 1 350.0",
			"pydat2 pmas 3000223 1 350.0",
			"pydat3 mdme 174 1 0",
			"pydat3 mdme 175 1 0",
			"pydat3 mdme 176 1 0",
			"pydat3 mdme 177 1 0",
			"pydat3 mdme 178 1 0",
			"pydat3 mdme 179 1 0",
			"pydat3 mdme 180 1 0",
			"pydat3 mdme 181 1 0",
			"pydat3 mdme 182 1 1",
			"pydat3 mdme 183 1 0",
			"pydat3 mdme 184 1 1",
			"pydat3 mdme 185 1 0",
			"pydat3 mdme 186 1 1",
			"pydat3 mdme 187 1 0",
			"pydat3 mdme 188 1 0",
			"pydat3 mdme 189 1 0",
			"pydat3 mdme 190 1 0",
			"pydat3 mdme 191 1 0",
			"pydat3 mdme 192 1 0",
			"pydat3 mdme 194 1 0",
			"pydat3 mdme 195 1 0",
			"pydat3 mdme 196 1 0",
			"pydat3 mdme 198 1 0",
			"pydat3 mdme 199 1 0",
			"pydat3 mdme 200 1 0",
			"pydat3 mdme 208 1 0"
			]

include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
topAlg += MultiLeptonFilter()
MultiLeptonFilter = topAlg.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 5000.
MultiLeptonFilter.Etacut = 2.7
MultiLeptonFilter.NLeptons = 3

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------

try:
	StreamEVGEN.RequireAlgs = [ "MultiLeptonFilter" ]
except Exception, e:
	pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.310

