from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence('TopAlg')
from Sherpa_i.Sherpa_iConf import Sherpa_i
sherpa = Sherpa_i()

"""
(run){
  EXCLUSIVE_CLUSTER_MODE=1
  GENERATE_RESULT_DIRECTORY=1
  ACTIVE[25]=0
  ME_SIGNAL_GENERATOR=Amegic

  CSS_AS_FS_FAC=0.4
  CSS_AS_IS_FAC=0.4 
}(run)

(model){
  MODEL         = SM
}(model)

(processes){
  Process 93 93 -> 90 90 24[a] 93{1};
  Order_EW 3;
  Decay  24[a] -> 90 91;
  CKKW sqr(20/E_CMS)
  Scales VAR{(PPerp2(p[2]+p[3])+Abs2(p[2]+p[3])+PPerp2(p[4]+p[5])+Abs2(p[4]+p[5]))/2.0}
  Selector_File RUNDATFILE|(selector1){|}(selector1)
  End process;

  Process 93 93 -> 90 90 -24[a] 93{1};
  Order_EW 3;
  Decay -24[a] -> 90 91;
  CKKW sqr(20/E_CMS)
  Scales VAR{(PPerp2(p[2]+p[3])+Abs2(p[2]+p[3])+PPerp2(p[4]+p[5])+Abs2(p[4]+p[5]))/2.0}
  Selector_File RUNDATFILE|(selector2){|}(selector2)
  End process;
}(processes)

(selector1){
  "m"  90,90  0.0,E_CMS:0.0,E_CMS:60.0,120.0
  "m"  90,90  0.0,E_CMS:0.0,E_CMS:60.0,120.0
  "m"  90,90  0.0,E_CMS:0.0,E_CMS:60.0,120.0
}(selector1)

(selector2){
  "m"  90,90  60.0,120.0:0.0,E_CMS:0.0,E_CMS
  "m"  90,90  60.0,120.0:0.0,E_CMS:0.0,E_CMS
  "m"  90,90  60.0,120.0:0.0,E_CMS:0.0,E_CMS
}(selector2)
"""

try:
    sherpa.Parameters += [ 'RUNDATA=%s' % runArgs.jobConfig[0] ]
    sherpa.Parameters += [ 'RUNDATFILE:=%s' % runArgs.jobConfig[0] ]
except NameError:
    pass

topAlg += sherpa
from MC10JobOptions.SherpaFFEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 5000
evgenConfig.weighting = 0
