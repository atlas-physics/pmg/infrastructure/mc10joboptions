# Jet sample pT>17GeV
# special production with jet reweighting
# responsible: Jan Kretzschmar
# cross section: 1.461E6 nb
#--------------------------------------------------------------
# Generator Options
# Monika Weilers/ Ian Hinchliffe
# Borut Kersevan: added fix for procs 81,82 for top prod
#                 MSTP(7)=6 has to be set
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

Pythia.PythiaCommand += [
			      "pysubs msel 0",
			      "pysubs ckin 3 15.",
			      "pysubs msub 11 1",
			      "pysubs msub 12 1",
			      "pysubs msub 13 1",
			      "pysubs msub 28 1",
			      "pysubs msub 53 1",
			      "pysubs msub 68 1",
		 	      "pysubs msub 81 1",
			      "pysubs msub 82 1",
			      "pysubs msub 14 1",
			      "pysubs msub 29 1",
			      "pysubs msub 1 1",
			      "pysubs msub 2 1",
			      "pypars mstp 7 6"]


#--------------------------------------------------------------
# Filter Options ('standard' jet filter)
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import JetFilter
topAlg += JetFilter()

JetFilter = topAlg.JetFilter
JetFilter.JetNumber = 1
JetFilter.EtaRange = 2.7
JetFilter.JetThreshold = 17000.;  # Note this is 17 GeV
JetFilter.JetType=False; #true is a cone, false is a grid
JetFilter.GridSizeEta=2; # sets the number of (approx 0.06 size) eta
JetFilter.GridSizePhi=2; # sets the number of (approx 0.06 size) phi cells
JetFilter.OutputLevel = INFO
#--------------------------------------------------------------
try:
     StreamEVGEN.RequireAlgs += ["JetFilter"]
except Exception, e:
     pass

#--------------------------------------------------------------

# make truth jets
try:
     from JetRec.JetGetters import *
     c4=make_StandardJetGetter('Cone',0.4,'Truth')
     c4alg = c4.jetAlgorithmHandle()
except Exception, e:
     pass

#--------------------------------------------------------------
# additional filter&reweight on thruth jets (TruthJetWeightFilter)
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import TruthJetWeightFilter
topAlg += TruthJetWeightFilter()

TruthJetWeightFilter = topAlg.TruthJetWeightFilter
TruthJetWeightFilter.OutputLevel = INFO
### Main algorithm switch
TruthJetWeightFilter.weightAlgorithm = 0 # JetSubinfo
##TruthJetWeightFilter.weightAlgorithm = 1 # Std Leading Jet Pt
##TruthJetWeightFilter.weightAlgorithm = 2 # Rescaled Leading Jet Pt
##TruthJetWeightFilter.weightAlgorithm = 3 # Std Leading Jet Pt + Leading Particle upweight
##TruthJetWeightFilter.weightAlgorithm = 4 # Rescaled Leading Jet Pt + Leading Particle upweight

### following are the default options for Jet Pt reweighting
### those are unused in case weightAlgorithm == 0!
#TruthJetWeightFilter.minPt = 20.
#TruthJetWeightFilter.maxPt = 80.
#TruthJetWeightFilter.minEfficiency = 0.01
#TruthJetWeightFilter.lowPtTune = 5.
#TruthJetWeightFilter.maxEtaRange = 2.7
### rescaling constants of em and hadronic components (standard scales 2, 0.5)
#TruthJetWeightFilter.rescaleEm = 2.0
#TruthJetWeightFilter.rescaleHad = 0.5
### up-weighting of large pt single particles
#TruthJetWeightFilter.minPtLeadUpweight = 20

### if deactivateFilter = True, the weight calculation is performed,
### but no events are rejected
#TruthJetWeightFilter.deactivateFilter = True
TruthJetWeightFilter.deactivateFilter = False
# set samplingRandomSeed=0 for fully randomized or specific number for fixed seed
TruthJetWeightFilter.samplingRandomSeed = 0


#--------------------------------------------------------------
try:
     StreamEVGEN.RequireAlgs += ["TruthJetWeightFilter"]
except Exception, e:
     pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
### measured combined filter efficiency is 0.0076+-0.0002
evgenConfig.efficiency = 0.007

 
#==============================================================
#
# End of job options file
#
###############################################################

