from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_Pythia_Common.py" )
Pythia.PythiaCommand+= ["pyinit pylisti 12",
                        "pyinit pylistf 1",
                        "pyinit dumpr 1 10",
                        "pysubs msel 0",
                        "pysubs msub 370 1",
                        "pydat2 pmas 3000111 1 750.0",  # pi_tc0
                        "pydat2 pmas 3000211 1 750.0",  # pi_tc+
                        "pydat2 pmas 3000113 1 800.0",  # rho_tc0
                        "pydat2 pmas 3000213 1 800.0",  # rho_tc+
                        "pydat2 pmas 3000223 1 800.0",  # omega_tc
                        "pydat2 pmas 3000115 1 880.0",  # m(a_tc0) = 1.1 * m(rho)
                        "pydat2 pmas 3000215 1 880.0",  # m(a_tc+) = 1.1 * m(rho)
                        "pydat2 pmas 3000221 1 5000.0",  # pi'_tc0
                        "pydat2 pmas 3000331 1 5000.0",  # eta_tc0 
                        "pytcsm rtcm 2 1.",       #Q_U=1
                        "pytcsm rtcm 3 0.333333"  # sin(Chi)
                        "pytcsm rtcm 12 800.",    # m_V1 = m(rho)
                        "pytcsm rtcm 13 800.",    # m_A1 = m(rho)
                        "pytcsm rtcm 48 800.",    # m_V2 = m(rho)
                        "pytcsm rtcm 49 800.",    # m_A1 = m(rho)
                        "pytcsm rtcm 50 5000.",   # m_V3 = out of the way
                        "pytcsm rtcm 51 5000.",   # m_A3 = out of the way
                        "pytcsm itcm 1 4.",       # N_TC
                        # Z decays
                        "pydat3 mdme 174 1 0",
                        "pydat3 mdme 175 1 0",
                        "pydat3 mdme 176 1 0",
                        "pydat3 mdme 177 1 0",
                        "pydat3 mdme 178 1 0",
                        "pydat3 mdme 179 1 0",
                        "pydat3 mdme 182 1 1",  # Z -> e e
                        "pydat3 mdme 183 1 0",
                        "pydat3 mdme 184 1 1",  # Z -> mu mu
                        "pydat3 mdme 185 1 0",
                        "pydat3 mdme 186 1 1",  # Z -> tau tau
                        "pydat3 mdme 187 1 0",
                        # W decays
                        "pydat3 mdme 190 1 0",
                        "pydat3 mdme 191 1 0",
                        "pydat3 mdme 192 1 0",
                        "pydat3 mdme 194 1 0",
                        "pydat3 mdme 195 1 0",
                        "pydat3 mdme 196 1 0",
                        "pydat3 mdme 198 1 0",
                        "pydat3 mdme 199 1 0",
                        "pydat3 mdme 200 1 0",
                        "pydat3 mdme 206 1 1", # W -> e nu
                        "pydat3 mdme 207 1 1", # W -> mu nu
                        "pydat3 mdme 208 1 1", # W -> tau nu
                        ]


from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
topAlg += MultiLeptonFilter()
MultiLeptonFilter = topAlg.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 5000.
MultiLeptonFilter.Etacut = 2.7
MultiLeptonFilter.NLeptons = 3


#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------

#StreamEVGEN.RequireAlgs = [ "MultiLeptonFilter" ]

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.310

