
###############################################################
#
# Job options file
#
#==============================================================

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_PythiaMC09p_Common.py" )
Pythia.PythiaCommand += [ "pyinit user madgraph",
                          "pyinit pylisti -1",
                          "pyinit pylistf 1",
                          "pyinit dumpr 1 2",
                          "pydat1 parj 90 20000",
                          "pydat3 mdcy 15 1 0"]

## ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

## ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


from MC10JobOptions.MadGraphEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'group10.phys-gener.madgraph.119869.tTWj_T53_M400.TXT.v1'
evgenConfig.efficiency = 0.9
