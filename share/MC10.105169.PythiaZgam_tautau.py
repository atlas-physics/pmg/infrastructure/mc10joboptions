# Z + 1 photon & Z-> tautau & tau->e nue nutau
#--------------------------------------------------------------
# prepared by Ahmed Abdelalim (ahmed.ali.abdelalim@cern.ch)
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

Pythia.PythiaCommand += [
     "pysubs msel 0",
     "pysubs msub 19 1",       # qqbar->Zgamma
     "pysubs ckin 3 10.",
     "pydat3 mdme 174 1 0",
     "pydat3 mdme 175 1 0",
     "pydat3 mdme 176 1 0",
     "pydat3 mdme 177 1 0",
     "pydat3 mdme 178 1 0",
     "pydat3 mdme 179 1 0",
     "pydat3 mdme 182 1 0",    # Switch for Z->ee.
     "pydat3 mdme 183 1 0",
     "pydat3 mdme 184 1 0",    # Switch for Z->mumu.
     "pydat3 mdme 185 1 0",
     "pydat3 mdme 186 1 1",    # Switch for Z->tautau.
     "pydat3 mdme 187 1 0",
     "pydat3 mdme 89   1 1",   # Switch for tau-> nu_ebar+e-+nu_tau
     "pydat3 mdme 90   1 0",
     "pydat3 mdme 91   1 0",
     "pydat3 mdme 92   1 0",
     "pydat3 mdme 93   1 0",
     "pydat3 mdme 94   1 0",
     "pydat3 mdme 95   1 0",
     "pydat3 mdme 96   1 0",
     "pydat3 mdme 97   1 0",
     "pydat3 mdme 98   1 0",
     "pydat3 mdme 99   1 0",
     "pydat3 mdme 100  1 0",
     "pydat3 mdme 101  1 0",
     "pydat3 mdme 102  1 0",
     "pydat3 mdme 103  1 0",
     "pydat3 mdme 104  1 0",
     "pydat3 mdme 105  1 0",
     "pydat3 mdme 106  1 0",
     "pydat3 mdme 107  1 0",
     "pydat3 mdme 108  1 0",
     "pydat3 mdme 109  1 0",
     "pydat3 mdme 110  1 0",
     "pydat3 mdme 111  1 0",
     "pydat3 mdme 112  1 0",
     "pydat3 mdme 113  1 0",
     "pydat3 mdme 114  1 0",
     "pydat3 mdme 115  1 0",
     "pydat3 mdme 116  1 0",
     "pydat3 mdme 117  1 0",
     "pydat3 mdme 118  1 0",
     "pydat3 mdme 119  1 0",
     "pydat3 mdme 120  1 0",
     "pydat3 mdme 121  1 0",
     "pydat3 mdme 122  1 0",
     "pydat3 mdme 123  1 0",
     "pydat3 mdme 124  1 0",
     "pydat3 mdme 125  1 0",
     "pydat3 mdme 126  1 0",
     "pydat3 mdme 127  1 0",
     "pydat3 mdme 128  1 0",
     "pydat3 mdme 129  1 0",
     "pydat3 mdme 130  1 0",
     "pydat3 mdme 131  1 0",
     "pydat3 mdme 132  1 0",
     "pydat3 mdme 133  1 0",
     "pydat3 mdme 134  1 0",
     "pydat3 mdme 135  1 0",
     "pydat3 mdme 136  1 0",
     "pydat3 mdme 137  1 0",
     "pydat3 mdme 138  1 0",
     "pydat3 mdme 139  1 0",
     "pydat3 mdme 140  1 0",
     "pydat3 mdme 141  1 0",
     "pydat3 mdme 142  1 0",
     "pydat1 parj 90 20000."]

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


from GeneratorFilters.GeneratorFiltersConf import PhotonFilter
topAlg += PhotonFilter()

from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
topAlg += MultiLeptonFilter()

PhotonFilter = topAlg.PhotonFilter
PhotonFilter.Ptcut = 15000.
PhotonFilter.Etacut = 2.7
PhotonFilter.NPhotons = 1

MultiLeptonFilter = topAlg.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 15000.
MultiLeptonFilter.Etacut = 2.7
MultiLeptonFilter.NLeptons = 2


#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------

try:
     StreamEVGEN.RequireAlgs +=  [ "PhotonFilter" ]
except Exception, e:
     pass
try:
     StreamEVGEN.RequireAlgs += [ "MultiLeptonFilter" ]
except Exception, e:
     pass


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
# REL14 
# 6066/22728*0.9 = 0.2402 
evgenConfig.efficiency = 0.240
evgenConfig.minevents = 5000
#==============================================================
#
# End of job options file
#
###############################################################

