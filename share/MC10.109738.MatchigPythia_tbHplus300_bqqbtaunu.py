###############################################################
#
# Job options file
#
# Matchig / Pythia gg/gb->t[b]H+ with t->bqq, H+->tau(had)nu
# H+ Mass 300GeV
#
# Responsible person(s)
#   Feb 16, 2009 : Martin Flechl (Martin.Flechl@cern.ch)
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

# ... Pythia
include ( "MC10JobOptions/MC10_Pythia_Common.py" )
Pythia.PythiaCommand += [
                            "pyinit user matchig",    #matchig user process
                            "pysubs msel 0",
                            "pysubs msub 161 1",      #f + g -> f' + H+/-
                            "pysubs msub 401 1",      #g + g -> t + b + H+/-
                            "pysubs msub 402 1",      #q + qbar -> t + b + H+/-
                            "pypars mstp 129 1000",   #number of attempts to find highest xsec with 2->3 variables
                        ]

#H+ parameters
Pythia.PythiaCommand += [
                            "pydat1 paru 141 30.",   #tan beta
                            "pydat2 pmas 37 1 300."  #H+ mass
                        ]

#top decay
Pythia.PythiaCommand += [
                            "pydat3 mdme 41 1 0",
                            "pydat3 mdme 42 1 0",
                            "pydat3 mdme 43 1 0",
                            "pydat3 mdme 44 1 0",
                            "pydat3 mdme 45 1 0",
                            "pydat3 mdme 46 1 1", # t->Wb
                            "pydat3 mdme 48 1 0",
                            "pydat3 mdme 49 1 0", # t->H+b
                            "pydat3 mdme 50 1 -1",
                            "pydat3 mdme 51 1 -1",
                            "pydat3 mdme 52 1 -1",
                            "pydat3 mdme 53 1 -1",
                            "pydat3 mdme 54 1 -1",
                            "pydat3 mdme 55 1 -1"
                        ]

#W decay
Pythia.PythiaCommand += [
                            "pydat3 mdme 190 1 1", #qq' : ud
                            "pydat3 mdme 191 1 1", #qq' : dc
                            "pydat3 mdme 192 1 0", #    : dt
                            "pydat3 mdme 194 1 1", #qq' : us
                            "pydat3 mdme 195 1 1", #qq' : cs
                            "pydat3 mdme 196 1 0", #    : st
                            "pydat3 mdme 198 1 0", #    : bu
                            "pydat3 mdme 199 1 1", #qq' : cb
                            "pydat3 mdme 200 1 0", #    : tb
                            "pydat3 mdme 206 1 0", #lnu : e nu
                            "pydat3 mdme 207 1 0", #lnu : mu nu
                            "pydat3 mdme 208 1 0"  #    : tau nu
                        ]

#H+ decay
Pythia.PythiaCommand += [
                            "pydat3 mdme 503 1 0",
                            "pydat3 mdme 504 1 0",
                            "pydat3 mdme 505 1 0", # H+ -> t b
                            "pydat3 mdme 507 1 0",
                            "pydat3 mdme 508 1 0",
                            "pydat3 mdme 509 1 1", # H+ -> tau nu
                            "pydat3 mdme 511 1 0", # H+ -> W h0
                            "pydat3 mdme 512 1 0", # H+ -> ~chi_10 ~chi_1+
                            "pydat3 mdme 513 1 0", # H+ -> ~chi_10 ~chi_2+
                            "pydat3 mdme 514 1 0", # H+ -> ~chi_20 ~chi_1+
                            "pydat3 mdme 515 1 0", # H+ -> ~chi_20 ~chi_2+
                            "pydat3 mdme 516 1 0", # H+ -> ~chi_30 ~chi_1+
                            "pydat3 mdme 517 1 0", # H+ -> ~chi_30 ~chi_2+
                            "pydat3 mdme 518 1 0", # H+ -> ~chi_40 ~chi_1+
                            "pydat3 mdme 519 1 0", # H+ -> ~chi_40 ~chi_2+
                            "pydat3 mdme 520 1 0", # below: non-chi SUSY decays
                            "pydat3 mdme 521 1 0",
                            "pydat3 mdme 522 1 0",
                            "pydat3 mdme 523 1 0",
                            "pydat3 mdme 524 1 0",
                            "pydat3 mdme 525 1 0",
                            "pydat3 mdme 526 1 0",
                            "pydat3 mdme 527 1 0",
                            "pydat3 mdme 528 1 0",
                            "pydat3 mdme 529 1 0"
                        ]

#For TAUOLA/PHOTOS
Pythia.PythiaCommand += [
                         "pydat1 parj 90 20000",   # Turn off FSR.
                         "pydat3 mdcy 15 1 0"      # Turn off tau decays.
                        ]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment_HadronicDecay.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.98

#==============================================================
#
# End of job options file
#
###############################################################
