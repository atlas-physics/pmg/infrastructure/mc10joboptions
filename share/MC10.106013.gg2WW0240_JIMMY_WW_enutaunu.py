################################################################
#
# gg2WW2.4/JIMMY/HERWIG gg -> W+W- -> e+nu tau-nu
#
# Responsible person(s)
#   Mar 14, 2010 : Tiesheng Dai (Tiesheng.Dai@cern.ch)
#
################################################################
#
# Job options file
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 
# ... Herwig
try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common.py" )
  if runArgs.ecmEnergy == 14000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common_14TeV.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig

Herwig.HerwigCommand += [ "modbos 1 2", "modbos 2 4",
                          "maxpr 10",
                          "modpdf 10000",    #  CTEQ6M
                          "taudec TAUOLA"]

# set UE tune appropriate for CTEQ6M (MC08 tune)
try:
  if runArgs.ecmEnergy == 7000.0:
    Herwig.HerwigCommand += ["ptjim 3.33","jmrad 73 1.8"]
  if runArgs.ecmEnergy == 10000.0:
    Herwig.HerwigCommand += ["ptjim 3.58","jmrad 73 1.8"]
except NameError:
  pass


# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
topAlg += LeptonFilter()

LeptonFilter = topAlg.LeptonFilter
LeptonFilter.Ptcut = 10000.
LeptonFilter.Etacut = 2.7

try:
     StreamEVGEN.RequireAlgs +=  [ "LeptonFilter" ]
except Exception, e:
     pass

from MC10JobOptions.McAtNloEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'gg2WW0240.106013'
try:
  if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputfilebase = 'group09.phys-gener.gg2WW0240.106013.07TeV_WW.TXT.v1'
  if runArgs.ecmEnergy == 8000.0:
    evgenConfig.inputfilebase = 'group09.phys-gener.gg2WW0240.106013.08TeV_WW.TXT.v1'
  if runArgs.ecmEnergy == 10000.0:
    evgenConfig.inputfilebase = 'group09.phys-gener.gg2WW0240.106013.10TeV_WW.TXT.v1'
except NameError:
  pass
                
evgenConfig.efficiency = 0.80
#==============================================================
#
# End of job options file
#
###############################################################
