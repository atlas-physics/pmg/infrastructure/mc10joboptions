###############################################################
#
# Job options file
#
# Pythia W->enu w/o 2lep EF
#
#==============================================================
# ... Main generator : Pythia
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

Pythia.PythiaCommand +=[ "pysubs msel 12",
                         "pydat1 parj 90 20000", # Turn off FSR.
                         "pydat3 mdcy 15 1 0",   # Turn off tau decays.
                         "pydat3 mdme 190 1 0",
                         "pydat3 mdme 191 1 0",
                         "pydat3 mdme 192 1 0",
                         "pydat3 mdme 193 1 0",
                         "pydat3 mdme 194 1 0",
                         "pydat3 mdme 195 1 0",
                         "pydat3 mdme 196 1 0",
                         "pydat3 mdme 197 1 0",
                         "pydat3 mdme 198 1 0",
                         "pydat3 mdme 199 1 0",
                         "pydat3 mdme 200 1 0",
                         "pydat3 mdme 201 1 0",
                         "pydat3 mdme 202 1 0",
                         "pydat3 mdme 203 1 0",
                         "pydat3 mdme 204 1 0",
                         "pydat3 mdme 205 1 0",
                         "pydat3 mdme 206 1 1",    # Switch for W->enu.
                         "pydat3 mdme 207 1 0",    # Switch for W->munu.
                         "pydat3 mdme 208 1 0",    # Switch for W->taunu.
                         "pydat3 mdme 209 1 0"
                         ]


# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
topAlg += MultiElecMuTauFilter()

MultiElecMuTauFilter = topAlg.MultiElecMuTauFilter
MultiElecMuTauFilter.NLeptons  = 2
MultiElecMuTauFilter.MaxEta = 10.0
MultiElecMuTauFilter.MinPt = 5000.0
MultiElecMuTauFilter.IncludeHadTaus = False # one can choose whether to include hadronic taus or not

try:
     StreamEVGEN.RequireAlgs = [ "MultiElecMuTauFilter" ]
except Exception, e:
     pass

from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.00324   #=0.0036*0.9
#==============================================================
#
# End of job options file
#
###############################################################
