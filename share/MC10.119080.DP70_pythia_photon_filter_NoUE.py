#--------------------------------------------------------------
# Author  : Lydia Roos (from MC10.115802.DP17_pythia_photon_filter.py)
# Modified by Martin Tripiana (July 2011)
# * turn off MI/UE
#
# Purpose : Inclusive photon sample (brem+hard process). 
#
#--------------------------------------------------------------

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

Pythia.PythiaCommand += [
                  "pysubs msel 0",
                  "pysubs ckin 3  65.",
                  "pysubs msub 11 1",
                  "pysubs msub 12 1",
                  "pysubs msub 13 1",
                  "pysubs msub 28 1",
                  "pysubs msub 53 1",
                  "pysubs msub 68 1",
                  "pysubs msub 14 1",
                  "pysubs msub 29 1"]


# ------- Turn off MI/UE -----------------
Pythia.PythiaCommand += [ "pypars mstp 81 20" ]
#-------------------------------------------------------------- 

#-------------------------------------------------------------
# Filter
#-------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import DirectPhotonFilter
topAlg += DirectPhotonFilter()

DirectPhotonFilter = topAlg.DirectPhotonFilter
DirectPhotonFilter.Ptcut = 70000.
DirectPhotonFilter.Etacut =  2.7
DirectPhotonFilter.NPhotons = 1

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------

try:
     StreamEVGEN.RequireAlgs +=  [ "DirectPhotonFilter" ]
except Exception, e:
     pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------

from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.0004
evgenConfig.minevents = 100

#==============================================================
#
# End of job options file
#
###############################################################
