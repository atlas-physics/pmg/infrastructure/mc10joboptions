################################################################
#
# Pythia Dijet J1 with 1 muon EF
#
# Responsible person(s)
#   Nov 12, 2008 : Junichi Tanaka (Junichi.Tanaka@cern.ch)
#   Apr  1, 2010 : Slight modification by Martin Flechl
#
################################################################
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
# MessageSvc = Service( "MessageSvc" )
# MessageSvc.OutputLevel = 4

# from AthenaCommon.AppMgr import ServiceMgr as svcMgr

# from AthenaServices.AthenaServicesConf import AtRndmGenSvc
# svcMgr += AtRndmGenSvc()
# svcMgr.AtRndmGenSvc.OutputLevel = 4

# import AthenaServices
# AthenaServices.AthenaServicesConf.AthenaEventLoopMgr.OutputLevel = 4

# dijet production with pythia
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )
Pythia.PythiaCommand += [
			      "pysubs msel 0",
			      "pysubs ckin 3 17.",
			      "pysubs ckin 4 35.",
			      "pysubs msub 11 1",
			      "pysubs msub 12 1",
			      "pysubs msub 13 1",
			      "pysubs msub 68 1",
			      "pysubs msub 28 1",
			      "pysubs msub 53 1"]

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import MultiMuonFilter
topAlg += MultiMuonFilter()
topAlg.MultiMuonFilter.OutputLevel = 4

MultiMuonFilter = topAlg.MultiMuonFilter
MultiMuonFilter.Ptcut = 8000.
MultiMuonFilter.Etacut = 3.0
MultiMuonFilter.NMuons = 1

try:
    StreamEVGEN.RequireAlgs +=  [ "MultiMuonFilter" ]
except Exception, e:
    pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.000918
# 14.2.24.3 eff=5000/4891619=0.00102
