###############################################################
#
# Job options file
#
# AcerMC EW ttbb then
# Pythia with semileptonic ttbar decays and 
# ttbar plus jets filter
#
# Responsible person(s)
#   11th August, 2009: Simon Dean (sdean@hep.ucl.ac.uk)
#
#==============================================================
#
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

Pythia.PythiaCommand += [ "pyinit user acermc",
                          "pydat1 parj 90 20000.",
                          "pydat3 mdcy 15 1 0",
                          "pydat3 mdme 190 1 2",
                          "pydat3 mdme 191 1 2",
                          "pydat3 mdme 192 1 2",
                          "pydat3 mdme 194 1 2",
                          "pydat3 mdme 195 1 2",
                          "pydat3 mdme 196 1 2",
                          "pydat3 mdme 198 1 2",
                          "pydat3 mdme 199 1 2",
                          "pydat3 mdme 200 1 2",
                          "pydat3 mdme 206 1 3",
                          "pydat3 mdme 207 1 3",
                          "pydat3 mdme 208 1 0"]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.AcerMCEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'acermc.109626.ttbbEW'
evgenConfig.efficiency = 0.782
evgenConfig.minevents = 500

###############################################################
# Filter efficiency = 78.2%
# 959 events per ME tarball producing 750 output events.
# AcerMC + Herwig cross section = 0.1954 pb +- 0.0006
# Only 500 events will be used in further processing
# Lumi/500 events = 500/(eff(filter)*XS(AcerMC + Herwig)) = 3.272 fb^-1
#==============================================================

# Make truth jets
from JetRec.JetRecConf import JetAlgorithm
import JetRec.JetRecConf as JR
import JetSimTools.JetSimToolsConf as JST
topAlg += JetAlgorithm("TruthConeJets")
TruthConeJets = topAlg.TruthConeJets
TruthConeJets.JetCollectionName = "ConeTruthJets"
toollist = [
    JST.JetsFromTruthTool("TruthLoader"),
    JR.JetSignalSelectorTool("InitialEtCut"),
    JR.JetConeFinderTool("ConeFinder"),
    JR.JetSplitMergeTool("SplitMerge"),
    JR.JetSignalSelectorTool("FinalEtCut") ]
toollist[0].MinPt                = 0.*MeV
toollist[0].MaxEta               = 5.
toollist[0].IncludeMuons         = False
toollist[0].TruthCollectionName  = "GEN_EVENT"
toollist[0].OutputCollectionName = "ConeParticleJets"
toollist[1].UseTransverseEnergy = True
toollist[1].MinimumSignal       = 0.*MeV
toollist[2].ConeR    = 0.4
toollist[2].SeedPt   = 2.*GeV
toollist[4].UseTransverseEnergy = True
toollist[4].MinimumSignal       = 10.*GeV
TruthConeJets.AlgTools = [ t.getFullName() for t in toollist ]
for t in toollist :
    TruthConeJets += t
    
# Apply ttbar plus jets filter
from GeneratorFilters.GeneratorFiltersConf import TTbarPlusJetsFilter
topAlg += TTbarPlusJetsFilter()
TTbarPlusJetsFilter = topAlg.TTbarPlusJetsFilter
TTbarPlusJetsFilter.PtMinJet = 15000.
TTbarPlusJetsFilter.EtaMaxJet = 5.2
TTbarPlusJetsFilter.NbJetMin = 6
TTbarPlusJetsFilter.PtMinJetB = 15000.
TTbarPlusJetsFilter.EtaMaxJetB = 2.7
TTbarPlusJetsFilter.NbJetBMin = 3
TTbarPlusJetsFilter.NbLeptonMin = 0
TTbarPlusJetsFilter.SelectLepHadEvents = False
TTbarPlusJetsFilter.NbEventMax = 0
TTbarPlusJetsFilter.OutputLevel = 1

try:
    StreamEVGEN.RequireAlgs += [ "TTbarPlusJetsFilter" ]
except Exception, e:
    pass

#==============================================================
#
# End of job options file
#
###############################################################

