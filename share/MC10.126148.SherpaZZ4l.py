from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence('TopAlg')
from Sherpa_i.Sherpa_iConf import Sherpa_i
sherpa = Sherpa_i()

"""
(run){
  EXCLUSIVE_CLUSTER_MODE=1
  GENERATE_RESULT_DIRECTORY=1
  ACTIVE[25]=0
  ME_SIGNAL_GENERATOR=Amegic
  MASSIVE[15]=1

  CSS_AS_FS_FAC=0.4
  CSS_AS_IS_FAC=0.4 
}(run)

(model){
  MODEL         = SM
}(model)

(processes){
  Process 93 93 -> 11 -11 11 -11 93{1};
  Order_EW 4;
  CKKW sqr(20/E_CMS)
  Scales VAR{(PPerp2(p[2]+p[3])+Abs2(p[2]+p[3])+PPerp2(p[4]+p[5])+Abs2(p[4]+p[5]))/2.0}
  Print_Graphs 1
  End process;

  Process 93 93 -> 11 -11 13 -13 93{1};
  Order_EW 4;
  CKKW sqr(20/E_CMS)
  Scales VAR{(PPerp2(p[2]+p[3])+Abs2(p[2]+p[3])+PPerp2(p[4]+p[5])+Abs2(p[4]+p[5]))/2.0}
  Print_Graphs 1
  End process;

  Process 93 93 -> 11 -11 15 -15 93{1};
  Order_EW 4;
  CKKW sqr(20/E_CMS)
  Scales VAR{(PPerp2(p[2]+p[3])+Abs2(p[2]+p[3])+PPerp2(p[4]+p[5])+Abs2(p[4]+p[5]))/2.0}
  End process;

  Process 93 93 -> 13 -13 13 -13 93{1};
  Order_EW 4;
  CKKW sqr(20/E_CMS)
  Scales VAR{(PPerp2(p[2]+p[3])+Abs2(p[2]+p[3])+PPerp2(p[4]+p[5])+Abs2(p[4]+p[5]))/2.0}
  End process;

  Process 93 93 -> 13 -13 15 -15 93{1};
  Order_EW 4;
  CKKW sqr(20/E_CMS)
  Scales VAR{(PPerp2(p[2]+p[3])+Abs2(p[2]+p[3])+PPerp2(p[4]+p[5])+Abs2(p[4]+p[5]))/2.0}
  End process;

  Process 93 93 -> 15 -15 15 -15 93{1};
  Order_EW 4;
  CKKW sqr(20/E_CMS)
  Scales VAR{(PPerp2(p[2]+p[3])+Abs2(p[2]+p[3])+PPerp2(p[4]+p[5])+Abs2(p[4]+p[5]))/2.0}
  End process;
}(processes)

(selector){
  PT 11 1.0 E_CMS
  PT -11 1.0 E_CMS
  PT 13 1.0 E_CMS
  PT -13 1.0 E_CMS
  PT 15 1.0 E_CMS
  PT -15 1.0 E_CMS
  "m"  11,-11  12.0,E_CMS:12.0,E_CMS:12.0,E_CMS:12.0,E_CMS
  "m"  13,-13  12.0,E_CMS:12.0,E_CMS:12.0,E_CMS:12.0,E_CMS
  "m"  15,-15  12.0,E_CMS:12.0,E_CMS:12.0,E_CMS:12.0,E_CMS
}(selector)
"""

try:
    sherpa.Parameters += [ 'RUNDATA=%s' % runArgs.jobConfig[0] ]
except NameError:
    pass

topAlg += sherpa
from MC10JobOptions.SherpaFFEvgenConfig import evgenConfig

evgenConfig.efficiency = 0.9
evgenConfig.minevents = 5000
evgenConfig.weighting = 0
