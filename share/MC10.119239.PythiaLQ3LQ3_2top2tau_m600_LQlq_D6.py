###############################################################
#
# Start of job options file
#
###############################################################


import AthenaCommon.AtlasUnixGeneratorJob

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_PythiaD6_Common.py" )
#
#--------------------------------------------------------------
#
#  Full user control
#
Pythia.PythiaCommand += [ "pysubs msel 0" ]     #desired subprocesses have to be switched on in MSUB, i.e. full user control
#
#  LQ production
#
#++
#
#    qg -> lLQ
Pythia.PythiaCommand += [ "pysubs msub 162 0" ]   #turn on
#
#    gg -> LQLQbar
Pythia.PythiaCommand += [ "pysubs msub 163 1" ]  #turn on
#
#    qqbar -> LQLQbar
Pythia.PythiaCommand += [ "pysubs msub 164 1" ]    #turn on
#
#  LQ mass
Pythia.PythiaCommand += [ "pydat2 pmas 42 1 600.0" ]     #set mass of LQ
Pythia.PythiaCommand += [ "pydat3 mdcy 42 2 539" ]
Pythia.PythiaCommand += [ "pydat3 mdcy 42 3 2" ]
#
#  To avoid problems in MC generation (mass ranges)
# range of allowed mass values of the two (or one) resonances produced in a "true" 2->2 process

Pythia.PythiaCommand += [ "pysubs ckin 41 500" ]  #m1>150GeV
Pythia.PythiaCommand += [ "pysubs ckin 42 700" ]  #m1<350GeV
Pythia.PythiaCommand += [ "pysubs ckin 43 500" ]  #m2>150GeV
Pythia.PythiaCommand += [ "pysubs ckin 44 700" ]  #m2<350GeV

#
#  Branching fraction for the decay described below
Pythia.PythiaCommand += [ "pydat3 brat 539 1.0" ]  #set branching ration of LQ

#
#  This is relevant to both the decay AND production mechanism for single LQ
#
Pythia.PythiaCommand += [ "pydat3 kfdp 539 1 6" ]  #decay product top
Pythia.PythiaCommand += [ "pydat3 kfdp 539 2 15" ] #decay product tau
#Pythia.PythiaCommand += [ "pydat3 kfdp 539 1 2" ]
#Pythia.PythiaCommand += [ "pydat3 kfdp 539 2 11" ]
#Pythia.PythiaCommand += [ "pydat3 kfdp 540 1 1" ]
#Pythia.PythiaCommand += [ "pydat3 kfdp 540 2 12" ]

#LQ->top + tau
#KFDP(539,1) = 6 ! quark flavor
#KFDP(539,2) = -15 ! lepton flavor

#LQ->b + nu_tau
#KFDP(539,1)=5         ! LQ->b+nu_tau
#KFDP(539,2)=16

#!


# turns FSR off
#Pythia.PythiaCommand += [ "pydat1 mstp 71 0" ]


#
#  Coupling: lambda=sqrt(4pi*alpha_em)
#
#  This choice of lambda was made to make single LQ production to have approx. same cross section as pair production
#
Pythia.PythiaCommand += [ "pydat1 paru 151 0.01" ]
#
#-------------------------------------------------------------
#
#  In order to prevent double counting in Pythia when PHOTOS is used
#
Pythia.PythiaCommand += [ "pydat1 parj 90 20000" ]
#
#  Tell Pythia NOT to decay taus
#
Pythia.PythiaCommand += [ "pydat3 mdcy 15 1 0" ]
#
#-------------------------------------------------------------
#
#  Print the event listing for events x though y:
#
Pythia.PythiaCommand += [ "pyinit dumpr 1 20" ]
#
#-------------------------------------------------------------
#
# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

#
# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


from GeneratorFilters.GeneratorFiltersConf import TTbarWToLeptonFilter
topAlg += TTbarWToLeptonFilter()
TTbarWToLeptonFilter = topAlg.TTbarWToLeptonFilter
TTbarWToLeptonFilter.Ptcut = 1.
try:
      StreamEVGEN.RequireAlgs = [ "TTbarWToLeptonFilter" ]
except Exception, e:
      pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
#evgenConfig.efficiency = 0.95
evgenConfig.efficiency = 0.55

#==============================================================
#
# End of job options file
#
###############################################################
