###############################################################
#
# Job options file
#
# Pythia gg/VBF H->gamgam (mH=120GeV) with 2 photon EF
#
# Responsible person(s)
#   24 June, 2008-xx xxx, 20xx: Junichi TANAKA (Junichi.Tanaka@cern.ch)
#
# Adapted for Pythia8:
#   Robindra Prabhu (prabhu@cern.ch), April 2011
#==============================================================

# ... Main generator: Pythia8

MessageSvc = Service( "MessageSvc" )
#MessageSvc.OutputLevel = 4

from AthenaCommon.AppMgr import ServiceMgr as svcMgr

import AthenaServices

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

# ... Pythia
include ("MC10JobOptions/MC10_Pythia8_Common.py")

Pythia8.Commands += ["HiggsSM:gg2H = on"]
Pythia8.Commands += ["HiggsSM:ff2Hff(t:WW) = on"]
Pythia8.Commands += ["HiggsSM:ff2Hff(t:ZZ) = on"]
Pythia8.Commands += ["25:m0 = 120."] # Higgs mass 120. GeV
Pythia8.Commands += ["25:onMode = off"] # switch off all Higgs decays
Pythia8.Commands += ["25:onIfAny = 22"] # switch on Higgs->gamma+gamma

# Filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import PhotonFilter
topAlg += PhotonFilter()

PhotonFilter = topAlg.PhotonFilter
PhotonFilter.Ptcut = 20000.
PhotonFilter.Etacut = 2.7
PhotonFilter.NPhotons = 2

try:
     StreamEVGEN.RequireAlgs +=  [ "PhotonFilter" ]
except Exception, e:
     pass

from MC10JobOptions.Pythia8EvgenConfig import evgenConfig
evgenConfig.efficiency = 0.70
# 14.1.0.4 but ignore "TestHepMC WARNING Unstable particle with no pareent found"
# 5206/6616=.78688029020556227327
# 5206/6616*0.9=.70819226118500604594
#==============================================================
#
# End of job options file
#
###############################################################
