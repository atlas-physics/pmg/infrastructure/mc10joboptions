#------------------------------------------------------------------------------------
#
# ttbar no-all-had Perugia0 tune
#
#------------------------------------------------------------------------------------

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_PythiaPerugia0_Common.py" )

Pythia.PythiaCommand += [ "pyinit user acermc",
			 "pydat1 parj 90 20000.",
			 "pydat3 mdcy 15 1 0" ]

include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

from MC10JobOptions.AcerMCEvgenConfig import evgenConfig

evgenConfig.inputfilebase = 'acermc'
try:
  if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.acermc37.117240.CTEQ5L_tt_7TeV.TXT.v1'
  if runArgs.ecmEnergy == 10000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.acermc37.117240.CTEQ5L_tt_10TeV.TXT.v1'
except NameError:
  pass

evgenConfig.efficiency = 0.95
