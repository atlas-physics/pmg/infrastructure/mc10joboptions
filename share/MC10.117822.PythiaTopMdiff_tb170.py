# example MC11 jO file that shows how to use PythiaTopMdiff class
# PythiaTopMdiff class:
# In pyofsh.F and pyscat.F:
# - for  qqbar (gg) -> QQbar [ISUB 81 (82)]:     
# -- pyofsh.F : ISUB 81,82: set top partner to 3000006, spectrum to be filled via SLHA
# -- pyscat.F : account for qqbar (gg) -> QQbar vs qqbar (gg) -> QbarQ 
# 5GeV mass Difference, Top = 175GeV, Antitop = 170GeV 

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_PythiaTopMdiff_Common.py" )

#----------------------------------------------------------------------------------------------------------------------
# enable using the modifications in 
#(default: UseTopMdiff=0; do not use TopMdiff modifications in pyofsh.F and pyscat.F)
#(UseTopMdiff=1; use modified )
Pythia.UseTopMdiff=1

# relevant proc. setup
Pythia.PythiaCommand += [ "pypars mstp 7 6",    # Heavy Flavour production top 
                                  "pymssm imss 1 11" ]  # Turn on use of SLHA

Pythia.SusyInputFile = "t_tb170_0.slha"

Pythia.PythiaCommand +=  ["pydat1 parj 90 200000",
                          "pydat3 mdcy 15 1 0"]
                                
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
topAlg += LeptonFilter()

LeptonFilter = topAlg.LeptonFilter
LeptonFilter.Ptcut = 5000.
LeptonFilter.Etacut = 2.8

try:
    StreamEVGEN.RequireAlgs +=  [ "LeptonFilter" ]
except Exception, e:
    pass
            

from MC10JobOptions.PythiaTopMdiffEvgenConfig import evgenConfig
#evgenConfig.minevents=5
evgenConfig.auxfiles += [ 't_tb170_0.slha' ]
