# Jet sample pT>17GeV
#--------------------------------------------------------------
# Generator Options
# Monika Weilers/ Ian Hinchliffe
# Borut Kersevan: added fix for procs 81,82 for top prod
#                 MSTP(7)=6 has to be set
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

Pythia.PythiaCommand += [
			      "pysubs msel 0",
			      "pysubs ckin 3 15.",
			      "pysubs msub 11 1",
			      "pysubs msub 12 1",
			      "pysubs msub 13 1",
			      "pysubs msub 28 1",
			      "pysubs msub 53 1",
			      "pysubs msub 68 1",
		 	      "pysubs msub 81 1",
			      "pysubs msub 82 1",
			      "pysubs msub 14 1",
			      "pysubs msub 29 1",
			      "pysubs msub 1 1",
			      "pysubs msub 2 1",
			      "pypars mstp 7 6"]


#--------------------------------------------------------------
# Filter Options ('standard' jet filter)
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import JetFilter
topAlg += JetFilter()

JetFilter = topAlg.JetFilter
JetFilter.JetNumber = 1
JetFilter.EtaRange = 2.7
JetFilter.JetThreshold = 17000.;  # Note this is 17 GeV
JetFilter.JetType=False; #true is a cone, false is a grid
JetFilter.GridSizeEta=2; # sets the number of (approx 0.06 size) eta
JetFilter.GridSizePhi=2; # sets the number of (approx 0.06 size) phi cells

#--------------------------------------------------------------
# Pool Output Options
#--------------------------------------------------------------
try:
     StreamEVGEN.RequireAlgs += ["JetFilter"]
except Exception, e:
     pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.075

#==============================================================
#
# End of job options file
#
###############################################################

