## Job options file for Herwig++, top-antitop production

## Get a handle on the top level algorithms' sequence
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

try:
     include ( "MC10JobOptions/MC10_Herwigpp_Common.py" )
except NameError:
     # needed (dummy) default
     # from Herwigpp_i.Herwigpp_iConf import Herwigpp
     # topAlg += Herwigpp()
     cmds =  ""

## Add to commands
cmds += """
# top-antitop production
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEHeavyQuark
set /Herwig/MatrixElements/MEHeavyQuark:QuarkType 6

#switch off tau polarisation
set /Herwig/Decays/Tau1Meson:PolarizationOption Force
set /Herwig/Decays/Tau1Meson:TauMinusPolarization 0.0
set /Herwig/Decays/Tau1Meson:TauPlusPolarization 0.0

set /Herwig/Decays/Tau2Leptons:PolarizationOption Force
set /Herwig/Decays/Tau2Leptons:TauMinusPolarization 0.0
set /Herwig/Decays/Tau2Leptons:TauPlusPolarization 0.0

set /Herwig/Decays/Tau2Meson:PolarizationOption Force
set /Herwig/Decays/Tau2Meson:TauMinusPolarization 0.0
set /Herwig/Decays/Tau2Meson:TauPlusPolarization 0.0

set /Herwig/Decays/Tau2MesonPhoton:PolarizationOption Force
set /Herwig/Decays/Tau2MesonPhoton:TauMinusPolarization 0.0
set /Herwig/Decays/Tau2MesonPhoton:TauPlusPolarization 0.0

set /Herwig/Decays/Tau3Kaon:PolarizationOption Force
set /Herwig/Decays/Tau3Kaon:TauMinusPolarization 0.0
set /Herwig/Decays/Tau3Kaon:TauPlusPolarization 0.0

set /Herwig/Decays/Tau3Meson:PolarizationOption Force
set /Herwig/Decays/Tau3Meson:TauMinusPolarization 0.0
set /Herwig/Decays/Tau3Meson:TauPlusPolarization 0.0

set /Herwig/Decays/Tau3Pion:PolarizationOption Force
set /Herwig/Decays/Tau3Pion:TauMinusPolarization 0.0
set /Herwig/Decays/Tau3Pion:TauPlusPolarization 0.0

set /Herwig/Decays/Tau4Pion:PolarizationOption Force
set /Herwig/Decays/Tau4Pion:TauMinusPolarization 0.0
set /Herwig/Decays/Tau4Pion:TauPlusPolarization 0.0

set /Herwig/Decays/Tau5Pion:PolarizationOption Force
set /Herwig/Decays/Tau5Pion:TauMinusPolarization 0.0
set /Herwig/Decays/Tau5Pion:TauPlusPolarization 0.0

set /Herwig/Decays/TauKPi:PolarizationOption Force
set /Herwig/Decays/TauKPi:TauMinusPolarization 0.0
set /Herwig/Decays/TauKPi:TauPlusPolarization 0.0

set /Herwig/Decays/Tau1Vector:PolarizationOption Force
set /Herwig/Decays/Tau1Vector:TauMinusPolarization 0.0
set /Herwig/Decays/Tau1Vector:TauPlusPolarization 0.0
"""

## Set commands
topAlg.Herwigpp.Commands = cmds.splitlines()

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import TTbarWToLeptonFilter
topAlg += TTbarWToLeptonFilter()

TTbarWToLeptonFilter = topAlg.TTbarWToLeptonFilter
TTbarWToLeptonFilter.Ptcut = 1.

try:
     StreamEVGEN.RequireAlgs = [ "TTbarWToLeptonFilter" ]
except Exception, e:
     pass
               
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.HerwigppEvgenConfig import evgenConfig
#evgenConfig.efficiency = 0.9
#evgenConfig.minevents=100

#Herwigpp.InFileDump = "myhppconfig.in"

#==============================================================
#
# End of job options file
#
###############################################################
