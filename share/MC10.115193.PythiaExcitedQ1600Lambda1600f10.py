from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

#--------------------------------------------------------------
# File prepared by Sing-leung Cheung [slcheung at physics d utoronto d ca]
# Excited quark production with pythia 
#--------------------------------------------------------------

######################################################
#                                                    #
#           Algorithm Private Options                #
#                                                    #
######################################################

#Excited Quark Mass (in GeV)
M_ExQ = 1600.0

#Mass Scale parameter (Lambda, in GeV)
M_Lam = 1600.0

#Coupling constant
f = 1.0

include ( "MC10JobOptions/MC10_Pythia_Common.py" )
Pythia.PythiaCommand +=["pysubs msel 0",
                        "pysubs msub 147 1",
                        "pysubs msub 148 1",
                        "pydat2 pmas 4000001 1 "+str(M_ExQ),
                        "pydat2 pmas 4000002 1 "+str(M_ExQ),
                        "pytcsm rtcm 41 "+str(M_Lam),
                        "pytcsm rtcm 43 "+str(f),     #coupling strength of SU(2)
                        "pytcsm rtcm 44 "+str(f),     #coupling strength of U(1)
                        "pytcsm rtcm 45 "+str(f),     #coupling strength of SU(3)
                        # select ExQ decays to all channels
                        "pydat3 mdme 4071 1 1", #d*->g   d
                        "pydat3 mdme 4072 1 1", #d*->gam d
                        "pydat3 mdme 4073 1 1", #d*->Z0  d
                        "pydat3 mdme 4074 1 1", #d*->W-  u
                        "pydat3 mdme 4075 1 1", #u*->g   u
                        "pydat3 mdme 4076 1 1", #u*->gam u
                        "pydat3 mdme 4077 1 1", #u*->Z0  u
                        "pydat3 mdme 4078 1 1", #u*->W+  d
                        "pydat3 mdcy 15 1 0",     # Turn off tau decays (Tauola takes care of it)
                        "pydat1 parj 90 20000"    # Set QED FSR to 20000 GeV (='infinity', Photos takes care of it)
                        ]


# Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )
# Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9


#==============================================================
#
# End of job options file
#
###############################################################
