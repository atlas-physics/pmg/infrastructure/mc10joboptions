###############################################################
#
# Job options file
#
#  Donnchadha Quilty (donnchadha.quilty@cern.ch)
# gg --> t tbar H --> l- nubar bbar q  qbar' b b bbar
#
#######################################################

#
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

#
# ... Pythia
include ( "MC10JobOptions/MC10_Pythia_Common.py" )


Pythia.PythiaCommand += [
                              "pysubs msel 0",
                              "pypars mstp 7 6",        # heavy flavour 6 (top)
                              "pysubs msub 121 1",      #sub-process 121 gg->QQh ON
                              "pysubs msub 122 1",      #sub-process 122 qq->QQh ON
                              "pydat2 pmas 25 1 140.",  ## Higgs mass = 140Gev
                              "pydat3 mdme 190 1 2",
                              "pydat3 mdme 191 1 2",
                              "pydat3 mdme 192 1 2",
                              "pydat3 mdme 194 1 2",
                              "pydat3 mdme 195 1 2",
                              "pydat3 mdme 196 1 2",
                              "pydat3 mdme 198 1 2",
                              "pydat3 mdme 199 1 2",
                              "pydat3 mdme 200 1 2",
                              "pydat3 mdme 206 1 3",
                              "pydat3 mdme 207 1 3",
                              "pydat3 mdme 208 1 3",
                              "pydat3 mdme 210 1 0",
                              "pydat3 mdme 211 1 0",
                              "pydat3 mdme 212 1 0",
                              "pydat3 mdme 213 1 0",
                              "pydat3 mdme 214 1 1",
                              "pydat3 mdme 215 1 0",
                              "pydat3 mdme 218 1 0",
                              "pydat3 mdme 219 1 0",
                              "pydat3 mdme 220 1 0",
                              "pydat3 mdme 222 1 0",
                              "pydat3 mdme 223 1 0",
                              "pydat3 mdme 224 1 0",
                              "pydat3 mdme 225 1 0",
                              "pydat3 mdme 226 1 0",
                              "pydat1 parj 90 20000.",  ## Turn off FSR
                              "pydat3 mdcy 15 1 0" ]     ## Turn off tau decays

# ... Tauola

include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )


# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


#--------------------------------------------------------------
# Filter
# efficiency:
# LeptonFilter         INFO  Events passed = 4466    Events Failed = 798
# efficiency = 0.848
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
topAlg += LeptonFilter()

LeptonFilter = topAlg.LeptonFilter
LeptonFilter.Ptcut = 10000.
LeptonFilter.Etacut = 5.0

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------
try:
    StreamEVGEN.RequireAlgs +=  [ "LeptonFilter" ]
except Exception, e:
    pass


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------

from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.848*0.90

#==============================================================
#
# End of job options file
#
###############################################################
