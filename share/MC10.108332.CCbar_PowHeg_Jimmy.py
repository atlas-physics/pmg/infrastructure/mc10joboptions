###############################################################
#
# Job options file for POWHEG with Herwig+Jimmy
# Based on sample 105860
# M. Flowerdew, Jun 2010 <flowerde@cern.ch>
#
#==============================================================
#--------------------------------------------------------------
# General Application Configuration options
#--------------------------------------------------------------

from AthenaCommon.AlgSequence import AlgSequence
topAlg=AlgSequence()

try:
    if runArgs.ecmEnergy == 7000.0:
        include ( "MC10JobOptions/MC10_PowHegJimmy_Common_7TeV.py" )
    if runArgs.ecmEnergy == 8000.0:
        include ( "MC10JobOptions/MC10_PowHegJimmy_Common_8TeV.py" )
    if runArgs.ecmEnergy == 10000.0:
        include ( "MC10JobOptions/MC10_PowHegJimmy_Common.py" )
    if runArgs.ecmEnergy == 14000.0:
        include ( "MC10JobOptions/MC10_PowHegJimmy_Common_14TeV.py" )
except NameError:
    # needed (dummy) default
    from Herwig_i.Herwig_iConf import Herwig
    topAlg += Herwig()
    Herwig = topAlg.Herwig

Herwig.HerwigCommand += [ "taudec TAUOLA" ]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import ElectronFilter
topAlg += ElectronFilter()

ElectronFilter = topAlg.ElectronFilter
ElectronFilter.Ptcut  = 5000.
ElectronFilter.Etacut = 2.7

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------
try:
     StreamEVGEN.RequireAlgs += [ "ElectronFilter" ]
except Exception, e:
     pass

from MC10JobOptions.EvgenConfig import evgenConfig

evgenConfig.generators += [ "Lhef", "Herwig" ]

#dummy needed
evgenConfig.inputfilebase = 'PowHeg'
try:
    if runArgs.ecmEnergy == 7000.0:
        evgenConfig.inputfilebase = 'group10.phys-gener.PowHeg_hvq_pl4.108332.CCbar.TXT.v1'
    if runArgs.ecmEnergy == 8000.0:
        evgenConfig.inputfilebase = 'group10.phys-gener.PowHeg_hvq_pl4.108332.CCbar_8TeV.TXT.v1'
    if runArgs.ecmEnergy == 10000.0:
        evgenConfig.inputfilebase = 'group10.phys-gener.PowHeg_hvq_pl4.108332.CCbar_10TeV.TXT.v1'
    if runArgs.ecmEnergy == 14000.0:
        evgenConfig.inputfilebase = 'group10.phys-gener.PowHeg_hvq_pl4.108332.CCbar_14TeV.TXT.v1'
except NameError:
  pass

evgenConfig.efficiency = 0.0004 ## Actual efficiency is ~0.04-0.05%

evgenConfig.minevents = 500 ## Low efficiency => fewer events per job

#---------------------------------------------------------------
# Ntuple service output
#---------------------------------------------------------------
#
#==============================================================
#
# End of job options file
#
###############################################################


###############################################################
#
# EXAMPLE FOR POWHEG INPUT FILE
#
###############################################################
# !Heavy flavour production parameters
# 
# maxev 100000   ! number of events to be generated
# ih1   1        ! hadron 1
# ih2   1        ! hadron 2
# ndns1 191      ! pdf for hadron 1
# ndns2 191      ! pdf for hadron 2
# lhans1 21100   ! lhapdf for hadron 1 (?)
# lhans2 21100   ! lhapdf for hadron 2 (?)
# ebeam1 3500    ! energy of beam 1
# ebeam2 3500    ! energy of beam 2
# qmass  1.55    ! mass of heavy quark in GeV
# facscfact 1    ! factorization scale factor: mufact=muref*facscfact
# renscfact 1    ! renormalization scale factor: muren=muref*renscfact
# bbscalevar 1    ! use variable re. and fct. scales
# 
# ! Parameters to allow-disallow use of stored data
# use-old-grid 0    ! if 0 use old grid if file pwggrids.dat is present (# 0: regenerate)
# use-old-ubound 0  ! if 0 use norm of upper bounding function stored in pwgubound.dat, if present; # 0: regenerate
# 
# ncall1 10000   ! number of calls for initializing the integration grid
# itmx1 5        ! number of iterations for initializing the integration grid
# ncall2 100000  ! number of calls for computing the integral and finding upper bound
# itmx2 5        ! number of iterations for computing the integral and finding upper bound
# foldx   5      ! number of folds on x integration
# foldy   5      ! number of folds on y integration
# foldphi 2      ! number of folds on phi integration
# nubound 100000  ! number of bbarra calls to setup norm of upper bounding function
# iymax 1        ! <= 10, normalization of upper bounding function in iunorm X iunorm square in y, log(m2qq)
# ixmax 10        ! <= 10, normalization of upper bounding function in iunorm X iunorm square in y, log(m2qq)
# xupbound 3      ! increase upper bound for radiation generation
