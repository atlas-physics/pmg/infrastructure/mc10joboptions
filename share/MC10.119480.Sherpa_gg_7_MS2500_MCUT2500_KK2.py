from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence('TopAlg')
from Sherpa_i.Sherpa_iConf import Sherpa_i
sherpa = Sherpa_i()
topAlg += sherpa
from MC10JobOptions.SherpaFEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'group10.phys-gener.sherpa010203.119480.gg_7_MS2500_MCUT2500_KK2_7TeV.TXT.v1'
evgenConfig.efficiency = 0.9
evgenConfig.weighting = 0
