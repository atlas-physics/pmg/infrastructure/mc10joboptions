###############################################################
#
# Job options file
#
# Pythia Z'->tautau (inclusive), m(Z')=500 GeV w/o GEF
#    Perugia2010 tune
#
# Perugia2010 parameters as in arXiv:1005.3457v1 [hep-ph]
# Perugia2010 (PYTUNE 327; from 6.423 onwards)
# reference: MC10.107382.Pythia_Zprime_tautau_SSM500.py
#
# Responsible person(s)
#   23 May, 2011: Martin Flechl (Martin.Flechl@cern.ch)
#
#==============================================================


from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

#--------------------------------------------------------------
# Algorithms Private Options
#--------------------------------------------------------------

include ( "MC10JobOptions/MC10_PythiaPerugia2010_Common.py" )

# Zprime resonance mass (in GeV)
ZprimeMass = 500

# Minimum mass for Drell-Yan production (in GeV)
ckin1 = 250

# Z prime specific parameters for pythia :
Pythia.PythiaCommand += [
       "pysubs msel 0",
       "pysubs msub 141 1",   # Z',Z,g with interference
       # Z' decays - quarks
       "pydat3 mdme 289 1 0",
       "pydat3 mdme 290 1 0",
       "pydat3 mdme 291 1 0",
       "pydat3 mdme 292 1 0",
       "pydat3 mdme 293 1 0",
       "pydat3 mdme 294 1 0",
       # Z' decays - leptons
       "pydat3 mdme 297 1 0",
       "pydat3 mdme 298 1 0",
       "pydat3 mdme 299 1 0", #Z'-> mu+ mu-
       "pydat3 mdme 300 1 0",
       "pydat3 mdme 301 1 1", #Z'-> tau+ tau-
       "pydat3 mdme 302 1 0",
       # tau decays are left open
       "pysubs ckin 1 "+str(ckin1),  # sqrhat > 500
       #    "pysubs ckin 13 -3",  #
       #    "pysubs ckin 14 3",   # eta cuts
       #    "pysubs ckin 15 -3",  # |eta| < 3
       #    "pysubs ckin 16 3",   #
       "pydat1 mstu 1 0",
       "pydat1 mstu 2 0",
       "pydat2 pmas 32 1 "+str(ZprimeMass)
       ]

Pythia.PythiaCommand +=  ["pydat1 parj 90 200000",
                          "pydat3 mdcy 15 1 0"] 

include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.95

#==============================================================
#
# End of job options file
#
###############################################################
