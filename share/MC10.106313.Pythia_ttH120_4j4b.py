###############################################################
#
# Job options file
#
# gg --> t tbar H(120) --> q qbar' b   q''  qbar''' bbar    b bbar
#
# Responsible person(s)
#   6 Oct, 2008-xx xxx, 20xx: C. Collins-Tooth (c.collins-tooth@physics.gla.ac.uk)
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_Pythia_Common.py" )
Pythia.PythiaCommand += ["pysubs msel 0",          # Users decay choice.
                         "pydat1 parj 90 20000",   # Turn off FSR.
                         "pydat3 mdcy 15 1 0",     # Turn off tau decays.
                         "pypars mstp 7 6",        # heavy flavour 6 (top)
                         "pysubs msub 121 1",      #sub-process 121 gg->QQh ON
                         "pysubs msub 122 1",      #sub-process 122 qq->QQh ON
                         "pydat2 pmas 25 1 120.",  #Mh=120GeV
                         "pydat3 mdme 190 1 1",
                         "pydat3 mdme 191 1 1",
                         "pydat3 mdme 192 1 1",
                         "pydat3 mdme 194 1 1",
                         "pydat3 mdme 195 1 1",
                         "pydat3 mdme 196 1 1",
                         "pydat3 mdme 198 1 1",
                         "pydat3 mdme 199 1 1",
                         "pydat3 mdme 200 1 1",
                         "pydat3 mdme 206 1 0",
                         "pydat3 mdme 207 1 0",
                         "pydat3 mdme 208 1 0",
                         "pydat3 mdme 210 1 0",
                         "pydat3 mdme 211 1 0",
                         "pydat3 mdme 212 1 0",
                         "pydat3 mdme 213 1 0",
                         "pydat3 mdme 214 1 1",
                         "pydat3 mdme 215 1 0",
                         "pydat3 mdme 218 1 0",
                         "pydat3 mdme 219 1 0",
                         "pydat3 mdme 220 1 0",
                         "pydat3 mdme 222 1 0",
                         "pydat3 mdme 223 1 0",
                         "pydat3 mdme 224 1 0",
                         "pydat3 mdme 225 1 0",
                         "pydat3 mdme 226 1 0"
                         ]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# JetRec on Truth
#--------------------------------------------------------------
try:
    from JetRec.JetGetters import *
    c4=make_StandardJetGetter('Cone',0.4,'Truth')
    c4alg = c4.jetAlgorithmHandle()
    c4alg.JetConeFinder.SeedPt = 2.*GeV
    c4alg.JetInitialEtCut.UseTransverseEnergy = True
    c4alg.JetInitialEtCut.MinimumSignal = 0.*MeV
    c4alg.JetFinalEtCut.UseTransverseEnergy =  True
    c4alg.JetFinalEtCut.MinimumSignal = 10.*GeV
except Exception, e:
    pass
#from JetRec.JetRecConf import JetAlgorithm
#import JetRec.JetRecConf as JR
#import JetSimTools.JetSimToolsConf as JST
#topAlg += JetAlgorithm("TruthConeJets")
#TruthConeJets = topAlg.TruthConeJets
#TruthConeJets.JetCollectionName = "ConeTruthJets"
#toollist = [
#    JST.JetsFromTruthTool("TruthLoader"),
#    JR.JetSignalSelectorTool("InitialEtCut"),
#    JR.JetConeFinderTool("ConeFinder"),
#    JR.JetSplitMergeTool("SplitMerge"),
#    JR.JetSignalSelectorTool("FinalEtCut") ]
#toollist[0].MinPt                = 0.*MeV
#toollist[0].MaxEta               = 5.
#toollist[0].IncludeMuons         = False
#toollist[0].TruthCollectionName  = "GEN_EVENT"
#toollist[0].OutputCollectionName = "ConeParticleJets"
#toollist[1].UseTransverseEnergy = True
#toollist[1].MinimumSignal       = 0.*MeV
#toollist[2].ConeR    = 0.4
#toollist[2].SeedPt   = 2.*GeV
#toollist[4].UseTransverseEnergy = True
#toollist[4].MinimumSignal       = 10.*GeV
#TruthConeJets.AlgTools = [ t.getFullName() for t in toollist ]
#for t in toollist :
#    TruthConeJets += t
    
#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import TTbarPlusJetsFilter
topAlg += TTbarPlusJetsFilter()
TTbarPlusJetsFilter = topAlg.TTbarPlusJetsFilter
TTbarPlusJetsFilter.PtMinJet = 15000.
TTbarPlusJetsFilter.EtaMaxJet = 5.2
TTbarPlusJetsFilter.NbJetMin = 6
TTbarPlusJetsFilter.PtMinJetB = 15000.
TTbarPlusJetsFilter.EtaMaxJetB = 2.7
TTbarPlusJetsFilter.NbJetBMin = 3
TTbarPlusJetsFilter.NbLeptonMin = 0
TTbarPlusJetsFilter.SelectLepHadEvents = False
TTbarPlusJetsFilter.NbEventMax = 0
TTbarPlusJetsFilter.InputJetContainer = "Cone4TruthJets"

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------
try:
    StreamEVGEN.RequireAlgs +=  [ "TTbarPlusJetsFilter" ]
except Exception, e:
    pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.8
#==============================================================
#
# End of job options file
#
###############################################################
