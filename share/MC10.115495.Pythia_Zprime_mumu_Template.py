# Zprime production with pythia
# remove Z',Z,g interference and apply mass cut OSC 12/2010
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

#--------------------------------------------------------------
# Algorithms Private Options
#--------------------------------------------------------------

# Zprime resonance mass (in GeV)
ZprimeMass = 1000

# Minimum mass for Drell-Yan production (in GeV)
ckin1 = 10

include ( "MC10JobOptions/MC10_PythiaResMod_Common.py" )

# enable using the modified pysgex.F
#(default: UseResMod=0; do not use modified pysgex.F)
#(UseResMod=1; use modified pysgex.F)
PythiaResMod.UseResMod=1

#other commands for PythiaResMod are as they are for Pythia alg.
# Z prime specific parameters for pythia :
PythiaResMod.PythiaCommand += [
       "pysubs msel 0",
       "pysubs msub 141 1",   # Z',Z,g with interference
       "pypars mstp 44 3",    # only Z'
       # Z' decays - quarks
       "pydat3 mdme 289 1 0",
       "pydat3 mdme 290 1 0",
       "pydat3 mdme 291 1 0",
       "pydat3 mdme 292 1 0",
       "pydat3 mdme 293 1 0",
       "pydat3 mdme 294 1 0",
       # Z' decays - leptons
       "pydat3 mdme 297 1 0",
       "pydat3 mdme 298 1 0",
       "pydat3 mdme 299 1 1", #Z'-> mu+ mu-
       "pydat3 mdme 300 1 0",
       "pydat3 mdme 301 1 0", #Z'-> tau+ tau-
       "pydat3 mdme 302 1 0",
       # tau decays are left open
       "pysubs ckin 1 "+str(ckin1),
       #    "pysubs ckin 13 -3",  #
       #    "pysubs ckin 14 3",   # eta cuts
       #    "pysubs ckin 15 -3",  # |eta| < 3
       #    "pysubs ckin 16 3",   #
       "pydat1 mstu 1 0",
       "pydat1 mstu 2 0",
       "pydat2 pmas 32 1 "+str(ZprimeMass)
       ]

PythiaResMod.PythiaCommand +=  ["pydat1 parj 90 200000",
                                "pydat3 mdcy 15 1 0"] 
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaResModEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9

#==============================================================
#
# End of job options file
#
###############################################################
