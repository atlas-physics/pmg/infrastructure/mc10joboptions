####################################################################################
####################################################################################
#                                                                                  #
#   same sign ditop using Pythia by zhichao.                                       #
#                                                                                  #
####################################################################################
####################################################################################

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

####################################################################################

Pythia.PythiaCommand += [
	"pysubs msel 39",
# IMSS(1)=1 : general MSSM simulation (model parameters set by array RMSS) :
	"pymssm imss 1 11",
        "pydat3 mdcy 15 1 0" # Turn off tau decays
                       ]

#####################################################################################
# PYTHIA CONTROL OUTPUT SETTING :                                                   #
#####################################################################################
Pythia.PythiaCommand += [
# here added to print out the set parameters
	"pyinit pylisti 13"]

Pythia.SusyInputFile = "susy_comp400.txt" #SLHA input file

#####################################################################################
# INCLUDES :                                                                        #
#####################################################################################
# ... Tauola 
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )  
# ... Photos 
include ( "MC10JobOptions/MC10_Photos_Fragment.py" ) 

#####################################################################################
# FILTERS :                                                                         #
#####################################################################################
# Multilepton Filter providing the lepton-lepton final state,
# to be compatible to the BG CSC.006353.AcerMC_Zbb_tautau.py 
from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
topAlg += MultiLeptonFilter()

MultiLeptonFilter = topAlg.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 5000.
MultiLeptonFilter.Etacut = 2.7
MultiLeptonFilter.NLeptons = 2

try:
     StreamEVGEN.RequireAlgs = [ "MultiLeptonFilter" ] 
except Exception, e:
     pass

#####################################################################################
# FILTER EFFICIENCY :                                                               #
#####################################################################################
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.013

# SUSYEvgenConfig
from MC10JobOptions.SUSYEvgenConfig import evgenConfig

