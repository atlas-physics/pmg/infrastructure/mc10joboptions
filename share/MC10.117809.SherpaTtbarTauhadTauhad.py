from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence('TopAlg')
from Sherpa_i.Sherpa_iConf import Sherpa_i
sherpa = Sherpa_i()

"""
(processes){
  # tau tau
  Process : 93 93 ->  6[a] -6[b] 93{2}
  DecayOS : 6[a] -> 24[c] 5
  DecayOS : -6[b] -> -24[d] -5
  DecayOS : 24[c] -> -15 16
  DecayOS : -24[d] -> 15 -16
  CKKW sqr(30/E_CMS)
  Scales VAR{MPerp2(p[2]+p[3]+p[4])+MPerp2(p[5]+p[6]+p[7])} {6}
  Scales VAR{MPerp2(p[2]+p[3]+p[4])+MPerp2(p[5]+p[6]+p[7])+MPerp2(p[8])} {7}
  Scales VAR{MPerp2(p[2]+p[3]+p[4])+MPerp2(p[5]+p[6]+p[7])+MPerp2(p[8])+MPerp2(p[9])} {8}
  Max_Epsilon 1e-6
  End process
}(processes)
"""
sherpa.Parameters += [ 'WIDTH[6]=1.47211' ]
sherpa.Parameters += [ 'WIDTH[24]=2.035169' ]
sherpa.Parameters += [ 'WRITE_MAPPING_FILE=3' ]
sherpa.Parameters += [ 'MASSIVE[15]=1' ]
sherpa.Parameters += [ 'ME_SIGNAL_GENERATOR=Comix' ]

# Take tau BR's into account:
sherpa.CrossSectionScaleFactor=0.419904

try:
    sherpa.Parameters += [ 'RUNDATA=%s' % runArgs.jobConfig[0] ]
except NameError:
    pass

topAlg += sherpa
from MC10JobOptions.SherpaFEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'group10.phys-gener.sherpa010203.117809.SherpaTtbarTauhadTauhad_7TeV.TXT.v1'
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 5000
evgenConfig.weighting = 0
