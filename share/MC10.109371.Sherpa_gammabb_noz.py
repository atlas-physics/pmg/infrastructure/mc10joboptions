###############################################################
# Job options file for Sherpa on-the-fly interface
# gamma bb (no Z)
# Tim Adye <T.J.Adye@rl.ac.uk> 5 Oct 2010
###############################################################
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence('TopAlg')
from Sherpa_i.Sherpa_iConf import Sherpa_i
sherpa = Sherpa_i()
topAlg += sherpa
from MC10JobOptions.SherpaFEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'group10.phys-gener.sherpa010202.109371.Sherpa_gammabb_noz.TXT.v1'
evgenConfig.efficiency = 0.9
evgenConfig.weighting = 0
