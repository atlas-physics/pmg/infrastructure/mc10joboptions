###############################################################
# for Black Hole production
#
# Michiru Kaneda <Michiru.Kaneda@cern.ch>

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_PythiaCT66_Common.py" )

Pythia.PythiaCommand += [
    # Use Madgraph interface
    "pyinit user lhef",

    # No tau decays (Tauola)
    #"pydat3 mdcy 15 1 0",
    # No FSR (Photos)
    "pydat1 parj 90 20000",
]


# Tauola
#include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from TruthExamples.TruthExamplesConf import TestHepMC
TestHepMC.EnergyDifference = 100000000.
TestHepMC.OutputLevel = DEBUG

from MC10JobOptions.LhefEvgenConfig import evgenConfig
evgenConfig.inputfilebase = "group09.phys-gener.PythiaBlackMax.105453.LM1_BM_n6_Mth2000.TXT.v1"
evgenConfig.efficiency = 0.9
