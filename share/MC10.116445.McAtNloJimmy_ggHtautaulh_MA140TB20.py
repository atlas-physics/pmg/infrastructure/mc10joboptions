# Higgs via gluon fusion with H->tautau with 1lepEF
#--------------------------------------------------------------
# Junichi TANAKA, 2010
#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------
# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 

try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common.py" )
  if runArgs.ecmEnergy == 14000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common_14TeV.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig


Herwig.HerwigCommand += ["rmass 201 140.0",
                         "taudec TAUOLA"]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
topAlg += MultiLeptonFilter()

MultiLeptonFilter = topAlg.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 5000.
MultiLeptonFilter.Etacut = 5.0
MultiLeptonFilter.NLeptons = 1

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------
try:
  StreamEVGEN.RequireAlgs +=  [ "MultiLeptonFilter" ]
except Exception, e:
  pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.McAtNloEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'mcatnlo'
try:
  if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputfilebase = 'group09.phys-gener.mcatnlo341.116445.ggHtautau_MA140TB20_7TeV.TXT.v1'
  if runArgs.ecmEnergy == 8000.0:
    evgenConfig.inputfilebase = 'group09.phys-gener.mcatnlo341.116445.ggHtautau_MA140TB20_8TeV.TXT.v1'
except NameError:
  pass
evgenConfig.efficiency = 0.40

#==============================================================
#
# End of job options file
#
###############################################################
