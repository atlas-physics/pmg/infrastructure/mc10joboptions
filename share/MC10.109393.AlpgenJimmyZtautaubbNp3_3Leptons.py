# Alpgen Z(->tautau)+bb+3p with 3-lepton filter
#--------------------------------------------------------------
# File prepared by Soshi Tsuno (Soshi.Tsuno@cern.ch) Dec 2008
#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------
# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 

try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common.py" )
  if runArgs.ecmEnergy == 14000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_14TeV.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig


Herwig.HerwigCommand += [ "iproc alpgen",
                          "taudec TAUOLA"
                        ]
# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
topAlg += MultiLeptonFilter()
MultiLeptonFilter          = topAlg.MultiLeptonFilter
MultiLeptonFilter.Ptcut    = 5000.                ## Min Pt
MultiLeptonFilter.Etacut   = 10.                  ## Max |Eta|
MultiLeptonFilter.NLeptons = 2                    ## Min Nlep

from GeneratorFilters.GeneratorFiltersConf import ATauFilter
topAlg += ATauFilter()
ATauFilter = topAlg.ATauFilter
ATauFilter.Etacut         = 10.                   ## Max |Eta|
ATauFilter.llPtcute       = 5000.                 ## Min Pt(e)  for lep-lep
ATauFilter.llPtcutmu      = 5000.                 ## Min Pt(mu) for lep-lep
ATauFilter.lhPtcute       = 5000.                 ## Min Pt(e)  for lep-had
ATauFilter.lhPtcutmu      = 5000.                 ## Min Pt(mu) for lep-had
ATauFilter.lhPtcuth       = 10000.                ## Min Pt(h)  for lep-had
ATauFilter.hhPtcut        = 10000.                ## Min Pt(h)  for had-had
#ATauFilter.maxdphi        = 3.0                   ## Max dPhi between tau and anti-tau

try:
    StreamEVGEN.RequireAlgs +=  [ "MultiLeptonFilter", "ATauFilter" ]
except Exception, e:
    pass

from MC10JobOptions.AlpgenEvgenConfig import evgenConfig
 
# inputfilebase
#evgenConfig.inputfilebase = 'alpgen.109393.ZtautaubbNp3_pt20_3leptons'
evgenConfig.inputfilebase = 'group09.phys-gener.alpgen.109393.ZtautaubbNp3_pt20_3leptons_7Tev.TXT.v1'

# 7TeV
# Filter efficiency  = 0.1637
# MLM matching efficiency = 0.1544
# Alpgen cross section = 2.45526797+-0.01083370 pb
# Herwig cross section = Alpgen cross section * eff(MLM) = 0.3792 pb
# Integrated Luminosity = 26374.7179 pb-1
# Number of events in 4-vector file of Alpgen = 5000/(eff*0.8) = 250000 events
#
# 10TeV
# Filter efficiency  = 0.1424
# MLM matching efficiency = 0.1522
# Alpgen cross section = 6.38472925+-0.03026015 pb
# Herwig cross section = Alpgen cross section * eff(MLM) = 0.9718 pb
# Number of events in 4-vector file of Alpgen = 5000/(eff*0.8) = 289000 events
#
# Filter efficiency x safety factor = eff x 80% = 0.1139
evgenConfig.efficiency = 0.1139
evgenConfig.minevents=5000
#==============================================================
#
# End of job options file
#
###############################################################
