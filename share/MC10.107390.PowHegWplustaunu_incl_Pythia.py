## Job options file for PowHeg W+ ->taunu, inclusive tau decays
## Responsible: Martin Flechl
## Created: 31/3/2011
## Based on JobO for dataset 108297

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

# ... Main generator : Pythia
include ( "MC10JobOptions/MC10_PowHegPythia_Common.py" )
Pythia.PythiaCommand += [ "pyinit user lhef",
                          "pyinit pylisti -1","pyinit pylistf 1","pyinit dumpr 1 2",
                          "pydat1 parj 90 20000", # Turn off FSR.
                          "pydat3 mdcy 15 1 0"   # Turn off tau decays.
]


# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
#
from MC10JobOptions.EvgenConfig  import evgenConfig, knownGenerators
evgenConfig.generators += ["Lhef", "Pythia"]

evgenConfig.inputfilebase = 'group10.phys-gener.PowHegBoxV1.107390.Wplustaunu_7TeV.TXT.v1'

evgenConfig.efficiency = 0.9
evgenConfig.minevents  = 5000

#
# End of job options file
#
###############################################################
