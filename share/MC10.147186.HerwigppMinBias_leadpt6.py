## Job options file for Herwig++, min bias production
## Andy Buckley, April '10
## Lead pT > 6 GeV

## Get a handle on the top level algorithms' sequence
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

try:
     include("MC10JobOptions/MC10_Herwigpp_Common.py")
except NameError:
     ## TODO: This fallback is a bad idea...
     from Herwigpp_i.Herwigpp_iConf import Herwigpp
     topAlg += Herwigpp()
     cmds =  ""

## Add to commands
cmds += """
## Set up min bias process
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEMinBias
set /Herwig/Cuts/JetKtCut:MinKT 0.0*GeV
set /Herwig/Cuts/QCDCuts:MHatMin 0.0*GeV
set /Herwig/Cuts/QCDCuts:X1Min 0.01
set /Herwig/Cuts/QCDCuts:X2Min 0.01
set /Herwig/UnderlyingEvent/MPIHandler:IdenticalToUE 0
set /Herwig/UnderlyingEvent/MPIHandler:softInt Yes
set /Herwig/UnderlyingEvent/MPIHandler:twoComp Yes
"""

## Set commands
topAlg.Herwigpp.Commands = cmds.splitlines()






from GeneratorFilters.GeneratorFiltersConf import ChargedTracksFilter
topAlg += ChargedTracksFilter()
	
ChargedTracksFilter = topAlg.ChargedTracksFilter
ChargedTracksFilter.Ptcut = 6000.
ChargedTracksFilter.Etacut = 2.5
ChargedTracksFilter.NTracks = 0

	
#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------
try:
     StreamEVGEN.RequireAlgs +=  [ "ChargedTracksFilter" ]
except Exception, e:
     pass




#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.HerwigppEvgenConfig import evgenConfig
#evgenConfig.efficiency = 0.9
#evgenConfig.minevents=50

#==============================================================
#
# End of job options file
#
###############################################################
