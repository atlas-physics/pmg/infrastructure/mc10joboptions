# dijet production with pythia
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

#--------------------------------------------------------------
# File prepared by Ian Hinchliffe Nov 2005
#--------------------------------------------------------------
Pythia.PythiaCommand += [
			      "pysubs msel 0",
			      "pysubs ckin 3 140.",
			      "pysubs ckin 4 280.",
			      "pysubs msub 11 1",
			      "pysubs msub 12 1",
			      "pysubs msub 13 1",
			      "pysubs msub 68 1",
			      "pysubs msub 28 1",
			      "pysubs msub 53 1",

# use fragmentation from Professor tune to LEP data [Eur.Phys.J. C65,331 (2010); arXiv:0907.2973]

                              "pydat1 mstj 11 5",     # frag. function (Lund-Bowler)
                              "pydat1 parj 21 0.313", # sigma_{q}
                              "pydat1 parj 41 0.49",  # a
                              "pydat1 parj 42 1.2",   # b
                              "pydat1 parj 47 1.0",   # r_{b}
                              "pydat1 parj 81 0.257", # lambda_{QCD}
                              "pydat1 parj 82 0.8",   # shower cut-off
                              # set back to default 
                              "pydat1 parj 46 1.0"]

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
