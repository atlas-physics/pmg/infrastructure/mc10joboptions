# Alpgen Z(->tautau)+cc+0p
#--------------------------------------------------------------
#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------
# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 

try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common.py" )
  if runArgs.ecmEnergy == 14000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_14TeV.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig


Herwig.HerwigCommand += [ "iproc alpgen",
                          "taudec TAUOLA"
                        ]
# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
# no filter so far.

from MC10JobOptions.AlpgenEvgenConfig import evgenConfig
 
# inputfilebase
#evgenConfig.inputfilebase = 'alpgen.117610.ZtautauccNp0_pt20_nofilter'
evgenConfig.inputfilebase = 'group10.phys-gener.alpgen.117610.ZtautauccNp0_pt20_nofilter_7Tev.TXT.v1'

# 7TeV
# Filter efficiency  = 1.0
# MLM matching efficiency = 0.7319
# Alpgen cross section = 8.70535000+-0.00376307 pb
# Herwig cross section = Alpgen cross section * eff(MLM) = 6.371 pb
# Integrated Luminosity = 784.804747 pb-1
# Number of events in 4-vector file of Alpgen = 5000/(eff*0.8) = 9000 events
#
# 10TeV
# Filter efficiency  = 1.0
# MLM matching efficiency = 0.7159
# Alpgen cross section = 17.27900000+-0.00813682 pb
# Herwig cross section = Alpgen cross section * eff(MLM) = 12.37 pb
# Number of events in 4-vector file of Alpgen = 5000/(eff*0.8) = 9000 events
#
# Filter efficiency x safety factor = eff x 90% = 0.90

# 7 TeV - Information on sample 	117610	
# 7 TeV - Filter efficiency  = 	1.0000	
# 7 TeV - MLM matching efficiency = 	0.78	
# 7 TeV - Number of Matrix Elements in input file  = 	8400	
# 7 TeV - Alpgen cross section = 	15.1	 pb
# 7 TeV - Herwig cross section = Alpgen cross section * eff(MLM) = 	11.7	 pb
# 7 TeV - Cross section after filtering = 	11.7	 pb
# 7 TeV - Lumi/5000 events post-filter = 5000/eff(Filter)/XS(jimmy) = 	426.75	 pb-1
#		
# 7 TeV - Filter efficiency estimate below reduced by 10% to produce 5556 events on average,		
# 7 TeV - of which only 5000 will be used in further processing		

evgenConfig.efficiency = 0.90
evgenConfig.minevents=5000
#==============================================================
#
# End of job options file
#
###############################################################
