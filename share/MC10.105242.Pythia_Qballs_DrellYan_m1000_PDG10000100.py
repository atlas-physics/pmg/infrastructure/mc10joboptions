###############################################
#
# JobOptions for Drell-Yan Qball/Monopole production
# through gamma/Z*
#
# By: 
# Philippe Mermod
# Christian Ohm
#
###############################################

###############################################
# User-defined inputs
###############################################
QballMass = 1000
pdgCode = 10000100
print "Requested Qball mass (in GeV) is ", QballMass
print "Requested Qball PDG code is ", pdgCode 

###############################################
# Set up the job
###############################################
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

# include ATLAS Pythia common parameters
include("MC10JobOptions/MC10_Pythia_Common.py")

###################################################
#
# Pythia commands
# gamma/Z* interference must be off but Z*->mu mu
# should still be included
#

Pythia.PythiaCommand += ["pysubs msel 0",          # Users decay choice.
                         "pydat1 parj 90 20000",   # Turn off Electromag FSR by min mass to a high number.
                         "pydat3 mdcy 15 1 0",     # Turn off tau decays.
                         "pysubs msub 1 1",        # Turn on ffbar -> Z/gamma*-> ffbar
                         "pypars mstp 43 1",       # Only produce gamma* - dealing with EM object, no assumption on weak coupling
                         "pydat2 pmas 13 1 "+str(QballMass), # Set mass of muon
                         "pysubs ckin 1 "+str(2*QballMass+1), # Set lower invariant mass of the gamma*
#                         "pypars mstp 81 0",       # No multiple interactions
#                         "pypars mstp 61 0",       # No initial state showers (QCD and QED radiation)
#                         "pypars mstp 71 0",       # No final state showers (QCD and QED radiation) 
#                         "pypars mstp 111 0",      # No hadronization 
                         # Regulate what gamma* can decay to
                         "pydat3 mdme 174 1 0",
                         "pydat3 mdme 175 1 0",
                         "pydat3 mdme 176 1 0",
                         "pydat3 mdme 177 1 0",
                         "pydat3 mdme 178 1 0",
                         "pydat3 mdme 179 1 0",
                         "pydat3 mdme 180 1 0",
                         "pydat3 mdme 181 1 0",
                         "pydat3 mdme 182 1 0",
                         "pydat3 mdme 183 1 0",
                         "pydat3 mdme 184 1 1", # only allow gamma* -> mumu
                         "pydat3 mdme 185 1 0",
                         "pydat3 mdme 186 1 0",
                         "pydat3 mdme 187 1 0",
                         # Dump event and initial settings
                         "pyinit pylisti 12",      # dumps all particle and decay data after initialization 
                         "pyinit pylistf 1",       # dumps complete event record for the specified events
                         "pyinit dumpr 0 10"       #
                         "pystat 1 3 4 5",         # call pystat 4 times, printing
                                                   # 1: Table of generated events of different classes, and cross sections
                                                   # 3: Table of the allowed flavors in the hard process
                                                   # 4: Table of the kinematical cuts used
                                                   # 5: Prints out all MSTP and PARP settings used
                          ]

include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# don't include FSR (highly-charged particles won't radiate photons in the same way as single-charged)
# include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


##############################################################
# Translate particles codes
# Assign heavy muon (PDG ID = 13) new ID = 100xxxx0, which
# corresponds to a Qball with xxxx electric charge (in tenths)
#############################################################
#theApp.Dlls += [ "HepMCTools" ]
#theApp.TopAlg += [ "TranslateParticleID" ]
#TranslateParticleID = Algorithm( "TranslateParticleID" )

from HepMCTools.HepMCToolsConf import TranslateParticleID
topAlg += TranslateParticleID()
topAlg.TranslateParticleID.OutputLevel = 1
topAlg.TranslateParticleID.OldParticleID = 13
topAlg.TranslateParticleID.NewParticleID = pdgCode

#############################################################
# Configuration for EvgenJobTransforms
#############################################################
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
####################################################################
