###############################################################
#
# Job options file
#
#-----------------------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg")

include ("MC10JobOptions/MC10_PythiaMC09p_Common.py" )

#--------------------------------------------------------------
# Algorithms Private Options
#--------------------------------------------------------------

Pythia.PythiaCommand+= ["pyinit user comphep",
                        "pyinit pylistf 1",
                        "pyinit pylisti 12",
                        "pyinit dumpr 1 12",
                        "pydat1 parj 90 20000.",  ## Turn off FSR
                        "pydat3 mdcy 15 1 0"]     ## Turn off tau decays

# ... Tauola
include ("MC10JobOptions/MC10_Tauola_Fragment.py" )
# ... Photos
include ("MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.CompHepEvgenConfig import evgenConfig
evgenConfig.inputfilebase ='group09.phys-gener.CompHep451.115185.Torsion1500_eta02_ee.TXT.v1'
evgenConfig.efficiency = 0.9

#
# End of job options file
#
###############################################################
