###############################################################
#
# Job options file for inclusive Wenu
#
# author: C. Gwenlan (June'10); based on L. Mijovic's studies
# ISR  systematics sample: more hard ISR activity, external LO process + PYTHIA                                
# PARP(67) and PARP(64) are varied                                                                             
# AMBT1 default values: PARP(67)=4., PARP(64)=1.                                                                
# PARP(67): controls suppression of ISR branchings above the coherence scale                                
#           PARP(67) was introduced in 6.4.19 for the new shower, see Pythia update notes for more info     
# PARP(64): multiplies ISR alpha_strong evolution scale, the effect is \propto 1/(lambda_ISR^2)             
#           see Pythia Manual for more info                                                                
#
# reference JO: MC10.106043.PythiaWenu_no_filter.py
#===============================================================

# ... Main generator : Pythia
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

# @todo: PARP(67) is not an appropriate parameter to vary for resonance production [instead: MSTP(32)?]
Pythia.PythiaCommand +=[ "pypars parp 67 6.0" ]   
Pythia.PythiaCommand +=[ "pypars parp 64 0.25" ]  

Pythia.PythiaCommand += [ "pysubs msel 0",          # Users decay choice.
                         "pydat1 parj 90 20000", # Turn off FSR.
                         "pydat3 mdcy 15 1 0",     # Turn off tau decays.
                         "pysubs msub 2 1",        # Create W bosons.
                         "pydat3 mdme 190 1 0",
                         "pydat3 mdme 191 1 0",
                         "pydat3 mdme 192 1 0",
                         "pydat3 mdme 194 1 0",
                         "pydat3 mdme 195 1 0",
                         "pydat3 mdme 196 1 0",
                         "pydat3 mdme 198 1 0",
                         "pydat3 mdme 199 1 0",
                         "pydat3 mdme 200 1 0",
                         "pydat3 mdme 206 1 1",    # Switch for W->enu.
                         "pydat3 mdme 207 1 0",    # Switch for W->munu.
                         "pydat3 mdme 208 1 0"]    # Switch for W->taunu.

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
# efficiency = 0.9 - no filtering
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.90

#==============================================================
#
# End of job options file
#
###############################################################
