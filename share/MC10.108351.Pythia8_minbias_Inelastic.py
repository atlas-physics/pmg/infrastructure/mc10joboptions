###############################################################
#
# Job options file
# Pythia8 minimum bias (ND+SD+DD) sample
# contact: Claire Gwenlan, James Monk
#==============================================================

# ... Main generator : Pythia8

MessageSvc = Service( "MessageSvc" )
#MessageSvc.OutputLevel = 4
	
from AthenaCommon.AppMgr import ServiceMgr as svcMgr
  
import AthenaServices
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")
include ("MC10JobOptions/MC10_Pythia8_Common.py")

Pythia8.Commands += ["SoftQCD:minBias = on"]
Pythia8.Commands += ["SoftQCD:singleDiffractive = on"]
Pythia8.Commands += ["SoftQCD:doubleDiffractive = on"]

#
from MC10JobOptions.Pythia8EvgenConfig import evgenConfig
evgenConfig.efficiency = 0.95
