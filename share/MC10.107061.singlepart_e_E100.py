
# Single electrons
# J.F. Arguin, Dec. 2005

# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

from ParticleGenerator.ParticleGeneratorConf import ParticleGenerator
topAlg += ParticleGenerator()


ParticleGenerator = topAlg.ParticleGenerator

# For VERBOSE output from ParticleGenerator.
ParticleGenerator.OutputLevel = 1

ParticleGenerator.orders = [
 "PDGcode: sequence -11 11",
 "e: constant 100000",
 "eta: flat -2.5 2.5",
 "phi: flat -3.14159 3.14159"
 ]

#==============================================================
#
# End of job options file
#
###############################################################
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.SingleEvgenConfig import evgenConfig
