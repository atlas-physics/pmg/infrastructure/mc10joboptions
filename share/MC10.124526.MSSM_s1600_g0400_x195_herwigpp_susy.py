## Herwig++ job option file for Susy SU4 2-parton -> 2-sparticle processes

## Get a handle on the top level algorithms' sequence
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

try:
     include ( "MC10JobOptions/MC10_Herwigpp_Common.py" )
except NameError:
     # needed (dummy) default
     # from Herwigpp_i.Herwigpp_iConf import Herwigpp
     # topAlg += Herwigpp()
     cmds =  ""

## Add Herwig++ parameters for this process
cmds += """

## Generate the process in MSSM equivalent to 2-parton -> 2-sparticle processes in Fortran Herwig
## Read the MSSM model details
read MSSM.model
cd /Herwig/NewPhysics 

# Leave out EW diagrams, and things we haven't explicitly specified
# (pretty much just EW)
set HPConstructor:IncludeEW No
set HPConstructor:Processes TwoParticleInclusive

#incoming parton
insert HPConstructor:Incoming 0 /Herwig/Particles/g
insert HPConstructor:Incoming 1 /Herwig/Particles/u
insert HPConstructor:Incoming 2 /Herwig/Particles/ubar
insert HPConstructor:Incoming 3 /Herwig/Particles/d
insert HPConstructor:Incoming 4 /Herwig/Particles/dbar
insert HPConstructor:Incoming 5 /Herwig/Particles/s
insert HPConstructor:Incoming 6 /Herwig/Particles/sbar
insert HPConstructor:Incoming 7 /Herwig/Particles/c
insert HPConstructor:Incoming 8 /Herwig/Particles/cbar

#outgoing sparton (squark and gluino)
insert HPConstructor:Outgoing 0 /Herwig/Particles/~g
insert HPConstructor:Outgoing 1 /Herwig/Particles/~u_L
insert HPConstructor:Outgoing 2 /Herwig/Particles/~u_Lbar
insert HPConstructor:Outgoing 3 /Herwig/Particles/~d_L
insert HPConstructor:Outgoing 4 /Herwig/Particles/~d_Lbar
insert HPConstructor:Outgoing 5 /Herwig/Particles/~s_L
insert HPConstructor:Outgoing 6 /Herwig/Particles/~s_Lbar
insert HPConstructor:Outgoing 7 /Herwig/Particles/~c_L
insert HPConstructor:Outgoing 8 /Herwig/Particles/~c_Lbar

insert HPConstructor:Outgoing 9 /Herwig/Particles/~u_R
insert HPConstructor:Outgoing 10 /Herwig/Particles/~u_Rbar
insert HPConstructor:Outgoing 11 /Herwig/Particles/~d_R
insert HPConstructor:Outgoing 12 /Herwig/Particles/~d_Rbar
insert HPConstructor:Outgoing 13 /Herwig/Particles/~s_R
insert HPConstructor:Outgoing 14 /Herwig/Particles/~s_Rbar
insert HPConstructor:Outgoing 15 /Herwig/Particles/~c_R
insert HPConstructor:Outgoing 16 /Herwig/Particles/~c_Rbar

## Read the SUSY spectrum file (SLHA format)
setup MSSM/Model susy_MSSM_s1600_g0400_x195.txt
"""
## Set the command vector
topAlg.Herwigpp.Commands = cmds.splitlines()

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.HerwigppEvgenConfig import evgenConfig
#evgenConfig.efficiency = 0.9
#evgenConfig.minevents=100

from MC10JobOptions.SUSYEvgenConfig import evgenConfig


#==============================================================
#
# End of job options file
#
###############################################################
