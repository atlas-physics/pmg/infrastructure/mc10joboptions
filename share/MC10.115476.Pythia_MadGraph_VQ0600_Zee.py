#--------------------------------------------------------------
# Acceptance sample
# Single production of a vector-like quark with an SM quark
# The vector-like quark decays into a vector boson and an SM quark
#--------------------------------------------------------------
# No filter , efficiency = 100% safe factor= 0.9
# File prepared by Alex Penson Oct 2010
#--------------------------------------------------------------

###############################################################
#
# Job options file
#
#==============================================================
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_PythiaMC09p_Common.py" )


Pythia.PythiaCommand += [ "pyinit user lhef",
                          "pyinit pylisti -1","pyinit pylistf 1","pyinit dumpr 1 2",
                          "pydat1 parj 90 20000", # Turn off FSR.
]



## ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )


## ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


#--------------------------------------------------------------
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
#from GeneratorFilters.GeneratorFiltersConf import PhotonFilter
#topAlg += PhotonFilter()
#PhotonFilter = topAlg.PhotonFilter
#PhotonFilter.Ptcut = 7000.
#PhotonFilter.Etacut = 2.5
#PhotonFilter.NPhotons = 1
#
#from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
#topAlg += LeptonFilter()
#LeptonFilter = topAlg.LeptonFilter
#LeptonFilter.Etacut = 2.5
#
#try:
#    StreamEVGEN.RequireAlgs +=  [ "PhotonFilter" ]
#except Exception, e:
#    pass
#try:
#    StreamEVGEN.RequireAlgs += [ "LeptonFilter" ]
#except Exception, e:
#    pass
##
#

from MC10JobOptions.PythiaEvgenConfig import evgenConfig
#from EvgenJobOptions.MadGraphEvgenConfig import evgenConfig
evgenConfig.generators += ["Lhef", "Pythia"]
evgenConfig.inputfilebase = 'group10.phys-gener.madgraph.115476.VQ0600_Zee.TXT.v1'
evgenConfig.efficiency = 0.9

#==============================================================
#
# End of job options file
#
###############################################################

