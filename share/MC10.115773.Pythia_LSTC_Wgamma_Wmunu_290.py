# LSTC (Z,Gamma) & Z->ee using Pythia
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

#--------------------------------------------------------------
# File prepared by Louis Helary Oct 10
#--------------------------------------------------------------
MrhoT=290.
MaT=320.
MPiT=160.


Pythia.PythiaCommand += ["pysubs msel 0", #Turn off all process
                       "pysubs msub 194 0",  # f+fbar -> f'+fbar' (ETC)
                       "pysubs msub 361 0",  # f + fbar -> W_L+ W_L-
                       "pysubs msub 362 0",  # f + fbar -> W_L+/- pi_T-/+
                       "pysubs msub 363 0",  # pi_tc+ pi_tc-
                       "pysubs msub 364 0",  # f + fbar -> gamma pi_T0
                       "pysubs msub 365 0",  # gamma pi_tc0'
                       "pysubs msub 366 0",  # f + fbar -> Z0 pi_T0
                       "pysubs msub 368 0",  # f + fbar -> W+/- pi_T-/+
                       "pysubs msub 370 0",  # f + fbar' -> W_L+/- Z_L0
                       "pysubs msub 371 0",  # f + fbar' -> W_L+/- pi_T0
                       "pysubs msub 372 0",  # f + fbar' -> pi_T+/- Z_L0
                       "pysubs msub 373 0",  # pi_tc+ pi_tc-
                       "pysubs msub 374 0",  # f + fbar' -> gamma pi_T+/-
                       "pysubs msub 375 0",  # f + fbar' -> Z0 pi_T+/-  (Z_T pi_tc+-)
                       "pysubs msub 376 0",  # f + fbar' -> W+/- pi_T0  (W_T pi_tc0)
                       "pysubs msub 367 0",  # Z pi_tc0'
                       "pysubs msub 377 0",  # W pi_tc0'
                       "pysubs msub 378 1",  # f + fbar' -> gamma W+/- (ETC
                       "pysubs msub 379 0",  # f + fbar -> gamma Z0 (ETC)
                       "pysubs msub 380 0",  # f + fbar -> Z0 Z0 (ETC)
                       "pysubs ckin 1 "+str(MrhoT-25), # Kinematical cut s>240 
                       "pysubs ckin 2 "+str(MaT+25)] # Kinematical cut s<260 
# W-> e nue 

Pythia.PythiaCommand += ["pydat3 mdme 190 1 0",
                         "pydat3 mdme 191 1 0",
                         "pydat3 mdme 192 1 0",
                         "pydat3 mdme 193 1 -1",
                         "pydat3 mdme 194 1 0",
                         "pydat3 mdme 195 1 0",
                         "pydat3 mdme 196 1 0",
                         "pydat3 mdme 197 1 -1",
                         "pydat3 mdme 198 1 0",
                         "pydat3 mdme 199 1 0",
                         "pydat3 mdme 200 1 0",
                         "pydat3 mdme 201 1 -1",
                         "pydat3 mdme 202 1 -1",
                         "pydat3 mdme 203 1 -1",
                         "pydat3 mdme 204 1 -1",
                         "pydat3 mdme 205 1 -1",
                         "pydat3 mdme 206 1 0",#Wenue
                         "pydat3 mdme 207 1 1",#mu
                         "pydat3 mdme 208 1 0"]#tau

#LSTC Parameters:
#mass:
Pythia.PythiaCommand += ["pydat2 pmas 3000111 1 "+str(MPiT), # pi_T0
                        "pydat2 pmas 3000211 1 "+str(MPiT), # pi_T+/-
                        "pydat2 pmas 3000221 1 "+str(MPiT*2), # pi_T0'
                        "pydat2 pmas 3000115 1 "+str(MaT), # a_T0
                        "pydat2 pmas 3000215 1 "+str(MaT), # a_T+/-
                        "pydat2 pmas 3000113 1 "+str(MrhoT), # rho_T0
                        "pydat2 pmas 3000213 1 "+str(MrhoT), # rho_T+/-
                        "pydat2 pmas 3000223 1 "+str(MrhoT)] # omega_T



Pythia.PythiaCommand += ["pypars mstp 42 0"] # switching off W width
Pythia.PythiaCommand += ["pypars mstp 32 4"] # Changing the scale of the interaction
Pythia.PythiaCommand += ["pytcsm rtcm 47 1.", # ratio coupling g_a-pi-pi to g_rho-pi-pi
                        "pytcsm rtcm 12 "+str(MrhoT), # M_V1
                        "pytcsm rtcm 13 "+str(MrhoT), # M_A1
                        "pytcsm rtcm 48 "+str(MrhoT), # M_V2
                        "pytcsm rtcm 49 "+str(MrhoT), # M_A2
                        "pytcsm rtcm 50 "+str(MrhoT), # M_V3
                        "pytcsm rtcm 51 "+str(MrhoT), # M_A3
                        "pytcsm rtcm 2 1.", # charge of up type TQ: QD=QU-1
                        "pytcsm rtcm 3 0.3333333"] # sin(chi)


Pythia.PythiaCommand += ["pydat1 parj 90 20000", # Turn off FSR.
                         "pydat3 mdcy 15 1 0"]     # Turn off tau decays.
			 
# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter Lepton & gamma
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import PhotonFilter
topAlg += PhotonFilter()

from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
topAlg += MultiLeptonFilter()


PhotonFilter = topAlg.PhotonFilter
PhotonFilter.Ptcut = 15000
PhotonFilter.Etacut = 3
PhotonFilter.NPhotons = 1

MultiLeptonFilter = topAlg.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 15000
MultiLeptonFilter.Etacut = 3
MultiLeptonFilter.NLeptons = 1

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------

try:
     StreamEVGEN.RequireAlgs +=  [ "PhotonFilter" ]
except Exception, e:
     pass
try:
     StreamEVGEN.RequireAlgs += [ "MultiLeptonFilter" ]
except Exception, e:
     pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------

from MC10JobOptions.EvgenConfig import evgenConfig
evgenConfig.generators += [ "Pythia" ]
evgenConfig.auxfiles += [ 'PDGTABLE.MeV', 'pdt.table', 'DECAY.DEC' ] 

#evgenConfig.efficiency = 0.97

