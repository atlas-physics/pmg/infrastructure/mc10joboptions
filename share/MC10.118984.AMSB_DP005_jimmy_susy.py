from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

try:
    if runArgs.ecmEnergy == 7000.0:
        include ( "MC10JobOptions/MC10_Herwig_Common_7TeV.py" )
    if runArgs.ecmEnergy == 10000.0:
        include ( "MC10JobOptions/MC10_Herwig_Common.py" )
except NameError:
    # needed (dummy) default
    from Herwig_i.Herwig_iConf import Herwig
    topAlg += Herwig()
    Herwig = topAlg.Herwig

Herwig.HerwigCommand += [ "iproc 13020",
                          "susyfile susy_direct_003.txt",
                          "taudec TAUOLA" ]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

# TruthJet filter
try:
  from JetRec.JetGetters import *
  if runArgs.ecmEnergy == 7000.0:
      c4=make_StandardJetGetter('AntiKt',0.4,'Truth')
  if runArgs.ecmEnergy == 8000.0:
      c4=make_StandardJetGetter('AntiKt',0.4,'Truth')
  if runArgs.ecmEnergy == 10000.0:
      c4=make_StandardJetGetter('Cone',0.4,'Truth')
  c4alg = c4.jetAlgorithmHandle()
except Exception, e:
  pass
            
from GeneratorFilters.GeneratorFiltersConf import TruthJetFilter
topAlg += TruthJetFilter()
            
TruthJetFilter = topAlg.TruthJetFilter
try:
  if runArgs.ecmEnergy == 7000.0:
      TruthJetFilter.Njet=1;
      TruthJetFilter.NjetMinPt=50.*GeV;
      TruthJetFilter.NjetMaxEta=5;
      TruthJetFilter.jet_pt1=50.*GeV;
      TruthJetFilter.TruthJetContainer="AntiKt4TruthJets";
  if runArgs.ecmEnergy == 8000.0:
      TruthJetFilter.Njet=1;
      TruthJetFilter.NjetMinPt=50.*GeV;
      TruthJetFilter.NjetMaxEta=5;
      TruthJetFilter.jet_pt1=50.*GeV;
      TruthJetFilter.TruthJetContainer="AntiKt4TruthJets";
  if runArgs.ecmEnergy == 10000.0:
      TruthJetFilter.Njet=1;
      TruthJetFilter.NjetMinPt=50.*GeV;
      TruthJetFilter.NjetMaxEta=5;
      TruthJetFilter.jet_pt1=50.*GeV;
      TruthJetFilter.TruthJetContainer="Cone4TruthJets";
except NameError:
  pass

try:
  StreamEVGEN.RequireAlgs = [ "TruthJetFilter" ]
except Exception, e:
  pass
                                                                                                                              

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.HerwigEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
evgenConfig.specialConfig="AMSBIndex=DP005;preInclude=SimulationJobOptions/preInclude.ASMB.py;"

from MC10JobOptions.SUSYEvgenConfig import evgenConfig

#==============================================================
#
# End of job options file
#
###############################################################

