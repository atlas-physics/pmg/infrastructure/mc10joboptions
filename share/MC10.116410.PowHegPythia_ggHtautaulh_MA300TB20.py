###############################################################
#
# Job options file for POWHEG with Pythia
# U. Husemann, C. Wasicki, Nov. 2009 / Feb. 2010
#
# 4vec was generated with Powheg-BOX/gg_H (SVN r210) (J.Tanaka)
#
#==============================================================
#--------------------------------------------------------------
# General Application Configuration options
#--------------------------------------------------------------
import AthenaCommon.AtlasUnixGeneratorJob

from AthenaCommon.AppMgr import theApp
from AthenaCommon.AppMgr import ServiceMgr

# make sure we are loading the ParticleProperty service
from PartPropSvc.PartPropSvcConf import PartPropSvc
ServiceMgr += PartPropSvc()

#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
ServiceMgr.MessageSvc.OutputLevel = INFO

from AthenaCommon.AlgSequence import AlgSequence
topAlg=AlgSequence()

# ... Main generator : Pythia
include( "MC10JobOptions/MC10_PowHegPythia_Common.py")

Pythia.PythiaCommand += [   "pydat3 mdme 210 1 0", # Higgs decay
                            "pydat3 mdme 211 1 0",
                            "pydat3 mdme 212 1 0",
                            "pydat3 mdme 213 1 0",
                            "pydat3 mdme 214 1 0",
                            "pydat3 mdme 215 1 0",
                            "pydat3 mdme 218 1 0",
                            "pydat3 mdme 219 1 0",
                            "pydat3 mdme 220 1 1",
                            "pydat3 mdme 222 1 0",
                            "pydat3 mdme 223 1 0",
                            "pydat3 mdme 224 1 0",
                            "pydat3 mdme 225 1 0",
                            "pydat3 mdme 226 1 0" ]

# ... Tauola
Pythia.PythiaCommand += [ "pydat3 mdcy 15 1 0"] # turn off tau decays
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )
Tauola.TauolaCommand = [        "tauola polar 1",
                                "tauola radcor 1",
                                "tauola phox 0.01",
                                "tauola dmode 3",
                                "tauola jak1 0",
                                "tauola jak2 0"]

# ... Photos
Pythia.PythiaCommand += ["pydat1 parj 90 20000"] # turn off FSR for Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

from MC10JobOptions.EvgenConfig import evgenConfig, knownGenerators

evgenConfig.generators += [ "Lhef", "Pythia" ]

#dummy needed
evgenConfig.inputfilebase = 'powheg'
try:
  if runArgs.ecmEnergy == 7000.0:
      evgenConfig.inputfilebase = 'group09.phys-gener.powheg.116410.ggH_MA300TB20_7TeV.TXT.v1'
  if runArgs.ecmEnergy == 8000.0:
      evgenConfig.inputfilebase = 'group09.phys-gener.powheg.116410.ggH_MA300TB20_8TeV.TXT.v1'
except NameError:
  pass

evgenConfig.efficiency = 0.9
#---------------------------------------------------------------
# Ntuple service output
#---------------------------------------------------------------
#
#==============================================================
#
# End of job options file
#
###############################################################
