###############################################################
#
# Job options file
#
# Low mass Drell Yan mumu
#==============================================================

# ... Main generator : Pythia
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

Pythia.PythiaCommand +=[ "pysubs msel 0",          # Users decay choice.
                         "pydat1 parj 90 20000",   # Turn off FSR.
                         "pydat3 mdcy 15 1 0",     # Turn off tau decays.
                         #
                         # Z production:
                         "pysubs msub 1 1",        # Create Z bosons.
                         "pysubs ckin 1 1.0",      # Lower invariant mass.
                         "pysubs ckin 2 12.0",     # Higher invariant mass.
                         "pydat3 mdme 174 1 0",
                         "pydat3 mdme 175 1 0",
                         "pydat3 mdme 176 1 0",
                         "pydat3 mdme 177 1 0",
                         "pydat3 mdme 178 1 0",
                         "pydat3 mdme 179 1 0",
                         "pydat3 mdme 182 1 0",    # Switch for Z->ee.
                         "pydat3 mdme 183 1 0",
                         "pydat3 mdme 184 1 1",    # Switch for Z->mumu.
                         "pydat3 mdme 185 1 0",
                         "pydat3 mdme 186 1 0",    # Switch for Z->tautau.
                         "pydat3 mdme 187 1 0"]
# W production:
#                         "pysubs msub 2 1",        # Create W bosons.
#                         "pydat3 mdme 190 1 0",
#                         "pydat3 mdme 191 1 0",
#                         "pydat3 mdme 192 1 0",
#                         "pydat3 mdme 194 1 0",
#                         "pydat3 mdme 195 1 0",
#                         "pydat3 mdme 196 1 0",
#                         "pydat3 mdme 198 1 0",
#                         "pydat3 mdme 199 1 0",
#                         "pydat3 mdme 200 1 0",
#                         "pydat3 mdme 206 1 1",    # Switch for W->enu.
#                         "pydat3 mdme 207 1 0",    # Switch for W->munu.
#                         "pydat3 mdme 208 1 0"]    # Switch for W->taunu.

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
topAlg += MultiLeptonFilter()

MultiLeptonFilter = topAlg.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 2500.
MultiLeptonFilter.Etacut = 2.7
MultiLeptonFilter.NLeptons = 2

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------

try:
    StreamEVGEN.RequireAlgs += [ "MultiLeptonFilter" ]
except Exception, e:
    pass


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig

# MC9
evgenConfig.efficiency = 0.008

#==============================================================
#
# End of job options file
#
###############################################################


