################################################################
#
# PythiaB bbbar -> lX with 1 lepton (e/mu) filter
# No decay channel is specified, where lepton could come from
# b quark or from b's products of c quark and tau.
#
# Responsible person(s)
#   April 23, 2009 : Junichi Tanaka <Junichi.Tanaka@cern.ch>
#
################################################################
#
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 4

#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_PythiaB_Common.py" )

#--------------------------------------------------------------
# Algorithms
#--------------------------------------------------------------
#--------------------------------------------------------------
#              PARAMETERS  SPECIFIC  TO   PYTHIABMODULE
#--------------------------------------------------------------
PythiaB = topAlg.PythiaB
PythiaB.ForceBDecay = "no"
PythiaB.ForceCDecay = "no"
#--------------------------------------------------------------
# --------  PYTHIA PARAMETERS OPTIMAL FOR BEAUTY PRODUCTION  --
#--------------------------------------------------------------
#  'msel 5' is only for fast tests!
#  for correct b-producion you should use 'msel 1'
# 'mstj 26 0' = no mixing was defined in Btune as default
# 'mstj 22 2' = no K0S, Lambda0 decays in Pythia - defined in Btune as default

include( "MC10JobOptions/MC10_PythiaB_Btune.py" )

PythiaB.PythiaCommand += ["pysubs msel 1",
                          "pysubs ckin 1  5.0",
                          "pysubs ckin 3  17.0",
                          "pysubs ckin 4  35.0" 
                         ]

#--------------------------------------------------------------
# -------------  DEFINE SELECTION CUTS  -------------
#--------------------------------------------------------------
PythiaB.flavour =  5.
#  --- Selections on b  quarks   -------------
PythiaB.cutbq = ["7.0 3.0 and 7.0 3.0"]
#  --- LVL1 muon cuts (0/1=OFF/ON, PtCut, etaCut) -------------
PythiaB.lvl1cut = [ 0.,  1.0, 2.8]
#  --- LVL2 muon/electron cuts (0/1=OFF/ON, ID 11/13=e/mu, PtCut, etaCut) -----
PythiaB.lvl2cut = [ 0.,  13.,     1.,   2.8]
#  --- Offline cuts 0=OFF 1=ON -------------
PythiaB.offcut = [ 0., 0., 102.5, 0., 102.5, 0., 102.5]
#  --- Number of repeated hadronization mhadr -------------
PythiaB.mhadr =  1.
###############################################################
#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
topAlg += MultiLeptonFilter()

MultiLeptonFilter = topAlg.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 8000.0
MultiLeptonFilter.Etacut = 3.0
MultiLeptonFilter.NLeptons = 1

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------
try:
     StreamEVGEN.RequireAlgs +=  [ "MultiLeptonFilter" ]
except Exception, e:
     pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaBEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.0429*0.9
#
#==============================================================
#
# End of job options file
#
###############################################################
