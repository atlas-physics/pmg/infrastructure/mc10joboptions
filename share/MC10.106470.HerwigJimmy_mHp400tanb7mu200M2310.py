#--------------------------------------------------------------
# 
# Job options file
#
#
# Herwig & Jimmy SUSY production (mH+=400GeV, tan(beta)=7, mu = 200, M_2 = 310) 
#
# Responsible person(s)
#   17 Jul, 2009 :  Caleb LAMPEN (lampen@physics.arizona.edu)
# 
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence
topAlg=AlgSequence("TopAlg")
 

try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_Herwig_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_Herwig_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_Herwig_Common.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig

topAlg.Herwig.HerwigCommand += [ "iproc 13000",              #iproc SUSY proccees 
                                  "susyfile susy_mHp400tanb7_2.txt",#isawig input file
                                  "taudec TAUOLA"]           #taudec tau dcay package
#Setup tauola and photos. 
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )
                                 
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#######################################################
# FILTER EFFICIENCY
#######################################################
from MC10JobOptions.HerwigEvgenConfig import evgenConfig
evgenConfig.efficiency = .9

from MC10JobOptions.SUSYEvgenConfig import evgenConfig
#==============================================================
#
# End of job options file
#
#####################################
