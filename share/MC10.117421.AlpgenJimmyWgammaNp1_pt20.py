#--------------------------------------------------------------
#
# Alpgen Wgamma with pt gamma > 40 GeV
#
#--------------------------------------------------------------

MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 2
MessageSvc.infoLimit = 1000

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 
try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common.py" )
  if runArgs.ecmEnergy == 14000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_14TeV.py" )
except NameError:
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig 

Herwig.HerwigCommand += [ "iproc alpgen",
                          "taudec TAUOLA",
                        ]
# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

from MC10JobOptions.AlpgenEvgenConfig import evgenConfig
 
# inputfilebase
evgenConfig.inputfilebase = 'alpgen'
try:
  if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.alpgen.117421.WgammaNp1_pt20_7tev.TXT.v1'
  if runArgs.ecmEnergy == 8000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.alpgen.117421.WgammaNp1_pt20_8tev.TXT.v1'
  if runArgs.ecmEnergy == 10000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.alpgen.117421.WgammaNp1_pt20_10tev.TXT.v1'
  if runArgs.ecmEnergy == 14000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.alpgen.117421.WgammaNp1_pt20_14tev.TXT.v1'    
except NameError:
  pass

evgenConfig.minevents=5000
