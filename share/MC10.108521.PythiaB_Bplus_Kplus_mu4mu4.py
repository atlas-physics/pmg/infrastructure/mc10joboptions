###############################################################################
#
# MC10.108523.PythiaB_Bplus_Kplus_mu4mu4.py
# Author: Pavel Reznicek (Pavel.Reznicek@cern.ch)
# Generation of B+ -> K+ mu+ mu- decay using EvtDecay
# PRODUCTION SYSTEM FRAGMENT
#
###############################################################################

#------------------------------------------------------------------------------
# Make EvtGen user decay file on the fly
#------------------------------------------------------------------------------

f = open("MYDECAY_Bplus_Kplus_mumu.DEC","w")
f.write("Decay B0\n")
f.write("Enddecay\n")
f.write("Decay B+\n")
f.write("Enddecay\n")
f.write("Decay B_s0\n")
f.write("Enddecay\n")
f.write("Decay anti-Lambda_b0\n")
f.write("Enddecay\n")
f.write("Decay anti-Xi_b+\n")
f.write("Enddecay\n")
f.write("Decay anti-Xi_b0\n")
f.write("Enddecay\n")
f.write("Decay anti-Sigma_b+\n")
f.write("Enddecay\n")
f.write("Decay anti-Sigma_b-\n")
f.write("Enddecay\n")
f.write("Decay anti-Sigma_b0\n")
f.write("Enddecay\n")
f.write("Decay B_c+\n")
f.write("Enddecay\n")
f.write("Decay B+\n")
f.write("1.0000  K+ mu+ mu-  BTOSLLBALL ;\n")
f.write("Enddecay\n")
f.write("End\n")
f.close()

#------------------------------------------------------------------------------
# Production driving parameters
#------------------------------------------------------------------------------

from MC10JobOptions.PythiaBEvgenConfig import evgenConfig
evgenConfig.minevents  = 500
evgenConfig.efficiency = 0.04

#------------------------------------------------------------------------------
# Import all needed algorithms (in the proper order)
#------------------------------------------------------------------------------

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include( "MC10JobOptions/MC10_PythiaB_Common.py" )

from EvtGen_i.EvtGen_iConf import EvtDecay
topAlg += EvtDecay()
EvtDecay = topAlg.EvtDecay

from GeneratorFilters.GeneratorFiltersConf import BSignalFilter
topAlg += BSignalFilter()
BSignalFilter = topAlg.BSignalFilter

#------------------------------------------------------------------------------
# PythiaB parameters settings
#------------------------------------------------------------------------------

# Stop all B-decays in Pythia and left them to decay by EvtGen
include( "MC10JobOptions/MC10_PythiaB_StopPytWeakBdecays.py" )

# Production settings
include( "MC10JobOptions/MC10_PythiaB_Btune.py" )
PythiaB.PythiaCommand += [ "pysubs ckin 3 6.",
                           "pysubs ckin 9 -3.5",
                           "pysubs ckin 10 3.5",
                           "pysubs ckin 11 -3.5",
                           "pysubs ckin 12 3.5",
                           "pysubs msel 1" ]

# Pythia b-quark cuts
PythiaB.cutbq = [ "0. 102.5 and 4. 2.5" ]

# Repeated hadronization
PythiaB.mhadr = 1

#------------------------------------------------------------------------------
# Signal event filtering
#------------------------------------------------------------------------------

# Muon pT cuts selection
BSignalFilter.LVL1MuonCutOn  = True
BSignalFilter.LVL1MuonCutPT  = 4000
BSignalFilter.LVL1MuonCutEta = 2.5
BSignalFilter.LVL2MuonCutOn  = True
BSignalFilter.LVL2MuonCutPT  = 4000
BSignalFilter.LVL2MuonCutEta = 2.5

# Hadronic tracks cuts
BSignalFilter.Cuts_Final_hadrons_switch = True
BSignalFilter.Cuts_Final_hadrons_pT     = 500.0
BSignalFilter.Cuts_Final_hadrons_eta    = 2.5
BSignalFilter.BParticle_cuts            = 521

#------------------------------------------------------------------------------
# EvtGen decay table for signal
#------------------------------------------------------------------------------

EvtDecay.userDecayTableName = "MYDECAY_Bplus_Kplus_mumu.DEC"

#------------------------------------------------------------------------------
# POOL / Root output
#------------------------------------------------------------------------------

try:
  StreamEVGEN.RequireAlgs += [ "BSignalFilter" ]
except Exception, e:
  pass

###############################################################################
#
# End of job options fragment for B+ -> K+ mu+ mu- decay
#
###############################################################################
