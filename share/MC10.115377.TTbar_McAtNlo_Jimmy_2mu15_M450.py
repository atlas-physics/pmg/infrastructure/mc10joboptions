###############################################################
#
# Job options file
# (based on original from Wouter Verkerke)
#
#==============================================================
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 3

#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------

#-- Dll's and Algorithms
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 

try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common.py" )
  if runArgs.ecmEnergy == 14000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common_14TeV.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig

# ... Main generator : Herwig
Herwig.HerwigCommand += [ "taudec TAUOLA"]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import MassRangeFilter
topAlg += MassRangeFilter()

MassRangeFilter = topAlg.MassRangeFilter
MassRangeFilter.PtCut  = 15000.
MassRangeFilter.PtCut2 = 15000.
MassRangeFilter.EtaCut  = 2.8
MassRangeFilter.EtaCut2 = 2.8
MassRangeFilter.InvMassMin =   450000.
MassRangeFilter.InvMassMax = 14000000.
MassRangeFilter.PartId  = 13
MassRangeFilter.PartId2 = 13

try:
    StreamEVGEN.RequireAlgs = [ "MassRangeFilter" ]
except Exception, e:
    pass

from MC10JobOptions.McAtNloEvgenConfig import evgenConfig

# dummy needed
evgenConfig.inputfilebase = 'mcatnlo'
try:
  if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputfilebase = 'group09.phys-gener.mcatnlo342.115377.ttbar_2mu15_450_inf_muskim_7TeV.TXT.v2'
  if runArgs.ecmEnergy == 8000.0:
    evgenConfig.inputfilebase = 'group09.phys-gener.mcatnlo342.115377.ttbar_2mu15_450_inf_muskim_8TeV.TXT.v1'
  if runArgs.ecmEnergy == 10000.0:
    evgenConfig.inputfilebase = 'group09.phys-gener.mcatnlo342.115377.ttbar_2mu15_450_inf_muskim_10TeV.TXT.v1' 
except NameError:
  pass

# 15.6.6.5
# 30/200621=0.0001495
#evgenConfig.efficiency = 0.0001495
# 15.6.12.5
# (153+149+139+157+146+165+142)/(1000809+40+1002911+56+1003767+41+1002458+46+1001866+39+1003436+44+1003510+39) = 0.000149735107
evgenConfig.efficiency = 0.000150
evgenConfig.minevents  = 100
evgenConfig.maxeventsfactor = 1.1

#==============================================================
#
# End of job options file
#
###############################################################
