###############################################################################
#
# MC10.108437.PythiaB_Lambdab_Lambda_mu4mu6.py
# Author: Pavel Reznicek (Pavel.Reznicek@cern.ch)
# Generation of Lambda_b0 -> Lambda0 mu+ mu- decay using EvtDecay
# PRODUCTION SYSTEM FRAGMENT
#
###############################################################################

# MAKE EVTGEN USER DECAY FILE ON THE FLY
f = open("MYDECAY_Lb2Lll.DEC","w")
f.write("Decay Lambda_b0\n")
f.write("1.0000  Lambda0 mu- mu+  Lb2Lll  0 \"SM\" \"HQET\" \"Unpolarized\" \"SD\" 0 100 0;\n")
f.write("Enddecay\n")
f.write("Decay Lambda0\n")
f.write("1.0000  p+ pi-  HypNonLepton  0.642 -6.5 1;\n")
f.write("Enddecay\n")
f.write("End\n")
f.close()
#------------------------------------------------------------------------------
# Production driving parameters
#------------------------------------------------------------------------------

from MC10JobOptions.PythiaBEvgenConfig import evgenConfig
evgenConfig.auxfiles   = [ "Bdecays0.dat","DECAY.DEC","pdt.table" ]
evgenConfig.minevents  = 500
#evgenConfig.efficiency = 0.03

#------------------------------------------------------------------------------
# Import all needed algorithms (in the proper order)
#------------------------------------------------------------------------------

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include( "MC10JobOptions/MC10_PythiaB_Common.py" )

from EvtGen_i.EvtGen_iConf import EvtDecay
topAlg += EvtDecay()
EvtDecay = topAlg.EvtDecay

from GeneratorFilters.GeneratorFiltersConf import BSignalFilter
topAlg += BSignalFilter()
BSignalFilter = topAlg.BSignalFilter

#------------------------------------------------------------------------------
# PythiaB parameters settings
#------------------------------------------------------------------------------

# Stop all B-decays in Pythia and left them to decay by EvtGen
include( "MC10JobOptions/MC10_PythiaB_StopPytWeakBdecays.py" )

# Force user-finsel requiring Lambda_b in the final state
PythiaB.ForceDecayChannel      = "LambdabmumuLambda"
PythiaB.DecayChannelParameters = [ +1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 ]

# Production settings
include( "MC10JobOptions/MC10_PythiaB_Btune.py" )
PythiaB.PythiaCommand += [ "pysubs ckin 3 8.",
                           "pysubs ckin 9 -3.5",
                           "pysubs ckin 10 3.5",
                           "pysubs ckin 11 -3.5",
                           "pysubs ckin 12 3.5",
                           "pysubs msel 1" ]

# Pythia b-quark cuts
PythiaB.cutbq = [ "7. 3.0 or 0. 0." ]

# Repeated hadronization
PythiaB.mhadr = 20

#------------------------------------------------------------------------------
# Signal event filtering
#------------------------------------------------------------------------------

# Ntuple concent settings
#BSignalFilter.SignaltoNtup = 10.0*float(evgenConfig.minevents)/float(evgenConfig.efficiency)
#BSignalFilter.StoreBQuarks = True

# Muon pT cuts selection
BSignalFilter.LVL1MuonCutOn  = True
BSignalFilter.LVL1MuonCutPT  = 4000
BSignalFilter.LVL1MuonCutEta = 2.5
BSignalFilter.LVL2MuonCutOn  = True
BSignalFilter.LVL2MuonCutPT  = 4000
BSignalFilter.LVL2MuonCutEta = 2.5

# Hadronic tracks cuts
BSignalFilter.Cuts_Final_hadrons_switch = True
BSignalFilter.Cuts_Final_hadrons_pT     = 500.0
BSignalFilter.Cuts_Final_hadrons_eta    = 2.5
BSignalFilter.BParticle_cuts            = 5122

#------------------------------------------------------------------------------
# EvtGen decay table for signal
#------------------------------------------------------------------------------

EvtDecay.userDecayTableName = "MYDECAY_Lb2Lll.DEC"

#------------------------------------------------------------------------------
# POOL / Root output
#------------------------------------------------------------------------------

try:
  StreamEVGEN.RequireAlgs += [ "BSignalFilter" ]
except Exception, e:
  pass

###############################################################################
#
# End of job options fragment for Lambda_b0 -> Lambda_0 mu+ mu- decay
#
###############################################################################
