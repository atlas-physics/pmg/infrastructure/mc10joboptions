###############################################################
#
# Job options file for dijet production with pythia
#
# author: C. Gwenlan (June'10)
# AMBT1 tune [ATLAS-CONF-2010-031]
# equivalent MC9 JO: MC10.105016.J7_pythia_jetjet.py
#===============================================================

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC9_PythiaAMBT1_Common.py" )

#--------------------------------------------------------------
Pythia.PythiaCommand += [
			      "pysubs msel 0",
			      "pysubs ckin 3 1120.",
			      "pysubs ckin 4 2240.",
			      "pysubs msub 11 1",
			      "pysubs msub 12 1",
			      "pysubs msub 13 1",
			      "pysubs msub 68 1",
			      "pysubs msub 28 1",
			      "pysubs msub 53 1"]

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
