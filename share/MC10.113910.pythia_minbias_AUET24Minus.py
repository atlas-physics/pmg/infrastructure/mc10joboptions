###############################################################
#
# Job options file
# Pythia6 Minbias sample w/ AUET2 4- tune
# contact: Robindra Prabhu (prabhu@cern.ch)
#==============================================================          

#min bias sample.
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 


include ( "MC10JobOptions/MC10_PythiaAUET24Minus_Common.py" )

Pythia.PythiaCommand += [  "pysubs msel 1" ]

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig

evgenConfig.efficiency = 0.9


#==============================================================
#
# End of job options file
#
###############################################################

