###############################################################
#
# Job options file
#
#==============================================================

# ... Main generator : Pythia
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )
Pythia.PythiaCommand +=[ "pysubs msel 0",          # Users decay choice.
                         "pysubs msub 15 1",       # f fbar -> g Z
                         "pysubs msub 30 1",       # f g -> f Z
                         "pysubs ckin 41 1.",      # mass limits
                         "pysubs ckin 42 12.",
                         "pysubs ckin 43 1.",
                         "pysubs ckin 44 12.",
                         "pypars parp 42 1.",
                         "pysubs ckin 3 17.",
                         "pydat3 mdme 174 1 0",
                         "pydat3 mdme 175 1 0",
                         "pydat3 mdme 176 1 0",
                         "pydat3 mdme 177 1 0",
                         "pydat3 mdme 178 1 0",
                         "pydat3 mdme 179 1 0",
                         "pydat3 mdme 182 1 0", # Switch for Z->ee.
                         "pydat3 mdme 183 1 0",
                         "pydat3 mdme 184 1 1", # Switch for Z->mumu.
                         "pydat3 mdme 185 1 0",
                         "pydat3 mdme 186 1 0", # Switch for Z->tautau.
                         "pydat3 mdme 187 1 0"]
# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


# NO NEEDED, NO FILTER  #--------------------------------------------------------------
# NO NEEDED, NO FILTER  # Filter
# NO NEEDED, NO FILTER  #--------------------------------------------------------------
# NO NEEDED, NO FILTER  
# NO NEEDED, NO FILTER  from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
# NO NEEDED, NO FILTER  topAlg += LeptonFilter()
# NO NEEDED, NO FILTER  
# NO NEEDED, NO FILTER  LeptonFilter = topAlg.LeptonFilter
# NO NEEDED, NO FILTER  LeptonFilter.Ptcut = 10000.
# NO NEEDED, NO FILTER  LeptonFilter.Etacut = 2.7
# NO NEEDED, NO FILTER  
# NO NEEDED, NO FILTER  
# NO NEEDED, NO FILTER  #---------------------------------------------------------------
# NO NEEDED, NO FILTER  # POOL / Root output
# NO NEEDED, NO FILTER  #---------------------------------------------------------------
# NO NEEDED, NO FILTER  
# NO NEEDED, NO FILTER  try:
# NO NEEDED, NO FILTER       StreamEVGEN.RequireAlgs +=  [ "LeptonFilter" ]
# NO NEEDED, NO FILTER  except Exception, e:
# NO NEEDED, NO FILTER       pass
# NO NEEDED, NO FILTER  

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9

# if I don't use filter this value shuold be 0.9
# if I put filter value * 0.9

#==============================================================
#
# End of job options file
#
###############################################################
