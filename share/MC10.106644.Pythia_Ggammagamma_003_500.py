from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")
 
include ( "MC10JobOptions/MC10_Pythia_Common.py" )


Pythia.PythiaCommand+= [ "pysubs msel 0",
                        # 391 and 392 are the graviton subprocesses qqbar/gg
                        "pysubs msub 391 1",
                        "pysubs msub 392 1",
                        "pydat3 mdcy 347 1 1",
                        # I  5000039  Graviton*    (m =  500.000 GeV)  -->
                        "pydat2 pmas 5000039 1 500",
                        "pydat3 mdme 4084 1 0",
                        "pydat3 mdme 4085 1 0",
                        "pydat3 mdme 4086 1 0",
                        "pydat3 mdme 4087 1 0",
                        "pydat3 mdme 4088 1 0",
                        "pydat3 mdme 4089 1 0",
                        "pydat3 mdme 4090 1 0",
                        "pydat3 mdme 4091 1 0",
                        "pydat3 mdme 4092 1 0",
                        # select decay to gamma gamma:
                        "pydat3 mdme 4093 1 0",
                        "pydat3 mdme 4094 1 0",
                        "pydat3 mdme 4095 1 0",
                        "pydat3 mdme 4096 1 0",
                        "pydat3 mdme 4097 1 0",
                        "pydat3 mdme 4098 1 0",
                        "pydat3 mdme 4099 1 0",
                        "pydat3 mdme 4100 1 0",
                        "pydat3 mdme 4101 1 1",
                        "pydat3 mdme 4102 1 0",
                        "pydat3 mdme 4103 1 0",
                        "pydat3 mdme 4104 1 0",
                        # PARP(50) : (D=0.054) dimensionless coupling, which enters quadratically
                        #                    in all partial widths of the excited graviton G* resonance,
                        #                     is kappa_mG* = sqrt(2)*x1*k/MPl~, where x1~3.83 is the first zero
                        #                     of the J1 Bessel function and MPl~ is the modified
                        #                     Planck mass scale [Ran99, Bij01].
                        #                     default corresponds to k/Mpl~=0.01 in hep-ph/0006114
			#   0.162=sqrt(2)*3.83*0.03 k/Mpl~=0.03
                        "pypars parp 50 0.162",
                        # cutoff for QED FSR in Pythia to 20000 GeV (='infinity', photos takes care of it).
                        "pydat1 parj 90 20000.",
                        # switch off tau decays in Pythia
                        "pydat3 mdcy 15 1 0"]

include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )
 
## ... Photos

include ( "MC10JobOptions/MC10_Photos_Fragment.py" )
 
#---------------------------------------------------------

from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
