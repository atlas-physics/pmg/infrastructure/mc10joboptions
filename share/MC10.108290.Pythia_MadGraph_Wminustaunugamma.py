#--------------------------------------------------------------
#Acceptance sample
# W-(taunu) + Gamma with full interference between ISR and FSR digram with MadGraph/Pythia
#--------------------------------------------------------------
# Non-filtered cross section in Rel. 15.6.1 : 16.8 pb
# No filter , efficiency = 0.174 safe factor= 0.9
# File prepared by Zhijun Liang  May 2009
#--------------------------------------------------------------

###############################################################
#
# Job options file
#
#==============================================================
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_PythiaMC09p_Common.py" )


Pythia.PythiaCommand += [ "pyinit user lhef",
                          "pyinit pylisti -1","pyinit pylistf 1","pyinit dumpr 1 2",
                          "pydat1 parj 90 20000", # Turn off FSR.
                          "pydat3 mdcy 15 1 0"   # Turn off tau decays.
]

## ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment_LeptonicDecay.py" )

## ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )



from GeneratorFilters.GeneratorFiltersConf import PhotonFilter
topAlg += PhotonFilter()
PhotonFilter = topAlg.PhotonFilter
PhotonFilter.Ptcut = 7000.
PhotonFilter.Etacut = 2.5
PhotonFilter.NPhotons = 1

from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
topAlg += LeptonFilter()
LeptonFilter = topAlg.LeptonFilter
LeptonFilter.Etacut = 2.5

try:
    StreamEVGEN.RequireAlgs +=  [ "PhotonFilter" ]
except Exception, e:
    pass
try:
    StreamEVGEN.RequireAlgs += [ "LeptonFilter" ]
except Exception, e:
    pass
#
#

from MC10JobOptions.PythiaEvgenConfig import evgenConfig
#from EvgenJobOptions.MadGraphEvgenConfig import evgenConfig
evgenConfig.generators += ["Lhef", "Pythia"]
evgenConfig.inputfilebase = 'group09.phys-gener.MadGraph442Pythia.108290.Wminustaunugamma.TXT.v1'
evgenConfig.efficiency = 0.15
evgenConfig.minevents = 2000


#==============================================================
#
# End of job options file
#
###############################################################

