###############################################################
#
# Job options file
# Wouter Verkerke
#
#==============================================================
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 3

#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------

# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 

try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common.py" )
  if runArgs.ecmEnergy == 14000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_14TeV.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig


Herwig.HerwigCommand += [ "iproc alpgen",
                          "taudec TAUOLA",
                        ]
# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

# Muon filter
from GeneratorFilters.GeneratorFiltersConf import MuonFilter
topAlg += MuonFilter()

MuonFilter.Ptcut  = 10.*GeV 
MuonFilter.Etacut = 2.8

try:
     StreamEVGEN.RequireAlgs += [ "MuonFilter" ]
except Exception, e:
     pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.AlpgenEvgenConfig import evgenConfig
 
# inputfilebase
evgenConfig.inputfilebase = 'alpgen.108830.QcdJ4Np4.TOPmufilt.pt20'
# Information on sample 108830
# Filter efficiency  = 0.01133
# MLM matching efficiency = 0.139
# Number of Matrix Elements in input file  = 375000
# Alpgen cross section = 359579.0 pb
# Herwig cross section = Alpgen cross section * eff(MLM) = 50056.0 pb
# Cross section after filtering = 567.1 pb
# Lumi/500 events post-filter = 500/eff(Filter)/XS(jimmy) = 0.8817 pb-1
#
# Information on sample 108830
# Filter efficiency  = 0.0113
# MLM matching efficiency = 0.14
# Number of Matrix Elements in input file  = 475000
# Alpgen cross section = 359579.0 pb
# Herwig cross section = Alpgen cross section * eff(MLM) = 50056.0 pb
# Cross section after filtering = 567.1 pb
# Lumi/500 events post-filter = 500/eff(Filter)/XS(jimmy) = 0.88 pb-1
#
# Filter efficiency estimate below reduced by 20% to produce 625 events on average,
# of which only 500 will be used in further processing
evgenConfig.efficiency = 0.00906


#==============================================================
#
# End of job options file
#
###############################################################
