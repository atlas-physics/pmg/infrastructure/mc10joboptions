###############################################################
#
# Job options file
# Christoph Wasicki
# (based on original by Wouter Verkerke)
#
#==============================================================
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 3

#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------

#-- Dll's and Algorithms
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

# ... Main generator : Herwig
try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common_7TeV.py" )
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common.py" )
  if runArgs.ecmEnergy == 14000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common_14TeV.py" )
except NameError:
    # needed (dummy) default for first scan of jo
    from Herwig_i.Herwig_iConf import Herwig
    topAlg += Herwig()
    Herwig = topAlg.Herwig

Herwig.HerwigCommand += [ "taudec TAUOLA"]

# ... New mass
Herwig.HerwigCommand.append( 'rmass 6 170.0' )

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import TTbarWToLeptonFilter
topAlg += TTbarWToLeptonFilter()

TTbarWToLeptonFilter = topAlg.TTbarWToLeptonFilter
TTbarWToLeptonFilter.Ptcut = 1.

try:
    StreamEVGEN.RequireAlgs = [ "TTbarWToLeptonFilter" ]
except Exception, e:
    pass


from MC10JobOptions.McAtNloEvgenConfig import evgenConfig

evgenConfig.inputfilebase = 'mcatnlo'
try:
    if runArgs.ecmEnergy == 7000.0:
        evgenConfig.inputfilebase = 'group09.phys-gener.mcatnlo341.106201.ttbar_7TeV.TXT.v1'
    if runArgs.ecmEnergy == 8000.0:
        evgenConfig.inputfilebase = 'group09.phys-gener.mcatnlo341.106201.ttbar_8TeV.TXT.v1'
    if runArgs.ecmEnergy == 10000.0:
        evgenConfig.inputfilebase = 'group09.phys-gener.mcatnlo341.106201.ttbar_10TeV.TXT.v1'
except NameError:
    pass

evgenConfig.efficiency = 0.5

#--------------------------------------------------------------
# MC@NLO data card
#--------------------------------------------------------------
#  './mcatnlo341.106201.ttbar_7TeV.TXT.v1._00026'                ! prefix for BASES files
#  './mcatnlo341.106201.ttbar_7TeV.TXT.v1._00026'                ! prefix for event files
#   7000 1 1 1 1 ! energy, fren, ffact, frenmc, ffactmc
#   -1706                          ! -1705/1706=bb/tt
#   170.0                       ! M_Q
#   0 0               ! 0..6 -> t dec, 7 -> t undec
#   1.3200                      ! top width
#   80.403 2.141                  ! M_W, Gamma_W
#   0 0 0 ! GammaX, M_T(min), M_T(max)
#   0 0 0 ! GammaX, M_Tb(min), M_Tb(max)
#   30 0 0 ! GammaX, M_V1(min), M_V1(max)
#   30 0 0 ! GammaX, M_V2(min), M_V2(max)
#   0.9748 0.2225 0.0036                  ! |V_ud|,|V_us|,|V_ub|
#   0.2225 0.9740 0.041                  ! |V_cd|,|V_cs|,|V_cb|
#   0.009 0.0405 0.9992                  ! |V_td|,|V_ts|,|V_tb|
#   1                       ! 0=t->Wb, 1=t->W+any d
#   0.1111                     ! t -> leptons branching ratio
#   0.3333                     ! t -> hadrons branching ratio
#   0.32 0.32 0.5 1.55 4.95 0.75 ! quark and gluon masses
#  'P'  'P'               ! hadron types
#  'LHAPDF'   10550            ! PDF group and id number
#   -1                     ! Lambda_5, <0 for default
#  'MS'                   ! scheme
#   15000                       ! number of events
#   1                        ! 0 => wgt=+1/-1, 1 => wgt=+w/-w
#   95361399                          ! seed for rnd numbers
#   0.3                             ! zi
#   10 10                 ! itmx1,itmx2

#--------------------------------------------------------------
# HERWIG data card
#--------------------------------------------------------------
#  'mcatnlo341.106201.ttbar_7TeV.TXT.v1._00026.events'        ! event file
#   15000                       ! number of events
#   1                        ! 0->Herwig PDFs, 1 otherwise
#  'P'  'P'               ! hadron types
#   3500 3500               ! beam momenta
#   -1706                          ! -1705/1706=bb/tt
#  'LHAPDF'                      ! PDF group (1)
#   10550                         ! PDF id number (1)
#  'LHAPDF'                      ! PDF group (2)
#   10550                         ! PDF id number (2)
#   -1                     ! Lambda_5, < 0 for default
#   170.0 80.403                 ! M_t, M_W
#   0.32 0.32 0.5 1.55 4.95 0.75 ! quark and gluon masses

#==============================================================
#
# End of job options file
#
###############################################################
