###############################################################
#
# Job options file
# Wouter Verkerke, Daniel Geerts
#
#==============================================================
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 3

#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------

# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 
try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common.py" )
  if runArgs.ecmEnergy == 14000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_14TeV.py" )
except NameError:
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig 

Herwig.HerwigCommand += [ "iproc alpgen",
                          "taudec TAUOLA",
                        ]
# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from MC10JobOptions.AlpgenEvgenConfig import evgenConfig
 
# inputfilebase
evgenConfig.inputfilebase = 'alpgen'
try:
  if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputfilebase = 'group09.phys-gener.alpgen.107671.ZtautauNp1_pt20_7tev.TXT.v3'
  if runArgs.ecmEnergy == 8000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.alpgen.107671.ZtautauNp1_pt20_8tev.TXT.v2'
  if runArgs.ecmEnergy == 10000.0:
    evgenConfig.inputfilebase = 'alpgen.107671.ZtautauNp1.pt20.v2'
except NameError:
  pass

evgenConfig.minevents = 5000
evgenConfig.efficiency = 0.90000

#
# 7 TeV - Information on sample 107671
# 7 TeV - Filter efficiency  = 1.0000
# 7 TeV - MLM matching efficiency = 0.42
# 7 TeV - Number of Matrix Elements in input file  = 15500
# 7 TeV - Alpgen cross section = 313.8 pb
# 7 TeV - Herwig cross section = Alpgen cross section * eff(MLM) = 133.0 pb
# 7 TeV - Cross section after filtering = 133.0 pb
# 7 TeV - Lumi/5000 events post-filter = 5000/eff(Filter)/XS(jimmy) = 37.6 pb-1
#
# 7 TeV - Filter efficiency estimate below reduced by 10% to produce 5556 events on average,
# 7 TeV - of which only 5000 will be used in further processing
#
# 8 TeV - Information on sample 	107671
# 8 TeV - Filter efficiency  = 	1.0000
# 8 TeV - MLM matching efficiency = 	0.42
# 8 TeV - Number of Matrix Elements in input file  = 	1550
# 8 TeV - Alpgen cross section = 	383.9	 pb
# 8 TeV - Herwig cross section = Alpgen cross section * eff(MLM) = 	162.8	 pb
# 8 TeV - Cross section after filtering = 	162.8	 pb
# 8 TeV - Lumi/5000 events post-filter = 5000/eff(Filter)/XS(jimmy) = 	30.7	 pb-1
#
# 8 TeV - Filter efficiency estimate below reduced by 10% to produce 5556 events on average,
# 8 TeV - of which only 5000 will be used in further processing		
#
# 10 TeV - Information on sample 107671
# 10 TeV - Filter efficiency  = 1.000
# 10 TeV - MLM matching efficiency = 0.41
# 10 TeV - Actual number of Matrix Elements in input file = 1600
# 10 TeV - Alpgen cross section = 505.7 pb
# 10 TeV - Herwig cross section = Alpgen cross section * eff(MLM) = 209.3 pb
# 10 TeV - Lumi/500 events post-filter = 500/eff(Filter)/XS(jimmy) = 2.389 pb-1

#==============================================================
#
# End of job options file
#
###############################################################
