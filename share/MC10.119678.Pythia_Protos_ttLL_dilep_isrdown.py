###############################################################
# Job options file for generating tt events with Protos
# Ning Zhou (ning.zhou@cern.ch)
#==============================================================
#
from AthenaCommon.AlgSequence import AlgSequence
topAlg=AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_PythiaMC09p_Common.py" )

Pythia.PythiaCommand += ["pyinit user protos",
                        "pydat1 parj 90 20000.",
                        "pydat3 mdcy 15 1 0",
                        "pyinit dumpr 1 12",
                        "pyinit pylistf 1"
                       ]


Pythia.PythiaCommand +=[ "pypars parp 67 0.5" ]
Pythia.PythiaCommand +=[ "pypars parp 64 4.0" ]


# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------

from MC10JobOptions.PythiaEvgenConfig import evgenConfig

# dummy needed
evgenConfig.inputfilebase = 'Protos'
try:
  if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputfilebase ='group10.phys-gener.Protos.119264.ttLL.TXT.v1'
except NameError:
  pass


evgenConfig.efficiency = 1.0

#==============================================================
# End of job options file
#
###############################################################

