# Single pi0 in log(E) between 200 MeV and 2 TeV

# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR,  6=FATAL )

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

from ParticleGenerator.ParticleGeneratorConf import ParticleGenerator
topAlg += ParticleGenerator()


ParticleGenerator = topAlg.ParticleGenerator

# For DEBUG output from ParticleGenerator.
ParticleGenerator.OutputLevel = 2

ParticleGenerator.orders = [
  "PDGcode: constant 111",
  "e: log 200. 2000000.",
  "eta: flat -5.5 5.5",
  "phi: flat -3.14159 3.14159"
  ]

#==============================================================
#
# End of job options file
#
###############################################################
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.SingleEvgenConfig import evgenConfig
