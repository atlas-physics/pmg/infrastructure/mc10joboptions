###############################################################
#
# Job options file
#
# Herwig ZH production with a higgs and Z pT filter
# Author: Giacinto Piacquadio (8/7/2008)
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 

try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_Herwig_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_Herwig_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_Herwig_Common.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig

Herwig.HerwigCommand += ["iproc 12705",
                         "rmass 201 120.0",
			 "taudec TAUOLA"]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

# Add the filters:
# Higgs and Z filter
from GeneratorFilters.GeneratorFiltersConf import ParticleFilter
from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
ParticleFilterHiggs= ParticleFilter(name = "ParticleFilterHiggs",
                                    StatusReq=195,
                                    PDG=25,
                                    Ptcut = 150.*GeV,
                                    Etacut = 3.0,
                                    Energycut = 1000000000.0)

ParticleFilterZ= ParticleFilter(name = "ParticleFilterZ",
                                StatusReq=195,
                                PDG=23,
                                Ptcut = 100.*GeV,
                                Etacut = 3.0,
                                Energycut = 1000000000.0)

LeptonFilter= LeptonFilter(name = "LeptonFilter")
LeptonFilter.Ptcut = 15.*GeV
LeptonFilter.Etacut = 3.0 

topAlg += LeptonFilter
topAlg += ParticleFilterHiggs
topAlg += ParticleFilterZ

try:
     StreamEVGEN.RequireAlgs += [ "LeptonFilter" ]
     StreamEVGEN.RequireAlgs += [ "ParticleFilterHiggs" ]
     StreamEVGEN.RequireAlgs += [ "ParticleFilterZ" ]
except Exception, e:
     pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.HerwigEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.0298
# 5000/150958=0.0331
#==============================================================
#
# End of job options file
#
###############################################################
