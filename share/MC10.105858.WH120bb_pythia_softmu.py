#--------------------------------------------------------------
# File prepared by Henri Bachacou Feb 2006
#--------------------------------------------------------------
# Generator:
#--------------------------------------------------------------
include( "MC10JobOptions/MC10.105850.WH120bb_pythia.py" )

#--------------------------------------------------------------
# Filter:
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

from GeneratorFilters.GeneratorFiltersConf import SoftLeptonFilter
topAlg += SoftLeptonFilter()

SoftLeptonFilter = topAlg.SoftLeptonFilter
SoftLeptonFilter.Ptcut = 3000.
SoftLeptonFilter.Etacut = 2.8
SoftLeptonFilter.LeptonType = 2
#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------
try:
     StreamEVGEN.RequireAlgs +=  [ "SoftLeptonFilter" ]
except Exception, e:
     pass


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.2
