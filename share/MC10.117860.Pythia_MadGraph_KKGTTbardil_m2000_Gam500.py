#------------------------------------------------------------------------------------
#
# top group jO
# KKGluon -> TTbar -> dileptons sample with m=2000 GeV, Gamma=500 GeV
#
#------------------------------------------------------------------------------------

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_PythiaMC09p_Common.py" )
Pythia.PythiaCommand += ["pyinit user madgraph"]

Pythia.PythiaCommand +=  [
    "pysubs msel 0",
    "pystat 1 3 4 5",
    "pyinit dumpr 1 5",
    "pyinit pylistf 1",
    "pydat1 parj 90 20000",
    "pydat3 mdcy 15 1 0",
    "pyinit pylistf 1",  # 1 = normal listing
    "pyinit pylisti 12",
    ]

include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

from MC10JobOptions.MadGraphEvgenConfig import evgenConfig

evgenConfig.inputfilebase = 'madgraph'
try:
  if runArgs.ecmEnergy == 7000:
    evgenConfig.inputfilebase = 'group10.phys-gener.madgraph445.117860.KKGTTbardil_m2000_Gam500_7TeV.TXT.v2'
  if runArgs.ecmEnergy == 8000:
    evgenConfig.inputfilebase = 'group10.phys-gener.madgraph445.117860.KKGTTbardil_m2000_Gam500_8TeV.TXT.v2'
  if runArgs.ecmEnergy == 10000:
    evgenConfig.inputfilebase = 'group10.phys-gener.madgraph445.117860.KKGTTbardil_m2000_Gam500_10TeV.TXT.v2'
  if runArgs.ecmEnergy == 14000:
    evgenConfig.inputfilebase = 'group10.phys-gener.madgraph445.117860.KKGTTbardil_m2000_Gam500_14TeV.TXT.v2'
except NameError:
  pass 

evgenConfig.efficiency = 0.9
