###############################################################
#
# Job options file
# Bill Quayle, 01 March 2010
# (wbquayle@gmail.com)
#
#==============================================================
#--------------------------------------------------------------
# ATLAS default Application Configuration options
#--------------------------------------------------------------
import AthenaCommon.AtlasUnixGeneratorJob

theApp.ExtSvc += ["AtRndmGenSvc"]
AtRndmGenSvc = Service( "AtRndmGenSvc" )

#--------------------------------------------------------------
# Set output level threshold (DEBUG, INFO, WARNING, ERROR, FATAL)
#--------------------------------------------------------------
#
## get a handle on the ServiceManager
from AthenaCommon.AppMgr import ServiceMgr as svcMgr
svcMgr.MessageSvc.OutputLevel = INFO

#
## get a handle on the top level algorithm sequence
from AthenaCommon.AlgSequence import AlgSequence
topSequence = AlgSequence()

#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
from Sherpa_i.Sherpa_iConf import Sherpa_i
sherpa = Sherpa_i()
sherpa.RunPath = "./"
topSequence += sherpa

#--------------------------------------------------------------
# EVGEN Config
#--------------------------------------------------------------

from MC10JobOptions.SherpaFEvgenConfig import evgenConfig

# dummy needed
evgenConfig.inputfilebase = 'sherpa'
try:
  if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputfilebase = 'group09.phys-gener.sherpa010103.109953.SherpaH260WWtoll_7TeV.TXT.v1'
  if runArgs.ecmEnergy == 8000.0:
    evgenConfig.inputfilebase = 'group09.phys-gener.sherpa010103.109953.SherpaH260WWtoll_8TeV.TXT.v1'
except NameError:
  pass
                      
evgenConfig.efficiency = 0.9

#--------------------------------------------------------------
#
# End of job options file
############################################################### 
