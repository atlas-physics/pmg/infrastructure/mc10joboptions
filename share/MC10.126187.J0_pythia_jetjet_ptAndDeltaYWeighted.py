###############################################################
#
# Job options file
#
# Pythia J0 event generation with pT-deltaY weighting.
# cross section as default pythia sample = 9.860*10^6 nb
#
# Prepared by Graham Jones, Graham.Jones [at] cern.ch 
#==============================================================

# dijet production with pythia
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 


include ( "MC10JobOptions/MC10_Pythia_Common.py" )

Pythia.PythiaCommand += ["pysubs msel 0",
                         "pysubs ckin 3 8.",
                         "pysubs ckin 4 17.",
                         "pysubs msub 11 1",
                         "pysubs msub 12 1",
                         "pysubs msub 13 1",
                         "pysubs msub 68 1",
                         "pysubs msub 28 1",
                         "pysubs msub 53 1"
                         ]


#--------------------------------------------------------------
# Truth Jets
#--------------------------------------------------------------

# Make truth jets for JetForwardFilter:
from AthenaCommon.AppMgr import ServiceMgr as svcMgr
include( "AthenaCommon/Atlas_Gen.UnixStandardJob.py" )
from JetRec.JetGetters import *
Kt6Alg = make_StandardJetGetter('AntiKt',0.6,'Truth').jetAlgorithmHandle()
Kt6Alg.AlgTools['JetFinalPtCut'].MinimumSignal = 7.0*GeV

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

## Truth filter
# Add the filter to the correct sequence!
from GeneratorFilters.GeneratorFiltersConf import JetForwardFilter
forwardFilter=JetForwardFilter()
topAlg += forwardFilter

# General properties
forwardFilter.JetContainer = "AntiKt6TruthJets"

# Kinematic cuts
forwardFilter.MinDeltaY = 0.0
forwardFilter.MaxDeltaY = 100.0
forwardFilter.MinPt1 = 12.0 #GeV
forwardFilter.MinPt2 = 12.0
forwardFilter.MaxPt1 = 70000.0
forwardFilter.MaxPt1 = 70000.0

#Gaussian mixture model describing pdf of unweighted sample
forwardFilter.muXs = [ 17.7420634518, 14.914007724, 15.6690842482, 20.2817607961, 24.2118800539,  ]
forwardFilter.muYs = [ 1.37233910155, 1.23457768835, 3.81086718226, 2.17781683742, 2.829029821,  ]
forwardFilter.sigmaXs = [ 2.42853629135, 1.44057307975, 1.95451311386, 3.08731632177, 4.61510414421,  ]
forwardFilter.sigmaYs = [ 0.86287321238, 0.806111102987, 1.37607728857, 1.22496134837, 1.68151264015,  ]
forwardFilter.rhos = [ -0.205752886994, -0.0448147711113, 0.0411736584681, -0.194790164266, -0.274925302465,  ]
forwardFilter.weights = [ 0.249538892002, 0.285933889979, 0.212281104625, 0.18265821733, 0.069587896063,  ]

#Target probability (how much weighting required roughly)
#Lower = more suppression
import math
forwardFilter.SuppressionFactor = 6.0*math.pow(10.0, -6.0)

#--------------------------------------------------------------
# Apply filtering
#--------------------------------------------------------------

try:
  StreamEVGEN.RequireAlgs += [ "JetForwardFilter" ]
except Exception, e:
     pass


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
# N_gen = 341190876; N_pass = 100000 -> eff = 2.930*10^-4
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.0002930
evgenConfig.minevents=1000
evgenConfig.weighting=0
