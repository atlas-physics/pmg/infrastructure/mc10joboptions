 ###############################################################
#
# Job Option for producting correlated antiLambda-antiLambda with Q in [0, 1] GeV
# originated from the same primary vertex
#
# Author        : Hok-Chuen (Tom) Cheng
# Date/Location : Oct 20, 2017/ Ann Arbor, MI
# Last update   : Oct 26, 2017
#
#==============================================================

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

from ParticleGenerator.ParticleGeneratorConf import ParticleGenerator
topAlg += ParticleGenerator()

ParticleGenerator = topAlg.ParticleGenerator
ParticleGenerator.OutputLevel = 5

ParticleGenerator.orders = [
 "id[0]: constant -3122",
 "pt[0]: flat 500. 10000.",
 "eta[0]: flat -2.7 2.7",
 "phi[0]: flat 0. 6.2832",
 "VertX[0]: gauss 0. 0.015",
 "VertY[0]: gauss 0. 0.015",
 "vertZ[0]: gauss 0. 56.",
 "id[1]: constant -3122",
 "pt[1]: flat 500. 10000.",
 "eta[1]: flat -2.7 2.7",
 "phi[1]: flat 0. 6.2832",
]


#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import MassRangeFilter
topAlg += MassRangeFilter()

from math import sqrt

MassRangeFilter            = topAlg.MassRangeFilter
MassRangeFilter.PtCut      = 0
MassRangeFilter.PtCut2     = 0
MassRangeFilter.EtaCut     = 2.7
MassRangeFilter.EtaCut2    = 2.7
MassRangeFilter.PartId     = -3122
MassRangeFilter.PartId2    = -3122
MassRangeFilter.InvMassMin = 0
MassRangeFilter.InvMassMax = sqrt(1000**2 + 4*1115.682**2) # Q in [0, 1] GeV since M^2 = Q^2 + (2*m0)^2

try:
     StreamEVGEN.RequireAlgs = [ "MassRangeFilter" ]
except Exception, e:
     pass

#---------------------------------------------------------------
# Pool Persistency
#---------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.SingleEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.003
#==============================================================
#
# End of job options file
#
###############################################################
