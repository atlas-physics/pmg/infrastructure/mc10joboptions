###############################################################
#
# Job options file
# Wolfgang F. Mader, Tue Mar  9 16:32:04 CET 2010
# (Wolfgang.Mader@CERN.CH)
#
#==============================================================
#--------------------------------------------------------------
# ATLAS default Application Configuration options
#--------------------------------------------------------------
import AthenaCommon.AtlasUnixGeneratorJob

theApp.ExtSvc += ["AtRndmGenSvc"]
AtRndmGenSvc = Service( "AtRndmGenSvc" )

#--------------------------------------------------------------
# Set output level threshold (DEBUG, INFO, WARNING, ERROR, FATAL)
#--------------------------------------------------------------
#
## get a handle on the ServiceManager
from AthenaCommon.AppMgr import ServiceMgr as svcMgr
svcMgr.MessageSvc.OutputLevel = INFO

#
## get a handle on the top level algorithm sequence
from AthenaCommon.AlgSequence import AlgSequence
topSequence = AlgSequence()

#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
from Sherpa_i.Sherpa_iConf import Sherpa_i
sherpa = Sherpa_i()
sherpa.RunPath = "./"
topSequence += sherpa

#--------------------------------------------------------------
# EVGEN Config
#--------------------------------------------------------------

from MC10JobOptions.SherpaFEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'group09.phys-gener.sherpa010103.109934.SherpabbAmumuMA300TB20_7TeV.TXT.v1'
evgenConfig.efficiency = 0.9

#--------------------------------------------------------------
#
# End of job options file
#
############################################################### 
