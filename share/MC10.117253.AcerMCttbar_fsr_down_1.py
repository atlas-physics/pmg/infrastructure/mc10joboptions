#______________________________________________________________________________________________________________#
# author: L. Mijovic (liza.mijovic@_nospam_cern.ch) for the top group                                          #
# FSR systematics sample: less FSR activity, external LO process + PYTHIA                                      #
# reference sample for Pythia param. variation is 105205
# PARP(72) and PARJ(82) are varied                                                                             #
# AMBT1 default values: PARP(72)=0.192 GeV, PARJ(82)=1.GeV                                                      #
# PARP (72) :  lambda_FSR                                                                                      #
# PARJ (82) :  FSR regularization IR cutoff                                                                    #
#              see Pythia Manual for more info                                                                 #
#______________________________________________________________________________________________________________#
#
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

Pythia.PythiaCommand += [ "pyinit user acermc",
			 "pydat1 parj 90 20000.",
			 "pydat3 mdcy 15 1 0" ]

  
Pythia.PythiaCommand += [ "pypars mstp 3 1",      #set mstp 3 1 (setting Lambda in alpha_s)
                          "pypars parp 1 0.192",  #reset the defaults after mstp 3 1
                          "pypars parp 72 0.144", #FSR Labmda value in running alpha_s (ATLAS def 0.192)
                          "pydat1 paru 112 0.192",#reset defaults after mstp 3 1
                          "pydat1 mstu 112 4",    #reset defaults after mstp 3 1
                          "pypars parp 61 0.192"] #reset defaults after mstp 3 1

Pythia.PythiaCommand +=[ "pydat1 parj 82 2.0" ]   

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.AcerMCEvgenConfig import evgenConfig


#dummy needed
evgenConfig.inputfilebase = 'acermc'
try:
  if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputfilebase = 'group09.phys-gener.acermc37.105205.tt_7TeV.TXT.v1'
  if runArgs.ecmEnergy == 10000.0:
    evgenConfig.inputfilebase = 'group09.phys-gener.acermc37.105205.tt_10TeV.TXT.v1'
except NameError:
  pass

evgenConfig.efficiency = 0.95

#==============================================================
#
# End of job options file
#
###############################################################

