###############################################################
#
# Job options file for Evgen
#
# Prepared by Graham Jones <graham.jones@cern.ch>
#
# Rel 15.1.0.3 (June 2009)
#
# QCD 2->2 events with large eta intervals between the highest
# Et jets. J2 parton Pt range.
#
#==============================================================

#Herwig as main generator
from AthenaCommon.AlgSequence import AlgSequence
topAlg=AlgSequence("TopAlg")
 

try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_Herwig_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_Herwig_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_Herwig_Common.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig


Herwig.HerwigCommand += ["iproc 11500",
                         "ptmin 35.0",
                         "ptmax 70.0",
                         "taudec TAUOLA" #for tau decay
                         ]


#Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

#Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Truth Jets
#--------------------------------------------------------------

# Make truth jets for JetGapFilter:
from AthenaCommon.AppMgr import ServiceMgr as svcMgr
include( "AthenaCommon/Atlas_Gen.UnixStandardJob.py" )
from JetRec.JetGetters import *
Kt7Alg = make_StandardJetGetter('Kt',0.7,'Truth', False).jetAlgorithmHandle()

#Change min pt of found jets to 0.0
Kt7Alg.AlgTools['JetFinalEtCut'].MinimumSignal = 0.0*GeV

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

## Truth filter
# Add the filter
from GeneratorFilters.GeneratorFiltersConf import JetIntervalFilter
etaFilter=JetIntervalFilter()
topAlg += etaFilter

# General properties
etaFilter.JetContainer = "Kt7TruthJets"
etaFilter.JetNumber = 2
etaFilter.OppositeSideJets = False

# Jet Energy Cuts (Cut on Et)
etaFilter.JetCutByPt = False

# Only used if cutting by et
etaFilter.Jet1MinEt = 12.0*GeV
etaFilter.Jet1MaxEt = 7000.0*GeV
etaFilter.Jet2MinEt = 12.0*GeV
etaFilter.Jet2MaxEt = 7000.0*GeV

# Jet Position Cuts (absolute)
etaFilter.Jet1MaxEta = 100.0
etaFilter.Jet1MinEta = 0.0
etaFilter.Jet2MaxEta = 100.0
etaFilter.Jet2MinEta = 0.0

# Jet delta eta cut
etaFilter.MinDeltaEta = 3.0
etaFilter.MaxDeltaEta = 10.0

# Control event weighting
etaFilter.WeightEvents = True
etaFilter.UniformMaxWeightBelowGausMean = False

# J2 tune
etaFilter.GausMean = 0.730
etaFilter.GausSigma = 1.83
etaFilter.AbsDEtaCutOff = 8.5

#--------------------------------------------------------------
# POOL / Root output
#--------------------------------------------------------------

try:
  StreamEVGEN.RequireAlgs += [ "JetIntervalFilter" ]
except Exception, e:
     pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
#Efficiency = (10000.0/28720553.0) * 0.9 = 0.0003132
#Non-filtered cross section in Rel. 15.1.0.3 : 45160470.0 pb
#--------------------------------------------------------------

from MC10JobOptions.HerwigEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.000313
evgenConfig.minevents=50

#==============================================================
#
# End of job options file
#
###############################################################
