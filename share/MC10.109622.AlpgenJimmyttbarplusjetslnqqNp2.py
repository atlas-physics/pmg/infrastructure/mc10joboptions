###############################################################
#
# Job options file
#
# Alpgen ttbar + 2 jets exclusive mode then
# Herwig plus ttbar plus jets filter 
#
# Responsible person(s)
#   18 April, 2009: Simon Dean (sdean@hep.ucl.ac.uk)
#
#==============================================================

MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 3

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")
 

try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common.py" )
  if runArgs.ecmEnergy == 14000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_14TeV.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig

Herwig.HerwigCommand+= [ "iproc alpgen" ]
Herwig.HerwigCommand+= [ "taudec TAUOLA"]

include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

# Alpgen dataset alpgen.109622.AlpgenJimmyttbarplusjetslnqqNp2
from MC10JobOptions.AlpgenEvgenConfig import evgenConfig
 
# inputfilebase
evgenConfig.inputfilebase = 'alpgen.109622.ttbarlnqqNp2'

# TTbarPlusJetsFilter efficiency
evgenConfig.efficiency = 0.7950

###############################################################
# Filter efficiency = 79.50%
# MLM matching efficiency = 20.84%
# 4527 events per ME tarball producing 750 output events.
# Alpgen cross section = 91.37 pb
# Herwig cross section =
#   Alpgen cross section * eff(MLM) = 19.04 pb
# Only 500 events will be used in further processing
# Lumi/500 events = 500/(eff(filter)*XS(Herwig)) = 33.03 pb^-1
#==============================================================

# Make truth jets
from JetRec.JetRecConf import JetAlgorithm
import JetRec.JetRecConf as JR
import JetSimTools.JetSimToolsConf as JST
topAlg += JetAlgorithm("TruthConeJets")
TruthConeJets = topAlg.TruthConeJets
TruthConeJets.JetCollectionName = "ConeTruthJets"
toollist = [
    JST.JetsFromTruthTool("TruthLoader"),
    JR.JetSignalSelectorTool("InitialEtCut"),
    JR.JetConeFinderTool("ConeFinder"),
    JR.JetSplitMergeTool("SplitMerge"),
    JR.JetSignalSelectorTool("FinalEtCut") ]
toollist[0].MinPt                = 0.*MeV
toollist[0].MaxEta               = 5.
toollist[0].IncludeMuons         = False
toollist[0].TruthCollectionName  = "GEN_EVENT"
toollist[0].OutputCollectionName = "ConeParticleJets"
toollist[1].UseTransverseEnergy = True
toollist[1].MinimumSignal       = 0.*MeV
toollist[2].ConeR    = 0.4
toollist[2].SeedPt   = 2.*GeV
toollist[4].UseTransverseEnergy = True
toollist[4].MinimumSignal       = 10.*GeV
TruthConeJets.AlgTools = [ t.getFullName() for t in toollist ]
for t in toollist :
    TruthConeJets += t

# Apply ttbar plus jets filter
from GeneratorFilters.GeneratorFiltersConf import TTbarPlusJetsFilter
topAlg += TTbarPlusJetsFilter()
TTbarPlusJetsFilter = topAlg.TTbarPlusJetsFilter
TTbarPlusJetsFilter.PtMinJet = 15000.
TTbarPlusJetsFilter.EtaMaxJet = 5.2
TTbarPlusJetsFilter.NbJetMin = 6
TTbarPlusJetsFilter.PtMinJetB = 15000.
TTbarPlusJetsFilter.EtaMaxJetB = 2.7
TTbarPlusJetsFilter.NbJetBMin = 3
TTbarPlusJetsFilter.NbLeptonMin = 0
TTbarPlusJetsFilter.SelectLepHadEvents = False
TTbarPlusJetsFilter.NbEventMax = 0
TTbarPlusJetsFilter.OutputLevel = 1

try:
    StreamEVGEN.RequireAlgs += [ "TTbarPlusJetsFilter" ]
except Exception, e:
    pass

