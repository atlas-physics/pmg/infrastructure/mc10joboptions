###############################################################
#
# Job options file for generation of B-events 
#  in user selected exclusive channel
#
#==============================================================
from MC10JobOptions.PythiaBEvgenConfig import evgenConfig
evgenConfig.minevents = 500
# Reduced events/file due to long production times 
evgenConfig.efficiency = 0.40 
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_PythiaB_Common.py" )

#--------------------------------------------------------------
# Algorithms
#--------------------------------------------------------------
#from GeneratorFilters.GeneratorFiltersConf import BSignalFilter
#topAlg += BSignalFilter()
from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
topAlg += MultiLeptonFilter()
#--------------------------------------------------------------				 
#              PARAMETERS  SPECIFIC  TO   PYTHIAB
#--------------------------------------------------------------
PythiaB.ForceCDecay = "no"
PythiaB.ForceBDecay = "yes"
#--------------------------------------------------------------				 
# -------------  FORCE   YOUR  B CHANNEL  HERE -------------
#--------------------------------------------------------------
# To force your B-decay channels decomment following 2 lines:
include( "MC10JobOptions/MC10_PythiaB_CloseAntibQuark.py" )
PythiaB.PythiaCommand += ["pydat3 mdme 889 1 1",    
                          "pydat3 mdme 858 1 1",
				"pydat3 mdme 859 1 0",
				"pydat3 mdme 860 1 0"        ]
#--------------------------------------------------------------
# --------  PYTHIA PARAMETERS OPTIMAL FOR BEAUTY PRODUCTION  --
#--------------------------------------------------------------
include( "MC10JobOptions/MC10_PythiaB_Btune.py" )

PythiaB.PythiaCommand += ["pysubs ckin 3 15.",
                                "pysubs ckin 9 -3.5",
				"pysubs ckin 10 3.5",
				"pysubs ckin 11 -3.5",
				"pysubs ckin 12 3.5",
                                "pydat1 mstj 22 2",
          "pysubs msel 1"]
#--------------------------------------------------------------
# -------------  DEFINE SELECTION CUTS  -------------
#--------------------------------------------------------------				
#  ------------- Selections on b  quarks   -------------
# simulate  only b-flavour events
PythiaB.flavour =  5.
# PythiaB force exclusive decay channels only on b=-5 side 
# ------------------- b=5  --- and/or ---  b=-5 --------
PythiaB.cutbq = ["10. 2.5 and 10. 2.5"]
#PythiaB.cutbq = ["6. 2.5 and 6. 2.5"]
#  ------------- LVL1 muon cuts 0=OFF 1=ON -------------
PythiaB.lvl1cut = [ 1.,  6., 2.5]
#  ------------- LVL2 muon/electron cuts  0=OFF 1=ON-------------
PythiaB.lvl2cut = [ 1.,  11.,     3.,   2.5]
#  ------------- Offline cuts 0=OFF 1=ON -------------
PythiaB.offcut = [ 0., 0.0, 102.5, 0., 102.5, 3.0, 2.5]
#  ------------- Number of repeated hadronization mhadr -------------
PythiaB.mhadr =  15. 
# Add the filters:
#BSignalFilter = topAlg.BSignalFilter
#BSignalFilter.LVL1MuonCutOn = TRUE
#BSignalFilter.LVL1MuonCutPT = 6000.0
#BSignalFilter.LVL1MuonCutEta = 2.5
#BSignalFilter.Cuts_Final_e_switch = TRUE
#BSignalFilter.Cuts_Final_e_pT = 3000.0
#BSignalFilter.Cuts_Final_e_eta = 2.5
MultiLeptonFilter = topAlg.MultiLeptonFilter
#MultiLeptonFilter.Ptcut = 3.0*GeV
MultiLeptonFilter.Ptcut = 3000.0
MultiLeptonFilter.Etacut = 2.5
MultiLeptonFilter.NLeptons = 3
###############################################################
# Add POOL persistency
try:
     StreamEVGEN.RequireAlgs +=  [ "MultiLeptonFilter" ]
	
except Exception, e:
     pass
#==============================================================
#
# End of job options file
#
###############################################################
