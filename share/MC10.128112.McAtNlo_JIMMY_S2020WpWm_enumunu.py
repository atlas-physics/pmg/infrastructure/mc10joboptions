################################################################
#
# MC@NLO/JIMMY/HERWIG W+W- -> e+nu mu-nu
#   Special sample with renormalization scale 2.0 and factorization scale 2.0
#
# Responsible person(s)
#   Jun  6, 2011 : Tiesheng Dai (Tiesheng.Dai@cern.ch)
#
################################################################
#
# Job options file
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 
# ... Herwig
try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common.py" )
  if runArgs.ecmEnergy == 14000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common_14TeV.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig

#
### Match W and Z mass and width with MC@NLO event file
### to avoid miss leading information from event generator log file
#
Herwig.HerwigCommand += [ "rmass 198 80.40", "rmass 199 80.40", "gamw 2.141",
                          "rmass 200 91.19", "gamz 2.495",
                          "maxpr 5",
                          "taudec TAUOLA"]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

from MC10JobOptions.McAtNloEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'mcatnlo0341.128112'
try:
  if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.mcatnlo0341.128112.07TeV_S2020WpWm_enumunu.TXT.v1'
  if runArgs.ecmEnergy == 8000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.mcatnlo0341.128112.08TeV_S2020WpWm_enumunu.TXT.v1'
  if runArgs.ecmEnergy == 10000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.mcatnlo0341.128112.10TeV_S2020WpWm_enumunu.TXT.v1'
except NameError:
  pass
                
evgenConfig.efficiency = 0.9

#==============================================================
#
# End of job options file
#
###############################################################
