###############################################################
#
# Job options file
#
# Alpgen ttbarbbbar + 0 jets inclusive mode then
# Herwig plus ttbar plus jets filter 
# This differs from 109625 by reducing the b-quark
# pT cut to 0 GeV, the dR(b,b) cut to 0 and
# using a Q-value of 232.5 GeV = mtop + mH/2
#
# Responsible person(s)
#   2nd July, 2009: Simon Dean (sdean@hep.ucl.ac.uk)
#
#==============================================================

MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 3

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")
 

try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common.py" )
  if runArgs.ecmEnergy == 14000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_14TeV.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig

Herwig.HerwigCommand+= [ "iproc alpgen" ]
Herwig.HerwigCommand+= [ "taudec TAUOLA"]

include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

# Alpgen dataset alpgen.109625.AlpgenJimmyttbarbbbarlnqqNp0
from MC10JobOptions.AlpgenEvgenConfig import evgenConfig
 
# inputfilebase
evgenConfig.inputfilebase = 'alpgen.109627.ttbarbbbarlnqqNp0'
evgenConfig.minevents=5000

# TTbarPlusJetsFilter efficiency
evgenConfig.efficiency = 0.71679*0.95

###############################################################
# Filter efficiency = 71.68%
# MLM matching efficiency = 100%
# 8370 events per ME tarball producing 6000 output events.
# Alpgen cross section = Herwig cross section = 3.70354 pb
# Only 5000 events will be used in further processing
# Lumi/5000 events = 5000/(eff(filter)*XS(Herwig)) = 1.88 fb^-1
#==============================================================

# Make truth jets
from JetRec.JetRecConf import JetAlgorithm
import JetRec.JetRecConf as JR
import JetSimTools.JetSimToolsConf as JST
topAlg += JetAlgorithm("TruthConeJets")
TruthConeJets = topAlg.TruthConeJets
TruthConeJets.JetCollectionName = "ConeTruthJets"
toollist = [
    JST.JetsFromTruthTool("TruthLoader"),
    JR.JetSignalSelectorTool("InitialEtCut"),
    JR.JetConeFinderTool("ConeFinder"),
    JR.JetSplitMergeTool("SplitMerge"),
    JR.JetSignalSelectorTool("FinalEtCut") ]
toollist[0].MinPt                = 0.*MeV
toollist[0].MaxEta               = 5.
toollist[0].IncludeMuons         = False
toollist[0].TruthCollectionName  = "GEN_EVENT"
toollist[0].OutputCollectionName = "ConeParticleJets"
toollist[1].UseTransverseEnergy = True
toollist[1].MinimumSignal       = 0.*MeV
toollist[2].ConeR    = 0.4
toollist[2].SeedPt   = 2.*GeV
toollist[4].UseTransverseEnergy = True
toollist[4].MinimumSignal       = 10.*GeV
TruthConeJets.AlgTools = [ t.getFullName() for t in toollist ]
for t in toollist :
    TruthConeJets += t

# Apply ttbar plus jets filter
from GeneratorFilters.GeneratorFiltersConf import TTbarPlusJetsFilter
topAlg += TTbarPlusJetsFilter()
TTbarPlusJetsFilter = topAlg.TTbarPlusJetsFilter
TTbarPlusJetsFilter.PtMinJet = 15000.
TTbarPlusJetsFilter.EtaMaxJet = 5.2
TTbarPlusJetsFilter.NbJetMin = 6
TTbarPlusJetsFilter.PtMinJetB = 15000.
TTbarPlusJetsFilter.EtaMaxJetB = 2.7
TTbarPlusJetsFilter.NbJetBMin = 3
TTbarPlusJetsFilter.NbLeptonMin = 0
TTbarPlusJetsFilter.SelectLepHadEvents = False
TTbarPlusJetsFilter.NbEventMax = 0
TTbarPlusJetsFilter.OutputLevel = 1

try:
    StreamEVGEN.RequireAlgs += [ "TTbarPlusJetsFilter" ]
except Exception, e:
    pass

