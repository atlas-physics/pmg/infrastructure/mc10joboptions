###############################################################
# PRODUCTION SYSTEM FRAGMENT
#
# bb->e7X
#
# Author: Darren Price (Darren.Price@cern.ch)
#
# Job options file for generation of B events 
# Selection criteria can be changed by datacards
#==============================================================

#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_PythiaB_Common.py" )

#--- cutoff for QED FSR in Pythia to 20000 GeV (='infinity', photos takes care of it).
PythiaB.PythiaCommand += [ "pydat1 parj 90 20000" ]

#--------------------------------------------------------------				 
#              PARAMETERS  SPECIFIC  TO   PYTHIABMODULE
#--------------------------------------------------------------
PythiaB.ForceBDecay = "no"
PythiaB.ForceCDecay = "no"

#--------------------------------------------------------------
# --------  PYTHIA PARAMETERS OPTIMAL FOR BEAUTY PRODUCTION  --
#--------------------------------------------------------------
include( "MC10JobOptions/MC10_PythiaB_Btune.py" )
    
PythiaB.PythiaCommand += [ "pysubs ckin 3 9.",
                           "pysubs msel 1" ]

#--- Selecton cut in PyhtiaB
#  ------------- Selections on b  quarks   -------------
# simulate  only b-flavour events
PythiaB.flavour =  5.				
#  ------------- Selections on b  quarks   -------------
PythiaB.cutbq = [ "5. 4.5 or 5. 4.5" ]
#  ------------- LVL1 muon cuts 0=OFF 1=ON -------------
PythiaB.lvl1cut = [ 0.,  6., 2.5 ]
#  ------------- LVL2 muon/electron cuts  0=OFF 1=ON-------------
#PythiaB.lvl2cut = [ 0.,  13.,     6.,   2.5 ]
PythiaB.lvl2cut = [ 1.,  11.,     7.,   2.5 ]
#  ------------- Offline cuts 0=OFF 1=ON -------------
PythiaB.offcut = [ 0., 0., 102.5, 0., 102.5, 0., 102.5 ]
#  ------------- Number of repeated hadronization mhadr -------------
PythiaB.mhadr =  5. 

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
# efficiency = 500/501*0.9 = 0.898
from MC10JobOptions.PythiaBEvgenConfig import evgenConfig
evgenConfig.minevents = 500
evgenConfig.efficiency = 0.898
