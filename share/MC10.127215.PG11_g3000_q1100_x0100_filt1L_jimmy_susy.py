from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")


try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_Herwig_Common_7TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_Herwig_Common.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig

Herwig.HerwigCommand += [ "iproc 13000",
                          "susyfile susy_PG11_g3000_q1100_x0100.txt",
                          "taudec TAUOLA" ]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )



#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------


from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
topAlg += MultiElecMuTauFilter()

MultiElecMuTauFilter = topAlg.MultiElecMuTauFilter
MultiElecMuTauFilter.NLeptons  = 1
MultiElecMuTauFilter.MinPt = 12000.
MultiElecMuTauFilter.MaxEta = 6.
MultiElecMuTauFilter.MinVisPtHadTau = 5000. # pt-cut on the visible hadronic tau
MultiElecMuTauFilter.IncludeHadTaus = False # one can choose whether to include hadronic taus or not

try:
     StreamEVGEN.RequireAlgs = [ "MultiElecMuTauFilter" ]
except Exception, e:
     pass


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.HerwigEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.3980


from MC10JobOptions.SUSYEvgenConfig import evgenConfig

#==============================================================
#
# End of job options file
#
###############################################################
