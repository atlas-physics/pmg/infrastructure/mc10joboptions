###############################################################
# PRODUCTION SYSTEM FRAGMENT
#
# Job options file for generation of B-events 
#  in user selected exclusive channel
#
# bb->J/psi->e(3)e(3)X with Photos
#
# Author: Gabriella Pasztor (Gabriella.Pasztor@cern.ch)
#
#==============================================================

#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_PythiaB_Common.py" )

#--- cutoff for QED FSR in Pythia to 20000 GeV (='infinity', photos takes care of it).
PythiaB.PythiaCommand += ["pydat1 parj 90 20000"]

#--------------------------------------------------------------				 
#              PARAMETERS  SPECIFIC  TO   PYTHIAB
#--------------------------------------------------------------
PythiaB.ForceCDecay = "no"

#--------------------------------------------------------------				 
# -------------  FORCE   YOUR  B CHANNEL  HERE -------------
#--------------------------------------------------------------
#--- To force your B-decay channels decomment following 2 lines:
# following line becomes redundant as MC10_PythiaB_Jpsichannels.py redefines the decay table
#include( "MC10JobOptions/MC10_PythiaB_CloseAntibQuark.py" )  
PythiaB.ForceBDecay = "yes"

#--- Inclusive B -> J/psi(ee) X production
include( "MC10JobOptions/MC10_PythiaB_Jpsichannels.py" )

#--------------------------------------------------------------
# --------  PYTHIA PARAMETERS OPTIMAL FOR BEAUTY PRODUCTION  --
#--------------------------------------------------------------
include( "MC10JobOptions/MC10_PythiaB_Btune.py" )

#---   Force J/psi to e+ e-
PythiaB.PythiaCommand += [ "pydat3 mdme 858 1 1", #e+e-
                           "pydat3 brat 858 1.",  
                           "pydat3 mdme 859 1 0", #mu+mu-
                           "pydat3 mdme 860 1 0", #rndmflav
                           "pysubs ckin 3 1.",
# why change PythiaB default tuning of +-4.5 for the allowed range of 2->2 decay products?
#                           "pysubs ckin 9 -3.5",
#                           "pysubs ckin 10 3.5",
#                           "pysubs ckin 11 -3.5",
#                           "pysubs ckin 12 3.5",
                           "pysubs msel 1" ]

#--- Selecton cut in PyhtiaB
#  ------------- Selections on b  quarks   -------------
# simulate  only b-flavour events
PythiaB.flavour =  5.
# PythiaB force exclusive decay channels only on b=-5 side 
# ------------------- b=5  --- and/or ---  b=-5 --------
PythiaB.cutbq = ["0. 102.5 and 0. 102.5"]
#  ------------- LVL1 muon cuts 0=OFF 1=ON -------------
PythiaB.lvl1cut = [ 0., 0., 102.5]
#  ------------- LVL2 muon/electron cuts  0=OFF 1=ON-------------
PythiaB.lvl2cut = [ 1., 11., 3., 2.7]
#  ------------- Offline cuts 0=OFF 1=ON -------------
PythiaB.offcut = [ 0., 0., 102.5, 0., 102.5, 0., 102.5]
#  ------------- Number of repeated hadronization mhadr -------------
PythiaB.mhadr =  1. 

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import MultiElectronFilter
topAlg += MultiElectronFilter()

MultiElectronFilter = topAlg.MultiElectronFilter
MultiElectronFilter.Ptcut = 3000.0
MultiElectronFilter.Etacut = 2.7
MultiElectronFilter.NElectrons = 2

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------

try:
    StreamEVGEN.RequireAlgs += [ "MultiElectronFilter" ]
except Exception, e:
    pass

###############################################################
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
# Cross section and efficiency in Rel. 15.6.6.5:
# non-filtered cross section = 0.174559 mbarn
# efficiency = 500/4253=0.118 - double electron filter on top of lvl2 cut
###############################################################
from MC10JobOptions.PythiaBEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.1
#  ------------- Number of events requested  -------------
evgenConfig.minevents = 500
#==============================================================
#
# End of job options file
#
###############################################################
