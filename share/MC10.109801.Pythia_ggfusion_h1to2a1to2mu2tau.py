###############################################################
#
# Job options file
#
# Pythia gluon fusion, h1->2a1->2mu2tau [mh1=100 GeV, ma1=5 GeV], tau->lep
#
# Responsible person(s)
#   October 2009: Chris Potter (chris.potter@cern.ch)
#   Thanks to Andy Haas!
#
#==============================================================

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

Pythia.PythiaCommand += ["pysubs msel 0",
                         "pysubs msub 152 1",        # gluon fusion production H0 (h1)
                         "pydat1 parj 90 20000",     # Turn off lepton radiation
                         "pydat3 mdcy 15 1 0"]       # Turn off tau decays

# set the masses and widths

Pythia.PythiaCommand += ["pydat2 pmas 35 1 100"]   # H0 (h1) mass
Pythia.PythiaCommand += ["pydat2 pmas 36 1 5"]     # A0 mass
Pythia.PythiaCommand += ["pydat2 pmas 36 2 0.001"] # A0 width
Pythia.PythiaCommand += ["pydat2 pmas 36 3 0.002"] # A0 max mass dev.

# set the H0 (h1) decay

Pythia.PythiaCommand += ["pydat3 mdme 334 1 0", # ddbar
                         "pydat3 mdme 335 1 0", # uubar
                         "pydat3 mdme 336 1 0", # ssbar
                         "pydat3 mdme 337 1 0", # ccbar
                         "pydat3 mdme 338 1 0", # bbbar
                         "pydat3 mdme 339 1 0", # ttbar
                         "pydat3 mdme 340 -1 0",# 2b'
                         "pydat3 mdme 341 -1 0",# 2t'
                         "pydat3 mdme 342 1 0", # 2e
                         "pydat3 mdme 343 1 0", # 2mu
                         "pydat3 mdme 344 1 0", # 2tau
                         "pydat3 mdme 345 -1 0",# 2tau'
                         "pydat3 mdme 346 1 0", # 2glue
                         "pydat3 mdme 347 1 0", # 2gamma
                         "pydat3 mdme 348 1 0", # Zgamma
                         "pydat3 mdme 349 1 0", # 2Z
                         "pydat3 mdme 350 1 0", # 2W
                         "pydat3 mdme 351 1 0", # Zh0
                         "pydat3 mdme 352 1 0", # 2h0
                         "pydat3 mdme 353 1 0", # W+H-
                         "pydat3 mdme 354 1 0", # H+W-
                         "pydat3 mdme 355 1 0", # ZA0
                         "pydat3 mdme 356 1 0", # h0A0
                         "pydat3 mdme 357 1 1"] # 2A0 (2a1)

# set the A0 (a1) decay

Pythia.PythiaCommand += ["pydat3 mdme 420 1 0", # ddbar
                         "pydat3 mdme 421 1 0", # uubar
                         "pydat3 mdme 422 1 0", # ssbar
                         "pydat3 mdme 423 1 0", # ccbar
                         "pydat3 mdme 424 1 0", # bbbar
                         "pydat3 mdme 425 1 0", # ttbar
                         "pydat3 mdme 426 -1 0",# 2b'
                         "pydat3 mdme 427 -1 0",# 2t'
                         "pydat3 mdme 428 1 0", # 2e
                         "pydat3 mdme 429 1 4", # 2mu
                         "pydat3 mdme 430 1 5", # 2tau
                         "pydat3 mdme 431 -1 0",# 2tau'
                         "pydat3 mdme 432 1 0", # 2glue
                         "pydat3 mdme 433 1 0", # 2gamma
                         "pydat3 mdme 434 1 0", # Zgamma
                         "pydat3 mdme 435 1 0", # 2Z
                         "pydat3 mdme 436 1 0", # 2W
                         "pydat3 mdme 437 1 0", # Zh0
                         "pydat3 mdme 438 1 0", # 2h0
                         "pydat3 mdme 439 1 0", # W+H-
                         "pydat3 mdme 440 1 0"] # H+W-

# Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# End jobO fragment
#--------------------------------------------------------------
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.95
