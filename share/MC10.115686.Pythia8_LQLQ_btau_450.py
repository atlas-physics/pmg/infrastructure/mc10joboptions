###############################################################
#==============================================================
# Job options file
# Pythia8 3rd generation Leptoquark samples
# Contact: Katharine Leney
#==============================================================
###############################################################

# ... Main generator : Pythia8
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ("MC10JobOptions/MC10_Pythia8_Common.py")

############################################
# Pythia 8 settings for LQLQ -> b,tau+b,tau
############################################
Pythia8.Commands += ["LeptoQuark:gg2LQLQbar = on",       # Switch on gluon fusion production mode
                     "LeptoQuark:qqbar2LQLQbar = on",    # Switch on q-qbar production mode               
                     "LeptoQuark:kCoup = 0.01",          # Should this be higher? (Def should for single LQ production)                    
                     "42:m0 = 450.0",                    # Set mass of leptoquark
                     "42:mMin = 350.0",
                     "42:mMax = 550.0",
                     "42:0:products = 5 -15",            # Tell the LQ to decay to a tau and a b-quark
                     "42:0:bRatio = 1.0",                # Set branching ratio = 1
                     "15:mayDecay = on",                 # Allow taus to decay
                     "5:mayDecay = on",                  # Allow b-quarks to decay
                     ]


############################################
# Filter
############################################

from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
topAlg += MultiLeptonFilter()

MultiLeptonFilter = topAlg.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 10000.
MultiLeptonFilter.Etacut = 2.7
MultiLeptonFilter.NLeptons = 1

try:
     StreamEVGEN.RequireAlgs +=  [ "MultiLeptonFilter" ]
except Exception, e:
     pass


############################################
# Configuration for EvgenJobTransforms
############################################

from MC10JobOptions.Pythia8EvgenConfig import evgenConfig
evgenConfig.efficiency = 0.7  


