###############################################################
#
# Job options file
#
# MadGraph gg H->Z'Z'->4l (mH=130GeV, mZ'=5GeV l=e,mu)
#
#
# Mathieu Aurousseau (mathieu.aurousseau@cern.ch)
#
#==============================================================


from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

# --> use the following pythia tune if CTEQ6l1 pdf has been used
include ( "MC10JobOptions/MC10_PythiaMC09p_Common.py" )

Pythia.PythiaCommand += [ "pyinit user madgraph",
                          "pyinit pylisti -1","pyinit pylistf 1","pyinit dumpr 1 2",
                          "pydat1 parj 90 20000", # Turn off FSR.
                          "pydat3 mdcy 15 1 0"   # Turn off tau decays.
                          ]

## ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

## ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.generators += ["MadGraph", "Pythia"]
evgenConfig.inputfilebase = 'group10.phys-gener.MadGraph.116673.ggH130_ZpZp5_4l.TXT.v1'
evgenConfig.efficiency = 1. # no filter
evgenConfig.minevents = 5000
evgenConfig.maxeventsfactor = 18.


