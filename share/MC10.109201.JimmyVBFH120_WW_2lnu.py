################################################################
#
# JIMMY/HERWIG VBF (Vector Boson Fusion) H -> WW -> lnulnu
# where l=e or mu with Higgs Mass 120GeV
#
# Responsible person(s)
#   Oct 22, 2008 : Tiesheng Dai (Tiesheng.Dai@cern.ch)
#
################################################################
#
# Job options file
#
#==============================================================

# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 

try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_Herwig_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_Herwig_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_Herwig_Common.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig


Herwig.HerwigCommand += [ "iproc 11910",
                          "rmass 201 120.0",
                          "modbos 1 5", "modbos 2 5",
                          "maxpr 10",
                          "taudec TAUOLA"]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.HerwigEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.90

#==============================================================
#
# End of job options file
#
###############################################################
