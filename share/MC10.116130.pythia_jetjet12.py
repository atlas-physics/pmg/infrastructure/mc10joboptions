#
# dijet production with pythia
# - QCD like J0,2,3... but apply JF17-like event filter for fake electron studies.
# - ckin(3)=10GeV
# - narrow jet pT>12GeV
# (Junichi TANAKA)
#
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 


include ( "MC10JobOptions/MC10_Pythia_Common.py" )

#--------------------------------------------------------------
# File prepared by Ian Hinchliffe Nov 2005
#--------------------------------------------------------------
Pythia.PythiaCommand += [
			      "pysubs msel 0",
			      "pysubs ckin 3 10.",
			      "pysubs msub 11 1",
			      "pysubs msub 12 1",
			      "pysubs msub 13 1",
			      "pysubs msub 68 1",
			      "pysubs msub 28 1",
			      "pysubs msub 53 1"]

#--------------------------------------------------------------
# Filter Options ('standard' jet filter)
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import JetFilter
topAlg += JetFilter()

JetFilter = topAlg.JetFilter
JetFilter.JetNumber = 1
JetFilter.EtaRange = 2.7
JetFilter.JetThreshold = 12000.;  # Note this is 12 GeV
JetFilter.JetType=False; #true is a cone, false is a grid
JetFilter.GridSizeEta=2; # sets the number of (approx 0.06 size) eta
JetFilter.GridSizePhi=2; # sets the number of (approx 0.06 size) phi cells

#--------------------------------------------------------------
# Pool Output Options
#--------------------------------------------------------------
try:
    StreamEVGEN.RequireAlgs += ["JetFilter"]
except Exception, e:
    pass
              
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.0649*0.9
