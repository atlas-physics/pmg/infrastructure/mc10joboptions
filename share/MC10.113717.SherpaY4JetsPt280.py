from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence('TopAlg')
from Sherpa_i.Sherpa_iConf import Sherpa_i
sherpa = Sherpa_i()

"""
(processes){
  Process 93 93 -> 22 93 93{3}
  Order_EW 1
  CKKW sqr(30.0/E_CMS)
  Integration_Error 0.04 {5};
  Integration_Error 0.05 {6};
  End process
}(processes)

(selector){
  PT  22  280.0  E_CMS
  DeltaR  22  93  0.3  20.0
}(selector)
"""
try:
    sherpa.Parameters += [ 'RUNDATA=%s' % runArgs.jobConfig[0] ]
except NameError:
    pass
sherpa.Parameters += [ 'WRITE_MAPPING_FILE=3' ]
sherpa.Parameters += [ 'MASSIVE[15]=1' ]
sherpa.Parameters += [ 'ME_SIGNAL_GENERATOR=Comix' ]

topAlg += sherpa
from MC10JobOptions.SherpaFEvgenConfig import evgenConfig

# dummy needed
evgenConfig.inputfilebase = 'sherpa'
try:
  if runArgs.ecmEnergy == 7000.0:
      evgenConfig.inputfilebase = 'group10.phys-gener.sherpa010203.113717.SherpaY4JetsPt280_7TeV.TXT.v2'
  if runArgs.ecmEnergy == 8000.0:
      evgenConfig.inputfilebase = 'group10.phys-gener.sherpa010203.113717.SherpaY4JetsPt280_8TeV.TXT.v1'
except NameError:
  pass


evgenConfig.efficiency = 0.9
# evgenConfig.minevents = 5000
evgenConfig.weighting = 0
