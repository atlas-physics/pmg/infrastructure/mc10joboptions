##################################################################
#
# Job options file
#
# Pythia ttbar->bW bH+, W->lepnu, H+ ->Wa1->Wlep2mu (lep=e/mu/tau)
#   [mH+=130GeV, ma1=9GeV, tan(beta)=2]
#
# Responsible person(s)
#   October 2009: Chris Potter (chris.potter@cern.ch)
#
#=================================================================

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

Pythia.PythiaCommand += ["pysubs msel 0",
                         "pysubs msel 6",          # ttbar production
                         "pydat1 parj 90 20000",   # Turn off lepton radiation
                         "pydat3 mdcy 15 1 0"]     # Turn off tau decays

# set the H+ and h0 (a1) masses, and tan beta
Pythia.PythiaCommand += ["pydat2 pmas 37 1 130"]
Pythia.PythiaCommand += ["pydat2 pmas 25 1 9"]
Pythia.PythiaCommand += ["pydat1 paru 141 2"]

# set the top decays
Pythia.PythiaCommand += ["pydat3 mdme 41 1 0",
                         "pydat3 mdme 42 1 0",
                         "pydat3 mdme 43 1 0",
                         "pydat3 mdme 44 1 0",
                         "pydat3 mdme 45 1 0",
                         "pydat3 mdme 46 1 2", # t->Wb
                         "pydat3 mdme 48 1 0",
                         "pydat3 mdme 49 1 3", # t->H+b
                         "pydat3 mdme 50 1 0",
                         "pydat3 mdme 51 1 0",
                         "pydat3 mdme 52 1 0",
                         "pydat3 mdme 53 1 0",
                         "pydat3 mdme 54 1 0",
                         "pydat3 mdme 55 1 0"]

# set the W decays
Pythia.PythiaCommand += ["pydat3 mdme 190 1 0",
                         "pydat3 mdme 191 1 0",
                         "pydat3 mdme 192 1 0",
                         "pydat3 mdme 194 1 0",
                         "pydat3 mdme 195 1 0",
                         "pydat3 mdme 196 1 0",
                         "pydat3 mdme 198 1 0",
                         "pydat3 mdme 199 1 0",
                         "pydat3 mdme 200 1 0",
                         "pydat3 mdme 206 1 1", #W-> e nu
                         "pydat3 mdme 207 1 1", #W-> mu nu
                         "pydat3 mdme 208 1 1"] #W-> tau nu 

# set the H+ decay
Pythia.PythiaCommand += ["pydat3 mdme 503 1 0",
                         "pydat3 mdme 504 1 0", #H+ -> c sbar
                         "pydat3 mdme 505 1 0",
                         "pydat3 mdme 507 1 0",
                         "pydat3 mdme 508 1 0",
                         "pydat3 mdme 509 1 0", #H+ -> tau+ nu
                         "pydat3 mdme 511 1 1", #H+ ->W+ h0
                         "pydat3 mdme 512 1 0",
                         "pydat3 mdme 513 1 0",
                         "pydat3 mdme 514 1 0",
                         "pydat3 mdme 515 1 0",
                         "pydat3 mdme 516 1 0",
                         "pydat3 mdme 517 1 0",
                         "pydat3 mdme 518 1 0",
                         "pydat3 mdme 519 1 0",
                         "pydat3 mdme 520 1 0",
                         "pydat3 mdme 521 1 0",
                         "pydat3 mdme 522 1 0",
                         "pydat3 mdme 523 1 0",
                         "pydat3 mdme 524 1 0",
                         "pydat3 mdme 525 1 0",
                         "pydat3 mdme 526 1 0",
                         "pydat3 mdme 527 1 0",
                         "pydat3 mdme 528 1 0",
                         "pydat3 mdme 529 1 0"]


# set the h0 (a1) decay
Pythia.PythiaCommand += ["pydat3 mdme 210 1 0",# h0->qqbar
                         "pydat3 mdme 211 1 0",# h0->qqbar
                         "pydat3 mdme 212 1 0",# h0->qqbar
                         "pydat3 mdme 213 1 0",# h0->qqbar
                         "pydat3 mdme 214 1 0",# h0->qqbar
                         "pydat3 mdme 215 1 0",# h0->qqbar
                         "pydat3 mdme 218 1 0",# h0->ee
                         "pydat3 mdme 219 1 1",# h0->mumu
                         "pydat3 mdme 220 1 0",# h0->tautau
                         "pydat3 mdme 222 1 0",# h0->gg
                         "pydat3 mdme 223 1 0",# h0->YY
                         "pydat3 mdme 224 1 0",# h0->gZ
                         "pydat3 mdme 225 1 0",# h0->ZZ
                         "pydat3 mdme 226 1 0"] # h0->WW

# Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

# Tauola 
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

#--------------------------------------------------------------
# End jobO fragment
#--------------------------------------------------------------
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.95
