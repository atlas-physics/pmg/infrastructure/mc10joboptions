###############################################################
#
# Job options file
# Created by Pavel Staroba on the request of Lashkar Kashif
# in Rel. 14.2.23.4 (December 2008)
# MC10.105140.JimmyZee.py without filter
#
#===============================================================

# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")
 

try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_Herwig_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_Herwig_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_Herwig_Common.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig


Herwig.HerwigCommand += [ "iproc 11351",
                          "emmin 60.",
                          "taudec TAUOLA"]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
# efficiency = 0.9 - no filtering
from MC10JobOptions.HerwigEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.90
#==============================================================
#
# End of job options file
#
###############################################################
