###############################################################
#
# Job options file
#
# Pythia J1 event generation with pT-deltaY weighting.
# cross section as default pythia sample = 6.78*10^5 nb
#
# Prepared by Graham Jones, Graham.Jones [at] cern.ch 
#==============================================================


# dijet production with pythia
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

Pythia.PythiaCommand += ["pysubs msel 0",
                         "pysubs ckin 3 17.",
                         "pysubs ckin 4 35.",
                         "pysubs msub 11 1",
                         "pysubs msub 12 1",
                         "pysubs msub 13 1",
                         "pysubs msub 68 1",
                         "pysubs msub 28 1",
                         "pysubs msub 53 1"]

#--------------------------------------------------------------
# Truth Jets
#--------------------------------------------------------------

# Make truth jets for JetForwardFilter:
from AthenaCommon.AppMgr import ServiceMgr as svcMgr
include( "AthenaCommon/Atlas_Gen.UnixStandardJob.py" )
from JetRec.JetGetters import *
Kt6Alg = make_StandardJetGetter('AntiKt',0.6,'Truth').jetAlgorithmHandle()
Kt6Alg.AlgTools['JetFinalPtCut'].MinimumSignal = 7.0*GeV

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

## Truth filter
# Add the filter to the correct sequence!
from GeneratorFilters.GeneratorFiltersConf import JetForwardFilter
forwardFilter=JetForwardFilter()
topAlg += forwardFilter

# General properties
forwardFilter.JetContainer = "AntiKt6TruthJets"

# Kinematic cuts
forwardFilter.MinDeltaY = 0.0
forwardFilter.MaxDeltaY = 100.0
forwardFilter.MinPt1 = 12.0 #GeV
forwardFilter.MinPt2 = 12.0
forwardFilter.MaxPt1 = 70000.0
forwardFilter.MaxPt2 = 70000.0

#Gaussian mixture model describing pdf of unweighted sample
forwardFilter.muXs = [ 25.5306426597, 36.6441246206, 19.9858398133, 30.3340948787, 21.6071907616  ]
forwardFilter.muYs = [ 1.59339296777, 2.71750157768, 2.82512699635, 1.82770080038, 1.26381679716  ]
forwardFilter.sigmaXs = [ 5.16959648881, 8.45648135993, 3.36783142967, 5.63849668332, 3.86795892279  ]
forwardFilter.sigmaYs = [ 1.04631435002, 1.67872817895, 1.60344865479, 1.21409157427, 0.82019194845  ]
forwardFilter.rhos = [ 0.0447161260178, -0.304856797893, 0.150549395229, -0.0982948384467, 0.0169941212472  ]
forwardFilter.weights = [ 0.231905254679, 0.0622945624605, 0.236657014085, 0.170366489821, 0.298776678954  ]

#Target probability (how much weighting required roughly)
#Lower = more suppression
import math
forwardFilter.SuppressionFactor = 4.0*math.pow(10.0, -7.0)

#--------------------------------------------------------------
# Apply filtering
#--------------------------------------------------------------

try:
  StreamEVGEN.RequireAlgs += [ "JetForwardFilter" ]
except Exception, e:
  pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
# N_gen = 351058810; N_pass = 100000-> eff = 2.849*10^-4
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.0002849
evgenConfig.minevents=1000 #~18 hours
evgenConfig.weighting=0
