#
# Job options file
# Wolfgang Mader, June 25 2009
# (Wolfgang.Mader@SLAC.Stanford.EDU)
#
#==============================================================

import AthenaCommon.AtlasUnixGeneratorJob
from AthenaCommon.AppMgr import ServiceMgr as svcMgr

#load relevant libraries
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

from Sherpa_i.Sherpa_iConf import ReadSherpa_i
sherpa = ReadSherpa_i()
sherpa.Files = [ "sherpa.evts" ]

topAlg += sherpa

#--------------------------------------------------------------
# Truth Jets
#--------------------------------------------------------------

#from RecExConfig.RecFlags  import rec
#rec.doTruth = True

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from MC10JobOptions.SherpaEvgenConfig import evgenConfig
 
# inputfilebase
evgenConfig.inputfilebase = 'Sherpa010103.109237.Z3jetstoeeEW_v2'
evgenConfig.efficiency = 1.00*0.95
