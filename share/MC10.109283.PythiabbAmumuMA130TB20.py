###############################################################
#
# Job options file
#
# Pythia MSSM bbA, bbA->mumu, tanBeta=20, MA=130GeV
#
# Responsible person(s)
#  14 Nov, 2008 - xx xxx, 20xx: Marija Vranjes Milosavljevic (marijam@phy.bg.ac.yu)
#
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

# ... Pythia
include ( "MC10JobOptions/MC10_Pythia_Common.py" )
Pythia.PythiaCommand += [     "pysubs msel 0",
                              "pypars mstp 2 2",     #select second order running alpha strong  
                              "pypars mstp 33 3",    #include a K factor in hard cross sections for parton-parton interactions by a shift in the alpha strong sqr(Q) argument
                              "pypars mstp 125 1",   # documentation of partonic process,lists ultimate string/particle configuration and short summary of the hard process 
                              "pypars mstp 4 1",			                                               
                              "pymssm imss 1 1",      # General MSSM
                              "pymssm rmss 1 200.0",  #set SU(1) gaugino mass 
                              "pymssm rmss 2 200.0",  #set SU(2) gaugino mass
                              "pymssm rmss 3 800.0",  #set SU(3) gluino mass parameter
                              "pymssm rmss 4 -200.0", #set higgsino mass parameter mu
                              "pymssm rmss 5 20",     #set tan(beta) (ratio of Higgs expectation values) <===
                              "pymssm rmss 6 1000.0", #set left slepton mass (physical mass with no additional D terms)
                              "pymssm rmss 7 1000.0", #set right slepton mass (physical mass with no additional D terms)
                              "pymssm rmss 8 1000.0", #set left squark mass (physical mass will have additional D terms)
                              "pymssm rmss 9 1000.0", #set right squark mass (physical mass will have additional D terms)
                              "pymssm rmss 10 1000.0",#set left squark mass for the third generation
                              "pymssm rmss 11 1000.0",#set right sbottom mass  
                              "pymssm rmss 12 1000.0",#set right stop mass
                              "pymssm rmss 13 1000.0",#set left stau mass
                              "pymssm rmss 14 1000.0",#set right stau mass
#set bottom trilinear coupling Ab
#                              "pymssm rmss 15 (sqrt(6.) * 1000.0)",
#                              "pymssm rmss 15 $sigma(sqrt(6.) * 1000.0)",
#                              "pymssm rmss 15 2440.0",
#                              "pymssm rmss 15 800.0"

#set top trilinear coupling At
#                              "pymssm rmss 16 (sqrt(6.) * 1000.0)",
#                              "pymssm rmss 16 $sigma(sqrt(6.) * 1000.0)",
                              "pymssm rmss 16 2440.0",
#                              "pymssm rmss 16 400.0",

#set tau trilinear coupling Atau
#                              "pymssm rmss 17 (sqrt(6.) * 1000.0)",
#                              "pymssm rmss 17 $sigma(sqrt(6.) * 1000.0)",
#                              "pymssm rmss 17 2440.0",
#                              "pymssm rmss 17 0.0",

                              "pymssm rmss 19 130", #set pseudoscalar Higgs mass parameter mA ,<===
			      
                              "pymssm imss 4 0",    #IMSS(4)=0 : Higgs sector determined by the approximate formulae of Carena-Quiros-Wagner Nucl. Phys. B461,407 (1996)			  

                              "pysubs msub 186 1",  #activate subprocess 186 : g + g -> Q + Qbar + A0

                              "pysubs msub 187 1",  #activate subprocess 187 : q + qbar -> Q + Qbar + A0			       

                              "pyint2 kfpr 186 2 5", #set flavour for products in subprocess 186 to beauty : g + g -> b + bbar + A0

                              "pyint2 kfpr 187 2 5", #set flavour for products in subprocess 187 to beauty : q + qbar -> b + bbar + A0
			       
#switch off individual decay channels 420 -> 502
                              "pydat3 mdme 420 1 0",
                              "pydat3 mdme 421 1 0",
                              "pydat3 mdme 422 1 0",
                              "pydat3 mdme 423 1 0",
                              "pydat3 mdme 424 1 0",
                              "pydat3 mdme 425 1 0",
                              "pydat3 mdme 426 1 0",
                              "pydat3 mdme 427 1 0",
                              "pydat3 mdme 428 1 0",
                              "pydat3 mdme 429 1 0",
                              "pydat3 mdme 430 1 0",
                              "pydat3 mdme 431 1 0",
                              "pydat3 mdme 432 1 0",
                              "pydat3 mdme 433 1 0",
                              "pydat3 mdme 434 1 0",
                              "pydat3 mdme 435 1 0",
                              "pydat3 mdme 436 1 0",
                              "pydat3 mdme 437 1 0",
                              "pydat3 mdme 438 1 0",
                              "pydat3 mdme 439 1 0",
                              "pydat3 mdme 440 1 0",
                              "pydat3 mdme 441 1 0",
                              "pydat3 mdme 442 1 0",
                              "pydat3 mdme 443 1 0",
                              "pydat3 mdme 444 1 0",
                              "pydat3 mdme 445 1 0",
                              "pydat3 mdme 446 1 0",
                              "pydat3 mdme 447 1 0",
                              "pydat3 mdme 448 1 0",
                              "pydat3 mdme 449 1 0",
                              "pydat3 mdme 450 1 0",
                              "pydat3 mdme 451 1 0",
                              "pydat3 mdme 452 1 0",
                              "pydat3 mdme 453 1 0",
                              "pydat3 mdme 454 1 0",
                              "pydat3 mdme 455 1 0",
                              "pydat3 mdme 456 1 0",
                              "pydat3 mdme 457 1 0",
                              "pydat3 mdme 458 1 0",
                              "pydat3 mdme 459 1 0",
                              "pydat3 mdme 460 1 0",
                              "pydat3 mdme 461 1 0",
                              "pydat3 mdme 462 1 0",
                              "pydat3 mdme 463 1 0",
                              "pydat3 mdme 464 1 0",
                              "pydat3 mdme 465 1 0",
                              "pydat3 mdme 466 1 0",
                              "pydat3 mdme 467 1 0",
                              "pydat3 mdme 468 1 0",
                              "pydat3 mdme 469 1 0",
                              "pydat3 mdme 470 1 0",
                              "pydat3 mdme 471 1 0",
                              "pydat3 mdme 472 1 0",
                              "pydat3 mdme 473 1 0",
                              "pydat3 mdme 474 1 0",
                              "pydat3 mdme 475 1 0",
                              "pydat3 mdme 476 1 0",
                              "pydat3 mdme 477 1 0",
                              "pydat3 mdme 478 1 0",
                              "pydat3 mdme 479 1 0",
                              "pydat3 mdme 480 1 0",
                              "pydat3 mdme 481 1 0",
                              "pydat3 mdme 482 1 0",
                              "pydat3 mdme 483 1 0",
                              "pydat3 mdme 484 1 0",
                              "pydat3 mdme 485 1 0",
                              "pydat3 mdme 486 1 0",
                              "pydat3 mdme 487 1 0",
                              "pydat3 mdme 488 1 0",
                              "pydat3 mdme 489 1 0",
                              "pydat3 mdme 490 1 0",
                              "pydat3 mdme 491 1 0",
                              "pydat3 mdme 492 1 0",
                              "pydat3 mdme 493 1 0",
                              "pydat3 mdme 494 1 0",
                              "pydat3 mdme 495 1 0",
                              "pydat3 mdme 496 1 0",
                              "pydat3 mdme 497 1 0",
                              "pydat3 mdme 498 1 0",
                              "pydat3 mdme 499 1 0",
                              "pydat3 mdme 500 1 0",
                              "pydat3 mdme 501 1 0",
                              "pydat3 mdme 502 1 0",
#switch on decay channel 429: A0 -> mu- + mu+ 
                              "pydat3 mdme 429 1 1",
                              "pydat3 mdcy 15 1 0",
                              "pydat1 parj 90 20000."
                          ]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
topAlg += MultiLeptonFilter()

MultiLeptonFilter = topAlg.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 5000.
MultiLeptonFilter.Etacut = 2.7
MultiLeptonFilter.NLeptons = 2

try:
     StreamEVGEN.RequireAlgs = [ "MultiLeptonFilter" ]
except Exception, e:
     pass

from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.80
#==============================================================
#
# End of job options file
#
###############################################################
