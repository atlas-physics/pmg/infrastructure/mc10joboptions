###############################################################
#
# JOB OPTIONS FILE TO USE PYTHIASGLUON_i
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_Pythia_Common.py" )



#--------------------------------------------------------------
# Algorithms Private Options
#--------------------------------------------------------------
#use external process : qqbar->sgluon sgluon  and gg->sgluon sgluon 
Pythia.PythiaCommand += ["pyinit user pythiasgluon"]

#adding particle to pythia ../../PythiaSgluon_i/share/sgluons.dat
Pythia.addParticle = True

#set the sgluon mass
Pythia.PythiaCommand += ["pydat2 pmas 5100021 1 100"]


#switch off all the sgluon decay channels
pydat3_mdme = "pydat3 mdme "
switch_off=" 1 0"
for IDC in range(5065,5072):
    c = pydat3_mdme + str(IDC)
    c += switch_off
    Pythia.PythiaCommand += [c]
    c = ""
    
# and turn on the good one
Pythia.PythiaCommand += ["pydat3 mdme 5071 1 1"]

# atlas tune
Pythia.useAtlasPythiaRecomm=True


# tau decays and QED FSR treatment
Pythia.PythiaCommand += ["pydat1 parj 90 20000",   # Turn off QED FSR.
                         "pydat3 mdcy 15 1 0"]     # Turn off tau decays.

# Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )
# Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


#--------------------------------------------------------
# evgen config for Evgen_trf.py
#--------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.99




#==============================================================
#
# End of job options file
#
###############################################################
