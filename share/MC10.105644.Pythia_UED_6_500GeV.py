# Universal Extra Dimensions production with Pythia
#  (lightest neutral KK particle "gamma*" made stable)
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

#--------------------------------------------------------------
# File prepared by Zuzana Rurikova Jan 2009
#--------------------------------------------------------------
#init UED parameters
Pythia.PythiaCommand += ["pypued iued 1 1",
                         "pypued iued 2 6",
                         "pypued rued 0 500."]	
#init all UED processes
Pythia.PythiaCommand += ["pysubs msel 0",
			 "pysubs msub 311 1",
			 "pysubs msub 312 1",
			 "pysubs msub 313 1",
			 "pysubs msub 314 1",
			 "pysubs msub 315 1",
			 "pysubs msub 316 1",
			 "pysubs msub 317 1",
			 "pysubs msub 318 1",
			 "pysubs msub 319 1"]	
# switch off decay gamma*--> gamma+ graviton
Pythia.PythiaCommand += ["pydat3 mdcy 473 1 0"]		

Pythia.PythiaCommand += [
                         "pydat1 parj 90 20000.",  ## Turn off FSR
                         "pydat3 mdcy 15 1 0"]     ## Turn off tau decays

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# Photos for QED FSR
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
