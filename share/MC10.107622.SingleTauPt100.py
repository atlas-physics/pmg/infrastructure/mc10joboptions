###############################################################
#
# Job options file for making single taus
# Ian Hinchliffe 
# - modified by Junichi TANAKA (11 March 2008)
#
#==============================================================

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

from ParticleGenerator.ParticleGeneratorConf import ParticleGenerator
topAlg += ParticleGenerator()


#--------------------------------------------------------------
# TauGenerator parameters
#--------------------------------------------------------------

# e- = 11, e+ = -11, mu- = 13, mu+ = -13, pi+ = 211, pi- = -211, pi0 = 111, geantino = 999
# See PDGTABLE or Rev.Part.Properties for the complete list.

# The orders are in the form of an array of strings.  Each string
# must contain exactly one command.  In most cases, if TauGenerator
# can't figure out what a command means, it will terminate during its
# initialization.  Energies are in GeV, distances and time (ct) are in mm.

# If there is more than one definition of 'TauGenerator.orders',
# only the last one will be used.

# For an example of how to use user-defined histograms to generate a
# distribution, see jobOptions_TauGeneratorExampleHistogram.py.

#--ParticleGenerator
ParticleGenerator = topAlg.ParticleGenerator
ParticleGenerator.orders = [
 "PDGcode: sequence  15 -15",
 "pt: constant 100000",
 "eta: flat -2.5 2.5",
 "phi: flat -3.14159 3.14159"
]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


#==============================================================
#
# End of job options file
#
###############################################################
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.SingleEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
