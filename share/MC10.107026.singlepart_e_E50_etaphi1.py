# Single electrons
# J.F. Arguin, Dec. 2005

# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence( "TopAlg" ) 

from ParticleGenerator.ParticleGeneratorConf import ParticleGenerator
topAlg += ParticleGenerator()


ParticleGenerator = topAlg.ParticleGenerator

# For VERBOSE output from ParticleGenerator.
ParticleGenerator.OutputLevel = 1

ParticleGenerator.orders = [ "PDGcode: sequence -11 11",
                             "e: constant 50000",
                             "eta: flat 0.65 0.75",
                             "phi: flat 0.785398 2.356194" ]

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.SingleEvgenConfig import evgenConfig
