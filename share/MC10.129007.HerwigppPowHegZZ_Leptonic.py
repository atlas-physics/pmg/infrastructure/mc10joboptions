## Job options file for Herwig++, NLO WW (leptonic decays) production

## Get a handle on the top level algorithms' sequence
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

try:
  if runArgs.ecmEnergy == 7000.0:
      include ( "MC10JobOptions/MC10_Herwigpp_NLOME_Common.py" )
  elif runArgs.ecmEnergy == 14000.0:
      include ( "MC10JobOptions/MC10_Herwigpp_UE-EE-3_NLOME_Common.py" )
except NameError:
  # needed (dummy) default
  # from Herwigpp_i.Herwigpp_iConf import Herwigpp
  # topAlg += Herwigpp()
  cmds =  ""


## Add to commands
cmds += """
## Set up Powheg truncated shower
set /Herwig/Shower/Evolver:HardEmissionMode POWHEG

## Use 2-loop alpha_s
create Herwig::O2AlphaS /Herwig/AlphaQCD_O2
set /Herwig/Generators/LHCGenerator:StandardModelParameters:QCD/RunningAlphaS /Herwig/AlphaQCD_O2

# matrix element
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/PowhegMEPP2VV
set /Herwig/MatrixElements/SimpleQCD:MatrixElements[0]:Process ZZ

# select leptonic decay modes 
do /Herwig/Particles/Z0:SelectDecayModes Z0->e-,e+; Z0->mu-,mu+; Z0->tau-,tau+;
"""

## Set commands
topAlg.Herwigpp.Commands = cmds.splitlines()

## BR
topAlg.Herwigpp.CrossSectionScaleFactor=0.100788*0.100788

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.HerwigppEvgenConfig import evgenConfig
#evgenConfig.efficiency = 0.9
#evgenConfig.minevents=100

#==============================================================
#
# End of job options file
#
###############################################################
