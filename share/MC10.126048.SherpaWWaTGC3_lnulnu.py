from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence('TopAlg')
from Sherpa_i.Sherpa_iConf import Sherpa_i
sherpa = Sherpa_i()

"""
(run){
  EXCLUSIVE_CLUSTER_MODE=1
  GENERATE_RESULT_DIRECTORY=1
  ACTIVE[25]=0
  ME_SIGNAL_GENERATOR=Amegic
  MASSIVE[5]=1 

  CSS_AS_FS_FAC=0.4
  CSS_AS_IS_FAC=0.4 
}(run)

(model){
  MODEL         = SM+AGC

  ## AGC settings:
  ## http://projects.hepforge.org/sherpa/doc/SHERPA-MC-1.2.3.html#AGC
  ## for all available parameters (by default AGC are disabled)

  KAPPA_Z=1.4
  KAPPA_GAMMA=1.4
  LAMBDA_Z=-0.5
  LAMBDA_GAMMA=-0.5
  G1_Z=1.4

  UNITARIZATION_SCALE = 3000.0
  UNITARIZATION_N = 2
}(model)

(processes){
  Process 93 93 -> 24[a] -24[b] 93{1};
  Order_EW 2;
  Decay  24[a] -> 90 91;
  Decay -24[b] -> 90 91;
  CKKW sqr(20/E_CMS)
  Scales VAR{(PPerp2(p[2]+p[3])+Abs2(p[2]+p[3])+PPerp2(p[4]+p[5])+Abs2(p[4]+p[5]))/2.0}
  End process;
}(processes)
"""

try:
    sherpa.Parameters += [ 'RUNDATA=%s' % runArgs.jobConfig[0] ]
except NameError:
    pass

topAlg += sherpa
from MC10JobOptions.SherpaFFEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 5000
evgenConfig.weighting = 0
