###############################################################
#
# Job options file
# Rodger Mantifel
#
#==============================================================
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 3

#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------

# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 
try:
  if runArgs.ecmEnergy == 7000.0:
    print runArgs 
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common.py" )
except NameError:
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig 

Herwig.HerwigCommand += [ "iproc alpgen",
                          "taudec TAUOLA"
                        ]
# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from MC10JobOptions.AlpgenEvgenConfig import evgenConfig
 
# inputfilebase
evgenConfig.inputfilebase = 'alpgen'
try:
  if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputfilebase = 'group09.phys-gener.alpgen.113299.WmunuNp3_iqopt2_7tev.TXT.v1'    
except NameError:
  pass

#
# 7 TeV - Information on sample 113299
# 7 TeV - Filter efficiency  = 1.0000
# 7 TeV - MLM matching efficiency = 0.199
# 7 TeV - Number of Matrix Elements in input file  = 125629
# 7 TeV - Alpgen cross section = 553.325 pb
# 7 TeV - Herwig cross section = Alpgen cross section * eff(MLM) = 110.112 pb
# 7 TeV - Cross section after filtering = 110.112 pb
# 7 TeV - Lumi/5000 events post-filter = 5000/eff(Filter)/XS(jimmy) = 45.4084 pb-1
#
# 7 TeV - Filter efficiency estimate below reduced by 10% to produce 5555 events on average,
# 7 TeV - of which only 5000 will be used in further processing
evgenConfig.efficiency = 0.9

#==============================================================
#
# End of job options file
#
###############################################################
evgenConfig.minevents=5000
