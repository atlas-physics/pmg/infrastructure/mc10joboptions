###############################################################
#
# Job options file
#
#-----------------------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_PythiaMC09p_Common.py" ) # use CTEQ6l PDFs

#--------------------------------------------------------------
# Algorithms Private Options
#--------------------------------------------------------------

Pythia.PythiaCommand+= ["pyinit user lhef",
                        "pyinit pylistf 1",
                        "pypars mstp 32 4",  # alpha_s @ Minv
                        "pydat1 parj 90 20000.",  ## Turn off FSR
                        "pydat3 mdcy 15 1 0"]     ## Turn off tau decays


# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )
# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.LhefEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'group10.phys-gener.CalcHep.119668.WStar_1200_dijets_7TeV.TXT.v2'
evgenConfig.efficiency = 0.9

#
# End of job options file
#
###############################################################
