from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence('TopAlg')
from Sherpa_i.Sherpa_iConf import Sherpa_i
sherpa = Sherpa_i()

"""
(run){
  EXCLUSIVE_CLUSTER_MODE=1
  GENERATE_RESULT_DIRECTORY=1
  ACTIVE[25]=0
  ME_SIGNAL_GENERATOR=Amegic
}(run)

(model){
  MODEL = SM+AGC
  H4_GAMMA = 0.0005
  UNITARIZATION_SCALE=2000
  UNITARIZATION_N=4
}(model)

(processes){
  Process 93 93 ->  13 -13 22 93{1}
  CKKW sqr(20/E_CMS)
  Order_EW 3  
  End process;
}(processes)
(selector){
  Mass 11 -11 40 7000
  Mass 13 -13 40 7000
  PT 22  40 7000
  PT 11  0 7000
  PT -11 0 7000
  PT 13  0 7000
  PT -13 0 7000
  DeltaR -11 22 0.5  1000
  DeltaR 11 22 0.5  1000
  DeltaR -13 22 0.5  1000
  DeltaR 13 22 0.5  1000
}(selector)
"""

try:
    sherpa.Parameters += [ 'RUNDATA=%s' % runArgs.jobConfig[0] ]
    sherpa.Parameters += [ 'RUNDATFILE:=%s' % runArgs.jobConfig[0] ]
except NameError:
    pass

topAlg += sherpa
from MC10JobOptions.SherpaFFEvgenConfig import evgenConfig
#evgenConfig.inputfilebase = 'group10.phys-gener.sherpa010203.126039.Sherpa_Zmumugamma_h4gamma_plus00005'
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 5000
evgenConfig.weighting = 0

