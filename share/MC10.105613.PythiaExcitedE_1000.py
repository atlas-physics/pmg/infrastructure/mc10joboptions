from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

#-------------------------------------------------------------
# File prepared by Ahmed Abdelalim [ahmed.ali.abdelalim@cern.ch]
# Excited electron production with pythia
#-------------------------------------------------------------

######################################################
#                                                    #
#           Algorithm Private Options                #
#                                                    #
######################################################

#Excited Eletron Mass (in GeV)
M_ExE = 1000.0

#Mass Scale parameter (Lambda, in GeV)
M_Lam = 5000.0

include ( "MC10JobOptions/MC10_Pythia_Common.py" )
Pythia.PythiaCommand +=["pysubs msel 0",
                        "pysubs msub 169 1", # qqbar->ee*
                        
                        "pydat2 pmas 4000011 1 "+str(M_ExE), #e* mass
                        
                        "pytcsm rtcm 41 "+str(M_Lam), # compositness scale

                       # select ExE decays to gamma + electron
                        "pydat3 mdme 4079 1 1",
                        "pydat3 mdme 4080 1 0",
                        "pydat3 mdme 4081 1 0",
                        ]


Pythia.PythiaCommand += ["pydat1 parj 90 20000.",  ## Turn off FSR
                         "pydat3 mdcy 15 1 0"]     ## Turn off tau decays

# Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import PhotonFilter
topAlg += PhotonFilter()

PhotonFilter = topAlg.PhotonFilter
PhotonFilter.Ptcut = 10000.
PhotonFilter.Etacut = 2.7
PhotonFilter.NPhotons = 1

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------

try:
     StreamEVGEN.RequireAlgs +=  [ "PhotonFilter" ]
except Exception, e:
     pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9

evgenConfig.minevents = 5000
#==============================================================
#
# End of job options file
#
###############################################################
