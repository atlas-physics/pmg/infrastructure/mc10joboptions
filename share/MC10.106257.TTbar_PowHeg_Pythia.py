###############################################################
#
# Job options file
# Ulrich Husemann, Christoph Wasicki, April 2010
#
#
# 
# FSR down 
# MC10: use the same events for 105860/1, 106255-8
#
#
#
#==============================================================
#--------------------------------------------------------------
# General Application Configuration options
#--------------------------------------------------------------
import AthenaCommon.AtlasUnixGeneratorJob

from AthenaCommon.AppMgr import theApp
from AthenaCommon.AppMgr import ServiceMgr

# make sure we are loading the ParticleProperty service
from PartPropSvc.PartPropSvcConf import PartPropSvc
ServiceMgr += PartPropSvc()

#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
ServiceMgr.MessageSvc.OutputLevel = INFO


from AthenaCommon.AlgSequence import AlgSequence
topAlg=AlgSequence()


# ... Main generator : Pythia
include( "MC10JobOptions/MC10_PowHegPythia_Common.py")




# ADD IFSR COMMANDS 
Pythia.PythiaCommand += ["pypars parp 61 0.192"]  # Lambda ISR 
Pythia.PythiaCommand += ["pypars parp 62 2.0"]  # ISR IR cut-off ATLAS=1.0
Pythia.PythiaCommand += ["pypars parp 72 0.096"]  # Lambda FSR
Pythia.PythiaCommand += ["pydat1 parj 82 2.0"]  # FSR IR cut-off
Pythia.PythiaCommand += ["pypars mstp 3 1"]  # set Lambda in PARP(61) , ATLAS=3 2: use Lambda of proton pdf

# ... Tauola
Pythia.PythiaCommand += [ "pydat3 mdcy 15 1 0"] # turn off tau decays
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
Pythia.PythiaCommand += ["pydat1 parj 90 20000"] # turn off FSR for Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import TTbarWToLeptonFilter
topAlg += TTbarWToLeptonFilter()

TTbarWToLeptonFilter = topAlg.TTbarWToLeptonFilter
TTbarWToLeptonFilter.Ptcut = 1.

try:
    StreamEVGEN.RequireAlgs = [ "TTbarWToLeptonFilter" ]
except Exception, e:
    pass

from MC10JobOptions.EvgenConfig import evgenConfig, knownGenerators

evgenConfig.generators += [ "Lhef", "Pythia" ]

#dummy needed
evgenConfig.inputfilebase = 'powheg'
try:
    if runArgs.ecmEnergy == 7000.0:
        evgenConfig.inputfilebase = 'group09.phys-gener.powhegp4.106257.ttbar_7TeV.TXT.v1'
    if runArgs.ecmEnergy == 8000.0:
        evgenConfig.inputfilebase = 'group09.phys-gener.powhegp4.106257.ttbar_8TeV.TXT.v1'
    if runArgs.ecmEnergy == 10000.0:
        evgenConfig.inputfilebase = 'group09.phys-gener.powhegp4.106257.ttbar_10TeV.TXT.v1'
except NameError:
    pass

evgenConfig.efficiency = 0.5

#---------------------------------------------------------------
# Ntuple service output
#---------------------------------------------------------------
#
#==============================================================
#
# End of job options file
#
###############################################################


###############################################################
#
# EXAMPLE FOR POWHEG INPUT FILE
#
###############################################################
#! Heavy flavour production parameters
#maxev @NEVENTS@  ! number of events to be generated
#randomseed @SEED@ !!!!!
#seed 0    ! random seed
#seedn1 0       ! seed counter 1
#seedn2 0       ! seed counter 2
#ih1   1        ! hadron 1
#ih2   1        ! hadron 2
#ndns1 131      ! pdf for hadron 1
#ndns2 131      ! pdf for hadron 2
#lhans1 10550   ! 10550 for 6.6 LHAPDF for hadron 1
#lhans2 10550   ! LHAPDF for hadron 2
#ebeam1 3500    ! energy of beam 1
#ebeam2 3500    ! energy of beam 2
#qmass 172.5    ! mass of heavy quark in GeV
#facscfact 1    ! factorization scale factor: mufact=muref*facscfact 
#renscfact 1    ! renormalization scale factor: muren=muref*renscfact 
#underlyingevent 1 ! UH: had to add this...
#bbscalevar 1    ! use variable re. and fct. scales
#
#
#topdecaymode 22222   ! an integer of 5 digits that are either 0, or 2, representing in 
#                     ! the order the maximum number of the following particles(antiparticles)
#                     ! in the final state: e  mu tau up charm
#                     ! For example
#                     ! 22222    All decays (up to 2 units of everything)
#                     ! 20000    both top go into b l nu (with the appropriate signs)
#                     ! 10011    one top goes into electron (or positron), the other into (any) hadrons
#                     ! 00022    Fully hadronic
#                     ! 00002    Fully hadronic with two charms
#                     ! 00011    Fully hadronic with a single charm
#                     ! 00012    Fully hadronic with at least one charm
#
#! Parameters for the generation of spin correlations in t tbar decays
#tdec/wmass 80.403  ! W mass for top decay
#tdec/wwidth 2.141
#tdec/bmass 4.95
#tdec/twidth 1.320  ! top width
#tdec/elbranching 0.108
#tdec/emass 0.00051
#tdec/mumass 0.1057
#tdec/taumass 1.777
#tdec/dmass   0.320
#tdec/umass   0.320
#tdec/smass   0.5
#tdec/cmass   1.55
#tdec/sin2cabibbo 0.051
#
#! Parameters to allow-disallow use of stored data
#use-old-grid -1    ! if 0 use old grid if file pwggrids.dat is present (# 0: regenerate)
#use-old-ubound -1  ! if 0 use norm of upper bounding function stored in pwgubound.dat, if present; # 0: regenerate
#
#ncall1 10000   ! number of calls for initializing the integration grid
#itmx1 5        ! number of iterations for initializing the integration grid
#ncall2 100000  ! number of calls for computing the integral and finding upper bound
#itmx2 5        ! number of iterations for computing the integral and finding upper bound
#foldx   1      ! number of folds on x integration
#foldy   1      ! number of folds on y integration
#foldphi 1      ! number of folds on phi integration
#nubound 100000  ! number of bbarra calls to setup norm of upper bounding function
#iymax 1        ! <= 10, normalization of upper bounding function in iunorm X iunorm square in y, log(m2qq)
#ixmax 1        ! <= 10, normalization of upper bounding function in iunorm X iunorm square in y, log(m2qq)
#xupbound 2      ! increase upper bound for radiation generation
