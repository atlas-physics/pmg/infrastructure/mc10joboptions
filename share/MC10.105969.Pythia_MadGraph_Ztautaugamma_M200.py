###############################################################
#
# Job options file
# prepared by Haiping Peng in Rel. 15.6.1.7 (Feb 2010)
# Acceptance sample
# Z (tau tau)+ Gamma with full interference between ISR and FSR digram with MadGraph/Pythia
# with Z mass >200GeV
#
#==============================================================

# ... Main generator : Pythia
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_PythiaMC09p_Common.py" )
Pythia.PythiaCommand += [ "pyinit user lhef",
                          "pyinit pylisti -1","pyinit pylistf 1","pyinit dumpr 1 2",
                          "pydat1 parj 90 20000", # Turn off FSR.
                          "pydat3 mdcy 15 1 0"   # Turn off tau decays.
]

## ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

## ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


#---------------------------------------------------------------------------
# Filter
#---------------------------------------------------------------------------
#
from GeneratorFilters.GeneratorFiltersConf import PhotonFilter
topAlg += PhotonFilter()
PhotonFilter = topAlg.PhotonFilter
PhotonFilter.Ptcut = 10000.
PhotonFilter.Etacut = 2.7
PhotonFilter.NPhotons = 1

from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
topAlg += LeptonFilter()
LeptonFilter = topAlg.LeptonFilter
LeptonFilter.Etacut = 2.7

try:
    StreamEVGEN.RequireAlgs +=  [ "PhotonFilter" ]
except Exception, e:
    pass
try:
    StreamEVGEN.RequireAlgs += [ "LeptonFilter" ]
except Exception, e:
    pass
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
# Non-filtered cross section in Rel. 15.6.1.7 : 2.40125e-02 pb
#--------------------------------------------------------------
# efficiency01 = 5683/13889*0.95= 4.0917e-01*0.95=3.8871e-01  
# efficiency02 = 5682/13889*0.95= 4.0910e-01*0.95=3.8865e-01
# efficiency03 = 5573/13889*0.95= 4.0125e-01*0.95=3.8119e-01
# efficiency04 = 5572/13889*0.95= 4.0118e-01*0.95=3.8112e-01
# efficiency05 = 5573/13889*0.95= 4.0125e-01*0.95=3.8119e-01
# efficiency06 = 5626/13889*0.95= 4.0507e-01*0.95=3.8482e-01
# efficiency07 = 5655/13889*0.95= 4.0716e-01*0.95=3.8680e-01
# efficiency08 = 5660/13889*0.95= 4.0752e-01*0.95=3.8714e-01
# efficiency09 = 5583/13889*0.95= 4.0197e-01*0.95=3.8187e-01
# efficiency10 = 5602/13889*0.95= 4.0334e-01*0.95=3.8317e-01
# tot eff      56209/138890*0.95= 4.0470e-01*0.95=3.8447e-01
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
#from EvgenJobOptions.MadGraphEvgenConfig import evgenConfig
evgenConfig.generators += ["Lhef", "Pythia"]
evgenConfig.inputfilebase = "group09.phys-gener.MadGraph.105969.Ztautaugamma_M200.TXT.v1"
evgenConfig.efficiency = 0.36
#  ------------- Number of events requested  -------------
evgenConfig.minevents = 5000
#==============================================================
#
# End of job options file
#
###############################################################

