###############################################################
#
# Job options file
#
# Sherpa H->Z*Z->invisible (mH=130GeV) 
#
# Wolfgang Mader, July 19 2008
# (Wolfgang.Mader@CERN.CH)
#
# Responsible person(s)
#   05 Aug, 2008: Andreas Ludwig (A.Ludwig@physik.tu-dresden.de)
#
#==============================================================
#--------------------------------------------------------------
# ATLAS default Application Configuration options
#--------------------------------------------------------------
#
from AthenaCommon.AppMgr import ServiceMgr as svcMgr
#
#load relevant libraries
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
#
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
from Sherpa_i.Sherpa_iConf import ReadSherpa_i
#
sherpa = ReadSherpa_i()
sherpa.Files = [ "sherpa.evts" ]
#
topAlg += sherpa
#
from MC10JobOptions.SherpaEvgenConfig import evgenConfig
 
# inputfilebase
evgenConfig.inputfilebase = 'sherpa010101.109011.SherpaHtoinvisibleMH130'
evgenConfig.efficiency = 0.95
#
#==============================================================
#
# End of job options file
################################################################ 
