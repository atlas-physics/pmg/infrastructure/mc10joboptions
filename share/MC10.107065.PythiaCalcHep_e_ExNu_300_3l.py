#---------------------------------------------------------------
# Job options file
# p p -> nux , e (mass nux = 300 GeV)
# Responsible person: Piyali Banerjee
#----------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

#include ( "MC10JobOptions/MC10_Pythia_Common.py" )
include ( "MC10JobOptions/MC10_PythiaMC09p_Common.py" )# use CTEQ6l PDFs

# Channel switches
Wtojj = "0"
Wtoln = "1"
Ztojj = "0"
Ztoll = "1"
nutoZ = "1"
nutoW = "1"
                                                 
Pythia.PythiaCommand+= ["pyinit user lhef",
                        "pysubs msel 0",
                        "pydat2 pmas 4000012 1 300.0",
                        "pydat2 pmas 4000011 1 300.0",
                        "pypars mstp 32 4",
                        
                        # Z -> to quarks
                        "pydat3 mdme 174 1 "+Ztojj,
                        "pydat3 mdme 175 1 "+Ztojj,
                        "pydat3 mdme 176 1 "+Ztojj,
                        "pydat3 mdme 177 1 "+Ztojj,
                        "pydat3 mdme 178 1 "+Ztojj,
                        "pydat3 mdme 179 1 "+Ztojj,
                        
                        # Z -> ll
                        "pydat3 mdme 182 1 "+Ztoll,
                        "pydat3 mdme 183 1 0",      # to nu_e
                        "pydat3 mdme 184 1 "+Ztoll,
                        "pydat3 mdme 185 1 0",      # to nu_mu
                        
                        # Close Z -> tau channels
                        "pydat3 mdme 186 1 0",
                        "pydat3 mdme 187 1 0",
                        "pydat3 mdme 188 1 0",
                        "pydat3 mdme 189 1 0",
                        
                        # W -> leptons chanels  
                        "pydat3 mdme 206 1 0",
                        "pydat3 mdme 207 1 "+Wtoln,
                        
                        # Close W -> tau channels
                        "pydat3 mdme 208 1 0", # no tau
                        "pydat3 mdme 209 1 0",

                        # W -> quarks channels 
                        "pydat3 mdme 190 1 "+Wtojj,
                        "pydat3 mdme 191 1 "+Wtojj,
                        "pydat3 mdme 192 1 "+Wtojj,
                        "pydat3 mdme 194 1 "+Wtojj,
                        "pydat3 mdme 195 1 "+Wtojj,
                        "pydat3 mdme 196 1 "+Wtojj,
                        "pydat3 mdme 198 1 "+Wtojj,
                        "pydat3 mdme 199 1 "+Wtojj,
                        "pydat3 mdme 200 1 "+Wtojj,
                        
                        "pydat3 mdme 4083 1 "+nutoW,
                        "pydat3 mdme 4082 1 "+nutoZ,
                                                                        
                        "pydat3 mdcy 15 1 0",
                        "pydat1 parj 90 20000",
                        "pyinit pylistf 1",
                        "pyinit dumpr 1 10",
                        "pyinit pylisti 12"
                        ]

# Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.LhefEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
evgenConfig.inputfilebase = "group09.phys-gener.CalcHep.107065.7TeV_e_ExNu_300_3l.TXT.v1"

