###############################################################
#
# Job options file for Starlight generation of
# Gamma + Gamma collisions at 2760 GeV
# continuum -> 2 mu
#
# Andrzej.Olszewski@ifj.edu.pl
#
# Feb 2011
#
#==============================================================

#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# configuring the Athena application for a 'generator' job
import AthenaCommon.AtlasUnixGeneratorJob

# make sure we are loading the ParticleProperty service
from AthenaCommon.AppMgr import ServiceMgr as svcMgr

from AthenaCommon.Configurable import Configurable
svcMgr.MessageSvc.OutputLevel = 3

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

from Starlight_i.Starlight_iConf import Starlight_i
topAlg += Starlight_i()

#--------------------------------------------------------------
# Algorithms Private Options
#--------------------------------------------------------------
# Use fixed seeds for reproducibility
import os
seed1 = int(os.popen2("date +%s")[1].read())
seed1 &= 0xfffffffe

Starlight_i = Algorithm( "Starlight_i" )
Starlight_i.McEventKey = "GEN_EVENT"
# define parameters either via config file
#Starlight_i.ConfigFileName = "slight.in"
# define parameters in python for:
# --------------------------------
# gamma-gamma continuum:
# "productionMode 1", "prodParticleId 13" (muons) or 11 (electrons),
# ------------------------------------------------------------------
# J/Psi in gamma-Pomeron:
# "productionMode 2", "prodParticleId 443013" (muons) or 443011 (electrons),
# --------------------------------------------------------------------------
# Upsilon in gamma-Pomeron:
# "productionMode 2", "prodParticleId 553013" (muons) or 553011 (electrons),
# --------------------------------------------------------------------------
Starlight_i.Initialize = [
    "beam1Z 82", "beam1A 208", #Z,A of projectile
    "beam2Z 82", "beam2A 208", #Z,A of target
    "beamLorentzGamma 1482",   #Gamma of the colliding ions, for sqrt(nn)=2.76 TeV
    "maxW 12", #Max value of w
    "minW 2", #Min value of w
    "nmbWBins 400", #Bins n w
    "maxRapidity 8.", #max y
    "nmbRapidityBins 1000", #Bins n y
    "accCutPt 1", #Cut in pT? 0 = (no, 1 = yes)
    "minPt 0.9", #Minimum pT in GeV
    "maxPt 7.0", #Maximum pT in GeV
    "accCutEta 1", #Cut in pseudorapidity? (0 = no, 1 = yes)
    "minEta -2.7", #Minimum pseudorapidity
    "maxEta 2.7", #Maximum pseudorapidity
    "productionMode 1", #(1=2-phot,2=vmeson(narrow),3=vmeson(wide))
    "nmbEventsTot 1", #Number of events
    "prodParticleId 13", #Channel of interest
    "randomSeed "+str(seed1), #Random number seed
    "outputFormat 2", #Form of the output
    "beamBreakupMode 5", #Controls the nuclear breakup
    "interferenceEnabled 0", #Interference (0 = off, 1 = on)
    "interferenceStrength 1.", #% of intefernce (0.0 - 0.1)
    "coherentProduction 1", #Coherent=1,Incoherent=0
    "incoherentFactor 1.", #percentage of incoherence
    "bford 9.5",
    "maxPtInterference 0.24", #Maximum pt considered, when interference is turned on
    "nmbPtBinsInterference 120" #Number of pt bins when interference is turned on
    ]

#---------------------------------------------------------------
# Pool Persistency
#---------------------------------------------------------------
from AthenaPoolCnvSvc.WriteAthenaPool import AthenaPoolOutputStream

#--- StreamEVGEN ---
StreamEVGEN = AthenaPoolOutputStream( "StreamEVGEN" )

# 2101 == EventInfo
# 133273 == MCTruth (HepMC)
try:
    StreamEVGEN.ItemList  = [ "2101#*","133273#GEN_EVENT" ]
except Exception, e:
    pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.StarlightEvgenConfig import evgenConfig
evgenConfig.weighting=0
