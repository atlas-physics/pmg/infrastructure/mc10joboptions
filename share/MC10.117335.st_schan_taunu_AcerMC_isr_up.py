#______________________________________________________________________________________________________________
# L. Mijovic (liza.mijovic@_nospam_cern.ch) for the top group                                          
# single top t-chan.->taunu jopOptions for AcerMC+Pythia
# written for AcerMC3.7 and Pythia6.423 and MC10 prod. round 
# photon radiation by Photos, Tau decays by Tauola
# _isr_up sample
#______________________________________________________________________________________________________________

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

Pythia.PythiaCommand += [ "pyinit user acermc",
			 "pydat1 parj 90 20000.",
			 "pydat3 mdcy 15 1 0" ]

include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )





Pythia.PythiaCommand +=[ "pypars parp 67 6.0" ] 
Pythia.PythiaCommand +=[ "pypars parp 64 0.25" ]


#______________________________________________________________________________________________________________
from MC10JobOptions.AcerMCEvgenConfig import evgenConfig

evgenConfig.inputfilebase = 'acermc'
try:
  if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.acermc37.117365.stop_sch_taunu_7TeV.TXT.v1'
  if runArgs.ecmEnergy == 8000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.acermc37.117365.stop_sch_taunu_8TeV.TXT.v1'    
  if runArgs.ecmEnergy == 10000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.acermc37.117365.stop_sch_taunu_10TeV.TXT.v1'
  if runArgs.ecmEnergy == 14000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.acermc37.117365.stop_sch_taunu_14TeV.TXT.v1'    
except NameError:
  pass

evgenConfig.efficiency = 0.95
#______________________________________________________________________________________________________________



