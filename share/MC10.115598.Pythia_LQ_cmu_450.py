#
#  Control file to generate LQ production in Pythia, M(LQ) = 400 GeV, decay: LQ -> c mu
#
#  Prepared by V.Savinov ( vps3@pitt.edu, http://www.phyast.pitt.edu/~savinov ), 01/18/07
#
#  Dec.14,2010:  Modified and (re)validated with 15.6.9.8 by D.Fortin
#
# Version Jan 19 2011 By Jahred Adelman changing coupling to 0.01
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )
# 

#
#--------------------------------------------------------------
#
# 
#
#  Full user control
#
Pythia.PythiaCommand += ["pysubs msel 0"]
#
#  LQ production
#
#++
#
#    qg -> lLQ
#
Pythia.PythiaCommand += ["pysubs msub 162 1"]
#
#    gg -> LQLQ
#
Pythia.PythiaCommand += ["pysubs msub 163 1"]
#
#    qqbar -> LQLQ
#
Pythia.PythiaCommand += ["pysubs msub 164 1"]
#
#  LQ mass
#
Pythia.PythiaCommand += ["pydat2 pmas 42 1 450."]
#
#  To avoid problems in MC generation
#
Pythia.PythiaCommand += ["pysubs ckin 41 350.0"]
Pythia.PythiaCommand += ["pysubs ckin 42 550.0"]
Pythia.PythiaCommand += ["pysubs ckin 43 350.0"]
Pythia.PythiaCommand += ["pysubs ckin 44 550.0"]
#
#  Branching fraction for the decay described below
#
Pythia.PythiaCommand += ["pydat3 brat 539 1."]
#
#  This is relevant to both the decay AND production mechanism for single LQ
#
Pythia.PythiaCommand += ["pydat3 kfdp 539 1 4"]
Pythia.PythiaCommand += ["pydat3 kfdp 539 2 13"]
#
#  Coupling: lambda=sqrt(4pi*alpha_em)
#
Pythia.PythiaCommand += ["pydat1 paru 151 0.01"]
# 
#-------------------------------------------------------------
#
#  In order to prevent double counting in Pythia when PHOTOS is used
#
Pythia.PythiaCommand += ["pydat1 parj 90 20000"]
#
#  Tell Pythia NOT to decay taus
#
Pythia.PythiaCommand += ["pydat3 mdcy 15 1 0"]
#
#-------------------------------------------------------------
#
#  Print the event listing for events x though y: 
#
Pythia.PythiaCommand += ["pyinit dumpr 1 20"]
#
#-------------------------------------------------------------
#
# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )
#
# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )
#
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
#--------
