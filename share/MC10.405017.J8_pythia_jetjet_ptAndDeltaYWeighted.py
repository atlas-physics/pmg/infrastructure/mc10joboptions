###############################################################
#
# Job options file
#
# Pythia J8 event generation with eta interval weighting.
# cross section as default pythia sample = 6.2099*10^-9  nb
#
# Prepared by Graham Jones, Graham.Jones [at] cern.ch 
#==============================================================

# dijet production with pythia
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

Pythia.PythiaCommand += ["pysubs msel 0",
                         "pysubs ckin 3 2240.",
                         "pysubs msub 11 1",
                         "pysubs msub 12 1",
                         "pysubs msub 13 1",
                         "pysubs msub 68 1",
                         "pysubs msub 28 1",
                         "pysubs msub 53 1"]

#--------------------------------------------------------------
# Truth Jets
#--------------------------------------------------------------

# Make truth jets for JetGapFilter:
from AthenaCommon.AppMgr import ServiceMgr as svcMgr
include( "AthenaCommon/Atlas_Gen.UnixStandardJob.py" )
from JetRec.JetGetters import *
Kt6Alg = make_StandardJetGetter('AntiKt',0.6,'Truth').jetAlgorithmHandle()
Kt6Alg.AlgTools['JetFinalPtCut'].MinimumSignal = 7.0*GeV

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

## Truth filter
# Add the filter to the correct sequence!
from GeneratorFilters.GeneratorFiltersConf import JetForwardFilter
forwardFilter=JetForwardFilter()
topAlg += forwardFilter

# General properties
forwardFilter.JetContainer = "AntiKt6TruthJets"

# Kinematic cuts
forwardFilter.MinDeltaY = 0.0
forwardFilter.MaxDeltaY = 100.0

forwardFilter.MinPt1 = 12.0 #GeV
forwardFilter.MinPt2 = 12.0
forwardFilter.MaxPt1 = 70000.0
forwardFilter.MaxPt1 = 70000.0

#Gaussian mixture model describing pdf of unweighted sample
forwardFilter.muXs = [ 2478.33870514, 2389.56183838, 2052.39039624, 2326.13645941, 2279.931211  ]
forwardFilter.muYs = [ 0.217756458021, 0.331330253334, 0.453931552879, 0.36742509253, 0.426711700698  ]
forwardFilter.sigmaXs = [ 120.060608695, 77.744426058, 306.070849702, 51.4593488424, 27.1586848492  ]
forwardFilter.sigmaYs = [ 0.157308658204, 0.23420208414, 0.34710043211, 0.257618069572, 0.292138205618  ]
forwardFilter.rhos = [ 0.133617709583, 0.209578195979, -0.211699377161, 0.15898906762, -0.00215961927239  ]
forwardFilter.weights = [ 0.0938273971306, 0.234507113415, 0.0123541886483, 0.290800214915, 0.368511085891  ]

#Target probability (how much weighting required roughly)
#Lower = more suppression
import math
forwardFilter.SuppressionFactor = 5.0*math.pow(10.0, -8.0)

#--------------------------------------------------------------
# Apply filtering
#--------------------------------------------------------------

try:
  StreamEVGEN.RequireAlgs += [ "JetForwardFilter" ]
except Exception, e:
     pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
# N_gen = 258704933; N_pass = 100000 -> eff = 3.865*10^-4
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.0003865
evgenConfig.minevents=500
evgenConfig.weighting=0

