###############################################################
#
# Job options file for POWHEG with Pythia
# Graham Jones Feb. 2011
#
#==============================================================

from AthenaCommon.AlgSequence import AlgSequence
topAlg=AlgSequence("TopAlg")

# ... Main generator : Pythia
include( "MC10JobOptions/MC10_PowHegPythia_Common.py")
Pythia.PythiaCommand += [ "pyinit pylisti -1",
                          "pyinit pylistf 1",
                          "pyinit dumpr 1 2",
                          "pydat3 mdcy 15 1 0",   # no tau decays
                          "pydat1 parj 90 20000", # no photon emmission from leptons
                          "pypars mstp 86 1"      # restrict MPI
                          ]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


#--------------------------------------------------------------
# Configuration for Evgen Job Transforms
#--------------------------------------------------------------

from MC10JobOptions.EvgenConfig import evgenConfig
evgenConfig.generators += [ "Lhef", "Pythia"]

#dummy needed
evgenConfig.inputfilebase = 'PowHeg'
try:
  if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.PowHeg7.113888.Dijet250.TXT.v1'
  if runArgs.ecmEnergy == 8000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.PowHeg8.113888.Dijet250.TXT.v1'
except NameError:
  pass

evgenConfig.efficiency = 0.9
evgenConfig.minevents = 5000

# 7TeV inputs produced with POWHEG-BOX r302
#
# URL: svn://powhegbox.mib.infn.it/trunk/POWHEG-BOX
# Repository Root: svn://powhegbox.mib.infn.it
# Repository UUID: 9e129b38-c2c0-4eec-9301-dc54fda404de
# Revision: 302
# Node Kind: directory
# Schedule: normal
# Last Changed Author: nason
# Last Changed Rev: 302
# Last Changed Date: 2010-12-21 14:36:25 +0000 (Tue, 21 Dec 2010)

#==============================================================
#
# End of job options file
#
###############################################################


