###############################################################
#
# Job options file
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_PythiaMC09p_Common.py" )

Pythia.PythiaCommand+= ["pyinit user comphep",
                        "pyinit pylistf 1",
                        "pyinit pylisti 12",
                        #                            "pyinit pylisti 1",
                        "pyinit dumpr 1 10"
                        "pystat 1 3 4 5",
                        "pydat2 vckm 3 4 0.1",
                        "pydat2 vckm 4 3 0.1",
                        #                            "pydat3 mdme 71 1 1",
                        "pydat3 mdme 190  1 0",
                        "pydat3 mdme 191  1 0",
                        "pydat3 mdme 192  1 0",
                        "pydat3 mdme 194  1 0",
                        "pydat3 mdme 195  1 0",
                        "pydat3 mdme 196  1 0",
                        "pydat3 mdme 198  1 0"
                        "pydat3 mdme 199  1 0",
                        "pydat3 mdme 200  1 0",
                        "pydat3 mdme 208  1 0"]

from TruthExamples.TruthExamplesConf import DumpMC
#job += DumpMC()

#--------------------------------------------------------------
# Tuning for Pythia 6.4
#-------------------------------------------------------------

# ... TAUOLA and Photos
Pythia.PythiaCommand += ["pydat1 parj 90 20000",
                          "pydat3 mdcy 15 1 0" ]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )
# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.CompHepEvgenConfig import evgenConfig
from MC10JobOptions.CompHepEvgenConfig import evgenConfig 
evgenConfig.inputfilebase = 'group09.phys-gener.CompHep.105526.t4_bbar_j3_250_7TeV.TXT.v1'
evgenConfig.efficiency = 0.9
#==============================================================
#
# End of job options file
#
###############################################################
