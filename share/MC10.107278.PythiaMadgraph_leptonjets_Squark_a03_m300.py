###############################################################
#
# Job options file
# Developed by Pavel Staroba from CSC.006904.PythiaZeeJet.py
# in Rel. 14.2.0.1 (July 2008)
#
#==============================================================
# ... Main generator : Pythia
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_PythiaMC09p_Common.py" )

Pythia.PythiaCommand += [ "pyinit user madgraph",
                          "pydat1 parj 90 20000.",
                          "pydat3 mdcy 15 1 0" ]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
# efficiency = 0.9 - no filtering
from MC10JobOptions.MadGraphEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.90
#evgenConfig.minevents  = 1  # Don't crash when testing
evgenConfig.maxeventsfactor = 100  # dito

evgenConfig.inputfilebase = "group09.phys-gener.MadGraph.107278.leptonjets_Squark_a03_m300.TXT.V1"
