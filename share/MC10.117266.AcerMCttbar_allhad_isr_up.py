#______________________________________________________________________________________________________________
# author: L. Mijovic (liza.mijovic@_nospam_cern.ch) for the top group                                          
# ISR     systematics sample: more hard ISR activity, external LO process + PYTHIA                             
# all-had. chan.
# PARP(67) and PARP(64) are varied                                                                             
# AMBT1 default values: PARP(67)=4., PARP(64)=1.                                                               
# PARP (67) :  controls suppression of ISR branchings above the coherence scale                                
#              PARP(67) was introduced in 6.4.19 for the new shower, see Pythia update notes for more info     
# PARP (64) :  multiplies ISR alpha_strong evolution scale, the effect is \propto 1/(lambda_ISR^2)             
#              see Pythia Manual for more info                                                                 
#______________________________________________________________________________________________________________
#
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

Pythia.PythiaCommand += [ "pyinit user acermc",
			 "pydat1 parj 90 20000.",
			 "pydat3 mdcy 15 1 0" ]

Pythia.PythiaCommand +=[ "pypars parp 67 6.0" ]   
Pythia.PythiaCommand +=[ "pypars parp 64 0.25" ]  

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.AcerMCEvgenConfig import evgenConfig


#dummy needed
evgenConfig.inputfilebase = 'acermc'
try:
  if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.acermc37.117265.tt_allhad_7TeV.TXT.v1'
  if runArgs.ecmEnergy == 8000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.acermc37.117265.tt_allhad_8TeV.TXT.v1'    
  if runArgs.ecmEnergy == 10000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.acermc37.117265.tt_allhad_10TeV.TXT.v1'
  if runArgs.ecmEnergy == 14000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.acermc37.117265.tt_allhad_14TeV.TXT.v1'    
except NameError:
  pass

evgenConfig.efficiency = 0.95

#==============================================================
#
# End of job options file
#
###############################################################

