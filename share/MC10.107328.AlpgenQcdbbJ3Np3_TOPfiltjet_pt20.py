###############################################################
#
# Job options file
# Wouter Verkerke
#
#==============================================================
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 2
MessageSvc.infoLimit = 1000

#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------

# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 
try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common.py" )
  if runArgs.ecmEnergy == 14000.0:
    include ( "MC10JobOptions/MC10_AlpgenJimmy_Common_14TeV.py" )
except NameError:
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig 

Herwig.HerwigCommand += [ "iproc alpgen",
                          "taudec TAUOLA",
                        ]
# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

# TruthJet filter
try:
     from JetRec.JetGetters import * 
     c4=make_StandardJetGetter('AntiKt',0.4,'Truth') 
     c4alg = c4.jetAlgorithmHandle() 
     c4alg.OutputLevel = 3
except Exception, e:
     pass
 
from GeneratorFilters.GeneratorFiltersConf import TruthJetFilter
topAlg += TruthJetFilter("TruthJetFilter1")

TruthJetFilter1 = topAlg.TruthJetFilter1
TruthJetFilter1.Njet=4
TruthJetFilter1.NjetMinPt=17.*GeV
TruthJetFilter1.NjetMaxEta=5.0
TruthJetFilter1.jet_pt1=17.*GeV
TruthJetFilter1.TruthJetContainer="AntiKt4TruthJets"

from GeneratorFilters.GeneratorFiltersConf import TruthJetFilter
topAlg += TruthJetFilter("TruthJetFilter2")

TruthJetFilter2 = topAlg.TruthJetFilter2
TruthJetFilter2.Njet=3
TruthJetFilter2.NjetMinPt=25.*GeV
TruthJetFilter2.NjetMaxEta=5.0
TruthJetFilter2.jet_pt1=25.*GeV
TruthJetFilter2.TruthJetContainer="AntiKt4TruthJets"

try:
     StreamEVGEN.RequireAlgs = [ "TruthJetFilter1", "TruthJetFilter2" ]
except Exception, e:
     pass


from MC10JobOptions.AlpgenEvgenConfig import evgenConfig
 
# inputfilebase
evgenConfig.inputfilebase = 'alpgen'
try:
  if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputfilebase = 'group09.phys-gener.alpgen.107328.QcdbbJ3Np3_TOPjetfilt_pt20.TXT.v1'    
  if runArgs.ecmEnergy == 10000.0:
    evgenConfig.inputfilebase = 'alpgen.107328.QcdbbJ3Np3.TOPjetfilt.pt20'    
except NameError:
  pass

# 7 TeV - Information on sample 107328
# 7 TeV - Filter efficiency  = 0.8358
# 7 TeV - MLM matching efficiency = 0.18
# 7 TeV - Number of Matrix Elements in input file  = 5000
# 7 TeV - Alpgen cross section = 16615.6 pb
# 7 TeV - Herwig cross section = Alpgen cross section * eff(MLM) = 2971.0 pb
# 7 TeV - Cross section after filtering = 2483.1 pb
# 7 TeV - Lumi/500 events post-filter = 500/eff(Filter)/XS(jimmy) = 0.20 pb-1
#
# 7 TeV - Filter efficiency estimate below reduced by 20% to produce 625 events on average,
# 7 TeV - of which only 500 will be used in further processing
evgenConfig.efficiency = 0.66862
#==============================================================
#
# End of job options file
#
###############################################################
