###############################################################
#
# Job options file
# (based on original from Wouter Verkerke)
#
#==============================================================
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 2

#MessageSvc.defaultLimit = 10**5
theApp.EvtMax = -1

#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------

#-- Dll's and Algorithms
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

try:
   if runArgs.ecmEnergy == 7000.0:
      include ( "MC10JobOptions/MC10_McAtNloJimmy_Common_7TeV.py" )
   if runArgs.ecmEnergy == 8000.0:
      include ( "MC10JobOptions/MC10_McAtNloJimmy_Common_7TeV.py" )
   if runArgs.ecmEnergy == 10000.0:
      include ( "MC10JobOptions/MC10_McAtNloJimmy_Common.py" )
   if runArgs.ecmEnergy == 14000.0:
      include ( "MC10JobOptions/MC10_McAtNloJimmy_Common_14TeV.py" )
except NameError:
   # needed (dummy) default
   from Herwig_i.Herwig_iConf import Herwig
   topAlg += Herwig()
   Herwig = topAlg.Herwig

# ... Main generator : Herwig
Herwig.HerwigCommand += [ "taudec TAUOLA","modbos 1 2", "modbos 2 2"]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


from MC10JobOptions.McAtNloEvgenConfig import evgenConfig

# dummy needed
evgenConfig.inputfilebase = 'mcatnlo'
try:
   if runArgs.ecmEnergy == 7000.0:
      evgenConfig.inputfilebase = 'group10.phys-gener.mcatnlo40.126077.12870_m0p3_1p0_0p3_3000_3_1.TXT.v2'
except NameError:
  pass

evgenConfig.efficiency = 0.9

#==============================================================


