###############################################################
#
# Job options file
#
#==============================================================
# EW
# gg --> t tbar b bbar --> lnb qqb b bbar
#

#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------

# ... Main generator : Pythia
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

Pythia.PythiaCommand = [ "pyinit user acermc",
                         "acermc acset12 4",
                         "pydat1 parj 90 20000.",
                         "pydat3 mdcy 15 1 0",
                         "pyinit pylisti 12",
                         "pyinit pylistf 1",
                         "pystat 1 3 4 5",
                         "pyinit dumpr 1 5"]


# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

#from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
#topAlg += LeptonFilter()
#
#LeptonFilter = topAlg.LeptonFilter
#LeptonFilter.Ptcut = 10000.
#LeptonFilter.Etacut = 2.7


#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------

#try:
#     StreamEVGEN.RequireAlgs +=  [ "LeptonFilter" ]
#except Exception, e:
#     pass


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.AcerMCEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'group09.phys-gener.acermc37.116100.gg_ttbbEW.TXT.v1'
evgenConfig.minevents=5000
evgenConfig.efficiency = 0.9

#==============================================================
#
# End of job options file
#
###############################################################
