# Pythia W' --> t b --> j j nu b
#
# James Ferrando
# May 4, 2011
#
# sigma =  0.1797 pb (m = 1500 GeV)

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

#--------------------------------------------------------------
# Algorithms Private Options
#--------------------------------------------------------------

# Wprime resonance mass (in GeV)
WprimeMass = 1500

# Wprime specific parameters for pythia :
Pythia.PythiaCommand += [
       "pysubs msel 0",
       "pydat1 parj 90 20000",
       "pydat3 mdcy 15 1 0",
       "pysubs msub 142 1",   # SSM W'
       # W' decays to quarks
       "pydat3 mdme 311 1 0",    # u dbar
       "pydat3 mdme 312 1 0",    # c dbar
       "pydat3 mdme 313 1 0",    # t dbar
       "pydat3 mdme 315 1 0",    # u sbar
       "pydat3 mdme 316 1 0",    # c sbar
       "pydat3 mdme 317 1 0",    # t sbar
       "pydat3 mdme 319 1 0",    # u bbar
       "pydat3 mdme 320 1 0",    # c bbar
       "pydat3 mdme 321 1 1",    # t bbar
       # W' decays to leptons 
       "pydat3 mdme 327 1 0",    # e nu
       "pydat3 mdme 328 1 0",    # mu nu
       "pydat3 mdme 329 1 0",    # tau nu
       # W decays
       "pydat3 mdme 190 1 1",    # u dbar
       "pydat3 mdme 191 1 1",    # c dbar
       "pydat3 mdme 192 1 1",    # t dbar
       "pydat3 mdme 194 1 1",    # u sbar
       "pydat3 mdme 195 1 1",    # c sbar
       "pydat3 mdme 196 1 1",    # t sbar
       "pydat3 mdme 198 1 1",    # u bbar
       "pydat3 mdme 199 1 1",    # c bbar
       "pydat3 mdme 200 1 1",    # t bbar
       "pydat3 mdme 206 1 0",    # e nu
       "pydat3 mdme 207 1 0",    # mu nu
       "pydat3 mdme 208 1 0",    # tau nu
       #
       "pydat1 mstu 1 0",
       "pydat1 mstu 2 0",
       "pydat2 pmas 34 1 "+str(WprimeMass),
       "pyinit pylisti 12"
       ]
# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )
# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#==============================================================
#
# End of job options file
#
###############################################################
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9


