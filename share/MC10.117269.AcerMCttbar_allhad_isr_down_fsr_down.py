#______________________________________________________________________________________________________________#
# top group PS systematics sample: min. ISR and min. FSR activity
# all-had chan.
# contact: liza.mijovic_at_nospam_cern.ch
#______________________________________________________________________________________________________________#


from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

Pythia.PythiaCommand +=[ "pyinit user acermc",        
                         "pydat1 parj 90 20000.", 
                         "pydat3 mdcy 15 1 0"    
                       ]

#need to use MSTP(3)=1 to set lam_FSR manually, but this also sets some other parameters to
#non-default values; these need to be set back to defaults.
Pythia.PythiaCommand += [ "pypars mstp 3 1",      #set mstp 3 1 (setting Lambda in alpha_s)
                          "pypars parp 1 0.192",  #reset the defaults after mstp 3 1
                          "pydat1 paru 112 0.192",#reset defaults after mstp 3 1
                          "pydat1 mstu 112 4",    #reset defaults after mstp 3 1
                          "pypars parp 61 0.192"] #reset defaults after mstp 3 1

#--------------------------------------------------------
# decrease ISR activity
#--------------------------------------------------------
# ISR starting scale:
Pythia.PythiaCommand +=[ "pypars parp 67 0.5" ]
# ~ renorm. scale used for ISR
Pythia.PythiaCommand +=[ "pypars parp 64 4.0" ]
#--------------------------------------------------------
# decrease FSR activity:
#--------------------------------------------------------
# Labmda value in running alpha_s (ATLAS def 0.192)
Pythia.PythiaCommand +=[ "pypars parp 72 0.096" ]
# FSR cutoff
Pythia.PythiaCommand +=[ "pydat1 parj 82 2.0" ]
#--------------------------------------------------------

include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

from MC10JobOptions.AcerMCEvgenConfig import evgenConfig

#dummy needed
evgenConfig.inputfilebase = 'acermc'
try:
  if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.acermc37.117265.tt_allhad_7TeV.TXT.v1'
  if runArgs.ecmEnergy == 8000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.acermc37.117265.tt_allhad_8TeV.TXT.v1'    
  if runArgs.ecmEnergy == 10000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.acermc37.117265.tt_allhad_10TeV.TXT.v1'
  if runArgs.ecmEnergy == 14000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.acermc37.117265.tt_allhad_14TeV.TXT.v1'    
except NameError:
  pass

evgenConfig.efficiency = 0.95



