#
#  Control file to generate
#
#  W_R (800 GeV) -> e nu_Re (500 GeV)
#     followed by
#  e_Re -> e mu nu_Rmu (50 GeV)
#     and
#  nu_Rmu -> mu jet jet
#     resulting in final state (no mixing) 
#  e e mu mu jet jet 
#
#  nu_Rtau is assumed to have mass of 1000 GeV
#
#  April 08, 2009:  prepared with 14.2.25.9 and 15.0.0.2 by Vladimir Savinov
#
#  May 18, 2009: overwriting typos in PYTHIA decay tables for heavy neutrinos
#
#--------------------------------------------------------------
#
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )
#
#--------------------------------------------------------------
#
#  Full user control
#
Pythia.PythiaCommand += ["pysubs msel 0"]
#
#  ffbar -> W_R+
#
Pythia.PythiaCommand += ["pysubs msub 354 1"]
#
#  Masses of W_R, Z_R and Majorana neutrinos
#    
Pythia.PythiaCommand += ["pydat2 pmas 9900024 1 800.0"]
Pythia.PythiaCommand += ["pydat2 pmas 9900023 1 1000.0"]
#
Pythia.PythiaCommand += ["pydat2 pmas 9900012 1 500.0"]
Pythia.PythiaCommand += ["pydat2 pmas 9900014 1 50.0"]
Pythia.PythiaCommand += ["pydat2 pmas 9900016 1 1000.0"]
# 
#-------------------------------------------------------------
#
#  Disable certain decays of W_R+ and W_R- and enable only a few or one
#
Pythia.PythiaCommand += ["pydat3 mdme 4185 1 0"]  
Pythia.PythiaCommand += ["pydat3 mdme 4186 1 0"]   
Pythia.PythiaCommand += ["pydat3 mdme 4187 1 0"]   
Pythia.PythiaCommand += ["pydat3 mdme 4188 1 0"]   
Pythia.PythiaCommand += ["pydat3 mdme 4189 1 0"]   
Pythia.PythiaCommand += ["pydat3 mdme 4190 1 0"]   
Pythia.PythiaCommand += ["pydat3 mdme 4191 1 0"]   
Pythia.PythiaCommand += ["pydat3 mdme 4192 1 0"]   
Pythia.PythiaCommand += ["pydat3 mdme 4193 1 0"]   
#
#  e+ nu_Re
#
Pythia.PythiaCommand += ["pydat3 mdme 4194 1 1"]
#
# fixing bugs in hardwired decay tables in Pythia:
#
Pythia.PythiaCommand += ["pydat3 kfdp 4122 1 11"]
Pythia.PythiaCommand += ["pydat3 kfdp 4122 2 -13"]
Pythia.PythiaCommand += ["pydat3 kfdp 4122 3 9900014"]
#
Pythia.PythiaCommand += ["pydat3 kfdp 4123 1 -11"]
Pythia.PythiaCommand += ["pydat3 kfdp 4123 2 13"]
Pythia.PythiaCommand += ["pydat3 kfdp 4123 3 9900014"]
#
Pythia.PythiaCommand += ["pydat3 kfdp 4124 1 11"]
Pythia.PythiaCommand += ["pydat3 kfdp 4124 2 -15"]
Pythia.PythiaCommand += ["pydat3 kfdp 4124 3 9900016"]
#
Pythia.PythiaCommand += ["pydat3 kfdp 4125 1 -11"]
Pythia.PythiaCommand += ["pydat3 kfdp 4125 2 15"]
Pythia.PythiaCommand += ["pydat3 kfdp 4125 3 9900016"]
#
#  mu+  nu_Rmu
#
Pythia.PythiaCommand += ["pydat3 mdme 4195 1 0"]
#
# fixing bugs in hardwired decay tables in Pythia:
#
Pythia.PythiaCommand += ["pydat3 kfdp 4144 1 13"]
Pythia.PythiaCommand += ["pydat3 kfdp 4144 2 -11"]
Pythia.PythiaCommand += ["pydat3 kfdp 4144 3 9900012"]
#
Pythia.PythiaCommand += ["pydat3 kfdp 4145 1 -13"]
Pythia.PythiaCommand += ["pydat3 kfdp 4145 2 11"]
Pythia.PythiaCommand += ["pydat3 kfdp 4145 3 9900012"]
#
Pythia.PythiaCommand += ["pydat3 kfdp 4146 1 13"]
Pythia.PythiaCommand += ["pydat3 kfdp 4146 2 -15"]
Pythia.PythiaCommand += ["pydat3 kfdp 4146 3 9900016"]
#
Pythia.PythiaCommand += ["pydat3 kfdp 4147 1 -13"]
Pythia.PythiaCommand += ["pydat3 kfdp 4147 2 15"]
Pythia.PythiaCommand += ["pydat3 kfdp 4147 3 9900016"]
#
#  tau+ nu_Rtau
#
Pythia.PythiaCommand += ["pydat3 mdme 4196 1 0"]
#
# fixing bugs in hardwired decay tables in Pythia:
#
Pythia.PythiaCommand += ["pydat3 kfdp 4166 1 15"]
Pythia.PythiaCommand += ["pydat3 kfdp 4166 2 -11"]
Pythia.PythiaCommand += ["pydat3 kfdp 4166 3 9900012"]
#
Pythia.PythiaCommand += ["pydat3 kfdp 4167 1 -15"]
Pythia.PythiaCommand += ["pydat3 kfdp 4167 2 11"]
Pythia.PythiaCommand += ["pydat3 kfdp 4167 3 9900012"]
#
Pythia.PythiaCommand += ["pydat3 kfdp 4168 1 15"]
Pythia.PythiaCommand += ["pydat3 kfdp 4168 2 -13"]
Pythia.PythiaCommand += ["pydat3 kfdp 4168 3 9900014"]
#
Pythia.PythiaCommand += ["pydat3 kfdp 4169 1 -15"]
Pythia.PythiaCommand += ["pydat3 kfdp 4169 2 13"]
Pythia.PythiaCommand += ["pydat3 kfdp 4169 3 9900014"]
#
#-------------------------------------------------------------
#
#  Disable certain decays of nu_Re
#
Pythia.PythiaCommand += ["pydat3 mdme 4104 1 0"]  
Pythia.PythiaCommand += ["pydat3 mdme 4105 1 0"]  
Pythia.PythiaCommand += ["pydat3 mdme 4106 1 0"]  
Pythia.PythiaCommand += ["pydat3 mdme 4107 1 0"]  
Pythia.PythiaCommand += ["pydat3 mdme 4108 1 0"]  
Pythia.PythiaCommand += ["pydat3 mdme 4109 1 0"]  
Pythia.PythiaCommand += ["pydat3 mdme 4110 1 0"]  
Pythia.PythiaCommand += ["pydat3 mdme 4111 1 0"]  
Pythia.PythiaCommand += ["pydat3 mdme 4112 1 0"]  
Pythia.PythiaCommand += ["pydat3 mdme 4113 1 0"]  
Pythia.PythiaCommand += ["pydat3 mdme 4114 1 0"]  
Pythia.PythiaCommand += ["pydat3 mdme 4115 1 0"]  
Pythia.PythiaCommand += ["pydat3 mdme 4116 1 0"]  
Pythia.PythiaCommand += ["pydat3 mdme 4117 1 0"]  
Pythia.PythiaCommand += ["pydat3 mdme 4118 1 0"]  
Pythia.PythiaCommand += ["pydat3 mdme 4119 1 0"]  
Pythia.PythiaCommand += ["pydat3 mdme 4120 1 0"]  
Pythia.PythiaCommand += ["pydat3 mdme 4121 1 0"]  
#
#-------------------------------------------------------------
#
#  In order to prevent double counting in Pythia when PHOTOS is used
#
Pythia.PythiaCommand += ["pydat1 parj 90 20000"]
#
#  Tell Pythia NOT to decay taus
#
Pythia.PythiaCommand += ["pydat3 mdcy 15 1 0"]
#
#  Print the event listing for events x though y: 
#
Pythia.PythiaCommand += ["pyinit dumpr 1 20"]
#
#-------------------------------------------------------------
#
# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )
#
#-------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
