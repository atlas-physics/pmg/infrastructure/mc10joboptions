# ND min bias sample
# ( >= 120 stable, charged particles with pT>100MeV, |eta|<2.5)
#
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 


include ( "MC10JobOptions/MC10_Pythia_Common.py" )

Pythia.PythiaCommand += [  "pysubs msel 1" ]


#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import ChargedTracksFilter
topAlg += ChargedTracksFilter()

ChargedTracksFilter = topAlg.ChargedTracksFilter
ChargedTracksFilter.Ptcut = 100.
ChargedTracksFilter.Etacut = 2.5
# NB filter cuts on nChargedTrack > nTrackCut
# (modified in MC10JobOptions-00-00-40; NTracks >=120)
# ChargedTracksFilter.NTracks = 120
ChargedTracksFilter.NTracks = 119

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------
try:
     StreamEVGEN.RequireAlgs +=  [ "ChargedTracksFilter" ]
except Exception, e:
     pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
# evgenConfig.efficiency = 0.9
evgenConfig.minevents=1000

#==============================================================
#
# End of job options file
#
###############################################################

