###############################################################
#
# Start of job options file
#
###############################################################


import AthenaCommon.AtlasUnixGeneratorJob

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_Pythia_Common.py" )

Pythia.PythiaCommand += [ "pysubs msel 0" ]

# 391 and 392 are the graviton subprocesses qqbar/gg
Pythia.PythiaCommand += [ "pysubs msub 391 1" ]
Pythia.PythiaCommand += [ "pysubs msub 392 1" ]
Pythia.PythiaCommand += [ "pydat3 mdcy 347 1 1" ]


# I  5000039  Graviton*    (m =  500.000 GeV)  -->
Pythia.PythiaCommand += [ "pydat2 pmas 5000039 1 500.0" ]

Pythia.PythiaCommand += [ "pydat3 mdme 4084 1 0",   # dd~
                          "pydat3 mdme 4085 1 0",   # uu~
                          "pydat3 mdme 4086 1 0",   # cc~
                          "pydat3 mdme 4087 1 0",   # ss~
                          "pydat3 mdme 4088 1 0",   # bb~
                          "pydat3 mdme 4089 1 0",   # tt~
                          "pydat3 mdme 4090 1 0",   # b'b'~
                          "pydat3 mdme 4091 1 0",   # t't'~
                          "pydat3 mdme 4092 1 0",   # e-e+
                          "pydat3 mdme 4093 1 0",   # nu_e,nu_ebar
                          "pydat3 mdme 4094 1 0",   # mu-mu+
                          "pydat3 mdme 4095 1 0",   # nu_mu,nu_mubar
                          "pydat3 mdme 4096 1 0",   # tau-tau+
                          "pydat3 mdme 4097 1 0",   # nu_tau,nu_taubar
                          "pydat3 mdme 4098 1 0",   # tau'-tau'+
                          "pydat3 mdme 4099 1 0",   # nu'_tau,nu'_taubar
                          "pydat3 mdme 4100 1 0",   # gg
                          "pydat3 mdme 4101 1 0",   # gammagamma
                          "pydat3 mdme 4102 1 0",   # ZZ
                          "pydat3 mdme 4103 1 1" ]   # W+W-
# PARP(50) : (D=0.054) dimensionless coupling, which enters quadratically
#                    in all partial widths of the excited graviton G* resonance,
#                     is kappa_mG* = sqrt(2)*x1*k/MPl~, where x1~3.83 is the first zero
#                     of the J1 Bessel function and MPl~ is the modified
#                     Planck mass scale [Ran99, Bij01].
#                     default corresponds to k/Mpl~=0.01 in hep-ph/0006114

Pythia.PythiaCommand += [ "pypars parp 50 0.54" ]
# cutoff for QED FSR in Pythia to 20000 GeV (='infinity', photos takes care of it).

#
#  In order to prevent double counting in Pythia when PHOTOS is used
#
Pythia.PythiaCommand += [ "pydat1 parj 90 20000" ]
#
#  Tell Pythia NOT to decay taus
#
Pythia.PythiaCommand += [ "pydat3 mdcy 15 1 0" ]


#!
#! W decays to quarks
#!
Pythia.PythiaCommand += [ "pydat3 mdme 190 1 5",   # d u~
                          "pydat3 mdme 191 1 5",   # d cbar
                          "pydat3 mdme 192 1 5",   # d tbar
                          "pydat3 mdme 193 1 -1",  # d t'bar
                          "pydat3 mdme 194 1 5",   # s ubar
                          "pydat3 mdme 195 1 5",   # s cbar
                          "pydat3 mdme 196 1 5",   # s tbar
                          "pydat3 mdme 197 1 -1",  # s t'bar
                          "pydat3 mdme 198 1 5",   # b ubar
                          "pydat3 mdme 199 1 5",   # b cbar
                          "pydat3 mdme 200 1 5",   # b tbar
                          "pydat3 mdme 201 1 -1",  # b t'bar
                          "pydat3 mdme 202 1 -1",  # b' ubar
                          "pydat3 mdme 203 1 -1",  # b' cbar
                          "pydat3 mdme 204 1 -1",  # b' tbar
                          "pydat3 mdme 205 1 -1" ]  # b' t'bar


#!
#! W decays to leptons
#!
Pythia.PythiaCommand += [ "pydat3 mdme 206 1 4",   # e nu_e
                          "pydat3 mdme 207 1 4",   # mu nu_mu
                          "pydat3 mdme 208 1 4",   # tau nu_tau
                          "pydat3 mdme 209 -1 4" ]  # tau' nu_tau'

 
 
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )
  
## ... Photos
 
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.95
