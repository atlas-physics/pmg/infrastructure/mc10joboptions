###############################################################
#
# Job options file
# prepared by Andrii Tykhonov 
# 
# Higgs decay to hidden sector which results in "electron jets"
#
#==============================================================
# ... Main generator : Pythia

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_PythiaMC09p_Common.py" ) 
#--------------------------------------------------------------
# Algorithms Private Options
#--------------------------------------------------------------
Pythia = topAlg.Pythia
#--------------------------------------------------------------
Pythia.PythiaCommand +=  ["pyinit user lhef",
            "pyinit pylisti -1","pyinit pylistf 1","pyinit dumpr 1 2",
            "pydat1 parj 90 20000", # Turn off FSR.
            "pydat3 mdcy 15 1 0"    # Turn off tau decays.
            ]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )
# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.MadGraphEvgenConfig import evgenConfig
evgenConfig.generators += ["Lhef", "Pythia"]
# dummy needed
evgenConfig.inputfilebase = 'MadGraph'
try:
  if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.MadGraph.115430.ZH_unweight_7TeV.TXT.v2'
  if runArgs.ecmEnergy == 8000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.MadGraph.115430.ZH_unweight_8TeV.TXT.v2'
except NameError:
  pass

evgenConfig.efficiency = 0.9
#==============================================================
#
# End of job options file
#
###############################################################

