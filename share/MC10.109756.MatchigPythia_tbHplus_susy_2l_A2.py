##############################################################
#
# Job options file
#
# Matchig / Pythia gg/gb->t[b]H+ with >=3 leptons
#
# based on input from Caleb Lampen
# Scenario A2: tan beta=15; mu=135 GeV; M2=210 GeV; M_{~l} = 110 GeV
# 
# All non-SUSY decays of H+ are suppressed
# 2 Lepton Filter SFOS
#
# Responsible person(s)
#   Nov 23, 2010 : Caleb Lampen <lampen@physics.arizona.edu>
#                  
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

# ... Pythia
include ( "MC10JobOptions/MC10_Pythia_Common.py" )
Pythia.PythiaCommand += [
                         "pyinit user matchig",  #matchig user process
                         "pysubs msel 0",
                         "pysubs msub 161 1",    #f + g -> f' + H+/-
                         "pysubs msub 401 1",    #g + g -> t + b + H+/-
                         "pysubs msub 402 1",    #q + qbar -> t + b + H+/-
                         "pypars mstp 129 1000", #number of attempts to find highest xsec with 2->3 variables
                         "pypars mstp 2 2",      #second order running alpha strong
                         "pypars mstp 4 1",      #Higgs sector. =1: neutral Higgs couplings set in PARU blocks
                        ]



#MSSM properties
Pythia.PythiaCommand += [
                         "pymssm imss 1 1",      #general MSSM (parameters set by RMSS)

                         #Changed between scenarios
                         "pymssm rmss 1 100.2330467858453",   #U(1) gaugino mass (M1)
                         "pymssm rmss 2 210.0",               #SU(2) gaugino mass (M2)_
                         "pymssm rmss 4 135",                 #mu
                         "pymssm rmss 5 15",                   #tan beta
                         "pymssm rmss 6 120.0",               #left slepton physical mass
                         "pymssm rmss 7 118.0",               #right slepton physical mass
                         "pymssm rmss 19 392.0918",       #mA = sqrt(mHp^2 - 6264.04)

                         #Static across scenarios
                         "pymssm rmss 3 800.0",  #SU(3) gluino mass parameter
                         "pymssm rmss 8 1000.0", #left squark mass
                         "pymssm rmss 9 1000.0", #right squark mass
                         "pymssm rmss 10 1000.0",#left squark mass (3rd generation)
                         "pymssm rmss 11 1000.0",#right sbottom mass
                         "pymssm rmss 12 1000.0",#right stop mass
                         "pymssm rmss 13 250.0", #left stau mass
                         "pymssm rmss 14 250.0", #right stau mass
                         "pymssm rmss 15 2000",  #bottom trilinear coupling Ab
                         "pymssm rmss 16 2000",  #top trilinear coupling At
                         "pymssm rmss 17 0",     #tau trilinear coupling Atau
                        ]

#For TAUOLA/PHOTOS
Pythia.PythiaCommand += [
                         "pydat1 parj 90 20000",   # Turn off FSR.
                         "pydat3 mdcy 15 1 0"      # Turn off tau decays.
                        ]

#Lock H+ decay channels
Pythia.PythiaCommand += [
          
         #Cmd         IDC on       Decay products
         "pydat3 mdme 503 1 0", #dbar            u      
         "pydat3 mdme 504 1 0", #sbar            c                                                                  
         "pydat3 mdme 505 1 0", #bbar            t                                  
         "pydat3 mdme 506 1 -1",#  b'bar           t'        
         "pydat3 mdme 507 1 0", #e+              nu_e                                                               
         "pydat3 mdme 508 1 0", #mu+             nu_mu                                                              
         "pydat3 mdme 509 1 0", #tau+            nu_tau                              
         "pydat3 mdme 510 1 -1",#  tau'+           nu'_tau                                                            
         "pydat3 mdme 511 1 0", #W+              h0                                                                 
         "pydat3 mdme 512 1 0", #~chi_10         ~chi_1+                                                            
         "pydat3 mdme 513 1 0", #~chi_10         ~chi_2+                                                            
         "pydat3 mdme 514 1 1", #~chi_20         ~chi_1+    
         "pydat3 mdme 515 1 1", #~chi_20         ~chi_2+   
         "pydat3 mdme 516 1 1", #~chi_30         ~chi_1+
         "pydat3 mdme 517 1 1", #~chi_30         ~chi_2+                                                            
         "pydat3 mdme 518 1 1", #~chi_40         ~chi_1+                                                            
         "pydat3 mdme 519 1 1", #~chi_40         ~chi_2+ 
         "pydat3 mdme 520 1 0", #~t_1            ~b_1bar                                                            
         "pydat3 mdme 521 1 0", #~t_2            ~b_1bar             
         "pydat3 mdme 522 1 0", #~t_1            ~b_2bar              
         "pydat3 mdme 523 1 0", #~t_2            ~b_2bar              
         "pydat3 mdme 524 1 0", #~d_Lbar         ~u_L       
         "pydat3 mdme 525 1 0", #~s_Lbar         ~c_L                                                               
         "pydat3 mdme 526 1 0", #~e_L+           ~nu_eL  
         "pydat3 mdme 527 1 0", #~mu_L+          ~nu_muL
         "pydat3 mdme 528 1 0", #~tau_1+         ~nu_tauL
         "pydat3 mdme 529 1 0", #~tau_2+         ~nu_tauL                
                          
    ]


# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


#Additional debug statements

#Dump out particle and decay info
#Pythia.PythiaCommand += [
#                         "pyinit pylisti 12",    #dump particle and decay data
#                         ]

#Allow all info to be printed
#from AthenaCommon.AppMgr import ServiceMgr
#ServiceMgr.MessageSvc.infoLimit = 0

#######################################

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

#Will use DiLepton mass filter to pick out events with two leptons of opposite sign,
#same flavor. However, will extend range of mass window as we're not interested
#in that feature
from GeneratorFilters.GeneratorFiltersConf import DiLeptonMassFilter
topAlg += DiLeptonMassFilter()

DiLeptonMassFilter = topAlg.DiLeptonMassFilter
DiLeptonMassFilter.MinPt = 7*GeV
DiLeptonMassFilter.MaxEta = 10
DiLeptonMassFilter.MinMass = 0
DiLeptonMassFilter.MaxMass = 20*TeV #effectively infinite
DiLeptonMassFilter.AllowElecMu = False
DiLeptonMassFilter.AllowSameCharge  = False

try:
  StreamEVGEN.RequireAlgs +=  [ "DiLeptonMassFilter" ]
except Exception, e:
  pass
        

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
#Efficiency actually is 0.5583420
evgenConfig.efficiency = 0.5583420*0.90


#==============================================================
#
# End of job options file
#
###############################################################

