from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence('TopAlg')
from Sherpa_i.Sherpa_iConf import Sherpa_i
sherpa = Sherpa_i()
topAlg += sherpa
from MC10JobOptions.SherpaFEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'group.phys-gener.sherpa.145073.ADDGtogg_2_MS5000_MC5000_KK5_7TeV.TXT.mc10_v1'
evgenConfig.efficiency = 0.9
evgenConfig.weighting = 0
