# dijet production with pythia
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

#--------------------------------------------------------------
# File prepared by Ivan Hollins Dec 2005
#--------------------------------------------------------------
include ( "MC10JobOptions/MC10_Pythia_Common.py" )
Pythia.PythiaCommand += [     "pysubs msel 0",
                              "pysubs ckin 3 17.",
                              "pysubs ckin 4 35.",
                              "pysubs msub 14 1",
                              "pysubs msub 29 1"]


#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import PhotonFilter
topAlg += PhotonFilter()

PhotonFilter = topAlg.PhotonFilter
PhotonFilter.Ptcut = 10000.
PhotonFilter.Etacut = 2.7
PhotonFilter.NPhotons = 1


#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------

try:
     StreamEVGEN.RequireAlgs +=  [ "PhotonFilter" ]
except Exception, e:
     pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
# GEF modified for v14.0.0.1 (21/4/08) by O.Jinnouchi
# 5624/9837*0.9=0.515
evgenConfig.efficiency = 0.515


#==============================================================
#
# End of job options file
#
###############################################################
