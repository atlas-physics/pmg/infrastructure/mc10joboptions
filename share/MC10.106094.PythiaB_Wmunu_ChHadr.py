###############################################################
# PRODUCTION FRAGMENT
#
# Job options file for generation of Wmunu events,
#
# Only events containing at least one muon
# with pT>8GeV |eta|<2.8
# and at least one Charm Hadron in the decay of interest
# are written to output.
#
# Author : Leonid Gladilin (gladilin@mail.cern.ch), 2008-0004-14
#          Tested in Rel. 14.2.25.8 (April 2009)
# Selection criteria can be changed by datacards
#==============================================================
# ... Main generator : PythiaB
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_PythiaB_Common.py" )
#--------------------------------------------------------------
# --------  PYTHIA PARAMETERS OPTIMAL FOR BEAUTY PRODUCTION  --
#--------------------------------------------------------------
# 'mstj 26 0' = no mixing was defined in Btune as default
# 'mstj 22 2' = no K0S, Lambda0 decays in Pythia - defined in Btune as default

PythiaB.PythiaCommand += [ "pysubs msel 0",        # Users decay choice.
                           "pydat1 parj 90 20000.",  # Turn off QED FSR
                           "pydat1 mstj 26 0",
                           "pydat1 parj 13 0.65",
                           "pydat1 parj 14 0.12",
                           "pydat1 parj 15 0.04",
                           "pydat1 parj 16 0.12",
                           "pydat1 parj 17 0.2",
# W production:
                           "pysubs msub 2 1",        # q + q' -> W
                           "pysubs msub 16 1",       # q + q' -> W + g
                           "pysubs msub 20 1",       # q + q' -> W + gamma
                           "pysubs msub 31 1",       # q + g  -> W + q' (signal)
# vanishing              "pysubs msub 36 1",       # q + gamma - > W + q'
#
                           "pydat3 mdme 190 1 0",
                           "pydat3 mdme 191 1 0",
                           "pydat3 mdme 192 1 0",
                           "pydat3 mdme 194 1 0",
                           "pydat3 mdme 195 1 0",
                           "pydat3 mdme 196 1 0",
                           "pydat3 mdme 198 1 0",
                           "pydat3 mdme 199 1 0",
                           "pydat3 mdme 200 1 0",
                           "pydat3 mdme 206 1 0",    # Switch for W->enu.
                           "pydat3 mdme 207 1 1",    # Switch for W->munu.
                           "pydat3 mdme 208 1 0"]    # Switch for W->taunu.

# ... Tauola
#include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Algorithms
#--------------------------------------------------------------
#--------------------------------------------------------------
#              PARAMETERS  SPECIFIC  TO   PYTHIABMODULE
#--------------------------------------------------------------
PythiaB.ForceBDecay = "no"
PythiaB.ForceCDecay = "no"
#
PythiaB.maxTriesHard = 500000
#
PythiaB.ForceDecayChannel = "CharmHadrons"
#
# lvl1 and lvl2 cuts pt_L1 eta_L1 pt_L2 eta_L2
#                                  D+-  Ds+- Lc+- D*(2)pi D*mu D*el D0(2) D0mu D0el |eta|
PythiaB.DecayChannelParameters = [ 2.7, 2.7, -2.7, 2.7,    0.0, 0.0, 0.0,  0.0, 0.0,  2.8]

#--------------------------------------------------------------
# -------------  DEFINE SELECTION CUTS  -------------
#--------------------------------------------------------------				
# Use "PythiaB.flavour =  4."  (events with c-quarks)       before rel. 15.1.0
PythiaB.flavour =  4.
# Use "PythiaB.flavour =  45." (events with b- and c-quarks) since rel. 15.1.0
# PythiaB.flavour =  45.
#  ------------- Selections on b  quarks   -------------
PythiaB.cutbq = ["0. 100. or 0. 100."]
#  ------------- LVL1 muon cuts 0=OFF 1=ON -------------
PythiaB.lvl1cut = [ 0.,  8., 2.8]
#  ------------- LVL2 muon/electron cuts  0=OFF 1=ON-------------
PythiaB.lvl2cut = [ 0.,  13.,     8.,   2.8]
#  ------------- Offline cuts 0=OFF 1=ON -------------
PythiaB.offcut = [ 0., 0.5, 2.5, 3., 2.5, 0.5, 2.5]
#  ------------- Number of repeated hadronization mhadr -------------
PythiaB.mhadr =  1
#
#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
topAlg += LeptonFilter()

LeptonFilter = topAlg.LeptonFilter
LeptonFilter.Ptcut = 8000.
LeptonFilter.Etacut = 2.8

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------

try:
     StreamEVGEN.RequireAlgs +=  [ "LeptonFilter" ]
except Exception, e:
     pass

#==============================================================
#
# End of job options file
#
###############################################################
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
# Cross section and efficiency in Rel. 14.2.25.8:
# non-filtered cross section w/o correction to Br(W->l,nu):
# using "PythiaB.flavour =  45." - 0.256 nb
# using "PythiaB.flavour =  4."  - 0.245 nb
#
# efficiency = 565/725*0.9=0.70137931
#
from MC10JobOptions.PythiaBEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.70
#  ------------- Number of events requested  -------------
evgenConfig.minevents = 500
#==============================================================
#
# End of job options file
#
###############################################################
