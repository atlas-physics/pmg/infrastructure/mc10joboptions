#
#  Control file to generate ZR (LRSM) production in Pythia, M(ZR) = 2500 GeV, M(NR) = 400 GeV
#
#  MC10 sample 106657
#
#  Prepared by V.Savinov ( vps3@pitt.edu, http://www.phyast.pitt.edu/~savinov ), 07/17/07
# 
#  June 23, 2008:  Modified and validated with Rel. 14.1.0.4 by R.Yoosoofmiya and V.Savinov 
#
#--------------------------------------------------------------
#
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )
#
#--------------------------------------------------------------
#
#  Full user control
#
Pythia.PythiaCommand += ["pysubs msel 0"]
#
#  WR (LRSM) production
#
#++
#
#  qqbar -> Z_R0
#
Pythia.PythiaCommand += ["pysubs msub 353 1"]
#
#  Masses of W_R, Z_R and Majorana neutrinos
#    
Pythia.PythiaCommand += ["pydat2 pmas 9900024 1 2000.0"]
Pythia.PythiaCommand += ["pydat2 pmas 9900023 1 2500.0"]
#
Pythia.PythiaCommand += ["pydat2 pmas 9900012 1 400.0"]
Pythia.PythiaCommand += ["pydat2 pmas 9900014 1 400.0"]
Pythia.PythiaCommand += ["pydat2 pmas 9900016 1 400.0"]
# 
#-------------------------------------------------------------
# 
#  Force certain decays of Z_R0: we are going to use only channels with charged leptons in final state
#
Pythia.PythiaCommand += ["pydat3 mdme 4170 1 0"]  
Pythia.PythiaCommand += ["pydat3 mdme 4171 1 0"]   
Pythia.PythiaCommand += ["pydat3 mdme 4172 1 0"]   
Pythia.PythiaCommand += ["pydat3 mdme 4173 1 0"]   
Pythia.PythiaCommand += ["pydat3 mdme 4174 1 0"]   
Pythia.PythiaCommand += ["pydat3 mdme 4175 1 0"]   
#
#  e+e-
#
Pythia.PythiaCommand += ["pydat3 mdme 4176 1 1"]   
Pythia.PythiaCommand += ["pydat3 mdme 4177 1 0"]   
#
#  Nu_e Nu_e
#
Pythia.PythiaCommand += ["pydat3 mdme 4178 1 1"]   
#
#  mu+mu-
#
Pythia.PythiaCommand += ["pydat3 mdme 4179 1 1"]   
Pythia.PythiaCommand += ["pydat3 mdme 4180 1 0"]   
#
#  Nu_mu Nu_mu
#
Pythia.PythiaCommand += ["pydat3 mdme 4181 1 1"]   
#
#  tau+tau-
#
Pythia.PythiaCommand += ["pydat3 mdme 4182 1 1"]   
Pythia.PythiaCommand += ["pydat3 mdme 4183 1 0"]   
#
#  Nu_tau Nu_tau
#
Pythia.PythiaCommand += ["pydat3 mdme 4184 1 1"]   
# 
#-------------------------------------------------------------
#
#  In order to prevent double counting in Pythia when PHOTOS is used
#
Pythia.PythiaCommand += ["pydat1 parj 90 20000"]
#
#  Tell Pythia NOT to decay taus
#
Pythia.PythiaCommand += ["pydat3 mdcy 15 1 0"]
#
#  Print the event listing for events x though y: 
#
Pythia.PythiaCommand += ["pyinit dumpr 1 20"]
#
#-------------------------------------------------------------
#
# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )
#
#-------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
