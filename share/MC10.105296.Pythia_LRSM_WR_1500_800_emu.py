#
#  Control file to generate W_R (1500 GeV) -> e nu_Rmu (800 GeV) and mu nu_Re (800 GeV) -> e mu jet jet (i.e. with mixing)
#
#  June 24, 2009:  prepared with 15.2.0 by Kirill Skovpen
#                  initial code by Vladimir Savinov and Alexei Maslennikov
#
#--------------------------------------------------------------
#
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )
#
#--------------------------------------------------------------
#
#  Full user control
#
Pythia.PythiaCommand += ["pysubs msel 0"]
#
#  ffbar -> W_R+
#
Pythia.PythiaCommand += ["pysubs msub 354 1"]
#
#  Masses of W_R, Z_R and Majorana neutrinos
#    
Pythia.PythiaCommand += ["pydat2 pmas 9900024 1 1500.0"]
Pythia.PythiaCommand += ["pydat2 pmas 9900023 1 1000.0"]
#
Pythia.PythiaCommand += ["pydat2 pmas 9900012 1 800.0"]
Pythia.PythiaCommand += ["pydat2 pmas 9900014 1 800.0"]
Pythia.PythiaCommand += ["pydat2 pmas 9900016 1 800.0"]
# 
#-------------------------------------------------------------
#
#  Force certain decays of W_R+ and W_R-: we are going to use only channels with leptons
#
Pythia.PythiaCommand += ["pydat3 mdme 4185 1 0"]  
Pythia.PythiaCommand += ["pydat3 mdme 4186 1 0"]   
Pythia.PythiaCommand += ["pydat3 mdme 4187 1 0"]   
Pythia.PythiaCommand += ["pydat3 mdme 4188 1 0"]   
Pythia.PythiaCommand += ["pydat3 mdme 4189 1 0"]   
Pythia.PythiaCommand += ["pydat3 mdme 4190 1 0"]   
Pythia.PythiaCommand += ["pydat3 mdme 4191 1 0"]   
Pythia.PythiaCommand += ["pydat3 mdme 4192 1 0"]   
Pythia.PythiaCommand += ["pydat3 mdme 4193 1 0"]   
#
# Redefine decay channels of the W_R to introduce Lepton Flavor Violation due to mixing in heavy neutrinos sector
#
#  e nu_Rmu
#
Pythia.PythiaCommand += ["pydat3 kfdp 4194 1 -11"]
Pythia.PythiaCommand += ["pydat3 kfdp 4194 2 9900014"]
#
#  mu nu_Re
#
Pythia.PythiaCommand += ["pydat3 kfdp 4195 1 -13"]
Pythia.PythiaCommand += ["pydat3 kfdp 4195 2 9900012"]
#
#  leave nu_Rtau alone for the time being...
#
#  e+ nu_Rmu
#
Pythia.PythiaCommand += ["pydat3 mdme 4194 1 1"]
#
#  mu+  nu_Re
#
Pythia.PythiaCommand += ["pydat3 mdme 4195 1 1"]
#
#  tau+ nu_rTau
#
Pythia.PythiaCommand += ["pydat3 mdme 4196 1 0"]
#
#-------------------------------------------------------------
#
#  In order to prevent double counting in Pythia when PHOTOS is used
#
Pythia.PythiaCommand += ["pydat1 parj 90 20000"]
#
#  Tell Pythia NOT to decay taus
#
Pythia.PythiaCommand += ["pydat3 mdcy 15 1 0"]
#
#  Print the event listing for events x though y: 
#
Pythia.PythiaCommand += ["pyinit dumpr 1 20"]
#
#-------------------------------------------------------------
#
# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )
#
#-------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
