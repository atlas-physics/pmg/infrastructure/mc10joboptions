###############################################################
#  10 TeV truth jet filter 32, ptmin 8
# Edward Sarkisyan-Grinbaum / May 2008 /
#==============================================================

#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------

# ... Main generator : Pythia
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )
Pythia.PythiaCommand += [ "pysubs msel 0",
                          "pysubs ckin 3 8.",
                          "pysubs msub 11 1",
                          "pysubs msub 12 1",
                          "pysubs msub 13 1",
                          "pysubs msub 68 1",
                          "pysubs msub 28 1",
                          "pysubs msub 53 1"]

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

try:
     from RecExConfig.RecFlags  import rec
     rec.doTruth = True
     from JetRec.JetGetters import * 
     c7=make_StandardJetGetter('Cone',0.7,'Truth') 
     c7alg = c7.jetAlgorithmHandle() 
     c7alg.JetFinalEtCut.MinimumSignal = 10.0*GeV
     c7alg.ConeFinder.SeedPt = 2.0*GeV
except Exception, e:
     pass
 
from GeneratorFilters.GeneratorFiltersConf import TruthJetFilter
topAlg += TruthJetFilter()



# TruthJet filter
TruthJetFilter = topAlg.TruthJetFilter
TruthJetFilter.Njet=1;
TruthJetFilter.NjetMinPt=32.*GeV;
TruthJetFilter.NjetMaxEta=5.5;
TruthJetFilter.jet_pt1=32.*GeV;
TruthJetFilter.TruthJetContainer="Cone7TruthJets";
TruthJetFilter.OutputLevel = 3

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------

try:
     StreamEVGEN.RequireAlgs += [ "TruthJetFilter" ]
except Exception, e:
     pass


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
## 4598/185186*0.9 = 0.02234
evgenConfig.efficiency = 0.0223 # generator filtering efficiency 

#==============================================================
#
# End of job options file
#
###############################################################
