###############################################################
#
# Job options file
#
# Higgs -> Gamma Gamma Pythia via WH, W->qq , mH = 110 GeV
#
# Responsible person(s)
#   2 Oct, 2008-xx xxx, 20xx: Bertrand Brelier (brelier@lps.umontreal.ca)
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

# ... Pythia
include ( "MC10JobOptions/MC10_Pythia_Common.py" )
Pythia.PythiaCommand += [
     "pysubs msel 0",
     "pydat1 parj 90 20000",
     "pysubs msub 26 1",
     "pydat2 pmas 25 1 110.",
     "pydat3 mdme 190 1 1",
     "pydat3 mdme 191 1 1",
     "pydat3 mdme 192 1 1",
     "pydat3 mdme 193 1 -1",
     "pydat3 mdme 194 1 1",
     "pydat3 mdme 195 1 1",
     "pydat3 mdme 196 1 1",
     "pydat3 mdme 197 1 -1",
     "pydat3 mdme 198 1 1",
     "pydat3 mdme 199 1 1",
     "pydat3 mdme 200 1 1",
     "pydat3 mdme 206 1 0", #e
     "pydat3 mdme 207 1 0", #mu
     "pydat3 mdme 208 1 0", #tau
     "pydat3 mdme 210 1 0",
     "pydat3 mdme 211 1 0",
     "pydat3 mdme 212 1 0",
     "pydat3 mdme 213 1 0",
     "pydat3 mdme 214 1 0",
     "pydat3 mdme 215 1 0",
     "pydat3 mdme 216 1 -1",
     "pydat3 mdme 217 1 -1",
     "pydat3 mdme 218 1 0",
     "pydat3 mdme 219 1 0",
     "pydat3 mdme 220 1 0",
     "pydat3 mdme 221 1 -1",
     "pydat3 mdme 222 1 0",
     "pydat3 mdme 223 1 1", #H-> GamGam
     "pydat3 mdme 224 1 0",
     "pydat3 mdme 225 1 0",
     "pydat3 mdme 226 1 0" ]

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import PhotonFilter
topAlg += PhotonFilter()

PhotonFilter = topAlg.PhotonFilter
PhotonFilter.Ptcut = 20000.
PhotonFilter.Etacut = 2.7
PhotonFilter.NPhotons = 2

try:
     StreamEVGEN.RequireAlgs +=  [ "PhotonFilter" ]
except Exception, e:
     pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.67
#==============================================================
#
# End of job options file
#
###############################################################
