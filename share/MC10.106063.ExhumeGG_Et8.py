###############################################################
# Job option file for Exhume sample
# 
# Created by Andrew Pilkington in Rel. 14.2.23.4(November 2008)
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

from Exhume_i.Exhume_iConf import ExHuME
topAlg +=ExHuME()

#--------------------------------------------------------------
# Exhume run control file
#--------------------------------------------------------------
exhumef=open('./TenTeVOverride.dat', 'w')
exhumeinp = """FNAL_or_LHC     -1
root_s  10000.0
"""
exhumef.write(exhumeinp)
exhumef.close()

ExHuME.Process  =  "GG"
ExHuME.ETMin=8.0
ExHuME.CosThetaMax=0.99
ExHuME.ExhumeRunControl="TenTeVOverride.dat"

from MC10JobOptions.ExhumeEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9

from TruthExamples.TruthExamplesConf import PrintMC
topAlg += PrintMC()
topAlg.PrintMC.McEventKey = "GEN_EVENT"
topAlg.PrintMC.VerboseOutput = TRUE
topAlg.PrintMC.PrintStyle = "Barcode"
topAlg.PrintMC.FirstEvent = 1
topAlg.PrintMC.LastEvent =  5

#
# End of job options file
#
###############################################################



