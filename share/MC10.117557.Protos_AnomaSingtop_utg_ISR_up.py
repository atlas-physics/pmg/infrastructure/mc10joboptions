###############################################################
# Job options file for generating Anomalous top(u+g->t) events with Protos 2.01
# The top mass is 172.5 GeV 
# more ISR
# Muhammad alhroob (alhroob@cern.ch)
#==============================================================
#
from AthenaCommon.AlgSequence import AlgSequence
topAlg=AlgSequence("TopAlg")

include ("MC10JobOptions/MC10_PythiaMC09p_Common.py")

# Systematics (ISR more)
Pythia.PythiaCommand += [ "pypars parp 67 6.0" ] #ISR up
Pythia.PythiaCommand += [ "pypars parp 64 0.25" ] #ISR up

# Pythia options
Pythia.PythiaCommand += ["pyinit user protos",
                        "pydat1 parj 90 20000.",
                        "pydat3 mdcy 15 1 0",
                        "pyinit dumpr 1 12",
                        "pyinit pylistf 1"
                       ]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
# Dummy filename for 8, 10 and 14 TeV used the same as for 7 TeV
evgenConfig.inputfilebase = 'protos'
try:
  if runArgs.ecmEnergy == 7000.0:
     evgenConfig.inputfilebase = 'group09.phys-gener.protos201.117550.AnomaSingtop_utg_7TeV.TXT.v1'
  if runArgs.ecmEnergy == 8000.0:
     evgenConfig.inputfilebase = 'group09.phys-gener.protos201.117550.AnomaSingtop_utg_8TeV.TXT.v1'
  if runArgs.ecmEnergy == 10000.0:
     evgenConfig.inputfilebase = 'group09.phys-gener.protos201.117550.AnomaSingtop_utg_10TeV.TXT.v1'
  if runArgs.ecmEnergy == 14000.0:
     evgenConfig.inputfilebase = 'group09.phys-gener.protos201.117550.AnomaSingtop_utg_14TeV.TXT.v1'  	
except NameError:
  pass

evgenConfig.efficiency = 0.9
#==============================================================
# End of job options file
#
###############################################################

