## Job options file for Herwig++, NLO Z -> mu mu production


## Get a handle on the top level algorithms' sequence
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

try:
     include ( "MC10JobOptions/MC10_Herwigpp_NLOME_Common.py" )
except NameError:
     # needed (dummy) default
     # from Herwigpp_i.Herwigpp_iConf import Herwigpp
     # topAlg += Herwigpp()
     cmds =  ""


## Add to commands
cmds += """
## Set up Powheg truncated shower
set /Herwig/Shower/Evolver:HardEmissionMode POWHEG
## Use 2-loop alpha_s
create Herwig::O2AlphaS /Herwig/AlphaQCD_O2
set /Herwig/Generators/LHCGenerator:StandardModelParameters:QCD/RunningAlphaS /Herwig/AlphaQCD_O2

## Set up qq -> Z -> l l process at NLO, using Powheg
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/PowhegMEqq2gZ2ff
set /Herwig/MatrixElements/SimpleQCD:MatrixElements[0]:Process Muon
set /Herwig/MatrixElements/SimpleQCD:MatrixElements[0]:ScaleOption Dynamic

## Set mass cut on dilepton pair (default = 20 GeV)
#set /Herwig/Cuts/MassCut:MinM 50.*GeV
"""

## Set commands
topAlg.Herwigpp.Commands = cmds.splitlines()

## Pass through LeptonFilter (ensures each event contains a charged lepton)
try:
     from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
     topAlg += LeptonFilter()
     topAlg.LeptonFilter.Ptcut = 7000.0
     topAlg.LeptonFilter.Etacut = 2.7
     StreamEVGEN.RequireAlgs += [ "LeptonFilter" ]
except Exception:
     pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.HerwigppEvgenConfig import evgenConfig
#evgenConfig.efficiency = 0.9
#evgenConfig.minevents=100

#==============================================================
#
# End of job options file
#
###############################################################


