# dijet production with pythia
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC10JobOptions/MC10_Pythia_Common.py" )
Pythia.PythiaCommand += [ "pysubs msel 0",
                          "pysubs ckin 3 15.",
                          "pysubs msub 14 1",
                          "pysubs msub 29 1" ]

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import AsymJetFilter
topAlg += AsymJetFilter()

AsymJetFilter = topAlg.AsymJetFilter
AsymJetFilter.JetNumber = 2
AsymJetFilter.EtaRange = 2.7
AsymJetFilter.JetThreshold1 = 30000.; 
AsymJetFilter.JetThreshold2 = 20000.; 
AsymJetFilter.JetType=FALSE; # true is a cone, false is a grid
AsymJetFilter.GridSizeEta=2; # sets the number of (approx 0.06 size) eta
AsymJetFilter.GridSizePhi=2; # sets the number of (approx 0.06 size) phi cells

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------

try:
     StreamEVGEN.RequireAlgs += [ "AsymJetFilter" ]
except Exception, e:
     pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
# efficiency = 5000/155215 = 0.0290
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.0290
