from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")
include ( "MC10JobOptions/MC10_Pythia_Common.py" )
	
Pythia.PythiaCommand += ["pydat1 parj 90 20000.",      ## Turn off FSR
	                         "pydat3 mdcy 15 1 0"] ## Turn off tau decays
Pythia.PythiaCommand +=["pyinit user exograviton"]
Pythia.PythiaCommand += ["grav 1 6","grav 2 1110",
                         "grav 3 7000","grav 4 3000",
	                 "grav 5 80","grav 6 3000",
	                 "grav 7 5E+5"]
	
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )
	
	
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
evgenConfig.minevents=500
