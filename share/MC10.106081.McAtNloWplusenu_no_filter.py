###############################################################
#
# Job options file
# Developed by Pavel Staroba
# in Rel. 15.6.1.7 (February 2010)
# Updated by Marc Goulette (September 2010)
#
#===============================================================

# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common_7TeV.py" )
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common.py" )
  if runArgs.ecmEnergy == 14000.0:
    include ( "MC10JobOptions/MC10_McAtNloJimmy_Common_14TeV.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig
Herwig.HerwigCommand += [ "taudec TAUOLA"]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
# efficiency = 0.95 - no filtering
# Non-filtered NLO cross section in Rel. 15.6.1.7 : 0.951899 nb
from MC10JobOptions.McAtNloEvgenConfig import evgenConfig

#dummy needed
#NOTE: We are using the patched DY version 341 from 06/10 which is equivalent to version 342
# The files are physically at: /afs/cern.ch/atlas/groups/WZgroup/scratch0/LINUX.106081.Pdf10550.OutputDir/
evgenConfig.inputfilebase = 'mcatnlo341'
try:
  if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputfilebase = 'group09.phys-gener.mcatnlo341.106081.Wplusenu_no_filter_7TeV_Pdf10550.TXT.v1'
    print "EEEE 7 TeV"
  if runArgs.ecmEnergy == 8000.0:
    evgenConfig.inputfilebase = 'group09.phys-gener.mcatnlo341.106081.Wplusenu_no_filter_8TeV_Pdf10550.TXT.v1'
    print "EEEE 8 TeV"
except NameError:
  pass

evgenConfig.efficiency = 0.84
#==============================================================
#
# End of job options file
#
###############################################################
