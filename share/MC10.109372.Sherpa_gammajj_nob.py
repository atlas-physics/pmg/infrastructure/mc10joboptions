###############################################################
#
# Job options file
#
# Sherpa gammajj (no b)
#
# Responsible person(s)
#   10 March, 2009-: William Murray <bill.murray@stfc.ac.uk>
#
#==============================================================
import AthenaCommon.AtlasUnixGeneratorJob
from AthenaCommon.AppMgr import ServiceMgr as svcMgr
#
#load relevant libraries
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
#
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
from Sherpa_i.Sherpa_iConf import ReadSherpa_i
#
sherpa = ReadSherpa_i()
sherpa.Files = [ "sherpa.evts" ]
#
topAlg += sherpa
#
from MC10JobOptions.SherpaEvgenConfig import evgenConfig
 
# inputfilebase
evgenConfig.inputfilebase = 'Sherpa10103.109372.gammajj_nob_v8'
evgenConfig.efficiency = 0.95
#
#==============================================================
#
# End of job options file
#
################################################################ 
