# Wprime template production with pythia
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

#--------------------------------------------------------------
# Algorithms Private Options
#--------------------------------------------------------------

# Zprime resonance mass (in GeV)
WprimeMass = 1000

include ( "MC10JobOptions/MC10_PythiaResMod_Common.py" )

# enable using the modified pysgex.F
#(default: UseResMod=0; do not use modified pysgex.F)
#(UseResMod=1; use modified pysgex.F)
PythiaResMod.UseResMod=1

#other commands for PythiaResMod are as they are for Pythia alg.
# W prime specific parameters for pythia :
PythiaResMod.PythiaCommand += [
       "pysubs msel 0",
       "pydat1 parj 90 20000",
       "pydat3 mdcy 15 1 0",
       "pysubs msub 142 1",   # SSM W'
       # W' decays  to quarks
       "pydat3 mdme 311 1 0",
       "pydat3 mdme 312 1 0",
       "pydat3 mdme 313 1 0",
       "pydat3 mdme 315 1 0",
       "pydat3 mdme 316 1 0",
       "pydat3 mdme 317 1 0",
       "pydat3 mdme 319 1 0",
       "pydat3 mdme 320 1 0",
       "pydat3 mdme 321 1 0",
       # W' decay to e nu - mu nu - tau nu 
       "pydat3 mdme 327 1 0",
       "pydat3 mdme 328 1 1",
       "pydat3 mdme 329 1 0",
       #
       "pydat1 mstu 1 0",
       "pydat1 mstu 2 0",
       "pydat2 pmas 34 1 "+str(WprimeMass),
       "pyinit pylisti 12"
       ]

include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaResModEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9

#==============================================================
#
# End of job options file
#
###############################################################
