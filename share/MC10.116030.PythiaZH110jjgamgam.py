###############################################################
#
# Job options file
#
# Higgs -> Gamma Gamma Pythia via ZH, Z->qq , mH = 110 GeV
#
# Responsible person(s)
#   2 Oct, 2008-xx xxx, 20xx: Bertrand Brelier (brelier@lps.umontreal.ca)
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

# ... Pythia
include ( "MC10JobOptions/MC10_Pythia_Common.py" )
Pythia.PythiaCommand += [
     "pysubs msel 0",
     "pydat1 parj 90 20000",
     "pysubs msub 24 1",
     "pydat2 pmas 25 1 110.",
     "pydat3 mdme 174 1 1",
     "pydat3 mdme 175 1 1",
     "pydat3 mdme 176 1 1",
     "pydat3 mdme 177 1 1",
     "pydat3 mdme 178 1 1",
     "pydat3 mdme 179 1 1",
     "pydat3 mdme 180 1 -1",
     "pydat3 mdme 181 1 -1",
     "pydat3 mdme 182 1 0", # e
     "pydat3 mdme 183 1 0", # nu_e
     "pydat3 mdme 184 1 0", # mu
     "pydat3 mdme 185 1 0", # nu_mu
     "pydat3 mdme 186 1 0", # tau
     "pydat3 mdme 187 1 0", # nu_tau
     "pydat3 mdme 210 1 0",
     "pydat3 mdme 211 1 0",
     "pydat3 mdme 212 1 0",
     "pydat3 mdme 213 1 0",
     "pydat3 mdme 214 1 0",
     "pydat3 mdme 215 1 0",
     "pydat3 mdme 216 1 -1",
     "pydat3 mdme 217 1 -1",
     "pydat3 mdme 218 1 0",
     "pydat3 mdme 219 1 0",
     "pydat3 mdme 220 1 0",
     "pydat3 mdme 221 1 -1",
     "pydat3 mdme 222 1 0",
     "pydat3 mdme 223 1 1", #H-> GamGam
     "pydat3 mdme 224 1 0",
     "pydat3 mdme 225 1 0",
     "pydat3 mdme 226 1 0" ]

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import PhotonFilter
topAlg += PhotonFilter()

PhotonFilter = topAlg.PhotonFilter
PhotonFilter.Ptcut = 20000.
PhotonFilter.Etacut = 2.7
PhotonFilter.NPhotons = 2

try:
     StreamEVGEN.RequireAlgs +=  [ "PhotonFilter" ]
except Exception, e:
     pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.67
#==============================================================
#
# End of job options file
#
###############################################################
