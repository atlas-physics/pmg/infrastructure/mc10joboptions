###############################################################
#
# Job options file
# prepared by Haiping Peng in Rel. 15.6.1.7 (Feb 2010)
# Acceptance sample
# Z (mu mu)+ Gamma with full interference between ISR and FSR digram with MadGraph/Pythia
# with Z mass >200GeV
#
#==============================================================

# ... Main generator : Pythia
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_PythiaMC09p_Common.py" )
Pythia.PythiaCommand += [ "pyinit user lhef",
                          "pyinit pylisti -1","pyinit pylistf 1","pyinit dumpr 1 2",
                          "pydat1 parj 90 20000", # Turn off FSR.
                          "pydat3 mdcy 15 1 0"   # Turn off tau decays.
]

## ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

## ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


#---------------------------------------------------------------------------
# Filter
#---------------------------------------------------------------------------
#
#from GeneratorFilters.GeneratorFiltersConf import PhotonFilter
#topAlg += PhotonFilter()
#PhotonFilter = topAlg.PhotonFilter
#PhotonFilter.Ptcut = 10000.
#PhotonFilter.Etacut = 2.7
#PhotonFilter.NPhotons = 1

#from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
#topAlg += LeptonFilter()
#LeptonFilter = topAlg.LeptonFilter
#LeptonFilter.Etacut = 2.7
#
#try:
#    StreamEVGEN.RequireAlgs +=  [ "PhotonFilter" ]
#except Exception, e:
#    pass
#try:
#    StreamEVGEN.RequireAlgs += [ "LeptonFilter" ]
#except Exception, e:
#    pass
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
# Non-filtered cross section in Rel. 15.6.1.7 : 2.399e-02 pb
#--------------------------------------------------------------
from MC10JobOptions.PythiaEvgenConfig import evgenConfig
#from EvgenJobOptions.MadGraphEvgenConfig import evgenConfig
evgenConfig.generators += ["Lhef", "Pythia"]
evgenConfig.inputfilebase = "group09.phys-gener.MadGraph.105968.Zmumugamma_M200.TXT.v1"
evgenConfig.efficiency = 0.9
#  ------------- Number of events requested  -------------
evgenConfig.minevents = 5000
#==============================================================
#
# End of job options file
#
###############################################################

