##############################################################
#
# Job option for POWHEG with Herwig+Jimmy
# Graham Jones Feb. 2011
#
##############################################################

from AthenaCommon.AlgSequence import AlgSequence
topAlg=AlgSequence()

#Include common Powheg setup
try:
    if runArgs.ecmEnergy == 7000.0:        
        include ( "MC10JobOptions/MC10_PowHegJimmy_Common_7TeV.py" )
    if runArgs.ecmEnergy == 8000.0:        
        include ( "MC10JobOptions/MC10_PowHegJimmy_Common_8TeV.py" )
    if runArgs.ecmEnergy == 10000.0:
        include ( "MC10JobOptions/MC10_PowHegJimmy_Common.py" )
    if runArgs.ecmEnergy == 14000.0:
        include ( "MC10JobOptions/MC10_PowHegJimmy_Common_14TeV.py" )
except NameError:
    # needed (dummy) default
    from Herwig_i.Herwig_iConf import Herwig
    topAlg += Herwig()
    Herwig = topAlg.Herwig


Herwig.HerwigCommand += [ "taudec TAUOLA",
                          "rmass 1 0.33",    # down
                          "rmass 2 0.33",    # up
                          "rmass 3 0.50",    # strange
                          "rmass 4 1.50",    # charm
                          "rmass 5 4.75",    # bottom
                          "rmass 6 176.0256" # top
                          ]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for Evgen Job Transforms
#--------------------------------------------------------------

from MC10JobOptions.EvgenConfig import evgenConfig, knownGenerators
evgenConfig.generators += [ "Lhef", "Herwig" ]

#dummy needed
evgenConfig.inputfilebase = 'PowHeg'
try:
    if runArgs.ecmEnergy == 7000.0:
        evgenConfig.inputfilebase = 'group10.phys-gener.PowHeg7.126175.Dijet.TXT.v1'
    if runArgs.ecmEnergy == 8000.0:
        evgenConfig.inputfilebase = 'group10.phys-gener.PowHeg8.126175.Dijet.TXT.v1'
except NameError:
    pass

evgenConfig.efficiency = 0.9
evgenConfig.weighting = 0    # Important to prevent Athena crashes from very large weights

# 7TeV inputs produced with POWHEG-BOX r302
#
# URL: svn://powhegbox.mib.infn.it/trunk/POWHEG-BOX
# Repository Root: svn://powhegbox.mib.infn.it
# Repository UUID: 9e129b38-c2c0-4eec-9301-dc54fda404de
# Revision: 302
# Node Kind: directory
# Schedule: normal
# Last Changed Author: nason
# Last Changed Rev: 302
# Last Changed Date: 2010-12-21 14:36:25 +0000 (Tue, 21 Dec 2010)

#==============================================================
#
# End of job options file
#
###############################################################
