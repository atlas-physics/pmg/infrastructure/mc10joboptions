from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg") 
include ( "MC10JobOptions/MC10_Pythia_Common.py" )
Pythia.PythiaCommand +=["pyinit user exograviton"]
Pythia.PythiaCommand += ["grav 1 3","grav 2 1110",
                         "grav 3 10000","grav 4 3500",
                         "grav 5 350","grav 6 3500",
                         "grav 7 2.5E+1"]

Pythia.PythiaCommand += ["pydat1 parj 90 20000.",  ## Turn off FSR
                         "pydat3 mdcy 15 1 0"]     ## Turn off tau decays

include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


from MC10JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
