###############################################################
# for Black Hole production
#
# Michiru Kaneda <Michiru.Kaneda@cern.ch>

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC10JobOptions/MC10_PythiaCT66_Common.py" )
Pythia.PythiaCommand += [
    # Use Lhef interface
    "pyinit user lhef",

    # No tau decays (Tauola)
    #"pydat3 mdcy 15 1 0",
    # No FSR (Photos)
    "pydat1 parj 90 20000",
]


# Tauola
#include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from TruthExamples.TruthExamplesConf import TestHepMC
TestHepMC.EnergyDifference = 100000000.
TestHepMC.OutputLevel = DEBUG

from MC10JobOptions.LhefEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
evgenConfig.inputfilebase = "group09.phys-gener.BlackMax.120397.BH2_BM_n4_Mth4500_MD1000.TXT.v1"
