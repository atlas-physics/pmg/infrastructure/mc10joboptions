##############################################################################
# Job options file for WHIZARD with Herwig+Jimmy
# ttbar+gamma events (no fully hadronic)
# Oliver Rosenthal, May 2011
#
#============================================================================
#--------------------------------------------------------------
# General Application Configuration options
#--------------------------------------------------------------
import AthenaCommon.AtlasUnixGeneratorJob

from AthenaCommon.AppMgr import theApp
from AthenaCommon.AppMgr import ServiceMgr

# make sure we are loading the ParticleProperty service
from PartPropSvc.PartPropSvcConf import PartPropSvc
ServiceMgr += PartPropSvc()

#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
ServiceMgr.MessageSvc.OutputLevel = INFO

#--------------------------------------------------------------
# Event related parameters
#--------------------------------------------------------------
# Number of events to be processed (default is 10)
#theApp.EvtMax = -1
#--------------------------------------------------------------
# Algorithms Private Options
#--------------------------------------------------------------
# from AthenaServices.AthenaServicesConf import AtRndmGenSvc
# ServiceMgr += AtRndmGenSvc()
# ServiceMgr.AtRndmGenSvc.Seeds = ["HERWIG 4789899 989240512", "HERWIG_INIT 820021 2347532"]


from AthenaCommon.AlgSequence import AlgSequence
topAlg=AlgSequence()

# ... Main generator : Herwig
try:
  if runArgs.ecmEnergy == 7000.0:
     include ( "MC10JobOptions/MC10_Herwig_Common_7TeV.py" )
  if runArgs.ecmEnergy == 8000.0:
     include ( "MC10JobOptions/MC10_Herwig_Common_8TeV.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig


Herwig.HerwigCommand += [
			  # initializations
			  "iproc lhef",
			  "taudec TAUOLA"]

# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )


from MC10JobOptions.EvgenConfig import evgenConfig, knownGenerators
evgenConfig.generators += [ "Lhef", "Herwig" ]
evgenConfig.inputfilebase = 'group10.phys-gener.whizard.117401.ttbarphoton_sm_nofullhad_7TeV.TXT.v1'
evgenConfig.efficiency = 0.95
#==============================================================
#
# End of job options file
#
##############################################################
