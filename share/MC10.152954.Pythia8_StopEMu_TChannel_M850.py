
#import AthenaCommon.AtlasUnixGeneratorJob
MessageSvc = Service( "MessageSvc" )
from AthenaCommon.AppMgr import ServiceMgr as svcMgr
import AthenaServices

#--------------------------------------------------------------
# Event related parameters
#--------------------------------------------------------------
# Number of events to be processed (default is 10)
theApp.EvtMax = 3
#--------------------------------------------------------------
# Algorithms Private Options
#--------------------------------------------------------------
#from AthenaServices.AthenaServicesConf import AtRndmGenSvc
#ServiceMgr += AtRndmGenSvc()
#ServiceMgr.AtRndmGenSvc.Seeds = ["PYTHIA8 4789899 989240512",
#                                 "PYTHIA8_INIT 820021 2347532"]
#
#svcMgr.MessageSvc.OutputLevel = INFO

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

from Pythia8_i.Pythia8_iConf import Pythia8_i
Pythia8 = Pythia8_i()
topAlg += Pythia8
Pythia8.Commands += ["Tune:pp=5"]
Pythia8.Commands += [ "Main:timesAllowErrors = 500"]
Pythia8.Commands += ["6:m0 = 172.5"]
Pythia8.Commands += ["23:m0 = 91.1876"]
Pythia8.Commands += ["23:mWidth = 2.4952"]
Pythia8.Commands += ["24:m0 = 80.403"]
Pythia8.Commands += ["24:mWidth = 2.141"]
Pythia8.Commands += ["1000006:m0 = 850.0"]

Pythia8.Commands += ["ParticleDecays:limitTau0 = on"]  # set K_S, Lambda stable


#topAlg.Pythia8_i.useRndmGenSvc = True
topAlg.Pythia8_i.UserProcess = "qqbar2emu"

#topAlg.Pythia8_i.Commands += ['Random:setSeed = on']
#topAlg.Pythia8_i.Commands += ['Random:seed = 123456789']

#topAlg.Pythia8_i.CollisionEnergy = 10000

#from TruthExamples.TruthExamplesConf import DumpMC
#topAlg += DumpMC()


from MC10JobOptions.Pythia8EvgenConfig import evgenConfig
evgenConfig.efficiency = 1
