###############################################################
#
#  Giacomo Polesello <giacomo.polesello@cern.ch>
#  Claire Gwenlan <c.gwenlan1@physics.ox.ac.uk>
#
#  POWHEG  WZ NLO production
#  
#  Z ->mu mu interfaced to PYTHIA
#  
#==============================================================

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

# ... Main generator : Pythia
include ( "MC10JobOptions/MC10_PowHegPythia_Common.py" )
Pythia.PythiaCommand += [ "pyinit user lhef",
                          "pyinit pylisti -1",
                          "pyinit pylistf 1",
                          "pyinit dumpr 1 2",
                          "pydat1 parj 90 20000", # Turn off FSR.
                          "pydat3 mdcy 15 1 0"    # Turn off tau decays.
]


# ... Tauola
include ( "MC10JobOptions/MC10_Tauola_Fragment.py" )

# ... Photos
include ( "MC10JobOptions/MC10_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
#
#from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
#topAlg += LeptonFilter()
#
#LeptonFilter = topAlg.LeptonFilter
#LeptonFilter.Ptcut = 10000.
#LeptonFilter.Etacut = 2.7
#
#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------
# 
#try:
#  StreamEVGEN.RequireAlgs += [ 'LeptonFilter' ]
#except Exception, e:
#  pass
# 
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
#
from MC10JobOptions.EvgenConfig  import evgenConfig, knownGenerators
evgenConfig.generators += ["Lhef", "Pythia"]

# input created using POWHEG-BOX v1
evgenConfig.inputfilebase = 'PowHeg'
try:
  if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.PowHegBoxV1.113905.Zmu_7TeV.TXT.v3'
  if runArgs.ecmEnergy == 8000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.PowHegBoxV1.113905.Zmu_8TeV.TXT.v1'
  if runArgs.ecmEnergy == 10000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.PowHegBoxV1.113905.Zmu_10TeV.TXT.v1'
  if runArgs.ecmEnergy == 14000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.PowHegBoxV1.113905.Zmu_14TeV.TXT.v1'
except NameError:
  pass

evgenConfig.efficiency = 0.9
evgenConfig.minevents  = 5000

#
# End of job options file
#
###############################################################
